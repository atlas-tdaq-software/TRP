Confadapter directory
=====================


This part of the TRP package is responsible for providing trigger menu and prescale information to the TRP. It contains two pieces
* the class [TriggerMenuHelpers](confadapter/src/TriggerMenuHelpers.h) that reads the menu and prescales configuration from the TriggerDB for a given set of configuration keys and provides various maps with information
* the TRP adapter [ConfAdapter](confadapter/src/ConfAdapter.h) that writes a subset of the configuration data to IS


Usage of the `TriggerMenuHelpers`
---------------------------------

In `TriggerMenuHelpers` two types of maps are defined
```
typedef std::map<std::string, std::vector<std::string> > Mapping;
typedef std::multimap<std::string, std::string> MultiMapping;
typedef std::map<std::string, float> MapStrInt;
```


An example of how to get all chains that go into a given stream

```
confadapter_imp::TriggerMenuHelpers mh(dbalias);

bool success = mh.getConfiguration( smk, hltpsk, l1psk);

const confadapter_imp::TriggerMenuHelpers::Mapping & stream2chains = mh.getMapping("str_hlt");

for( auto & entry : stream2chains ) {
   const std::string & streamName = entry.first;
   const std::vector<std::string> & chains = entry.second
   ...
}
```

For `const Mapping & TriggerMenuHelpers::getMapping( mapName )` the following maps are currently available:
- `str_hlt`: maps stream name to the vector of chains that go into the stream
- `str_type`: maps stream name to the stream type (*TODO* change return type to string)
- `gr_hlt`: maps group name to the vector of chains that are part of this group
- `cc_hlt`: maps chain ID to the chain name (instead of being a `map<string,vector<string>` a `map<unsigned int,string>` would be better - *TODO*)
- `hlt_ps`: maps chain name to the prescale (returned prescale is a vector<string> - *TODO*) 
- `hlt_l1`: maps the chain name to their seeding l1 items (*TODO* need to split multi-item seeds)

For `const MultiMapping & TriggerMenuHelpers::getMultiMapping( mapName )` the following maps are currently available:
- `CC_HLT`: as `cc_hlt`
- `HLT_L1`: as `hlt_l1`
- `gr_HLT`: as `gr_hlt`
- `HLT_PS`: as `hlt_ps`

Not sure how useful their are, they don't add any extra functionality beyond the map of key2vector. Outside the confadapter they are only used in one more place (xmon).

Testing
-------

A test program [test/ConfReading](../test/test_ConfReading.cxx) exists. It is not installed, so only available when building TRP locally.

The Run 2 DB can be tested with
```
build/TRP/test_ConfReading --smk 2459 --hltpsk 9261 --db TRIGGERDBRUN2 --run2
```

The Run 3 DB can be tested with
```
build/TRP/test_ConfReading --smk 13 --hltpsk 18 --l1psk 38 --db TRIGGERDB
```

Make sure you have `TRIGGERDB` and `TRIGGERDBRUN2` defined in the CORAL lookup and auth files.