#ifndef confadapter_h
#define confadapter_h

#define CONF_DEBUG

#include <iostream>
#include <ctime>
#include <string>
#include <map>

#include "unistd.h"
#include "stdio.h"
#include "signal.h"


#include "is/infoany.h"
#include <is/infodictionary.h>
#include <is/infoT.h>
#include <is/info.h>
#include <is/infoiterator.h>
#include <is/inforeceiver.h>
#include <is/serveriterator.h>
#include "is/infodocument.h"


class ConfAdapter {
 public:
  void Run();
  void ReadInfo();
  ConfAdapter(std::string partition_name, std::string server_name, 
	      int smk_key, int l1_key, int hlt_key, std::string output_name, 
	      bool ri);   

  ConfAdapter(std::string partition_name, std::string server_name, 
	      std::string alias, 
	      std::string output_name, bool ri=true);   
  // Way of implementing the singleton pattern.
  ~ConfAdapter();	// Way of implementing the singleton pattern.
  ConfAdapter( const ConfAdapter & );	// Way of implementing the singleton pattern.
 private:
  unsigned int m_smk;
  unsigned int m_l1k;
  unsigned int m_hltk;
  
  bool remove_info;
  
  std::string pPartitionName;
  std::string pServerName;
  std::string pAlias;
  std::string pOutName;
  IPCPartition * mPartition;	// Partition object, provides connection to the IS.
  ISInfoReceiver * mReceiver;	// Used for callback registration.
  void TriggerKeyCallback(ISCallbackInfo *isc);
};
#endif
