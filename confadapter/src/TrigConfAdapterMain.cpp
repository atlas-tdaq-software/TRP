#include "TRP/TriggerMenuHelpers.h"
#include <iostream>

#include "config/Configuration.h"
#include "ers/ers.h"
#include "TRP/Args.h"
#include "ipc/partition.h"
#include <ipc/core.h>

#include "ConfAdapter.h"
#include "TRP/ReadXML.h"
#include "TRP/Utils.h"
#include <fstream>

using namespace confadapter_imp;
std::string server_name;
std::string partition_name;
std::string alias_name;
bool remove_info;
std::string output_name;

void ReadConfig(std::string  config_file){
  std::cout << "ReadConfig(" << config_file << " ) " << std::endl;
  ReadXML my_reader;
  my_reader.SetElement("trigconfadapter");
  std::vector<std::string> my_str;
  my_str.push_back("do_erase");
  my_str.push_back("alias");
  my_str.push_back("outputname");
  
  my_reader.SetAttribute(my_str);
  
  my_reader.readConfigFile(config_file);
  
  //  partition_name = my_reader.GetValC("partition");
  server_name = my_reader.GetValC("server");
  remove_info = (trp_utils::convertToInt(my_reader.GetVal("do_erase"))==0) ? false : true ;
  alias_name = my_reader.GetVal("alias");
  output_name = my_reader.GetVal("outputname");

}

void showHelp( std::ostream& os, std::string pgmName ){
  os << "Usage: " << pgmName << " [-p <str>]  [-s <str>]  [-rm]  [-out <str>] [-c <file.xml>] " << std::endl;
  os << "                                                                      " << std::endl;
  os << " If one of the theee trigger keys is missing it will take the current " << std::endl;
  os << "  trigger keys from RunParams                                         " << std::endl;
  os << "Options/Arguments:                                                    " << std::endl;
  os << "  -p <str>   partition name  (if not present $TDAQ_PARTITION is used) " << std::endl; 
   os << "  -c <file1.xml:file2.xml:...>   reads info from xml file instead. Picks info from existing file" << std::endl;
  os << "                                                                      " << std::endl;
  os << "  or in alternative from the command line as follows     " << std::endl;
  os << "  -s <str>   server name                                              " << std::endl;
  os << "  -rm Option to remove previous information on rates on IS Server     " << std::endl;
  os << "  -out suffix to add to the TimePoint name                            " << std::endl;
  os << "  -c <file.xml>   reads info from sml file instead" << std::endl;
  os << "Description:                                                          " << std::endl;
  os << "   The ConfAdapterMain reads the Trigger Menu, and stores it in IS    " << std::endl;
  os << "Debug level and verbosity:                                            " << std::endl;
  os << "   They are controlled via the TDAQ_ERS shell variables:              " << std::endl; 
  os << "     - $TDAQ_ERS_DEBUG_LEVEL        [0-4]                             " << std::endl; 
  os << "     - $TDAQ_ERS_VERBOSITY_LEVEL    [0-3]                             " << std::endl;
  os << "     - $TDAQ_ERS_FATAL              [eg: 'lsterr,throw']              " << std::endl;   
  os << "     - $TDAQ_ERS_ERROR                                                " << std::endl;
  os << "     - $TDAQ_ERS_WARNING                                              " << std::endl;
  os << "     - $TDAQ_ERS_INFO               [eg: 'filter(efd),lstdout']       " << std::endl;
  os << "     - $TDAQ_ERS_DEBUG                                                " << std::endl;
  exit(1);
}

int main(int ac, char* av[]) {
  
  
  // get arguments
  Args args( ac, av );
  
  // get program name
  std::string pgmName = av[0];
  std::string::size_type pos = pgmName.rfind( '/' );
  if( pos != std::string::npos )
    pgmName = pgmName.substr( pos+1 );
  
  
  // check if requested help
  if( args.hasArg("-h")     || 
      args.hasArg("-?")     || 
      args.hasArg("--help") || 
      ac <= 1  )
    { showHelp( std::cerr, pgmName ); }
  
  // reconstruct command line string for debugging feedback
  std::stringstream cmdLine;
  cmdLine << av[0];
  for( int i = 1; i < ac; ++i )
    cmdLine << " " << av[i];
  std::cout << "cmdline: '" << cmdLine.str() << "'" << std::endl;
  remove_info = false;

  // Retrive the partition name (from command line or from environment) 
  if (args.hasArg("-p")) {
    partition_name = args.getStr("-p");
    std::cout << "Retrieved partition name from command line: " << partition_name << std::endl;
  } else if (getenv("TDAQ_PARTITION")) {
    partition_name = getenv("TDAQ_PARTITION");
  } else {
    std::cerr << "Partition name not set (neither in command line nor in environment)" << std::endl;
    showHelp( std::cerr, pgmName );
  }

  std::string config_file;
  if (args.hasArg("-c")) {
    config_file  = args.getStr("-c");
    ERS_DEBUG(0, "Config file from  command line: '" << config_file  << "'");
    std::cout<< "Config file from  command line: " << config_file  << std::endl;
    std::vector<std::string> file_list; 
    //      for (unsigned int i = 0; i < config_file.size(); ++i) {
    //      file_list = trp_utils::split(config_file,"\:");
    trp_utils::tokenize(config_file,file_list,":");
    // }
    std::cout << "File_list size is:" << file_list.size() << std::endl;
    for(std::vector<std::string>::iterator it=file_list.begin();
	it!=file_list.end();++it){
      const char* it_cstr = it->c_str();
      std::ifstream my_file(it_cstr);
      if(my_file.is_open()){
	// here reads the file .xml
	ReadConfig(*it);
	break;
      }
    }
  } else {
    
    // Retreive the server name (from command line only) 
    if (args.hasArg("-s")) {
      server_name = args.getStr("-s");
      ERS_DEBUG(0, "Retrieved server name from command line: '" << server_name << "'");
      std::cout << "Retrieved server name from command line: '" << server_name << std::endl ;
    } else { 
      std::cerr << "Server name not set (in command line)" << std::endl;
      showHelp( std::cerr, pgmName );
    }
    if (args.hasArg("-a")) {
      alias_name = args.getStr("-a");
      ERS_DEBUG(0, "Retrieved alias name from command line: '" << alias_name << "'");
      std::cout << "Retrieved alias name from command line: '" << alias_name << std::endl ;
    } 
    if (args.hasArg("-out")) {
      output_name = args.getStr("-out");
      ERS_DEBUG(0, "Retrieved alias name from command line: '" << output_name << "'");
      std::cout << "Retrieved alias name from command line: '" << output_name << std::endl ;
    } 

    if( args.hasArg("-rm")){
      remove_info = true;
      std::cout << "Remove previous information from IS" << std::endl;
    }
    
  } // end else read config
  

  // dump the config
  std::cout << partition_name  << " ,  " << server_name << " , " << remove_info << std::endl;
  std::cout << "config_file = " << config_file << " alais_name:" << alias_name << std::endl;
  
  // here starts the real thing
  IPCCore::init( ac, av );
  ConfAdapter my_ca(partition_name, server_name, alias_name, output_name, remove_info);
  my_ca.ReadInfo();
  my_ca.Run();
} // end main

