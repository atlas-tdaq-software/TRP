#include "ConfAdapter.h"
#include "TRP/TriggerMenuHelpers.h"
#include <iostream>
#include "ers/ers.h"

#include "ipc/partition.h"
#include <ipc/core.h>

#include "TRP/Tree.h"
#include "TRP/TrigConfSmKey.h"
#include "TRP/TrigConfL1PsKey.h"
#include "TRP/TrigConfHltPsKey.h"
#include <vector>
#include <map>
#include <is/info.h>
#include <is/inforeceiver.h>
#include <is/infodictionary.h>
#include <ipc/core.h>
#include "TRP/Utils.h"

using namespace confadapter_imp;
using namespace trp_utils;

ConfAdapter::ConfAdapter(std::string partition_name, std::string server_name, 
			 int smk_key, int l1_key, int hlt_key, std::string output_name, bool ri){
  pPartitionName = partition_name;
  pServerName = server_name;
  m_smk = smk_key;
  m_l1k = l1_key;
  m_hltk = hlt_key;
  std::cout << "ConfAdapter::ConfAdapter with keys: should never be there!" << std::endl;
  remove_info = ri;
  pOutName = output_name;
}
ConfAdapter::ConfAdapter(std::string partition_name, 
			 std::string server_name, 
			 std::string alias,
			 std::string output_name, 
			 bool ri){
  pPartitionName = partition_name;
  pServerName = server_name;
  m_smk = -999;
  m_l1k = -999;
  m_hltk = -999;
  pOutName = output_name;  
  remove_info = ri;
  pAlias = alias;
  std::cout << "Ending ConfAdapter constructor" << std::endl;
}
ConfAdapter::ConfAdapter(const ConfAdapter& )
{
  // implemented only to forbid copying
  
}

ConfAdapter::~ConfAdapter() 
{
  ERS_DEBUG( 3, "ConfAdapter::~ConfAdapter()" );
}
void ConfAdapter::Run(){
  std::cout << "Starting Confadapter::Run" << std::endl;
  //  if(!mPartition) mPartition = new IPCPartition( pPartitionName );
  mPartition = new IPCPartition( pPartitionName );
  std::cout << "pippo1" << std::endl;
  ISInfoReceiver rec(*mPartition);
  std::cout << "pippo2" << std::endl;
  rec.subscribe("RunParams", TrigConfSmKey::type() , 
		&ConfAdapter::TriggerKeyCallback, this);  
  rec.subscribe("RunParams", TrigConfL1PsKey::type() , 
		&ConfAdapter::TriggerKeyCallback, this);
  rec.subscribe("RunParams", TrigConfHltPsKey::type() , 
		&ConfAdapter::TriggerKeyCallback, this);
  std::cout << "pippo3" << std::endl;
  //rec.run();
}

void ConfAdapter::TriggerKeyCallback(ISCallbackInfo *isc) {
  if(isc->reason() == ISInfo::Deleted) return;
  std::cout << "Trigger Keys Changed:" << std::endl;
  TrigConfSmKey sm_k;
  TrigConfL1PsKey l1_k;
  TrigConfHltPsKey hlt_k;
  isc->value(sm_k);
  isc->value(l1_k);
  isc->value(hlt_k);
  
  int smk = sm_k.SuperMasterKey;
  int l1k = l1_k.L1PrescaleKey;
  int hltk = hlt_k.HltPrescaleKey;
  std::cout << " New Trigger Keys SMK " << smk << "  L1K:" << l1k << "  HLTK:" << hltk << std::endl;
  std::cout << pPartitionName <<" , " <<  pServerName << " , " << pAlias << std::endl;
  TriggerMenuHelpers menu(pPartitionName, pAlias);
  //  menu.set_db(pAlias);
  menu.getConfiguration(smk, hltk, l1k);
    
  std::string str_out = pServerName;
  if(remove_info){
    std::cout << "Remove info" << std::endl;
    std::string str_erase = "L2_EF"+pOutName+".*";
    menu.EraseInfo(str_out,str_erase);
    str_erase = "L2_L2"+pOutName+".*";
    menu.EraseInfo(str_out,str_erase);
    str_erase = "L2_EF"+pOutName+".*";
    menu.EraseInfo(str_out,str_erase);
    str_erase = "STR_L2"+pOutName+".*";
    menu.EraseInfo(str_out,str_erase);
    str_erase = "GR_EF"+pOutName+".*";
    menu.EraseInfo(str_out,str_erase);
    str_erase = "GR_L2"+pOutName+".*";
    menu.EraseInfo(str_out,str_erase);
    str_erase = "L2_PS"+pOutName+".*";
    menu.EraseInfo(str_out,str_erase);
    str_erase = "EF_PS"+pOutName+".*";
    menu.EraseInfo(str_out,str_erase);
  }
  // L2 <-->PS
  std::vector<Branch> b_l2_ps;
  const TriggerMenuHelpers::MultiMapping & m_l2_ps = menu.getMultiMapping("L2_PS");
  menu.Mapping2Branch(m_l2_ps,b_l2_ps);
  str_out = pServerName + ".";
  str_out += "L2_PS"+pOutName;
  std::cout << "menu.PublishBranch(str_out,b_l2_ps)" << std::endl;
  menu.PublishBranch(str_out,b_l2_ps);
#ifdef CONF_DEBUG
  std::cout << "-------------------------------------------" << std::endl;  
  menu.printMultiMapping(m_l2_ps ," " );
  std::cout << "-------------------------------------------" << std::endl;  
#endif

  // EF <-->PS
  std::vector<Branch> b_ef_ps;
  const TriggerMenuHelpers::MultiMapping & m_ef_ps = menu.getMultiMapping("EF_PS");
  menu.Mapping2Branch(m_ef_ps,b_ef_ps);
  str_out = pServerName + ".";
  str_out += "EF_PS"+pOutName;
  //std::cout << "menu.PublishBranch(str_out,b_ef_ps)" << std::endl;
  menu.PublishBranch(str_out,b_ef_ps);
#ifdef CONF_DEBUG
  std::cout << "-------------------------------------------" << std::endl;  
  menu.printMultiMapping(m_ef_ps ," " );
  std::cout << "-------------------------------------------" << std::endl;  
#endif
  
  
  
  // L1 <-->L2
  std::vector<Branch> b_l2_l1;
  const TriggerMenuHelpers::MultiMapping & m_l2_l1 = menu.getMultiMapping("L1_L2");
  menu.Mapping2Branch(m_l2_l1,b_l2_l1);
  str_out = pServerName + ".";
  str_out += "L1_L2"+pOutName;
  //std::cout << "menu.PublishBranch(str_out,b_l2_l1)" << std::endl;
  menu.PublishBranch(str_out,b_l2_l1);

#ifdef CONF_DEBUG
  std::cout << "-------------------------------------------" << std::endl;  
  menu.printMultiMapping(m_l2_l1 ," " );  
  std::cout << "-------------------------------------------" << std::endl;  
#endif

    
  // L2 <---> EF
  std::vector<Branch> b_ef_l2;
  const TriggerMenuHelpers::MultiMapping & m_ef_l2 = menu.getMultiMapping("L2_EF");
  menu.Mapping2Branch(m_ef_l2,b_ef_l2);
  str_out = pServerName + ".";
  str_out += "L2_EF"+pOutName;
  //std::cout << "menu.PublishBranch(str_out,b_ef_l2)" << std::endl;
  menu.PublishBranch(str_out,b_ef_l2);
#ifdef CONF_DEBUG
  std::cout << "-------------------------------------------" << std::endl;  
  menu.printMultiMapping(m_ef_l2 ," " );  
  std::cout << "-------------------------------------------" << std::endl;  
#endif
    
  // L1 <---> L2 <---> EF
  std::vector<Tree> t_chain;
  str_out = pServerName + ".";
  str_out += "L1_L2_EF"+pOutName;
  menu.Mapping2Tree(m_l2_l1,m_ef_l2,t_chain);
  //std::cout << "menu.PublishTree(str_out,t_chain)" << std::endl;
  menu.PublishTree(str_out,t_chain);
  
  // stream <--> L2
  std::vector<Branch> b_str_l2;
  const TriggerMenuHelpers::MultiMapping & m_str_l2 = menu.getMultiMapping("str_L2");
  menu.Mapping2Branch(m_str_l2,b_str_l2);
  str_out = pServerName + ".";
  str_out += "STR_L2"+pOutName;
  //std::cout << "menu.PublishBranch(str_out,b_str_l2)" << std::endl;
  menu.PublishBranch(str_out,b_str_l2);
#ifdef CONF_DEBUG
  std::cout << "-------------------------------------------" << std::endl;  
  menu.printMultiMapping(m_str_l2 ," " );  
  std::cout << "-------------------------------------------" << std::endl;  
#endif

    
    
  // stream <--> EF
  std::vector<Branch> b_str_ef;
  const TriggerMenuHelpers::MultiMapping & m_str_ef = menu.getMultiMapping("str_EF");
  menu.Mapping2Branch(m_str_ef,b_str_ef);
  str_out = pServerName + ".";
  str_out += "STR_EF"+pOutName;
  //std::cout << "menu.PublishBranch(str_out,b_str_ef)" << std::endl;
  menu.PublishBranch(str_out,b_str_ef);
#ifdef CONF_DEBUG
  std::cout << "-------------------------------------------" << std::endl;  
  menu.printMultiMapping(m_str_ef ," " );  
  std::cout << "-------------------------------------------" << std::endl;  
#endif    

  // stream <--> type
  std::vector<Branch> b_str_type;
  const TriggerMenuHelpers::MultiMapping & m_str_type = menu.getMultiMapping("str_TYPE");
  menu.Mapping2Branch(m_str_type,b_str_type);
  str_out = pServerName + ".";
  str_out += "STR_TYPE"+pOutName;
  //std::cout << "menu.PublishBranch(str_out,b_str_type)" << std::endl;
  menu.PublishBranch(str_out,b_str_type);
#ifdef CONF_DEBUG
  std::cout << "-------------------------------------------" << std::endl;  
  menu.printMultiMapping(m_str_type ," " );  
  std::cout << "-------------------------------------------" << std::endl;  
#endif    
  
  // group <---> L2
  std::vector<Branch> b_gr_l2;
  const TriggerMenuHelpers::MultiMapping & m_gr_l2 = menu.getMultiMapping("gr_L2");
  menu.Mapping2Branch(m_gr_l2,b_gr_l2);
  str_out = pServerName + ".";
  str_out += "GR_L2"+pOutName;
  //std::cout << "menu.PublishBranch(str_out,b_gr_l2)" << std::endl;
  menu.PublishBranch(str_out,b_gr_l2);
#ifdef CONF_DEBUG
  std::cout << "-------------------------------------------" << std::endl;  
  menu.printMultiMapping(m_gr_l2 ," " );  
  std::cout << "-------------------------------------------" << std::endl;  
#endif    
  
  // group <---> EF
  std::vector<Branch> b_gr_ef;
  const TriggerMenuHelpers::MultiMapping & m_gr_ef = menu.getMultiMapping("gr_EF");
  menu.Mapping2Branch(m_gr_ef,b_gr_ef);
  str_out = pServerName + ".";
  str_out += "GR_EF"+pOutName;
  //std::cout << "menu.PublishBranch(str_out,b_gr_ef)" << std::endl;
  menu.PublishBranch(str_out,b_gr_ef);
#ifdef CONF_DEBUG
  std::cout << "-------------------------------------------" << std::endl;  
  menu.printMultiMapping(m_gr_ef ," " );  
  std::cout << "-------------------------------------------" << std::endl;  
#endif    
}



void ConfAdapter::ReadInfo() {
  std::cout << "Starting Confadapter::ReadInfo" << std::endl;
  if(!mPartition) mPartition = new IPCPartition( pPartitionName );
  GetTriggerKeys(pPartitionName, m_smk, m_hltk, m_l1k  );

  TriggerMenuHelpers menu(pPartitionName, pAlias);
  
  //  std::cout << "Reading Config :" << m_smk << " : " << m_l1k << " : " 	    << m_hltk << std::endl; 
  
  menu.getConfiguration(m_smk, m_hltk, m_l1k);
    
  std::string str_out = pServerName;
  if(remove_info){  
    menu.EraseInfo(str_out,"L2_EF.*");
    menu.EraseInfo(str_out,"L1_L2.*");
    menu.EraseInfo(str_out,"STR_EF.*");
    menu.EraseInfo(str_out,"STR_L2.*");
    menu.EraseInfo(str_out,"GR_EF.*");
    menu.EraseInfo(str_out,"GR_L2.*");
    menu.EraseInfo(str_out,"L2_PS.*");
    menu.EraseInfo(str_out,"EF_PS.*");
  }
  // L2 <-->PS
  std::vector<Branch> b_l2_ps;
  const TriggerMenuHelpers::MultiMapping & m_l2_ps = menu.getMultiMapping("L2_PS");
  menu.Mapping2Branch(m_l2_ps,b_l2_ps);
  str_out = pServerName + ".";
  str_out += "L2_PS";
  std::cout << "menu.PublishBranch(str_out,b_l2_ps)" << std::endl;
  menu.PublishBranch(str_out,b_l2_ps);
  //  menu.printMultiMapping(m_l2_l1 ," " );
  //std::cout << "-------------------------------------------" << std::endl;  
  
  // EF <-->PS
  std::vector<Branch> b_ef_ps;
  const TriggerMenuHelpers::MultiMapping & m_ef_ps = menu.getMultiMapping("EF_PS");
  menu.Mapping2Branch(m_ef_ps,b_ef_ps);
  str_out = pServerName + ".";
  str_out += "EF_PS";
  //std::cout << "menu.PublishBranch(str_out,b_ef_ps)" << std::endl;
  menu.PublishBranch(str_out,b_ef_ps);
  // menu.printMultiMapping(m_l2_l1 ," " );
  std::cout << "-------------------------------------------" << std::endl;  
  
  
  
  // L1 <-->L2
  std::vector<Branch> b_l2_l1;
  const TriggerMenuHelpers::MultiMapping & m_l2_l1 = menu.getMultiMapping("L1_L2");
  menu.Mapping2Branch(m_l2_l1,b_l2_l1);
  str_out = pServerName + ".";
  str_out += "L1_L2";
  std::cout << "menu.PublishBranch(str_out,b_l2_l1)" << std::endl;
  menu.PublishBranch(str_out,b_l2_l1);
  //menu.printMultiMapping(m_l2_l1 ," " );
  std::cout << "-------------------------------------------" << std::endl;  
    
  // L2 <---> EF
  std::vector<Branch> b_ef_l2;
  const TriggerMenuHelpers::MultiMapping & m_ef_l2 = menu.getMultiMapping("L2_EF");
  menu.Mapping2Branch(m_ef_l2,b_ef_l2);
  str_out = pServerName + ".";
  str_out += "L2_EF";
  std::cout << "menu.PublishBranch(str_out,b_ef_l2)" << std::endl;
  menu.PublishBranch(str_out,b_ef_l2);
  //menu.printMultiMapping(m_ef_l2 ," " );
  std::cout << "-------------------------------------------" << std::endl;
    
  // L1 <---> L2 <---> EF
  std::vector<Tree> t_chain;
  str_out = pServerName + ".";
  str_out += "L1_L2_EF";
  menu.Mapping2Tree(m_l2_l1,m_ef_l2,t_chain);
  std::cout << "menu.PublishTree(str_out,t_chain)" << std::endl;
  menu.PublishTree(str_out,t_chain);
  
  // stream <--> L2
  std::vector<Branch> b_str_l2;
  const TriggerMenuHelpers::MultiMapping & m_str_l2 = menu.getMultiMapping("str_L2");
  menu.Mapping2Branch(m_str_l2,b_str_l2);
  str_out = pServerName + ".";
  str_out += "STR_L2";
  std::cout << "menu.PublishBranch(str_out,b_str_l2)" << std::endl;
  menu.PublishBranch(str_out,b_str_l2);
  //menu.printMultiMapping(m_str_l2 ," " );
    
    
  // stream <--> EF
  std::vector<Branch> b_str_ef;
  const TriggerMenuHelpers::MultiMapping & m_str_ef = menu.getMultiMapping("str_EF");
  menu.Mapping2Branch(m_str_ef,b_str_ef);
  str_out = pServerName + ".";
  str_out += "STR_EF";
  std::cout << "menu.PublishBranch(str_out,b_str_ef)" << std::endl;
  menu.PublishBranch(str_out,b_str_ef);
  //menu.printMultiMapping(m_str_ef ," " );
    
  
  // group <---> L2
  std::vector<Branch> b_gr_l2;
  const TriggerMenuHelpers::MultiMapping & m_gr_l2 = menu.getMultiMapping("gr_L2");
  menu.Mapping2Branch(m_gr_l2,b_gr_l2);
  str_out = pServerName + ".";
  str_out += "GR_L2";
  std::cout << "menu.PublishBranch(str_out,b_gr_l2)" << std::endl;
  menu.PublishBranch(str_out,b_gr_l2);
  //menu.printMultiMapping(m_gr_l2 ," " );
  
  // group <---> EF
  std::vector<Branch> b_gr_ef;
  const TriggerMenuHelpers::MultiMapping & m_gr_ef = menu.getMultiMapping("gr_EF");
  menu.Mapping2Branch(m_gr_ef,b_gr_ef);
  str_out = pServerName + ".";
  str_out += "GR_EF";
  std::cout << "menu.PublishBranch(str_out,b_gr_ef)" << std::endl;
  menu.PublishBranch(str_out,b_gr_ef);
  //menu.printMultiMapping(m_gr_ef ," " );
  
  
}
