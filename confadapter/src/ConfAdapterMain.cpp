#include "TRP/TriggerMenuHelpers.h"
#include <iostream>

#include "config/Configuration.h"
#include "ers/ers.h"
#include "TRP/Args.h"
#include "ipc/partition.h"
#include <ipc/core.h>

#include "TRP/Tree.h"
#include <vector>
#include <map>


void showHelp( std::ostream& os, std::string pgmName ){
  os << "Usage: " << pgmName << " [-p <str>]  [-s <str>]  [-smk <int>] [-hlt <int>] [-l1k <int>] [-rm]" << std::endl;
  os << "                                                                      " << std::endl;
  os << "Options/Arguments:                                                    " << std::endl;
  os << "  -p <str>   partition name  (if not present $TDAQ_PARTITION is used) " << std::endl; 
  os << "  -s <str>   server name                                              " << std::endl;
  os << "  -smk <int> supermaster trigger key  (SMK) (if not present 369 will be used )  " << std::endl;
  os << "  -hltk <int> HLT trigger key  (HLTK) (if not present 390 will be used )" << std::endl;
  os << "  -l1k <int> L1 trigger key  (L1K) (if not present 541 will be used )  " << std::endl;
  os << "  -rm Option to remove previous information on rates on IS Server  " << std::endl;
  os << "Description:                                                           " << std::endl;
  os << "   The ConfAdapterMain reads the Trigger Menu, and stores it in IS     " << std::endl;
  os << "Debug level and verbosity:                                            " << std::endl;
  os << "   They are controlled via the TDAQ_ERS shell variables:              " << std::endl; 
  os << "     - $TDAQ_ERS_DEBUG_LEVEL        [0-4]                             " << std::endl; 
  os << "     - $TDAQ_ERS_VERBOSITY_LEVEL    [0-3]                             " << std::endl;
  os << "     - $TDAQ_ERS_FATAL              [eg: 'lsterr,throw']              " << std::endl;   
  os << "     - $TDAQ_ERS_ERROR                                                " << std::endl;
  os << "     - $TDAQ_ERS_WARNING                                              " << std::endl;
  os << "     - $TDAQ_ERS_INFO               [eg: 'filter(efd),lstdout']       " << std::endl;
  os << "     - $TDAQ_ERS_DEBUG                                                " << std::endl;
  exit(1);
}

int main(int ac, char* av[]) {
  
  
  // get arguments
  Args args( ac, av );
  
  // get program name
  std::string pgmName = av[0];
  std::string::size_type pos = pgmName.rfind( '/' );
  if( pos != std::string::npos )
    pgmName = pgmName.substr( pos+1 );
  
  
  // check if requested help
  if( args.hasArg("-h")     || 
      args.hasArg("-?")     || 
      args.hasArg("--help") || 
      ac <= 1  )
    { showHelp( std::cerr, pgmName ); }
  
  // reconstruct command line string for debugging feedback
  std::stringstream cmdLine;
  cmdLine << av[0];
  for( int i = 1; i < ac; ++i )
    cmdLine << " " << av[i];
  std::cout << "cmdline: '" << cmdLine.str() << "'" << std::endl;
  
  // Retrive the partition name (from command line or from environment) 
  std::string partition_name;
  if (args.hasArg("-p")) {
    partition_name = args.getStr("-p");
    ERS_DEBUG(0, "Retrieved partition name from command line: '" << partition_name << "'");
  } else if (getenv("TDAQ_PARTITION")) {
    partition_name = getenv("TDAQ_PARTITION");
    ERS_DEBUG(0, "Retrieved partition name from environment: '" << partition_name << "'");
  } else {
    std::cerr << "Partition name not set (neither in command line nor in environment)" << std::endl;
    showHelp( std::cerr, pgmName );
  }
  
  // Retreive the server name (from command line only) 
  std::string server_name;
  if (args.hasArg("-s")) {
    server_name = args.getStr("-s");
    ERS_DEBUG(0, "Retrieved server name from command line: '" << server_name << "'");
    std::cout << "Retrieved server name from command line: '" << server_name << std::endl ;
  } else { 
    std::cerr << "Server name not set (in command line)" << std::endl;
    showHelp( std::cerr, pgmName );
  }
  
  // Retreive the trigger keys 

  int smk;
  if (args.hasArg("-smk")) {
    smk = args.getInt("-smk");
    ERS_DEBUG(0, "Retrieved SMK  from command line: '" << smk << "'");
    std::cout << "Retrieved SMK  from command line: '" << smk << "'" << std::endl;
  }
  else { 
    smk = 383;
    std::cerr << "SMK not set (in command line) using default: " << smk << std::endl;
  }
  int hlt;
  if (args.hasArg("-hltk")) {
    hlt = args.getInt("-hltk");
    ERS_DEBUG(0, "Retrieved HLT key  from command line: '" << hlt << "'");
    std::cout << "Retrieved HLT key  from command line: '" << hlt << std::endl;

  } else { 
    hlt = 416;
    std::cerr << "HLT key not set (in command line) using default: " << hlt << std::endl;
  }
  int l1k;
  if (args.hasArg("-l1k")) {
    l1k = args.getInt("-l1k");
    ERS_DEBUG(0, "Retrieved L1 key  from command line: '" << l1k << "'");
    std::cout << "Retrieved L1 key  from command line: '" << l1k << std::endl;
  } else { 
    l1k = 556;
    std::cerr << "HLT key not set (in command line) using default: " << l1k << std::endl;
  }
  bool remove_info = false;
  if( args.hasArg("-rm")){
    remove_info = true;
    std::cout << "Remove previous information from IS" << std::endl;
  }
  
  // here starts the real thing
  IPCCore::init( ac, av );

  using namespace confadapter_imp;
  TriggerMenuHelpers menu(partition_name, server_name);
  // these should be taken from command line or AS
  
  menu.getConfiguration(smk, hlt, l1k);

  

  // erase info
  std::string str_out = server_name;
  //str_out += "L2_EF";
  if(remove_info){
    menu.EraseInfo(str_out,"L2_EF.*");
    menu.EraseInfo(str_out,"L1_L2.*");
    menu.EraseInfo(str_out,"STR_EF.*");
    menu.EraseInfo(str_out,"STR_TYPE.*");
    menu.EraseInfo(str_out,"STR_L2.*");
    menu.EraseInfo(str_out,"GR_EF.*");
    menu.EraseInfo(str_out,"GR_L2.*");
    menu.EraseInfo(str_out,"L2_PS.*");
    menu.EraseInfo(str_out,"EF_PS.*");
    
  }
  
  std::cout << "-------------------------------------------" << std::endl;

  // L2 <-->PS
  std::vector<Branch> b_l2_ps;
  const TriggerMenuHelpers::MultiMapping & m_l2_ps = menu.getMultiMapping("L2_PS");
  menu.Mapping2Branch(m_l2_ps,b_l2_ps);
  str_out = server_name + ".";
  str_out += "L2_PS";
  std::cout << "menu.PublishBranch(str_out,b_l2_ps)" << std::endl;
  menu.PublishBranch(str_out,b_l2_ps);
  std::cout << "-------------------------------------------" << std::endl;  

  // EF <-->PS
  std::vector<Branch> b_ef_ps;
  const TriggerMenuHelpers::MultiMapping & m_ef_ps = menu.getMultiMapping("EF_PS");
  menu.Mapping2Branch(m_ef_ps,b_ef_ps);
  str_out = server_name + ".";
  str_out += "EF_PS";
  std::cout << "menu.PublishBranch(str_out,b_ef_ps)" << std::endl;
  menu.PublishBranch(str_out,b_ef_ps);
  std::cout << "-------------------------------------------" << std::endl;  

  

  // L1 <-->L2
  std::vector<Branch> b_l2_l1;
  const TriggerMenuHelpers::MultiMapping & m_l2_l1 = menu.getMultiMapping("L1_L2");
  menu.Mapping2Branch(m_l2_l1,b_l2_l1);
  str_out = server_name + ".";
  str_out += "L1_L2";
  std::cout << "menu.PublishBranch(str_out,b_l2_l1)" << std::endl;
  menu.PublishBranch(str_out,b_l2_l1);
  std::cout << "-------------------------------------------" << std::endl;  

  // L2 <---> EF
  std::vector<Branch> b_ef_l2;
  const TriggerMenuHelpers::MultiMapping & m_ef_l2 = menu.getMultiMapping("L2_EF");
  menu.Mapping2Branch(m_ef_l2,b_ef_l2);
  str_out = server_name + ".";
  str_out += "L2_EF";
  std::cout << "menu.PublishBranch(str_out,b_ef_l2)" << std::endl;
  menu.PublishBranch(str_out,b_ef_l2);
  std::cout << "-------------------------------------------" << std::endl;

  // L1 <---> L2 <---> EF
  std::vector<Tree> t_chain;
  str_out = server_name + ".";
  str_out += "L1_L2_EF";
  menu.Mapping2Tree( m_l2_l1, m_ef_l2, t_chain );
  std::cout << "menu.PublishTree(str_out,t_chain)" << std::endl;
  menu.PublishTree(str_out,t_chain);
  
  // stream <--> L2
  std::vector<Branch> b_str_l2;
  const TriggerMenuHelpers::MultiMapping & m_str_l2 = menu.getMultiMapping("str_L2");
  menu.Mapping2Branch(m_str_l2,b_str_l2);
  str_out = server_name + ".";
  str_out += "STR_L2";
  std::cout << "menu.PublishBranch(str_out,b_str_l2)" << std::endl;
  menu.PublishBranch(str_out,b_str_l2);
  
  
  // stream <--> EF
  std::vector<Branch> b_str_ef;
  const TriggerMenuHelpers::MultiMapping & m_str_ef = menu.getMultiMapping("str_EF");
  menu.Mapping2Branch(m_str_ef,b_str_ef);
  str_out = server_name + ".";
  str_out += "STR_EF";
  std::cout << "menu.PublishBranch(str_out,b_str_ef)" << std::endl;
  menu.PublishBranch(str_out,b_str_ef);


  // stream <--> TYPE
  std::vector<Branch> b_str_type;
  const TriggerMenuHelpers::MultiMapping & m_str_type = menu.getMultiMapping("str_TYPE");
  menu.Mapping2Branch(m_str_type,b_str_type);
  str_out = server_name + ".";
  str_out += "STR_TYPE";
  std::cout << "menu.PublishBranch(str_out,b_str_type)" << std::endl;
  menu.PublishBranch(str_out,b_str_type);


  // group <---> L2
  std::vector<Branch> b_gr_l2;
  const TriggerMenuHelpers::MultiMapping & m_gr_l2 = menu.getMultiMapping("gr_L2");
  menu.Mapping2Branch(m_gr_l2,b_gr_l2);
  str_out = server_name + ".";
  str_out += "GR_L2";
  std::cout << "menu.PublishBranch(str_out,b_gr_l2)" << std::endl;
  menu.PublishBranch(str_out,b_gr_l2);
  
  // group <---> EF
  std::vector<Branch> b_gr_ef;
  const TriggerMenuHelpers::MultiMapping & m_gr_ef = menu.getMultiMapping("gr_EF");
  menu.Mapping2Branch(m_gr_ef,b_gr_ef);
  str_out = server_name + ".";
  str_out += "GR_EF";
  std::cout << "menu.PublishBranch(str_out,b_gr_ef)" << std::endl;
  menu.PublishBranch(str_out,b_gr_ef);

  menu.printMapping(menu.getMapping("l2_l1") ,"c2c " );  // L2 chains -> L1 items
  menu.printMapping(menu.getMapping("ef_l2"), "c2c ");  // EF chains -> L2 chains
  menu.printMapping(menu.getMapping("str_ef"), "s2c ");  // stream tags -> EF chains
  menu.printMapping(menu.getMapping("str_type"), "s2c ");  // stream tags -> type chains  
  menu.printMapping(menu.getMapping("str_l2"), "s2c ");  // stream tags -> L2 chains
  menu.printMapping(menu.getMapping("gr_l2"), "g2c ");  // groups -> L2 chains
  menu.printMapping(menu.getMapping("gr_ef"), "g2c ");  // groups -> EF chains
}
