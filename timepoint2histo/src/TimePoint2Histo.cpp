/** 
 * @file TimePoint2HistoMain.cpp
 * @author Antonio Sidoti
 * @date 21/01/2009
 * @brief Gets hlt histo containing rates from OH
 */


#include "TimePoint2Histo.h"
#include <oh/OHUtil.h>


#include <ipc/core.h>
#include <iostream>
#include "ers/ers.h"
#include <time.h>



#include <ipc/core.h>
#include "TRP/TimePoint_IS.h"
#include <rc/RunParams.h>
#include <TH2F.h>



ERS_DECLARE_ISSUE(timepoint2histo, Issue, "Invalid config", )

using namespace std;




TimePoint2Histo::TimePoint2Histo(std::string partition_name, std::string server_nameIn,  
				 std::string server_nameOut, std::string  tp_regex_str,  
				 std::string  provider_str,  std::string  histogram_str, 
				 std::string  histogram_str2){
  ERS_DEBUG( 0, "TimePoint2Histo::TimePoint2Histo()" );

  pPartitionName = partition_name;
  pServerNameIn  = server_nameIn;
  pServerNameOut = server_nameOut;
  pRegex_str = tp_regex_str;
  pProvider = provider_str;
  pHisto_suff = histogram_str;
  pHisto_pref = histogram_str2;
  my_provider = 0;
  m_is_first_event = true;
  m_num_event = 0;
  cout << "Version 2012-03-21-2" << endl;
}

TimePoint2Histo::TimePoint2Histo(const TimePoint2Histo& )
{
  // implemented only to forbid copying

}

TimePoint2Histo::~TimePoint2Histo()
{
  ERS_DEBUG( 3, "TimePoint2Histo::~TimePoint2Histo()" );
}


void TimePoint2Histo::Run() { // one for L2 and the other for EF
  
  ERS_DEBUG( 3, "TimePoint2Histo::Run");
  IPCPartition Partition(pPartitionName);

  ISCriteria *my_criteria = NULL;
  try {
    my_criteria = new ISCriteria(pRegex_str, TimePoint_IS::type(), 
				 ISCriteria::AND );
  } catch(daq::is::InvalidCriteria &ex) {
    ers::warning(timepoint2histo::Issue(ERS_HERE, "Invalid Criteria "  
					+boost::lexical_cast<std::string>(ex.what())
					+boost::lexical_cast<std::string>(pRegex_str)
					));
  } catch(daq::is::InvalidName &ex) {
    ers::warning(timepoint2histo::Issue(ERS_HERE, "Invalid Name "  
					+boost::lexical_cast<std::string>(ex.what())
					+boost::lexical_cast<std::string>(pRegex_str)
					));
  }


  // first do a first loop overall histos to build the histograms

  ISInfoIterator *ii2 = NULL;
  try {
    ii2 = new ISInfoIterator(Partition, pServerNameIn, *my_criteria);
    while ( (*ii2)() ) {
      // strip the name of the server from the name
      std::string name_h = ii2->name();
      size_t found;
      found = name_h.find(pServerNameIn);
      if(found!=string::npos){
        name_h = pHisto_pref + name_h.substr(found+pServerNameIn.size()+1) + pHisto_suff;
      }
      std::cout << "name is:" << name_h << std::endl;
      TimePoint_IS my_tp;
      ii2->value(my_tp);
      TH2F * th2 = new TH2F( name_h.c_str(), name_h.c_str(), my_tp.NumCol(), 0, my_tp.NumCol(), my_tp.NumRow(), 0, my_tp.NumRow());
      for (int i=1;i<=my_tp.NumCol();i++) th2->GetXaxis()->SetBinLabel(i, (my_tp.XLabels[i-1]).c_str());
      for (int i=1;i<=my_tp.NumRow();i++) th2->GetYaxis()->SetBinLabel(i, (my_tp.YLabels[i-1]).c_str());
    

      my_map_ret = my_map_histo.insert(pair<std::string, TH2F *>(name_h,th2) );
      if(my_map_ret.second==false){
        ers::warning(timepoint2histo::Issue(ERS_HERE, "Histogram already filled"+name_h));
      }
    }
  } catch (daq::is::RepositoryNotFound &ex){
    ers::warning(timepoint2histo::Issue(ERS_HERE, "IS Repository not found " 
				    +boost::lexical_cast<std::string>(ex.what())));
    exit(0);
  } catch (daq::is::InvalidCriteria &ex) {
    ers::warning(timepoint2histo::Issue(ERS_HERE, "Invalid criteria " 
                                    +boost::lexical_cast<std::string>( ex.what())));
  }

  // now the histograms are in memory


  ISInfoReceiver receiver(Partition);
  Receiver = &receiver;
  try {
    Receiver->subscribe(pServerNameIn, *my_criteria, &TimePoint2Histo::callback_tp2oh, this);
  } catch(daq::is::RepositoryNotFound &i_ex){
    ers::warning(timepoint2histo::Issue(ERS_HERE, "Repository Not Found"+
					boost::lexical_cast<std::string>(i_ex.what())));
  }
  catch(daq::is::AlreadySubscribed &i_ex){
    ers::warning(timepoint2histo::Issue(ERS_HERE, "Already Subscribed"+
					boost::lexical_cast<std::string>(i_ex.what())));
  }
  catch(daq::is::InvalidCriteria &i_ex){
    ers::warning(timepoint2histo::Issue(ERS_HERE, "Invalid Criteria"+
					boost::lexical_cast<std::string>(i_ex.what())));
  }

  try {
    my_provider = new OHRootProvider(Partition, pServerNameOut.c_str(), pProvider.c_str());
  } catch( daq::oh::Exception & i_ex ) {
    ers::fatal(timepoint2histo::Issue(ERS_HERE, "Exception"+
				      boost::lexical_cast<std::string>(i_ex.what())));
    exit(0);
  }

  
  //Receiver->run();
}


void TimePoint2Histo::callback_tp2oh(ISCallbackInfo *isc) {
  if(isc->reason() == ISInfo::Deleted) return;
  ++m_num_event;
  
  bool be_verbose = false;
  
  if(m_is_first_event || 
     m_num_event%100==0 || 
     m_num_event%100==1 || 
     m_num_event%100==2) be_verbose = true;
  if(be_verbose){
    cout << "--CALLBACK:: " << isc->name() << '\n'; // with server name
    cout << "--Reason code: " << isc->reason() << '\n';
    cout << "num_event:" << m_num_event << endl;
  }
  TimePoint_IS my_tp;
  isc->value(my_tp);
  
  std::string name_2 = isc->name();
  if(be_verbose) std::cout << "Name 2:" << name_2 << std::endl;
  size_t found;
  found = name_2.find(pServerNameIn);
  if(found!=string::npos){
    name_2 = pHisto_pref + name_2.substr(found+pServerNameIn.size()+1) + pHisto_suff;
  }
  if(be_verbose) std::cout << "Name 2 2:" << name_2 << std::endl;
  TH2F *th2 = my_map_histo[name_2];
  for(int ix = 0; ix < my_tp.NumCol();++ix){
    for(int iy = 0; iy < my_tp.NumRow();++iy){
      float val  = -9;
      if(my_tp.get(ix, iy, val)){ // this is the correct
	th2->SetBinContent(ix+1, iy+1, val);
      }
    }
  }
  if(be_verbose) std::cout << "Inserting " << std::endl;  
  try {
    if(be_verbose) std::cout << "Publishing " << std::endl;
    my_provider -> publish( *(TH1*)th2, name_2);
  } catch( daq::oh::Exception & i_ex ) {
    ers::error(timepoint2histo::Issue(ERS_HERE, "Exception"+
				      boost::lexical_cast<std::string>(i_ex.what())));
  }
  if(m_is_first_event){
    m_is_first_event=false;
  }
  
}

void TimePoint2Histo::Stop(){
  cout << "In Stop" << endl;
  if (Receiver != 0) {
    cout << "Has Receiver" << endl;
    try {
      Receiver->unsubscribe(pServerNameIn);
    } catch(...){
      std::cout << " Cannot unsubscribe to " << pServerNameIn  << std::endl;
      exit(0);
    }
  }
}

void TimePoint2Histo::signal_handler() {
  cout << "in signal_handler" << endl;
  if (Receiver != 0) {
    std::cout << " Has Receiver" << endl;
    try {
      //Receiver->stop();
    } catch(...){
      std::cout << " Problem in Stopping Receiver" << endl;
      exit(0);
    }
  } else {
    exit(0);
  }
  exit(0);
}
