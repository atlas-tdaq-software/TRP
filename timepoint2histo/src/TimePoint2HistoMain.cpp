
#include <iostream>
#include <sstream>
#include <stdexcept>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#include "ers/ers.h"
#include "TRP/Args.h"

#include "config/Configuration.h"
#include "ipc/partition.h"
#include <ipc/core.h>
#include "boost/smart_ptr.hpp"

#include "TimePoint2Histo.h"


#include "TRP/ReadXML.h"
#include "TRP/Utils.h"
#include <fstream>
#include <signal.h>

#undef TRP_DEBUG
#define TRP_DEBUG(lvl,msg) //cout << lvl << " ** " << msg << endl

std::string partition_name;
std::string server_nameIn;  // input server name for TP
std::string server_nameOut; // output server name for Histo 
std::string tp_regex_str;   // string for regular expression of timepoint 
std::string provider_str;   // provider name for Histo
std::string histogram_str;  // histogram suffix for Histo
std::string histogram_str2;  // histogram prefix for Histo


std::string threadPer = "0";
std::string maxServerThread = "9";
std::string threadPool = "0";
std::string maxGIOP =  "7";

std::list< std::pair< std::string, std::string > > options_ipc;

namespace
{
  TimePoint2Histo* my_tp_adapter = 0;
  int ReceivedSignal = 0;
  void signal_handler( int sig )
  {
    std::cout << "Received signal:" << sig << endl;
    if (my_tp_adapter != 0) {
      ReceivedSignal = sig;
      my_tp_adapter->signal_handler();
    }
  }
}


void ReadConfig(std::string  config_file){
  std::cout << "ReadConfig(" << config_file << " ) " << std::endl;
  ReadXML my_reader;
  my_reader.SetElement("timepoint2histo");
  std::vector<std::string> my_str;
  my_str.push_back("input_is");
  my_str.push_back("provider");
  my_str.push_back("histosuff");
  my_str.push_back("histopref");
  my_str.push_back("timepoint_regex");
  my_reader.SetAttribute(my_str);

  my_reader.readConfigFile(config_file);
  
  // partition_name = my_reader.GetValC("partition");
  server_nameOut = my_reader.GetValC("server"); //--> this is ISS_TRP
  
  server_nameIn = my_reader.GetVal("input_is"); 
  provider_str  = my_reader.GetVal("provider");  
  histogram_str = my_reader.GetVal("histosuff");
  histogram_str2 = my_reader.GetVal("histopref");
  tp_regex_str  = my_reader.GetVal("timepoint_regex");
  
  
  // read ipc options
  ReadXML my_reader_ipc;
  my_reader_ipc.SetElement("ipcoptions");
  std::vector<std::string> my_str_ipc;
  my_str_ipc.push_back("threadPerConnectionPolicy");
  my_str_ipc.push_back("maxServerThreadPoolSize");
  my_str_ipc.push_back("threadPoolWatchConnection");
  my_str_ipc.push_back("maxGIOPConnectionPerServer");
    
  my_reader_ipc.SetAttribute(my_str_ipc);
  
  my_reader_ipc.readConfigFile(config_file);
  
    threadPer = my_reader_ipc.GetVal("threadPerConnectionPolicy");
  maxServerThread = my_reader_ipc.GetVal("maxServerThreadPoolSize");
  threadPool = my_reader_ipc.GetVal("threadPoolWatchConnection");
  maxGIOP = my_reader_ipc.GetVal("maxGIOPConnectionPerServer");
  
  options_ipc.push_front( std::make_pair( std::string("threadPerConnectionPolicy"),threadPer  ) );
  options_ipc.push_front( std::make_pair( std::string("maxServerThreadPoolSize"), maxServerThread ) );
  options_ipc.push_front( std::make_pair( std::string("threadPoolWatchConnection"), threadPool ) );
  options_ipc.push_front( std::make_pair( std::string("maxGIOPConnectionPerServer"), maxGIOP ) );
  


  std::cout << "End of ReadConfig" << std::endl;
}
 

void showHelp( std::ostream& os, std::string pgmName ){
  os << "Usage: " << pgmName << " [-p <str>]  [-c <file.xml:file2.xml>] " << std::endl;
  os << "                                                                      " << std::endl;
  os << "Options/Arguments:                                                    " << std::endl;
  os << "  -p <str>   partition name  (if not present $TDAQ_PARTITION is used) " << std::endl; 
  os << "  -c <file.xml>   reads info from xml file instead" << std::endl;
  os << " or in alternative " << std::endl;


  os << "                                                                      " << std::endl;
  os << "Description:                                                          " << std::endl;
  os << "   TimePoint2Histo gets TimePoint, converts them in Histo and publishes in OH                        " << std::endl;
  os << "Debug level and verbosity:                                            " << std::endl;
  os << "   They are controlled via the TDAQ_ERS shell variables:              " << std::endl; 
  os << "     - $TDAQ_ERS_DEBUG_LEVEL        [0-4]                             " << std::endl; 
  os << "     - $TDAQ_ERS_VERBOSITY_LEVEL    [0-3]                             " << std::endl;
  os << "     - $TDAQ_ERS_FATAL              [eg: 'lsterr,throw']              " << std::endl;   
  os << "     - $TDAQ_ERS_ERROR                                                " << std::endl;
  os << "     - $TDAQ_ERS_WARNING                                              " << std::endl;
  os << "     - $TDAQ_ERS_INFO               [eg: 'filter(efd),lstdout']       " << std::endl;
  os << "     - $TDAQ_ERS_DEBUG                                                " << std::endl;
  exit(1);
}

int main(int ac, char* av[] ){
  try{
    
    // get arguments
    Args args( ac, av );
    
    // get program name
    std::string pgmName = av[0];
    std::string::size_type pos = pgmName.rfind( '/' );
    if( pos != std::string::npos )
      pgmName = pgmName.substr( pos+1 );
    

    // check if requested help
    if( args.hasArg("-h")     || 
        args.hasArg("-?")     || 
        args.hasArg("--help") || 
        ac <= 1  )
//           || 
//	!args.hasArg("-n"))
      { showHelp( std::cerr, pgmName ); }


    // reconstruct command line string for debugging feedback
    std::stringstream cmdLine;
    cmdLine << av[0];
    for( int i = 1; i < ac; ++i )
      cmdLine << " " << av[i];
    //std::cout << "cmdline: '" << cmdLine.str() << "'" << std::endl;
    ERS_LOG( "cmdline '" << cmdLine.str() << "'" );
      // Retrive the partition name (from command line or from environment) 
    if (args.hasArg("-p")) {
      partition_name = args.getStr("-p");
      ERS_DEBUG(0, "Retrieved partition name from command line: '" << partition_name << "'");
      std::cout << "Retrieved partition name from command line: '" << partition_name << std::endl;
    } else if (getenv("TDAQ_PARTITION")) {
      partition_name = getenv("TDAQ_PARTITION");
      ERS_DEBUG(0, "Retrieved partition name from environment: '" << partition_name << "'");
      std::cout << "Retrieved partition name from environment: '" << partition_name << std::endl;
    } else {
      std::cerr << "Partition name not set (neither in command line nor in environment)" << std::endl;
      showHelp( std::cerr, pgmName );
    }

    std::string config_file;
    if (args.hasArg("-c")) {
      config_file  = args.getStr("-c");
      ERS_DEBUG(0, "Config file from  command line: '" << config_file  << "'");
      std::cout<< "Config file from  command line: '" << config_file  << std::endl;
      std::vector<std::string> file_list; 
      trp_utils::tokenize(config_file,file_list,":");
       std::cout << "File_list size is:" << file_list.size() << std::endl;
      for(std::vector<std::string>::iterator it=file_list.begin();
	  it!=file_list.end();++it){
	const char* it_cstr = it->c_str();
	std::ifstream my_file(it_cstr);
	if(my_file.is_open()){
	  // here reads the file .xml
	  ReadConfig(*it);
	  break;
	}
      }
    }

    
    std::cout << "Dumping Hlt Adapter current config:" << std::endl;
    std::cout << "Partition:" << partition_name << " Input:" << server_nameIn << "  Out:" << server_nameOut << std::endl;
    std::cout << "Providers  :" <<  provider_str << "  HistoSuff:" << histogram_str << std::endl;
    std::cout << "TimePointRegex :" <<  tp_regex_str << std::endl;

    ERS_DEBUG(0, "Starting program job" );
    
    IPCCore::init( ac, av );
    //    IPCCore::init( options_ipc );
  }
  catch( daq::ipc::Exception & ex ) {
    ers::fatal( ex );
    return 1;
  }

  signal(SIGABRT, signal_handler);
  signal(SIGTERM, signal_handler);
  signal(SIGINT,  signal_handler);

  try {
    TRP_DEBUG(0, "Starting program job" );
    my_tp_adapter = new TimePoint2Histo(partition_name,server_nameIn, server_nameOut,
                        tp_regex_str, provider_str, histogram_str, histogram_str2);

    // subscribe to IS and block the main function with semaphore.wait()
    my_tp_adapter->Run();

    // when signal received, signal_handler calls semaphore.post()
    if (ReceivedSignal) {
      std::cout << " Signal " << ReceivedSignal << " received." << std::endl;
    }

    // unsubscribe to IS, this code executed after semaphore.wait()
    my_tp_adapter->Stop();

  }
  catch( ers::Issue &ex)   { TRP_DEBUG( CoreIssue, "Caught ers::Issue: " << ex ); }
  catch( std::exception& e) { TRP_DEBUG( CoreIssue, "Caught std::exception: " << e.what() ); }
  catch( ... )             { TRP_DEBUG( CoreIssue, "Caught unidentified exception" );
  }
  ERS_LOG( "Signal " << ReceivedSignal << " received, stopping '" << server_nameIn << "' server ... " );
  TRP_DEBUG(0, "Program terminating gracefully" );
  ERS_LOG( "done" );
  delete my_tp_adapter;
  return 0;
}
