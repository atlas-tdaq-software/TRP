#ifndef TIMEPOINT2HISTO_H
#define  TIMEPOINT2HISTO_H
/** 
 * @file TimePoint2Histo.cpp
 * @author Antonio Sidoti
 * @date 21/01/2009
 * @brief Gets timepoints, converts them in  histo and publishes in OH
 */

#include <owl/regexp.h>
#include <ipc/core.h>
#include <ipc/partition.h>
#include "is/infoany.h"
#include <is/infodictionary.h>
#include <is/infoT.h>
#include <is/info.h>
#include <is/infoiterator.h>
#include <is/inforeceiver.h>
#include <is/serveriterator.h>
#include "is/infodocument.h"

#include <oh/OHRootReceiver.h>
#include <oh/OHSubscriber.h>
#include <owl/regexp.h>
#include <oh/OHRootProvider.h>

#include <vector>
#include <map>

using namespace std;

class TH2F;

class TimePoint2Histo {
 public:
  TimePoint2Histo( std::string partition_name, std::string server_nameIn,  
		   std::string server_nameOut, std::string  tp_regex_str,  
		   std::string  provider_str,  std::string  histogram_str, 
		   std::string  histogram_str2);
  
  ~TimePoint2Histo();    // Way of implementing the singleton pattern.
  TimePoint2Histo( const TimePoint2Histo & );     // Way of implementing the singleton pattern.
  
  void Run();
  
  void Stop();

  void signal_handler(); 
  

 private:


  std::string pPartitionName;
  std::string pServerNameIn;
  std::string pServerNameOut;
  std::string pRegex_str;
  std::string pProvider;
  std::string pHisto_suff;
  std::string pHisto_pref;
  OHRootProvider * my_provider;
  void callback_tp2oh(ISCallbackInfo *isc);

  ISInfoReceiver   receiver;
  ISInfoReceiver * Receiver;
  
  bool m_is_first_event;
  int m_num_event;
  

  std::map<std::string, TH2F *> my_map_histo;
  pair<std::map<std::string, TH2F *>::iterator,bool> my_map_ret;
  std::map<std::string, TH2F *>::iterator  my_map_it;



  


};

#endif
