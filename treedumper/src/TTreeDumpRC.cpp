/** 
 * @file treedumper.cpp
 * @author Antonio Sidoti Humboldt Universitat zu Berlin
 * @date 02/12/2008
 * @brief Dummy data reader published in  IS.
 */

#include "TTreeDumpRC.h"
#include <iostream>
#include <ipc/core.h>

#include "is/infoany.h"
#include "is/infoT.h"
#include "is/info.h"
#include "is/infodictionary.h"
#include "is/infoiterator.h"
#include "is/inforeceiver.h"
#include "is/serveriterator.h"
#include "ipc/partition.h"
#include "is/infodocument.h"


#include "TRP/TimePoint_IS.h" 

#include "TRP/Utils.h"
#include "cocaIPC/cocaIPC.hh"
#include "ers/ers.h"
#include <boost/filesystem.hpp>   // includes all needed Boost.Filesystem declarations
#include <boost/regex.hpp>
#include <boost/range/algorithm/replace.hpp>   // includes all needed Boost.Replace
#include <boost/filesystem/operations.hpp>
#include <boost/cerrno.hpp>
#include <boost/config.hpp>
#include <boost/detail/lightweight_test.hpp>
#include <boost/bind.hpp>
#include <coca/client/Register.h>
#include <coca/common/Issues.h>
ERS_DECLARE_ISSUE(ttreedumper, Issue, "CoCa Issue", )
  
ERS_DECLARE_ISSUE(ttreedumper, TPChange, "Number of row for timepoint \"" << name_tp <<  
		  "\" has changed. Previous number was " << old_num_row << 
		  "\". Present number is \"" << new_num_row << 
		  "\" . Convene with trigger on call expert to stop and restart the archiver. This error appears only ONCE.", 
		  ((std::string)name_tp) ((int) old_num_row) ((int) new_num_row ))



using namespace std;


map<string, string> map_corr; // map for ntuple leafs disambiguation

boost::regex pattern(":");
boost::regex pattern2("\\.");
boost::regex pattern3("-");
std::string replaced("_");


TTreeDumpRC::TTreeDumpRC(std::string name): daq::rc::Controllable(), //daq::rc::Controllable(name),
                                            MAX_VAR(60000),
                                            MAX_NUM_TREE(100),
                                            f_mydata(0), 
					    m_running(false)
{
  std::cout << " Start TTreeDumpRC constructor" << std::endl;
  file_name = "";
  m_partition_name = "";
  m_server_name = "";
  t_update = 300;
  prio = 10; // from mda/databases/mda/MDA_test_segment.data.xml
  dtc  = 86400;
  std::cout << " End TTreeDumpRC constructor" << std::endl;

  map_corr["L1_Rate"] = "L1_";
  map_corr["HLT_Rate"] = "HLT_";
  map_corr["L1_Rate_Pred"] = "L1p_";
  map_corr["HLT_Rate_Pred"] = "EFp_";
}



void TTreeDumpRC::SetOptions(std::string partition_name,std::string server_name, 
			      std::string file_suff, std::string vars_name, 
			     int update_interval, std::string coca_server_name, 
			     std::string coca_partition, std::string coca_dataset, 
			     std::string coca_top_dir, 
			     float thresh_diff, int avg_pnt, int usecoca,
			     int overwrite_file, int sleep_time){
  std::cout << "Start TTreeDumpRC::SetOptions" << std::endl;
  m_partition_name = partition_name;
  m_server_name = server_name;
  
  m_vars_name = vars_name;
  std::cout << "Regexp this:" << vars_name << std::endl;
  
  t_update = update_interval;

  m_file_suff = file_suff;
  m_usecoca = usecoca;
  if(usecoca==1){
    std::cout << "Instantiating coca_server" << std::endl;
    try {
      coca_server = new daq::coca::Register(coca_server_name, coca_partition);
    } catch (daq::coca::RegisterIssues::IPCException &i_ex){
      ers::warning(ttreedumper::Issue(ERS_HERE, "problem in instantiating IPCException coca_server"));
      std::cout << i_ex.what() << std::endl;
    } catch (daq::coca::RegisterIssues::SetupError &i_se){
      ers::warning(ttreedumper::Issue(ERS_HERE, "problem in instantiating SetupError coca_server"));
      std::cout << i_se.what() << std::endl;
    } catch(...){
      ers::warning(ttreedumper::Issue(ERS_HERE, "Generic problem in instantiating coca_server"));
    }
  }
  dataset = coca_dataset;
  TopDir = coca_top_dir;
  m_avg_point = avg_pnt;
  m_thresh_diff = thresh_diff;
  m_overwrite_file = overwrite_file;
  m_sleep_time = sleep_time;
  std::cout << "End TTreeDumpRC::SetOptions" << std::endl;
}



void TTreeDumpRC::configure(const daq::rc::TransitionCmd & args){
  std::cout << "In TTreeDumpRC::configure" << std::endl;
}


void TTreeDumpRC::connect(const daq::rc::TransitionCmd & args){
  std::cout << "In TTreeDumpRC::connect" << std::endl;
}


//void TTreeDumpRC::startTrigger(std::string & /*s*/){
bool TTreeDumpRC::processPrepareForRun(){
  std::cout << "In TTreeDumpRC::processPrepareForRun" << std::endl;
  CreateFile();
  MyRun();
  m_running = true;
  return false;
}



void TTreeDumpRC::prepareForRun(const daq::rc::TransitionCmd & args){
  std::cout << "In TTreeDumpRC::prepareForRun" << std::endl;
  m_alarm.reset( new IPCAlarm( m_sleep_time, boost::bind( &TTreeDumpRC::processPrepareForRun, this ) ) );
  
}

void TTreeDumpRC::CreateFile(){

  const char *file_name_c;
  unsigned int my_run = 999;
  unsigned short my_lb =  9999;
  trp_utils::GetRunParams(m_partition_name, my_run, my_lb);
  std::string s_run;
  std::stringstream out_run;
  out_run << my_run;
  s_run = out_run.str();
  // here you get the timestamp to avoid accidentally overwriting the file
  time_t seconds;
  seconds = time (NULL);
  std::stringstream out_sec;
  out_sec << seconds;
  std::string s_sec;
  s_sec = out_sec.str();
  file_name = m_file_suff + "_" + m_partition_name + "_" + s_run;
  
  file_name2 = file_name + ".root"; // this the one with that will be stored in CoCa // RELATIVE PATH
  file_name = file_name + "_" + s_sec + ".root";
  file_name = TopDir + file_name; // this one is ABSOLUTE PATH
  file_name_c = file_name.c_str(); 
  cout << "creating root file-: " << file_name_c << endl;
  // check if directory exists
  boost::filesystem::path m_path(TopDir);
  if(!boost::filesystem::is_directory(m_path)){
    std::cout << "Warning! Directory " << TopDir << " doesn't exist. Trying to create one" << std::endl;
    if(boost::filesystem::create_directory( m_path)){
      std::cout << "Created directory" << std::endl;
    } else {
      ers::fatal(ttreedumper::Issue(ERS_HERE, "cannot create directory " + TopDir + " Please create one in the machine where I'm supposed to run and restart me please..."));
      exit(1);
    }
  }

  my_file = new TFile(file_name_c,"recreate");
  
  cout << "Configuring TTreeDumpRC to:" << endl;
  cout << "partition_name = " << m_partition_name << endl;
  cout << "Server name" << m_server_name << endl;
  cout << "file_name" << file_name << endl;
  my_time_stamp_d =0.;
  my_lumi_block=0;
  my_run=0;
  my_smk = 0;
  my_l1k = 0;
  my_hltk = 0;
  my_bgk = 0;
  my_int = 0;
  m_var.clear();
  empty_server = false;
  prev_t =   time(NULL);
  my_file->ls();
  m_is_first_event = true;
  //  my_tp_vec_new.reserve(10);

  std::cout << "End of CreateFile" << std::endl;
}


void TTreeDumpRC::MyRun(){
  // li mortacci dello startofTrigger
  it_ct_as.clear();
  m_is_first_event_tree.clear();
  it_ct_as.reserve(MAX_NUM_TREE);
  m_is_first_event_tree.reserve(MAX_NUM_TREE);
  f_mydata = new float[MAX_VAR];
  m_is_first_event_timepoint.clear();
  m_is_tree_built.clear();
  mMap.clear();
  m_num_row.clear();
  it_ct = 0;
  id = 0;
  id_tree = 0;
  my_tree_vec.clear();
  
  // erase the various maps and deques
  old_lumiblock_map.clear();
  my_tp_map_deq.clear();
  m_is_first_event=true;
  
  
  //  mPartition = 0;
  if (!mPartition) mPartition = new IPCPartition( m_partition_name );
  rec = NULL;
  try {
    rec = new ISInfoReceiver(*mPartition);
  } catch(...) {
    ers::warning(ttreedumper::Issue(ERS_HERE, "No ISInfoReceiver "));
  }
  

  // now you subscribe to the servers
  
  ISCriteria *my_criteria = NULL;
  try {
    my_criteria = new ISCriteria(m_vars_name, TimePoint_IS::type(), 
			     ISCriteria::AND ); 
  } catch(daq::is::InvalidCriteria &ex) {
    ers::warning(ttreedumper::Issue(ERS_HERE, "Invalid Criteria "  
				    +boost::lexical_cast<std::string>(ex.what())
				    +boost::lexical_cast<std::string>(m_vars_name)
				    ));
  }
  
  try {
    ers::info(ttreedumper::Issue(ERS_HERE, "Subscribing to:" + m_vars_name ));
    rec->subscribe(m_server_name, *my_criteria, &TTreeDumpRC::callback_tp, this);
  } catch(daq::is::RepositoryNotFound &i_ex){
    ers::warning(ttreedumper::Issue(ERS_HERE, "Repository Not Found"+boost::lexical_cast<std::string>(i_ex.what())));
  }
  catch(daq::is::AlreadySubscribed &i_ex){
    ers::warning(ttreedumper::Issue(ERS_HERE, "Already Subscribed"+boost::lexical_cast<std::string>(i_ex.what())));
  }
  catch(daq::is::InvalidCriteria &i_ex){
    ers::warning(ttreedumper::Issue(ERS_HERE, "Invalid Criteria"+boost::lexical_cast<std::string>(i_ex.what())));
  }
} // end of MyRun method
  
void TTreeDumpRC::callback_tp(ISCallbackInfo *isc) {
  if(isc->reason() == ISInfo::Deleted) return;
  TimePoint_IS my_tp;
  isc->value(my_tp);
  if (my_tp.XLabels[0].compare("xxx")==0 || my_tp.YLabels[0].compare("yyy")==0) return;
  // here get the name of the variable
  // strip the name of the IS Server away
  std::string time_point_name = isc->name();
  size_t pos1;
  pos1 = time_point_name.find(".");
  std::string s_name = time_point_name.substr(pos1+1);
  std::map<std::string, bool>::iterator it_map=m_is_first_event_timepoint.find(s_name);
  if(it_map==m_is_first_event_timepoint.end()){
    m_is_first_event_timepoint[s_name]=true;
  }

  if(m_is_first_event_timepoint[s_name]){
    m_is_first_event_timepoint[s_name] = false;

    //int mycount = std::count_if(m_is_first_event_timepoint.begin(), 
    //                            m_is_first_event_timepoint.end(), 
    //                            [](const std::map<std::string,bool>::value_type& e) {
    //  return e.second == true;
    //});
    //if (mycount > 1) return; 
    m_is_tree_built[s_name] = false;
    int mycount = std::count_if(m_is_tree_built.begin(), 
                                m_is_tree_built.end(), 
                                [](const std::map<std::string,bool>::value_type& e) {
      return e.second == false;
    });
    if (mycount > 1) return;


    cout << "--CALLBACK:: first event " << isc->name() << ", isTreeBuilt: " << m_is_tree_built.find(s_name)->second << '\n'; // with server name
    my_tp.print_as_table(std::cout);  
    BuildTree(my_tp,s_name);
    m_num_row[s_name] = my_tp.NumRow();
  } else { 
    if(!m_is_tree_built[s_name]) return;
    FillTree(my_tp,s_name);
    // this is to check if some time point changed it structure
    if(my_tp.NumRow()!=m_num_row[s_name]){
      cout << "--Error rows differ: " << my_tp.NumRow() << " vs " << m_num_row[s_name] << '\n';
      //      ers::error(ttreedumper::Issue(ERS_HERE, "Number of row for timepoint " + boost::lexical_cast<std::string>(s_name)+ 
      //				    " has changed. Previous number was " + boost::lexical_cast<int>(m_num_row[s_name])+
      //				    ". Present number is "+  boost::lexical_cast<int>(my_tp.NumRow()) + 
      //				    " . Convene with trigger on call expert to stop and restart the archiver"  ));
      ers::error(ttreedumper::TPChange(ERS_HERE, s_name, m_num_row[s_name], my_tp.NumRow()));
      m_num_row[s_name] = my_tp.NumRow();
    }
  }

} // end of callback_tp

void TTreeDumpRC::BuildTree(TimePoint_IS my_tp, string tp_name) {
  std::cout << "in BuildTree("  << tp_name << ")" << "Last update: 5 Jun 15, 18:17" << std::endl;
  mMap[tp_name] = it_ct; // with server name
  my_tree = new TTree(tp_name.c_str(),tp_name.c_str());

  my_time_stamp_d = my_tp.TimeStamp.c_time();
  my_lumi_block = my_tp.LumiBlock;
  my_run =  my_tp.RunNumber;
  //if (tp_name.compare("L1_Rate")==0 || tp_name.compare("HLT_Rate")==0) {
  if ( tp_name.compare("L1_Rate")==0 || tp_name.compare("HLT_Rate")==0 ||
       tp_name.compare("Test_L1_Rate")==0 || tp_name.compare("Test_HLT_Rate")==0) {
    my_smk =  std::stoi( my_tp.MetaData[0] );
    my_l1k =  std::stoi( my_tp.MetaData[1] );
    my_hltk = std::stoi( my_tp.MetaData[2] );
    my_bgk =  std::stoi( my_tp.MetaData[3] );
  }

  my_tree->Branch("TimeStamp",&my_time_stamp_d,"TimeStamp/D");
  my_tree->Branch("LumiBlock",&my_lumi_block,"LumiBlock/I");
  my_tree->Branch("Run",&my_run,"Run/I");
  //if (tp_name.compare("L1_Rate")==0 || tp_name.compare("HLT_Rate")==0) {
  if ( tp_name.compare("L1_Rate")==0 || tp_name.compare("HLT_Rate")==0 ||
       tp_name.compare("Test_L1_Rate")==0 || tp_name.compare("Test_HLT_Rate")==0) {
    my_tree->Branch("SMK",&my_smk,"SMK/I");
    my_tree->Branch("L1PSK",&my_l1k,"L1PS/I");
    my_tree->Branch("HLTPSK",&my_hltk,"HLTPSK/I");
    my_tree->Branch("BGK",&my_bgk,"BGK/I");
  }
  if (tp_name.compare("HLT_Rate")==0 || tp_name.compare("Test_HLT_Rate")==0) {
    if (my_tp.MetaData.size() > 4) {
      my_int = 1;
      std::string var_name_tp = my_tp.MetaData[4];
      std::string var_name = boost::regex_replace( var_name_tp, pattern2, replaced); // "." -> "_"
      std::string var_name2;
      var_name2 = var_name+"/I";
      char *vars1 = new char[50];
      char *vars2 = new char[50];
      strcpy(vars1,var_name.c_str());
      strcpy(vars2,var_name2.c_str());
      my_tree->Branch(vars1,&my_int,vars2);
      delete [] vars1;
      delete [] vars2;
    }
  }

  unsigned int currentcell = 0;
  for ( int ix = 0; ix < my_tp.NumCol(); ++ix ){
    for ( int iy = 0; iy < my_tp.NumRow(); ++iy ){
      
      if(my_tp.XLabels[ix]=="" || my_tp.YLabels[iy]=="") {
	std::cout << "Warning: Empty Label: " << std::endl;
      }
      std::string new_Xname =   trp_utils::remove_char(my_tp.XLabels[ix],"'");
      //new_Xname = boost::regex_replace( new_Xname, pattern, replaced);   // ":" -> "_"
      //new_Xname = boost::regex_replace( new_Xname, pattern2, replaced);  // "." -> "_"
      //new_Xname = boost::regex_replace( new_Xname, pattern3, replaced);  // "-" -> "_"
      std::string var_name = GetVarName(tp_name, new_Xname, my_tp.YLabels[iy]);
      std::string var_name2;
      var_name2 = var_name+"/F";
      char *vars1 = new char[500];
      char *vars2 = new char[500];
      strcpy(vars1,var_name.c_str());
      strcpy(vars2,var_name2.c_str());
      //	int id = iy*my_tp.NumCol()+ix;
      //	int id = ix*my_tp.NumRow()+iy; // this is internal nothing to do with TimePoint.Data ordering
      if ((sizeof(*f_mydata)) == 0) { std::cout << "Warning: Empty Data " << std::endl; }
      currentcell++;
      my_tree->Branch(vars1,&f_mydata[id],vars2);
      mMap_Rates_id[vars1] = id; // questo ci vuole
      delete [] vars1;
      delete [] vars2;
      ++id;
      if(id >= MAX_VAR){
	ers::error(ttreedumper::Issue(ERS_HERE, "Too many variables increase MAX_VAR ! \n RATES WON'T BE ARCHIVED! \n INFORM EXPERT! "  ));
      }
    }
  }
  std::cout << " inserting in my_tree:" <<  id_tree << std::endl;
  my_tree_vec.push_back(my_tree); // OK
  it_ct_as.push_back(0); // OK
  m_is_first_event_tree.push_back(true); // OK
  ++id_tree; // OK
  ++it_ct;
  m_is_tree_built[tp_name] = true;
} // end of BuildTree


void TTreeDumpRC::FillTree(TimePoint_IS my_tp, string tp_name) {

  my_time_stamp_d = my_tp.TimeStamp.c_time();
  my_lumi_block = my_tp.LumiBlock;
  my_run =  my_tp.RunNumber;
  //if (tp_name.compare("L1_Rate")==0 || tp_name.compare("HLT_Rate")==0) {
  if ( tp_name.compare("L1_Rate")==0 || tp_name.compare("HLT_Rate")==0 ||
       tp_name.compare("Test_L1_Rate")==0 || tp_name.compare("Test_HLT_Rate")==0) {
    my_smk  = std::stoi( my_tp.MetaData[0] );
    my_l1k  = std::stoi( my_tp.MetaData[1] );
    my_hltk = std::stoi( my_tp.MetaData[2] );
    my_bgk  = std::stoi( my_tp.MetaData[3] );
  }
  if (tp_name.compare("HLT_Rate")==0 || tp_name.compare("Test_HLT_Rate")==0) {
    if (my_tp.MetaData.size() > 4) {
      my_int = 1;
    }
  }
    
  Int_t my_id = 0;
  for(std::vector<float>::iterator it_f=my_tp.Data.begin();
      it_f != my_tp.Data.end();
      ++it_f){
    // get the x_y string
    std::string var_x = "";
    std::string var_y = "";
    if(my_tp.get_xy(my_id, var_x, var_y)){
      std::string var_name = GetVarName(tp_name, var_x, var_y);
      Int_t id_mat = mMap_Rates_id.find(var_name)->second;
      *(f_mydata+id_mat) = *it_f;
    }
    ++my_id;
  }
  
  
  // Fill TTree
  Int_t it = mMap.find(tp_name)->second;
  
  // you need to fill only once per LB and if n+1 point is far more than diff_thresh that previous averaged points
  if(doFillTree(it,my_tp)){
    my_tree_vec[it]->Fill();
  }
  
  ++it_ct_as[it];
  if(it_ct_as[it]%t_update==0 && my_tree_vec[it]->GetEntries() > 0) {
    std::cout << "saving Tree it:" << it << std::endl;
    my_tree_vec[it]->AutoSave("SaveSelf");
  }
  
  
  if(m_is_first_event_tree[it])
    m_is_first_event_tree[it] = false;
  
  bool pip = false;
  for(unsigned int itt = 0; itt<my_tree_vec.size();++itt){
    if(m_is_first_event_tree[itt]){
      pip = true;
      break;
    }
  }
  
  if(!pip)
    m_is_first_event = false;
  
} // end of FillTree method


void TTreeDumpRC::stopROIB(const daq::rc::TransitionCmd & args){ 
  std::cout << "in stopROIB" << std::endl;
  ERS_DEBUG( 3, "stopROIB" );
  m_alarm.reset(0); 
  if ( !m_running ) return; 
  m_running = false; 

  ISCriteria *my_criteria = NULL;
  try {
    my_criteria = new ISCriteria(m_vars_name, TimePoint_IS::type(), 
				 ISCriteria::AND ); 
  } catch(daq::is::InvalidCriteria &ex) {
    ers::warning(ttreedumper::Issue(ERS_HERE, "Invalid Criteria "  
				    +boost::lexical_cast<std::string>(ex.what())
				    +boost::lexical_cast<std::string>(m_vars_name)
				    ));
  }
  std::cout << "Unsubscribing to server" << std::endl; 
  ers::info(ttreedumper::Issue(ERS_HERE, "Un-Subscribing to:" + m_vars_name ));
  try{ 
    rec->unsubscribe(m_server_name, *my_criteria);
  } catch(daq::is::RepositoryNotFound  &i_ex ) {
    ers::warning(ttreedumper::Issue(ERS_HERE, "Repository Not Found:" 
				    +boost::lexical_cast<std::string>(i_ex.what())));
  } catch(daq::is::SubscriptionNotFound  &i_ex) {
    ers::warning(ttreedumper::Issue(ERS_HERE, "Subscritpion Not Found:" 
				    +boost::lexical_cast<std::string>(i_ex.what())));
  } 
  
  std::cout << "End of stopROIB" << std::endl;

}


void TTreeDumpRC::stopArchiving(const daq::rc::TransitionCmd & args){ 
  std::cout << "in stopArchiving" << std::endl;
  std::cout << "IRH file size is " << my_file->GetSize() << std::endl;
  if (my_file->GetSize() < 0) {
    if(boost::filesystem::exists(file_name.c_str()))
      boost::filesystem::remove(file_name.c_str());
    return;
  }

  ERS_DEBUG( 3, "stopArchiving" );
  // so you should figure out where you will write the tree....
  std::cout << "Vector of tree is :" << my_tree_vec.size() << std::endl;
  for(my_tree_it=my_tree_vec.begin();
      my_tree_it!=my_tree_vec.end();
      ++my_tree_it){
    std::cout << "writing the tree " << std::endl;  
    (**my_tree_it).Print("all");
    (**my_tree_it).Write();
  }  
  std::cout << "End of writing the TTrees" << std::endl;
  
  
  // delete f_mydata array
  delete[] f_mydata;
  m_var.clear();
  // closing root file
  std::cout << "Closing Root file" << std::endl;
  my_file->Close();
  
  if(m_usecoca==1){
    std::cout << "Registering with CoCa" << std::endl;
    boost::filesystem::path temp_name(file_name.c_str()); // ABSOLUTE PATH - LONG NAME
    boost::filesystem::path final_name(file_name2.c_str()); // RELATIVE PATH - SHORT NAME
    final_name =  temp_name.parent_path()/final_name;
    std::cout << "final_name is:" << final_name << std::endl;
    std::cout << "temp_name is:" << temp_name << std::endl;
    //    std::string temp_name_str = temp_name.leaf();
    //    std::string final_name_str = final_name.leaf();
    std::string temp_name_str = temp_name.string();
    std::string final_name_str = final_name.string();
    if( ! boost::filesystem::exists(final_name)){
      std::cout << "copying " << boost::lexical_cast<std::string>(temp_name) << " into :" <<  boost::lexical_cast<std::string>(final_name) << std::endl;
      try {
	// boost::filesystem::copy_file( temp_name, final_name, boost::filesystem::copy_option::fail_if_exists );
	boost::filesystem::copy_file( temp_name, final_name);
      }
      catch (...) {
	ers::warning(ttreedumper::Issue(ERS_HERE, "Should never happen!. Problem in copying file : "
					+  boost::lexical_cast<std::string>(temp_name)
					+" to file: "
					+ boost::lexical_cast<std::string>(final_name)
					+ ".Destination file seems to exists already!"));
	return;
      }
      std::cout << "Registering file :" << file_name2 << std::endl;
      try {
	coca_server->registerFile(dataset, final_name_str, prio, dtc, TopDir);
      } catch( ers::Issue & ex){
	ers::warning(ttreedumper::Issue(ERS_HERE, ex.what()));
	ers::warning(ttreedumper::Issue(ERS_HERE, "Generic problem 1 in registering file: "
					+ boost::lexical_cast<std::string>(final_name)
					+ " in coca. Using extended name: " +
					boost::lexical_cast<std::string>(temp_name) ));
	try {
	  coca_server->registerFile(dataset, temp_name_str, prio, dtc, TopDir);
	} catch( ers::Issue & ex){
	  ers::warning(ttreedumper::Issue(ERS_HERE, ex.what()));
	  ers::warning(ttreedumper::Issue(ERS_HERE, "Generic problem 2 in registering long name file: "
					  + boost::lexical_cast<std::string>(temp_name)
					  + "in coca. Register it Manually"));
	  return;
	}
      }
    } else { // final_name file exists
      ers::warning(ttreedumper::Issue(ERS_HERE, "File :" + 
				      boost::lexical_cast<std::string>(final_name) + " exists already."));
      if(m_overwrite_file==1){
	ers::warning(ttreedumper::Issue(ERS_HERE, "Trying to  register in coca with full name: " +
					boost::lexical_cast<std::string>(temp_name)));
	std::cout << "Registering file :" << file_name << std::endl;
	try {
	  coca_server->registerFile(dataset, temp_name_str, prio, dtc, TopDir);
	} catch( ers::Issue & ex){
	  ers::warning(ttreedumper::Issue(ERS_HERE, "Problem 3 in registering file: "+boost::lexical_cast<std::string>(temp_name) + " in coca. You have to do it manually!"));
	  ers::warning(ttreedumper::Issue(ERS_HERE, ex.what()));
	  return;
	}
      } else { // overwrite file
	ers::warning(ttreedumper::Issue(ERS_HERE, "File: " +
					boost::lexical_cast<std::string>(temp_name) +
					"will NOT be registered in coca. Do it manually if this is required"));
	return;
      }
    }
  }
  
  std::cout << "End of stopArchiving" << std::endl;
}


std::string  TTreeDumpRC::GetVarName(std::string s_name, std::string var_x, std::string var_y) {

  std::string new_var_x = var_x;
  new_var_x = boost::regex_replace( new_var_x, pattern, replaced);   // ":" -> "_"
  new_var_x = boost::regex_replace( new_var_x, pattern2, replaced);  // "." -> "_"
  new_var_x = boost::regex_replace( new_var_x, pattern3, replaced);  // "-" -> "_"

  std::string out_string =  new_var_x+"_"+var_y;

  // disambiguation
  std::string pref = map_corr[s_name];
  if(s_name == "L1_Rate_Pred" ||  s_name == "HLT_Rate_Pred" ){
    out_string = pref + out_string;
    return(out_string);
  } 
  
  if( s_name == "HLT_Rate" || s_name == "L1_Rate" ){
    size_t found;
    found=out_string.find(pref);
    if (found!=string::npos){
      return(out_string);
    }
    if (int(found)!=0){
      out_string = pref + out_string;
    }
  }
  return(out_string);
}


bool TTreeDumpRC::doFillTree(int it_tp , TimePoint_IS my_tp){
  if(m_avg_point==0 || m_avg_point==1) {
    return(true); // returns true if no moving average is required
  }
  TimePoint_deq my_tp_deq;
  Map_TimePoint_deq_it it = my_tp_map_deq.find(it_tp);
  if(it!=my_tp_map_deq.end()){
    my_tp_deq = it->second;
  } 
  my_tp_deq.push_back(my_tp); // at least start with 1 tp in queue 
  // new item is in queue
  
  
  // now calculate the mean in a clever way
  TimePoint_IS my_temp_avg;
  Map_TimePoint_it it_avg = my_tp_mean_map.find(it_tp);
  bool found_avg = true;
  if(it_avg==my_tp_mean_map.end()){
    my_temp_avg = my_tp;
    found_avg = false;
  } else {
    my_temp_avg = it_avg->second; // this is the previous step mean. It is useful to optimize the calculation
  }
  TimePoint_IS my_tp_avg = CalcAvg(my_tp_deq, my_temp_avg); // here I calculate the average
  if(found_avg){
    my_tp_mean_map.erase(it_tp);
  }
  my_tp_mean_map[it_tp] = my_tp_avg;
  // delete the first point if size of queue exceed the depth_size
  if((int)my_tp_deq.size()>=(int)m_avg_point){
    my_tp_deq.pop_front();
  } 
  
  Map_TimePoint_deq_it it2 = my_tp_map_deq.find(it_tp);
  if(it2!=my_tp_map_deq.end()){
    my_tp_map_deq.erase(it2);
  } 
  my_tp_map_deq[it_tp] = my_tp_deq;

  if(((int)my_tp_deq.size()+1)<(int)m_avg_point){
    return(true);
  }

  
  // check if change of Lumiblock
  std::map<int, int>::iterator it_lb;
  it_lb = old_lumiblock_map.find(it_tp);
  if(it_lb==old_lumiblock_map.end()){
    old_lumiblock_map[it_tp] = my_tp.LumiBlock;
    return true;
  }
  int my_old_lb = it_lb->second;
  if(my_old_lb != my_tp.LumiBlock){
    old_lumiblock_map.erase(it_lb);
    old_lumiblock_map[it_tp] = my_tp.LumiBlock;
    return true;
  }

  bool do_fill = DiffTP(my_tp_avg, my_tp,m_thresh_diff);
  return(do_fill);
  
}
TimePoint_IS TTreeDumpRC::CalcAvg(TimePoint_deq my_tp_deq, 
			  TimePoint_IS my_tp_avg){
  int it_step = my_tp_deq.size();
#ifdef DEBUG
  std::cout << "Size of deq is:" << it_step << std::endl; 
  for(int its=0;its<it_step;++its){
    std::cout << "------------- Printing " << its << "-th component of TimePoint_deq" << std::endl;
    my_tp_deq[its].print_as_table(std::cout);
  }
#endif
  if(it_step==0){
    std::cout << "This is not possibile!" << std::endl;
    return my_tp_avg;
  } else  if(it_step==1){
    my_tp_avg = my_tp_deq[0];
    return my_tp_avg;
  } else {
    TimePoint_IS temp_avg;
    for(int its=0;its<it_step;++its){
      trp_utils::Add(temp_avg, my_tp_deq[its]);
    }
    trp_utils::Scale(temp_avg, 1/(float)it_step);
    return temp_avg;
  }
  std::cout << "pippo3.2 SHOULD NEVER BE PRINTED" << std::endl;
  return my_tp_avg;
}

bool TTreeDumpRC::DiffTP(TimePoint_IS tp_mean, TimePoint_IS my_tp, 
			 float diff_thresh){

  std::vector<float>::iterator it_mean;
  std::vector<float> vec_mean = tp_mean.Data;
  std::vector<float> vec_tp = my_tp.Data;
    
  int it_vec = 0;
  for(it_mean=vec_mean.begin(); it_mean!=vec_mean.end();++it_mean,++it_vec){
    if((*it_mean)==0 && vec_tp[it_vec]==0) continue;
    float ratio = fabs((*it_mean)-vec_tp[it_vec])/(fabs((*it_mean)+vec_tp[it_vec]));
    if(ratio>diff_thresh) return true; // --> difference is important so you have to write into TTree
  }
  return false; // you dont need to write into the TTree
}
