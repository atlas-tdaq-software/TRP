/** 
 * @file TTreeDumperMain.cpp
 * @author Antonio Sidoti Humboldt Universitat zu Berlin
 * @date 02/12/2008
 * @brief Reads info from IS and dump it in a TTree. Check that there are 
 * no duplicates and automaticcally configure the TTree variables accoirding 
 * to XLabels and YLabels
 */

#include <iostream>

#include "ers/ers.h"
#include "TRP/Args.h"


#include "TRP/ReadXML.h"
#include "TRP/Utils.h"

#include "TTreeDumpRC.h"
//#include "RunController/ItemCtrl.h"
//#include "RunController/Exceptions.h"
#include "RunControl/RunControl.h"
#include "TRP/Utils.h"
#include <fstream>


#undef TRP_DEBUG
#define TRP_DEBUG(lvl,msg) //cout << lvl << " ** " << msg << endl

#define DBG(msg) //std::cerr << msg << endl;

std::string partition_name;
std::string server_name;
std::string vars_name;
std::string file_suff ="pippo";
bool dump_all = false;
int update_interval = 300;
bool interactiveMode;
//std::string app_name = "TTreeDumper";
std::string parentName;
std::string segmentName;
std::string controllerName;
std::string coca_server_name;
std::string coca_partition = "initial"; 
std::string coca_dataset = "TRP-Rates"; 
std::string coca_top_dir;
int average_pnt = 5;
int sleep_time = 5;
std::string sleep_time_arg = "0";
float thresh_diff = 0.1; // 10 %
int usecoca_int = 1;
int overwrite_file = 1;
void ReadConfig(std::string  config_file){
  std::cout << "ReadConfig(" << config_file << " ) " << std::endl;
  
  ReadXML my_reader;
  my_reader.SetElement("treedumper");
  std::vector<std::string> my_str;
  my_str.push_back("block_name");
  my_str.push_back("filesuff");
  my_str.push_back("update");
  my_str.push_back("CoCaPartition");
  my_str.push_back("CoCaServerName");
  my_str.push_back("CoCaTopDir");
  my_str.push_back("CoCaDataset");
  my_str.push_back("ThreshDiff");
  my_str.push_back("AvgPoints");
  my_str.push_back("UseCoca");
  my_str.push_back("OverWriteFile");
  my_str.push_back("SleepTime");
  my_reader.SetAttribute(my_str);
  my_reader.readConfigFile(config_file);
  
  //  partition_name = my_reader.GetValC("partition");
  server_name    = my_reader.GetValC("server");
  vars_name      = my_reader.GetVal("block_name");
  file_suff      = my_reader.GetVal("filesuff");
  update_interval= trp_utils::convertToInt(my_reader.GetVal("update"));
  coca_partition = my_reader.GetVal("CoCaPartition");
  coca_server_name = my_reader.GetVal("CoCaServerName");
  coca_top_dir = my_reader.GetVal("CoCaTopDir");
  coca_dataset = my_reader.GetVal("CoCaDataset");
  thresh_diff = (float)trp_utils::convertToDouble(my_reader.GetVal("ThreshDiff"));
  average_pnt  = trp_utils::convertToInt(my_reader.GetVal("AvgPoints"));
  usecoca_int  = trp_utils::convertToInt(my_reader.GetVal("UseCoca"));
  overwrite_file = trp_utils::convertToInt(my_reader.GetVal("OverWriteFile"));
  if (sleep_time_arg.compare("0") != 0) {
    sleep_time = trp_utils::convertToInt(sleep_time_arg);
    std::cout << "Set parameter from command line: SleepTime = " << sleep_time << std::endl;
  } else {
    sleep_time = trp_utils::convertToInt(my_reader.GetVal("SleepTime"));
  }
}

void showHelp( std::ostream& os, std::string pgmName ){
  os << "Usage: " << pgmName << " [-i] -P <str> -n <str> -s <str>[-p <str>]  [-S <str>] [-a <str1:str2:...>] [-A]   " << std::endl;
  os << "                                                                      " << std::endl;
  os << "Arguments:                                                    " << std::endl;
  os << "  -i switch-on interactive mode " << std::endl; 
  os << "  -P name of the parent " << std::endl;
  os << "  -s Name of segment " << std::endl;
  os << "  -n Name of Controller " << std::endl; 
  os << "  -p <str>   partition name  (if not present $TDAQ_PARTITION is used) " << std::endl; 
   os << "  -c <file1.xml:file2.xml:...>   reads info from xml file instead. Picks info from existing file" << std::endl;
  os << "                                                                      " << std::endl;
  os << "  or in alternative from the command line as follows     " << std::endl;
  os << "  -S <str>   server name  (if not present $TDAQ_ISSERVER is used) " << std::endl;
  os << "-a <str1:str2:...> variable name separated by : (if not present $TDAQ_ISINFOVAR is used)" << std::endl;
  os << " if -A option is defined then all the info from server will be dumped" << std::endl;
  os << "Description:                                                          " << std::endl;
  os << "   The TTreeDumper gets infos  from IS             " << std::endl;
  os << "   and sotres it in TTree of text file.          " << std::endl; 
  os << "   callculatiom and store in IS TimePoint variable with               " << std::endl; 
  os << "Debug level and verbosity:                                            " << std::endl;
  os << "   They are controlled via the TDAQ_ERS shell variables:              " << std::endl; 
  os << "     - $TDAQ_TRP_DEBUG_LEVEL        [0-4]                             " << std::endl; 
  os << "     - $TDAQ_TRP_VERBOSITY_LEVEL    [0-3]                             " << std::endl;
  os << "     - $TDAQ_TRP_FATAL              [eg: 'lsterr,throw']              " << std::endl;   
  os << "     - $TDAQ_TRP_ERROR                                                " << std::endl;
  os << "     - $TDAQ_TRP_WARNING                                              " << std::endl;
  os << "     - $TDAQ_TRP_INFO               [eg: 'filter(efd),lstdout']       " << std::endl;
  os << "     - $TDAQ_TRP_DEBUG                                                " << std::endl;
  exit(1);
}


int main( int argc, char* argv[] ){

  bool IPCFailed=false;
  try{
    
    // get arguments
    Args args( argc, argv );
    
    // get program name
    std::string pgmName = argv[0];
    std::string::size_type pos = pgmName.rfind( '/' );
    if( pos != std::string::npos )
      pgmName = pgmName.substr( pos+1 );
    
    // check if requested help
    if( args.hasArg("-h")     || 
        args.hasArg("-?")     || 
        args.hasArg("--help") || 
        argc <= 1  )
      //           || 
      //	!args.hasArg("-n"))
      { showHelp( std::cerr, pgmName ); }
    
    // reconstruct command line string for debugging feedback
    std::stringstream cmdLine;
    cmdLine << argv[0];
    for( int i = 1; i < argc; ++i )
      cmdLine << " " << argv[i];
    std::cout << "cmdline: '" << cmdLine.str() << "'" << std::endl;
    if (args.hasArg("-i")) {
      interactiveMode = true;
    }
    if (args.hasArg("-P")) {
      parentName = args.getStr("-P");
      ERS_DEBUG(0, "ParentName from  command line: '" << parentName  << "'");
      std::cout<< "ParentName from  command line: '" << parentName  << std::endl;
    }
    if (args.hasArg("-s")) {
      segmentName = args.getStr("-s");
      ERS_DEBUG(0, "SegmentName from  command line: '" << segmentName  << "'");
      std::cout<< "SegmentName from  command line: '" << segmentName  << std::endl;
    }
    if (args.hasArg("-n")) {
      controllerName = args.getStr("-n");
      ERS_DEBUG(0, "ControllerName from  command line: '" << controllerName  << "'");
      std::cout<< "ControllerName from  command line: '" << controllerName  << std::endl;
    }
    // Retrieve sleep time option
    if (args.hasArg("-t")) {  // at start
      sleep_time_arg = args.getStr("-t");
      ERS_DEBUG(0, "SleepTime from  command line at start: '" << sleep_time_arg  << "'");
      std::cout<< "SleepTime from  command line at start: '" << sleep_time_arg  << "'" << std::endl;
    }
    if (args.hasArg("-T")) {  // at re-start
      sleep_time_arg = args.getStr("-T");
      ERS_DEBUG(0, "SleepTime from  command line at re-start: '" << sleep_time_arg  << "'");
      std::cout<< "SleepTime from  command line at re-start: '" << sleep_time_arg  << "'" << std::endl;
    }

    // Retrive the partition name (from command line or from environment) 
    if (args.hasArg("-p")) {
      partition_name = args.getStr("-p");
      TRP_DEBUG(0, "Retrieved partition name from command line: '" << partition_name << "'");
      std::cout << "Retrieved partition name from command line: " << partition_name << std::endl;
    } else if (getenv("TDAQ_PARTITION")) {
      partition_name = getenv("TDAQ_PARTITION");
      TRP_DEBUG(0, "Retrieved partition name from environment: '" << partition_name << "'");
    } else {
      std::cerr << "Partition name not set (neither in command line nor in environment)" << std::endl;
      showHelp( std::cerr, pgmName );
    }

    std::string config_file;
    if (args.hasArg("-c")) {
      config_file  = args.getStr("-c");
      ERS_DEBUG(0, "Config file from  command line: '" << config_file  << "'");
      std::cout<< "Config file from  command line: " << config_file  << std::endl;
      std::vector<std::string> file_list; 
      trp_utils::tokenize(config_file,file_list,":");
      std::cout << "File_list size is:" << file_list.size() << std::endl;
      for(std::vector<std::string>::iterator it=file_list.begin();
	  it!=file_list.end();++it){
	const char* it_cstr = it->c_str();
	std::ifstream my_file(it_cstr);
	if(my_file.is_open()){
	  // here reads the file .xml
	  ReadConfig(*it);
	  break;
	}
      }
    } else {
      // Retrive the IS Server name partition (from command line or from environment) 
      if (args.hasArg("-S")) {
	server_name = args.getStr("-S");
	TRP_DEBUG(0, "Retrieved IS Server name from command line: '" << server_name << "'");
      } else if (getenv("TDAQ_ISSERVER")) {
	server_name = getenv("TDAQ_ISSERVER");
	TRP_DEBUG(0, "Retrieved IS Server name from environment: '" << server_name << "'");
      } else {
	std::cerr << "IS Server name not set (neither in command line nor in environment)" << std::endl;
	showHelp( std::cerr, pgmName );
      }
      dump_all = false;
      if (args.hasArg("-A")) {
	TRP_DEBUG(0, "Dumping all contents from IS Server ");
	dump_all = true;
      } else {
	if (args.hasArg("-a")) {
	  vars_name = args.getStr("-a");
	  TRP_DEBUG(0, "Retrieved ISInfo variables to store  from command line: '" << server_name << "'");
	} else if (getenv("TDAQ_ISINFOVAR")) {
	  vars_name = getenv("TDAQ_ISINFOVAR");
	  TRP_DEBUG(0, "Retrieved IS Variables names from environment: '" << server_name << "'");
	} else {
	  std::cerr << "IS name not set (neither in command line nor in environment)" << std::endl;
	  showHelp( std::cerr, pgmName );
	}
      }
    } // end else read config
    
    try{
      IPCCore::init(argc,argv);
    }catch(std::exception &ex){
      IPCFailed=true;
    } 
    /*
      Create an instance of the TTreeDump implementation
    */

    //    daq::rc::Controllable * my_TreeDump = new TTreeDumpRC(app_name);
    //TTreeDumpRC * my_TreeDump = new TTreeDumpRC(controllerName);
//    const std::shared_ptr<daq::rc::Controllable>& my_TreeDump = std::shared_ptr<daq::rc::Controllable>(new TTreeDumpRC(controllerName));
    const std::shared_ptr<TTreeDumpRC>& my_TreeDump = std::make_shared<TTreeDumpRC>(controllerName);
    /*
                                        file_suff, 
                                        vars_name,
                                        update_interval,
                                        coca_server_name,
                                        coca_partition,
                                        coca_dataset,
                                        coca_top_dir,
                                        thresh_diff,
                                        average_pnt,
                                        usecoca_int,
                                        overwrite_file,
                                        sleep_time);*/
    
    // TTreeDumpRC::TTreeDumpRC my_TreeDump(app_name);
       // my_TreeDump->SetOptions(partition_name, server_name, vars_name,
       // 			    file_suff, update_interval);
    
   
    my_TreeDump->SetOptions(partition_name, server_name, file_suff, vars_name,
			    update_interval, coca_server_name, coca_partition, 
			    coca_dataset, coca_top_dir, thresh_diff, average_pnt,usecoca_int,
			    overwrite_file, sleep_time);
    // daq::rc::ItemCtrl * myItem = new daq::rc::ItemCtrl(my_TreeDump, interactiveMode, 
    // 				       parentName);
    daq::rc::CmdLineParser cmdParser(argc, argv, true);
    daq::rc::ItemCtrl myItem(cmdParser,
			     my_TreeDump,
			     std::shared_ptr<daq::rc::ControllableDispatcher>(new daq::rc::DefaultDispatcher()));
    // daq::rc::ItemCtrl * myItem = new daq::rc::ItemCtrl(parentName, //name
    //                                                    parentName, //parent
    //                                                    parentName, //segment
    //                                                    partition_name, // partition
    //                                                    my_TreeDump, //controllable
    //                                                    interactiveMode); // isInteractive         
    myItem.init();
    myItem.run();
    
    //delete myItem;
    
    // ERS_DEBUG(0,"Exiting from " << my_TreeDump->getName().c_str() << ".");
    std::exit (EXIT_SUCCESS);
    // end of method
    
  } catch(ers::Issue& ex) {
    if(!IPCFailed)ers::fatal(ex);
    return EXIT_FAILURE;
  }catch(std::exception& ex) {
    //ers::error(daq::rc::CmdlParsing(ERS_HERE, "", ex));
    std::cerr<<"Caught exception "<<ex.what()<<std::endl;
    return EXIT_FAILURE;
  }
}
