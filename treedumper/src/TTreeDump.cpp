/** 
 * @file treedumper.cpp
 * @author Antonio Sidoti Humboldt Universitat zu Berlin
 * @date 02/12/2008
 * @brief Dummy data reader published in  IS.
 */
#include "TTreeDump.h"
#include <iostream>
#include <ipc/core.h>

#include "is/infoany.h"
#include <is/infoT.h>
#include <is/info.h>
#include <is/infodictionary.h>
#include <is/infoiterator.h>
#include <is/inforeceiver.h>
#include <is/serveriterator.h>
#include <ipc/partition.h>
#include "is/infodocument.h"

//#include "MyInfo.h"

#include "TRP/TimePoint_IS.h" 

#include "TRP/Utils.h"


using namespace std;


TTreeDump::TTreeDump(std::string partition_name,std::string server_name, 
		     std::string vars_name, bool dump_all, 
		     std::string file_name,int update_interval){
  m_partition_name = partition_name;
  m_server_name = server_name;
  for (unsigned int i = 0; i < vars_name.size(); ++i) {
    m_var = trp_utils::split(vars_name,":");
  }
  std::string name2_str = "";
  for(std::vector<std::string>::iterator it=m_var.begin();
      it!=m_var.end();++it){
    name2_str += (*it)+"_";
  }



  const char *file_name_c;
  file_name += name2_str;
  file_name += ".root";
  file_name_c = file_name.c_str();
  cout << "creating root file: " << file_name_c << endl;
  my_file = new TFile(file_name_c,"recreate");
  
  cout << "Configuring TTreeDump to:" << endl;
  cout << "partition_name = " << m_partition_name << endl;
  cout << "Server name" << m_server_name << endl;
  for(m_var_it = m_var.begin();m_var_it != m_var.end();++m_var_it){
    cout << "var:" << *m_var_it << endl;
  }
  cout << "dump_all:" << dump_all << endl;
  cout << "file_name" << file_name << endl;
  my_time_stamp_d =0.;
  my_lumi_block=0;
  my_run=0;
  empty_server = false;
  t_update = update_interval;
  prev_t =   time(NULL);
  my_file->ls();
  m_is_first_event = true;
  //  my_tp_vec_new.reserve(10);
  
  std::cout << "End of constructor" << std::endl;
}
TTreeDump::TTreeDump(const TTreeDump& )
{
	// implemented only to forbid copying

}

TTreeDump::~TTreeDump() 
{
  ERS_DEBUG( 3, "TTreeDump::~TTreeDump()" );
}

void TTreeDump::Configure(){
  ERS_DEBUG( 3, "Start Configuring" );
  std::cout << "TTree Dump Configuring" <<  std::endl;
  it_ct_as = 0;
  mPartition = 0;
  if (!mPartition) mPartition = new IPCPartition( m_partition_name );
  
  ISInfoDictionary  dict(*mPartition);
  // read only one entry to get the structure and headers for ntuples
  
  TimePoint_IS my_time_point;

  // first you have to understand the dimension of f_mydata
  int nvar=0;
  for(m_var_it = m_var.begin(); m_var_it!=m_var.end();++m_var_it){
    std::cout << "m_var=" << (*m_var_it) << std::endl; 
    std::string time_point_name = m_server_name+"."+(*m_var_it);
    std::cout << "reading " << time_point_name << std::endl;
    try {
      dict.getValue(time_point_name,my_time_point);

    } catch (...){
      empty_server = true;
      cout << "empty server " << endl;
      return;

    }
    for ( int iy = 0; iy < my_time_point.NumRow(); ++iy ){
      for ( int ix = 0; ix < my_time_point.NumCol(); ++ix ){
	if(my_time_point.XLabels[ix]=="" || my_time_point.YLabels[iy]==""){
	  std::cout << "WARNING empty label" << std::endl;
	}
	++nvar;
      }
    }
  }
  std::cout << " Number of variables is: " << nvar <<std::endl;
  f_mydata = new float[nvar];

  // now you can build you tree
  int it_ct = 0;
  for(m_var_it = m_var.begin(); m_var_it!=m_var.end();++m_var_it,++it_ct){
    
    std::cout << "accessing var:" << (*m_var_it) << std::endl; 
    
    std::string time_point_name = m_server_name+"."+(*m_var_it);
    try {
      dict.getValue(time_point_name,my_time_point);
    }  catch (...){
      empty_server = true;
      cout << "empty server 2" << endl;
      return;
    }
    std::cout << "timepoint name is:" << time_point_name << std::endl;
    
    //   mMap.insert( pair<std::string, int> (time_point_name,it_ct));
    mMap[time_point_name] = it_ct;
    my_tree = new TTree(m_var_it->c_str(),m_var_it->c_str());
    
    my_tree->Branch("TimeStamp",&my_time_stamp_d,"TimeStamp/D");
    my_tree->Branch("LumiBlock",&my_lumi_block,"LumiBlock/I");
    my_tree->Branch("Run",&my_run,"Run/I");
    // cambiamo a causa della mignotta
    for ( int ix = 0; ix < my_time_point.NumCol(); ++ix ){
      for ( int iy = 0; iy < my_time_point.NumRow(); ++iy ){
      
	if(my_time_point.XLabels[ix]=="" || my_time_point.YLabels[iy]=="") {
	  std::cout << "Warning: Empty Label: " << std::endl;
	}
	std::cout << "***** iy " << iy << "   ix" << ix << std::endl;
	std::cout << "XLabel: " << my_time_point.XLabels[ix] << "  YLabel: " 
		  <<  my_time_point.YLabels[iy] << std::endl;
	std::string new_Xname =   trp_utils::remove_char(my_time_point.XLabels[ix],"'");
	std::string var_name;
	var_name = new_Xname+"_"+my_time_point.YLabels[iy];
	std::string var_name2;
	var_name2 = var_name+"/F";
	char *vars1 = new char[100];
	char *vars2 = new char[100];
	strcpy(vars1,var_name.c_str());
	strcpy(vars2,var_name2.c_str());
	// cambiamo a causa della mignotta
	//	int id = iy*my_time_point.NumCol()+ix;
	int id = ix*my_time_point.NumRow()+iy;
	my_tree->Branch(vars1,&f_mydata[id],vars2);
	std::cout << "****  my_tree->Branch(" <<  vars1 << "   " << vars2 
		  << ")  " << std::endl;
	delete [] vars1;
	delete [] vars2;
      }
    }
    my_tree_vec.push_back(my_tree);
  }
  empty_server = false;
  ERS_DEBUG( 3, "End Configuring" );
}

void TTreeDump::Run(){
  ERS_DEBUG( 3, "Start Running" );
  
  // READ all the info present in the IS Server

  if (!mPartition) mPartition = new IPCPartition( m_partition_name );
  
  ISInfoDictionary  dict(*mPartition);

  while(empty_server){ // do a loop with 5 minutes of delay until you find a server
    sleep(10);
    Configure();
  }

  

  // loop on all the branches
  if(!empty_server){
    int it_var = 0;
    for(m_var_it = m_var.begin(); m_var_it!=m_var.end();++m_var_it){ // loop on branches
      std::string time_point_name = m_server_name+"."+(*m_var_it);
      std::string var_name = (*m_var_it);
      std::vector<TimePoint_IS> my_tp_vec;
      std::cout << "reading server name:" << m_server_name << std::endl;
      
      ISInfoIterator ii( *mPartition, m_server_name, TimePoint_IS::type() && 
			 var_name  );
      while ( ii() ){
	my_tp_vec_new.clear();

	// dict.getValue(time_point_name,my_tp_vec);
	
	dict.getValues( ii.name(), my_tp_vec_new, -1 );
	my_tp_vec.insert( my_tp_vec.end(), my_tp_vec_new.begin(), my_tp_vec_new.end() );
      }
      //  std::sort( my_tp_vec.begin(), my_tp_vec.end() );	
      ERS_DEBUG( 3, "Got " << my_tp_vec.size() << "  entries from:" << time_point_name );
      std::cout << "Got " << my_tp_vec.size() << "  entries from:" << time_point_name << std::endl;
      // now store in ntuple
      // store time stamp of last TimePoint_IS
      if(my_tp_vec.size()>0){
	my_last_time = -9999;
	std::vector<TimePoint_IS>::iterator it_tp_vec;
	for(it_tp_vec = my_tp_vec.begin(); it_tp_vec != my_tp_vec.end(); 
	    ++it_tp_vec){
	  my_time_stamp_d = it_tp_vec->TimeStamp.c_time();
	  if(my_last_time<=my_time_stamp_d){
	    my_last_time = my_time_stamp_d;
	  }
	  
	  my_lumi_block = it_tp_vec->LumiBlock;
	  my_run =  it_tp_vec->RunNumber;
	  std::vector<float> my_vec_data = it_tp_vec->Data;
	  if(m_is_first_event)
	    std::cout << "size Data" << my_vec_data.size() << std::endl;
	  int id = 0;
	  std::vector<float>::iterator it_f;
	  for(it_f=my_vec_data.begin();
	      it_f != my_vec_data.end();
	      ++it_f,++id){
	    *(f_mydata+id) = *it_f;
	    //	  std::cout << "id = " << id <<  " itf:" << (*it_f) << std::endl;
	  }
	  // Fill TTree
	  if(m_is_first_event){
	    std::cout << "Filling Info with lumiblock: " << my_lumi_block << std::endl;
	    std::cout << "it_var:" << it_var << "  m_var_it" << *m_var_it << std::endl;
	  }
	  my_tree_vec[it_var]->Fill();
	  if(m_is_first_event)
	    m_is_first_event=false;
	}
#ifdef DEBUG_TD
	std::cout << "Autosave previous TP  var:" << it_var << std::endl;
#endif
	my_tree_vec[it_var]->AutoSave("SaveSelf");
#ifdef DEBUG_TD
	std::cout << "$$$$$$$$$$$$$$$$" << std::endl;
#endif
      }
      ++it_var;
    }

  
    m_is_first_event=true;
    // now subscribe to IS
    ISInfoReceiver rec(*mPartition);
    std::cout << "Subscribing to IS" << std::endl;
    // devi sottoscrivere ad una lista di servers
    for(m_var_it = m_var.begin(); m_var_it!=m_var.end();++m_var_it){
      ISCriteria criteria( m_var_it->c_str());
      try {
	rec.subscribe(m_server_name, criteria, &TTreeDump::callback_tp, this);
      } catch(...){
	std::cout << "Server " << m_server_name << "  not found " << std::endl;
	std::cout << " Should Never be there! " << std::endl;
	std::cerr << "Server " << m_server_name << "  not found " << std::endl;
      }
    }
    //rec.run();
    rec.unsubscribe(m_server_name, ".*" );
    // normally these are never called....
    // so you should figure out whi will write the tree....
    for(my_tree_it=my_tree_vec.begin();
	my_tree_it!=my_tree_vec.end();
	++my_tree_it){
      (**my_tree_it).Write();
    }
    my_file->Close();
    ERS_DEBUG( 3, "End of Running" );
  } // end of if not empty server
}
  
void TTreeDump::callback_tp(ISCallbackInfo *isc) {
  if(isc->reason() == ISInfo::Deleted) return;
  if(m_is_first_event){
    cout << "--CALLBACK:: " << isc->name() << '\n';
    cout << "--Reason code: " << isc->reason() << '\n';
  }
  TimePoint_IS my_tp;
  isc->value(my_tp);
  my_time_stamp_d = my_tp.TimeStamp.c_time();
  if(my_last_time<=my_time_stamp_d){
    if(m_is_first_event)
      std::cout << "New data" << std::endl;
    my_lumi_block = my_tp.LumiBlock;
    my_run =  my_tp.RunNumber;
    int id = 0;
    for(std::vector<float>::iterator it_f=my_tp.Data.begin();
	it_f != my_tp.Data.end();
	++it_f){
      *(f_mydata+id) = *it_f;
      ++id;
    }
    if(m_is_first_event)
      std::cout << "filling lumiblock:" << my_lumi_block << std::endl;
    // Fill TTree
    // anche qui devi guardarci....
    int it = mMap.find(isc->name())->second;
    my_tree_vec[it]->Fill();
    ++it_ct_as;
    if(it_ct_as%t_update==0) {
      std::cout << "saving Tree" << std::endl;
      my_tree->AutoSave("SaveSelf");
    }
  }
  if(m_is_first_event)
    m_is_first_event = false;
}  
