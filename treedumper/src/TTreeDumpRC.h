/** 
 * @file TTreeDumpRC.h
 * @author Antonio Sidoti
 * @date 28/11/2008
 * @brief Class that dumps in a text file or root file 
 * @ some variables found in IS servers
 * @  It's a run controllable application now
 */
 
#ifndef ttreedumpRC_h
#define ttreedumpRC_h

#include <iostream>

#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>
#include <TBrowser.h>

#include <ctime>

#include <vector>
#include <deque>
#include <utility>
#include <algorithm>
#include <string>

#include <ipc/core.h>
#include <ipc/partition.h>

#include "unistd.h"
#include "stdio.h"
#include "stdlib.h"
#include "signal.h"


#include "is/infoany.h"
#include <is/infodictionary.h>
#include <is/infoT.h>
#include <is/info.h>
#include <is/infoiterator.h>
#include <is/inforeceiver.h>
#include <is/serveriterator.h>
#include "is/infodocument.h"
#include <ipc/alarm.h>

#include "TRP/TimePoint_IS.h" 

//#include <criteria.h>

//#include "RunController/Controllable.h"
#include "RunControl/RunControl.h"
#include "ers/ers.h"
#include <coca/coca.h>

//#define MAX_VAR 60000
//#define MAX_NUM_TREE 100


using std::string;
using std::vector;
using std::deque;
using std::pair;
using std::cout;
using std::cerr;
using std::endl;



/**
 * @brief TTreeDumpRC reads LVL1 rates and publish them in trpIS.
 */
class TTreeDumpRC: public daq::rc::Controllable {
  
 public:

  TTreeDumpRC(std::string name);
  /* TTreeDumpRC(std::string file_suff, */
  /*             std::string vars_name, */
  /*             int update_interval, */
  /*             std::string coca_server_name, */
  /*             std::string coca_partition, */
  /*             ); */

  virtual ~TTreeDumpRC() noexcept {}  

  
  void SetOptions(std::string partition_name, std::string server_name, 
		  std::string file_suff, std::string vars_name, 
		  int update_interval, std::string coca_server_name, 
		  std::string coca_partition, std::string coca_dataset, 
		  std::string coca_top_dir, float thresh_diff, int avg_pnt=0,
		  int usecoca=1, int overwrite_file=1, int sleep_time=5);	    // OK 
 
  // Virtual methods for RC application
  virtual void configure(const daq::rc::TransitionCmd & args)override;

  virtual void connect(const daq::rc::TransitionCmd & args)override;

  // virtual void prepareForRun(std::string & args) { sleep(5);ERS_DEBUG(1,"Got prepareForRun with arguments" << args);}
  //  virtual void startTrigger(std::string & args);
  //  virtual void stopTrigger(std::string & args);
  virtual void prepareForRun(const daq::rc::TransitionCmd & args)override;
  virtual void stopROIB(const daq::rc::TransitionCmd & args)override;
  virtual void stopArchiving(const daq::rc::TransitionCmd & args)override;
//  virtual void disconnect(std::string & args);
  //  virtual void unconfigure(std::string & args)   { sleep(5);ERS_DEBUG(1,"Got unconfigure with arguments" << args);}
  // virtual void userCommand(std::string & cmd, std::string & args) {ERS_DEBUG(0,"Got user command " << cmd.c_str() << " with arguments " << args.c_str() << "." );}

  void CreateFile();
  void MyConfigure(); // OK
  void MyRun(); // OK

  
  
 private:
  bool processPrepareForRun();
  void BuildTree(TimePoint_IS my_tp, string tp_name);
  void FillTree(TimePoint_IS my_tp, string tp_name);
  bool first_evt;
  bool empty_server;
  time_t prev_t;
  int t_update;
  int m_avg_point;
  float m_thresh_diff;
  int m_overwrite_file;
  std::string m_partition_name;
  std::string m_server_name;
  std::string m_file_suff;
  std::string file_name;
  std::string file_name2;
  std::string m_vars_name;
  vector<std::string> m_var;
  vector<std::string>::iterator m_var_it;
  std::vector<ISCriteria> m_crit_vec;
  std::map<std::string, bool> m_is_first_event_timepoint;
  std::map<std::string, bool> m_is_tree_built;
  bool m_root_file;
  TFile *my_file;
  TTree *my_tree;
  Int_t it_ct;
  Int_t id;
  Int_t id_tree;
  std::vector<TTree*> my_tree_vec;
  std::vector<TTree*>::iterator my_tree_it; 
  std::map<std::string, Int_t> mMap;
  std::map<std::string, Int_t> mMap_Rates_id;
  std::vector<TimePoint_IS> my_tp_vec_new;
  std::map<int, TimePoint_IS> my_tp_mean_map;
  std::map<int, int> old_lumiblock_map;
  typedef std::map<int, TimePoint_IS>::iterator Map_TimePoint_it;
  
  typedef std::deque<TimePoint_IS> TimePoint_deq;
  typedef std::map<int, TimePoint_deq> Map_TimePoint_deq;
  typedef Map_TimePoint_deq::iterator Map_TimePoint_deq_it;
  
  std::map<std::string, int> m_num_row;
  
  Map_TimePoint_deq my_tp_map_deq;
  
  Int_t MAX_VAR;
  Int_t MAX_NUM_TREE;
  float *f_mydata;
  int my_lumi_block;
  int my_run;
  int my_smk;
  int my_l1k;
  int my_hltk;
  int my_bgk;
  int my_int;
  double  my_time_stamp_d;   
  std::vector<Int_t> it_ct_as; // counter of ttree autosave // it is a vector since you have multiple trees
  
  double my_last_time;
  
  std::string pSubscription;
  
  IPCPartition * mPartition;	// Partition object, provides connection to the IS.
  ISInfoReceiver * rec;	// Used for callback registration.
  
  
  void callback_tp(ISCallbackInfo *isc);
  std::string GetVarName(std::string s_name, std::string var_x, std::string var_y); // for branches disambiguation
  bool doFillTree(int it_tp , TimePoint_IS my_tp);
  TimePoint_IS CalcAvg(TimePoint_deq my_tp_deq, 
	       TimePoint_IS my_tp_avg);
  bool DiffTP(TimePoint_IS tp_mean, TimePoint_IS my_tp, 
	      float diff_thresh);
  
  bool m_is_first_event;
  std::vector<bool> m_is_first_event_tree;
  int m_usecoca;
  daq::coca::Register *coca_server;
  std::string dataset;
  //  daq::coca::Types::prio_t prio;
  int prio;
  //daq::coca::Types::dtc_t dtc;
  unsigned dtc;
  std::string TopDir;
  int m_sleep_time;
  std::unique_ptr<IPCAlarm> m_alarm;
  bool m_running;
};

#endif

