/** 
 * @file TTreeDump.h
 * @author Antonio Sidoti
 * @date 28/11/2008
 * @brief Class that dumps in a text file or root file 
 * @ some variables found in IS servers
 */
 
#ifndef ttreedump_h
#define ttreedump_h

#include <iostream>

#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>
#include <TBrowser.h>

#include <ctime>

#include <vector>
#include <deque>
#include <utility>
#include <algorithm>
#include <string>

#include <ipc/core.h>
#include <ipc/partition.h>

#include "unistd.h"
#include "stdio.h"
#include "signal.h"


#include "is/infoany.h"
#include <is/infodictionary.h>
#include <is/infoT.h>
#include <is/info.h>
#include <is/infoiterator.h>
#include <is/inforeceiver.h>
#include <is/serveriterator.h>
#include "is/infodocument.h"

#include "TRP/TimePoint_IS.h" 


#include "ers/ers.h"


#define MAX_VAR 5000


using std::string;
using std::vector;
using std::deque;
using std::pair;
using std::cout;
using std::cerr;
using std::endl;



/**
 * @brief TTreeDump reads LVL1 rates and publish them in trpIS.
 */
class TTreeDump {
  
 public:
  
  void Configure();
  
  void Run();

  TTreeDump(std::string partition_name, std::string server_name, 
	    std::string vars_name, bool dump_all, std::string file_type,
	    int update_interval);	    // Way of implementing the singleton pattern.
  ~TTreeDump();	// Way of implementing the singleton pattern.
  TTreeDump( const TTreeDump & );	// Way of implementing the singleton pattern.
  
  
 private:
  
  bool first_evt;
  bool empty_server;
  time_t prev_t;
  int t_update;
  std::string m_partition_name;
  std::string m_server_name;
  bool m_dump_all;
  vector<std::string> m_var;
  vector<std::string>::iterator m_var_it;

  std::string m_file_name_out;
  bool m_root_file;
  TFile *my_file;
  TTree *my_tree;
  std::vector<TTree*> my_tree_vec;
  std::vector<TTree*>::iterator my_tree_it; 
  std::map<std::string, int> mMap;
  std::vector<TimePoint_IS> my_tp_vec_new;

  float *f_mydata;
  int my_lumi_block;
  int my_run;
  double  my_time_stamp_d;   
  int it_ct_as; // counter of ttree aoutosave
  
  double my_last_time;
  
  std::string pSubscription;
  
  IPCPartition * mPartition;	// Partition object, provides connection to the IS.
  ISInfoReceiver * mReceiver;	// Used for callback registration.
  
  
  void callback_tp(ISCallbackInfo *isc);
  bool m_is_first_event;
  
};

#endif

