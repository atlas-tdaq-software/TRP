/** 
 * @file TTreeDumperMain.cpp
 * @author Antonio Sidoti Humboldt Universitat zu Berlin
 * @date 02/12/2008
 * @brief Reads info from IS and dump it in a TTree. Check that there are 
 * no duplicates and automaticcally configure the TTree variables accoirding 
 * to XLabels and YLabels
 */

#include <iostream>

#include "ers/ers.h"
#include "TRP/Args.h"

#include "config/Configuration.h"
#include <ipc/core.h>

#include "is/infoany.h"
#include <is/infoT.h>
#include <is/infodictionary.h>
#include <is/infoiterator.h>
#include <is/inforeceiver.h>
#include <is/serveriterator.h>
#include <ipc/partition.h>
#include "is/infodocument.h"

#include "TRP/ReadXML.h"
#include "TRP/Utils.h"

#include "TTreeDump.h"
#include <fstream>

#undef TRP_DEBUG
#define TRP_DEBUG(lvl,msg) //cout << lvl << " ** " << msg << endl

#define DBG(msg) //std::cerr << msg << endl;

std::string partition_name;
std::string server_name;
std::string vars_name;
std::string file_suff ="pippo";
bool dump_all = false;
int update_interval = 300;
void ReadConfig(std::string  config_file){
  std::cout << "ReadConfig(" << config_file << " ) " << std::endl;
  
  ReadXML my_reader;
  my_reader.SetElement("treedumper");
  std::vector<std::string> my_str;
  my_str.push_back("block_name");
  my_str.push_back("filesuff");
  my_str.push_back("update");
  my_reader.SetAttribute(my_str);
  my_reader.readConfigFile(config_file);
  
  //  partition_name = my_reader.GetValC("partition");
  server_name    = my_reader.GetValC("server");
  vars_name      = my_reader.GetVal("block_name");
  file_suff      = my_reader.GetVal("filesuff");
  update_interval= trp_utils::convertToInt(my_reader.GetVal("update"));

}

void showHelp( std::ostream& os, std::string pgmName ){
  os << "Usage: " << pgmName << " [-p <str>]  [-s <str>] [-a <str1:str2:...>] [-A]   " << std::endl;
  os << "                                                                      " << std::endl;
  os << "Arguments:                                                    " << std::endl;
  os << "  -p <str>   partition name  (if not present $TDAQ_PARTITION is used) " << std::endl; 
  os << "  -c <file1.xml:file2.xml:...>   reads info from xml file instead. Picks info from existing file" << std::endl;
  os << "                                                                      " << std::endl;
  os << "  or in alternative from the command line as follows     " << std::endl;
  os << "  -s <str>   server name  (if not present $TDAQ_ISSERVER is used) " << std::endl;
  os << "-a <str1:str2:...> variable name separated by : (if not present $TDAQ_ISINFOVAR is used)" << std::endl;
  os << " if -A option is defined then all the info from server will be dumped" << std::endl;
  os << "Description:                                                          " << std::endl;
  os << "   The TTreeDumper gets infos  from IS             " << std::endl;
  os << "   and sotres it in TTree of text file.          " << std::endl; 
  os << "   callculatiom and store in IS TimePoint variable with               " << std::endl; 
  os << "Debug level and verbosity:                                            " << std::endl;
  os << "   They are controlled via the TDAQ_ERS shell variables:              " << std::endl; 
  os << "     - $TDAQ_TRP_DEBUG_LEVEL        [0-4]                             " << std::endl; 
  os << "     - $TDAQ_TRP_VERBOSITY_LEVEL    [0-3]                             " << std::endl;
  os << "     - $TDAQ_TRP_FATAL              [eg: 'lsterr,throw']              " << std::endl;   
  os << "     - $TDAQ_TRP_ERROR                                                " << std::endl;
  os << "     - $TDAQ_TRP_WARNING                                              " << std::endl;
  os << "     - $TDAQ_TRP_INFO               [eg: 'filter(efd),lstdout']       " << std::endl;
  os << "     - $TDAQ_TRP_DEBUG                                                " << std::endl;
  exit(1);
}


int main( int ac, char* av[] ){
  try{
    
    // get arguments
    Args args( ac, av );
    
    // get program name
    std::string pgmName = av[0];
    std::string::size_type pos = pgmName.rfind( '/' );
    if( pos != std::string::npos )
      pgmName = pgmName.substr( pos+1 );

    // check if requested help
    if( args.hasArg("-h")     || 
        args.hasArg("-?")     || 
        args.hasArg("--help") || 
        ac <= 1  )
//           || 
//	!args.hasArg("-n"))
      { showHelp( std::cerr, pgmName ); }
    
    // reconstruct command line string for debugging feedback
    std::stringstream cmdLine;
    cmdLine << av[0];
    for( int i = 1; i < ac; ++i )
      cmdLine << " " << av[i];
    std::cout << "cmdline: '" << cmdLine.str() << "'" << std::endl;
    // Retrive the partition name (from command line or from environment) 
    if (args.hasArg("-p")) {
      partition_name = args.getStr("-p");
      TRP_DEBUG(0, "Retrieved partition name from command line: '" << partition_name << "'");
      std::cout << "Retrieved partition name from command line: " << partition_name << std::endl;
    } else if (getenv("TDAQ_PARTITION")) {
      partition_name = getenv("TDAQ_PARTITION");
      TRP_DEBUG(0, "Retrieved partition name from environment: '" << partition_name << "'");
    } else {
      std::cerr << "Partition name not set (neither in command line nor in environment)" << std::endl;
      showHelp( std::cerr, pgmName );
    }
    std::string config_file;
    if (args.hasArg("-c")) {
      config_file  = args.getStr("-c");
      ERS_DEBUG(0, "Config file from  command line: '" << config_file  << "'");
      std::cout<< "Config file from  command line: " << config_file  << std::endl;
      std::vector<std::string> file_list; 
      //      for (unsigned int i = 0; i < config_file.size(); ++i) {
      //      file_list = trp_utils::split(config_file,"\:");
      trp_utils::tokenize(config_file,file_list,":");
      // }
      std::cout << "File_list size is:" << file_list.size() << std::endl;
      for(std::vector<std::string>::iterator it=file_list.begin();
	  it!=file_list.end();++it){
	const char* it_cstr = it->c_str();
	std::ifstream my_file(it_cstr);
	if(my_file.is_open()){
	  // here reads the file .xml
	  ReadConfig(*it);
	  break;
	}
      }
    } else {

      // Retrive the IS Server name partition (from command line or from environment) 
      if (args.hasArg("-s")) {
	server_name = args.getStr("-s");
	TRP_DEBUG(0, "Retrieved IS Server name from command line: '" << server_name << "'");
      } else if (getenv("TDAQ_ISSERVER")) {
	server_name = getenv("TDAQ_ISSERVER");
	TRP_DEBUG(0, "Retrieved IS Server name from environment: '" << server_name << "'");
      } else {
	std::cerr << "IS Server name not set (neither in command line nor in environment)" << std::endl;
	showHelp( std::cerr, pgmName );
      }
      dump_all = false;
      if (args.hasArg("-A")) {
	TRP_DEBUG(0, "Dumping all contents from IS Server ");
	dump_all = true;
      } else {
	if (args.hasArg("-a")) {
	  vars_name = args.getStr("-a");
	  TRP_DEBUG(0, "Retrieved ISInfo variables to store  from command line: '" << server_name << "'");
	} else if (getenv("TDAQ_ISINFOVAR")) {
	  vars_name = getenv("TDAQ_ISINFOVAR");
	  TRP_DEBUG(0, "Retrieved IS Variables names from environment: '" << server_name << "'");
	} else {
	  std::cerr << "IS name not set (neither in command line nor in environment)" << std::endl;
	  showHelp( std::cerr, pgmName );
	}
      }
    } // end else read config

    TRP_DEBUG(0, "Starting program job" );
    IPCCore::init( ac, av );
    TTreeDump my_isdumper(partition_name,server_name,vars_name,dump_all,file_suff,update_interval);
    my_isdumper.Configure();
    my_isdumper.Run();    
    TRP_DEBUG(0, "Program terminating gracefully" );
  }
  catch( ers::Issue &ex)   { TRP_DEBUG( CoreIssue, "Caught ers::Issue: " << ex ); }
  catch( std::exception& e) { TRP_DEBUG( CoreIssue, "Caught std::exception: " << e.what() ); }
  catch( ... )             { TRP_DEBUG( CoreIssue, "Caught unidentified exception" );
  }
  return 0;
}

  
  
