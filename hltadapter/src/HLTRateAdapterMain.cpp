#include "HistoToTimePoint.h"

#include <iostream>
#include <sstream>
#include <stdexcept>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#include "ers/ers.h"
#include "TRP/Args.h"

#include "config/Configuration.h"
#include "ipc/partition.h"
#include <ipc/core.h>
#include <is/inforeceiver.h>
#include <is/serveriterator.h>
#include "boost/smart_ptr.hpp"

#include "hltadapter/src/HLTRateAdapterSum.h"
#include "hltadapter/src/HLTRateAdapter.h"

#include "TRP/ReadXML.h"
#include "TRP/Utils.h"
#include <fstream>
#include <signal.h>
#include <owl/regexp.h>
#include "boost/regex.hpp"

using namespace std;
using namespace boost;

using namespace HltRate;
//using HltRate::HLTRateAdapterSum;
std::string partition_name;
std::string server_nameIn;
std::string server_nameOut;
std::string provider_str;
std::string histogram_str;
std::string name_out;
std::string cpu_pattern =  ".*(pc-tdq-xpu-[0-9]{4}:[0-9]{1,2}).*";
bool use_summer = true;
bool get_rate = false;
// used in summer
int int_size = 20; // seconds of interval
int num_steps = 24; // number of steps on in_size the summed rates 
                    //     are kept in memory 
                    // (so total time is int_size x num_steps e.g. 4 min)
int int_publish = 120; // start to publish after int_publish sec
int int_thresh = 10;   // min number of over/under flows 
                       // for which the full memory is reased
bool bool_correct = false;
int int_time_from_histo_option = 0;
std::string str_ready_condition = "ThirdBinFilled50";

std::string threadPer = "0";
std::string maxServerThread = "9";
std::string threadPool = "0";
std::string maxGIOP =  "7";
std::string mode =  "uncorr";
float warn_cond = 0.05;
int num_succ_warn = 5;
int multi_thread = 1;

std::list< std::pair< std::string, std::string > > options_ipc;
/*
HLTRateAdapterSum *s_HLTRateAdapterSum(0);

void My_Stop(int sig){
  std::cout << " I have received a SIG : " << sig << std::endl;
  if ( s_HLTRateAdapterSum )
    s_HLTRateAdapterSum->Stop();
  exit(0);
}
*/
namespace
{ 
  HLTRateAdapterSum* my_hlt_adapter_sum = 0;
  HLTRateAdapter*    my_hlt_adapter = 0;
  int ReceivedSignal = 0;
  void signal_handler( int sig )
  {
    if (my_hlt_adapter_sum != 0) {
      ReceivedSignal = sig;
      my_hlt_adapter_sum->signal_handler();
    }
    if (my_hlt_adapter != 0) {
      ReceivedSignal = sig;
      my_hlt_adapter->signal_handler();
    }
  }
}


void ReadConfig(std::string  config_file){
  std::cout << "ReadConfig(" << config_file << " ) " << std::endl;
  ReadXML my_reader;
  my_reader.SetElement("hltadapter");
  std::vector<std::string> my_str;
  my_str.push_back("input_is");
  my_str.push_back("provider");
  my_str.push_back("histoname");
  my_str.push_back("outputname");
  my_str.push_back("summer");
  my_str.push_back("get_rate");
  my_str.push_back("steps");
  my_str.push_back("interval");
  my_str.push_back("publish");
  my_str.push_back("thresh");
  my_str.push_back("correct");
  my_str.push_back("cpu_pattern");
  my_str.push_back("time_from_histo_option");
  my_str.push_back("ready_condition");
  my_str.push_back("mode");
  my_str.push_back("warning_cond_lost_provider");
  my_str.push_back("warning_cond_succ_lost");
  my_str.push_back("multi_thread");
  my_reader.SetAttribute(my_str);

  my_reader.readConfigFile(config_file);
  
  // partition_name = my_reader.GetValC("partition");
  server_nameOut = my_reader.GetValC("server"); //--> this is ISS_TRP
  
  server_nameIn = my_reader.GetVal("input_is"); //-> "Histogramming-EF-.*"
  provider_str  = my_reader.GetVal("provider");  //-> .*
  histogram_str  = my_reader.GetVal("histoname"); // -> .*Rate10s
  name_out = my_reader.GetVal("outputname");  // -> EF_Rate
  std::string summer_str = my_reader.GetVal("summer");
  use_summer = (summer_str=="1") ? true : false ;
  std::string get_rate_str = my_reader.GetVal("get_rate");
  get_rate = (get_rate_str=="1") ? true : false ;
  int_size = trp_utils::convertToInt(my_reader.GetVal("interval"));
  num_steps = trp_utils::convertToInt(my_reader.GetVal("steps"));
  int_publish = trp_utils::convertToInt(my_reader.GetVal("publish"));
  int_thresh = trp_utils::convertToInt(my_reader.GetVal("thresh"));
  std::string get_correct_str = my_reader.GetVal("correct");
  bool_correct = (get_correct_str=="1") ? true : false ;
  if ( my_reader.GetVal("time_from_histo_option") != "" )
    int_time_from_histo_option = trp_utils::convertToInt(my_reader.GetVal("time_from_histo_option"));
  if ( my_reader.GetVal("ready_condition") != "" ) 
    str_ready_condition = my_reader.GetVal("ready_condition");
  if ( my_reader.GetVal("mode") != "" )
    mode = my_reader.GetVal("mode");
  
  cpu_pattern = my_reader.GetVal("cpu_pattern"); // --> EF 
  warn_cond = trp_utils::convertToDouble(my_reader.GetVal("warning_cond_lost_provider"));
  num_succ_warn = trp_utils::convertToInt(my_reader.GetVal("warning_cond_succ_lost"));
  multi_thread = trp_utils::convertToInt(my_reader.GetVal("multi_thread"));
    // read ipc options
  ReadXML my_reader_ipc;
  my_reader_ipc.SetElement("ipcoptions");
  std::vector<std::string> my_str_ipc;
  my_str_ipc.push_back("threadPerConnectionPolicy");
  my_str_ipc.push_back("maxServerThreadPoolSize");
  my_str_ipc.push_back("threadPoolWatchConnection");
  my_str_ipc.push_back("maxGIOPConnectionPerServer");
    
  my_reader_ipc.SetAttribute(my_str_ipc);
  
  my_reader_ipc.readConfigFile(config_file);
  
  //  partition_name = my_reader.GetValC("partition");
  threadPer = my_reader_ipc.GetVal("threadPerConnectionPolicy");
  maxServerThread = my_reader_ipc.GetVal("maxServerThreadPoolSize");
  threadPool = my_reader_ipc.GetVal("threadPoolWatchConnection");
  maxGIOP = my_reader_ipc.GetVal("maxGIOPConnectionPerServer");
  
  options_ipc.push_front( std::make_pair( std::string("threadPerConnectionPolicy"),threadPer  ) );
  options_ipc.push_front( std::make_pair( std::string("maxServerThreadPoolSize"), maxServerThread ) );
  options_ipc.push_front( std::make_pair( std::string("threadPoolWatchConnection"), threadPool ) );
  options_ipc.push_front( std::make_pair( std::string("maxGIOPConnectionPerServer"), maxGIOP ) );
  


  std::cout << "End of ReadConfig" << std::endl;
}
 

void showHelp( std::ostream& os, std::string pgmName ){
  os << "Usage: " << pgmName << " [-p <str>]  [-c <file.xml:file2.xml>] [-s <str>]  [-prov <regexp>] [-hist <regexp>]  [-out <regexp>] [-c <file.xml>] " << std::endl;
  os << "                                                                      " << std::endl;
  os << "Options/Arguments:                                                    " << std::endl;
  os << "  -p <str>   partition name  (if not present $TDAQ_PARTITION is used) " << std::endl; 
  os << "  -c <file.xml>   reads info from xml file instead" << std::endl;
  os << " or in alternative " << std::endl;
  os << "  -s <str>   server Name where TrigSteerMoni publishes the HLT rate histo " << std::endl; 
  os << "  -o <str>   ServerName where this application publishes the TimePoint_IS " << std::endl; 
  os << "  -prov <regexp>   Provider  (i.e. Top-LVL2-L2-Segment-1-Gatherer )" << std::endl; 
  os << "  -hist <regexp>   Histogram (i.e. SHIFT/TrigSteer_L2/Rate10s)" << std::endl;
  os << "  -out <regexp>    name with which the TimePoint_IS will be published " << std::endl;

  os << "                                                                      " << std::endl;
  os << "Description:                                                          " << std::endl;
  os << "   The HLTRateAdapterMain gets rates (histos)  from OH                        " << std::endl;
  os << "   and store them in IS TimePoint variale                                     " << std::endl; 
  os << "Debug level and verbosity:                                            " << std::endl;
  os << "   They are controlled via the TDAQ_ERS shell variables:              " << std::endl; 
  os << "     - $TDAQ_ERS_DEBUG_LEVEL        [0-4]                             " << std::endl; 
  os << "     - $TDAQ_ERS_VERBOSITY_LEVEL    [0-3]                             " << std::endl;
  os << "     - $TDAQ_ERS_FATAL              [eg: 'lsterr,throw']              " << std::endl;   
  os << "     - $TDAQ_ERS_ERROR                                                " << std::endl;
  os << "     - $TDAQ_ERS_WARNING                                              " << std::endl;
  os << "     - $TDAQ_ERS_INFO               [eg: 'filter(efd),lstdout']       " << std::endl;
  os << "     - $TDAQ_ERS_DEBUG                                                " << std::endl;
  exit(1);
}

int main(int ac, char* av[] ){

  /*
  signal(SIGABRT, &My_Stop);
  signal(SIGTERM, &My_Stop);
  signal(SIGINT,  &My_Stop);
  */

  try{
    
    // get arguments
    Args args( ac, av );
    
    // get program name
    std::string pgmName = av[0];
    std::string::size_type pos = pgmName.rfind( '/' );
    if( pos != std::string::npos )
      pgmName = pgmName.substr( pos+1 );
    

    // check if requested help
    if( args.hasArg("-h")     || 
        args.hasArg("-?")     || 
        args.hasArg("--help") || 
        ac <= 1  )
//           || 
//	!args.hasArg("-n"))
      { showHelp( std::cerr, pgmName ); }


    // reconstruct command line string for debugging feedback
    std::stringstream cmdLine;
    cmdLine << av[0];
    for( int i = 1; i < ac; ++i )
      cmdLine << " " << av[i];
    std::cout << "cmdline: '" << cmdLine.str() << "'" << std::endl;
      // Retrive the partition name (from command line or from environment) 
    if (args.hasArg("-p")) {
      partition_name = args.getStr("-p");
      ERS_DEBUG(0, "Retrieved partition name from command line: '" << partition_name << "'");
      std::cout << "Retrieved partition name from command line: '" << partition_name << std::endl;
    } else if (getenv("TDAQ_PARTITION")) {
      partition_name = getenv("TDAQ_PARTITION");
      ERS_DEBUG(0, "Retrieved partition name from environment: '" << partition_name << "'");
      std::cout << "Retrieved partition name from environment: '" << partition_name << std::endl;
    } else {
      std::cerr << "Partition name not set (neither in command line nor in environment)" << std::endl;
      showHelp( std::cerr, pgmName );
    }

    std::string config_file;
    if (args.hasArg("-c")) {
      config_file  = args.getStr("-c");
      ERS_DEBUG(0, "Config file from  command line: '" << config_file  << "'");
      std::cout<< "Config file from  command line: '" << config_file  << std::endl;
      std::vector<std::string> file_list; 
      trp_utils::tokenize(config_file,file_list,":");
       std::cout << "File_list size is:" << file_list.size() << std::endl;
      for(std::vector<std::string>::iterator it=file_list.begin();
	  it!=file_list.end();++it){
	const char* it_cstr = it->c_str();
	std::ifstream my_file(it_cstr);
	if(my_file.is_open()){
	  // here reads the file .xml
	  ReadConfig(*it);
	  break;
	}
      }
    } else { // start of config without XML
      
      // Retreive the server name (from command line only) 
      if (args.hasArg("-s")) {
	server_nameIn = args.getStr("-s");
	ERS_DEBUG(0, "Retrieved server name from command line: '" << server_nameIn << "'");
	std::cout << "Retrieved server name from command line: '" << server_nameIn << std::endl;
      } else { 
	std::cerr << "Server name not set (in command line)" << std::endl;
	showHelp( std::cerr, pgmName );
      }
      if (args.hasArg("-o")) {
	server_nameOut = args.getStr("-o");
	ERS_DEBUG(0, "Retrieved server name Output from command line: '" << server_nameOut << "'");
	std::cout << "Retrieved server name Output from command line: '" <<  server_nameOut << std::endl;
      } else { 
	std::cerr << "Server name not set (in command line)" << std::endl;
	showHelp( std::cerr, pgmName );
      }
      if (args.hasArg("-prov")) {
	provider_str = args.getStr("-prov");
	ERS_DEBUG(0, "Retrieved provider name from command line: '" << provider_str << "'");
	std::cout << "Retrieved provider name from command line: '" << provider_str << std::endl;
      } else { 
	provider_str = "Top-LVL2-L2-Segment-1-Gatherer";
      }
      
      if (args.hasArg("-hist")) {
	histogram_str = args.getStr("-hist");
	ERS_DEBUG(0, "Retrieved Histogram  Name from command line: '" << histogram_str << "'");
	std::cout << "Retrieved Histogram  Name from command line: '" << histogram_str << std::endl;
      } else { 
	histogram_str = "SHIFT/TrigSteer_L2/Rate10s";
      }
      
      if (args.hasArg("-out")) {
	name_out = args.getStr("-out");
	ERS_DEBUG(0, "Retrieved TimePoint_IS output Name from command line: '" << name_out << "'");
	std::cout << "Retrieved TimePoint_IS output Name from command line: '" << name_out << std::endl;
      } else { 
	name_out = "HLT_Rate";
      }
    } // end else read config
    
    std::cout << "Dumping Hlt Adapter current config:" << std::endl;
    std::cout << "Partition:" << partition_name << " Input:" << server_nameIn << "  Out:" << server_nameOut << std::endl;
    std::cout << "Providers  :" <<  provider_str << "  Histo:" << histogram_str << std::endl;
    std::cout << "OutputName :" <<  name_out << std::endl;

    IPCCore::init( options_ipc );                         
    //IPCCore::init( ac, av );

  } 
  catch( daq::ipc::Exception & ex ) {
    ers::fatal( ex );
    return 1;
  }   
    
  signal(SIGABRT, signal_handler);
  signal(SIGTERM, signal_handler);
  signal(SIGINT,  signal_handler);

  try {

    ERS_DEBUG(0, "Starting program job" );
    
    if(!use_summer){
      bool do_transpose = false;
      if(mode=="transpose")
	do_transpose = true;
      //HLTRateAdapter my_hlt_adapter(partition_name,server_nameIn, server_nameOut,name_out,get_rate, do_transpose);
      my_hlt_adapter = new HLTRateAdapter(partition_name,server_nameIn, server_nameOut,name_out,bool_correct,get_rate, do_transpose);
      my_hlt_adapter->Configure();
      my_hlt_adapter->Run(provider_str,histogram_str);
      if (ReceivedSignal) {
        std::cout << " Signal " << ReceivedSignal << " received." << std::endl;
      }
      my_hlt_adapter->Stop();
    } else if(use_summer){
      my_hlt_adapter_sum = new HltRate::HLTRateAdapterSum (partition_name,server_nameIn, 
        						    server_nameOut, name_out, 
							    cpu_pattern,
							    num_steps, int_size, int_publish, 
							    int_thresh, 
							    int_time_from_histo_option, 
							    str_ready_condition,
							    mode,warn_cond,num_succ_warn, multi_thread);
      my_hlt_adapter_sum->Run(provider_str,histogram_str);
      if (ReceivedSignal) {                                   
        std::cout << " Signal " << ReceivedSignal << " received." << std::endl;
      }                                                                                                             
      my_hlt_adapter_sum->Stop();
    }
  }                                                         
    catch( daq::is::Exception & ex )                        
  {                                                         
    is::fatal( ex );                                        
  }     
      
  ERS_LOG( "Signal " << ReceivedSignal << " received, stopping '" << server_nameIn << "' server ... " );
  ERS_DEBUG(0, "Program terminating gracefully" );
  ERS_LOG( "LVL1PredAdapterMain done." );
  if (my_hlt_adapter_sum) delete my_hlt_adapter_sum;  
  if (my_hlt_adapter) delete my_hlt_adapter;  
  return 0;
}
