/** 
 * @file HLTRateAdapter.cpp
 * @author Antonio Sidoti
 * @date 21/01/2009
 * @brief Gets hlt histo containing rates from OH
 */


#include "hltadapter/src/HLTRateAdapter.h"
#include <oh/OHUtil.h>


#include <ipc/core.h>
#include <iostream>
#include "ers/ers.h"
#include <time.h>

#include "TRP/TimePoint_IS.h"
#include <rc/RunParams.h>
#include <TH2F.h>
#include "HistoToTimePoint.h"


using namespace std;

HLTRateAdapter::HLTRateAdapter(std::string partition_name,
                               std::string server_nameIn, 
			       std::string server_nameOut,
			       std::string name_out,
                               bool bool_correct,
			       bool get_rate,
			       bool transpose):
  Receiver(0),Provider(0),Histogram(0),semaphore(0){
  ERS_DEBUG( 0, "HLTRateAdapter::HLTRateAdapter()" );
  pPartitionName = partition_name;
  pServerNameIn  = server_nameIn;
  pServerNameOut = server_nameOut;
  pNameOut = name_out;
  m_correct = bool_correct;
  m_get_rate = get_rate;
  m_transpose = transpose;
}

HLTRateAdapter::HLTRateAdapter(const HLTRateAdapter& )
{
  // implemented only to forbid copying

}

HLTRateAdapter::~HLTRateAdapter()
{
  ERS_DEBUG( 3, "HLTRateAdapter::~HLTRateAdapter()" );
}

void HLTRateAdapter::Configure(){

  ERS_DEBUG( 3, "HLTRateAdapter::Configure" );
  mPartition = 0;
  if (!mPartition) mPartition = new IPCPartition( pPartitionName );
//  mReceiver_L2 = new HLTHistogramReceiver();
}

void HLTRateAdapter::Run(std::string provider_L2_str,
			 std::string histogram_L2_str) { // one for L2 and the other for EF

  ERS_DEBUG( 3, "HLTRateAdapter::Run");
  //  mPartition = 0;
  if (!mPartition) mPartition = new IPCPartition( pPartitionName );
  std::cout << "Setting up L2 Receiver:" << std::endl;
  std::cout << "receiver.SetupReceiver( " << pPartitionName << " , " << pServerNameIn << " , " <<
    pServerNameOut << " ) ; " << std::endl;
  HLTHistogramReceiver receiver;
  receiver.SetupReceiver(pPartitionName, pServerNameIn, pServerNameOut,pNameOut, m_correct, m_get_rate, m_transpose);
  Receiver = &receiver;
  OWLSemaphore s;
  semaphore = &s; 
 
  OHSubscriber ohhs( *mPartition, pServerNameIn, *Receiver, true ); // serialize
  Ohhs = &ohhs;
  //  std::string provider_name = "UTripServerIS";
  //  std::string provider_name = "L2-Gathered";
  //std::string provider_name_L2 = "Top-LVL2-L2-Segment-1-Gatherer";
  //  std::string histogram_name = "hltSteer_L2/hltSteer_L2/Rate5m";
  //std::string histogram_name_L2 = "SHIFT/TrigSteer_L2/Rate10s";
  OWLRegexp provider_L2(provider_L2_str);
  OWLRegexp histogram_L2(histogram_L2_str);
  Provider  = &provider_L2;
  Histogram = &histogram_L2;

  try{
    Ohhs->subscribe( provider_L2, 
		    histogram_L2, true); // true -> provides histogram value
    
 }
  catch(daq::oh::Exception & ex ){
    ERS_DEBUG(0, "Can not get Rates histogram from OH");
  }
  std::cout << "About to ohhs.run" << std::endl;
  //ohhs.run();
  semaphore->wait();
  semaphore = nullptr;
}

void HLTRateAdapter::Stop() {
    try {
      Ohhs->unsubscribe(*Provider, *Histogram);
    } catch(...){
      ERS_DEBUG(0, "OHSubscriber cannot unsubscribe to " << Provider->str() << ", " << Histogram->str() << ".");
    }
}

void HLTRateAdapter::signal_handler() {

  if (Receiver != 0) {
    if (semaphore != 0) {
      semaphore->post();
    }
  }
}
