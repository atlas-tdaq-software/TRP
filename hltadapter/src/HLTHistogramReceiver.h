#ifndef HLT_RECEIVER_H
#define HLT_RECEIVER_H



#include <vector>
#include <ipc/core.h>
#include <ipc/partition.h>

#include <is/infoiterator.h>
#include <is/inforeceiver.h>
#include <is/infodictionary.h>
#include <is/infostream.h>

#include <is/serveriterator.h>

#include <oh/core/GraphData.h>
#include <oh/core/ProfileData.h>
#include <oh/core/HistogramData.h>
#include <oh/OHUtil.h>
#include <oh/exceptions.h>
#include <oh/OHRootReceiver.h>
#include <oh/OHSubscriber.h>
#include "TRP/TimePoint_IS.h"
#include "TRP/HLT_Rate.h"
#include <TH2F.h>
#include <iostream>
#include <string>
using std::vector;
using namespace std;

class HLTHistogramReceiver : public OHRootReceiver {
 public:
  HLTHistogramReceiver( ){ 
    m_updated = false;
  }
  //  throw ( daq::oh::Exception );
  ~HLTHistogramReceiver ();
  
  
 public:
  
  virtual void receive( OHRootHistogram & histogram);
  virtual void receive( vector<OHRootHistogram*> & ){}
  virtual void receive(OHRootGraph&){}
  virtual void receive(std::vector <OHRootGraph*>&){}
  virtual void receive(OHRootGraph2D&){}
  virtual void receive(std::vector <OHRootGraph2D*>&){}

  void getRunParams(int & run, int & lb);
  void SetupReceiver(std::string pPartitionName, std::string pServerNameIn, std::string pServerNameOut, std::string NameOut,
                     bool bool_correct = false,
                     bool get_rate = false, 
		     bool transpose = false);

  // not to be used anymore...
  TimePoint_IS GetTimePoint(){return my_tp;};
 private:
  TimePoint_IS my_tp;
  HLT_Rate my_hlt_rate;
  IPCPartition * mPartition;
  std::string pPartitionName;
  std::string pServerNameIn;
  std::string pServerNameOut;
  std::string pNameOut;
  bool m_correct;
  bool m_get_rate;
  bool m_updated;
  bool m_is_first_event;
  unsigned int m_smk, m_hltk, m_l1k, m_bgk;
  const TH1 * input_th_old;
  long long time_utc_old;
  bool my_ret;
  bool m_transpose;
  int m_counter;

  int max_providers;

};


#endif
