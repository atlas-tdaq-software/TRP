/** 
 * @file HLTRateAdapterSum.cpp
 * @author Antonio Sidoti
 * @date 21/01/2009
 * @brief Gets hlt histo containing rates from OH
 */

#include "boost/foreach.hpp"

#include "hltadapter/src/HLTRateAdapterSum.h"
#include <oh/OHUtil.h>


#include <ipc/core.h>
#include <iostream>
#include "ers/ers.h"
#include <time.h>
#include <is/serveriterator.h>

#include <ipc/core.h>

#include <oh/core/HistogramData.h>
#include "TRP/TimePoint_IS.h"
#include <rc/RunParams.h>
#include <TH2F.h>
#include "HistoToTimePoint.h"


#include "TRP/L2SV.h"
#include "TRP/L2PU.h"
#include "TRP/EFD.h"


#include "TRP/Utils.h"


using namespace std;
using namespace boost;




namespace HltRate {

  ERS_DECLARE_ISSUE(hltadapter, ContentIssue, "content issue", )
    
    
  //#define DEBUG_L2_PROB
  //#define DEBUG
    
  //const char* L2PU_pattern_c = ".*(L2PU-[0-9]{4}).*";
  //const char* EFPT_pattern_c = ".*(pc-tdq-xpu-[0-9]{4}:[0-9]{1,2}).*";
    
    float my_lvl1_out, my_lvl2_out, my_ef_out;
  std::string tot_mode;
  
  
  regex *r_pat;
  regex r_ServerNameg; // to match the IS server name
  
  
  std::string mpattern_cpu;
  
  static const char* monitoringX[15] = {"providers", "previous_providers",  "inserts", "unneded",
					"begin", "end", "duration", "under", 
					"over", "tot_inserts", "oldest", 
					"newest", "l1_out", "l2_out", "ef_out" };
  const std::vector<std::string> ListTimePoint::s_monitringX(monitoringX, monitoringX+15);
  static const char* monitoringY[1] = {"value"};
  const std::vector<std::string> ListTimePoint::s_monitringY(monitoringY, monitoringY+1);
  GetTimeFromHisto *timeFromHisto(0);
  
  int mon_clbk_int=500;
  int mon_hlt_int=20;

/////////////////////////////////////////////////////////////////////////////
HLTRateAdapterSum::HLTRateAdapterSum(std::string partition_name,
				     std::string server_nameIn, // -> "Histogramming-EF-.*"
				     std::string server_nameOut, // ISS_TRP
				     std::string name_obj,   // EF_Rate
				     std::string cpu_pattern, // "pc-tdq"
				     int num_steps,
				     int interval_size,
				     int force_publish_interval,
				     int thresh,
				     int timeFromHistoOption, 
				     const std::string& readyCondition, std::string mode,
				     float warning_condition, int warn_successive_lost, int multi_thread) 
  : m_partitionName(partition_name),
    m_serverNameIn(server_nameIn),
    m_serverNameOut(server_nameOut), 
    m_nameObj(name_obj),
    m_publicationName(m_serverNameOut + "." + m_nameObj),
    m_threshold(thresh),
    m_timeFromHistoOption(timeFromHistoOption),
    m_cache(num_steps, interval_size, force_publish_interval, 0)
{
  ERS_DEBUG( 0, "HLTRateAdapterSum::HLTRateAdapterSum()" );
  r_pat = new boost::regex(cpu_pattern.c_str()); 
  r_ServerNameg = server_nameIn.c_str();
  timeFromHisto = new GetTimeFromHisto((GetTimeFromHisto::TimeOption)m_timeFromHistoOption);
  parseReadyCondition(readyCondition);
  my_lvl1_out = my_lvl2_out = my_ef_out = 0;
  m_mode = mode;
  tot_mode = mode;
  m_hlt_event = 0;
  m_is_first_event = true;
  m_warning_condition = warning_condition;
  warn_it=0;
  m_warn_it = warn_successive_lost;
  max_provider=0;
  m_multi = multi_thread;
  m_hlt_event=0;
}


/////////////////////////////////////////////////////////////////////////////
HLTRateAdapterSum::HLTRateAdapterSum(const HLTRateAdapterSum& )
  : m_threshold(0), m_timeFromHistoOption(0), m_readyCondition(0), m_cache(0,0,0,0) 
{
  // implemented only to forbid copying
}

/////////////////////////////////////////////////////////////////////////////
HLTRateAdapterSum::~HLTRateAdapterSum()
{
  ERS_DEBUG( 3, "HLTRateAdapterSum::~HLTRateAdapterSum()" );
  delete m_sender; m_sender = 0;
}




/////////////////////////////////////////////////////////////////////////////
void HLTRateAdapterSum::Run(std::string /* provider_l2_str*/,   // ".*"
			    std::string histogram_name_regex)  // ".*rate10s"
{ // one for l2 and the other for ef
  ERS_DEBUG( 3, "Run:");
  //  mpartition = 0;
  try {
    if (!m_partition) m_partition = new IPCPartition( m_partitionName );
  } catch (ers::Issue &ex) {
    ers::error(ex);
  }
  std::cout << "Run: Setting up HLT Receiver:" << std::endl;
  std::cout << "Run: mSummer.SetupReceiver( " << m_partitionName << " , " << m_serverNameIn << " , " <<
    m_serverNameOut << " ) ; " << std::endl;
  try {

    //    m_rec = new ISInfoReceiver (*m_partition); // this is seralized
    
    bool do_serialize_callbacks = false; // NOT serialize i.e. multihreaded
    //   do_serialize_callbacks = true;  // YES serialized i.e. NOT multihreaded
    //m_rec = new ISInfoReceiver (*m_partition, do_serialize_callbacks);
    ISInfoReceiver receiver(*m_partition, do_serialize_callbacks);
    Receiver = &receiver;
    OWLSemaphore s;
    semaphore = &s;

  } catch (ers::Issue &ex) {
    ers::error(ex);
  }

  ISServerIterator it(*m_partition);
  m_criteria = new ISCriteria(histogram_name_regex, oh::HistogramData<float>::type(), 
			      ISCriteria::AND ); // name obj is ".*Rate10s"


  ISInfoDictionary* dict = new ISInfoDictionary(*m_partition );
  m_sender = new TimePointAsyncSender(dict, m_publicationName);

  unsigned count(0);
  while (it()) {
    cmatch res_ServerNameg;
    if(regex_search(it.name(), res_ServerNameg, r_ServerNameg)){ // match the server name
      m_iss.push_back(it.name());
      try {
	Receiver->subscribe(it.name(), *m_criteria ,
			 &HLTRateAdapterSum::HLTcallback_rates, this);
	ERS_INFO("Run: Subscribing to IS server: " << it.name());
	count++;
      } catch(daq::is::InvalidName &ex){
	ers::warning(hltadapter::ContentIssue(ERS_HERE, "ISInfoDictionary  Invalid Name" +boost::lexical_cast<std::string>( ex.what())));
      } catch(daq::is::RepositoryNotFound &ex) {
	ers::warning(hltadapter::ContentIssue(ERS_HERE, "Repository not found" +boost::lexical_cast<std::string>( ex.what())+"  RESTART THE ADAPTER" ));
      } catch(daq::is::AlreadySubscribed &ex){
	ers::warning(hltadapter::ContentIssue(ERS_HERE, "Already subscribed" +boost::lexical_cast<std::string>( ex.what())));
      } catch(...){
	ers::warning(hltadapter::ContentIssue(ERS_HERE, "Unknown exception. Better RESTART the adapter"));
      }
    }
  }
  ERS_INFO("Run: Subscribed to: " << count << " IS servers");
  
  // subscribe to RunCtrlStatistics to get overall rates
  
  //ISCriteria L2SV_crit("L2SV-SUM", L2SV::type(), ISCriteria::AND );
  //ISCriteria L2PU_crit("L2PU-SUM", L2PU::type(), ISCriteria::AND );
  //ISCriteria  EFD_crit( "EFD-SUM",  EFD::type(), ISCriteria::AND );
  try {
    //Receiver->subscribe("RunCtrlStatistics",  L2SV_crit, &HLTRateAdapterSum::HLTcallback_L2SV, this);
    //Receiver->subscribe("RunCtrlStatistics",  L2PU_crit, &HLTRateAdapterSum::HLTcallback_L2PU, this);
    //Receiver->subscribe("RunCtrlStatistics",   EFD_crit, &HLTRateAdapterSum::HLTcallback_EFD, this);
    Receiver->subscribe("RunCtrlStatistics.L2SV-SUM",  &HLTRateAdapterSum::HLTcallback_L2SV, this);
    Receiver->subscribe("RunCtrlStatistics.L2PU-SUM",  &HLTRateAdapterSum::HLTcallback_L2PU, this);
    Receiver->subscribe("RunCtrlStatistics.EFD-SUM",  &HLTRateAdapterSum::HLTcallback_EFD, this);
  } catch (...) {
    ERS_INFO("Run: Could not subscribe to RunCrtlStatistics data sources " );
  }
  m_running = true;    
  ERS_INFO("Run: Entered running state");
  //m_rec->run();
  semaphore->wait();
}
/////////////////////////////////////////////////////////////////////////////
void HLTRateAdapterSum::Stop() {
  m_running = false;
  ERS_INFO("Stop: Unsubscribing");
  if ( !Receiver ) {
    ERS_INFO("Stop: Receiver not created, unsubscription not needed");
  }
  BOOST_FOREACH( const std::string & iss_name, m_iss) {
    try {
      Receiver->unsubscribe(iss_name, *m_criteria, true);
      ERS_INFO("Stop: Unsubscribed from: " << iss_name);
    } catch (ers::Issue &ex) {
      ers::warning(ex);
    }
  }
  //ISCriteria L2SV_crit("L2SV-SUM", L2SV::type(), ISCriteria::AND );
  //ISCriteria L2PU_crit("L2PU-SUM", L2PU::type(), ISCriteria::AND );
  //ISCriteria  EFD_crit( "EFD-SUM",  EFD::type(), ISCriteria::AND );
  try {
    //Receiver->unsubscribe("RunCtrlStatistics",  L2SV_crit);
    //Receiver->unsubscribe("RunCtrlStatistics",  L2PU_crit);
    //Receiver->unsubscribe("RunCtrlStatistics",   EFD_crit);
    Receiver->unsubscribe("RunCtrlStatistics.L2SV-SUM");
    Receiver->unsubscribe("RunCtrlStatistics.L2PU-SUM");
    Receiver->unsubscribe("RunCtrlStatistics.EFD-SUM");
  } catch (...) {
    ERS_INFO("Could not Unsubscribe to RunCrtlStatistics data sources " );
  }
  
  m_iss.clear();
  //delete m_rec;
  delete m_criteria;
}

/////////////////////////////////////////////////////////////////////////////
void HLTRateAdapterSum::signal_handler() {

  if (Receiver != 0) {
    if (semaphore != 0) {
      semaphore->post();
    }
  }
}
/////////////////////////////////////////////////////////////////////////////
void HLTRateAdapterSum::HLTcallback_L2SV(ISCallbackInfo *isc){
  if(m_is_first_event)
    ERS_INFO("First Event ! In HLTcallback_L2SV " );
  if(isc->reason() != ISInfo::Deleted){
    L2SV my_l2sv;
    
    isc->value(my_l2sv);
    my_lvl1_out = my_l2sv.IntervalEventRate;
  }
}

void HLTRateAdapterSum::HLTcallback_L2PU(ISCallbackInfo *isc){
  if(m_is_first_event)
    ERS_INFO("First Event ! In HLTcallback_L2PU " );
  if(isc->reason() != ISInfo::Deleted){
    L2PU my_l2pu;
    isc->value(my_l2pu);
    my_lvl2_out = my_l2pu.LVL2IntervalRate;
  }
}


void HLTRateAdapterSum::HLTcallback_EFD(ISCallbackInfo *isc){
  if(m_is_first_event)
    ERS_INFO("First Event! In HLTcallback_EFD " );
  if(isc->reason() != ISInfo::Deleted){
    EFD my_efd;
    isc->value(my_efd);
    my_ef_out = my_efd.RateOut;
  }
}



/////////////////////////////////////////////////////////////////////////////
void HLTRateAdapterSum::HLTcallback_rates(ISCallbackInfo *isc){
  if (!m_running) {
    ERS_LOG(" not running " );
    return;
  }
  if(m_is_first_event)
    ERS_INFO(" First Event ! ---------------------------------------------" );
  if(isc->reason() == ISInfo::Deleted) return;
  m_is_first_event = false;
  // 1) receive histos from all the PUs
  oh::HistogramData<float> hda; // do we need to create this object on each callback
  isc->value(hda);
  //  std::string h_name_tmp = isc->name(); // name of the histogram 
  std::string provider(isc->name());
  //std::size_t firstdot(provider.find_first_of('.')+1);
  //std::size_t lasttdot(provider.find_last_of('.'));  
  //provider = provider.substr(firstdot, lasttdot - firstdot);
  if(MyVerbose())
    ERS_LOG("Evt:" << m_hlt_event << "  Provider Before matching: " << provider );
  
  provider = regex_replace(provider, *r_pat, "\\1", boost::match_default | boost::format_sed);
  ++m_hlt_callback_rate;
  if(MyVerbose()){
    ERS_LOG("Evt:" << m_hlt_event << "  Provider: " << provider <<  "  numcallback:" << m_hlt_callback_rate  << "  time:" << isc->time());
    time_t t = timeFromHisto->get(hda);
    OWLTime my_o(t);
    ERS_LOG("Time from histo is: " << my_o);
    //    m_is_first_event = true;
    ERS_LOG("Dumping first TP from cache BEFORE inserting:");
    ERS_LOG("" <<  m_cache );
  }
  boost::mutex::scoped_lock lock(m_cache_mutex);
  m_cache.insert(hda, provider);

  //  m_cache_mutex.lock();

  //  m_cache_mutex.unlock();
  
  if(MyVerbose()){
    ERS_LOG("Dumping first TP from cache AFTER inserting:");
    ERS_LOG("" <<  m_cache );
  }

  if ( m_cache.is_any_ready() ) {
    ++m_hlt_event;
    m_hlt_callback_rate=0;
    //ERS_DEBUG(3, "Cache got one rate ready " << m_cache );  
    if(MyVerbose(true)){
      ERS_LOG("Event = " << m_hlt_event);
      ERS_LOG("============== Cache is READY to be published"); 
      ERS_LOG("Dumping1 Cache BEFORE getting" << m_cache );
    }
    ListTimePoint::Collected h = m_cache.get();
    if(h.first) {
      if(MyVerbose(true)){
	ERS_LOG("Dumping2 Cache AFTER getting and h.first valid" << m_cache );
	//	  std::cout << "%%%%%" << h.first->print(std::cout) << std::endl;
      }
      m_sender->send(h.first);
    } else {
      //      ERS_DEBUG(3, "Got NULL rate TimePoint to send" );
      ERS_LOG("Got NULL rate TimePoint to send" );
    }
    if((h.first && h.second)) {
      float my_prov_val = -999999.;
      if(h.second->get("providers", "value",my_prov_val)){
	if(max_provider<my_prov_val)
	  max_provider=my_prov_val;
	if(max_provider>my_prov_val){
	  float ratio = fabs(max_provider-my_prov_val)/
	    (max_provider+my_prov_val);
	  if(ratio>m_warning_condition){
	    ++warn_it;
	    if(warn_it>m_warn_it){
	      ers::warning(hltadapter::ContentIssue(ERS_HERE, "Lost some providers.You might observe some artificial rates decrease .BETTER TO RESTART THE ADAPTER!. The difference is:" + boost::lexical_cast<string>( ratio )));
	      ers::warning(hltadapter::ContentIssue(ERS_HERE, "Current provider is:"+ boost::lexical_cast<string>(my_prov_val) + "  Previous one is:" + boost::lexical_cast<string>(max_provider) + " ratio is:" + boost::lexical_cast<string>( ratio )));
	    }
	  } else {
	    warn_it=0;
	  }
	}
      }
      
      m_sender->send(h.second, m_publicationName+"_mon");
    } else if(!h.first && h.second) {
      ers::warning(hltadapter::ContentIssue(ERS_HERE, "Something strange happened! Resetting the whole cache"));
      m_cache.Reset();
      ERS_DEBUG(3, "Got NULL mon TimePoint to send" );
    }
#ifdef DEBUG
    std::cout << "netstat Before Publ EST:" << std::endl;
    system("netstat -a -t | grep \"ESTABLISHED\" | wc -l");
#endif

  } else {
    ERS_DEBUG(3, "Nothing ready" );
  }
} // end of callback hlt rates method


void HLTRateAdapterSum::parseReadyCondition(const std::string& cmd) {  
  m_readyCondition = 0; 

  if ( cmd.find("HistoryToShort") != std::string::npos )
    m_readyCondition |= ListTimePoint::HistoryToShort;
  
  if ( cmd.find("SecondBinFilled50") != std::string::npos )
    m_readyCondition |= ListTimePoint::SecondBinFilled50;  

  if ( cmd.find("SecondBinFilled90") != std::string::npos )
    m_readyCondition |= ListTimePoint::SecondBinFilled90;  

  if ( cmd.find("ThirdBinFilled50") != std::string::npos )
    m_readyCondition |= ListTimePoint::ThirdBinFilled50;

  if ( cmd.find("NewObjectsCount2") != std::string::npos )
    m_readyCondition |= ListTimePoint::NewObjectsCount2;    

  if ( cmd.find("NewObjectsCount3") != std::string::npos )
    m_readyCondition |= ListTimePoint::NewObjectsCount3;    

  if ( cmd.find("ForceAfterInterval") != std::string::npos )
    m_readyCondition |= ListTimePoint::ForceAfterInterval;    

  if ( cmd.find("HoldForInitialPeriod") != std::string::npos )
    m_readyCondition |= ListTimePoint::HoldForInitialPeriod;    

  //  if ( cmd.find("") != std::string::npos )
  //    m_readyCondition |= ListTimePoint::;    

  if (m_readyCondition == 0 ) {// nothing was set
    ERS_LOG("Conditions mask used to send histograms was 0, resetting it to the default!");
    m_readyCondition = ListTimePoint::HistoryToShort | ListTimePoint::SecondBinFilled90;
  }
  
  ERS_LOG("Conditions mask used to send histograms: " << std::hex  << "0x" <<  m_readyCondition);
  m_cache.setReadyCondition(m_readyCondition);
}



//////////////////////////////////////////////////////////////////////////////////
GetTimeFromHisto::GetTimeFromHisto(TimeOption option) 
  : m_option(option) {}

/////////////////////////////////////////////////////////////////////////////
time_t GetTimeFromHisto::get(const oh::HistogramData<float> & hda) const {

  if ( m_option == CurrentTime ) {
    ERS_DEBUG(3, "using current time as timestamp for histogram" );
    return time(0);
  }
  if ( m_option ==  PublicationTime ) {
    ERS_DEBUG(3, "using publication time as timestamp for histogram " <<  hda.time() );
    // ERS_LOG("using publication time as timestamp for histogram " <<  hda.time() );
    return hda.time().c_time();
  }



  // actual time of creation of histo is put in the last column
  // year, number of days of the year, hour, min and secs.

  int last_bin = hda.get_bin_count(oh::Axis::X);
  //  int labels_num = (int)hda.get_indices(oh::Axis::X).size();
  //  int last_bin = -1;
  //  if ( labels_num > 0 )
  //    last_bin = (int)hda.get_indices(oh::Axis::X)[labels_num-1];

  // check that last_bin in x corresponds to time label
  // and first to total
  if( hda.get_labels(oh::Axis::X).front() != "total" 
      || hda.get_labels(oh::Axis::X).back() != "time" 
      || last_bin < 1  ){ 
    throw hltadapter::ContentIssue(ERS_HERE, "Cannot get time and total rate from oh::HistogramData");
  } 
  
  struct tm time_ptr;
  time_ptr.tm_year = OWLTime(time(0)).year()-1900;
  time_ptr.tm_hour = (int)hda.get_bin_value(last_bin,2,0)-1;
  time_ptr.tm_mon  = 0;
  time_ptr.tm_mday = 0;
  time_ptr.tm_min =  (int)hda.get_bin_value(last_bin,3,0);
  time_ptr.tm_sec =  (int)hda.get_bin_value(last_bin,4,0);
  
  
  time_t t = OWLTime(time_ptr).c_time();
  unsigned yday = (int)hda.get_bin_value(last_bin,1,0)+1;
  t += yday * 24 * 3600;

  // according to:
  // http://cpptruths.blogspot.com/2005/10/return-value-optimization.html
  // it is better to create return value in place
  ERS_DEBUG(3, "Time extracted from histogram " << OWLTime(t) );
  return t;
}

/////////////////////////////////////////////////////////////////////////////
HLTTimePoint::HLTTimePoint() 
  : numInserts(0), numUnnededInserts(0), timeBegin(0), timeEnd(0) {}

/////////////////////////////////////////////////////////////////////////////
TimePoint_IS* HLTTimePoint::rate() const {
  if ( cpu.empty() ) 
    return 0;
  TimePoint_IS *t0 = new TimePoint_IS();  
  float trp_rate = -9;
  if(tot_mode == "l2corr"){
    if(t0->get("total","output",trp_rate)){
      t0->Scale(my_lvl2_out/trp_rate);
    }
  }
  if(tot_mode == "efcorr"){
    if(t0->get("total","output",trp_rate)){
      t0->Scale(my_ef_out/trp_rate);
    }
  }
  
  
  bool conv_stat = hltadapter_imp::convert(&sum,t0);   
  
  if (conv_stat) {

    // clear time stamp from the data
    t0->set("time", "input", 0);
    t0->set("time", "prescale", 0);
    t0->set("time", "raw", 0);
    t0->set("time", "output", 0);
    return t0;
  }
  else
    delete t0;
  return 0;
}

/////////////////////////////////////////////////////////////////////////////
bool HLTTimePoint::insert(const oh::HistogramData<float>& newdata, 
			  const std::string& source, const time_t& ctime) {
  bool ret = false;
  numInserts++;
  if ( cpu.empty() ) {
    // this is first time we need to insert rate data
    sum = newdata; // prepare structure
    cpu.insert(source);
    timeBegin = ctime;
    timeEnd   = ctime;
  }
#ifdef DEBUG_L2_PROB  
  std::cout << "%%%%% " << source << "   " <<  newdata.get_bins_array( )[1] << "  " <<  newdata.get_bins_array( )[2] << "  " << ctime << std::endl;
#endif  
  if ( cpu.find(source) == cpu.end() ) {
    // do real insert

    size_t sz = sum.get_bins_size( );
    for ( unsigned int i = 0; i < sz; ++i ) {
      sum.get_bins_array( )[i] += newdata.get_bins_array( )[i];
    }    
    cpu.insert(source);
    ret = true;
    if ( ctime < timeBegin )
      timeBegin = ctime;
    if ( ctime > timeEnd )
      timeEnd = ctime;

  } else {
    numUnnededInserts++;
  }
  return ret;
}

/////////////////////////////////////////////////////////////////////////////
ostream& operator<<(ostream& str, const HLTTimePoint& p) {
  str << " num CPUs received: " << p.cpu.size()
      << " num inserts: " << p.numInserts
      << " num ~inserts: " << p.numUnnededInserts    
      << " begin: " << p.timeBegin
      << " end: " << p.timeEnd;
    //<< " total input rate:" << p.sum.get_bin_value(0,0,0)

  //std::copy(p.cpu.begin(), p.cpu.end(), std::ostream_iterator<std::string>(str, " "));
  //str << std::endl;
  return str;
}

/////////////////////////////////////////////////////////////////////////////
ListTimePoint::ListTimePoint(unsigned steps, unsigned interval, unsigned force_interval, 
			     unsigned cond) 
  : time_0(0), numSteps(steps), intervalSize(interval), list_htp(numSteps), 
  numInserts(0), 
  numEffectiveInserts(0),
  numUnder(0),
  numOver(0),
  m_forcePublishInterval(force_interval),
  m_conditionMask(cond), 
  m_initialPeriod(true)
    {}


/////////////////////////////////////////////////////////////////////////////
void ListTimePoint::insert(const oh::HistogramData<float>& data, const std::string& source) {
  time_t t = timeFromHisto->get(data);
  numInserts++;


  m_oldestSinceLastPublication = std::min(t, m_oldestSinceLastPublication);
  m_newestSinceLastPublication = std::max(t, m_newestSinceLastPublication);

  // first object we ever get initializes the time
  // the time_0 will be aligned with localtime % interval
  if ( time_0 == 0 ) {

    time_0 = t - OWLTime(t).sec()%intervalSize; 
    ERS_DEBUG(1, "Cache start time intialized to: " << OWLTime(time_0) << " " << OWLTime(t) << " s" <<  OWLTime(t).sec()%intervalSize );
  }

  // calculate the bin
  int bin = queue_bin(t);
  // 
  //  if(bin<0) bin = -1;  
  //  if(bin >= m_cache.num_steps ) bin = m_cache.num_steps; 

  ERS_DEBUG(2, "Time bin: " << bin << " queue start: " << OWLTime(time_0) << " histogram time: " << OWLTime(t) );
  // numInserts++;

  if(bin < 0){
    ++numUnder;
    ERS_LOG("Too small bin. Time from histo:" << OWLTime(t) << " is smaller than current time: " << OWLTime(time_0) );
  } else if(bin > numSteps){
    ERS_LOG("Too large bin. Time from histo:" << OWLTime(t) << " is larger  than current time: " << OWLTime(time_0) );
    ++numOver;
  } else {
    // we got right nuber of bins (note in all other cases we lose counts)

    HLTTimePoint& tp = list_htp.at(bin);
    if ( tp.insert(data, source, t) )
      numEffectiveInserts++;

  }
}

/////////////////////////////////////////////////////////////////////////////
int ListTimePoint::queue_bin(time_t x) const {
  return (int)( (float)(x - time_0)/(float)intervalSize);
}

/////////////////////////////////////////////////////////////////////////////
void ListTimePoint::Reset(){
  list_htp.clear();
  time_0 = 0;
  numInserts = numEffectiveInserts = numCpuLastPublished = numUnder = numOver = 0;
  m_initialPeriod=true;
  m_oldestSinceLastPublication = 0;
  m_newestSinceLastPublication = 0;
}


/////////////////////////////////////////////////////////////////////////////
bool ListTimePoint::is_any_ready() {

  unsigned in_first  = list_htp[0].cpu.size();
  unsigned in_second = list_htp[1].cpu.size();
  unsigned in_third  = list_htp[2].cpu.size();
  

  time_t temp_time = time(NULL); // now
  int bin = queue_bin(temp_time);
  long int diff = temp_time-time_0; 
  
#ifdef DEBUG
  std::cout << "temp_time:" << temp_time << "  m_cache.time_0:" << time_0 
	    << "   m_cache.m_interval_size:" << intervalSize 
	    <<"  m_cache.num_steps:" << numSteps << std::endl;
  std::cout << "diff:" << diff << "   bin:" <<  bin << std::endl; 
  std::cout << "Num Prev Cpu:" <<numCpuLastPublished  << std::endl;
  std::cout << "Num Act Cpu:" << list_htp[0].cpu.size() << std::endl;
#endif

  if(diff>(intervalSize*numSteps) || diff < -intervalSize){
    ERS_LOG("Time difference is large");
    cout << "-->  Now is:" <<  OWLTime(temp_time) << " Starting time of queues is:" << OWLTime(time_0) << "  diff is:" << diff << endl;
  }

  
  if ( m_conditionMask & SecondBinFilled50 && in_second > 0.5*in_first  ) {
    ERS_LOG("Publishing because in second bin there is already 50% of this what was in 1st: " 
	     << in_first << " in 2nd: " << in_second );
    return true;
  }
  
  if ( m_conditionMask & SecondBinFilled90 && in_second > 0.9*in_first  ) {
    ERS_LOG("Publishing because in second bin there is already 90% of this what was in 1st: " 
	     << in_first << " in 2nd: " << in_second );
    return true;
  }
  
  if ( m_conditionMask & ThirdBinFilled50 && in_third > 0.5*in_first  ) {
    ERS_LOG("Publishing because in second bin there is already 50% of this what was in 1st: " 
	     << in_first << " in 2nd: " << in_second );
    return true;
  }
  
  if ( m_conditionMask & NewObjectsCount2  && numEffectiveInserts > 2.* in_first ) {
    ERS_LOG("Publishing because there is already 2 times more histograms arrived that is in 1st slot " );
    return true; 
  }
  
  if ( m_conditionMask & NewObjectsCount3  && numEffectiveInserts > 3.* in_first ) {
    ERS_LOG("Publishing because there is already 2 times more histograms arrived that is in 1st slot " );
    return true; 
  }
  
  if ( m_conditionMask & ForceAfterInterval && list_htp[m_forcePublishInterval].cpu.empty() ) {
    ERS_LOG("Publishing because in :" << m_forcePublishInterval << " bin there are already entries" );
    return true; 
  }

  // security hooks
  // publish even when filling does not start nicelly in first bins
  // may happen when at begin of the run or rates are not published for some time
  //  if ( list_htp[0].numInserts < 2*(numInserts - list_htp[0].numInserts) &&  list_htp[0].numInserts>0) {
  if ( list_htp[0].numInserts < 2*(numInserts - list_htp[0].numInserts) ) {
    ERS_LOG("Publishing because in the number of inserts (besides first bin) (since last get operation) exceeded 2 times the content of first bin: " 
	     << list_htp[0].numInserts << " inserts: " << 2*(numInserts - list_htp[0].numInserts) );
    cout << "---> list_htp[0].numInserts=" <<  list_htp[0].numInserts << " numInserts: " << numInserts << "  numEffectiveInserts:" << numEffectiveInserts << endl;
    return true;
  }

  // publish when the queue gets to short
  if ( numOver > 0  ) {
    ERS_LOG("Publishing because in the number of inserts  there are attempts to publish beyond last queue bin: " 
	     << numOver  );
    return true;
  }


    
  if   ( m_conditionMask & HistoryToShort && bin >= numSteps){
    ERS_LOG("Publishing because queue is to short to hold new entries as it starts: " 
	      << OWLTime(time_0) << " now: " << OWLTime(temp_time));
    return true;
  }

  return false;  
}

/////////////////////////////////////////////////////////////////////////////
std::pair<const TimePoint_IS*, const TimePoint_IS*> ListTimePoint::get() {
  // figure out which rate is ready to be published
  //  if ( ! is_any_ready() )
  //    return make_pair((const TimePoint_IS*)0,(const TimePoint_IS*)0);
  
  TimePoint_IS* monitoring = new TimePoint_IS();
  TimePoint_IS* rate(0);
  monitoring->format(ListTimePoint::s_monitringX, s_monitringY);

  // {
  const HLTTimePoint& ready = list_htp.front();
  monitoring->set( "providers", "value", ready.cpu.size());
  monitoring->set( "previous_providers",  "value", numCpuLastPublished);
  monitoring->set( "inserts",   "value", ready.numInserts);    
  monitoring->set( "unneded",   "value", ready.numUnnededInserts);
  monitoring->set( "begin",     "value", OWLTime(ready.timeBegin).sec());
  monitoring->set( "end",       "value", OWLTime(ready.timeEnd).sec());
  monitoring->set( "duration",  "value", ready.timeEnd - ready.timeBegin);
  
  monitoring->set( "under",     "value", numUnder);    
  monitoring->set( "over",      "value", numOver);
  monitoring->set( "oldest",    "value", time_0 - m_oldestSinceLastPublication); // this tells how many updates the whole queue got
  monitoring->set( "newest",    "value", m_newestSinceLastPublication - time_0 ); // this tells how many updates the whole queue got
  monitoring->set( "tot_inserts",  "value", numInserts); // this tells how many updates the whole queue got        
  monitoring->set( "l1_out",    "value", my_lvl1_out); 
  monitoring->set( "l2_out",    "value", my_lvl2_out); 
  monitoring->set( "ef_out",    "value", my_ef_out); 
  
  numCpuLastPublished = ready.cpu.size();
  rate = ready.rate();
  //    use current time_0 as timestamp for this rate measurement
  if (rate) rate->TimeStamp = OWLTime(time_0);
  monitoring->TimeStamp = OWLTime(time_0);
  //}
  // we remove one slot but just afterwards add another one so queue lenght stays always the same
  list_htp.pop_front();
  list_htp.push_back(HLTTimePoint());

  // we need to advance in time (by configured interval)
  time_0 += intervalSize;

  // reset stats
  numInserts = 0;
  numEffectiveInserts = 0;
  BOOST_FOREACH(const HLTTimePoint& hlttp, list_htp) {
    numInserts += hlttp.numInserts;
    numEffectiveInserts += (hlttp.numInserts - hlttp.numUnnededInserts);
  }

  numOver = 0;
  numUnder = 0;
  m_oldestSinceLastPublication = time_0;
  m_newestSinceLastPublication = time_0;
  m_initialPeriod=false; // after first publication we are not anymore ini initial period



  return make_pair(rate, monitoring);
}

// ///////////////////////////////////////////////////////////////////////////
ostream& operator<<(ostream& str, const ListTimePoint& l) {
  str << "\n list size: " << l.list_htp.size() 
      << " configured size: " << l.numSteps 
      << " interval size: " << l.intervalSize
      << " arrived to late: " << l.numUnder 
      << " arrived to early: " << l.numOver 
      << " inserts since last get: " << l.numInserts 
      << " in range inserts since last get: " << l.numEffectiveInserts
      << std::endl;

  std::deque<HLTTimePoint>::const_iterator i;
  for ( i = l.list_htp.begin(); i != l.list_htp.end() ; ++i ) 
    if (i->cpu.size() != 0 )
      str << "slot: "<< std::distance(l.list_htp.begin(), i)  << *i << std::endl;
  /* that was crashing for some reason
    float rate = -1.;
    if((l.list_htp[0].rate())->get( "total", "input", rate)){
    str << "Total Input rate is:" << rate  << std::endl;
    } else {
    str << "Total input rate NO RATE" << std::endl;
    }
  */
  return str;
}
  bool HLTRateAdapterSum::MyVerbose(bool only_evt){
    bool my_ret = false;
    int my_rem_evt = m_hlt_event%mon_hlt_int;
    if(my_rem_evt<=3){
      if(only_evt){
	return true;
      } else {
	int my_rem_clb = m_hlt_callback_rate%mon_clbk_int;
	if(my_rem_clb<=3){
	  return true;
	} else return false;
      }
    } else {
      return false;
    }
    return my_ret;
  }
}

