#include "HLTRateSummer.h"
#include <TH2F.h>
#include "HistoToTimePoint.h"
//#include "rc/LuminosityInfo.h"
//#include "TTCInfo/LuminosityInfo.h"
#include "TTCInfo/LumiBlock.h"
#include "TRP/Utils.h"
//#include "TRP/LuminosityInfo.h"
#include "boost/regex.hpp"

using namespace boost;

// I guess this code is not used any more // AS 2011 09 21
// these are performed now during configuration
const char* L2PU_pattern_c = ".*(L2PU-[0-9]{1,4}).*";
const char* EFPT_pattern_c = ".*(pc-tdq-xpu-[0-9]{1,5}:[0-9]{1,2}).*";
regex r_L2(L2PU_pattern_c);
regex r_EF(EFPT_pattern_c);


void HLTRateSummer::SetupReceiver(std::string PartitionName, 
				  std::string ServerNameOut,
				  std::string NameObj,
				  int list_num_steps,
				  int list_interval_size,
				  int interval_publish,
				  int flow_thresh){
#ifdef DEBUG
  std::cout << "Starting HLTRateSummer::SetupReceiver" << std::endl;
#endif
  pPartitionName = PartitionName;
  pServerNameOut = ServerNameOut;
  if (!mPartition) mPartition = new IPCPartition( pPartitionName );
  m_smk = m_hltk = m_l1k = 0;
  m_is_first_event = true;
  m_str_add = NameObj;
  // initialize the list
  m_publish_interval = interval_publish;
  m_list_num_steps = list_num_steps;
  m_list_interval_size = list_interval_size;
  InitializeList(m_list_num_steps, m_list_interval_size);
  m_is_first_event = true;
  m_flow_thresh = flow_thresh;
  m_Run = 0;
  m_LB = 0;
#ifdef DEBUG
  std::cout << "Ending HLTRateSummer::SetupReceiver" << std::endl;
#endif
}
void HLTRateSummer::receive( OHRootHistogram & histogram){
  mPartition = 0;
  if (!mPartition) mPartition = new IPCPartition( pPartitionName );
  // subscribe to lumiblock not clear how to do it....
  // ISInfoReceiver rec(*mPartition);
  //  rec.subscribe("RunParams", TrigConfSmKey::type() , // TODO 
  //		&HLTRateSummer::LumiBlockCallback, this); 
  
  
  ISInfoDictionary dict( *mPartition );
  // 1) receive histos from all the PUs
  TimePoint_IS my_tp;
  unsigned period = 0;
  float quality = 0;
  const TH1 * input_th = histogram.histogram.get();
  OHRootHistogram *hist_ptr = new OHRootHistogram(histogram);

  // 2) convert them in TimePoint_IS
  bool my_ret = hltadapter_imp::convert(input_th, my_tp, 0, period, quality,  m_is_first_event);
  my_tp.RunNumber=m_Run;
  my_tp.LumiBlock=m_LB;
  std::string s_smk  = trp_utils::convertToString(m_smk);
  std::string s_l1k  = trp_utils::convertToString(m_l1k);
  std::string s_hltk = trp_utils::convertToString(m_hltk);
  //  (my_tp.MetaData).push_back(s_smk);
  //(my_tp.MetaData).push_back(s_l1k);
  //(my_tp.MetaData).push_back(s_hltk);
  //sets the TimeStamp of TimePoint to the one from hist_ptr
  OWLTime my_time = hist_ptr->time;
  my_tp.TimeStamp = my_time;
  std::string my_name = hist_ptr->histogram->GetName();
  cmatch res_L2;
  cmatch res_EF;
  //  if(regex_search(my_name.c_str(), res_L2, r_L2)){
  //  my_name =  res_L2[1].str();
  //} else if(regex_search(my_name.c_str(), res_EF, r_EF)){
  //  my_name = res_EF[1].str(); // this it the subsegment index
  //}
  if(regex_search(my_name.c_str(), res_L2, r_pat)){
    my_name =  res_L2[1].str();
  } 
#ifdef DEBUG
  else {
    std::cout << "Warning!  No Match with" << my_name << std::endl;
  }
#endif 
  (my_tp.MetaData).push_back(my_name);
#ifdef DEBUG
  std::cout << "Name is:" << my_name << std::endl;
  if(m_is_first_event){
    std::cout << " >>>>>>>>>>>>  First Event <<<<<<<<<<<<<<<<" << std::endl;
    // qui dovresti anche dire da quale PU/PT proviene TODO
    my_tp.print(std::cout);
    std::cout << "hist_ptr.histogram->GetName()" << std::endl;
    std::cout << hist_ptr->histogram->GetName() << std::endl;
    m_is_first_event = false;
  }
#endif
  delete hist_ptr;
  // check if too many overflows
  // if so reinitialize the list
  if(my_lt.num_over>m_flow_thresh){
    std::cout << "Too many Overflows - Resetting the TimePoint list" << std::endl;
    std::cout << "Num_Under:" << my_lt.num_under << "   Num_Over:" << my_lt.num_over << std::endl;
#ifdef DEBUG
    DumpList();
#endif
    InitializeList(m_list_num_steps, m_list_interval_size);
    // qui si incasina.... 
  }
  
  // 3) sum them in list/queue/map of TimePoint_IS (4 mins of history)


  int l_bin = -1;
#ifdef DEBUG
  std::cout << "AddToList" << std::endl;
#endif
  AddToList(my_tp, l_bin);
#ifdef DEBUG
  std::cout << "Histo added in bin:" << l_bin << std::endl;
#endif
  // 
  // no need to implement with threads 
  // basta che pubblichi se e`  
  // passato abbastanza tempo oppure siamo alla fine della lista
  // It would be more clever to publish if the number of histograms summed
  // for a given interval does not change too much
  bool do_publish = false;
  bool do_move = false; 
  time_t temp_time = time(NULL);

  int bin =  (((float)temp_time - (float)my_lt.time_0)/
	      ((float)my_lt.m_interval_size*(float)my_lt.num_steps))*my_lt.num_steps;

  long int diff = temp_time-my_lt.time_0;

#ifdef DEBUG
  std::cout << "temp_time:" << temp_time << "  my_lt.time_0:" << my_lt.time_0 
	    << "   my_lt.m_interval_size:" << my_lt.m_interval_size 
	    <<"  my_lt.num_steps:" << my_lt.num_steps << std::endl;
  std::cout << "diff:" << diff << "   bin:" <<  bin << std::endl; 
  std::cout << " m_publish_interval" << m_publish_interval << std::endl;
#endif

  if((temp_time-my_lt.time_0>m_publish_interval)||(bin>=my_lt.num_steps)){
    do_publish = true;
    do_move = true;
  }

  
  TimePoint_IS my_tp_publ = my_lt.list_tp[0];
 
  if(my_tp_publ.XLabels[0]=="xxx") {
#ifdef DEBUG
    std::cout << "empty time_point" << std::endl;
#endif
    do_move = true; // don't publish empty tp but you need to 
    do_publish = false;
    // move the circular buffer
  }
#ifdef DEBUG
  std::cout << "^^^ bin:" << bin << std::endl;
  std::cout << "^^ diff:" << diff << std::endl;
  std::cout << "^^ do_publish:" << do_publish << std::endl;
  std::cout << "^^ do_move:" << do_move << std::endl;
#endif
  
  // 4) publish the first histo in queue
  if(do_publish||do_move){
    //  if(m_is_first_event){
    //  std::cout << ">>>>>>>>>>>>>> First Event <<<<<<<<<<<<<<<<<" << std::endl;
    // std::cout << "Publishing TimePoint_is" << std::endl;
    //  DumpList();
    // }




#ifdef DEBUG
    std::cout << "Dumping List" << std::endl;
    DumpList();
#endif
    if(do_publish){
#ifdef DEBUG
      std::cout << "Dumping TP to publish" << std::endl;
      my_tp_publ.print(std::cout);
#endif      
      std::string str_add = pServerNameOut + ".";
      str_add+= m_str_add;
      
      try {
	dict.insert(str_add,my_tp_publ);
#ifdef DEBUG
	std::cout << "publish 1: " << str_add << std::endl; 
#endif
      } catch(daq::is::InfoAlreadyExist){
	dict.update(str_add,my_tp_publ,true);
#ifdef DEBUG
	std::cout << "publish 2: " << str_add << std::endl; 
#endif
      }
    }
    
    // 5) delete the histo published and move the list in the future by 10s
    if(do_move){
      MoveList();
    }
    if(m_is_first_event){
      m_is_first_event=false;
    }
  } // end of if(do_publish||do_move)
  
  // 6) go back to 1
}

void HLTRateSummer::AddToList(TimePoint_IS  tp, int &bin){
  OWLTime ow_time;
  ow_time = tp.TimeStamp;
  // convert OWLTime in time_t
  time_t o_time = trp_utils::OWL2Time(ow_time);
#ifdef DEBUG
  std::cout << "o_time = " << o_time << std::endl;
  std::cout << " my_lt.time_0:" << my_lt.time_0 << "  int_size:" << my_lt.m_interval_size << "  nstep:" <<
    my_lt.num_steps << std::endl;
#endif
  bin = (((float)o_time - (float)my_lt.time_0)/
	 ((float)my_lt.m_interval_size*(float)my_lt.num_steps))*my_lt.num_steps;
#ifdef DEBUG
  std::cout << "bin = " << bin << std::endl;
#endif
  if(bin<0) bin = -1;
  if(bin>=my_lt.num_steps) bin=my_lt.num_steps;
  // underflow is -1 // overflow is bin num_steps
  if(bin<0){
    ++my_lt.num_under;
  } else if(bin>=my_lt.num_steps){
    ++my_lt.num_over;
  } else {
    TimePoint_IS my_temp = my_lt.list_tp[bin];
    trp_utils::Add(my_temp, tp);
    my_lt.list_tp[bin] = my_temp;
    ++my_lt.num_tp[bin];
  }
#ifdef DEBUG
  std::cout << "End of AddtoList" << std::endl;
#endif
}
void HLTRateSummer::MoveList(){
#ifdef DEBUG
  std::cout << "Move List" << std::endl;
  std::cout << "First Item before deleting:" << std::endl;
  my_lt.list_tp[0].print(std::cout);
#endif
  my_lt.list_tp.pop_front();
  my_lt.num_tp.pop_front();
  my_lt.list_tp.resize(my_lt.num_steps);
  my_lt.num_tp.resize(my_lt.num_steps);
  my_lt.time_0+=my_lt.m_interval_size;
#ifdef DEBUG
  std::cout << "First Item after deleting:" << std::endl;
  my_lt.list_tp[0].print(std::cout);
#endif
  my_lt.num_under=0;
  my_lt.num_over=0;
}


void HLTRateSummer::InitializeList(int num_steps, int interval_size){
  my_lt.list_tp.clear();
  my_lt.num_tp.clear();
  my_lt.num_steps = num_steps;
  my_lt.list_tp.resize(num_steps);
  my_lt.num_tp.resize(num_steps);
  my_lt.num_under=0;
  my_lt.num_over=0;
  // setta il tempo in multipli di 10s ??
  time_t now_t = time(NULL);
  int i_time = ((float)now_t/10.);
  i_time *= 10;
  //OWLTime o_time = i_time;
  my_lt.time_0 = i_time;
  // erase of the lists TODO
  my_lt.m_interval_size = m_list_interval_size;
#ifdef DEBUG
  std::cout << "Start Initialize List" << std::endl;
  std::cout << "time_0 = " << my_lt.time_0 << std::endl;
  DumpList();
  std::cout << "End Initialize List" << std::endl;
#endif
}
void HLTRateSummer::DumpList(){
  std::cout << "Dumping List" << std::endl;
  std::cout << "Time Init:     NumSteps:     IntSize:" << std::endl;
  std::cout << my_lt.time_0 << "  " << my_lt.num_steps << "  " << my_lt.m_interval_size << std::endl;
  std::cout << " Num  Under " << my_lt.num_under << "  Num Over" << my_lt.num_over << std::endl;
  std::deque<TimePoint_IS>::iterator it;
  for(it = (my_lt.list_tp).begin(); it != (my_lt.list_tp).end();++it){
    (*it).dump(std::cout);
  }
  std::cout << "=============================================" << std::endl;

}
void HLTRateSummer::LumiBlockCallback(ISCallbackInfo *isc) {
  std::cout << "LumiBlock Changed:" << std::endl;
  // TODO
  //LuminosityInfo lb;
  LumiBlock lb;  

  isc->value(lb);
  m_LB = lb.LumiBlockNumber;
  m_Run = lb.RunNumber;
  
  // qui facci quello che devi fare con lb
  // now get the trigger keys
  
  trp_utils::GetTriggerKeys(pPartitionName, m_smk, m_hltk, m_l1k);
}
