#include "HLTHistogramReceiver.h"
#include <TH2F.h>
#include "HistoToTimePoint.h"
//#include "rc/LuminosityInfo.h"
//#include "TTCInfo/LuminosityInfo.h"
#include "TRP/Utils.h"

#include <boost/regex.hpp>

boost::regex pattern("\\.");
std::string replaced("");

void HLTHistogramReceiver::SetupReceiver(std::string PartitionName, 
    std::string ServerNameIn, 
    std::string ServerNameOut,
    std::string NameOut,
    bool bool_correct,
    bool get_rate, bool transpose){
  pPartitionName = PartitionName;
  pServerNameIn  = ServerNameIn;
  pServerNameOut = ServerNameOut;
  pNameOut = NameOut;
  if (!mPartition) mPartition = new IPCPartition( pPartitionName );
  m_smk = m_hltk = m_l1k = m_bgk = 0;
  m_is_first_event = true;
  m_correct = bool_correct;
  m_get_rate = get_rate;
  m_transpose = transpose;
  m_counter=0;
  max_providers=0;
}



void HLTHistogramReceiver::receive( OHRootHistogram & histogram){
  mPartition = 0;
  if (!mPartition) mPartition = new IPCPartition( pPartitionName );
  ISInfoDictionary dict( *mPartition );
  ERS_DEBUG( 0, "Starting HLTHistogramReceiver::receive()" << std::endl);
  unsigned period = 0;
  float quality = 0;
  const TH1 * input_th = histogram.histogram.get();
  const Annotations& my_ann = histogram.annotations;


  int num_providers=-1;
  bool has_providers = false; 

  //check if the input histogram has annotations (number of providers)
  for (auto&& pair: my_ann) {
    if (pair.first == "providers") {
      num_providers = atoi(pair.second.c_str());
      has_providers = true;
      break;
    }
  }
  if (num_providers == -1) {
    std::cerr<<"Input histogram has no annotations. Setting num_providers = -1\n";
  }
  //std::cout << "has_providers: " << has_providers << std::endl; 

  // provider correction from HistoToTimePoint.cpp
  //std::map<std::string, std::string> const ann_map(my_ann.begin(), my_ann.end());
  //auto const prov_iter = ann_map.find("providers");
  //bool const has_providers = prov_iter != ann_map.end();

  float correction = 1.;
  if(has_providers)
    {
      //int num_providers = atoi(prov_iter->second.c_str()); 
      if (num_providers > 0 && max_providers > 0 && m_correct) {
	correction = (float)num_providers/(float)max_providers;
      }
      else if(num_providers <= 0){ // protection if providers=0. For now set correction=1. In future, set num_providers to previous value. 
	correction = 1.;
	std::cout << "WARNING: num_providers = " << num_providers << ", WARNING: max_providers = " << max_providers << " - this should be investigated" << std::endl;
      }
      //std::cout << "num_providers " << num_providers << std::endl;
      //std::cout << "max_providers " << max_providers << std::endl;
      //std::cout << "correction " << correction << std::endl;
    }
  else
    {
      std::cerr<<"Input histogram has no annotations. Setting num_providers = -1, correction = 1\n";
    }

  if (num_providers > max_providers) max_providers = num_providers;

  OWLTime now(OWLTime::Seconds); // for the moment take the time here. 
  
  // call to Convert() -- applies correction to legacy code -----
  if(!m_get_rate){
  my_ret = hltadapter_imp::convert(input_th, my_tp, 0, period, quality,  my_ann,
       max_providers,
      m_correct,
      m_is_first_event, m_transpose);
  } else {
    float diff_time = (histogram.time).total_mksec_utc()-time_utc_old;
    my_ret = hltadapter_imp::convert(input_th, input_th_old, diff_time,
				     my_tp, 0, period, quality,  
				     m_is_first_event, m_transpose);
    //input_th_old = new TH1(*input_th);
    input_th_old = input_th;
  } //----- end -----
  
  // get trigger keys
  trp_utils::GetTriggerKeys(pPartitionName, m_smk, m_hltk, m_l1k, m_bgk);  
  std::string s_smk  = std::to_string(m_smk);
  std::string s_l1k  = std::to_string(m_l1k);
  std::string s_hltk = std::to_string(m_hltk);
  std::string s_bgk  = std::to_string(m_bgk);
  // get trigger release
  std::string s_rel = "";
  trp_utils::GetTriggerRelease(pPartitionName, s_rel);

  //if we are using the Run3 scheme instead of TimePoint_IS
  if(pNameOut == "Run3_mode")
  {
    int NXbins = input_th->GetNbinsX(); 
    int NYbins = input_th->GetNbinsY();


    int labeledBinsX = 0;
    for ( int bin = 1; bin <= NXbins; ++bin) {
      if (strlen(input_th->GetXaxis()->GetBinLabel(bin))>0) {
        labeledBinsX++;
      }
    }
    NXbins = labeledBinsX;

    int labeledBinsY = 0;
    for ( int bin = 1; bin <= NYbins; ++bin) {
      if (strlen(input_th->GetYaxis()->GetBinLabel(bin))>0) {
        labeledBinsY++;
      }
    }
    NYbins = labeledBinsY;

    for ( int i = 1; i <= NXbins; ++i ) {
      const char* xlabel=input_th->GetXaxis()->GetBinLabel(i); 
      std::string output_name = pServerNameOut + "." + xlabel;
      for ( int j = 1; j <= NYbins; ++j ) {
        const char* ylabel = input_th->GetYaxis()->GetBinLabel(j);
        std::string my_ylabel(ylabel);
        if (my_ylabel == "Input"){
          my_hlt_rate.Input = input_th->GetBinContent(i, j);
	  my_hlt_rate.Input = my_hlt_rate.Input/correction;
	}
        else if(my_ylabel == "AfterPS"){
          my_hlt_rate.AfterPS = input_th->GetBinContent(i, j);
	  my_hlt_rate.AfterPS = my_hlt_rate.AfterPS/correction;
	}
	else if(my_ylabel == "Output"){
          my_hlt_rate.Output = input_th->GetBinContent(i, j);
	  my_hlt_rate.Output = my_hlt_rate.Output/correction;
	}
        else if(my_ylabel == "Express"){
	  my_hlt_rate.Express = input_th->GetBinContent(i, j);
	  my_hlt_rate.Express = my_hlt_rate.Express/correction;
	}
        else
          std::cout<<"Attribute "<<my_ylabel<<" is not part of HLT_Rate class. The attributes are:\nInput, AfterPS, Output and Express\n";
      }
      dict.checkin(output_name,my_hlt_rate, true);	
    }


    // add trigger keys
    //not implemented for Run3 mode yet
    /*
    //int m_rel = 0;
    //size_t position = s_rel.find("_");
    //std::string rel_num = s_rel.substr(position+1);
    //std::string release_number = boost::regex_replace( rel_num, pattern, replaced);
    //if (release_number != "NaN") m_rel = std::stoi(release_number);
    //int dim_data = my_tp.Data.size() + my_tp.YLabels.size() * 1; // add TriggerDB_Keys
    //(my_tp.Data).resize(dim_data);
    //my_tp.XLabels.push_back("TriggerDB_Keys");

    //THE NEXT ONES ARE IN THE WRONG ORDER
    my_hlt_rate.input = (float)m_smk;
    my_hlt_rate.prescaled= (float)m_l1k;
    my_hlt_rate.raw = (float)m_hltk;
    my_hlt_rate.output = (float)m_bgk;
    my_hlt_rate.rerun = (float)m_rel;
    my_hlt_rate.algoIn = (float)my_runnumber;

    std::string output_name = pServerNameOut + ".TriggerDB_Keys"; 
    dict.checkin(output_name,my_hlt_rate, true);
    */
  }
  else //if we are not in run3_mode, publish using TimePoint_IS
  {
    time_utc_old = (histogram.time).total_mksec_utc();
    // get timestamp, num and lumiblock
    //  time_t now_t;
    // questo e`  sbaliato... devi prendere il tempo 
    // di pubblicazione dell'histo originario TODO
    // nel convert
    unsigned int my_runnumber = 0;
    unsigned short my_lumiblock = 0;
    trp_utils::GetRunParams(pPartitionName,my_runnumber,my_lumiblock);
    my_tp.RunNumber = my_runnumber;
    my_tp.LumiBlock = my_lumiblock;
    my_tp.TimeStamp = now; // To change
    // set meta data
    (my_tp.MetaData).push_back(s_smk);
    (my_tp.MetaData).push_back(s_l1k);
    (my_tp.MetaData).push_back(s_hltk);
    (my_tp.MetaData).push_back(s_bgk);
    (my_tp.MetaData).push_back(s_rel);
    // add trigger keys
    int m_rel = 0;
    size_t position = s_rel.find("_");
    std::string rel_num = s_rel.substr(position+1);
    std::string release_number = boost::regex_replace( rel_num, pattern, replaced);
    if (release_number != "NaN") m_rel = std::stoi(release_number);
    int dim_data = my_tp.Data.size() + my_tp.YLabels.size() * 1; // add TriggerDB_Keys
    (my_tp.Data).resize(dim_data);
    my_tp.XLabels.push_back("TriggerDB_Keys");
    my_tp.Data[dim_data-6] = (float)m_smk;
    my_tp.Data[dim_data-5] = (float)m_l1k;
    my_tp.Data[dim_data-4] = (float)m_hltk;
    my_tp.Data[dim_data-3] = (float)m_bgk;
    my_tp.Data[dim_data-2] = (float)m_rel;
    my_tp.Data[dim_data-1] = (float)my_runnumber;
    // fino a qui nel convert
    if(m_is_first_event){
      my_tp.print(std::cout);
    }
    // publishing
    // check the name and publish accordingly
    // OWLRegexp isL2_rexp("\\L2");
    std::string histoname = input_th->GetName();
    if(!m_updated) m_updated = true;
    std::string str_add = pServerNameOut + ".";
    str_add += pNameOut;

    if(m_is_first_event) std::cout << "Publishing in " << "dict.insert("<< str_add << ", " << " my_tp)"  << std::endl;
    try {
      dict.insert(str_add,my_tp);
      ++m_counter;
    } catch(daq::is::InfoAlreadyExist&){
      dict.update(str_add,my_tp,true);
      ++m_counter;
    }
    if(m_is_first_event) m_is_first_event = false;
    int rem = m_counter%60;
    int cnt = (int)(m_counter/60);
    if(rem==0){
      if(m_counter-cnt<5){
        cout << "Publishing now: " << now << " cnt:" << m_counter << endl;
      }
    }

  }
}

void HLTHistogramReceiver::getRunParams(int & my_run, int & my_lb){
  ISInfoDictionary dict( *mPartition );
  //LuminosityInfo lb;
  LumiBlock lb;
  try{
    //dict.getValue("RunParams.LuminosityInfo", lb);
    dict.getValue("RunParams.LumiBlock", lb);
    my_run = lb.RunNumber;
    my_lb = lb.LumiBlockNumber;
    //  }catch(ers::Issue &e){
}catch(...){
  //ERS_DEBUG(0, "Cannot access RunParams.LuminosityInfo info");
  ERS_DEBUG(0, "Cannot access RunParams.LumiBlock info");
  my_run = 99999;
  my_lb = 9;
}

}


HLTHistogramReceiver::~HLTHistogramReceiver()
{

  
  if(pNameOut == "Run3_mode")
  {
    //The run has ended. Histogram Receiver will send NULL values to all attributes.
    if (!mPartition) mPartition = new IPCPartition( pPartitionName );
    //First, setting the iterator to all items of HLT_Rate class
    ISInfoIterator ii( *mPartition, pServerNameOut, HLT_Rate::type() );
    ISInfoDictionary dict( *mPartition );

    //then seting all values of the local hlt_rate class inctance to null.
    my_hlt_rate.Input = 0; 
    my_hlt_rate.AfterPS = 0;
    my_hlt_rate.Output = 0;
    my_hlt_rate.Express = 0;

    //Finally, broadcasting NULLS to all items of class HLT_Rate.
    while ( ii())
    {
      dict.checkin(ii.name(),my_hlt_rate, true);	

    }
  }

}
