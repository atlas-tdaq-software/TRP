#ifndef HLT_RATESUMMER_H
#define HLT_RATESUMMER_H

#include <vector>
#include <ipc/core.h>
#include <ipc/partition.h>

#include <is/infoiterator.h>
#include <is/inforeceiver.h>
#include <is/infodictionary.h>
#include <is/infostream.h>

#include <oh/core/GraphData.h>
#include <oh/core/ProfileData.h>
#include <oh/core/HistogramData.h>
#include <oh/OHUtil.h>
#include <oh/exceptions.h>
#include <oh/OHRootReceiver.h>
#include <oh/OHSubscriber.h>
#include "TRP/TimePoint_IS.h"
#include <TH2F.h>
#include "HistoToTimePoint.h"
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <deque>


using std::vector;


struct ListTimePoint {

  time_t time_0;
  std::deque<TimePoint_IS> list_tp;
  int num_steps;
  std::deque<int> num_tp;
  //  std::deque<std::vector<std::string> > list_vec_nodes;
  int m_interval_size;
  int num_under;
  int num_over;
  
};


class HLTRateSummer : public OHRootReceiver {
 private:
  bool m_updated;
 public:
  HLTRateSummer( ){ 
    m_updated = false;
  }
  void receive( OHRootHistogram & histogram);
  void receive( vector<OHRootHistogram*> & ){};

  void SetupReceiver(std::string pPartitionName, std::string pServerNameOut,
		     std::string NameObj,
		     int list_num_steps,
		     int list_interval_size,
		     int interval_publish,
		     int flow_thresh);
  
  
  void AddToList(TimePoint_IS  tp, int &bin);
  ListTimePoint my_lt;
  void MoveList();
  void InitializeList(int num_steps=24, int interval_size=10);
  void DumpList();
  void LumiBlockCallback(ISCallbackInfo *isc);
 private:	
  int m_publish_interval;
  int m_list_num_steps;
  int m_list_interval_size;
  std::string m_str_add;
  bool m_is_first_event;
  IPCPartition * mPartition;
  int m_flow_thresh;
  std::string pPartitionName;
  std::string pServerNameIn;
  std::string pServerNameOut;
  int m_smk, m_hltk, m_l1k;
  unsigned int m_Run;
  unsigned short m_LB;
  regex *r_pat;
};
#endif
