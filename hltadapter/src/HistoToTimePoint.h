#ifndef hltadapter_HistoToTimePoint_h
#define hltadapter_HistoToTimePoint_h




#include <string>
#include "oh/OHRootHistogram.h"
#include "oh/core/HistogramData.h"

#include <time.h>

typedef std::vector< std::pair< std::string,std::string > > Annotations;

/**
   @brief Converts the TH2 which contains HLT rate informations from steering to the TimePoint obejct.
   For the HLT rate histograms refer to TrigSteerMoni offline package. TrigRateMoni file. 
   @warning the convert method creates the TimePoint object   
   @param selector selects which content to pick   
   Two additional quantities are calculated. 
   @param period which numbers time period from which this readout comes
   @param collection_quality tells how many entries are comming from this period, value from 0 to 1, while 1 is best (for exceptionally good quality value 2 is used)

  
 */

class TimePoint_IS;

namespace hltadapter_imp {
  enum {
    total = 1,
    groups = 2,
    streams = 4,
    chains  = 8,
    all = 0xf
  };
  bool convert (OHRootHistogram* input, TimePoint_IS* output, unsigned selector, unsigned& period, 
		float& collection_quality, bool verbose=false);
  
  bool convert (const TH1* input, TimePoint_IS* output, unsigned selector, unsigned& period, 
		float& collection_quality, bool verbose=false);

  bool convert (const TH1* input, TimePoint_IS& output, unsigned selector, unsigned& period, 
		float& collection_quality, Annotations my_ann,
                int& max_providers,
                bool correct=false,
		bool verbose=false, bool transpose=false); // this is the one used
  
  bool convert (const TH1* input, const TH1* input_old , float diff_time, 
		TimePoint_IS& output, unsigned selector, 
		unsigned& period, float& collection_quality, 
		bool verbose=false, bool transpose=false);
  bool convert (oh::HistogramData<float> const * input, TimePoint_IS* output );
 
 
} // end of namespace hltadapter_imp

#endif
