//#include <TH2F.h>
#include "TRP/TimePoint_IS.h"
#include "HistoToTimePoint.h"
#include "HLTRateAdapterSum.h"

#include <iostream>
#include "ers/ers.h"

using namespace std;

ERS_DECLARE_ISSUE(hltadapter, Issue, "conversion issue", ERS_EMPTY)

//#define DEBUG_H

// TODO put the config keys and the Timestamps
namespace hltadapter_imp {
  bool convert ( const TH1* input, TimePoint_IS* output, unsigned /*selector*/, 
		 unsigned& period, float& quality, bool verbosity) {
    if(verbosity)
      std::cout << "hltadapter_imp::convert::1" << std::endl;
    
    period = 0;
    quality = 0.;

    // figure out number of X bins which are labeled
    int NXbins = input->GetNbinsX();
    int NYbins = input->GetNbinsY();
  
    std::vector<std::string> xlabels; 
    std::vector<std::string> ylabels; 
    int labeledBinsX = 0;
    for ( int bin = 1; bin <= NXbins; ++bin) {
      if (strlen(input->GetXaxis()->GetBinLabel(bin))>0) {
	labeledBinsX++;
	xlabels.push_back(std::string(input->GetXaxis()->GetBinLabel(bin)));
      }
    }
    NXbins = labeledBinsX;

    int labeledBinsY = 0;
    for ( int bin = 1; bin <= NYbins; ++bin) {
      if (strlen(input->GetYaxis()->GetBinLabel(bin))>0) {
	labeledBinsY++;
	ylabels.push_back(std::string(input->GetYaxis()->GetBinLabel(bin)));
      }
    }
    NYbins = labeledBinsY;

    // create TimePoint_IS
    //    output = new TimePoint_IS(); // without commenting I get a glibc detected *** malloc(): memory corruption
    output->SetXAxis(xlabels);
    output->SetYAxis(ylabels);
    // go over the values and stuff them in now
    for ( int i = 1; i <= NXbins; ++i ) {
      for ( int j = 1; j <= NYbins; ++j ) {
	output->set(input->GetXaxis()->GetBinLabel(i), 
		    input->GetYaxis()->GetBinLabel(j), 
		    input->GetBinContent(i, j));
      }
    }
    // calculate quality & period
    unsigned sum=0;
    unsigned max=0;
    period = 0;
    for ( int p = NXbins; p <= input->GetNbinsX(); ++p ) {
      unsigned v = (unsigned)input->GetBinContent(p, 1);
      sum += v;
      if ( v > max ) {
	max = v;
	period = p - NXbins;
      }
    }
    return false;

    if ( sum == 0 )
      return false;

    quality = max/sum;
    if ( sum == max ) 
      quality = 2.;


    return true;
  }
  
  // ------------------------------------------------------------------
  
  bool convert( OHRootHistogram* input, TimePoint_IS* output2, unsigned /*selector*/, unsigned& period, float& quality, bool verbosity) {
    output2 = 0;
    bool my_ret = false;
    TH1 * input_th  = input->histogram.release();
    if(verbosity) {
      std::cout << "Dumping TH1 * input_th" << std::endl;
      input_th->Print();
    }
    my_ret = convert(input_th, output2, 0, period, quality);
    return(my_ret);
  }
  

// ------------------------------------------------------------------
  bool convert( const TH1* input, TimePoint_IS &output, unsigned /*selector*/, 
		unsigned& period, float& quality, Annotations my_ann,
                int& max_providers,
                bool correct,
                bool /*verbosity*/, bool transpose) {
    // this is the one used
    
    period = 0;
    quality = 0.;
    
    // figure out number of X bins which are labeled
    int NXbins = input->GetNbinsX();
    int NYbins = input->GetNbinsY();

    string time_str = "time";
    
    std::vector<std::string> xlabels; 
    std::vector<std::string> ylabels; 
    int labeledBins = 0;
    for ( int bin = 1; bin <= NXbins; ++bin) {
      if (strlen(input->GetXaxis()->GetBinLabel(bin))>0) {
	if(time_str.compare(input->GetXaxis()->GetBinLabel(bin)) == 0) continue;
	labeledBins++;
	xlabels.push_back(std::string(input->GetXaxis()->GetBinLabel(bin)));
      }
    }
    /// print annotations
    //    int it2=0;
    //    for(Annotations::iterator i_ann=my_ann.begin();i_ann!=my_ann.end();++i_ann,++it2){
    //      cout << "it2 = " << it2 << my_ann[it2].first << "   " << my_ann[it2].second << endl;
    //    } 

    // for simplicity convert annotations to map
    std::map<std::string, std::string> const ann_map(my_ann.begin(), my_ann.end());

    auto const prov_iter = ann_map.find("providers");
    bool const has_providers = prov_iter != ann_map.end();
    if(has_providers){
      xlabels.push_back("Providers");
      labeledBins++;
    }
    NXbins = labeledBins;
    
    int labeledBinsY = 0;
    for ( int bin = 1; bin <= NYbins; ++bin) {
      if (strlen(input->GetYaxis()->GetBinLabel(bin))>0) {
	labeledBinsY++;
	ylabels.push_back(std::string(input->GetYaxis()->GetBinLabel(bin)));
      }
    }
    NYbins = labeledBinsY;
    
    
    
    // create TimePoint
    // may be you should comment this one....
    //output = new TimePoint_IS(); // without commenting I get a glibc detected *** malloc(): memory corruption
    output.EraseCont();
    output.SetXAxis(xlabels);
    output.SetYAxis(ylabels);
    if(transpose){
      output.SetXAxis(ylabels);
      output.SetYAxis(xlabels);
    }
   
    // correction for uncomplete sums
    //std::cout << "Apply correction " << correct << std::endl;
    //
    double correction = 1.;
  if(has_providers)
  {
    int num_providers = atoi(prov_iter->second.c_str());
    if (max_providers > 0 && correct) {
      correction = (double)num_providers/(double)max_providers;
    }
    //std::cout << "num_providers " << num_providers << std::endl;
    //std::cout << "max_providers " << max_providers << std::endl;
    //std::cout << "correction " << correction << std::endl;
  }
  else
  {
    std::cerr<<"Input histogram has no annotations. Setting num_providers = -1, correction = 1\n";
  }

    // go over the values and stuff them in now
    for ( int i = 1; i <= NXbins-1; ++i ) {
      for ( int j = 1; j <= NYbins; ++j ) {
	if(time_str.compare(input->GetXaxis()->GetBinLabel(i)) == 0) continue;
	//(output.Data).push_back(input->GetBinContent(i, j));
        double data = input->GetBinContent(i, j);
        data = data/correction;
	if(transpose) { 
          //(output.Data).push_back(input->GetBinContent(j, i));
          double data = input->GetBinContent(j, i);
          data = data/correction;
        }
        (output.Data).push_back((float)data);
      }
    }


    // add annotations
    if(has_providers) {
      for (  int j = 1; j <= NYbins; ++j ) {
        // the format of the annotation strings is defined in Gatherer, it
        // is "providers" for lowest level and "mig.lvlN" for higher level.
        std::string const key = j == 1 ? "providers" : "mig.lvl" + std::to_string(j-1);
        auto itr = ann_map.find(key);
        if (itr == ann_map.end()) {
          (output.Data).push_back(0);
        } else {
          int num_providers = atoi(itr->second.c_str());
          (output.Data).push_back((float)num_providers);
        }
      }
    } else {
      for ( int j = 1; j <= NYbins; ++j ) (output.Data).push_back(0);
    }


    // calculate quality & period
    unsigned sum=0;
    unsigned max=0;
    period = 0;
    for ( int p = NXbins; p <= input->GetNbinsX(); ++p ) {
      unsigned v = (unsigned)input->GetBinContent(p, 1);
      sum += v;
      if ( v > max ) {
	max = v;
	period = p - NXbins;
      }
    }
    return false;
    
    if ( sum == 0 )
      return false;
    
    quality = max/sum;
    
    if ( sum == max ) 
      quality = 2.;
    
    return true;
  }
  

// ------------------------------------------------------------------
  bool convert( const TH1* input, const TH1* input_old,  float diff_sec, 
		TimePoint_IS &output, 
	      unsigned /*selector*/, 
		unsigned& period, float& quality, bool /*verbosity*/, bool transpose) {
  period = 0;
  quality = 0.;

  
  // figure out number of X bins which are labeled
  int NXbins = input->GetNbinsX();
  int NYbins = input->GetNbinsY();
  
  std::vector<std::string> xlabels; 
  std::vector<std::string> ylabels; 
  int labeledBins = 0;
  for ( int bin = 1; bin <= NXbins; ++bin) {
    if (strlen(input->GetXaxis()->GetBinLabel(bin))>0) {
      labeledBins++;
      xlabels.push_back(std::string(input->GetXaxis()->GetBinLabel(bin)));
    }
  }
  NXbins = labeledBins;
  
  int labeledBinsY = 0;
  for ( int bin = 1; bin <= NYbins; ++bin) {
    if (strlen(input->GetYaxis()->GetBinLabel(bin))>0) {
      labeledBinsY++;
      ylabels.push_back(std::string(input->GetYaxis()->GetBinLabel(bin)));
    }
  }
  NYbins = labeledBinsY;
  
  
  // protection on null input_old
  bool empty_rate = false;
  if(!input_old) empty_rate = true;
  // protection for zero time diff  
  if(diff_sec<=0) empty_rate = true;
  
  // create TimePoint
  // may be you should comment this one....
  //output = new TimePoint_IS(); // without commenting I get a glibc detected *** malloc(): memory corruption
  output.EraseCont();
  output.SetXAxis(xlabels);
  output.SetYAxis(ylabels);
  if(transpose){
    output.SetYAxis(xlabels);
    output.SetXAxis(ylabels);
  }
  // go over the values and stuff them in now
  if(!transpose){
    for ( int i = 1; i <= NXbins; ++i ) {
      for ( int j = 1; j <= NYbins; ++j ) {
	
	float rate = (empty_rate) ? 0. :  
	  (input->GetBinContent(i, j)-input_old->GetBinContent(i, j))/diff_sec;
	
	(output.Data).push_back(rate);
      }
    }
  } else { // this is transposed
    for ( int i = 1; i <= NXbins; ++i ) {
      for ( int j = 1; j <= NYbins; ++j ) {
	
	float rate = (empty_rate) ? 0. :  
	  (input->GetBinContent(j, i)-input_old->GetBinContent(j, i))/diff_sec;
	
	(output.Data).push_back(rate);
      }
    }
  }
  // calculate quality & period
  unsigned sum=0;
  unsigned max=0;
  period = 0;
  for ( int p = NXbins; p <= input->GetNbinsX(); ++p ) {
    unsigned v = (unsigned)input->GetBinContent(p, 1);
    sum += v;
    if ( v > max ) {
      max = v;
      period = p - NXbins;
    }
  }
  return false;
  
  if ( sum == 0 )
    return false;
  
  quality = max/sum;
  
  if ( sum == max ) 
    quality = 2.;
  
  return true;
  }
  
  
  
  bool convert ( oh::HistogramData<float> const * input, TimePoint_IS* output ) {
    // get axis
#ifdef DEBUG_H
    std::cout << "get_bin_count:" <<  input->get_bin_count(oh::Axis::X) 
	      << "  " << (int)input->get_labels(oh::Axis::X).size() << std::endl;
    std::cout << "get_bin_count:" <<  input->get_bin_count(oh::Axis::Y) 
	      << "  " << (int)input->get_labels(oh::Axis::Y).size() << std::endl;
    
    for(std::vector<std::string>::iterator it = input->get_labels(oh::Axis::X).begin(); it != input->get_labels(oh::Axis::X).end();++it){
      std::cout << "X lab:" << (*it) << std::endl;
    }
    for(std::vector<std::string>::iterator it = input->get_labels(oh::Axis::Y).begin(); it != input->get_labels(oh::Axis::Y).end();++it){
      std::cout << "Y lab:" << (*it) << std::endl;
    }
#endif
    
    if ( input->get_bin_count(oh::Axis::X) != (int)input->get_labels(oh::Axis::X).size() ) {
      ers::warning(hltadapter::Issue(ERS_HERE, "not all X bins are labeled, bins:"
				     +boost::lexical_cast<std::string>(input->get_bin_count(oh::Axis::X))
				     + " labels: "
				     +boost::lexical_cast<std::string>(input->get_labels(oh::Axis::X).size())
				     ));
      
      std::cout << "get_bin_count:" <<  input->get_bin_count(oh::Axis::X) 
		<< "  " << (int)input->get_labels(oh::Axis::X).size() << std::endl;
      copy(input->get_labels(oh::Axis::X).begin(), input->get_labels(oh::Axis::X).end(), 
	   std::ostream_iterator<std::string>(std::cout));
      
      return false;
    }
    
    if ( input->get_bin_count(oh::Axis::Y) != (int)input->get_labels(oh::Axis::Y).size() ) {
      ers::warning(hltadapter::Issue(ERS_HERE, "not all Y bins are labeled, bins:"
				     +boost::lexical_cast<std::string>(input->get_bin_count(oh::Axis::Y))
				     + " labels: "
				     +boost::lexical_cast<std::string>(input->get_labels(oh::Axis::Y).size())
				     ));
      
      std::cout << "get_bin_count:" <<  input->get_bin_count(oh::Axis::Y) 
		<< "  " << (int)input->get_labels(oh::Axis::Y).size() << std::endl;
      copy(input->get_labels(oh::Axis::Y).begin(), input->get_labels(oh::Axis::Y).end(), 
	   std::ostream_iterator<std::string>(std::cout));
      return false;
      
    }
    
    // prepare TimePointIS (including resets)
    output->format( input->get_labels(oh::Axis::X), input->get_labels(oh::Axis::Y));
    
    // just consider bins with labels
    //  for ( unsigned x = 0, xmax = (unsigned)input->get_labels(oh::Axis::X).size(); x < xmax; ++x ) {
    //    for ( unsigned y = 0, ymax = (unsigned)input->get_labels(oh::Axis::Y).size(); y < ymax; ++y ) {
    
    for ( unsigned x = 1, xmax = (unsigned)input->get_bin_count(oh::Axis::X); x <= xmax; x++ ) {
      for ( unsigned y = 1, ymax = (unsigned)input->get_bin_count(oh::Axis::Y); y <= ymax; y++ ) {
	//std::cout << "(x  y   val) = " << x << "  " << y << "  " << input->get_bin_value(x,y,0) << std::endl;      
	output->set(x-1,y-1, input->get_bin_value(x,y,0)); // z index is fake
      }
    }           
    return true;
  }

} // end of namespace!
