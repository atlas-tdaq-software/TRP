#ifndef HLTRATEADAPTER_H
#undef HLTRATEADAPTER_H
/** 
 * @file HLTRateAdapter.cpp
 * @author Antonio Sidoti
 * @date 21/01/2009
 * @brief Gets hlt histo (i.e. rates) containing rates from OH
 */

#include <owl/regexp.h>
#include <ipc/core.h>
#include <ipc/partition.h>
#include <oh/OHRootReceiver.h>
#include "HLTHistogramReceiver.h"
#include <oh/OHSubscriber.h>
#include "owl/semaphore.h"


class HLTRateAdapter {
 public:
  void Run(std::string provider_str,
	   std::string histogram_str);

  void Configure();
  HLTRateAdapter(std::string partition_name,
                 std::string server_nameIn,
		 std::string server_nameOut,
		 std::string name_out,
                 bool bool_correct=false,
		 bool get_rate=false,
		 bool transpose = false);          // Way of implementing the singleton pattern.
  ~HLTRateAdapter();    // Way of implementing the singleton pattern.
  HLTRateAdapter( const HLTRateAdapter & );     // Way of implementing the singleton pattern.

  void Stop();
  void signal_handler();

 private:


  std::string pPartitionName;
  std::string pServerNameIn;
  std::string pServerNameOut;
  std::string pNameOut;
  bool m_correct;
  bool m_get_rate;
  //  OHRootReceiver *mReceiver;
  // HLTReceiver *mReceiver;
  /*! Subscriber associated to the receiver. This OHS client is in charge of 
    actually subscribing to histograms. */
  //  OHSubscriber* m_subscriber;

  IPCPartition * mPartition;    // Partition object, provides connection to the IS.
  //  OWLRegexp m_provider;
  /*! Regular expression describing the set of histograms to subscribe to. */
  //  OWLRegexp m_histogram;
  HLTHistogramReceiver* Receiver;
  OHSubscriber* Ohhs;
  OWLRegexp*    Provider;
  OWLRegexp*    Histogram;
  bool m_transpose;
  OWLSemaphore*    semaphore; 


};

#endif
