#include "StreamsConfig.h"
#include "TRP/TriggerMenuHelpers.h"


bool StreamsConfig::SetConfig(unsigned int smk, unsigned int hlt_psk)
{
  Reset();
  
  confadapter_imp::TriggerMenuHelpers tmh(m_db_alias);
   
  try {
    std::cout << "attempting to fetch configuration corresponding to keys: "
	      << ", " << smk << ", " << hlt_psk << std::endl;
    
    tmh.getConfiguration(smk, hlt_psk, 0);
   }
  catch(std::runtime_error &e) {
    std::cout << "StreamsConfig::SetConfig failed to retrieve trigger configuration -- " << e.what() 
	      << std::endl;
    return false;
  }
  catch(...) {
    std::cout << "StreamsConfig::SetConfig failed to retrieve trigger configuration, unknown error" 
	      << std::endl;
    return false;
  }
  
  const confadapter_imp::TriggerMenuHelpers::Mapping & str_ef = tmh.getMapping("str_ef");
  
  const confadapter_imp::TriggerMenuHelpers::Mapping & ef_ps = tmh.getMapping("ef_ps");
  
   //std::cout << " str_ef map" << std::endl;
   //tmh.printMapping(str_ef);
  confadapter_imp::TriggerMenuHelpers::Mapping::const_iterator istr_ef;
  for(istr_ef = str_ef.begin(); istr_ef != str_ef.end(); istr_ef++) {
    
    const std::string &stream = istr_ef->first;
    const std::vector<std::string> &chains = istr_ef->second;
    
    const confadapter_imp::TriggerMenuHelpers::MapStrInt & strPrescaleMap = tmh.getStreamPrescalesMap( stream );
    
    if(strPrescaleMap.size() == 0) //Only put into m_streamChainMap if it appears in the stream prescale map 
      continue;

    for(std::vector<std::string>::const_iterator ichain = chains.begin(); 
	ichain != chains.end(); ichain++) {
      confadapter_imp::TriggerMenuHelpers::MapStrInt::const_iterator iPrescale 
	= strPrescaleMap.find(*ichain);
      float streamPrescale = 1;
      if(iPrescale != strPrescaleMap.end())
	streamPrescale = iPrescale->second;
      
      if(streamPrescale < 0)
	continue;
      
      confadapter_imp::TriggerMenuHelpers::Mapping::const_iterator i_ef_ps = ef_ps.find( *ichain );
      float chainPrescale = 0.;
      if( i_ef_ps != ef_ps.end() ) {
	std::stringstream ss(i_ef_ps->second[0]);
	ss >> chainPrescale;
      }
      
      StreamsConfig::ChainInfo strInfo(*ichain, chainPrescale, streamPrescale);
      m_streamChainMap[stream].push_back(strInfo);
      
    }
    
  }
  m_isValid = true;
  return true;
}

void StreamsConfig::Reset(){
  m_streamChainMap.clear();
  m_isValid = false;
}

void StreamsConfig::PrintMap() {

  std::cout << "Chain --> Stream map " << std::endl;

  for (std::map<std::string, std::vector<ChainInfo> >::const_iterator strit = m_streamChainMap.begin();
       strit != m_streamChainMap.end(); strit++) {

    std::cout << "Stream: " << strit->first << std::endl;

    const std::vector<ChainInfo> &chVec = strit->second;
    for(std::vector<ChainInfo>::const_iterator chit = chVec.begin(); chit != chVec.end(); chit++) {
      std::cout << "    " << chit->name << ", " 
		<< chit->chainPrescale << ", " 
		<< chit->streamPrescale << std::endl;
    }
  }

}

