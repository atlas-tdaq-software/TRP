 /**
    @file streamsMain.cpp

    streamsadapter extracts rate information from TRP's EF_rates
    objects for any streams for which stream prescales are defined and
    publishes for each such stream a TimePoint_IS containing the
    chains contributing to that stream, their prescales, their stream
    prescales and rates. Stream prescales are only defined for the
    Express stream so only one TimePoint_IS is actually made. (The
    code is more general than it has to be due to my misunderstanding
    of the requirements.)

    This file contains the main routine and three callback routines
    (and a few minor functions). At initialization, it subscribes for
    the smk, the HLT_psk and the EF_rates time point from TRP. The
    callbacks for smk and HLT_psk simply changes the current key
    values. The EF_rates callback checks for changes in the keys
    and if detected, fetches new trigger configuration info from the
    database. It then creates and publishes a new TimePoint_IS.

    @author M. Medinnis
 */

#include <iostream>
#include <sstream>
#include <stdexcept>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#include "ers/ers.h"

#include "config/Configuration.h"
#include "ipc/partition.h"
#include <ipc/core.h>
#include "boost/smart_ptr.hpp"

#include <is/info.h>
#include <is/infoT.h>
#include <is/inforeceiver.h>
#include "is/infodictionary.h"

#include "TRP/Args.h"
#include "TRP/ReadXML.h"
#include "TRP/Utils.h"
#include "TRP/TimePoint_IS.h"

#include "StreamsConfig.h"
#include "chainTP.h"
#include "ers/ers.h"

#include <signal.h>

static std::string partition_name;
static std::string trp_server;
static std::string ef_rates;
static std::string output_prefix = "STR_";
static std::string SMK_name;
static std::string HLT_PSK_name;
static std::string pgmName;
static std::string db_alias;
static std::string debug_string;

static bool debug_mode = false; 

static ISInfoReceiver *recSMK = 0;
static ISInfoReceiver *recHLTpsk = 0;
static ISInfoReceiver *recEF = 0;

static unsigned int current_SMK = 0, new_SMK = 0;
static unsigned int current_HLT_PSK = 0, new_HLT_PSK = 0;

static std::set<std::string> published; // for remembering which TimePoints have been published


static void UnsubscribeAll();
static void SubscribeAll();

/** Publish. Keep track of what is published so that it can be removed
    if no longer needed. */
static void Publish(const std::string streamName, const TimePoint_IS &tp)
{

  ISInfoDictionary dict(partition_name);

  std::string pubName = trp_server + "." + output_prefix + streamName;

  if(published.find(pubName) == published.end()) {
    //std::cout << "would have inserted " << pubName << std::endl;
    dict.insert(pubName, tp);
    published.insert(pubName);
  } else {
    //std::cout << "would have updated " << pubName << std::endl;
    dict.update(pubName, tp, true);
  }
}

/** Remove all publications */
static void RemoveAll() {

  ISInfoDictionary *dict;
  try {
    dict = new ISInfoDictionary(partition_name);
  } catch(ers::Issue & ex){
    ers::error(ex);
    throw;
  }
  for(std::set<std::string>::const_iterator sp = published.begin(); sp != published.end(); sp++) {
    //std::cout << "would have removed " << *sp << std::endl;
    try {
      dict->remove( *sp );
    } catch(ers::Issue & ex){
      ers::error(ex);
      throw;
    }
  }
  try {
    published.clear();
  } catch(ers::Issue & ex){
    ers::error(ex);
    throw;
  }
}

/** Fill info from ctps into tp */
static void FillTimePoint( TimePoint_IS &tp, const std::set<ChainTP> &ctps) {

  std::string yLabels[] = {"inRate", "chainPS", "outRate", "streamPS",  "rate_after_SPS", ""};
  static std::vector<std::string> yLabelsVec;
  static bool first = true;

  if(first) {
    first = false;
    for(int i = 0; yLabels[i] != ""; i++)
      yLabelsVec.push_back(yLabels[i]);
  }

  std::vector<std::string> xLabelsVec;

  for( std::set<ChainTP>::const_iterator ictp = ctps.begin(); ictp != ctps.end(); ictp++)
    xLabelsVec.push_back(ictp->GetName());

  tp.format(xLabelsVec, yLabelsVec);
  
  for( std::set<ChainTP>::const_iterator ictp = ctps.begin(); ictp != ctps.end(); ictp++) {
    tp.set(ictp->GetName(), yLabels[0], ictp->GetChainInRate());
    tp.set(ictp->GetName(), yLabels[1], ictp->GetChainPrescale());
    tp.set(ictp->GetName(), yLabels[2], ictp->GetRate()); 
    tp.set(ictp->GetName(), yLabels[3], ictp->GetStreamPrescale());
    float ps = ictp->GetStreamPrescale();
    if(ps > 0.1)
      tp.set(ictp->GetName(), yLabels[4], ictp->GetRate() / ictp->GetStreamPrescale());
    else
      tp.set(ictp->GetName(), yLabels[4], 0.);
  }

}

/** Print tp to std::cout */
void PrintTimePoint(TimePoint_IS &tp)
{
  std::cout << "                       ";
  for(std::vector<std::string>::const_iterator sit = tp.YLabels.begin(); sit != tp.YLabels.end(); sit++) {
    std::cout << *sit << "  ";
  }
  std::cout << std::endl;
  
  for(std::vector<std::string>::const_iterator xit = tp.XLabels.begin(); xit != tp.XLabels.end(); xit++) {
    std::cout << *xit << "  ";
    for(std::vector<std::string>::const_iterator yit = tp.YLabels.begin(); yit != tp.YLabels.end(); yit++) {
      float val;
      if( tp.get(*xit, *yit, val) )
	std::cout << val << "     ";
      else
	std::cout << "n/a     ";
    }
    std::cout << std::endl;
  }

}

/** 

    Called when a new EF_rates object is received. 

 */
static void callbackEFRates(ISCallbackInfo * isc){

  static StreamsConfig streamsConfig(db_alias);

  //std::cout << "callbackEFRates: " << isc->name() << std::endl;
  //std::cout << "Reason code : " << isc->reason() << std::endl;
  

  /** This probably never happens. */
  if(isc->reason() == is::Deleted) {
    // Take a break then try to subscribe again
    UnsubscribeAll();
    sleep(30);
    SubscribeAll();
  }
  
  /** All the work is done here. */
  
  /** First check if the configuration needs to be updated */
  if(current_SMK == 0 || current_SMK != new_SMK || 
     current_HLT_PSK == 0 || current_HLT_PSK != new_HLT_PSK) {
    
    if( !streamsConfig.SetConfig(new_SMK, new_HLT_PSK) )
      return;

    /** Only an Express stream is produced. Not much point in removing it, so disable. */
    // RemoveAll();

    streamsConfig.PrintMap();

  }
  current_SMK = new_SMK;
  current_HLT_PSK = new_HLT_PSK;
  
  
  if( !streamsConfig.IsValid() ) {
    std::cout << "Invalid configuration" << std::endl;
    return;
  }
  
  try {
    
    TimePoint_IS tp;
    isc->value( tp );

    const std::map<std::string, std::vector<StreamsConfig::ChainInfo> > &streamChainMap 
      = streamsConfig.GetMap();
    
    /** For each stream in the map, produce a TimePoint_IS */
    for (std::map<std::string, std::vector<StreamsConfig::ChainInfo> >::const_iterator strit 
	   = streamChainMap.begin();
	 strit != streamChainMap.end(); strit++) {

      /** This is mainly for sorting according to rate into stream */
      std::set<ChainTP> ctps; 

      /** Extract the rates for each chain in the stream */
      for( std::vector<StreamsConfig::ChainInfo>::const_iterator chit = strit->second.begin(); 
	   chit != strit->second.end(); chit++) {
	if(chit->chainPrescale < .999) continue; // Don't show inactive chains
	float inRate = 0., outRate = 0.;
	if( !tp.get(chit->name, "input", inRate)) {
	  std::cout << "Warning: no input rate found for " << chit->name << std::endl;
	}
	if (!tp.get(chit->name, "output", outRate)) {
	  std::cout << "Warning: no output rate found for " << chit->name << std::endl;
	}

	/** Constuct a ChainTP and insert into ctps */
	ChainTP aChain(chit->name, inRate, chit->chainPrescale, chit->streamPrescale, outRate);
	ctps.insert( aChain );
      }

      /** Fill time point and publish */
      TimePoint_IS streamTP;
      FillTimePoint( streamTP, ctps);

      /** Time stamp is copied from the EF_rates object */
      streamTP.TimeStamp = tp.TimeStamp;

      //std::cout << "Time point for " << strit->first << std::endl;
      //streamTP.print( std::cout );
      //PrintTimePoint(streamTP);

      Publish(strit->first, streamTP);
    }   
  }
  catch (ers::Issue & ex) {
    ers::error(ex);
    std::cout << "Update failed" << std::endl;
  }
}

static void callbackSMK(ISCallbackInfo * isc){
  std::cout << "callbackSMK: " << isc->name() << std::endl;
  std::cout << "Reason code : " << isc->reason() << std::endl;
  
  if(isc->reason() == is::Deleted) {
    new_SMK = 0;
    std::cout << "value set to " << new_SMK << std::endl;
    return;
  }
  
  ISInfoUnsignedInt smk;
  isc->value(smk);
  new_SMK = smk.getValue();
  
  std::cout << "value: " << new_SMK << std::endl;
}

static void callbackHLTpsk(ISCallbackInfo * isc){
  std::cout << "callbackHLTpsk: " << isc->name() << std::endl;
  std::cout << "Reason code : " << isc->reason() << std::endl;
  
  if(isc->reason() == is::Deleted) {
    new_HLT_PSK = 0;
    std::cout << "value set to " << new_SMK << std::endl;
    return;
  }
  
  ISInfoUnsignedInt hlt_psk;
  isc->value(hlt_psk);
  new_HLT_PSK = hlt_psk.getValue();

  std::cout << "value: " << new_HLT_PSK << std::endl;
}

static void Subscribe(ISInfoReceiver *receiver, 
		      const std::string &name, 
		      void (*callback)(ISCallbackInfo * isc))
{

  bool success = false;
  while (!success) {
    try {
      receiver->subscribe(name, callback );
      success = true;
    }
    catch(daq::is::RepositoryNotFound&) {
      std::cout << "Failed to subscribe to " << name << ", repository not found, try again in 10 sec" 
		<< std::endl;
    }
    catch(daq::is::AlreadySubscribed&) {
      std::cout << "Failed to subscribe to " << name << ", already subscribed, returning" << std::endl;
      success =  true; // assume it's okay
    }
    catch(daq::is::InvalidName&) {
      std::cout << "Failed to subscribe to " << name << ", invalid name, try again in 10 sec" << std::endl;
    }
    sleep(10);
  }
  
}

static void SubscribeAll() {

  // Subscribe for SMK updates
  std::cout << "Subscribing for SMK updates" << std::endl;
  Subscribe(recSMK, 
	    SMK_name, 
	    callbackSMK);

  // Subscribe for HLT prescale key updates

  std::cout << "Subscribing for HLT PSK updates" << std::endl;
  Subscribe(recHLTpsk, 
	    HLT_PSK_name, 
	    callbackHLTpsk);

  // Subscribe for EF rates
  std::cout << "Subscribing for EF rate updates" << std::endl;
  Subscribe(recEF, 
	    trp_server + "." + ef_rates,
	    callbackEFRates);

}

static void UnsubscribeAll() {
 // Remove subscriptions
  if(recHLTpsk != 0) {
    try {
      recHLTpsk->unsubscribe( HLT_PSK_name );
      delete recHLTpsk;
    }
    catch(ers::Issue & ex) {
      ers::error(ex);
      throw;
    }

  }

  if(recSMK != 0) {
    try { 
      recSMK->unsubscribe( SMK_name );
      delete recSMK;
    }
    catch(ers::Issue & ex) {
      ers::error(ex);
      throw;
    }

  }

  if(recEF != 0) {
    try {
      recEF->unsubscribe( trp_server + "." + ef_rates );
      delete recEF;
    }
    catch(ers::Issue & ex) {
      ers::error(ex);
      throw;
    }
  }

}

static void My_Stop(int sig){  
  std::cout << " I have received a SIG : " << sig << std::endl;
  if(sig==6){
    std::cout << "Kill Kill Bang Bang" << std::endl;
    exit(0);
  }
  try {
    UnsubscribeAll();
  } catch(...){
    exit(0);
  }
  try {
    RemoveAll();
  } catch(...){
    exit(0);
  }
  exit(0);
}

static void ReadConfig(std::string  config_file){
  
  std::cout << "ReadConfig(" << config_file << " ) " << std::endl;
  
  ReadXML my_reader;
  
  my_reader.SetElement("streamsadapter");
  
  std::vector<std::string> my_str;
  
  my_str.push_back("trp_server");
  my_str.push_back("ef_rates");
  my_str.push_back("output_prefix");
  my_str.push_back("debug_mode");
  my_str.push_back("db_alias");
  my_str.push_back("SMK_name");
  my_str.push_back("HLT_PSK_name");
  
  my_reader.SetAttribute(my_str);
  my_reader.readConfigFile(config_file);
  
  // partition_name= my_reader.GetValC("partition"); // it comes from the command line or environment.
  trp_server     = my_reader.GetVal("trp_server");
  ef_rates       = my_reader.GetVal("ef_rates");
  output_prefix  = my_reader.GetVal("output_prefix");
  db_alias       = my_reader.GetVal("db_alias");
  SMK_name       = my_reader.GetVal("SMK_name");
  HLT_PSK_name   = my_reader.GetVal("HLT_PSK_name");
  debug_string   = my_reader.GetVal("debug_mode");
  debug_mode = (debug_string == "true")? true : false;

}


void showHelp( std::ostream& os) {
  os << "Usage: " << pgmName << " options/args                                              " << std::endl;
  os << "                                                                                   " << std::endl;
  os << "Options/Arguments:                                                                 " << std::endl;
  os << "  -p partition -c <file.xml>   Read info from xml file instead                     " << std::endl;
  os << "                                                                                   " << std::endl;
  os << "Description:                                                                       " << std::endl;
  os << "   streamadapter produces timepoints containing chain rates for each active stream " << std::endl;
  os << "Debug level and verbosity:                                                         " << std::endl;
  os << "   They are controlled via the TDAQ_ERS shell variables:                           " << std::endl; 
  os << "     - $TDAQ_ERS_DEBUG_LEVEL        [0-4]                                          " << std::endl; 
  os << "     - $TDAQ_ERS_VERBOSITY_LEVEL    [0-3]                                          " << std::endl;
  os << "     - $TDAQ_ERS_FATAL              [eg: 'lsterr,throw']                           " << std::endl;   
  os << "     - $TDAQ_ERS_ERROR                                                             " << std::endl;
  os << "     - $TDAQ_ERS_WARNING                                                           " << std::endl;
  os << "     - $TDAQ_ERS_INFO               [eg: 'filter(efd),lstdout']                    " << std::endl;
  os << "     - $TDAQ_ERS_DEBUG                                                             " << std::endl;
  exit(1);
}


static bool GetISInfo(const ISInfoDictionary &dict, const std::string &infoName, ISInfo &info)
{
  try {
    dict.getValue( infoName, info );
  }
  catch (daq::is::InvalidName&) {
    std::cout << "GetISInfo: failed to get " << infoName << ": InvalidName" << std::endl;
    return false;
  }
  catch (daq::is::RepositoryNotFound&) {
    std::cout << "GetISInfo: failed to get " << infoName << ": Repository not found" << std::endl;
    return false;
  }
  catch (daq::is::InfoNotFound&) {
    std::cout << "GetISInfo: failed to get " << infoName << ": Info not found" << std::endl;
    return false;
  }
  catch (daq::is::InfoNotCompatible&) {
    std::cout << "GetISInfo: failed to get " << infoName << ": Info not compatible" << std::endl;
    return false;
  }
  return true;
}

/** The keys are needed at startup */
static bool getKeys(unsigned int &smk, unsigned int &hlt_psk) {

  ISInfoDictionary dict(partition_name);

  ISInfoAny keyInfo;

    if (!GetISInfo(dict, SMK_name, keyInfo)) {
      std::cout << "getKeys: SMK not available at startup" << std::endl;
      smk = 0;
      return false;
    } else if( keyInfo.type().entryType( 0 ) != ISType::U32 ) {
      std::cout << "getKeys: unexpected type for SMK" << std::endl;
      return false;
    }

    keyInfo >> smk;

    if (!GetISInfo(dict, HLT_PSK_name, keyInfo)) {
      std::cout << "getKeys: HLT PSK not available at startup" << std::endl;
      smk = 0;
      return false;
    } else if( keyInfo.type().entryType( 0 ) != ISType::U32 ) {
      std::cout << "getKeys: unexpected type for HLT_PSK" << std::endl;
      return false;
    }

    keyInfo >> hlt_psk;

    return true;
}


int main(int ac, char* av[] ){
  
  signal(SIGABRT, &My_Stop);
  signal(SIGTERM, &My_Stop);
  signal(SIGINT,  &My_Stop);
  
  // get arguments
  Args args( ac, av );
  
  // get program name
  
  pgmName = av[0];
  
  std::string::size_type pos = pgmName.rfind( '/' );
  if( pos != std::string::npos )
    pgmName = pgmName.substr( pos+1 );
  
  
  // check if requested help
  if( args.hasArg("-h")     || 
      args.hasArg("-?")     || 
      args.hasArg("--help") || 
      ac <= 1  )
    { showHelp( std::cerr); }
  
  // reconstruct command line string for debugging feedback
  std::stringstream cmdLine;
  cmdLine << av[0];
  
  for( int i = 1; i < ac; ++i )
    cmdLine << " " << av[i];
  
  std::cout << "cmdline: '" << cmdLine.str() << "'" << std::endl;
  
  
  // Retrieve the partition name from command line or environment 
  if (args.hasArg("-p")) {
    partition_name = args.getStr("-p");
    ERS_DEBUG(0, "Retrieved partition name from command line: '" << partition_name << "'");
    std::cout << "Retrieved partition name from command line: '" << partition_name << std::endl;
  } else if (getenv("TDAQ_PARTITION")) {
    partition_name = getenv("TDAQ_PARTITION");
    ERS_DEBUG(0, "Retrieved partition name from environment: '" << partition_name << "'");
    std::cout << "Retrieved partition name from environment: '" << partition_name << std::endl;
  } else {
    std::cerr << "Partition name not set (neither in command line nor in environment)" << std::endl;
    showHelp( std::cerr );
  }
  
  std::string config_file;
  
  if (args.hasArg("-c")) {
    config_file  = args.getStr("-c");
    ERS_DEBUG(0, "Config file from  command line: '" << config_file  << "'");
    std::cout<< "Config file from  command line: '" << config_file  << std::endl;
    ReadConfig(config_file);
  } else {
    showHelp(std::cerr);
  } // end else read config
  
  std::cout << "Streams Adapter configuration:" << std::endl;
  std::cout << "    Partition:              " << partition_name  << std::endl;
  std::cout << "    TRP server:             " << trp_server << std::endl; 
  std::cout << "    EF rates name:          " << ef_rates << std::endl; 
  std::cout << "    Output prefix:          " << output_prefix << std::endl; 
  std::cout << "    debug_mode:             " << debug_mode << std::endl;
  std::cout << "    db_alias:               " << db_alias << std::endl;
  std::cout << "    SMK_name:               " << SMK_name << std::endl;
  std::cout << "    HLT_PSK_name:           " << HLT_PSK_name << std::endl;
  
  ERS_DEBUG(0, "Starting program job" );
    
    
  IPCCore::init( ac, av );
  IPCPartition partition(partition_name);
  
  // Create IS receiver instances 
  recSMK    = new ISInfoReceiver (partition_name);
  recHLTpsk = new ISInfoReceiver (partition_name);
  recEF     = new ISInfoReceiver (partition_name);
    
    
  // get the current values of the SMK and PSK if available
  getKeys(new_SMK, new_HLT_PSK);
  std::cout << "keys after fetching from IS: " << new_SMK << ", " << new_HLT_PSK << std::endl;

  SubscribeAll();

  // call method run to block the current thread until
  // somebody will call the stop for the rec object
  std::cout << "About to call recEF.run" << std::endl;
  //recEF->run();
  
  
  return 0;
}

