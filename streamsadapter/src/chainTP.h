 /**
    @file chainTP.h

    ChainTP holds chain specific data which will be inserted into a
    TimePoint_IS object. The main purpose is to provide for sorting.
    ChainTP is fully defined in this file. There is no corresponding
    .cpp file.

    @author M. Medinnis
 */

#ifndef _CHAINTP_H_
#define _CHAINTP_H_

class ChainTP {

 public:
  /** Consructor. */
  ChainTP(const std::string name, float inRate, float chainPrescale, float streamPrescale, float rate) :
    m_name(name), 
    m_inRate(inRate), 
    m_chainPrescale(chainPrescale), 
    m_streamPrescale(streamPrescale), 
    m_rate(rate)
    {;}

    const std::string &GetName() const {return m_name;}
    float GetChainInRate() const {return m_inRate;}
    float GetChainPrescale() const {return m_chainPrescale;}
    float GetStreamPrescale() const {return m_streamPrescale;}
    float GetRate() const {return m_rate;}

    /** Sort order: by decreasing output rate or, for a tie, alphabetically by chain name */
    bool operator< (const ChainTP & rhs) const {
      float preRate = 0.;
      if(m_streamPrescale > 0.1)
	preRate = m_rate / m_streamPrescale;

      float rhsPreRate = 0.;
      if( rhs.m_streamPrescale > 0.1)
	rhsPreRate = rhs.m_rate / rhs.m_streamPrescale;
      
      if( preRate > rhsPreRate ) return true;
      if( preRate < rhsPreRate ) return false;
      return m_name < rhs.m_name;
    }
    
 private:
    std::string m_name;
    float m_inRate;
    float m_chainPrescale;
    float m_streamPrescale;
    float m_rate;
};

#endif
