 /**
    @file StreamsConfig.h

    Fetch from DB and hold on to the needed trigger configuration
    info. Two classes are defined: StreamsConfig and
    StreamsConfig::ChainInfo.


    @author M. Medinnis
 */

#ifndef _STREAMS_CONFIG_H
#define _STREAMS_CONFIG_H

#include <string>
#include <map>
#include <vector>

/** Maintains a map of stream name --> vector of ChainInfo objects
    containing entries for each chain in corresponding stream. */
class StreamsConfig {

 public:

  class ChainInfo {
  public:
    ChainInfo(const std::string &c_name, float c_chainPrescale, float c_streamPrescale) : 
      name(c_name), chainPrescale(c_chainPrescale), streamPrescale(c_streamPrescale) {;}
      /** chain name */
      std::string name; 

      /** global chain prescale */
      float chainPrescale;

      /** chain prescale for a particular stream */
      float streamPrescale;
  };
  
  /** Constuctor */
  StreamsConfig(const std::string &db_alias) : m_db_alias(db_alias), m_isValid(false) {;}

  const std::map<std::string, std::vector<ChainInfo> > &GetMap() const {return m_streamChainMap;}

  /** Retrieve configuration corresponding to smk and hlt_psk from DB */
  bool SetConfig(unsigned int smk, unsigned int hlt_psk);

  /** Returns true if the map was properly set up */
  bool IsValid() const {return m_isValid;}

  /** Print map */
  void PrintMap();

  /** Clean out the map. (Called at beginning of SetConfig */
  void Reset();

 private:

  std::string m_db_alias;
  std::map<std::string, std::vector<ChainInfo> > m_streamChainMap; // for each chain, its streams and prescales
  bool m_isValid;

};

#endif
