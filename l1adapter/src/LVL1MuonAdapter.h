/** 
 * @file LVL1RateAdapter.h
 * @author Antonio Sidoti
 * @date 28/11/2008
 * @brief Triger Level 1 classes for obtaining the data from IS.
 * @This reads from LVL1 IS and stores in TripIS
 * @Inspired from TDataManagerLvl1.h and .cpp
 */
 
#ifndef lvl1muonadapter_h
#define lvl1muonadapter_h


#include <iostream>

#include <ctime>

#include <vector>
#include <deque>
#include <utility>
#include <algorithm>
#include <string>
#include <set>

#include <ipc/core.h>
#include <ipc/partition.h>

#include "unistd.h"
#include "stdio.h"
#include "signal.h"


#include "is/infoany.h"
#include <is/infodictionary.h>
#include <is/infoT.h>
#include <is/info.h>
#include <is/infoiterator.h>
#include <is/inforeceiver.h>
#include <is/serveriterator.h>
#include "is/infodocument.h"
#include <is/criteria.h>

#include "TRP/RPCL1Rates.h"
#include "TRP/TimePoint_IS.h"
#include "TRP/TimePointAsyncSender.h"
#include "TRP/ISCTPBUSY.h"
#include "TRP/SFIRate.h"
#include "TRP/SFORate.h"
#include "TRP/ISMIOCTSectorRun1.h"
#include "TRP/L1Rates.h"
#include "TRP/L1CaloRates.h"

using std::string;
using std::vector;
using std::deque;
using std::pair;
using std::cout;
using std::cerr;
using std::endl;
using std::set;



/**
 * @brief LVL1MuonAdapter reads LVL1 rates and publish them in trpIS.
 */
class LVL1MuonAdapter {
  
 public:
  
  void Configure();
  
  void Run();


  LVL1MuonAdapter(std::string partition_name, std::string server_name,
		  std::string name_out, std::string mode, std::string mode_sfo);   // Way of implementing the singleton pattern.
  ~LVL1MuonAdapter();	// Way of implementing the singleton pattern.
  LVL1MuonAdapter( const LVL1MuonAdapter & );	// Way of implementing the singleton pattern.

  
 private:
  
  
  std::string pPartitionName;
  std::string pSubscription;
  std::string pServerName;
  std::string pNameOut;
  std::string m_mode;
  std::string m_mode_rec;
  IPCPartition * mPartition;	// Partition object, provides connection to the IS.
  ISInfoReceiver * mReceiver;	// Used for callback registration.
  TimePoint_IS m_mu_point;
  std::vector<std::string>  mSectorNames;
  std::vector<std::string>  myXaxis; 
  std::vector<std::string>  myYaxis; 
  std::set<std::string>  myXaxis_set; 
  std::set<std::string>  myYaxis_set; 
  
  RPCL1Rates my_RPCRates;
  SFIRate my_SFIRate;
  SFORate my_SFORate;
  ISCTPBUSY my_Busy;
  ISMIOCTSectorRun1 my_mioct;
  L1Rates my_l1rate;
  L1CaloRates my_l1calorate;
  ISInfoAny my_isa_tgc;
  
  ISCriteria *m_criteria;
  ISCriteria *m_criteria2;

  void ConfigureRPC();
  void ConfigureL1Calo();
  void ConfigureBusy();
  void ConfigureSFI();
  void ConfigureSFO();
  void ConfigureMIOCT();
  void ConfigureLUCID();
  void ConfigureL1TGC();
  
  void SetTimePoint(RPCL1Rates my_rpc);  
  void SetTimePoint(ISCTPBUSY my_busy);
  void SetTimePoint(SFIRate my_busy,std::string name);
  void SetTimePoint(SFORate my_busy,std::string name);
  void SetTimePoint(ISMIOCTSectorRun1 my_mioct);
  void SetTimePoint(L1Rates my_lucid);
  void SetTimePoint(L1CaloRates my_l1calo);
  void SetTimePoint(std::string mode);
  
  bool GetSFName(std::string is_name, std::string & sf_name, 
		 std::string & stream_name, std::string tag="StreamInfo");

  bool GetCaloName(std::string name, std::string & lab_name, 
		   bool fold);
  
  bool GetTGCName(std::string name, 
		  std::string & x_label, std::string & y_label);

};
  


#endif


