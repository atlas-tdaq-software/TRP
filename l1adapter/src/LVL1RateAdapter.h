/** 
 * @file LVL1RateAdapter.h
 * @author Antonio Sidoti
 * @date 28/11/2008
 * @brief Triger Level 1 classes for obtaining the data from IS.
 * @This reads from LVL1 IS and stores in TripIS
 * @Inspired from TDataManagerLvl1.h and .cpp
 */
 
#ifndef lvl1rateadapter_h
#define lvl1rateadapter_h


#include <iostream>

#include <ctime>

#include <vector>
#include <deque>
#include <map>
#include <utility>
#include <algorithm>
#include <string>

#include <ipc/core.h>
#include <ipc/partition.h>

#include "unistd.h"
#include "stdio.h"
#include "signal.h"


#include "is/infoany.h"
#include <is/infodictionary.h>
#include <is/infoT.h>
#include <is/info.h>
#include <is/infoiterator.h>
#include <is/inforeceiver.h>
#include <is/serveriterator.h>
#include "is/infodocument.h"

//#include <CTPCOREModule/IS_LB.h>
#include "TRP/IS_LB.h"
#include "TRP/TrigConfL1Deadtimes.h"
#include "TRP/ISPrescalesAPTool.h"
#include "TRP/TrigConfL1Items.h"
#include "TRP/TimePoint_IS.h"
#include "TRP/L1_Rate.h"
#include "TRP/TimePointAsyncSender.h"
#include "TRP/ISCTPBUSY.h"
#include "TRP/DeadtimeMon.h"
#include "TRP/LumiRaw.h"
#include "TRP/L1Rates.h"
#include "TRP/CtpinRateInfo.h"
#include "TRP/CtpinRateInfoCounter.h"
#include "TRP/CtpcoreTriggerRateInfo.h"
#include "TRP/ISMIOCTSectorContainer.h"
#include "TRP/ISMIOCTSector.h"

#include "owl/semaphore.h"

using std::string;
using std::vector;
using std::deque;
using std::pair;
using std::cout;
using std::cerr;
using std::endl;

#define use_Obsolete

//class trp;


/**
 * @brief LVL1RateAdapter reads LVL1 rates and publish them in trpIS.
 */
class LVL1RateAdapter {
  //  friend class trp;
  // friend class TDataManager;
  
 public:
  
  void Configure();
  
  void Run();
  void Stop();
//  void setLB(IS_LB my_is_lb, TimePoint_ISNamed *my_timepoint); 
  void setLB(IS_LB my_is_lb, TimePoint_IS &my_timepoint); 

  LVL1RateAdapter(std::string partition_name, std::string server_name,
		  std::string APserver_name,
		  std::string l1confkeys, std::string name_out,
		  std::string mode,
		  std::string partition_name_out,
		  std::string object_name,
		  float my_time = 8., bool do_acc = false);   // Way of implementing the singleton pattern.
  
  ~LVL1RateAdapter();	// Way of implementing the singleton pattern.
  LVL1RateAdapter( const LVL1RateAdapter & );	// Way of implementing the singleton pattern.

  void signal_handler();
  
 private:
  
  
  std::string pPartitionName;
  std::string pPartitionNameOut;
  std::string pSubscription;
  std::string pServerName;
  std::string pPrescaleAP_server;
  std::string pNameOut;
  std::string m_object_name;
  bool m_do_acc;
  IPCPartition * mPartition;	// Partition object, provides connection to the IS.
  IPCPartition * mPartitionOut;	// Partition object, provides connection to the IS

  ISInfoReceiver   receiver;
  ISInfoReceiver * Receiver;
  OWLSemaphore*    semaphore { nullptr }; 
 
  std::string  l1conf_pattern;
//  TimePoint_ISNamed *mlvl1_point;
  TimePoint_IS mlvl1_point, mlvl1_old_point;
  //  ISInfoDictionary *m_dict;
  //TimePointAsyncSender *m_sender;
  L1_Rate my_l1_rate;
  IS_LB my_IS_LB, my_old_IS_LB;
  L1CT::CtpinRateInfo my_ctprateinfo;
  ISPrescalesAPTool my_IS_LB_AP;
  TrigConfL1Deadtimes IS_Deadtimes;
  TrigConfL1Items info;
  ISCTPBUSY my_Busy;
  DeadtimeMon my_dt_mon;
  TimePoint_IS my_tp_mon;
  ISInfoAny my_isa;
  LumiRaw my_lumraw;
  L1Rates my_l1rate;
  TimePoint_IS my_l1point;
  double m_lumid;
  float mbts_LumZeroAndInst,mbts_LumZeroAnd;
  std::string m_mode;
  float m_min_time;
  std::vector <std::string> m_object_name_vec;
  
  bool m_updated;
  std::map<std::string,bool> updated_map;
  bool m_has_old_lvl1_point;
  bool old_LB;
  bool isNewLB;
  bool updated_flag[3];
  
  void LVL1callback_Lum(ISCallbackInfo *isc);
  void LVL1callback_rates(ISCallbackInfo *isc);
  void LVL1callback_CTPIN(ISCallbackInfo *isc);
  void LVL1callback_config(ISCallbackInfo *isc);
  void LVL1callback_APrates(ISCallbackInfo *isc);
  void LVL1callback_Busy(ISCallbackInfo *isc);
  void LVL1callback_DT(ISCallbackInfo *isc);
  void LVL1callback_TP(ISCallbackInfo *isc);
  void LVL1callback_mbtslum(ISCallbackInfo *isc);
  void LVL1callback_Lucid(ISCallbackInfo *isc);
  void LVL1callback_LucidL1(ISCallbackInfo *isc);
  void LVL1callback_DQM(ISCallbackInfo *isc);
  void L1CTcallback_CTPCORE(ISCallbackInfo *isc);
  void L1CTcallback_CTPCORE_Run3(ISCallbackInfo *isc);
  void L1CTcallback_TrigConfig(ISCallbackInfo *isc);
  void L1CTcallback_MIOCT(ISCallbackInfo *isc);
  void publishIS();
  void ReadConfL1();
  void ReadConfCTPIN();
  std::vector<int>  mPSfactor;
  std::vector<int>  mPSfactor_AP;
  std::vector<std::string>  mL1Names;
  int simple_deadtime;
  bool IsNewLB(IS_LB is_lb, IS_LB old_is_lb);
//  void CopyNewOld();
  
  bool GetRates(IS_LB is_lb, IS_LB old_is_lb);
  void RatesClear();  
  std::vector<float> rates_TBP;
  std::vector<float> rates_TAP;
  std::vector<float> rates_TAV;
  std::vector<float> mDeadTimeFrac;
  float rates_L1A_TBP;
  float rates_L1A;
  float rates_L1A_AP;
  time_t time_stamp;
  float deadtime_corr;
  unsigned short test_time_point_lb;
  
  std::vector<float> old_rates_TBP;
  std::vector<float> old_rates_TAP;
  std::vector<float> old_rates_TAV;
  float old_rates_L1A;
  time_t old_time_stamp;
  unsigned int old_turn;
  double old_count_acc[4];
  
  std::vector<int> items_to_check;

  float m_variation {0.1};

  int m_time {300};
  unsigned int m_smk, m_hltk, m_l1k, m_bgk, m_smk_old;
  double m_var1, m_var2, m_var3;
  unsigned int m_var4, m_var5;
  time_t GetExactTime(IS_LB my_LB);
  bool m_is_first_event;
  int m_num_evts;
  bool m_is_publishing;

  L1CT::CtpcoreTriggerRateInfo my_rateinfo;
  std::vector<L1CT::CtpcoreItemRateInfo> my_items;
  OWLTime ctpcore_newtime;
  OWLTime ctpcore_oldtime;

  L1CT::ISMIOCTSectorContainer mioct_rateinfo;
  OWLTime mioct_newtime;
  OWLTime mioct_oldtime;

};

#endif
