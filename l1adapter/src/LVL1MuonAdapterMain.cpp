// ------------------------------
// runs the various trp adapters that store info into the trpIS 
// stolen from DAQ/DataFlow/efd/src/efd.cxx
// Author: Antonio Sidoti - Humboldt Universitat zu Berlin
// 
// Date: 01/12/2008
// ------------------------------


#include <iostream>


#include "ers/ers.h"
#include "TRP/Args.h"

#include "config/Configuration.h"
#include "ipc/partition.h"
#include <ipc/core.h>
#include "boost/smart_ptr.hpp"
#include "LVL1MuonAdapter.h" 
#include "TRP/ReadXML.h"
#include "TRP/Utils.h"
#include <fstream>
#include <signal.h>

#undef TRP_DEBUG
#define TRP_DEBUG(lvl,msg) //cout << lvl << " ** " << msg << endl


#define DBG(msg) //std::cerr << msg << endl;


std::string partition_name;
std::string server_name;
std::string APserver_name;
std::string l1confkey;
std::string name_out = "L1_Rate_Muon";
int rate_update = 60;
std::string mode = "L1_RPC";
std::string mode_rec = "rec";

void ReadConfig(std::string  config_file){
  std::cout << "ReadConfig(" << config_file << " ) " << std::endl;

  
  ReadXML my_reader;
  my_reader.SetElement("l1muonadapter");
  std::vector<std::string> my_str;
  my_str.push_back("outputname");
  my_str.push_back("rate_update");
  my_str.push_back("mode");
  my_str.push_back("modeSFO");
  my_reader.SetAttribute(my_str);

  my_reader.readConfigFile(config_file);
  
  //  partition_name = my_reader.GetValC("partition");
  server_name = my_reader.GetValC("server");
  
  name_out    = my_reader.GetVal("outputname");
  mode        = my_reader.GetVal("mode");
  rate_update = trp_utils::convertToInt(my_reader.GetVal("rate_update"));
  mode_rec    = my_reader.GetVal("modeSFO");
}


void showHelp( std::ostream& os, std::string pgmName ){
  os << "Usage: " << pgmName << " [-p <str>]  [-c <file.xml>] [-s <str>] [-out <str>]" << std::endl;
  os << "                                                                      " << std::endl;
  os << "Options/Arguments:                                                    " << std::endl;
  os << "  -p <str>   partition name  (if not present $TDAQ_PARTITION is used) " << std::endl; 
  os << "  -c <file1.xml:file2.xml:...>   reads info from xml file instead. Picks info from existing file" << std::endl;
  os << "                                                                      " << std::endl;
  os << "  or in alternative from the command line as follows     " << std::endl;
  os << "  -s <str>   server name                                              " << std::endl; 
  os << "  -out <str> outputName of TimePoint_IS                                " << std::endl;
  os << "Description:                                                          " << std::endl;
  os << "   The LVL1RateAdapterMain gets infos (IS_LB,...) from IS             " << std::endl;
  os << "   reads LVL1 configuration at each LumiBlock, performs rate          " << std::endl; 
  os << "   callculatiom and store in IS TimePoint variable with               " << std::endl; 
  os << "   dynamical time intervals.                                          " << std::endl;
  os << "                                                                      " << std::endl;
  os << "Debug level and verbosity:                                            " << std::endl;
  os << "   They are controlled via the TDAQ_ERS shell variables:              " << std::endl; 
  os << "     - $TDAQ_TRP_DEBUG_LEVEL        [0-4]                             " << std::endl; 
  os << "     - $TDAQ_TRP_VERBOSITY_LEVEL    [0-3]                             " << std::endl;
  os << "     - $TDAQ_TRP_FATAL              [eg: 'lsterr,throw']              " << std::endl;   
  os << "     - $TDAQ_TRP_ERROR                                                " << std::endl;
  os << "     - $TDAQ_TRP_WARNING                                              " << std::endl;
  os << "     - $TDAQ_TRP_INFO               [eg: 'filter(efd),lstdout']       " << std::endl;
  os << "     - $TDAQ_TRP_DEBUG                                                " << std::endl;
  exit(1);
}

void My_Stop(int sig){  
  std::cout << " I have received a SIG : " << sig << std::endl;
  std::cout << "Kill Kill Bang Bang" << endl;
  exit(0);
  IPCPartition mPartition(partition_name);
  ISInfoReceiver rec(mPartition);
  if(mode == "L1_RPC" || mode == "LUCID"){
    try {
      rec.unsubscribe("L1TriggerRates");
    } catch(...){
      std::cout << " Cannot unsubscribe to L1TriggerRates  " << std::endl;
    }
  }
  if(mode == "MIOCT"){
    try {
      rec.unsubscribe("L1CT");
    } catch(...){
      std::cout << " Cannot unsubscribe to L1CT  " << std::endl;
    }
  }
  if(mode == "BUSY"){
    try {
      rec.unsubscribe("L1CT-History");
    } catch(...){
      std::cout << " Cannot unsubscribe to L1CT-History  " << std::endl;
    }
  }
  if(mode == "SFI" || mode == "SFO") {
    try {
      rec.unsubscribe("DF");
    } catch(...){
      std::cout << " Cannot unsubscribe to DF  " << std::endl;
    }
  }
  std::cout << "Now exiting" <<  std::endl;
  exit(0);
}


int main( int ac, char* av[] ){
  IPCCore::init( ac, av );
  signal(SIGABRT, &My_Stop);
  signal(SIGTERM, &My_Stop);
  signal(SIGINT,  &My_Stop);
  
  try{
    
    // get arguments
    Args args( ac, av );
    
    // get program name
    std::string pgmName = av[0];
    std::string::size_type pos = pgmName.rfind( '/' );
    if( pos != std::string::npos )
      pgmName = pgmName.substr( pos+1 );
    

    // check if requested help
    if( args.hasArg("-h")     || 
        args.hasArg("-?")     || 
        args.hasArg("--help") || 
        ac <= 1  )
//           || 
//	!args.hasArg("-n"))
      { showHelp( std::cerr, pgmName ); }
    
    // reconstruct command line string for debugging feedback
    std::stringstream cmdLine;
    cmdLine << av[0];
    for( int i = 1; i < ac; ++i )
      cmdLine << " " << av[i];
    std::cout << "cmdline: '" << cmdLine.str() << "'" << std::endl;
    
    // Retrive the partition name (from command line or from environment) 
    if (args.hasArg("-p")) {
      partition_name = args.getStr("-p");
      TRP_DEBUG(0, "Retrieved partition name from command line: '" << partition_name << "'");
      std::cout << "Retrieved partition name from command line: " << partition_name << std::endl;
    } else if (getenv("TDAQ_PARTITION")) {
      partition_name = getenv("TDAQ_PARTITION");
      TRP_DEBUG(0, "Retrieved partition name from environment: '" << partition_name << "'");
    } else {
      std::cerr << "Partition name not set (neither in command line nor in environment)" << std::endl;
      showHelp( std::cerr, pgmName );
    }
    
    std::string config_file;
    if (args.hasArg("-c")) {
      config_file  = args.getStr("-c");
      ERS_DEBUG(0, "Config file from  command line: '" << config_file  << "'");
      std::cout<< "Config file from  command line: " << config_file  << std::endl;
      std::vector<std::string> file_list; 
      //      for (unsigned int i = 0; i < config_file.size(); ++i) {
      //      file_list = trp_utils::split(config_file,"\:");
      trp_utils::tokenize(config_file,file_list,":");
      // }
      std::cout << "File_list size is:" << file_list.size() << std::endl;
      for(std::vector<std::string>::iterator it=file_list.begin();
	  it!=file_list.end();++it){
	const char* it_cstr = it->c_str();
	std::ifstream my_file(it_cstr);
	if(my_file.is_open()){
	  // here reads the file .xml
	  ReadConfig(*it);
	  break;
	}
      }
    } else {
      
      // retrieve ther Server Name
      if (args.hasArg("-s")) {
	server_name = args.getStr("-s");
	TRP_DEBUG(0, "Retrieved server name from command line: '" << server_name << "'");
      } else {
	std::cerr << "Server name not set in command line" << std::endl;
	showHelp( std::cerr, pgmName );
      }
      if (args.hasArg("-out")) {
	name_out = args.getStr("-out");
	TRP_DEBUG(0, "Retrieved outputname from command line: '" << name_out << "'");
      }
    }
    
    // Dumping config
    std::cout << "Dumping COnfigurations for LVL1MuonAdapter:" << std::endl;
    std::cout << partition_name << " ,  " << server_name << " , " << l1confkey << "  outputnme:" << name_out << std::endl;
    std::cout << "config_file = " << config_file << std::endl;

    TRP_DEBUG(0, "Starting program job" );
    LVL1MuonAdapter my_lvl1_adapter(partition_name,server_name,name_out,mode,mode_rec);
 
    my_lvl1_adapter.Configure();
    while(1){
      my_lvl1_adapter.Run();
      sleep(rate_update);
    }
    TRP_DEBUG(0, "Program terminating gracefully" );
  }
  catch( ers::Issue &ex)   { TRP_DEBUG( CoreIssue, "Caught ers::Issue: " << ex ); }
  catch( std::exception& e) { TRP_DEBUG( CoreIssue, "Caught std::exception: " << e.what() ); }
  catch( ... )             { TRP_DEBUG( CoreIssue, "Caught unidentified exception" );
  }
  return 0;
}
