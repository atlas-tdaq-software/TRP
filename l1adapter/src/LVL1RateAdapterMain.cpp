// ------------------------------
// runs the various trp adapters that store info into the trpIS 
// stolen from DAQ/DataFlow/efd/src/efd.cxx
// Author: Antonio Sidoti - Humboldt Universitat zu Berlin
// 
// Date: 01/12/2008
// ------------------------------


#include <iostream>
#include <is/inforeceiver.h>


#include "ers/ers.h"
#include "TRP/Args.h"

#include "config/Configuration.h"
#include "ipc/partition.h"
#include <ipc/core.h>
#include "boost/smart_ptr.hpp"
#include "LVL1RateAdapter.h" 
#include "TRP/ReadXML.h"
#include "TRP/Utils.h"
#include <fstream>
#include <signal.h>

std::string partition_name; // this is where you read it should be the same of partition_name_out except ehn you read from different partitions 
std::string server_name;
std::string APserver_name;
std::string l1confkey;
std::string name_out = "L1_Rate";
std::string mode = "L1";
std::string partition_name_out;
float min_time = 8.;
std::string object_name = "OCApp/ATL_instLum";
bool do_acc=false;

namespace
{
  LVL1RateAdapter* my_lvl1_adapter = 0;
  int ReceivedSignal = 0;
  void signal_handler( int sig )
  {
    if (my_lvl1_adapter != 0) {
      ReceivedSignal = sig;
      my_lvl1_adapter->signal_handler();
    }
  }
}

void ReadConfig(std::string  config_file){
  std::cout << "ReadConfig(" << config_file << " ) " << std::endl;

  
  ReadXML my_reader;
  my_reader.SetElement("l1adapter");
  std::vector<std::string> my_str;
  my_str.push_back("l1k");
  my_str.push_back("outputname");
  my_str.push_back("APServerName");
  my_str.push_back("mode");
  my_str.push_back("min_time");
  my_str.push_back("object_name");
  my_str.push_back("do_acc");

  my_reader.SetAttribute(my_str);

  my_reader.readConfigFile(config_file);
  
  //  partition_name = my_reader.GetValC("partition");
  server_name = my_reader.GetValC("server");
  
  l1confkey = my_reader.GetVal("l1k");
  name_out  = my_reader.GetVal("outputname");
  APserver_name  = my_reader.GetVal("APServerName");
  mode = my_reader.GetVal("mode");
  min_time = atof(my_reader.GetVal("min_time").c_str());
  object_name = my_reader.GetVal("object_name");
  std::string do_acc_str = my_reader.GetVal("do_acc");
  do_acc = (do_acc_str=="1") ? true : false ;
}


void showHelp( std::ostream& os, std::string pgmName ){
  os << "Usage: " << pgmName << " [-p <str>]  [-c <file.xml>] [-s <str>] [-k <str>] [-out <str>]" << std::endl;
  os << "                                                                      " << std::endl;
  os << "Options/Arguments:                                                    " << std::endl;
  os << "  -p <str>   partition name  (if not present $TDAQ_PARTITION is used) " << std::endl; 
  os << "  -c <file1.xml:file2.xml:...>   reads info from xml file instead. Picks info from existing file" << std::endl;
  os << "                                                                      " << std::endl;
  os << "  or in alternative from the command line as follows     " << std::endl;
  os << "  -s <str>   server name                                              " << std::endl; 
  os << "  -k <str>   L1 Regular expression                                    " << std::endl;
 os << "  -out <str> outputName of TimePoint_IS                                " << std::endl;
  os << "Description:                                                          " << std::endl;
  os << "   The LVL1RateAdapterMain gets infos (IS_LB,...) from IS             " << std::endl;
  os << "   reads LVL1 configuration at each LumiBlock, performs rate          " << std::endl; 
  os << "   callculatiom and store in IS TimePoint variable with               " << std::endl; 
  os << "   dynamical time intervals.                                          " << std::endl;
  exit(1);
}


int main( int ac, char* av[] ){
  IPCCore::init( ac, av );
  try{
    
    // get arguments
    Args args( ac, av );
    
    // get program name
    std::string pgmName = av[0];
    std::string::size_type pos = pgmName.rfind( '/' );
    if( pos != std::string::npos )
      pgmName = pgmName.substr( pos+1 );
    

    // check if requested help
    if( args.hasArg("-h")     || 
        args.hasArg("-?")     || 
        args.hasArg("--help") || 
        ac <= 1  )
//           || 
//	!args.hasArg("-n"))
      { showHelp( std::cerr, pgmName ); }
    
    // reconstruct command line string for debugging feedback
    std::stringstream cmdLine;
    cmdLine << av[0];
    for( int i = 1; i < ac; ++i )
      cmdLine << " " << av[i];
    //std::cout << "cmdline: '" << cmdLine.str() << "'" << std::endl;
    ERS_LOG( "cmdline '" << cmdLine.str() << "'" );   
 
    // Retrive the partition name (from command line or from environment) 
    if (args.hasArg("-p")) { // from where you read
      partition_name = args.getStr("-p");
      ERS_DEBUG(0, "Retrieved partition name from command line: '" << partition_name << "'");
      std::cout << "Retrieved partition name from command line: " << partition_name << std::endl;
    } else if (getenv("TDAQ_PARTITION")) {
      partition_name = getenv("TDAQ_PARTITION");
      ERS_DEBUG(0, "Retrieved partition name from environment: '" << partition_name << "'");
    } else {
      std::cerr << "Partition name not set (neither in command line nor in environment)" << std::endl;
      showHelp( std::cerr, pgmName );
    }
     if (args.hasArg("-po")) { // where you publish
      partition_name_out = args.getStr("-po");
      ERS_DEBUG(0, "Retrieved partition name where you publish from command line: '" << partition_name_out << "'");
      std::cout << "Retrieved partition name where you publish from command line: " << partition_name_out << std::endl;
     } else {
       partition_name_out = partition_name;
     }
    
    
    std::string config_file;
    if (args.hasArg("-c")) {
      config_file  = args.getStr("-c");
      ERS_DEBUG(0, "Config file from  command line: '" << config_file  << "'");
      std::cout<< "Config file from  command line: " << config_file  << std::endl;
      std::vector<std::string> file_list; 
      //      for (unsigned int i = 0; i < config_file.size(); ++i) {
      //      file_list = trp_utils::split(config_file,"\:");
      trp_utils::tokenize(config_file,file_list,":");
      // }
      std::cout << "File_list size is:" << file_list.size() << std::endl;
      for(std::vector<std::string>::iterator it=file_list.begin();
	  it!=file_list.end();++it){
	const char* it_cstr = it->c_str();
	std::ifstream my_file(it_cstr);
	if(my_file.is_open()){
	  // here reads the file .xml
	  ReadConfig(*it);
	  break;
	}
      }
    } else {
      
      // retrieve ther Server Name
      if (args.hasArg("-s")) {
	server_name = args.getStr("-s");
	ERS_DEBUG(0, "Retrieved server name from command line: '" << server_name << "'");
      } else {
	std::cerr << "Server name not set in command line" << std::endl;
	showHelp( std::cerr, pgmName );
      }
      if (args.hasArg("-k")) {
	l1confkey = args.getStr("-k");
	ERS_DEBUG(0, "Retrieved l1 regular expression from command line: '" << l1confkey << "'");
      }
      if (args.hasArg("-out")) {
	name_out = args.getStr("-out");
	ERS_DEBUG(0, "Retrieved outputname from command line: '" << name_out << "'");
      }
    }
    
    // Dumping config
    std::cout << "Dumping Configurations for LVL1RateAdapter:" << std::endl;
    std::cout << partition_name << " ,  " << server_name << " , " << l1confkey << "  outputnme:" << name_out << std::endl;
    std::cout << "config_file = " << config_file << std::endl;


  } 
  catch( daq::ipc::Exception & ex ) {
    ers::fatal( ex );
    return 1;
  }   


  signal(SIGABRT, signal_handler);
  signal(SIGTERM, signal_handler);
  signal(SIGINT,  signal_handler);

  try {
    ERS_LOG("Starting program job" );
    my_lvl1_adapter = new LVL1RateAdapter(partition_name,server_name,APserver_name,
                          l1confkey,name_out,mode, partition_name_out,object_name, min_time, do_acc);

    // subscribe to IS and block the main function with semaphore.wait()
    my_lvl1_adapter->Configure();
    my_lvl1_adapter->Run();

    // when signal received, signal_handler calls semaphore.post()
    if (ReceivedSignal) {
      std::cout << " Signal " << ReceivedSignal << " received." << std::endl;
    }

    // unsubscribe to IS, this code executed after semaphore.wait()
    my_lvl1_adapter->Stop();

  }
    catch( daq::is::Exception & ex )
  {
    is::fatal( ex );
  }

  ERS_LOG( "Signal " << ReceivedSignal << " received, stopping '" << server_name << "' server ... " );
  ERS_DEBUG(0, "Program terminating gracefully" );
  ERS_LOG( "LVL1RateAdapterMain done." );
  delete my_lvl1_adapter;
  return 0;
}
