/** 
 * @file LVL1MuonAdapter.h
 * @author Antonio Sidoti Humboldt Universitat zu Berlin antonio.sidoti@cern.ch
 * @date 12/12/2008
 * @brief Triger Level 1 classes for obtaining the data from IS.
 */

// dai un occhio ai time stamps
// e vedi se puoi fare una cosa piu`  elegante mettendo la neccesita 
// di updatere o meno l' info se il TimePoint_IS e` variato.

#include "LVL1MuonAdapter.h"
//#include "hltpGUI.h"
#include <ipc/core.h>
#include <iostream>
#include "ers/ers.h"
#include <time.h>
#include <regex.h>
//#include "TriP/hltp_globals.h"
#include "TRP/Utils.h"
#include "boost/regex.hpp"

//void callback(ISCallbackInfo *isc);
//#define DEBUG_MUL1

#include "ers/ers.h"

ERS_DECLARE_ISSUE(l1muonadapter, Issue, "conversion issue", )

using namespace boost;

LVL1MuonAdapter::LVL1MuonAdapter(std::string partition_name, 
				 std::string server_name, 
				 std::string name_out,
				 std::string mode, std::string mode_sfo){
  ERS_DEBUG( 3, "LVL1MuonAdapter::LVL1MuonAdapter()" );

  pPartitionName = partition_name;
  pServerName = server_name;
  pNameOut = name_out;
  m_mode = mode;
  m_mode_rec = mode_sfo;

  myXaxis_set.clear( );
  myYaxis_set.clear( );
  myXaxis.clear( );
  myYaxis.clear( );
  
  
  std::cout << "In LVL1MuonAdapter::LVL1MuonAdapter partition_name" <<   pPartitionName << "  Server:" << pServerName << "  NameOut:" << pNameOut << "  Mode:" << m_mode << " ModeSfo:" << m_mode_rec << std::endl;

}

LVL1MuonAdapter::LVL1MuonAdapter(const LVL1MuonAdapter& )
{
	// implemented only to forbid copying

}

LVL1MuonAdapter::~LVL1MuonAdapter() 
{
  ERS_DEBUG( 3, "LVL1MuonAdapter::~LVL1MuonAdapter()" );
}

void LVL1MuonAdapter::Configure(){
  ERS_DEBUG(0, "In Configuration");
  std::cout <<  "In Configuration" << std::endl;
  
  // if (!mPartition) mPartition = new IPCPartition( pPartitionName );
  mPartition = new IPCPartition( pPartitionName );
  pSubscription = "";
  // reserve dimension
  if(m_mode == "L1_RPC") ConfigureRPC();
  if(m_mode == "L1_CALO") ConfigureL1Calo();
  if(m_mode == "L1_TGC") ConfigureL1TGC();
  if(m_mode == "MIOCT") ConfigureMIOCT();
  if(m_mode == "BUSY") ConfigureBusy();
  if(m_mode == "SFI") ConfigureSFI();
  if(m_mode == "SFO") ConfigureSFO();
  if(m_mode == "LUCID") ConfigureLUCID();
  
  // determine the dimension of vector Data and MetaData of TimePoint_IS
  std::cout << "TimePoint has Row:" <<  m_mu_point.NumRow() << "  Col:" << 
    m_mu_point.NumCol() << std::endl;
  int dim_data = m_mu_point.NumCol() * m_mu_point.NumRow();
  std::cout << "TimePoint Data is:" << dim_data << std::endl;
  (m_mu_point.Data).reserve(dim_data);
  cout << "Version 1.1" << endl;

  

}


void LVL1MuonAdapter::Run(){
  ERS_DEBUG( 3, "LVL1MuonAdapter::Run() subscribe to " << pPartitionName );
  //  mPartition = 0;
  if (!mPartition) mPartition = new IPCPartition( pPartitionName );

  std::string my_server_name = "";

  // here you have to loop on L1TriggerRates IS and get L1.RPC.sxx  and add them to m_mu_point
  if(m_mode == "L1_RPC"){
    m_criteria = new ISCriteria("L1Muon.RPC.s.*", RPCL1Rates::type(),
				ISCriteria::AND ); // name obj is ".*Rate10s"
    my_server_name = "L1TriggerRates";
  } 
  if(m_mode == "L1_CALO"){
    m_criteria = new ISCriteria("L1Calo.CMM.Module.*", 
				L1CaloRates::type(),
				ISCriteria::AND ); // name obj is ".*Rate10s"
    my_server_name = "L1TriggerRates";
  } 
  if(m_mode == "L1_TGC"){
    m_criteria = new ISCriteria("L1Muon.TGC.*"); // name obj is ".*Rate10s"
    my_server_name = "L1TriggerRates";
  }


  if(m_mode == "MIOCT"){
    m_criteria = new ISCriteria("RPC-Sector.*", ISMIOCTSectorRun1::type(),
				ISCriteria::AND ); // name obj is ".*Rate10s"
    my_server_name = "L1CT";
  } 
  if(m_mode == "BUSY"){
    m_criteria = new ISCriteria("ISCTPBUSY", ISCTPBUSY::type(), // AS
				ISCriteria::AND ); // name obj is ".*Rate10s"
    my_server_name = "L1CT-History";
  } 
  if(m_mode == "SFI"){
    m_criteria = new ISCriteria("SFI.*\\.StreamInfo\\..*", SFIRate::type(), // AS
				ISCriteria::AND ); // name obj is ".*Rate10s"
    my_server_name = "DF";
  } 
  if(m_mode == "SFO"){
    m_criteria = new ISCriteria("SFO.*\\.StreamInfo\\..*", SFORate::type(), // AS
				ISCriteria::AND ); // name obj is ".*Rate10s"
    my_server_name = "DF";
  } 
  if(m_mode == "LUCID"){
    m_criteria = new ISCriteria("L1CT.CTPIN8"); // name obj is ".*Rate10s"
    my_server_name = "L1TriggerRates";
  }

  
  try {
   ISInfoIterator ii(*mPartition, my_server_name, *m_criteria );
    while( (ii)() ){ // starting loop on ISInfoIterator
      if(m_mode == "L1_RPC"){
        ii.value(my_RPCRates);
        SetTimePoint(my_RPCRates);
      }
      if(m_mode == "MIOCT"){
        ii.value(my_mioct);
        SetTimePoint(my_mioct);
      }
      if(m_mode == "BUSY"){
        ii.value(my_Busy);
        SetTimePoint(my_Busy); // AS
      }
      if(m_mode == "SFI"){
        ii.value(my_SFIRate);
        std::string my_name = ii.name();
        SetTimePoint(my_SFIRate,my_name); // AS
      }
      if(m_mode == "SFO"){
        ii.value(my_SFORate);
        std::string my_name = ii.name();
        SetTimePoint(my_SFORate,my_name); // AS
      }
      if(m_mode == "LUCID"){
        ii.value(my_l1rate);
        SetTimePoint(my_l1rate);
      }
      if(m_mode == "L1_CALO"){
        ii.value(my_l1calorate);
        SetTimePoint(my_l1calorate);
      }
      if(m_mode == "L1_TGC"){
        ii.value(my_isa_tgc);
        SetTimePoint(m_mode);
      }
    }
  } catch (daq::is::RepositoryNotFound &ex){
    ers::warning(l1muonadapter::Issue(ERS_HERE, "IS Repository not found " +boost::lexical_cast<std::string>( ex.what())));
  } catch (daq::is::InvalidCriteria &ex) {
    ers::warning(l1muonadapter::Issue(ERS_HERE, "Invalid criteria " +boost::lexical_cast<std::string>( ex.what())));
  }

  if(m_mode == "SFI" || m_mode == "SFO" || m_mode =="MIOCT" || 
     m_mode =="L1_RPC" || m_mode =="L1_TGC"){
    // do the overall sum
    trp_utils::TotalSum(m_mu_point);
  }
  
  unsigned int my_run = 0;
  unsigned short my_lb =  0;
  trp_utils::GetRunParams(pPartitionName, my_run, my_lb);
  m_mu_point.RunNumber = my_run;
  m_mu_point.LumiBlock = my_lb;
  
  OWLTime o_time;
  trp_utils::GetTime(o_time);
  m_mu_point.TimeStamp =  o_time;
  // now publish
  //  TimePoint_IS *m_mu_point_ptr;
  //m_mu_point_ptr = new TimePoint_IS(m_mu_point);
  //m_sender->send(m_mu_point_ptr);
  //delete m_mu_point_ptr;
  // questo non funziona... devi tornare a non usare il sender...

  ISInfoDictionary *dict;
  
  dict = NULL;
  try{
    dict = new ISInfoDictionary(*mPartition );
  } catch(...){
    ers::warning(l1muonadapter::Issue(ERS_HERE, "IS Cant Instantiate ISInfoDictionary "));
  }

  std::string pub_name = pServerName + ".";
  pub_name += pNameOut;

  try {
    dict->checkin(pub_name,m_mu_point, true);
  } catch(daq::is::InvalidName &ex){
    ers::warning(l1muonadapter::Issue(ERS_HERE, "ISInfoDictionary  Invalid Name" +boost::lexical_cast<std::string>( ex.what())));
  } catch(daq::is::RepositoryNotFound &ex) {
    ers::warning(l1muonadapter::Issue(ERS_HERE, "Repository not found" +boost::lexical_cast<std::string>( ex.what())));
  } catch(daq::is::InfoNotCompatible &ex){
    ers::warning(l1muonadapter::Issue(ERS_HERE, "Info not compatible" +boost::lexical_cast<std::string>( ex.what())));
  }
  delete dict;
} // end Run method


void LVL1MuonAdapter::ConfigureRPC() {
  mSectorNames.reserve(65); // 64 Sectors
  std::ostringstream out;
  //out.right;
  
  
  for(int i=0;i<64;++i){
    out.str(""); 
    out.width(2);
    out.fill('0'); 
    out << i;
    std::string is = "sect_" + out.str();
    //    is += out.str();
    mSectorNames.push_back(is);
  }
  
  
  // now X axis
  std::vector<std::string> myXaxis;

  myXaxis.push_back("SectorID");  
  myXaxis.push_back("t0");
  myXaxis.push_back("t1");
  myXaxis.push_back("t2");
  myXaxis.push_back("t3");
  myXaxis.push_back("t4");
  myXaxis.push_back("t5");
  myXaxis.push_back("t6");
  myXaxis.push_back("t7");

  mSectorNames.push_back("Rate_total");  
  m_mu_point.format(mSectorNames,myXaxis);
} 


void LVL1MuonAdapter::ConfigureL1Calo(){
  
  std::ostringstream out_i;
  std::ostringstream out_j;
  //out.right;
  for(int j=0;j<4;++j){
    out_j.str("");
    if(j==0) out_j << "EM"; 
    if(j==1) out_j << "Energy";
    if(j==2) out_j << "JET"; 
    if(j==3) out_j << "TAU";
    
    for(int i=1;i<5;++i){
      out_i.str("");
      out_i << "Q" << i;
      std::string is = "phi_" +out_i.str() + "_" + out_j.str();
      mSectorNames.push_back(is);
    }
  }
  std::ostringstream out_eta;
  std::vector<std::string> myXaxis;
  myXaxis.push_back("SectorID");
  std::vector<std::string> col_label;
  col_label.push_back("eta<-2.4");
  col_label.push_back("-2.4<eta<-1.6");
  col_label.push_back("-1.6<eta<-0.8");
  col_label.push_back("-0.8<eta<0"); 
  col_label.push_back("0<eta<0.8"); 
  col_label.push_back("0.8<eta<1.6");
  col_label.push_back("1.6<eta<2.4");
  col_label.push_back("2.4<eta");


  for(int it_eta=0;it_eta<8;++it_eta){
     out_eta.str("");
     out_eta.width(2);
     out_eta.fill('0'); 
     out_eta << it_eta;
     std::string is = "eta_" + out_eta.str();
#ifdef OLD_CALO_LAB 
     myXaxis.push_back(is);
#endif
     myXaxis.push_back(col_label[it_eta]);
  }



  
  m_mu_point.format(mSectorNames,myXaxis);
  
}


void LVL1MuonAdapter::ConfigureBusy() {
  myYaxis.reserve(35); 
  
  myYaxis.push_back("ctpmi_ecr_rate");
  myYaxis.push_back("ctpmi_vme_rate");
  myYaxis.push_back("ctpmi_vto0_rate");
  myYaxis.push_back("ctpmi_vto1_rate");
  myYaxis.push_back("ctpmi_bckp_rate");
  myYaxis.push_back("ctpcore_rdt_rate");
  myYaxis.push_back("ctpcore_mon_rate");
  myYaxis.push_back("ctpcore_bckp_rate");
  myYaxis.push_back("ctpmi_rslt_rate");
  myYaxis.push_back("ctpcore_moni0_rate");
  myYaxis.push_back("ctpcore_moni1_rate");
  myYaxis.push_back("ctpcore_moni2_rate");
  myYaxis.push_back("ctpcore_moni3_rate");
  myYaxis.push_back("ctpcore_roi_slink_rate");
  myYaxis.push_back("ctpcore_daq_slink_rate");
  myYaxis.push_back("ctpout_lucid");
  myYaxis.push_back("ctpout_pixel");
  myYaxis.push_back("ctpout_sct");
  myYaxis.push_back("ctpout_trt");
  myYaxis.push_back("ctpout_l1calo");
  myYaxis.push_back("ctpout_bcm");
  myYaxis.push_back("ctpout_zdc");
  myYaxis.push_back("ctpout_larhf");
  myYaxis.push_back("ctpout_laremec");
  myYaxis.push_back("ctpout_laremb");
  myYaxis.push_back("ctpout_lhcf");
  myYaxis.push_back("ctpout_mdtb");
  myYaxis.push_back("ctpout_mdtec");
  myYaxis.push_back("ctpout_tileeb");
  myYaxis.push_back("ctpout_tilelb");
  myYaxis.push_back("ctpout_csc");
  myYaxis.push_back("ctpout_alfa");
  myYaxis.push_back("ctpout_tgc");
  myYaxis.push_back("ctpout_rpc");
  myYaxis.push_back("ctpout_muctpi");
  
  
  // now X axis
  std::vector<std::string> myXaxis;
  
  myXaxis.push_back("val");
  
  m_mu_point.format(myYaxis,myXaxis);
} 


void LVL1MuonAdapter::ConfigureSFI() {
  
  m_criteria2 = new ISCriteria("SFI.*\\.StreamInfo\\..*", SFIRate::type(), // AS
			       ISCriteria::AND ); // name obj is ".*Rate10s"

  myYaxis.reserve(150);
  myXaxis.reserve(30);
  //  mPartition = 0;
  if (!mPartition) mPartition = new IPCPartition( pPartitionName );
  try {
    ISInfoIterator ii(*mPartition, "DF", *m_criteria2 );
    while( (ii)() ){
      ii.value(my_SFIRate);
      std::string name_sfi = ii.name();
      std::string sfi_id = "";
      std::string stream_id = "";
      if(!GetSFName(name_sfi, sfi_id, stream_id)) continue;

      if(myYaxis_set.find(sfi_id)==myYaxis_set.end()){
        myYaxis_set.insert(sfi_id);
      }
      if(myXaxis_set.find(stream_id)==myXaxis_set.end()){
        myXaxis_set.insert(stream_id);
      }  
    }
  } catch (daq::is::RepositoryNotFound &ex){
    ers::warning(l1muonadapter::Issue(ERS_HERE, "IS Repository not found " +boost::lexical_cast<std::string>( ex.what())));
  } catch (daq::is::InvalidCriteria &ex) {
    ers::warning(l1muonadapter::Issue(ERS_HERE, "Invalid criteria " +boost::lexical_cast<std::string>( ex.what())));
  }

  for(std::set<std::string>::iterator it = myXaxis_set.begin();
      it !=  myXaxis_set.end(); ++it){
    myXaxis.push_back(*it);
  }
  for(std::set<std::string>::iterator it = myYaxis_set.begin();
      it !=  myYaxis_set.end(); ++it){
    myYaxis.push_back(*it);
  }
  
  
  myYaxis.push_back("SFI_total");
  m_mu_point.format(myYaxis,myXaxis);
} 

void LVL1MuonAdapter::ConfigureSFO() {
  m_criteria2 = new ISCriteria("SFO.*\\StreamInfo\\..*", SFORate::type(), // AS
			       ISCriteria::AND ); // name obj is ".*Rate10s"
  myYaxis.clear();
  myXaxis.clear();
  myYaxis.reserve(150);
  myXaxis.reserve(30);
  //  mPartition = 0;
  if (!mPartition) mPartition = new IPCPartition( pPartitionName );  
  try {
    ISInfoIterator ii(*mPartition, "DF", *m_criteria2 );
    while( (ii)() ){
      ii.value(my_SFORate);
      std::string name_sfi = ii.name();
      std::string sfi_id = "";
      std::string stream_id = "";
      if(m_mode_rec=="rec"){
        if(!GetSFName(name_sfi, sfi_id, stream_id,".StreamInfo.Received."))
	  continue;
      } else {
        if(!GetSFName(name_sfi, sfi_id, stream_id,".StreamInfo.Saved."))
	  continue;
      }
    
      sfi_id = sfi_id + "_";
      sfi_id = sfi_id + m_mode_rec;
      if(myYaxis_set.find(sfi_id)==myYaxis_set.end()){
        myYaxis_set.insert(sfi_id);
      }
      if(myXaxis_set.find(stream_id)==myXaxis_set.end()){
        myXaxis_set.insert(stream_id);
      }
    }
  } catch (daq::is::RepositoryNotFound &ex){
    ers::warning(l1muonadapter::Issue(ERS_HERE, "IS Repository not found " +boost::lexical_cast<std::string>( ex.what())));
  } catch (daq::is::InvalidCriteria &ex) {
    ers::warning(l1muonadapter::Issue(ERS_HERE, "Invalid criteria " +boost::lexical_cast<std::string>( ex.what())));
  }
  
  for(std::set<std::string>::iterator it = myXaxis_set.begin();
      it !=  myXaxis_set.end(); ++it){
    myXaxis.push_back(*it);
  }
  for(std::set<std::string>::iterator it = myYaxis_set.begin();
      it !=  myYaxis_set.end(); ++it){
    myYaxis.push_back(*it);
  }
  if(m_mode_rec=="rec"){
  myYaxis.push_back("SFO_total_rec");
  } else {
  myYaxis.push_back("SFO_total_sav");
 } 
  m_mu_point.format(myYaxis,myXaxis);
  std::cout << "End of ConfigureSFO" << std::endl;
} 



void LVL1MuonAdapter::ConfigureL1TGC(){
  
  myYaxis.reserve(13);
  myXaxis.reserve(13);
  
  //  std::vector<std::string> phi_sect;
  for(int iphi=0;iphi<12;++iphi){
    std::ostringstream out;
    out.str("");
    out.width(2);
    out.fill('0'); 
    out << iphi;
    //    myXaxis->push_back("TGC_"+out.str()+"_In");
    std::string tmp_out = "TGC_"+out.str();
    myXaxis.push_back(tmp_out);
    //    phi_sect->push_back("TGC_"+out.str());
  }
  
  
  std::vector<std::string>::iterator is1, is2, is3, is4;
  std::vector<std::string> side;
  side.push_back("A"); side.push_back("C"); 
  std::vector<std::string> low_hi;
  low_hi.push_back("E"); low_hi.push_back("F"); 
  std::vector<std::string> phi_F;
  phi_F.push_back("00"); phi_F.push_back("02"); 
  std::vector<std::string> phi_E;
  phi_E.push_back("00"); phi_E.push_back("01"); 
  phi_E.push_back("02"); phi_E.push_back("03");
  
  
  myYaxis.push_back("SectorID");
  for(is1=side.begin();is1!=side.end();++is1){
    for(is2=low_hi.begin();is2!=low_hi.end();++is2){
      std::vector<std::string> phi_2;
      if((*is2)=="F") phi_2 = phi_F;
      if((*is2)=="E") phi_2 = phi_E;
      for(is3=phi_2.begin();is3!=phi_2.end();++is3){
	std::string ylab = (*is1) + "_" + (*is2) + "_" + (*is3);
	myYaxis.push_back(ylab);
      }
    }
  }
  myXaxis.push_back("Rate_total");
  m_mu_point.format(myXaxis,myYaxis);
  
}


void LVL1MuonAdapter::ConfigureMIOCT(){
  //  mPartition = 0;
  if (!mPartition) mPartition = new IPCPartition( pPartitionName );  
  m_criteria2 = new ISCriteria("RPC-Sector.*", 
			       ISMIOCTSectorRun1::type(), // AS
			       ISCriteria::AND ); // name obj is ".*Rate10s"
  
  myYaxis.reserve(150);
  myXaxis.reserve(30);

  try {
    ISInfoIterator ii(*mPartition, "L1CT", *m_criteria2 );
    while( (ii)() ){
      ii.value(my_mioct);
      std::string name_mioct = my_mioct.Name;
      //size_t found = name_mioct.find("/"); // commented out due ti set-but-not-used warning
      std::string sec1 = name_mioct.substr(11,2); // 
      std::string sec2 = name_mioct.substr(14,4); // 
      std::string sec = "RPC_" + sec1 + "_" + sec2;
      myYaxis.push_back(sec);
    }
  } catch (daq::is::RepositoryNotFound &ex){
    ers::warning(l1muonadapter::Issue(ERS_HERE, "IS Repository not found " +boost::lexical_cast<std::string>( ex.what())));
  } catch (daq::is::InvalidCriteria &ex) {
    ers::warning(l1muonadapter::Issue(ERS_HERE, "Invalid criteria " +boost::lexical_cast<std::string>( ex.what())));
  }

  //   // now the x axis which is 6 x 2 = 12
  for(int j=0;j<1;++j){
    for(int i=0;i<6;++i){
      std::string is = "";
      std::ostringstream out;
      out << i;
      if(j==0){
       	is = "P" + out.str();
      }
      if(j==1){
       	is = "2nd_pt" + out.str();
      }
      myXaxis.push_back(is);
    }
  }
  myYaxis.push_back("Rate_total");
  m_mu_point.format(myYaxis,myXaxis);
}

void LVL1MuonAdapter::ConfigureLUCID() {
  myYaxis.reserve(10);
  myXaxis.reserve(10);
  myXaxis.push_back("value");
  myYaxis.push_back("LUCID_A");
  myYaxis.push_back("LUCID_C");
  myYaxis.push_back("LUCID_AC");
  myYaxis.push_back("LUCID_MAT");
  myYaxis.push_back("LUCID2_A");
  myYaxis.push_back("LUCID2_C");
  myYaxis.push_back("LUCID2_AC");
  myYaxis.push_back("LUCID2_MAT");
  m_mu_point.format(myYaxis,myXaxis);


}



void LVL1MuonAdapter::SetTimePoint(RPCL1Rates my_rpc){
  std::vector<std::string> tower_vec =  my_rpc.rateLabels;
  std::vector<double> rate_vec = my_rpc.rateValues;
  
  int itr = 0;
  for(std::vector<std::string>::iterator it = tower_vec.begin();
      it!= tower_vec.end(); ++it,++itr){
    
    std::string sl_name = (*it);
    std::string x_val1 = sl_name.substr(1,2);
    std::string x_val = "sect_"+x_val1;
    std::string y_val = sl_name.substr(4);
    
    //    std::cout << "x_val = " << x_val << " y_val = " << y_val << "  rate:" <<  rate_vec[itr] << std::endl;
    m_mu_point.set(x_val, y_val, rate_vec[itr]);
  }
  
  std::ostringstream out;
  //  out.right;

  
  for(int i=0;i<64;++i){
    out.str("");
    out.width(2);
    out.fill('0'); 
    out << i;
    std::string is = "sect_" + out.str();
    m_mu_point.set(is, "SectorID", i);
  }
}
void LVL1MuonAdapter::SetTimePoint(ISCTPBUSY my_busy){
  m_mu_point.set("ctpmi_ecr_rate","val",my_busy.ctpmi_ecr_rate);
  m_mu_point.set("ctpmi_vme_rate","val",my_busy.ctpmi_vme_rate);
  m_mu_point.set("ctpmi_vto0_rate","val",my_busy.ctpmi_vto0_rate);
  m_mu_point.set("ctpmi_vto1_rate","val",my_busy.ctpmi_vto1_rate);
  m_mu_point.set("ctpmi_bckp_rate","val",my_busy.ctpmi_bckp_rate);
  m_mu_point.set("ctpcore_rdt_rate","val",my_busy.ctpcore_rdt_rate);
  m_mu_point.set("ctpcore_mon_rate","val",my_busy.ctpcore_mon_rate);
  m_mu_point.set("ctpcore_bckp_rate","val",my_busy.ctpcore_bckp_rate);
  m_mu_point.set("ctpmi_rslt_rate","val",my_busy.ctpcore_rslt_rate);
  m_mu_point.set("ctpcore_moni0_rate","val",my_busy.ctpcore_moni0_rate);
  m_mu_point.set("ctpcore_moni1_rate","val",my_busy.ctpcore_moni1_rate);
  m_mu_point.set("ctpcore_moni2_rate","val",my_busy.ctpcore_moni2_rate);
  m_mu_point.set("ctpcore_moni3_rate","val",my_busy.ctpcore_moni3_rate);
  m_mu_point.set("ctpcore_roi_slink_rate","val",my_busy.ctpcore_roi_slink_rate);
  m_mu_point.set("ctpcore_daq_slink_rate","val",my_busy.ctpcore_daq_slink_rate);

  std::vector<double> tmp_vec;
  
  tmp_vec = my_busy.ctpout_12;
  m_mu_point.set("ctpout_lucid","val",tmp_vec[0]);
  m_mu_point.set("ctpout_pixel","val",tmp_vec[1]);
  m_mu_point.set("ctpout_sct","val",tmp_vec[2]);
  m_mu_point.set("ctpout_trt","val",tmp_vec[3]);
  m_mu_point.set("ctpout_l1calo","val",tmp_vec[4]);

  tmp_vec = my_busy.ctpout_13;
  m_mu_point.set("ctpout_bcm","val",tmp_vec[0]);
  m_mu_point.set("ctpout_zdc","val",tmp_vec[1]);
  m_mu_point.set("ctpout_larhf","val",tmp_vec[2]);
  m_mu_point.set("ctpout_laremec","val",tmp_vec[3]);
  m_mu_point.set("ctpout_laremb","val",tmp_vec[4]);

  tmp_vec = my_busy.ctpout_14;
  m_mu_point.set("ctpout_lhcf","val",tmp_vec[0]);
  m_mu_point.set("ctpout_mdtb","val",tmp_vec[1]);
  m_mu_point.set("ctpout_mdtec","val",tmp_vec[2]);
  m_mu_point.set("ctpout_tileeb","val",tmp_vec[3]);
  m_mu_point.set("ctpout_tilelb","val",tmp_vec[4]);

  tmp_vec = my_busy.ctpout_15;
  m_mu_point.set("ctpout_csc","val",tmp_vec[0]);
  m_mu_point.set("ctpout_alfa","val",tmp_vec[1]);
  m_mu_point.set("ctpout_tgc","val",tmp_vec[2]);
  m_mu_point.set("ctpout_rpc","val",tmp_vec[3]);
  m_mu_point.set("ctpout_muctpi","val",tmp_vec[4]);

}

void LVL1MuonAdapter::SetTimePoint(SFIRate my_SFI, std::string my_name){
  std::string sf_name = "";
  std::string stream_name = "";
  if(GetSFName(my_name, sf_name, stream_name)){
    double rate = my_SFI.Rate;
    m_mu_point.set(sf_name,stream_name, rate);
  }
}

void LVL1MuonAdapter::SetTimePoint(SFORate my_SFO, std::string my_name){
  std::string sf_name = "";
  std::string stream_name = "";
  double rate = my_SFO.Rate;
  if(m_mode_rec=="rec"){
    if(GetSFName(my_name, sf_name, stream_name,".StreamInfo.Received.")){
      sf_name = sf_name + "_";
      sf_name = sf_name + m_mode_rec;
      m_mu_point.set(sf_name, stream_name, rate);
    }
  } else {
    if(GetSFName(my_name, sf_name, stream_name,".StreamInfo.Saved.")){
      sf_name = sf_name + "_";
      sf_name = sf_name + m_mode_rec;
      m_mu_point.set(sf_name, stream_name, rate);
    }
  }
}

void LVL1MuonAdapter::SetTimePoint(ISMIOCTSectorRun1 my_mio){
  std::string  name_mioct = my_mio.Name;
  // size_t found = name_mioct.find("/"); 
  std::string sec1 = name_mioct.substr(11,2); // 
  std::string sec2 = name_mioct.substr(14,4); // 
  std::string sec = "RPC_" + sec1 + "_" + sec2;

  std::vector<double> rate_1 = my_mio.Rate_cand1;
  std::vector<double> rate_2 = my_mio.Rate_cand2;

  int ip = 0;
  for(std::vector<double>::iterator it = rate_1.begin();
      it != rate_1.end(); ++it,++ip){
    std::ostringstream out;
    out << ip;
    std::string y_val = "P"+out.str();
    m_mu_point.set(sec,y_val,*it);
  }
  ip = 0;
//   for(std::vector<double>::iterator it = rate_2.begin();
//       it != rate_2.end(); ++it,++ip){
//     std::ostringstream out;
//     out << ip;
//     std::string y_val = "2nd_pt"+out.str();
//     m_mu_point.set(sec,y_val,*it);
//   }
  
}


void LVL1MuonAdapter::SetTimePoint(L1Rates my_l1rate){

  m_mu_point.set("LUCID_A","value",my_l1rate.rateValues2[4]);
  m_mu_point.set("LUCID_C","value",my_l1rate.rateValues2[5]);
  m_mu_point.set("LUCID_AC","value",my_l1rate.rateValues2[17]);
  m_mu_point.set("LUCID_MAT","value",my_l1rate.rateValues2[18]);
  m_mu_point.set("LUCID2_A","value",my_l1rate.rateValues2[9]);
  m_mu_point.set("LUCID2_C","value",my_l1rate.rateValues2[10]);
  m_mu_point.set("LUCID2_AC","value",my_l1rate.rateValues2[11]);
  m_mu_point.set("LUCID2_MAT","value",my_l1rate.rateValues2[20]);

  
  

}

void LVL1MuonAdapter::SetTimePoint(L1CaloRates my_l1calorate){
  bool fold = true;
  std::string calo_label =  my_l1calorate.label;
  // determine if it's EM, ENERGY, JET, TAU
  size_t found = calo_label.find("Module.Q");
  std::string x_label, quadrant_string;
  if(found!=string::npos){
    x_label = calo_label.substr(found+10,20);
    quadrant_string = calo_label.substr(found+8,1);
    if(x_label=="Energy" || x_label=="JET") fold = false;
    x_label = "phi_Q" + quadrant_string + "_" + x_label;
  } else {
    std::cout << "Cannot find string" << std::endl;
    return;
  }
  std::vector<std::string> name_vec = my_l1calorate.names;
  std::vector<double> rate_vec = my_l1calorate.rates;
  int itr = 0;
  for(std::vector<std::string>::iterator it = name_vec.begin();
      it!= name_vec.end(); ++it,++itr){
    std::string y_label;
    if(!GetCaloName(*it, y_label,fold)) continue;
    float my_rate = -1;
    if(!fold) my_rate =  rate_vec[itr];
    else { // you have to sum
      if(y_label=="eta<-2.4") my_rate = rate_vec[1];
      if(y_label=="-2.4<eta<-1.6") my_rate = rate_vec[2]+rate_vec[3];
      if(y_label=="-1.6<eta<-0.8") my_rate = rate_vec[4]+rate_vec[5];
      if(y_label=="-0.8<eta<0") my_rate = rate_vec[6]+rate_vec[7];
      if(y_label=="0<eta<0.8") my_rate = rate_vec[8]+rate_vec[9];
      if(y_label=="0.8<eta<1.6") my_rate = rate_vec[10]+rate_vec[11];
      if(y_label=="1.6<eta<2.4") my_rate = rate_vec[12]+rate_vec[13];
      if(y_label=="2.4<eta") my_rate = rate_vec[14];
    }
    m_mu_point.set(x_label, y_label, my_rate);
  }
  itr = 0;
  for(std::vector<std::string>::iterator it=m_mu_point.XLabels.begin();
      it != m_mu_point.XLabels.end();++it,++itr){
    m_mu_point.set( (*it),"SectorID",itr);
  }
}


void LVL1MuonAdapter::SetTimePoint(std::string mode){
  // for the moment the only mode is L1_TGC
  if(mode == "L1_TGC") {
    //    std::string my_name = my_isa_tgc.name();
    // get the ylabel for that
    std::string xlab, ylab;
    
      std::string isa_label;
      my_isa_tgc >> isa_label;
      std::string isa_desc;
      my_isa_tgc >> isa_desc;
      std::vector<std::string> isa_name;
      my_isa_tgc >> isa_name;
      std::vector<double> isa_val;
      my_isa_tgc >> isa_val;
      //float my_val_in  = isa_val[0];
      float my_val_out = isa_val[1];
      if(GetTGCName(isa_label,xlab, ylab)){
	//m_mu_point.set(xlab + "_In", ylab, my_val_in);
	m_mu_point.set(xlab , ylab, my_val_out);
	int itr = 0;
	for(std::vector<std::string>::iterator it=m_mu_point.XLabels.begin();
	    it != m_mu_point.XLabels.end();++it,++itr){
	  m_mu_point.set( (*it),"SectorID",itr);
	}
      }
  }
}


bool LVL1MuonAdapter::GetSFName(std::string is_name, std::string & sf_name, 
				std::string & stream_name, std::string tag){

  size_t found;
  found = is_name.find(tag);
  if(found!=string::npos){
    sf_name = is_name.substr(3,found-3); // this is Y
    stream_name = is_name.substr(found+tag.size()); // this is X
    return true;
  } else {
    return false;
  }
}

bool LVL1MuonAdapter::GetCaloName(std::string name, std::string & lab_name, 
				  bool fold){
  size_t str_leng = name.length();
  if(str_leng==0) return false;
  std::string y_label_2 = name.substr(str_leng-2,2);
  std::string y_label_1 = name.substr(str_leng-7,1);
  //lab_name = "l1calo_" + y_label_1 + "-" + y_label_2;
#ifdef OLD_CALO_LAB
  if(!fold){
    if(y_label_2=="00"||y_label_2=="08") lab_name = "eta_00";
    if(y_label_2=="01"||y_label_2=="09") lab_name = "eta_01";
    if(y_label_2=="02"||y_label_2=="10") lab_name = "eta_02";
    if(y_label_2=="03"||y_label_2=="11") lab_name = "eta_03";
    if(y_label_2=="04"||y_label_2=="12") lab_name = "eta_04";
    if(y_label_2=="05"||y_label_2=="13") lab_name = "eta_05";
    if(y_label_2=="06"||y_label_2=="14") lab_name = "eta_06";
    if(y_label_2=="07"||y_label_2=="15") lab_name = "eta_07";
  } else {
    if(y_label_2=="01") lab_name = "eta_00";
    if(y_label_2=="02"||y_label_2=="03") lab_name = "eta_01";
    if(y_label_2=="04"||y_label_2=="05") lab_name = "eta_02";
    if(y_label_2=="06"||y_label_2=="07") lab_name = "eta_03";
    if(y_label_2=="08"||y_label_2=="09") lab_name = "eta_04";
    if(y_label_2=="10"||y_label_2=="11") lab_name = "eta_05";
    if(y_label_2=="12"||y_label_2=="13") lab_name = "eta_06";
    if(y_label_2=="14") lab_name = "eta_07";
  }
#endif
  if(!fold){
    if(y_label_2=="00"||y_label_2=="08") lab_name = "eta<-2.4";
    if(y_label_2=="01"||y_label_2=="09") lab_name = "-2.4<eta<-1.6";
    if(y_label_2=="02"||y_label_2=="10") lab_name = "-1.6<eta<-0.8";
    if(y_label_2=="03"||y_label_2=="11") lab_name = "-0.8<eta<0";
    if(y_label_2=="04"||y_label_2=="12") lab_name = "0<eta<0.8";
    if(y_label_2=="05"||y_label_2=="13") lab_name = "0.8<eta<1.6";
    if(y_label_2=="06"||y_label_2=="14") lab_name = "1.6<eta<2.4";
    if(y_label_2=="07"||y_label_2=="15") lab_name = "2.4<eta";
  } else {
    if(y_label_2=="01") lab_name = "eta<-2.4";
    if(y_label_2=="02"||y_label_2=="03") lab_name = "-2.4<eta<-1.6";
    if(y_label_2=="04"||y_label_2=="05") lab_name = "-1.6<eta<-0.8";;
    if(y_label_2=="06"||y_label_2=="07") lab_name = "-0.8<eta<0";
    if(y_label_2=="08"||y_label_2=="09") lab_name = "0<eta<0.8";
    if(y_label_2=="10"||y_label_2=="11") lab_name = "0.8<eta<1.6";
    if(y_label_2=="12"||y_label_2=="13") lab_name = "1.6<eta<2.4";
    if(y_label_2=="14") lab_name = "2.4<eta";
  }
  return true;
}
bool LVL1MuonAdapter::GetTGCName(std::string name, std::string & x_lab_name, 
				 std::string & y_lab_name){
  size_t str_leng = name.length();
  if(str_leng==0) return false;
  std::string y_label_phi = name.substr(str_leng-2,2);
  std::string y_label_ef = name.substr(str_leng-6,1);  
  std::string x_label_phi = name.substr(str_leng-9,2);
  std::string y_label_side = name.substr(str_leng-10,1);
  
  x_lab_name = "TGC_"+x_label_phi;
  y_lab_name = y_label_side + "_" + y_label_ef + "_" + y_label_phi;
  return true;
  
}
