/** 
 * @file LVL1RateAdapter.h
 * @author Antonio Sidoti Humboldt Universitat zu Berlin antonio.sidoti@cern.ch
 * @date 12/12/2008
 * @brief Triger Level 1 classes for obtaining the data from IS.
 */

// dai un occhio ai time stamps
// e vedi se puoi fare una cosa piu`  elegante mettendo la neccesita 
// di updatere o meno l' info se il TimePoint_IS e` variato.

#include "LVL1RateAdapter.h"
#include <ipc/core.h>
#include <iostream>
#include "ers/ers.h"
#include <time.h>
#include <regex.h>
#include "TRP/Utils.h"
#include "boost/regex.hpp"
#include "dqmf/is/Tag.h"
#include "TRP/TrigConfSmKey.h"

//#define L1_debug
//#define L1_debug2


#define NUM_L1_ITEMS 256
#define NUM_CTPIN 768

ERS_DECLARE_ISSUE(l1adapter, Issue, "conversion issue", )

const double m_sigma = 40e-27; // inelastic sigma at injection energy -- temp. kloodge
using namespace boost;

LVL1RateAdapter::LVL1RateAdapter(std::string partition_name, 
				 std::string server_name, 
				 std::string APserver_name,
				 std::string l1confkeys, 
				 std::string name_out,
				 std::string mode,
				 std::string partition_name_out,
				 std::string object_name,
				 float  my_time, bool do_acc)
{
  ERS_DEBUG( 3, "LVL1RateAdapter::LVL1RateAdapter()" );
  m_updated = false;
  pPartitionName = partition_name;
  pPartitionNameOut = partition_name_out;
  pPrescaleAP_server = APserver_name;
  pServerName = server_name;
  pNameOut = name_out;
  m_has_old_lvl1_point = false;
  old_LB = false;
  isNewLB = false;
  if(l1confkeys.size()==0){
    l1conf_pattern = "\'(.*)\'.*prescale.*\\:(.*)\\| TrigType(.*)";
  } else {
    l1conf_pattern = l1confkeys;
  }
  m_smk = m_hltk = m_l1k = m_bgk = m_smk_old =0;
  m_is_first_event = false;
  m_is_publishing  = false;
  std::cout << "LVL1RateAdapter L1 Conf Pattern:" << l1conf_pattern << std::endl;
#ifdef L1_debug
  std::cout << "L1_debug is set" << std::endl;
#endif
#ifdef use_Obsolete
  std::cout << "use_Obsolete is set" << std::endl;
#endif
  m_num_evts=0;
  deadtime_corr = 0;
  test_time_point_lb = 0;
  mbts_LumZeroAndInst = mbts_LumZeroAnd = 0.;
  m_mode = mode;
  m_min_time = my_time;
  m_object_name = object_name;
  for(int i=0;i<4;++i)
    old_count_acc[i] = 0;
  old_turn = 0;
  m_do_acc = do_acc;
  for(int it=0;it<3;++it) updated_flag[it]=false;
  cout << " Version 1.1" << endl;
}

LVL1RateAdapter::LVL1RateAdapter(const LVL1RateAdapter& )
{
	// implemented only to forbid copying

}

LVL1RateAdapter::~LVL1RateAdapter() 
{
  ERS_DEBUG( 3, "LVL1RateAdapter::~LVL1RateAdapter()" );
}

void LVL1RateAdapter::Configure(){
  ERS_DEBUG(0, "In Configuration");
  // ------------  CPTIN
  if(m_mode== "CTPIN"){
    pSubscription = "L1CT";
    mL1Names.reserve(NUM_CTPIN); // 768 numbers of CTPIN
    
    // here set the X and Y axis for mlvl1_point
    // should be done reading the Level1 configuration
    // X axis
    mlvl1_point.EraseCont();
    ReadConfCTPIN();
    mlvl1_point.SetXAxis(mL1Names);
    // now Y axis
    std::vector<std::string> myYaxis;
    //  myYaxis[0] = "Input";
    myYaxis.push_back("Rate");
    mlvl1_point.SetYAxis(myYaxis);
    // determine the dimension of vector Data and MetaData of TimePoint_IS
    std::cout << "TimePoint has Row:" <<  mlvl1_point.NumRow() << "  Col:" << mlvl1_point.NumCol() 
	      << std::endl;
    int dim_data = mlvl1_point.NumCol() * mlvl1_point.NumRow();
    std::cout << "TimePoint Data is:" << dim_data << std::endl;
    (mlvl1_point.Data).reserve(dim_data);
  }
  // ------------  LUCID 
  if(m_mode== "LUCID"){
    mlvl1_point.EraseCont();
    std::vector<std::string> myXaxis;
    std::vector<std::string> myYaxis;
    myXaxis.push_back("Lucid_A");
    myXaxis.push_back("Lucid_C");
    myXaxis.push_back("Lucid_AC");
    myXaxis.push_back("Lucid_Mat");
    
    myYaxis.push_back("Rate");
    myYaxis.push_back("L1");


    
    mlvl1_point.format(myXaxis, myYaxis);
    
    
    mlvl1_point.SetYAxis(myYaxis);
    mlvl1_point.SetXAxis(myXaxis);
    int dim_data = mlvl1_point.NumCol() * mlvl1_point.NumRow();
    std::cout << "TimePoint Data is:" << dim_data << std::endl;
    (mlvl1_point.Data).reserve(dim_data);
    m_has_old_lvl1_point = false;
    old_LB = false;
    isNewLB = false;
    
  }
// ------------  BEAMSPOT
  if(m_mode== "BEAMSPOT"){
    mlvl1_point.EraseCont();
    std::vector<std::string> myXaxis;
    std::vector<std::string> myYaxis;
    myXaxis.push_back("Vertex_X");
    myXaxis.push_back("Vertex_Y");
    myXaxis.push_back("Vertex_Z");
    //    myXaxis.push_back("Vertex_Num");
    
    myYaxis.push_back("Mean");
    myYaxis.push_back("Width");


    
    mlvl1_point.format(myXaxis, myYaxis);
    
    
    mlvl1_point.SetYAxis(myYaxis);
    mlvl1_point.SetXAxis(myXaxis);
    int dim_data = mlvl1_point.NumCol() * mlvl1_point.NumRow();
    std::cout << "TimePoint Data is:" << dim_data << std::endl;
    (mlvl1_point.Data).reserve(dim_data);
    for(int it=0;it<3;++it) updated_flag[it]=false;
    
  }
  // ------------  LUM  (Luminosity)
  else if(m_mode== "LUM"){
    pSubscription = "OLC";
    mlvl1_point.EraseCont();
    std::vector<std::string> myXaxis;
    std::vector<std::string> myYaxis;
    // run on all the tokenized servers for luminosity
    m_object_name_vec = trp_utils::split(m_object_name, "|");
    //    trp_utils::tokenize(m_object_name, m_object_name_vec, "|");
    for(std::vector<std::string>::iterator it = m_object_name_vec.begin();
	it != m_object_name_vec.end(); ++it){
      // remove the trailing OLCApp/ string
      std::string x_label =  (*it);
      size_t found = x_label.find("/");
      if(found!=string::npos){
        std::string test = x_label.substr(found+1,40);
        std::cout << "IRH x_label = " << test << std::endl;
	myXaxis.push_back(x_label.substr(found+1,40));
	updated_map[x_label.substr(found+1,40)] = false;
      }
    }
    
    myYaxis.push_back("RawLumi_Val");
    myYaxis.push_back("RawLumi_Err");
    myYaxis.push_back("CalibLumi_Val");
    myYaxis.push_back("CalibLumi_Err");
    myYaxis.push_back("SpecLumi_Val");
    myYaxis.push_back("SpecLumi_Err");
    myYaxis.push_back("Mu");
    myYaxis.push_back("Mu_err");  
 
    
    mlvl1_point.format(myXaxis, myYaxis);
    
    
    mlvl1_point.SetYAxis(myYaxis);
    mlvl1_point.SetXAxis(myXaxis);
    int dim_data = mlvl1_point.NumCol() * mlvl1_point.NumRow();
    std::cout << "TimePoint Data is:" << dim_data << std::endl;
    (mlvl1_point.Data).reserve(dim_data);
    m_has_old_lvl1_point = false;
    old_LB = false;
    isNewLB = false;
  }
  // ------------  ALFA  (rates)
  else if(m_mode== "ALFA"){
    pSubscription = "alfa_ctpin";
    mlvl1_point.EraseCont();
    std::vector<std::string> myXaxis;
    std::vector<std::string> myYaxis;
    // run on all the tokenized servers for luminosity
    m_object_name_vec = trp_utils::split(m_object_name, "|");
    //    trp_utils::tokenize(m_object_name, m_object_name_vec, "|");
    for(std::vector<std::string>::iterator it = m_object_name_vec.begin();
        it != m_object_name_vec.end(); ++it){
      // remove the trailing OLCApp/ string
      std::string x_label =  (*it);
      size_t found = x_label.find("/");
      if(found!=string::npos){
        std::string test = x_label.substr(found+1,40);
        std::cout << "IRH x_label = " << test << std::endl;
        myXaxis.push_back(x_label.substr(found+1,40));
        updated_map[x_label.substr(found+1,40)] = false;
      }
    }

    myYaxis.push_back("RawLumi_Val");
    myYaxis.push_back("RawLumi_Err");
    myYaxis.push_back("CalibLumi_Val");
    myYaxis.push_back("CalibLumi_Err");
    myYaxis.push_back("SpecLumi_Val");
    myYaxis.push_back("SpecLumi_Err");
    myYaxis.push_back("Mu");
    myYaxis.push_back("Mu_err");


    mlvl1_point.format(myXaxis, myYaxis);


    mlvl1_point.SetYAxis(myYaxis);
    mlvl1_point.SetXAxis(myXaxis);
    int dim_data = mlvl1_point.NumCol() * mlvl1_point.NumRow();
    std::cout << "TimePoint Data is:" << dim_data << std::endl;
    (mlvl1_point.Data).reserve(dim_data);
    m_has_old_lvl1_point = false;
    old_LB = false;
    isNewLB = false;
  }
  // ------------  L1  (Level1 Rates)
  else if(m_mode == "L1"){
  pSubscription = "LumiBlock";
  // reserve dimension
  mL1Names.reserve(258); // 256 +1 for L1A +1 LBN
  mPSfactor.reserve(258);
  mPSfactor_AP.reserve(258);
  for(int ip=0;ip<257;++ip){
    mPSfactor_AP.push_back(-1);
    mPSfactor.push_back(-1);
  }
  rates_L1A = rates_L1A_AP = simple_deadtime = -1;
  
  // here set the X and Y axis for mlvl1_point
  // should be done reading the Level1 configuration
  // X axis
  mlvl1_point.EraseCont();
  ReadConfL1();
  mlvl1_point.SetXAxis(mL1Names);
  // now Y axis
  std::vector<std::string> myYaxis;
  //  myYaxis[0] = "Input";
  myYaxis.push_back("TBP");
  myYaxis.push_back("TAP");
  myYaxis.push_back("TAV");
  myYaxis.push_back("PS");
  myYaxis.push_back("DT"); // Deadtime Fraction
  //  myYaxis.push_back("PS_AP");
  mlvl1_point.SetYAxis(myYaxis);
  // determine the dimension of vector Data and MetaData of TimePoint_IS
  std::cout << "TimePoint has Row:" <<  mlvl1_point.NumRow() << "  Col:" << mlvl1_point.NumCol() 
	    << std::endl;
  int dim_data = mlvl1_point.NumCol() * mlvl1_point.NumRow();
  std::cout << "TimePoint Data is:" << dim_data << std::endl;
  (mlvl1_point.Data).reserve(dim_data);
  (mlvl1_point.Data).reserve(260*5);
  (mlvl1_point.MetaData).reserve(3); // the trigger keys
  // configure items to check // theyr anot used anymore 
  // this is an example 
  // should be configured from somwhere in a practical way
  items_to_check.push_back(0);
  items_to_check.push_back(1);
  items_to_check.push_back(2);
  items_to_check.push_back(256);
  // get trigger keys
  trp_utils::GetTriggerKeys(pPartitionName, m_smk, m_hltk, m_l1k);
  
  m_has_old_lvl1_point = false;
  old_LB = false;
  isNewLB = false;
  } // end of L1 mode
  else if(m_mode == "CTPCORE"){
  mlvl1_point.EraseData();
  mlvl1_point.XLabels.clear();
  // now Y axis
  std::vector<std::string> myYaxis;
  //  myYaxis[0] = "Input";
  myYaxis.push_back("TBP");
  myYaxis.push_back("TAP");
  myYaxis.push_back("TAV");
  myYaxis.push_back("PS");
  myYaxis.push_back("DT"); // Deadtime Fraction
  //  myYaxis.push_back("PS_AP");
  mlvl1_point.SetYAxis(myYaxis);
  // determine the dimension of vector Data and MetaData of TimePoint_IS
  (mlvl1_point.MetaData).reserve(4); // the trigger keys
  trp_utils::GetTriggerKeys(pPartitionName, m_smk, m_hltk, m_l1k, m_bgk);
  (mlvl1_point.MetaData).push_back(std::to_string(m_smk));
  (mlvl1_point.MetaData).push_back(std::to_string(m_l1k));
  (mlvl1_point.MetaData).push_back(std::to_string(m_hltk));
  (mlvl1_point.MetaData).push_back(std::to_string(m_bgk));
  } // end of CTPCORE mode
  else if(m_mode == "CTPCORE_Run3"){ 
  trp_utils::GetTriggerKeys(pPartitionName, m_smk, m_hltk, m_l1k, m_bgk);
  } // end of CTPCORE_Run3 mode
  else if(m_mode == "MIOCT") {
  // now Y axis
  std::vector<std::string> myYaxis;
  myYaxis.push_back("Slot");
  myYaxis.push_back("Input");
  myYaxis.push_back("Mode");
  myYaxis.push_back("Pipeline");
  myYaxis.push_back("Phase");
  myYaxis.push_back("InsertBCID");
  // 6 rates in Hz for 6 different pt thresholds
  myYaxis.push_back("Cand1Pt1");
  myYaxis.push_back("Cand1Pt2");
  myYaxis.push_back("Cand1Pt3");
  myYaxis.push_back("Cand1Pt4");
  myYaxis.push_back("Cand1Pt5");
  myYaxis.push_back("Cand1Pt6");
  // 6 more rates in Hz for 6 different pt thresholds 
  myYaxis.push_back("Cand2Pt1");
  myYaxis.push_back("Cand2Pt2");
  myYaxis.push_back("Cand2Pt3");
  myYaxis.push_back("Cand2Pt4");
  myYaxis.push_back("Cand2Pt5");
  myYaxis.push_back("Cand2Pt6");
  // 1 more rate in Hz for sorting, the maximum of the 12 rates
  myYaxis.push_back("Max");
  mlvl1_point.SetYAxis(myYaxis);
  (mlvl1_point.MetaData).reserve(3); // the trigger keys
  trp_utils::GetTriggerKeys(pPartitionName, m_smk, m_hltk, m_l1k);
  (mlvl1_point.MetaData).push_back(std::to_string(m_smk));
  (mlvl1_point.MetaData).push_back(std::to_string(m_l1k));
  (mlvl1_point.MetaData).push_back(std::to_string(m_hltk));
  } // end of MIOCT mode
  std::cout << "IRH LVL1RateAdapter Trigger Keys are:" <<  m_smk << " , " << m_l1k << " , " << m_hltk << " , " << m_bgk
	    << std::endl;
  ctpcore_oldtime = OWLTime();
}


void LVL1RateAdapter::Run(){
  ERS_DEBUG( 3, "LVL1RateAdapter::Run() subscribe to " << pPartitionName );
  
  IPCPartition Partition(pPartitionName);
  ISInfoReceiver receiver(Partition);
  Receiver = &receiver;
  OWLSemaphore s;
  semaphore = &s;
  // Subscribe
  if(m_mode == "LUCID"){
    try {
      ISCriteria m_crit ("L1CT.CTPIN8", L1Rates::type(), // AS
			 ISCriteria::AND );
      Receiver->subscribe("L1TriggerRates" , m_crit, 
		     &LVL1RateAdapter::LVL1callback_Lucid, this);
    } catch( daq::is::RepositoryNotFound &ex) {
      ers::warning(l1adapter::Issue(ERS_HERE, "Repository not found "
				    +boost::lexical_cast<std::string>(ex.what())));
    } catch (daq::is::AlreadySubscribed &ex) {
      ers::warning(l1adapter::Issue(ERS_HERE, "Already subscribed "
				    +boost::lexical_cast<std::string>(ex.what())));
    } catch ( daq::is::InvalidCriteria &ex) {
      ers::warning(l1adapter::Issue(ERS_HERE, "Invalid Criteria "
				    +boost::lexical_cast<std::string>(ex.what())));
    }
    try {
      ISCriteria m_crit ("L1_Rate", TimePoint_IS::type(), // AS
			 ISCriteria::AND );
      Receiver->subscribe("ISS_TRP.L1_Rate" , m_crit,
		     &LVL1RateAdapter::LVL1callback_LucidL1, this);
    } catch( daq::is::RepositoryNotFound &ex) {
      ers::warning(l1adapter::Issue(ERS_HERE, "Repository not found "
				    +boost::lexical_cast<std::string>(ex.what())));
    } catch (daq::is::AlreadySubscribed &ex) {
      ers::warning(l1adapter::Issue(ERS_HERE, "Already subscribed "
				    +boost::lexical_cast<std::string>(ex.what())));
    } catch ( daq::is::InvalidCriteria &ex) {
      ers::warning(l1adapter::Issue(ERS_HERE, "Invalid Criteria "
				    +boost::lexical_cast<std::string>(ex.what())));
    }
  }
  if(m_mode == "BEAMSPOT"){
    try {
      ISCriteria m_crit ("Vertex_.*");
      Receiver->subscribe("DQM", m_crit , 
		     &LVL1RateAdapter::LVL1callback_DQM, this);
        
    } catch( daq::is::RepositoryNotFound &ex) {
      ers::warning(l1adapter::Issue(ERS_HERE, "Repository not found "
				    +boost::lexical_cast<std::string>(ex.what())));
    } catch (daq::is::AlreadySubscribed &ex) {
      ers::warning(l1adapter::Issue(ERS_HERE, "Already subscribed "
				    +boost::lexical_cast<std::string>(ex.what())));
    } catch ( daq::is::InvalidCriteria &ex) {
      ers::warning(l1adapter::Issue(ERS_HERE, "Invalid Criteria "
				    +boost::lexical_cast<std::string>(ex.what())));
    }
  }
  if(m_mode == "LUM"){
    for(std::vector<std::string>::iterator it_vec = m_object_name_vec.begin();
	it_vec !=  m_object_name_vec.end(); ++it_vec){
      std::string rec_name = pPartitionName + "." + (*it_vec);
      std::cout << "IRH rec_name = " << rec_name << std::endl;
      try {
	ISCriteria m_crit ((*it_vec));
	Receiver->subscribe("OLC" ,  m_crit,
		       &LVL1RateAdapter::LVL1callback_Lum, this);
      } catch( daq::is::RepositoryNotFound &ex) {
	ers::warning(l1adapter::Issue(ERS_HERE, "Repository not found "
				      +boost::lexical_cast<std::string>(ex.what())));
      } catch (daq::is::AlreadySubscribed &ex) {
	ers::warning(l1adapter::Issue(ERS_HERE, "Already subscribed "
				      +boost::lexical_cast<std::string>(ex.what())));
      } catch ( daq::is::InvalidCriteria &ex) {
	ers::warning(l1adapter::Issue(ERS_HERE, "Invalid Criteria "
				      +boost::lexical_cast<std::string>(ex.what())));
      }
    }
  } 
  if(m_mode == "CTPIN"){
    ERS_DEBUG( 0, "Subscribing to Servers");
    ERS_LOG("Subscribing to L1CT");
    try {
      
      //      Receiver->subscribe("L1CT.CTPIN.Instantaneous.Rates", CtpinRateInfo::type() , 
      //			  &LVL1RateAdapter::LVL1callback_CTPIN, this);
      ISCriteria ctpin_crit (".*CTPIN.Instantaneous.*", L1CT::CtpinRateInfo::type(), 
			     ISCriteria::AND );
      ISCriteria ctpin_crit_lb (".*CTPIN.PerLumiBlock.*", L1CT::CtpinRateInfo::type(), 
				ISCriteria::AND );
      if(l1conf_pattern=="PerLumiBlock"){
	Receiver->subscribe("L1CT", ctpin_crit_lb, 
			    &LVL1RateAdapter::LVL1callback_CTPIN, this);
      } else {
	Receiver->subscribe("L1CT", ctpin_crit, 
			    &LVL1RateAdapter::LVL1callback_CTPIN, this);
      }
    } catch( daq::is::RepositoryNotFound &ex) {
      ers::warning(l1adapter::Issue(ERS_HERE, "Repository not found "
				    +boost::lexical_cast<std::string>(ex.what())));
      exit(0);
    } catch (daq::is::AlreadySubscribed &ex) {
      ers::warning(l1adapter::Issue(ERS_HERE, "Already subscribed "
				    +boost::lexical_cast<std::string>(ex.what())));
    } catch ( daq::is::InvalidCriteria &ex) {
      ers::warning(l1adapter::Issue(ERS_HERE, "Invalid Criteria "
				    +boost::lexical_cast<std::string>(ex.what())));
    }
  } else if(m_mode == "L1"){
  
  ERS_DEBUG( 0, "Subscribing to Servers");
  ERS_LOG("Subscribing to L1CT:DeadtimeMon");
  try {
    Receiver->subscribe("L1CT", DeadtimeMon::type() , 
                   &LVL1RateAdapter::LVL1callback_DT, this);
  } catch( daq::is::RepositoryNotFound &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Repository not found "
				  +boost::lexical_cast<std::string>(ex.what())));
  } catch (daq::is::AlreadySubscribed &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Already subscribed "
				  +boost::lexical_cast<std::string>(ex.what())));
  } catch ( daq::is::InvalidCriteria &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Invalid Criteria "
				  +boost::lexical_cast<std::string>(ex.what())));
  }

  ERS_LOG("Subscribing to LumiBlock:IS_LB "); 
  try {
    Receiver->subscribe("LumiBlock", IS_LB::type() , 
		   &LVL1RateAdapter::LVL1callback_rates, this);
  } catch( daq::is::RepositoryNotFound &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Repository not found "
				  +boost::lexical_cast<std::string>(ex.what())));
    exit(0);
  } catch (daq::is::AlreadySubscribed &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Already subscribed "
				  +boost::lexical_cast<std::string>(ex.what())));
  } catch ( daq::is::InvalidCriteria &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Invalid Criteria "
				  +boost::lexical_cast<std::string>(ex.what())));
  } 

  ERS_LOG("Subscribing to pPrescaleAP_server:ISPrescalesAPTool " << pPrescaleAP_server); 
  try {
    Receiver->subscribe(pPrescaleAP_server,ISPrescalesAPTool::type(), 
		   &LVL1RateAdapter::LVL1callback_APrates, this);
  } catch( daq::is::RepositoryNotFound &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Repository not found "
				  +boost::lexical_cast<std::string>(ex.what())));
  } catch (daq::is::AlreadySubscribed &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Already subscribed "
				  +boost::lexical_cast<std::string>(ex.what())));
  } catch ( daq::is::InvalidCriteria &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Invalid Criteria "
				  +boost::lexical_cast<std::string>(ex.what())));
  }
 
  ERS_LOG("Subscribing to L1CT:LumiRaw "); 
  try {
    Receiver->subscribe("L1CT",LumiRaw::type(), 
		   &LVL1RateAdapter::LVL1callback_mbtslum, this);
  } catch( daq::is::RepositoryNotFound &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Repository not found "
				  +boost::lexical_cast<std::string>(ex.what())));
  } catch (daq::is::AlreadySubscribed &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Already subscribed "
				  +boost::lexical_cast<std::string>(ex.what())));
  } catch ( daq::is::InvalidCriteria &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Invalid Criteria "
				  +boost::lexical_cast<std::string>(ex.what())));
  }
  
  ERS_LOG("Subscribing to L1CT:TrigConfL1Items ");
  try {
    Receiver->subscribe("L1CT", TrigConfL1Items::type(), 
		   &LVL1RateAdapter::LVL1callback_config, this);
  }  catch( daq::is::RepositoryNotFound &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Repository not found "
				  +boost::lexical_cast<std::string>(ex.what())));
  } catch (daq::is::AlreadySubscribed &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Already subscribed "
				  +boost::lexical_cast<std::string>(ex.what())));
  } catch ( daq::is::InvalidCriteria &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Invalid Criteria "
				  +boost::lexical_cast<std::string>(ex.what())));
  }
  // if mode L1  
  } else if(m_mode == "CTPCORE"){
  ERS_LOG("Subscribing to Servers");
  try {

    if(l1conf_pattern=="PerLumiBlock"){
      ERS_LOG("Setting up receiver for PerLumiBlock.TriggerRates.");
      Receiver->subscribe("L1CT.CTPCORE.PerLumiBlock.TriggerRates", &LVL1RateAdapter::L1CTcallback_CTPCORE, this);
    } else {
      ERS_LOG("Setting up receiver for Instantaneous.TriggerRates.");
      Receiver->subscribe("L1CT.CTPCORE.Instantaneous.TriggerRates", &LVL1RateAdapter::L1CTcallback_CTPCORE, this);
    }
  } catch( daq::is::RepositoryNotFound &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Repository not found "
                                  +boost::lexical_cast<std::string>(ex.what())));
  } catch (daq::is::AlreadySubscribed &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Already subscribed "
                                  +boost::lexical_cast<std::string>(ex.what())));
  } catch ( daq::is::InvalidCriteria &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Invalid Criteria "
                                  +boost::lexical_cast<std::string>(ex.what())));
  }
  // update trigger keys
  try {
    Receiver->subscribe("RunParams", TrigConfSmKey::type(),
                   &LVL1RateAdapter::L1CTcallback_TrigConfig, this);
  } catch( daq::is::RepositoryNotFound &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Repository not found "
                                  +boost::lexical_cast<std::string>(ex.what())));
  } catch (daq::is::AlreadySubscribed &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Already subscribed "
                                  +boost::lexical_cast<std::string>(ex.what())));
  } catch ( daq::is::InvalidCriteria &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Invalid Criteria "
                                  +boost::lexical_cast<std::string>(ex.what())));
  }
  }  else if(m_mode == "CTPCORE_Run3"){
  ERS_LOG("Subscribing to Servers");
  try {

    if(l1conf_pattern=="PerLumiBlock"){
      ERS_LOG("Setting up receiver for PerLumiBlock.TriggerRates.");
      Receiver->subscribe("L1CT.CTPCORE.PerLumiBlock.TriggerRates", &LVL1RateAdapter::L1CTcallback_CTPCORE_Run3, this);
    } else {
      ERS_LOG("Setting up receiver for Instantaneous.TriggerRates.");
      Receiver->subscribe("L1CT.CTPCORE.Instantaneous.TriggerRates", &LVL1RateAdapter::L1CTcallback_CTPCORE_Run3, this);
    }
  } catch( daq::is::RepositoryNotFound &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Repository not found "
                                  +boost::lexical_cast<std::string>(ex.what())));
  } catch (daq::is::AlreadySubscribed &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Already subscribed "
                                  +boost::lexical_cast<std::string>(ex.what())));
  } catch ( daq::is::InvalidCriteria &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Invalid Criteria "
                                  +boost::lexical_cast<std::string>(ex.what())));
  }

// update trigger keys
  try {
    Receiver->subscribe("RunParams", TrigConfSmKey::type(),
                   &LVL1RateAdapter::L1CTcallback_TrigConfig, this);
  } catch( daq::is::RepositoryNotFound &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Repository not found "
                                  +boost::lexical_cast<std::string>(ex.what())));
  } catch (daq::is::AlreadySubscribed &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Already subscribed "
                                  +boost::lexical_cast<std::string>(ex.what())));
  } catch ( daq::is::InvalidCriteria &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Invalid Criteria "
                                  +boost::lexical_cast<std::string>(ex.what())));
  }
// if mode MIOCT
} else if(m_mode == "MIOCT"){
  ERS_LOG("Subscribing to Servers");
  try {
    Receiver->subscribe("L1CT", L1CT::ISMIOCTSectorContainer::type(),
                   &LVL1RateAdapter::L1CTcallback_MIOCT, this);
  } catch( daq::is::RepositoryNotFound &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Repository not found "
                                  +boost::lexical_cast<std::string>(ex.what())));
  } catch (daq::is::AlreadySubscribed &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Already subscribed "
                                  +boost::lexical_cast<std::string>(ex.what())));
  } catch ( daq::is::InvalidCriteria &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Invalid Criteria "
                                  +boost::lexical_cast<std::string>(ex.what())));
  }


  } else if(m_mode == "TEST"){
  
  ERS_DEBUG( 0, "Subscribing to Servers");
  ERS_LOG("Subscribing to Servers");
  ERS_LOG("Subscribing to ISS_hristova:TimePoint_IS");
  try {
    Receiver->subscribe("ISS_hristova", TimePoint_IS::type() ,
                   &LVL1RateAdapter::LVL1callback_TP, this);
  } catch( daq::is::RepositoryNotFound &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Repository not found "
                                  +boost::lexical_cast<std::string>(ex.what())));
  } catch (daq::is::AlreadySubscribed &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Already subscribed "
                                  +boost::lexical_cast<std::string>(ex.what())));
  } catch ( daq::is::InvalidCriteria &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Invalid Criteria "
                                  +boost::lexical_cast<std::string>(ex.what())));
  }

  } // if mode TEST

  semaphore->wait();
  semaphore = nullptr;
}


void LVL1RateAdapter::L1CTcallback_CTPCORE(ISCallbackInfo *isc) {

  if(isc->reason() == ISInfo::Deleted) return;

  ctpcore_newtime = isc->time();
  if ( ctpcore_newtime.c_time() - ctpcore_oldtime.c_time() < m_min_time) return; 

  if (m_is_publishing) return;
  m_is_publishing = true;

  isc->value(my_rateinfo);

  my_items = my_rateinfo.items;
  if (my_items[0].partition !=1) return; // only ATLAS partition

  mlvl1_point.RunNumber = my_rateinfo.run_number;       // will be overwritten in publishIS
  mlvl1_point.LumiBlock = my_rateinfo.lumiblock_number; // will be overwritten in publishIS
  mlvl1_point.TimeStamp = isc->time();

  trp_utils::GetTriggerKeys(pPartitionName, m_smk, m_hltk, m_l1k, m_bgk);
  mlvl1_point.MetaData[0] = std::to_string(m_smk);
  mlvl1_point.MetaData[1] = std::to_string(m_l1k);
  mlvl1_point.MetaData[2] = std::to_string(m_hltk);
  mlvl1_point.MetaData[3] = std::to_string(m_bgk);

  trp_utils::GetLuminosityInfo("OLC", m_var1, m_var2, m_var3, m_var4, m_var5);


  int dim_data = mlvl1_point.YLabels.size() * my_items.size() + mlvl1_point.YLabels.size() * 1; // add L1ARate
  (mlvl1_point.Data).resize(dim_data);


  int j=0;
  for (size_t i=0; i<my_items.size(); i++) {
    mlvl1_point.XLabels.push_back(my_items[i].name);
    mlvl1_point.Data[j] = my_items[i].tbp;
    mlvl1_point.Data[j+1] = my_items[i].tap;
    mlvl1_point.Data[j+2] = my_items[i].tav;
    if (my_items[i].enabled) {
      mlvl1_point.Data[j+3] = my_items[i].prescale;
    } else {
      mlvl1_point.Data[j+3] = -1 * my_items[i].prescale;
    }
    float my_dt = (my_items[i].tap>0) ?  1 - ( my_items[i].tav / my_items[i].tap) : 0.;
    mlvl1_point.Data[j+4] = my_dt;
    j+=5;
  }
  // add L1ARate
  mlvl1_point.XLabels.push_back("L1A");
  for (int i=dim_data-5; i<dim_data; i++) {
    mlvl1_point.Data[i] = my_rateinfo.L1ARate;
  }
  // add trigger keys
  dim_data = dim_data + mlvl1_point.YLabels.size() * 2; // add TriggerDB_Keys and Lumi
  (mlvl1_point.Data).resize(dim_data);
  mlvl1_point.XLabels.push_back("TriggerDB_Keys");
  mlvl1_point.Data[dim_data-10] = (float)m_smk;
  mlvl1_point.Data[dim_data-9] = (float)m_l1k;
  mlvl1_point.Data[dim_data-8] = (float)m_hltk;
  mlvl1_point.Data[dim_data-7] = (-1.0)*(float)m_bgk;
  mlvl1_point.Data[dim_data-6] = (float)mlvl1_point.LumiBlock;
  mlvl1_point.XLabels.push_back("Luminosity_Info");
  mlvl1_point.Data[dim_data-5] = m_var1;
  mlvl1_point.Data[dim_data-4] = m_var2;
  mlvl1_point.Data[dim_data-3] = m_var3;
  mlvl1_point.Data[dim_data-2] = (-1.)*m_var4;
  mlvl1_point.Data[dim_data-1] = m_var5;  

  publishIS();
  ctpcore_oldtime = OWLTime();
  mlvl1_point.EraseData();
  mlvl1_point.XLabels.clear();
  m_is_publishing = false;
}


// L1 Rate Adapter for Run3 
void LVL1RateAdapter::L1CTcallback_CTPCORE_Run3(ISCallbackInfo *isc) {

  if(isc->reason() == ISInfo::Deleted) return;

  ctpcore_newtime = isc->time();
  if ( ctpcore_newtime.c_time() - ctpcore_oldtime.c_time() < m_min_time) return; 

  if (m_is_publishing) return;
  m_is_publishing = true;

  isc->value(my_rateinfo);

  my_items = my_rateinfo.items;
  if (my_items[0].partition !=1) return; // only ATLAS partition


  ISInfoDictionary *dict = NULL;
  IPCPartition Partition(pPartitionName);
  dict = new ISInfoDictionary(Partition);

  for(auto item : my_items) {
 	my_l1_rate.TBP = item.tbp;
 	my_l1_rate.TAP = item.tap;
 	my_l1_rate.TAV = item.tav;
 	my_l1_rate.PS = item.prescale;
 	my_l1_rate.enabled = item.enabled;
 	my_l1_rate.DT = (item.tap>0) ?  1 - ( item.tav / item.tap) : 0.;

	// suffix used to separate enabled/disabled rates on Grafana dashboard 
	std::string extension = "";
	if (item.enabled)
	  extension = "--enabled";      // '--' delimiter set for ease on dashboard 
	else 
	  extension = "--disabled";

 	std::string ratename = pServerName + "." + item.name + extension;
 	dict->checkin(ratename,my_l1_rate, true);
  }

  // add L1ARate
  {
 	my_l1_rate.TBP = -1; 
 	my_l1_rate.TAP = -1 ;
 	my_l1_rate.TAV = my_rateinfo.L1ARate;
 	my_l1_rate.PS = -1;
 	my_l1_rate.DT = -1;

  	std::string ratename = pServerName + ".L1A";
 	dict->checkin(ratename,my_l1_rate, true);
  }

  // add trigger keys (still in progress)
  {
 	my_l1_rate.TBP = (float)m_smk;
 	my_l1_rate.TAP = (float)m_l1k;
 	my_l1_rate.TAV = (float)m_hltk;
 	my_l1_rate.PS = (-1.0)*(float)m_bgk;
 	my_l1_rate.DT = (float)mlvl1_point.LumiBlock;

  	std::string ratename = pServerName + ".TriggerDB_Keys";
  	dict->checkin(ratename,my_l1_rate, true);

  }

  //add luminosity info (still in progress)
  trp_utils::GetLuminosityInfo("OLC", m_var1, m_var2, m_var3, m_var4, m_var5);
  {
 	my_l1_rate.TBP =  m_var1;
 	my_l1_rate.TAP = m_var2;
 	my_l1_rate.TAV = m_var3;
 	my_l1_rate.PS = (-1.)*m_var4;
 	my_l1_rate.DT = m_var5;


  	std::string ratename = pServerName + ".Luminosity_Info";
  	dict->checkin(ratename,my_l1_rate, true);
  }

  m_is_publishing = false;
  ctpcore_oldtime = OWLTime();
}

void LVL1RateAdapter::L1CTcallback_TrigConfig(ISCallbackInfo *isc) {
  TrigConfSmKey sm_k;
  isc->value(sm_k);
  if(sm_k.SuperMasterKey != m_smk_old ) {

    try{
      trp_utils::GetTriggerKeys(pPartitionName, m_smk, m_hltk, m_l1k, m_bgk);
    } catch (std::runtime_error &exception) {
      std::cout << "runtime error on getTriggerKeys " << std::endl;
    } catch (std::exception &exception) {
      std::cout << "exeption on getTriggerKeys " << std::endl;
    }
    if (m_smk == m_smk_old) return;
    m_smk_old = m_smk;
  }
}

void LVL1RateAdapter::L1CTcallback_MIOCT(ISCallbackInfo *isc) {

  if(isc->reason() == ISInfo::Deleted) return;
  mioct_newtime = isc->time();
  if ( mioct_newtime.c_time() - mioct_oldtime.c_time() < m_min_time) return;

  isc->value(mioct_rateinfo);

  mlvl1_point.TimeStamp = isc->time();

  std::vector<L1CT::ISMIOCTSector> my_items = mioct_rateinfo.Sectors;

  int dim_data = 19 * my_items.size();
  (mlvl1_point.Data).resize(dim_data);


  int j=0;
  for (size_t i=0; i<my_items.size(); i++) {
    mlvl1_point.XLabels.push_back(my_items[i].Name);
    mlvl1_point.Data[j] = my_items[i].MioctSlot;
    mlvl1_point.Data[j+1] = my_items[i].MioctInput;
    std::string smode = my_items[i].Mode;
    float mode=0.;
    if (smode.compare("DISABLED") == 0) mode = -1.0;
    if (smode.compare("EXTERNAL") == 0) mode = 1.0;
    if (smode.compare("MEMORY") == 0) mode = 2.0;
    if (smode.compare("TRANSMIT") == 0) mode = 3.0;
    mlvl1_point.Data[j+2] = mode;
    mlvl1_point.Data[j+3] = my_items[i].Pipeline;
    mlvl1_point.Data[j+4] = my_items[i].Phase;
    mlvl1_point.Data[j+5] = my_items[i].InsertBCID;
    double maxpt = 0.;
    std::vector<double> rate_cand1 = my_items[i].Rate_cand1;
    for (size_t k=0; k<rate_cand1.size(); k++) {
      mlvl1_point.Data[j+6+k] = rate_cand1[k];
      if (rate_cand1[k] > maxpt) {
        maxpt = rate_cand1[k];
      }
    }
    std::vector<double> rate_cand2 = my_items[i].Rate_cand2;
    for (size_t k=0; k<rate_cand1.size(); k++) {
      mlvl1_point.Data[j+12+k] = rate_cand2[k];
      if (rate_cand2[k] > maxpt) {
        maxpt = rate_cand2[k];
      }
    }
    mlvl1_point.Data[j+18] = maxpt;
    j+=19;
  }
 
  publishIS();
  mlvl1_point.EraseData();
  mlvl1_point.XLabels.clear();
  mioct_oldtime = OWLTime();
}



void LVL1RateAdapter::LVL1callback_DQM(ISCallbackInfo *isc) {
  if(isc->reason() == ISInfo::Deleted) return;
  ISInfoAny isa;
  isc->value(isa);
  std::string my_name = isc->name();
  int id_v =0;
  std::string xlab = "Vertex_X";
  if(my_name=="Vertex_X") {
    id_v=0;
    xlab = "Vertex_X";
  }
  if(my_name=="Vertex_Y") {
    id_v=1;
    xlab = "Vertex_Y";
  }
  if(my_name=="Vertex_Z") {
    id_v=2;
    xlab = "Vertex_Z";
  }
  int dqm_status;
  isa >> dqm_status;
  std::vector<std::string> dqm_obj;
  isa >> dqm_obj;
  std::vector<dqmf::is::Tag> dqm_tag;
  isa >> dqm_tag;
  std::vector<dqmf::is::Tag>::iterator it_tag;
  for(it_tag=dqm_tag.begin();it_tag!=dqm_tag.end();++it_tag){
    if(it_tag->name=="Mean")  mlvl1_point.set(xlab,"Mean",it_tag->value);
    if(it_tag->name=="Sigma") mlvl1_point.set(xlab,"Width",it_tag->value);
    updated_flag[id_v] = true;
  }
  
  bool do_publish=true;
  for(int it=0;it<3;++it)
    if(!updated_flag[it]) do_publish=false;
  if(do_publish){
    publishIS();
    for(int it=0;it<3;++it) updated_flag[it]=false;
    mlvl1_point.EraseData();
  } 
}
  
void LVL1RateAdapter::LVL1callback_Lucid(ISCallbackInfo *isc) {
  if(isc->reason() == ISInfo::Deleted) return;
  isc->value(my_l1rate);
  mlvl1_point.set("Lucid_C","Rate",my_l1rate.rateValues2[5]/3.);
  mlvl1_point.set("Lucid_AC","Rate",my_l1rate.rateValues2[22]);
  mlvl1_point.set("Lucid_Mat","Rate",my_l1rate.rateValues2[23]/3.);
  mlvl1_point.set("Lucid_A","Rate",my_l1rate.rateValues2[4]/3.);




#ifdef L1_debug2
  std::cout << "Lucid_A  Rate" << my_l1rate.rateValues2[4] << "  " << my_l1rate.rateValues2[5] << std::endl;
#endif
}

void LVL1RateAdapter::LVL1callback_LucidL1(ISCallbackInfo *isc) {
  if(isc->reason() == ISInfo::Deleted) return;
  isc->value(my_l1point);
  float new_count_acc[4];
  new_count_acc[0] = -1;
  my_l1point.get("L1_LUCID_A", "TBP", new_count_acc[0]);
  new_count_acc[1] = -1;
  my_l1point.get("L1_LUCID_C", "TBP", new_count_acc[1]);
  new_count_acc[2] = -1;
  my_l1point.get("L1_LUCID_A_C", "TBP", new_count_acc[2]);
  new_count_acc[3] = -1;

  mlvl1_point.set("Lucid_A","L1",new_count_acc[0]);
  mlvl1_point.set("Lucid_C","L1",new_count_acc[1]);
  mlvl1_point.set("Lucid_AC","L1",new_count_acc[2]);
  mlvl1_point.set("Lucid_Mat","L1",new_count_acc[3]);

  
  // setting time and lumiblock
  mlvl1_point.TimeStamp = my_l1point.time();
#ifdef L1_debug
  mlvl1_point.print(std::cout);
#endif

  publishIS();
  //  mlvl1_point.EraseData();
}




void LVL1RateAdapter::LVL1callback_Lum(ISCallbackInfo *isc) {
  if(isc->reason() == ISInfo::Deleted) return;
  ISInfoAny isa;
  isc->value(isa);
#ifdef L1_debug
  int attr_number = isa.countAttributes( );
  std::cout <<" attr_number:" << attr_number << std::endl;
#endif
  double my_lum[10];
  for(int it=0;it<10;++it){
    isa >> my_lum[it];
  }
  std::string calib_type;
  isa >> calib_type;
  std::vector<double> calib_pars;
  isa >> calib_pars;
  unsigned int is_run;
  isa >> is_run;
  unsigned int is_lbn;
  isa >> is_lbn;
  // get Ylabel for that
  std::string my_name = isc->name();
  // remove the trailing OLCApp/ string
  std::string x_label;
  size_t found = my_name.find("/");
  if(found!=string::npos){
    x_label = my_name.substr(found+1,40);
    mlvl1_point.set(x_label,"RawLumi_Val", (float)my_lum[0]);
    mlvl1_point.set(x_label,"RawLumi_Err", (float)my_lum[1]);
    mlvl1_point.set(x_label,"CalibLumi_Val", (float)my_lum[6]);
    mlvl1_point.set(x_label,"CalibLumi_Err", (float)my_lum[7]);
    mlvl1_point.set(x_label,"SpecLumi_Val", (float)my_lum[8]);
    mlvl1_point.set(x_label,"SpecLumi_Err", (float)my_lum[9]);
    mlvl1_point.set(x_label,"Mu", (float)my_lum[4]);
    mlvl1_point.set(x_label,"Mu_err", (float)my_lum[5]);
    updated_map[x_label] = true;
  }
  

  mlvl1_point.TimeStamp = isa.time();
  //std::cout << "Lumi TimeStamp, RawLimi_Val: " <<   isa.time() << ", " << (float)my_lum[0] << std::endl;

#ifdef L1_debug
  std::cout << "Lumi:" << my_lumi << "  Rate:" << m_rate << std::endl;
#endif

  bool do_publish = true;

  int itr=0;
  for(std::map<std::string,bool>::iterator it=updated_map.begin();
      it != updated_map.end(); ++it,++itr){ 
    if(!(*it).second) {
      do_publish = false;
      break;
    }
  }

  if(do_publish){
    unsigned int my_run = is_run;
    unsigned short my_lb =  is_lbn;
    mlvl1_point.RunNumber = my_run;
    mlvl1_point.LumiBlock = my_lb;
    publishIS();
    for(std::map<std::string,bool>::iterator it=updated_map.begin();
	it != updated_map.end(); ++it,++itr)
      updated_map[(*it).first] = false;
    
    
  }
}

void LVL1RateAdapter::LVL1callback_mbtslum(ISCallbackInfo *isc) {
  if(isc->reason() == ISInfo::Deleted) return;
  isc->value(my_lumraw);
  std::string my_name = isc->name();
  if(my_name == "L1CT.mbtsLumLuminosityFromZeroAndInst"){
    mbts_LumZeroAndInst = my_lumraw.RawData;
  }
  if(my_name == "L1CT.mbtsLumLuminosityFromZeroAnd"){
    mbts_LumZeroAnd = my_lumraw.RawData;
  }
  
}


void LVL1RateAdapter::LVL1callback_APrates(ISCallbackInfo *isc) {
  if(isc->reason() == ISInfo::Deleted) return;
  isc->value(my_IS_LB_AP);
  mPSfactor_AP = my_IS_LB_AP.NewPrescales;
  rates_L1A_AP = my_IS_LB_AP.NewL1Rate;
}

void LVL1RateAdapter::LVL1callback_TP(ISCallbackInfo *isc) {
  ERS_LOG("TP callback " << "\" " << isc->name()
                         << "\" " << isc->reason()
                         << "\" ");
  if(isc->reason() == ISInfo::Deleted) return;
  isc->value(my_tp_mon);
  test_time_point_lb = my_tp_mon.LumiBlock;
}

void LVL1RateAdapter::LVL1callback_DT(ISCallbackInfo *isc) {
  ERS_LOG("DT callback " << "\" " << isc->name()
                         << "\" " << isc->reason()
                         << "\" ");  
  if(isc->reason() == ISInfo::Deleted) return;
  isc->value(my_dt_mon);
  deadtime_corr = my_dt_mon.TOTCplx1DT;
}
void LVL1RateAdapter::LVL1callback_Busy(ISCallbackInfo *isc) {
  if(isc->reason() == ISInfo::Deleted) return;
  isc->value(my_Busy);
 // deadtime_corr = std::max(my_Busy.ctpmi_bckp_rate,my_Busy.ctpcore_rslt_rate);
  deadtime_corr = my_Busy.ctpcore_moni0_rate;
}

void LVL1RateAdapter::LVL1callback_CTPIN(ISCallbackInfo *isc) {
  if(isc->reason() == ISInfo::Deleted) return;
  isc->value(my_ctprateinfo);
  mlvl1_point.RunNumber = my_ctprateinfo.run_number;
  mlvl1_point.TimeStamp = my_ctprateinfo.time();
  mlvl1_point.Data.clear();
  mlvl1_point.format( mL1Names, mlvl1_point.YLabels);
  vector<L1CT::CtpinRateInfoCounter> ctpin_cnt = my_ctprateinfo.counters;
  vector<L1CT::CtpinRateInfoCounter>::iterator ctpin_cnt_it = ctpin_cnt.begin();
  int i=0;
  for(;ctpin_cnt_it!=ctpin_cnt.end();ctpin_cnt_it++,i++){
    
    string ctpin_str = ctpin_cnt_it->name;
    // make sure that there are no "." (dots)
    regex r_dots("\\.");
    ctpin_str =  regex_replace (ctpin_str, r_dots, "_");
    double rate = ctpin_cnt_it->rate;
    mlvl1_point.set(ctpin_str,"Rate",rate);
  }
  publishIS();
  mlvl1_point.EraseData();
  RatesClear();
}

void LVL1RateAdapter::LVL1callback_rates(ISCallbackInfo *isc) {
  if(isc->reason() == ISInfo::Deleted) return;
#ifdef L1_debug2
  std::cout << "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" << std::endl;
#endif 

  isc->value(my_IS_LB);
  // determine if we changed lumiblock and get timestamp of info 
  // (in this case you shoud re-read 
  // LVL1 items and prescales to check if they changed)
  bool newLB=false;
  newLB = IsNewLB(my_IS_LB, my_old_IS_LB);
#ifdef L1_debug2
  std::cout << "IsNewLB = " << newLB << std::endl;
#endif
  
  if(newLB) { //check again the PS and Trigger items

#ifdef L1_debug
    std::cout << "LVL1RateAdapter:: New LB" << std::endl;
    std::cout << "New LB is:" << my_IS_LB.LBN << std::endl;
#endif
    if(m_is_first_event)
      ReadConfL1();
    
    mlvl1_point.SetXAxis(mL1Names);
    // also the prescales...
    // read again the trigger keys
    trp_utils::GetTriggerKeys(pPartitionName, m_smk, m_hltk, m_l1k);

#ifdef L1_debug
    std::cout << "LVL1RateAdapter::Trigger Keys: " << m_smk << " , " <<   m_l1k  << " , " << m_hltk 
	      << std::endl;
#endif   

  } // end of if(newLB)
  // Evaluate rates
  bool enough_time = GetRates(my_IS_LB, my_old_IS_LB);
  
  if(m_is_first_event){
    std::cout << "Printing my_IS_LB:" << std::endl;
    my_IS_LB.print(std::cout);
  }
#ifdef L1_debug2
  float my_dbg = -9;
  bool pip = mlvl1_point.get( "L1_MBTS_1_BGRP1", "TBP" , my_dbg);
#endif

  if(newLB){ // dont publish but copy

#ifdef L1_debug2
    std::cout << "is newLB won't publish this rate: " << my_dbg << std::endl;
#endif 

    my_old_IS_LB = my_IS_LB;
    old_LB = true;
    RatesClear();
  } else { // isn't newLB
    if(enough_time){
      setLB(my_IS_LB, mlvl1_point);
      old_LB = true;
      my_old_IS_LB = my_IS_LB;
      publishIS();
      mlvl1_point.EraseData();  
    } else {
#ifdef L1_debug2
      std::cout << "No enough time won't publish this rate: " << my_dbg << std::endl;
#endif 
      old_LB = true;
      mlvl1_point.EraseData();
      RatesClear();
    }
  }
#ifdef L1_debug2
  std::cout << "VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV" << std::endl;
#endif 
  

  if(m_is_first_event && newLB && enough_time){
    std::cout << "Printing mlvl1_point:" << std::endl;
    mlvl1_point.print(std::cout);
  }
  
  if(m_is_first_event) m_is_first_event = false;
}
  

void LVL1RateAdapter::setLB(IS_LB my_LB, TimePoint_IS &lvl1_point){
  lvl1_point.RunNumber = my_LB.runnumber;

  //  time_t my_time=atoi((my_LB.Starttime).c_str()); // this is in seconds, need to convert in time_t
  //time_t time_stamp = GetExactTime(my_LB);
  //lvl1_point.TimeStamp = OWLTime(time_stamp);
  // in alternative 

  lvl1_point.TimeStamp = my_LB.time();

  if(m_is_first_event) {
    std::cout << "In SetLB  TimeStamp:" << lvl1_point.TimeStamp << std::endl;
  }

  lvl1_point.Data.clear();
  
  lvl1_point.format( mL1Names, lvl1_point.YLabels);
  for (unsigned int i = 0; i < rates_TBP.size(); i++){
    lvl1_point.set(i,0,rates_TBP[i]);
    lvl1_point.set(i,1,rates_TAP[i]);
    lvl1_point.set(i,3,mPSfactor[i]);
    if(mPSfactor[i]>=1){
      lvl1_point.set(i,2,rates_TAV[i]);
    } else {
      lvl1_point.set(i,2,0);
    }
    if (mDeadTimeFrac.size()>i) {
      lvl1_point.set(i,4,mDeadTimeFrac[i]);
    } else {
      lvl1_point.set(i,4,0.);
    }

    //    lvl1_point.set(i,4,mPSfactor_AP[i]);
  }
  lvl1_point.set(256,0,rates_L1A_TBP);
  lvl1_point.set(256,1,0);
  lvl1_point.set(256,2,rates_L1A);
  lvl1_point.set(256,3,mPSfactor[256]);
  float mDeadTimeFracAll = 0;
  if(rates_L1A_TBP!= 0) mDeadTimeFracAll=1-(rates_L1A/rates_L1A_TBP);
  lvl1_point.set(256,4, mDeadTimeFracAll);
  //  lvl1_point.set(256,4,rates_L1A_AP);
  
  /*  lvl1_point.set(257,0, mbts_LumZeroAndInst);
  lvl1_point.set(257,1,0);
  lvl1_point.set(257,2,0);
  lvl1_point.set(257,3,-1);
  lvl1_point.set(257,4,0);
  
  lvl1_point.set(258,0,mbts_LumZeroAnd);
  lvl1_point.set(258,1,0);
  lvl1_point.set(258,2,0);
  lvl1_point.set(258,3,-1);
  lvl1_point.set(258,4, 0); */
  
  lvl1_point.set(257,0,my_LB.LBN); // this is lumiblobk
  lvl1_point.set(257,1,0);
  lvl1_point.set(257,2,0);
  lvl1_point.set(257,3,-1);
  lvl1_point.set(257,4, 0);
  


#ifdef DEBUG_L1
  std::cout << "Rates_TBP" << std::endl;
  for (unsigned int i = 0; i < rates_TBP.size(); i++)
    std::cout << "i = " << i << " " << rates_TBP[i] << std::endl;
  std::cout << "Rates_TAP" << std::endl;
  for (unsigned int i = 0; i < rates_TAP.size(); i++)
    std::cout << "i = " << i << " " << rates_TAP[i] << std::endl;
  std::cout << "Rates_TAV" << std::endl;
  for (unsigned int i = 0; i < rates_TAV.size(); i++)
    std::cout << "i = " << i << " " << rates_TAV[i] << std::endl;
  std::cout << "PS" << std::endl;
  for (unsigned int i = 0; i < mPSfactor.size(); i++)
    std::cout << "i = " << i << " " << mPSfactor[i] << std::endl;
  std::cout << "PS_AP" << std::endl;
  for (unsigned int i = 0; i < mPSfactor_AP.size(); i++)
    std::cout << "i = " << i << " " << mPSfactor_AP[i] << std::endl;
  
  std::cout << " L1A:" << rates_L1A << "  L1_AP:" << rates_L1A_AP << std::endl;
#endif
  RatesClear();


  lvl1_point.LumiBlock = my_LB.LBN;

  
  if(m_is_first_event)
    std::cout << "End of  SetLB" << std::endl;
}

void LVL1RateAdapter::publishIS(){

  //std::cout << ">>>>>>>>>>>>>> in pusblishIS <<<<<<<<<<<<<<<<<" << std::endl;
  ISInfoDictionary *dict = NULL;
  if(m_mode== "LUM"){
    try {
      IPCPartition PartitionOut(pPartitionNameOut);
      dict = new ISInfoDictionary(PartitionOut);
    } catch (...) {
      ers::warning(l1adapter::Issue(ERS_HERE, "No ISInfoDictionary "));
    }
  } else {
    try {
      IPCPartition Partition(pPartitionName);
      dict = new ISInfoDictionary(Partition);
    } catch (...) {
      ers::warning(l1adapter::Issue(ERS_HERE, "No ISInfoDictionary "));
    }
  }
  std::string l1rate_server = pServerName + ".";
  l1rate_server += pNameOut;
  
  unsigned int my_run = 0;
  unsigned short my_lb =  0;
  if(m_mode!="LUM"){
    trp_utils::GetRunParams(pPartitionName, my_run, my_lb);
    mlvl1_point.RunNumber = my_run;
    mlvl1_point.LumiBlock = my_lb;
  }
  dict->checkin(l1rate_server,mlvl1_point, true);
//   std::cout << "cazzo3.1" << std::endl;
//   TimePoint_IS *mlvl1_point_ptr;
//   std::cout << "cazzo4" << std::endl;
//   mlvl1_point_ptr = new TimePoint_IS(mlvl1_point);
//   std::cout << "cazzo5" << std::endl;
//   m_sender->send(mlvl1_point_ptr);
//   std::cout << "cazzo6" << std::endl;
//   delete mlvl1_point_ptr;
//   std::cout << "cazzo7" << std::endl;
}


void LVL1RateAdapter::LVL1callback_config(ISCallbackInfo *isc) {
  if(isc->reason() == ISInfo::Deleted) return;
  const char* l1conf_pattern_c = l1conf_pattern.c_str();
  regex r_l1conf(l1conf_pattern_c);
  ERS_DEBUG(0, "Reading LVL1 Conf again" );
  isc->value(info);
 
  // now store names of LVL1 and PS into vectors
  mPSfactor.clear();
  mPSfactor_AP.clear();
  mL1Names.clear();
  mlvl1_point.XLabels.clear();
  
  std::vector<string> names;
  names.reserve(260);
  // THE FOLLOwing 256 lines are just crazy I know.....
  names.push_back(info.Item0);
  names.push_back(info.Item1);
  names.push_back(info.Item2);
  names.push_back(info.Item3);
  names.push_back(info.Item4);
  names.push_back(info.Item5);
  names.push_back(info.Item6);
  names.push_back(info.Item7);
  names.push_back(info.Item8);
  names.push_back(info.Item9);
  names.push_back(info.Item10);
  names.push_back(info.Item11);
  names.push_back(info.Item12);
  names.push_back(info.Item13);
  names.push_back(info.Item14);
  names.push_back(info.Item15);
  names.push_back(info.Item16);
  names.push_back(info.Item17);
  names.push_back(info.Item18);
  names.push_back(info.Item19);
  names.push_back(info.Item20);
  names.push_back(info.Item21);
  names.push_back(info.Item22);
  names.push_back(info.Item23);
  names.push_back(info.Item24);
  names.push_back(info.Item25);
  names.push_back(info.Item26);
  names.push_back(info.Item27);
  names.push_back(info.Item28);
  names.push_back(info.Item29);
  names.push_back(info.Item30);
  names.push_back(info.Item31);
  names.push_back(info.Item32);
  names.push_back(info.Item33);
  names.push_back(info.Item34);
  names.push_back(info.Item35);
  names.push_back(info.Item36);
  names.push_back(info.Item37);
  names.push_back(info.Item38);
  names.push_back(info.Item39);
  names.push_back(info.Item40);
  names.push_back(info.Item41);
  names.push_back(info.Item42);
  names.push_back(info.Item43);
  names.push_back(info.Item44);
  names.push_back(info.Item45);
  names.push_back(info.Item46);
  names.push_back(info.Item47);
  names.push_back(info.Item48);
  names.push_back(info.Item49);
  names.push_back(info.Item50);
  names.push_back(info.Item51);
  names.push_back(info.Item52);
  names.push_back(info.Item53);
  names.push_back(info.Item54);
  names.push_back(info.Item55);
  names.push_back(info.Item56);
  names.push_back(info.Item57);
  names.push_back(info.Item58);
  names.push_back(info.Item59);
  names.push_back(info.Item60);
  names.push_back(info.Item61);
  names.push_back(info.Item62);
  names.push_back(info.Item63);
  names.push_back(info.Item64);
  names.push_back(info.Item65);
  names.push_back(info.Item66);
  names.push_back(info.Item67);
  names.push_back(info.Item68);
  names.push_back(info.Item69);
  names.push_back(info.Item70);
  names.push_back(info.Item71);
  names.push_back(info.Item72);
  names.push_back(info.Item73);
  names.push_back(info.Item74);
  names.push_back(info.Item75);
  names.push_back(info.Item76);
  names.push_back(info.Item77);
  names.push_back(info.Item78);
  names.push_back(info.Item79);
  names.push_back(info.Item80);
  names.push_back(info.Item81);
  names.push_back(info.Item82);
  names.push_back(info.Item83);
  names.push_back(info.Item84);
  names.push_back(info.Item85);
  names.push_back(info.Item86);
  names.push_back(info.Item87);
  names.push_back(info.Item88);
  names.push_back(info.Item89);
  names.push_back(info.Item90);
  names.push_back(info.Item91);
  names.push_back(info.Item92);
  names.push_back(info.Item93);
  names.push_back(info.Item94);
  names.push_back(info.Item95);
  names.push_back(info.Item96);
  names.push_back(info.Item97);
  names.push_back(info.Item98);
  names.push_back(info.Item99);
  names.push_back(info.Item100);
  names.push_back(info.Item101);
  names.push_back(info.Item102);
  names.push_back(info.Item103);
  names.push_back(info.Item104);
  names.push_back(info.Item105);
  names.push_back(info.Item106);
  names.push_back(info.Item107);
  names.push_back(info.Item108);
  names.push_back(info.Item109);
  names.push_back(info.Item110);
  names.push_back(info.Item111);
  names.push_back(info.Item112);
  names.push_back(info.Item113);
  names.push_back(info.Item114);
  names.push_back(info.Item115);
  names.push_back(info.Item116);
  names.push_back(info.Item117);
  names.push_back(info.Item118);
  names.push_back(info.Item119);
  names.push_back(info.Item120);
  names.push_back(info.Item121);
  names.push_back(info.Item122);
  names.push_back(info.Item123);
  names.push_back(info.Item124);
  names.push_back(info.Item125);
  names.push_back(info.Item126);
  names.push_back(info.Item127);
  names.push_back(info.Item128);
  names.push_back(info.Item129);
  names.push_back(info.Item130);
  names.push_back(info.Item131);
  names.push_back(info.Item132);
  names.push_back(info.Item133);
  names.push_back(info.Item134);
  names.push_back(info.Item135);
  names.push_back(info.Item136);
  names.push_back(info.Item137);
  names.push_back(info.Item138);
  names.push_back(info.Item139);
  names.push_back(info.Item140);
  names.push_back(info.Item141);
  names.push_back(info.Item142);
  names.push_back(info.Item143);
  names.push_back(info.Item144);
  names.push_back(info.Item145);
  names.push_back(info.Item146);
  names.push_back(info.Item147);
  names.push_back(info.Item148);
  names.push_back(info.Item149);
  names.push_back(info.Item150);
  names.push_back(info.Item151);
  names.push_back(info.Item152);
  names.push_back(info.Item153);
  names.push_back(info.Item154);
  names.push_back(info.Item155);
  names.push_back(info.Item156);
  names.push_back(info.Item157);
  names.push_back(info.Item158);
  names.push_back(info.Item159);
  names.push_back(info.Item160);
  names.push_back(info.Item161);
  names.push_back(info.Item162);
  names.push_back(info.Item163);
  names.push_back(info.Item164);
  names.push_back(info.Item165);
  names.push_back(info.Item166);
  names.push_back(info.Item167);
  names.push_back(info.Item168);
  names.push_back(info.Item169);
  names.push_back(info.Item170);
  names.push_back(info.Item171);
  names.push_back(info.Item172);
  names.push_back(info.Item173);
  names.push_back(info.Item174);
  names.push_back(info.Item175);
  names.push_back(info.Item176);
  names.push_back(info.Item177);
  names.push_back(info.Item178);
  names.push_back(info.Item179);
  names.push_back(info.Item180);
  names.push_back(info.Item181);
  names.push_back(info.Item182);
  names.push_back(info.Item183);
  names.push_back(info.Item184);
  names.push_back(info.Item185);
  names.push_back(info.Item186);
  names.push_back(info.Item187);
  names.push_back(info.Item188);
  names.push_back(info.Item189);
  names.push_back(info.Item190);
  names.push_back(info.Item191);
  names.push_back(info.Item192);
  names.push_back(info.Item193);
  names.push_back(info.Item194);
  names.push_back(info.Item195);
  names.push_back(info.Item196);
  names.push_back(info.Item197);
  names.push_back(info.Item198);
  names.push_back(info.Item199);
  names.push_back(info.Item200);
  names.push_back(info.Item201);
  names.push_back(info.Item202);
  names.push_back(info.Item203);
  names.push_back(info.Item204);
  names.push_back(info.Item205);
  names.push_back(info.Item206);
  names.push_back(info.Item207);
  names.push_back(info.Item208);
  names.push_back(info.Item209);
  names.push_back(info.Item210);
  names.push_back(info.Item211);
  names.push_back(info.Item212);
  names.push_back(info.Item213);
  names.push_back(info.Item214);
  names.push_back(info.Item215);
  names.push_back(info.Item216);
  names.push_back(info.Item217);
  names.push_back(info.Item218);
  names.push_back(info.Item219);
  names.push_back(info.Item220);
  names.push_back(info.Item221);
  names.push_back(info.Item222);
  names.push_back(info.Item223);
  names.push_back(info.Item224);
  names.push_back(info.Item225);
  names.push_back(info.Item226);
  names.push_back(info.Item227);
  names.push_back(info.Item228);
  names.push_back(info.Item229);
  names.push_back(info.Item230);
  names.push_back(info.Item231);
  names.push_back(info.Item232);
  names.push_back(info.Item233);
  names.push_back(info.Item234);
  names.push_back(info.Item235);
  names.push_back(info.Item236);
  names.push_back(info.Item237);
  names.push_back(info.Item238);
  names.push_back(info.Item239);
  names.push_back(info.Item240);
  names.push_back(info.Item241);
  names.push_back(info.Item242);
  names.push_back(info.Item243);
  names.push_back(info.Item244);
  names.push_back(info.Item245);
  names.push_back(info.Item246);
  names.push_back(info.Item247);
  names.push_back(info.Item248);
  names.push_back(info.Item249);
  names.push_back(info.Item250);
  names.push_back(info.Item251);
  names.push_back(info.Item252);
  names.push_back(info.Item253);
  names.push_back(info.Item254);
  names.push_back(info.Item255);
  // here ends this crazy thing!
  int i_emp=0;
  for (unsigned int i = 0; i < names.size(); ++i) {
    
    //     std::vector<std::string> res = trp_utils::split(names[i],"|");
    //     mL1Names.push_back(res[0]);
    //     int ps_fac = trp_utils::convertToInt(res[3]);
    //     mPSfactor.push_back(ps_fac); // ? boh you should check
    // use boost instead
    cmatch res_l1conf;
    if(regex_search(names[i].c_str(), res_l1conf, r_l1conf)){
      if(res_l1conf[1]==""){
	std::string emptylabel;
	emptylabel = "EMPTY_";
	char buf[11];
	sprintf(buf, "%d",i_emp);
	emptylabel += buf;
	mL1Names.push_back(emptylabel);
	++i_emp;
      } else {
	mL1Names.push_back(res_l1conf[1].str());
      }
      int ps_fac = trp_utils::convertToInt(res_l1conf[2].str());
      mPSfactor.push_back(ps_fac);
    } else {
      std::string emptylabel;
      emptylabel = "EMPTY_";
      char buf[11];
      sprintf(buf, "%d",i_emp);
      emptylabel += buf;
      mL1Names.push_back(emptylabel);
      mPSfactor.push_back(-1);
      ++i_emp;
    }
  }
  // adding additional quantities (remember to increase the reserve data)
  mL1Names.push_back("L1A");
  //mL1Names.push_back("mbtsLumZeroAndInst");
  //mL1Names.push_back("mbtsLumZeroAnd");
  mL1Names.push_back("LB_Num");
  mPSfactor.push_back(1);
  mPSfactor.push_back(1);
} // end of callback

void LVL1RateAdapter::ReadConfCTPIN() {
  std::cout << "Reading ReadConfCTPIN" << std::endl;
  IPCPartition Partition(pPartitionName);
  ISInfoIterator *Iter = NULL;
  
  ISCriteria ctpin_crit (".*CTPIN.Instantaneous.*", L1CT::CtpinRateInfo::type(), 
			 ISCriteria::AND );
  ISCriteria ctpin_crit_lb (".*CTPIN.PerLumiBlock.*", L1CT::CtpinRateInfo::type(), 
			 ISCriteria::AND );

  try {
    //    Iter = new ISInfoIterator(Partition, "L1CT.PerLumiBlock.TriggerRates", CtpinRateInfo::type() );
    //    Iter = new ISInfoIterator(Partition, "L1CT", CtpinRateInfo::type() );
    if(l1conf_pattern=="PerLumiBlock"){
      Iter = new ISInfoIterator(Partition, "L1CT", ctpin_crit_lb);
    } else {
      Iter = new ISInfoIterator(Partition, "L1CT", ctpin_crit);
    }
    while ( (*Iter)() ){ // occhio che se non metti il type legge anche l' IS successivo....
      mlvl1_point.XLabels.clear();
      L1CT::CtpinRateInfo info_ctp;
      Iter->value(info_ctp);
      std::vector<L1CT::CtpinRateInfoCounter> ctp_rate_cnt
	= info_ctp.counters;
      std::vector<L1CT::CtpinRateInfoCounter>::iterator ctp_rate_cnt_it = 
	ctp_rate_cnt.begin();
      // now store names of CTPIN into vector
      mL1Names.clear();
      std::vector<string> names;
      names.reserve(NUM_CTPIN);
      for(;ctp_rate_cnt_it!=ctp_rate_cnt.end();++ctp_rate_cnt_it){
	string ctpin_str = ctp_rate_cnt_it->name;
	// make sure that there are no "." (dots)
	regex r_dots("\\.");
	ctpin_str =  regex_replace (ctpin_str, r_dots, "_");
	names.push_back(ctpin_str);
      }
      mL1Names = names;
    }
  }  catch (daq::is::RepositoryNotFound &ex){
    ers::warning(l1adapter::Issue(ERS_HERE, "IS Repository not found " +boost::lexical_cast<std::string>( ex.what())));
  } catch (daq::is::InvalidCriteria &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Invalid criteria PORC and L2PU" +boost::lexical_cast<std::string>( ex.what())));
  }

} // end of method

void LVL1RateAdapter::ReadConfL1() {
  std::cout << "Reading ReadConfL1" << std::endl;
  const char* l1conf_pattern_c = l1conf_pattern.c_str();
  std::cout << "Config Pattern is:" << l1conf_pattern_c << std::endl;
  regex r_l1conf(l1conf_pattern_c);
  IPCPartition Partition(pPartitionName);
#ifdef L1_debug2
  ISInfoIterator Iter2(Partition, "L1CT.TrigConfL1Items");
  while ( Iter2() ){
    cout << "Iter2:" << Iter2.name() << " : " << Iter2.type() << endl;
  }
#endif
  ISInfoIterator *Iter = NULL;
  try {
    Iter = new ISInfoIterator(Partition, "L1CT", TrigConfL1Items::type() );
    while ( (*Iter)() ){ // occhio che se non metti il type legge anche l' IS successivo....
    std::cout << "Iter:" << Iter->name() << " : " << Iter->type() << endl;
    mlvl1_point.XLabels.clear();
    TrigConfL1Items info;
    Iter->value(info);
      // now store names of LVL1 and PS into vectors
    mPSfactor.clear();
    mL1Names.clear();
      //      info.print(std::cout);
      
    std::vector<string> names;
    names.reserve(260);
      // THE FOLLOwing 256 lines are just crazy I know.....
      names.push_back(info.Item0);
      names.push_back(info.Item1);
      names.push_back(info.Item2);
      names.push_back(info.Item3);
      names.push_back(info.Item4);
      names.push_back(info.Item5);
      names.push_back(info.Item6);
      names.push_back(info.Item7);
      names.push_back(info.Item8);
      names.push_back(info.Item9);
      names.push_back(info.Item10);
      names.push_back(info.Item11);
      names.push_back(info.Item12);
      names.push_back(info.Item13);
      names.push_back(info.Item14);
      names.push_back(info.Item15);
      names.push_back(info.Item16);
      names.push_back(info.Item17);
      names.push_back(info.Item18);
    names.push_back(info.Item19);
    names.push_back(info.Item20);
    names.push_back(info.Item21);
    names.push_back(info.Item22);
    names.push_back(info.Item23);
    names.push_back(info.Item24);
    names.push_back(info.Item25);
    names.push_back(info.Item26);
    names.push_back(info.Item27);
    names.push_back(info.Item28);
    names.push_back(info.Item29);
    names.push_back(info.Item30);
    names.push_back(info.Item31);
    names.push_back(info.Item32);
    names.push_back(info.Item33);
    names.push_back(info.Item34);
    names.push_back(info.Item35);
    names.push_back(info.Item36);
    names.push_back(info.Item37);
    names.push_back(info.Item38);
    names.push_back(info.Item39);
    names.push_back(info.Item40);
    names.push_back(info.Item41);
    names.push_back(info.Item42);
    names.push_back(info.Item43);
    names.push_back(info.Item44);
    names.push_back(info.Item45);
    names.push_back(info.Item46);
    names.push_back(info.Item47);
    names.push_back(info.Item48);
    names.push_back(info.Item49);
    names.push_back(info.Item50);
    names.push_back(info.Item51);
    names.push_back(info.Item52);
    names.push_back(info.Item53);
    names.push_back(info.Item54);
    names.push_back(info.Item55);
    names.push_back(info.Item56);
    names.push_back(info.Item57);
    names.push_back(info.Item58);
    names.push_back(info.Item59);
    names.push_back(info.Item60);
    names.push_back(info.Item61);
    names.push_back(info.Item62);
    names.push_back(info.Item63);
  names.push_back(info.Item64);
  names.push_back(info.Item65);
  names.push_back(info.Item66);
  names.push_back(info.Item67);
  names.push_back(info.Item68);
  names.push_back(info.Item69);
  names.push_back(info.Item70);
  names.push_back(info.Item71);
  names.push_back(info.Item72);
  names.push_back(info.Item73);
  names.push_back(info.Item74);
  names.push_back(info.Item75);
  names.push_back(info.Item76);
  names.push_back(info.Item77);
  names.push_back(info.Item78);
  names.push_back(info.Item79);
  names.push_back(info.Item80);
  names.push_back(info.Item81);
  names.push_back(info.Item82);
  names.push_back(info.Item83);
  names.push_back(info.Item84);
  names.push_back(info.Item85);
  names.push_back(info.Item86);
  names.push_back(info.Item87);
  names.push_back(info.Item88);
  names.push_back(info.Item89);
  names.push_back(info.Item90);
  names.push_back(info.Item91);
  names.push_back(info.Item92);
  names.push_back(info.Item93);
  names.push_back(info.Item94);
  names.push_back(info.Item95);
  names.push_back(info.Item96);
  names.push_back(info.Item97);
  names.push_back(info.Item98);
  names.push_back(info.Item99);
  names.push_back(info.Item100);
  names.push_back(info.Item101);
  names.push_back(info.Item102);
  names.push_back(info.Item103);
  names.push_back(info.Item104);
  names.push_back(info.Item105);
  names.push_back(info.Item106);
  names.push_back(info.Item107);
  names.push_back(info.Item108);
  names.push_back(info.Item109);
  names.push_back(info.Item110);
  names.push_back(info.Item111);
  names.push_back(info.Item112);
  names.push_back(info.Item113);
  names.push_back(info.Item114);
  names.push_back(info.Item115);
  names.push_back(info.Item116);
  names.push_back(info.Item117);
  names.push_back(info.Item118);
  names.push_back(info.Item119);
  names.push_back(info.Item120);
  names.push_back(info.Item121);
  names.push_back(info.Item122);
  names.push_back(info.Item123);
  names.push_back(info.Item124);
  names.push_back(info.Item125);
  names.push_back(info.Item126);
  names.push_back(info.Item127);
  names.push_back(info.Item128);
  names.push_back(info.Item129);
  names.push_back(info.Item130);
  names.push_back(info.Item131);
  names.push_back(info.Item132);
  names.push_back(info.Item133);
  names.push_back(info.Item134);
  names.push_back(info.Item135);
  names.push_back(info.Item136);
  names.push_back(info.Item137);
  names.push_back(info.Item138);
  names.push_back(info.Item139);
  names.push_back(info.Item140);
  names.push_back(info.Item141);
  names.push_back(info.Item142);
  names.push_back(info.Item143);
  names.push_back(info.Item144);
  names.push_back(info.Item145);
  names.push_back(info.Item146);
  names.push_back(info.Item147);
  names.push_back(info.Item148);
  names.push_back(info.Item149);
  names.push_back(info.Item150);
  names.push_back(info.Item151);
  names.push_back(info.Item152);
  names.push_back(info.Item153);
  names.push_back(info.Item154);
  names.push_back(info.Item155);
  names.push_back(info.Item156);
  names.push_back(info.Item157);
  names.push_back(info.Item158);
  names.push_back(info.Item159);
  names.push_back(info.Item160);
  names.push_back(info.Item161);
  names.push_back(info.Item162);
  names.push_back(info.Item163);
  names.push_back(info.Item164);
  names.push_back(info.Item165);
  names.push_back(info.Item166);
  names.push_back(info.Item167);
  names.push_back(info.Item168);
  names.push_back(info.Item169);
  names.push_back(info.Item170);
  names.push_back(info.Item171);
  names.push_back(info.Item172);
  names.push_back(info.Item173);
  names.push_back(info.Item174);
  names.push_back(info.Item175);
  names.push_back(info.Item176);
  names.push_back(info.Item177);
  names.push_back(info.Item178);
  names.push_back(info.Item179);
  names.push_back(info.Item180);
  names.push_back(info.Item181);
  names.push_back(info.Item182);
  names.push_back(info.Item183);
  names.push_back(info.Item184);
  names.push_back(info.Item185);
  names.push_back(info.Item186);
  names.push_back(info.Item187);
  names.push_back(info.Item188);
  names.push_back(info.Item189);
  names.push_back(info.Item190);
  names.push_back(info.Item191);
  names.push_back(info.Item192);
  names.push_back(info.Item193);
  names.push_back(info.Item194);
  names.push_back(info.Item195);
  names.push_back(info.Item196);
  names.push_back(info.Item197);
  names.push_back(info.Item198);
  names.push_back(info.Item199);
  names.push_back(info.Item200);
  names.push_back(info.Item201);
  names.push_back(info.Item202);
  names.push_back(info.Item203);
  names.push_back(info.Item204);
  names.push_back(info.Item205);
  names.push_back(info.Item206);
  names.push_back(info.Item207);
  names.push_back(info.Item208);
  names.push_back(info.Item209);
  names.push_back(info.Item210);
  names.push_back(info.Item211);
  names.push_back(info.Item212);
  names.push_back(info.Item213);
  names.push_back(info.Item214);
  names.push_back(info.Item215);
  names.push_back(info.Item216);
  names.push_back(info.Item217);
  names.push_back(info.Item218);
  names.push_back(info.Item219);
  names.push_back(info.Item220);
  names.push_back(info.Item221);
  names.push_back(info.Item222);
  names.push_back(info.Item223);
  names.push_back(info.Item224);
  names.push_back(info.Item225);
  names.push_back(info.Item226);
  names.push_back(info.Item227);
  names.push_back(info.Item228);
  names.push_back(info.Item229);
  names.push_back(info.Item230);
  names.push_back(info.Item231);
  names.push_back(info.Item232);
  names.push_back(info.Item233);
  names.push_back(info.Item234);
  names.push_back(info.Item235);
  names.push_back(info.Item236);
  names.push_back(info.Item237);
  names.push_back(info.Item238);
  names.push_back(info.Item239);
  names.push_back(info.Item240);
  names.push_back(info.Item241);
  names.push_back(info.Item242);
  names.push_back(info.Item243);
  names.push_back(info.Item244);
  names.push_back(info.Item245);
  names.push_back(info.Item246);
  names.push_back(info.Item247);
  names.push_back(info.Item248);
  names.push_back(info.Item249);
  names.push_back(info.Item250);
  names.push_back(info.Item251);
  names.push_back(info.Item252);
  names.push_back(info.Item253);
  names.push_back(info.Item254);
  names.push_back(info.Item255);
  // here ends this crazy thing!
  int i_emp=0;
  for (unsigned int i = 0; i < names.size(); ++i) {
    // use Boost now
    cmatch res_l1conf;
#ifdef L1_debug
    std::cout << "regexp match:" << i << "  name:" << names[i].c_str() << "  " << r_l1conf << std::endl;
#endif
    if(regex_search(names[i].c_str(), res_l1conf, r_l1conf)){
#ifdef L1_debug2
      std::cout << "Config  Keys0:" <<  res_l1conf[1] << "  1:" << res_l1conf[2] << std::endl;
#endif
      if(res_l1conf[1]==""){
	std::string emptylabel;
	emptylabel = "EMPTY_";
	char buf[11];
	sprintf(buf, "%d",i_emp);
	emptylabel += buf;
	mL1Names.push_back(emptylabel);
	++i_emp;
      } else {
	mL1Names.push_back(res_l1conf[1].str());
      }
      int ps_fac = trp_utils::convertToInt(res_l1conf[2].str());
      mPSfactor.push_back(ps_fac);
    } else {
      std::string emptylabel;
      emptylabel = "EMPTY_";
      char buf[11];
      sprintf(buf, "%d",i_emp);
      emptylabel += buf;
      mL1Names.push_back(emptylabel);
      mPSfactor.push_back(-1);
      ++i_emp;
    }
  }
  // adding additional quantities (remember to increase the reserve data)
  mL1Names.push_back("L1A");
  //mL1Names.push_back("mbtsLumZeroAndInst");
  //mL1Names.push_back("mbtsLumZeroAnd");
  mL1Names.push_back("LB_Num");
  mPSfactor.push_back(1);
  mPSfactor.push_back(1);
  
//  mL1Names.push_back("SimpleDeadTime");
//  mPSfactor.push_back(1);

    } // end of while

  }  catch (daq::is::RepositoryNotFound &ex){
    ers::warning(l1adapter::Issue(ERS_HERE, "IS Repository not found " +boost::lexical_cast<std::string>( ex.what())));
  } catch (daq::is::InvalidCriteria &ex) {
    ers::warning(l1adapter::Issue(ERS_HERE, "Invalid criteria PORC and L2PU" +boost::lexical_cast<std::string>( ex.what())));
  }

} // end of method

time_t LVL1RateAdapter::GetExactTime(IS_LB my_LB){
  float exact_time = my_LB.Starttime_sec;
  exact_time += float(my_LB.L1A_turns)/11246.;

  return (time_t)exact_time;
}

bool LVL1RateAdapter::GetRates(IS_LB my_LB, IS_LB my_old_LB){
  bool enough_time = true;
  ++m_num_evts;
  if( my_old_LB.runnumber!=0 ) old_LB = false;
  isNewLB =IsNewLB(my_LB, my_old_LB);

#ifdef L1_debug2
  std::cout << " ^^^^^^^^^^^^^^ in GetRates ^^^^^^^^^^ " << std::endl;
  if(isNewLB) std::cout << " isNewLB" << std::endl;
  if(!isNewLB) std::cout << " isOldLB" << std::endl; 
#endif
  
  float time_TBP = 0;
  time_TBP = (!isNewLB) ? (my_LB.L1A_turns - my_old_LB.L1A_turns )/ 11246. 
    : (my_LB.L1A_turns)/ 11246. ;
  // skip the current my_LB if the time is too small (less than 8 seconds)
  int it_TBP=0;
  std::vector<uint64_t>::iterator it_pt_TBP;
  for(it_pt_TBP = (my_LB.TBP_counters).begin();
      it_pt_TBP != (my_LB.TBP_counters).end();
      ++it_pt_TBP, ++it_TBP){
    float my_rate_TBP=0;
    if(!m_do_acc){
      my_rate_TBP=(*it_pt_TBP - my_old_LB.TBP_counters[it_TBP] );
      if(time_TBP!=0 && (my_rate_TBP>=0)) {
	my_rate_TBP = (!isNewLB) ? my_rate_TBP / time_TBP : (*it_pt_TBP)/ time_TBP ;
      } else {
	my_rate_TBP = 0.;
      }
      if(my_rate_TBP>100000000) my_rate_TBP = 0.;
    } else 
      my_rate_TBP = *it_pt_TBP;
    
    rates_TBP.push_back(my_rate_TBP);
  }
  
  
  if(time_TBP<m_min_time){
    enough_time = false;
  }

  // now the TAP
  float time_TAP = 0;
  time_TAP = (!isNewLB) ? (my_LB.L1A_turns - my_old_LB.L1A_turns )/ 11246.
    : (my_LB.L1A_turns)/ 11246. ;
  int it_TAP=0;
  std::vector<uint64_t>::iterator it_pt_TAP;
  for(it_pt_TAP = (my_LB.TAP_counters).begin();
      it_pt_TAP != (my_LB.TAP_counters).end();
      ++it_pt_TAP,++it_TAP){
    float my_rate_TAP=0;
    if(!m_do_acc){
      my_rate_TAP=(*it_pt_TAP - my_old_LB.TAP_counters[it_TAP] );
      if(time_TAP!=0 && (my_rate_TAP>=0)) {
	my_rate_TAP = (!isNewLB) ? my_rate_TAP / time_TAP : (*it_pt_TAP)/ time_TAP ;
      } else {
	my_rate_TAP = 0.;
      }
      if(my_rate_TAP>100000000) my_rate_TAP = 0.;
    } else {
      my_rate_TAP = *it_pt_TAP;
    }
    rates_TAP.push_back(my_rate_TAP);
  }

  // now the TAV
  float time_TAV = 0;
  time_TAV = (!isNewLB) ? (my_LB.L1A_turns - my_old_LB.L1A_turns )/ 11246.
    : (my_LB.L1A_turns)/ 11246. ;
  int it_TAV=0;
  std::vector<uint64_t>::iterator it_pt_TAV;
  for(it_pt_TAV = (my_LB.TAV_counters).begin();
      it_pt_TAV != (my_LB.TAV_counters).end();
      ++it_pt_TAV,++it_TAV){
    float my_rate_TAV=0;
    if (!m_do_acc){
      my_rate_TAV=(*it_pt_TAV - my_old_LB.TAV_counters[it_TAV] );
      if(time_TAV!=0 && (my_rate_TAV>=0)) {
	my_rate_TAV = (!isNewLB) ? my_rate_TAV / time_TAV : (*it_pt_TAV)/ time_TAV ;
      } else {
	my_rate_TAV = 0.;
      }
      if(my_rate_TAV>100000000) my_rate_TAV = 0.;
    } else {
      my_rate_TAV = *it_pt_TAV;
    }
    rates_TAV.push_back(my_rate_TAV);
  }
  // now L1A
  float time_L1A = 0;
  time_L1A = (!isNewLB) ? (my_LB.L1A_turns - my_old_LB.L1A_turns )/ 11246. 
    : (my_LB.L1A_turns)/ 11246. ;
  rates_L1A=0;
  if(!m_do_acc){
    rates_L1A= (my_LB.L1A_counter - my_old_LB.L1A_counter );
    if(time_L1A!=0 && (rates_L1A >=0) ) {
      rates_L1A = (!isNewLB) ? rates_L1A / time_L1A : my_LB.L1A_counter/ time_L1A ;
    } else {
      rates_L1A = 0.;
    }
  } else {
    rates_L1A = my_LB.L1A_counter;
  }
  // now calculate the deadtime fraction for each trigger item
  if(!m_do_acc){
    int it_id=0;
    std::vector<float>::iterator it_rates_TAP;
    for(it_rates_TAP = rates_TAP.begin();
	it_rates_TAP != rates_TAP.end();
	++it_rates_TAP,++it_id){
      float my_dt = (*(it_rates_TAP)>0) ?  1 - ( rates_TAV[it_id]/ *(it_rates_TAP)) :
	0.;
      mDeadTimeFrac.push_back(my_dt);
    }
  }
  
  float corr_factor = 1.-deadtime_corr/100.;
  if(corr_factor>0.001){
    rates_L1A_TBP = rates_L1A/corr_factor;
  } else {
    rates_L1A_TBP = 0.;  // 
  }
  if(rates_L1A_TBP > 200000 && !m_do_acc){
    ers::warning(l1adapter::Issue(ERS_HERE, "Too large L1A_TBP Rate Setting it to 0Hz"));
    rates_L1A_TBP = 0.;
  }
  if(rates_L1A > 2000000 && !m_do_acc){
    ers::warning(l1adapter::Issue(ERS_HERE, "Too large L1A_TAV Rate Setting it to 0Hz"));
    rates_L1A = 0.;
  }
  
  if(m_num_evts%100==0){
    std::cout << "===== L1A  time:" << time_L1A << "  Rate_TBP:" << rates_L1A_TBP << "  Rate:" << rates_L1A << "  num Update:" << m_num_evts << std::endl;
  }


#ifdef L1_debug
  std::cout << " vvvvvvvvvvvvvvvvvvv end GetRates vvvvvvvvvvvvvv " << std::endl;
#endif
  // if(isNewLB) enough_time=false;
  return enough_time;

}




bool LVL1RateAdapter::IsNewLB(IS_LB is_lb, IS_LB old_is_lb){
//  if(!old_LB) return true;
#ifdef L1_debug2
  std::cout << "Runnumber Old:" << old_is_lb.runnumber << "  New: " << is_lb.runnumber << std::endl;
  std::cout << "LBN Old:" << old_is_lb.LBN << "  New: " << is_lb.LBN << std::endl;
#endif
  if(is_lb.runnumber!=old_is_lb.runnumber) return true;
  if(is_lb.LBN!=old_is_lb.LBN) return true;
  return false;
}

void LVL1RateAdapter::RatesClear(){
  rates_TBP.clear();
  rates_TAP.clear();
  rates_TAV.clear();
  mDeadTimeFrac.clear();
}

void LVL1RateAdapter::Stop() {

  if(m_mode == "LUM"){
    try {
      Receiver->unsubscribe("OLC");
    } catch (daq::is::RepositoryNotFound &ex){
      std::cout << "Repository not found " << std::endl;
    } catch(daq::is::SubscriptionNotFound &ex){
      std::cout << "Subscription not found " << std::endl;
    }
  } else if(m_mode == "CTPIN"){
    try {
      Receiver->unsubscribe("L1CT");
    } catch(...){
      std::cout << " Cannot unsubscribe to L1CT  " << std::endl;
    } 
  } else if(m_mode == "L1"){
    try {
      Receiver->unsubscribe("L1CT-History");
    } catch(...){
      std::cout << " Cannot unsubscribe to L1CT-History  " << std::endl;
    }
    try {
      Receiver->unsubscribe("LumiBlock");
    } catch(...){
      std::cout << " Cannot unsubscribe to LumiBlock  " << std::endl;
    }
    try {
      Receiver->unsubscribe(pPrescaleAP_server);
    } catch(...){

      std::cout <<  " Cannot unsubscribe to APserver_name " << std::endl;
    }
    try {
      Receiver->unsubscribe("L1CT");
    } catch(...){
      std::cout << " Cannot unsubscribe to L1CT  " << std::endl;
    }
  } else if(m_mode == "CTPCORE_Run3"){

      //the run has ended. Sending null values for all attributes.
      ISInfoDictionary *dict = NULL;
      IPCPartition Partition(pPartitionName);
      dict = new ISInfoDictionary(Partition);

      for(auto item : my_items) {
  	    my_l1_rate.TBP = 0;
   	    my_l1_rate.TAP = 0;
 	    my_l1_rate.TAV = 0;
 	    my_l1_rate.PS = 0;
 	    my_l1_rate.DT = 0;

 	    std::string ratename = pServerName + "." + item.name;
 	    dict->checkin(ratename,my_l1_rate, true);
      }//end of the loop over attributes
  }//end of CTPCORE_Run3   
}


void LVL1RateAdapter::signal_handler() {

  if (Receiver != 0) {
    if (semaphore != 0) {
      semaphore->post();
    }
  }
}
