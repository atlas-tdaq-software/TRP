#!/bin/sh
#rm ipc_server.log; ipc_server -p MyTestPartitionAS >& ipc_server.log &
#sleep 30
rm rdb_server.log; rdb_server -p MyTestPartitionAS -d ISRepository -S  ../share/is_timepoint.schema.xml >& rdb_server.log &
sleep 10
rm rdb_server2.log; rdb_server -p MyTestPartitionAS -d ISRepository -S  ../share/is_key_value_tree_two.xml >& rdb_server2.log &
sleep 10
rm is_server.log; is_server -p MyTestPartitionAS -n ISS_TRP -H 1000 >& is_server.log &
sleep 5
rm is_server_1.log; is_server -p MyTestPartitionAS -n Histogramming-L2-1 >& is_server_1.log &
sleep 5
rm is_server_2.log; is_server -p MyTestPartitionAS -n Histogramming-L2-2 >& is_server_2.log &
sleep 5
rm is_server_3.log; is_server -p MyTestPartitionAS -n Histogramming-EF-1 >& is_server_3.log &
sleep 5
rm is_server_4.log; is_server -p MyTestPartitionAS -n Histogramming-EF-2  >& is_server_4.log &
sleep 5
rm publish_hlt.log; ./publish_hlt.pl >& publish_hlt.log &
sleep 10
################### USE THIS OR THE FOLLOWING COMMENTED
#rm lvl1adapter.log; l1adapter_exe  -p MyTestPartitionAS  -s UTripServerIS >& lvl1adapter.log &
#sleep 5
#rm hltadapter.log; hltadapter_exe -p MyTestPartitionAS -s UTripServerIS -o UTripServerIS -pl2 "\L2-Gather.*" -hl2 ".*\Rate10s" -pef "\L2-Gath.*" -hef ".*\Rate5m" >&  hltadapter.log;
#sleep 5
#rm farmadapter.log;  farmadapter_exe -p MyTestPartitionAS -s UTripServerIS >& farmadapter.log  &
#sleep 5
#rm ttreedumper.log;  ttreedumper_exe -p MyTestPartitionAS -s UTripServerIS -a LVL1_Rate >& ttreedumper.log &
#sleep 5
#rm conf.log;  trigconfadapter -p MyTestPartitionAS -s UTripServerIS -a TRIGGERDBV2>& conf.log &
############## ALTERNATIVELY YOU CAN USE THE FOLLOWING  (nicer?) (or trp_adapter_conf.xml at Pt1)
rm lvl1adapter.log; l1adapter_exe  -c .../share/trp_adapter_conf_testPt1_L2.xml >& lvl1adapter.log &
sleep 5
rm hltadapter_EF.log; hltadapter_exe -c ../share/trp_adapter_conf_testPt1_EF.xml >&  hltadapter_EF.log;
sleep 5
rm hltadapter_L2.log; hltadapter_exe -c ../share/trp_adapter_conf_testPt1_L2.xml >&  hltadapter_L2.log;
sleep 5
rm farmadapter.log;  farmadapter_exe -c ../share/trp_adapter_conf_testPt1_L2.xml >& farmadapter.log  &
sleep 5
rm ttreedumper.log;  ttreedumper_exe -c  ../share/trp_adapter_conf_testPt1_L2.xml>& ttreedumper.log &
sleep 5
rm conf.log;  trigconfadapter -c ../share/trp_adapter_conf_testPt1_L2.xml >& conf.log &
