 /**
    @file CPUUsageAdapterMain.cpp

    Fill and publish a TimePoint_IS object on the TRP server which
    contains a summary of current information on CPU usage of chains
    in either L2 and EF. The source of the average time information is
    either .*TIMERS/TrigSteer_L2_Chains or
    .*TIMERS/TrigSteer_EF_Chains.  Rate information comes from the
    corresponding TRP object: L2_rates or EF_rates. A subscription for
    the histogram is made. When an update is received, the rates
    are fetched from TRP.

    The total CPU usage of a chain is the product of the average
    execution time per invocation from the histogram and the
    corresponding rate from TRP. This should be more accurate than
    taking it only from the histogram since the TRP time points are
    less susceptible to gathering issues.

    Note that one instance of cpuadapter_exe for L2 and another for EF
    is needed.

    @author M. Medinnis
 */
#include <iostream>
#include <sstream>
#include <stdexcept>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#include "ers/ers.h"
#include "TRP/Args.h"

#include "config/Configuration.h"
#include "ipc/partition.h"
#include <ipc/core.h>
#include "boost/smart_ptr.hpp"

#include <oh/OHRootReceiver.h>
#include <oh/OHSubscriber.h>

#include "CPUReceiver.h"

#include "TRP/ReadXML.h"
#include "TRP/Utils.h"

#include <signal.h>

static std::string partition_name;
static std::string trp_server;

static std::string out_name;
static std::string his_server;
static std::string his_provider;
static std::string his_name;
static std::string rate_name;
static std::string farm_name;
static std::string farm_prefix;

static std::string pgmName;

static bool CA_DEBUG_MODE = false; 

static CPUReceiver *pReceiver = 0; // allows My_Stop to communicate with it

void My_Stop(int sig){  
  std::cout << " I have received a SIG : " << sig << std::endl;

  if(pReceiver != 0)
    pReceiver->RemoveOutputTP();

  exit(0);
}

/** The configuration is read from a general trp configuration file
for either L2 or EF. 
*/

void ReadConfig(std::string  config_file){
  
  std::cout << "ReadConfig(" << config_file << " ) " << std::endl;
  
  ReadXML my_reader;
  
  my_reader.SetElement("cpuadapter");
  
  std::vector<std::string> my_str;
  
  my_str.push_back("his_input_is");
  my_str.push_back("his_provider");
  my_str.push_back("his_name");
  my_str.push_back("rate_name");
  my_str.push_back("farm_name");
  my_str.push_back("farm_prefix");
  my_str.push_back("outputname");
  my_str.push_back("debug_mode");
  
  my_reader.SetAttribute(my_str);
  my_reader.readConfigFile(config_file);
  
  // partition_name= my_reader.GetValC("partition"); // it comes from the command line or environment.
  trp_server    = my_reader.GetValC("server");
  out_name      = my_reader.GetVal("outputname");
  his_server    = my_reader.GetVal("his_input_is");
  his_provider  = my_reader.GetVal("his_provider");
  his_name      = my_reader.GetVal("his_name");
  rate_name     = my_reader.GetVal("rate_name");
  farm_name     = my_reader.GetVal("farm_name");
  farm_prefix   = my_reader.GetVal("farm_prefix");

  if(my_reader.GetVal("debug_mode") == "true") {
    std::cout << "Setting debug mode -- no publication" << std::endl;
     CA_DEBUG_MODE = true;
  }
}


void showHelp( std::ostream& os) {
  os << "Usage: " << pgmName << " options/args                                              " << std::endl;
  os << "                                                                                   " << std::endl;
  os << "Options/Arguments:                                                                 " << std::endl;
  os << "  -c <file.xml>   Read info from xml file instead                                  " << std::endl;
  os << "                                                                                   " << std::endl;
  os << "Description:                                                                       " << std::endl;
  os << "   CPUUSageAdapterMain computes average CPU time per chain invocation using an OH  " << std::endl;
  os << "   histo and, combines it with rate info from TRP, for total CPU usage per chain   " << std::endl; 
  os << "   which is published as a TimePoint_IS structure                                  " << std::endl; 
  os << "Debug level and verbosity:                                                         " << std::endl;
  os << "   They are controlled via the TDAQ_ERS shell variables:                           " << std::endl; 
  os << "     - $TDAQ_ERS_DEBUG_LEVEL        [0-4]                                          " << std::endl; 
  os << "     - $TDAQ_ERS_VERBOSITY_LEVEL    [0-3]                                          " << std::endl;
  os << "     - $TDAQ_ERS_FATAL              [eg: 'lsterr,throw']                           " << std::endl;   
  os << "     - $TDAQ_ERS_ERROR                                                             " << std::endl;
  os << "     - $TDAQ_ERS_WARNING                                                           " << std::endl;
  os << "     - $TDAQ_ERS_INFO               [eg: 'filter(efd),lstdout']                    " << std::endl;
  os << "     - $TDAQ_ERS_DEBUG                                                             " << std::endl;
  exit(1);
}

/** Read the configuration, set up the receiver, subscribe for the
    timing info and then sleep. */
int main(int ac, char* av[] ){

  signal(SIGABRT, &My_Stop);
  signal(SIGTERM, &My_Stop);
  signal(SIGINT,  &My_Stop);

  try{
    
    // get arguments
    Args args( ac, av );
    
    // get program name
    
    pgmName = av[0];
    
    std::string::size_type pos = pgmName.rfind( '/' );
    if( pos != std::string::npos )
      pgmName = pgmName.substr( pos+1 );
    
    
    // check if requested help
    if( args.hasArg("-h")     || 
        args.hasArg("-?")     || 
        args.hasArg("--help") || 
        ac <= 1  )
      { showHelp( std::cerr); }
    
    // reconstruct command line string for debugging feedback
    std::stringstream cmdLine;
    cmdLine << av[0];
    
    for( int i = 1; i < ac; ++i )
      cmdLine << " " << av[i];
    
    std::cout << "cmdline: '" << cmdLine.str() << "'" << std::endl;


      // Retrieve the partition name (from command line or from environment) 
    if (args.hasArg("-p")) {
      partition_name = args.getStr("-p");
      ERS_DEBUG(0, "Retrieved partition name from command line: '" << partition_name << "'");
      std::cout << "Retrieved partition name from command line: '" << partition_name << std::endl;
    } else if (getenv("TDAQ_PARTITION")) {
      partition_name = getenv("TDAQ_PARTITION");
      ERS_DEBUG(0, "Retrieved partition name from environment: '" << partition_name << "'");
      std::cout << "Retrieved partition name from environment: '" << partition_name << std::endl;
    } else {
      std::cerr << "Partition name not set (neither in command line nor in environment)" << std::endl;
      showHelp( std::cerr );
    }

    std::string config_file;

    if (args.hasArg("-c")) {
      config_file  = args.getStr("-c");
      ERS_DEBUG(0, "Config file from  command line: '" << config_file  << "'");
      std::cout<< "Config file from  command line: '" << config_file  << std::endl;
      ReadConfig(config_file);
    } else { // start of config without XML
      showHelp(std::cerr);
    } // end else read config
    
    std::cout << "CPU Adapter configuration:" << std::endl;
    std::cout << "    Partition:              " << partition_name  << std::endl;
    std::cout << "    In rate timepoint name: " << rate_name << std::endl; 
    std::cout << "    In farm timepoint name: " << farm_name << std::endl; 
    std::cout << "    Timing Input:           " << his_server << std::endl;
    std::cout << "    Timing his provider:    " << his_provider << std::endl;
    std::cout << "    Timing his:             " << his_name << std::endl;
    std::cout << "    Output server:          " << trp_server << std::endl;
    std::cout << "    OutputName :            " << out_name << std::endl;
    
    ERS_DEBUG(0, "Starting program job" );

    IPCCore::init( ac, av );
    IPCPartition partition(partition_name);

    CPUReceiver Receiver(
			 partition_name,
			 trp_server,
			 out_name,
			 rate_name,
			 farm_name,
			 farm_prefix
		);
    pReceiver = &Receiver;

    Receiver.SetDebugMode( CA_DEBUG_MODE );

    OHSubscriber ohhs( partition, his_server, Receiver );
    
    OWLRegexp provider(his_provider);
    OWLRegexp histogram(his_name);
    
    bool keepTrying = true;

    int waitInterval = 10;
    while(keepTrying) {
      try{
	ohhs.subscribe( provider, histogram, true); 
	keepTrying = false;
      }
      catch(daq::oh::ObjectNotFound &onf) {
	std::cout << his_name << " not found, retry in " << waitInterval << " sec" << std::endl;
	ERS_DEBUG(0, "Cannot subscribe for cpu usage histogram from OH: object not found");
      }
      catch(daq::oh::RepositoryNotFound &rnf) {
	std::cout << " Repository not found, retry in " << waitInterval << " sec" << std::endl;
	ERS_DEBUG(0, "Cannot subscribe for cpu usage histogram from OH: repository not found");
      }
      catch(daq::oh::ProviderNotFound &pnf) {
	std::cout << " Provider not found, retry in " << waitInterval << " sec" << std::endl;
	ERS_DEBUG(0, "Cannot subscribe for cpu usage histogram from OH: provider not found");
      }
      
      catch(daq::oh::Exception & ex ){
	ERS_DEBUG(0, "Cannot subscribe for cpu usage histogram from OH" );
	std::cout << " Exception on subscription attempt, retry in " << waitInterval << " sec" << std::endl;
      }
      sleep(waitInterval);
      if(waitInterval < 120) 
	waitInterval += 10;
      else
	keepTrying = false;
    }    

    ERS_DEBUG( 3, "CPUUsageAdapter::Run");
    std::cout << "About to ohhs.run" << std::endl;
    //ohhs.run();
    

    ERS_DEBUG(0, "Program terminating gracefully" );

  }

  catch( ers::Issue &ex)   { ERS_DEBUG( 0, "Caught ers::Issue: " << ex ); }
  catch( std::exception& e) { ERS_DEBUG( 0, "Caught std::exception: " << e.what() ); }
  catch( ... )             { ERS_DEBUG( 0, "Caught unidentified exception" ); }

  return 0;
}
