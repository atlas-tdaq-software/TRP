#include "CPUReceiver.h"
#include <algorithm>
#include <sstream>

#include <TAxis.h>
#include <THashList.h>
#include <TIterator.h>
#include <TH2F.h>
#include <TObjString.h>

//#include "rc/LuminosityInfo.h"
#include "TTCInfo/LumiBlock.h"

#include "TRP/Utils.h"

unsigned int CPUReceiver::m_TPupdateInterval = 10; // seconds

const size_t CPUReceiver::prefLength = std::string("TrigSteer_XX:Chain_").size();

const std::string CPUReceiver::aveLab =  "PerCall";
const std::string CPUReceiver::fracLab = "FracOF";
const std::string CPUReceiver::ninetyPercentLab = "Time90";
const std::string CPUReceiver::totLab =  "TotCPU";

static void PrintTP(TimePoint_IS &tp);


CPUReceiver::CPUReceiver(
			 const std::string &partition_name,
			 const std::string &trp_server,
			 const std::string &out_name,
			 const std::string &rate_name_str,
			 const std::string &farm_name,
			 const std::string &farm_prefix
			 ) :
  DEBUG_MODE( false ),
  m_trp_server(trp_server),
  m_out_name(out_name),
  m_rate_name(rate_name_str),
  m_farm_name(farm_name),
  m_farm_prefix(farm_prefix),
  m_lastUpdate(0),
  m_dict( partition_name ),
  m_full_out_name(m_trp_server + "." + m_out_name),
  m_full_rate_name(m_trp_server + "." + m_rate_name)
{
  m_refTime = OWLTime(OWLTime::Seconds).total_mksec_utc();

  if(farm_name == "")
    m_farm_name = "_Rate_mon";

  if(farm_prefix == "" && out_name.substr(0,2) == "L2")
    m_farm_prefix = "L2";
  else
    m_farm_prefix = "EF";

  m_full_farm_name = m_trp_server + "." + m_farm_prefix + m_farm_name;

}

static bool printEnabled = false;

void CPUReceiver::receive( OHRootHistogram & histogram){
  
  if(DEBUG_MODE) std::cout << "In Receive" << std::endl;
  
  
  static int nReceives = 0; // , enablePrintCounter = 1;
  nReceives++;
  printEnabled = (nReceives < 16);
  
  
  ERS_DEBUG( 0, "Starting CPUReceiver::receive()" << std::endl);
  if(printEnabled) std::cout << "Starting CPUReceiver::receive(), nReceives: " << nReceives 
			     << ", Histo time stamp: " << histogram.time << std::endl;
  
  int gatherTime = static_cast<int>( (histogram.time.total_mksec_utc() - m_refTime) / m_oneMillion );
  
  if(DEBUG_MODE) std::cout << "About to get the average rate" << std::endl;
  
  TimePoint_IS averageRate;
  if ( !GetAverageRate(gatherTime, averageRate) ) {
    return;
  }
  if(DEBUG_MODE) {
    std::cout << "Average rate" << std::endl;
    PrintTP(averageRate);
  }
  if(printEnabled) std::cout << "Got average rate" << std::endl;

    int nNodes;
  bool gnfn_rc = GetNumFarmNodes(nNodes);
  if(printEnabled) 
    std::cout << "RC from GetNumFarmNodes: " << gnfn_rc << ", nNodes: " << nNodes << std::endl;

  if(gnfn_rc == false) {
    nNodes = 0;
    if(DEBUG_MODE)
      std::cout << "GetNumFarmNodes returned false" << std::endl;
  }
  if(DEBUG_MODE) std::cout << "Number of farm nodes: " << nNodes << std::endl;

  TimePoint_IS newResult;

  
  TH2F *pHist = dynamic_cast<TH2F*>( histogram.histogram.get() );

  if(pHist == 0) {
    std::cerr << "Nul histogram pointer received" << std::endl;
    return;
  }

  if ( !CalculateNewResult(newResult, pHist, averageRate, nNodes) ) {
    return;
  }

  if(printEnabled) std::cout << "Calculated new result" << std::endl;

  std::stringstream ss;
  ss << "nNodes=" << nNodes;  

  newResult.MetaData.push_back( ss.str() );
  newResult.TimeStamp = histogram.time;
  

  // publish

  static bool notInserted = true;
  if(notInserted) {
    try {
      std::cout << "Attempting to insert " << m_full_out_name << " into dictionary" << std::endl;
      m_dict.insert(m_full_out_name, newResult);
      notInserted = false;
    }
    catch( daq::is::InvalidName&) {
      if(printEnabled) std::cout << "Caught exception on insert: InvalidName -- name:" << 
	m_full_out_name << std::endl;
    }
    catch( daq::is::RepositoryNotFound&) {
      if(printEnabled) std::cout << "Caught exception on insert: RepositoryNotFound -- name:"  << 
	m_full_out_name << std::endl;
    }
    catch( daq::is::InfoAlreadyExist&) {
      if(printEnabled) std::cout << "Caught exception on insert: InfoAlreadyExist -- name:"  << 
	m_full_out_name << std::endl;
    }
  } else {
    // Error recovery strategy is to wait until next update comes and attempt to re-insert.
    try {
      if(printEnabled)  std::cout << "Attempting to update " << m_full_out_name 
				  << std::endl;
      m_dict.update(m_full_out_name, newResult,true);
    }
    catch( daq::is::InvalidName&) {
      if(printEnabled) std::cout << "Caught exception on update: InvalidName -- name:"  << 
	m_full_out_name << std::endl;
      notInserted = true;
    }
    catch( daq::is::RepositoryNotFound& ) {
      if(printEnabled) std::cout << "Caught exception on update: RepositoryNotFound -- name:"  << 
	m_full_out_name << std::endl;
      notInserted = true;
    }
    catch( daq::is::InfoNotFound &) {
      if(printEnabled) std::cout << "Caught exception on update: InfoNotFound -- name:"  << 
	m_full_out_name << std::endl;
      notInserted = true;
    }
    catch( daq::is::InfoNotCompatible& ) {
      if(printEnabled) std::cout << "Caught exception on update: InfoNotCompatible -- name:"  << 
	m_full_out_name << std::endl;
      RemoveOutputTP();
      notInserted = true;
    }
  }
  m_lastUpdate = gatherTime;
  
  //if( (nReceives == enablePrintCounter) && enablePrintCounter < 128)
  //  enablePrintCounter *= 2;
}

bool CPUReceiver::GetAverageRate(int gatherTime, TimePoint_IS &averageRate)
{
  /*
    This fetches time points and averages every TP with time stamp after
    the last update. (If no new time point is available, it uses the latest available.) 
    The number of time points  fetched is computed as the time between
    gatherTime and the last update divided by an assumed update interval. The resulting average is
    taken to be the average rate since the last update. It clearly doesn't correspond
    very exactly to the accumulation time of the cpu histograms but it is almost
    certainly precise enough for the intended purpose. 
  */

  TimePoint_IS &tp = averageRate;

  std::vector<TimePoint_IS> tpvec;

  unsigned int pointsToFetch = static_cast<unsigned int>(gatherTime - m_lastUpdate)/m_TPupdateInterval + 1;
  pointsToFetch = (pointsToFetch > 15)? 15 : pointsToFetch;
  pointsToFetch = (pointsToFetch <  2)?  2 : pointsToFetch;

  if(DEBUG_MODE) std::cout << "GetAverageRate: pointsToFetch: " << pointsToFetch << std::endl;

  if(printEnabled) std::cout << "GetAverageRate: pointsToFetch: " << pointsToFetch << std::endl;

  try {
    m_dict.getValues( m_full_rate_name, tpvec, pointsToFetch );
  }
  catch (daq::is::InvalidName&) {
    std::cout << "CPUReceiver: failed to get " << m_full_rate_name << ": InvalidName" << std::endl;
    return false;
  }
  catch (daq::is::RepositoryNotFound&) {
    std::cout << "CPUReceiver: failed to get " << m_full_rate_name << ": Repository not found" << std::endl;
    return false;
  }
  catch (daq::is::InfoNotFound&) {
    std::cout << "CPUReceiver: failed to get " << m_full_rate_name << ": Info not found" << std::endl;
    return false;
  }
  catch (daq::is::InfoNotCompatible& ) {
    std::cout << "CPUReceiver: failed to get " << m_full_rate_name << ": Info not compatible" << std::endl;
    return false;
  }
  if(tpvec.size() == 0) {
    std::cout << "CPUReceiver: failed to get" << m_full_rate_name << ": no time points returned" << std::endl;
    return false;
  }
  
  
  unsigned int dsize = 0;
  
  int nPoints = 0;
  int nOld = 0;
  OWLTime lastSeen(0,0); // for getting rid of possible duplicates

  std::vector<TimePoint_IS>::iterator ritp = tpvec.begin(); // seems the latest time is first in the vector
  for(; ritp != tpvec.end(); ritp++) {
    
    if(dsize == 0) 
      dsize = ritp->Data.size();
    
    if(dsize < 3)
      continue;
    
    if(ritp->Data.size() != dsize) {
      std::cout << "warning, different sized time points in history, 1st 2nd: " 
		<< dsize << ", " << ritp->Data.size() << std::endl;
      continue;
    }
    
    if(ritp->TimeStamp == lastSeen)
      continue;
    lastSeen = ritp->TimeStamp;

    //std::cout << ritp->TimeStamp << " size " << ritp->Data.size() << std::endl;
    if(ritp->TimeStamp <= m_lastUpdate){
      if(++nOld > 1) 
	break;
      continue;
    } 
    
    if(nPoints == 0) {
      tp = *ritp;
    } else {
      unsigned int iElement = 0;
      for(std::vector<float>::iterator id = ritp->Data.begin(); id != ritp->Data.end(); id++, iElement++) {
	tp.Data[iElement] = ( tp.Data[iElement] * nPoints + *id ) / (nPoints + 1);
      }
    }
    nPoints++;
  }

  if( DEBUG_MODE ) std::cout << "GetAverageRate: nPoints: " << nPoints << std::endl;
  if( printEnabled ) std::cout << "GetAverageRate: nPoints: " << nPoints << std::endl;
  
  if(nPoints == 0) {
    std::cout << "CPUReceiver: no valid time points in update from TRP server" << std::endl;
    return false;
  }

  m_lastUpdate = static_cast<int>( (tp.TimeStamp.total_mksec_utc() - m_refTime) / m_oneMillion );

  
  //std::cout << "Averaged over " << nPoints << " time points" << std::endl;
  
  return true;
}


bool CPUReceiver::GetNumFarmNodes(int &nNodes)
{

  TimePoint_IS farmTP;
  try {
    m_dict.getValue( m_full_farm_name, farmTP );
  }
  catch (daq::is::InvalidName&) {
    std::cout << "CPUReceiver: failed to get " << m_full_farm_name << ": InvalidName" << std::endl;
    return false;
  }
  catch (daq::is::RepositoryNotFound&) {
    std::cout << "CPUReceiver: failed to get " << m_full_farm_name << ": Repository not found" << std::endl;
    return false;
  }
  catch (daq::is::InfoNotFound&) {
    std::cout << "CPUReceiver: failed to get " << m_full_farm_name << ": Info not found" << std::endl;
    return false;
  }
  catch (daq::is::InfoNotCompatible&) {
    std::cout << "CPUReceiver: failed to get " << m_full_farm_name << ": Info not compatible" << std::endl;
    return false;
  }
  
  float val = 1;
  if(!farmTP.get("providers", "value", val)) {
    std::cout << "GetNumFarmNodes: providers not found" << std::endl;
    nNodes = 1;
    return false;
  }
  nNodes = static_cast<int>(val);
  nNodes = (nNodes < 1)? 1 : nNodes;

  return true;
}


bool CPUReceiver::CalculateNewResult(TimePoint_IS &newResult, TH2F *histogram, TimePoint_IS &averageRate, int nFarmNodes)
{

  newResult.EraseCont();

  newResult.RunNumber = averageRate.RunNumber;
  newResult.LumiBlock = averageRate.LumiBlock;

  vector<std::string> xLabels, yLabels;

  yLabels.push_back(aveLab);
  yLabels.push_back(fracLab);
  yLabels.push_back(ninetyPercentLab);
  yLabels.push_back(totLab);

  TAxis *xAxis = histogram->GetXaxis();
  TAxis *yAxis = histogram->GetYaxis();

  if(xAxis == 0 || yAxis == 0) {
    std::cerr << "CalculateNewResult: nul axis labels" << std::endl;
    return false;
  }

  THashList *inXlabels = xAxis->GetLabels();
  if(inXlabels == 0) {
    std::cerr << "CalculateNewResult: nul label hash" << std::endl;
    return false;
  }

  TIterator *inXiter = inXlabels->MakeIterator();
  if(inXiter == 0) {
    std::cerr << "CalculateNewResult: inXlabels->MakeIterator() call failed" << std::endl;
    return false;
  }

  while (TObjString *lab = (TObjString *) inXiter->Next()) {
    const char *cstr = lab->GetString();
    const std::string str = cstr;
    std::string chainName = str.substr(prefLength);
    //std::cout << str << " --> " << chainName << std::endl;
    xLabels.push_back(chainName);
  }

  if(xLabels.size() == 0 || yLabels.size() == 0) {
    std::cerr << "CalculateNewResult: either x or y label vector has zero length" << std::endl;
    return false;
  }

  newResult.format(xLabels, yLabels);
  newResult.clear(); // initializes data to 0

  int nXBins = histogram->GetNbinsX();
  int nYBins = histogram->GetNbinsY();
  
  /* use the algoIn rate if available, the next best is the prescale rate */
  std::string rateLabel = "prescale";
  if( std::find(averageRate.YLabels.begin(), averageRate.YLabels.end(), "algoIn") != averageRate.YLabels.end())
    rateLabel = "algoIn";
  static bool firstCall = true;
  if(firstCall) {
    firstCall = false;
    std::cout << "Using " << rateLabel << " for rate computations" << std::endl;
  }

  
  for( int ix = 1; ix <= nXBins; ix++) {

    const std::string &chainName = xLabels[ix-1];

    float rate = 0;
    if( !averageRate.get(chainName, rateLabel, rate) ) {
      std::cout << "CPUReceiver:: " << chainName << " not found in averageRate TP" << std::endl;
    }

    float ave = 0, tot = 0;
    for(int iy = 1; iy <= nYBins; iy++) {
      tot += histogram->GetBinContent(ix, iy);
      if(iy != 1)
	ave += histogram->GetBinContent(ix, iy) * yAxis->GetBinCenter(iy);
      else
	ave += histogram->GetBinContent(ix, iy) * .001; // this is a kludge
      // the bin size is too coarse and if a chain is activated frequently but just returns
      // it wildly distorts the timing so I legislate that the first bin is worth 1 microsec.
    }

    if(tot > 0.1) {
      ave /= tot;
    }

    float overFlow = histogram->GetBinContent(ix, nYBins+1);
    if(overFlow + tot > 0.1)
      overFlow = overFlow/ (overFlow + tot);

    float totalCPU = ave * rate;

    if(nFarmNodes != 0)
      totalCPU /= nFarmNodes;

    // find minimum time which is greater than the execution time of 90% of the events 

    float runningTot = 0;
    float time90 = 0;
    float n90 = 0.9 * (tot + overFlow);
    if(overFlow + tot > 0.1) {
      int iy = 0;
      for(iy = 1; iy <= nYBins; iy++) {
	runningTot += histogram->GetBinContent(ix, iy);
	if(runningTot >= n90)
	  break;
      }
      time90 = yAxis->GetBinCenter(iy) + yAxis->GetBinWidth(iy);
    }

    newResult.set(chainName, aveLab, ave);
    newResult.set(chainName, fracLab, overFlow);
    newResult.set(chainName, ninetyPercentLab, time90);
    newResult.set(chainName, totLab, totalCPU);


  }
  return true;
}

static void PrintTP(TimePoint_IS &tp) {

  std::vector<std::string>::const_iterator xl;
  for( xl = tp.XLabels.begin(); xl != tp.XLabels.end(); xl++) {

    std::cout << *xl << ": ";
      
    std::vector<std::string>::const_iterator yl;
    for( yl = tp.YLabels.begin(); yl != tp.YLabels.end(); yl++) {
      
      float val;
      tp.get(*xl, *yl, val);
      std::cout << val << ", ";
    }
    std::cout << std::endl;
  }
}

void CPUReceiver::RemoveOutputTP()
{
  m_dict.remove( m_full_out_name );
}

