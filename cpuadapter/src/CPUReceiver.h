 /**
    @file CPUReceiver.h

    Define a callback for receiving a timing histogram and computing
    CPU usage.

    @author M. Medinnis
 */

#ifndef CPU_RECEIVER_H
#define CPU_RECEIVER_H

#include <vector>
#include <ipc/core.h>
#include <ipc/partition.h>

#include <is/infoiterator.h>
#include <is/inforeceiver.h>
#include <is/infodictionary.h>
#include <is/infostream.h>

#include <owl/time.h>

#include <oh/core/GraphData.h>
#include <oh/core/ProfileData.h>
#include <oh/core/HistogramData.h>
#include <oh/OHUtil.h>
#include <oh/exceptions.h>
#include <oh/OHRootReceiver.h>
#include <oh/OHSubscriber.h>
#include <TH2F.h>
#include <iostream>
#include <string>
#include <memory>

#include "TRP/TimePoint_IS.h"

using std::vector;

/**
  CPUReceiver actually does essentially all of the work. 
*/
class CPUReceiver : public OHRootReceiver {
 public:

  CPUReceiver(
	      const std::string &partition_name,
	      const std::string &trp_server,
	      const std::string &out_name,
	      const std::string &rate_name_str,
	      const std::string &farm_name,
	      const std::string &farm_prefix
	      );  
 public:
  
  /** Not implemented. */
  void Run();

  /** only the first receive is implemented. The following declarations
  just keep the compiler from issuing warnings */
  void receive(OHRootHistogram & histogram);
  void receive( std::vector<OHRootHistogram*> & ){;}
  void receive( OHRootGraph & ){;}
  void receive( std::vector<OHRootGraph*> & ){;}
  void receive( OHRootGraph2D & ){;}
  void receive( std::vector<OHRootGraph2D*> & ){;}

  /** For debugging -- produce printout on std::cout, suppress publishing */
  void SetDebugMode(bool debugMode){ DEBUG_MODE = debugMode; }

  /** Get the latest TimePoint_IS objects from TRP and average them to
      produce the average rates since the last time a timing histogram
      was received. */
  bool GetAverageRate(int gatherTime, TimePoint_IS &averageRate);

  /** Retrieve the number of farm nodes in use. This comes from TRP's
      L2_Rate_mon (or EF_...)  unless changed in the configuration
      file. The number is used to compute the CPU usage per node. */
  bool GetNumFarmNodes(int &nNodes);

  /** Computes newResult from the given histogram, averageRate and nFarmNodes */
  bool CalculateNewResult(TimePoint_IS &newResult, TH2F *histogram, TimePoint_IS &averageRate, int nFarmNodes);
  void RemoveOutputTP();

 private:

  bool DEBUG_MODE;

  std::string m_his_server_str;
  std::string m_his_provider_str;
  std::string m_his_name_str;
  
  std::string m_trp_server;
  std::string m_out_name;
  std::string m_rate_name;
  std::string m_farm_name; // name of the farm timepoint on the TRP server
  std::string m_farm_prefix;

  int              m_lastUpdate; // in seconds since m_refTime
  long long        m_refTime;
  ISInfoDictionary m_dict;
  std::string      m_full_out_name;
  std::string      m_full_rate_name;
  std::string      m_full_farm_name;

  static unsigned int m_TPupdateInterval;
  const static long long m_oneMillion = 1000000;
  const static size_t prefLength;

  const static std::string aveLab;
  const static std::string fracLab;
  const static std::string ninetyPercentLab;
  const static std::string totLab;

};


#endif
