#ifndef HLTRATEADAPTERSUMRC_H
#define HLTRATEADAPTERSUMRC_H
/** 
 * @file HLTRateAdapterSum.cpp
 * @author Antonio Sidoti
 * @date 21/01/2009
 * @brief Gets hlt histo (i.e. rates) containing rates from OH
 * @ use its own gatherer
 */

#include <owl/regexp.h>
#include <ipc/core.h>
#include <ipc/partition.h>
#include <is/inforeceiver.h>
#include <is/serveriterator.h>
#include <is/criteria.h>

#include <oh/OHRootReceiver.h>
#include <oh/OHSubscriber.h>
#include <oh/core/HistogramData.h>
#include <owl/regexp.h>
#include <owl/time.h>
#include <string>
#include <vector>
#include <list>
#include <deque>
#include <set>
#include <boost/thread/mutex.hpp>
#include "boost/regex.hpp"
#include "TRP/TimePointAsyncSender.h"
//#include "RunController/Controllable.h"
#include "RunControl/RunControl.h"

/**
 * @brief gets time from histogram
 * @param option defines how this should be done
 **/

namespace HltRateRC { // start of hltRateRC namespace
class GetTimeFromHisto {
 public:
  typedef enum  { EmbededTime=0, PublicationTime,  CurrentTime} TimeOption;
  GetTimeFromHisto(TimeOption option = EmbededTime);
  time_t get(const oh::HistogramData<float> & hda) const;
 private:
  TimeOption m_option;
};


/////////////////////////////////////////////////////////////////////////////
class HLTTimePoint {

 public:
  HLTTimePoint();

  oh::HistogramData<float> sum;
  std::set<std::string>    cpu;  

  //int num_cpu;
  int numInserts;
  int numUnnededInserts;
  time_t timeBegin;
  time_t timeEnd;

  bool insert(const oh::HistogramData<float> & data, const std::string& source, const time_t& time);  
  TimePoint_IS* rate() const;
}; // end of struct
std::ostream& operator<<(std::ostream& str, const HLTTimePoint& );



/////////////////////////////////////////////////////////////////////////////
class ListTimePoint {
 public:
  ListTimePoint( unsigned steps, unsigned interval, unsigned force_interval, unsigned condition);
  void set( unsigned steps, unsigned interval, unsigned force_interval, unsigned condition);
  void cleanup();
  
  enum ConditionMasks { HistoryToShort    = 0x1, // publish when histogram arrives but all past slots are alrady taken
			SecondBinFilled50 = 0x2, // publish when in second bin there is already 50% of wha is in first
			SecondBinFilled90 = 0x4, // as above but 90%
			ThirdBinFilled50  = 0x8, // publish when in third bin number of entries is 50% of what is in first
			NewObjectsCount2  = 0x10,// publish when number of new object since last publication is already 2 times more than in first bin
			NewObjectsCount3  = 0x20,// publish when number of new object since last publication is already 3 times more than in first bin
			ForceAfterInterval= 0x40,  // publish when bin configured though threshold starts to be filled already 
			HoldForInitialPeriod = 0x80 // during intial period ignore all conditions besides the ForceAfterInterval & HistoryToShort
  };
  
  bool is_any_ready();  //!< true if there is some rate info to be sent  
  typedef std::pair<const TimePoint_IS*, const TimePoint_IS*> Collected;
  Collected get(); //!< gets ready to send objects
  void insert(const oh::HistogramData<float>& data, const std::string& source); //!< inserts new readout

  time_t time_0;

  int numSteps;     // usually 24 steps  
  int intervalSize; // usally 10s // ---> so a total of 240 seconds
  std::deque<HLTTimePoint> list_htp;

  int numInserts;
  int numEffectiveInserts;
  int numCpuLastPublished;


  int numUnder;
  int numOver;
  const static std::vector<std::string> s_monitringX;
  const static std::vector<std::string> s_monitringY;
  void setReadyCondition(unsigned c) { m_conditionMask = c; }
 private:
  unsigned m_forcePublishInterval;
  unsigned m_conditionMask;
  bool m_initialPeriod;
  time_t m_oldestSinceLastPublication; 
  time_t m_newestSinceLastPublication; 
  int queue_bin(time_t x) const;  
};
std::ostream& operator<<(std::ostream& str, const ListTimePoint& );



/////////////////////////////////////////////////////////////////////////////
  class HLTRateAdapterSum: public daq::rc::Controllable {
 public:
  HLTRateAdapterSum(std::string name);

  // Virtual methods for RC application
  virtual void configure(const daq::rc::TransitionCmd & args)override;  
  virtual void connect(const daq::rc::TransitionCmd & args)override;
  //  virtual void startTrigger(std::string & args);
  //  virtual void stopTrigger(std::string & args);
  virtual void prepareForRun(const daq::rc::TransitionCmd & args)override;
  virtual void stopROIB(const daq::rc::TransitionCmd & args)override;
  virtual void disconnect(const daq::rc::TransitionCmd & args)override;
  virtual void unconfigure(const daq::rc::TransitionCmd & args)override;
  void SetOptions(std::string partition_name,
		  std::string server_nameIn,
		  std::string server_nameOut,
		  std::string name_obj,
		  std::string cpu_pattern,
		  int num_steps,
		  int int_size,
		  int int_publish,
		  int thresh, 
		  int timeFromHistoOption, 
		  const std::string& readyCondition, std::string mode,
		  std::string histogram_L2_str);
 
 private:
  
  
  std::string m_partitionName;
  std::string m_serverNameIn;
  std::string m_serverNameOut;
  std::string m_nameObj;
  std::string m_publicationName;
  std::string m_histogram_name_regex;
  int m_threshold;
  int m_timeFromHistoOption;
  unsigned m_readyCondition;
  std::vector<std::string> iss_vec; 
  std::string m_mode;


  IPCPartition * m_partition;    // Partition object, provides connection to the IS.
  ISInfoDictionary *m_dict;
  ISServerIterator *m_it;
  ISInfoReceiver   *my_rec;
  TimePointAsyncSender *m_sender;
  ISCriteria *m_criteria;

  //  const char* m_serverPattern;  
  void HLTcallback_rates(ISCallbackInfo *isc);
  void HLTcallback_L2SV(ISCallbackInfo *isc);
  void HLTcallback_L2PU(ISCallbackInfo *isc);
  void HLTcallback_EFD(ISCallbackInfo *isc);

 
  void parseReadyCondition(const std::string& );
  ListTimePoint m_cache;

  long int m_hlt_callback_rate;
  bool m_is_first_event;

};

} // end of hltRateRC namespace
#endif
