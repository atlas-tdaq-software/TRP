/** 
 * @file HLTRateAdapterSum.cpp
 * @author Antonio Sidoti
 * @date 21/01/2009
 * @brief Gets hlt histo containing rates from OH
 */


#include "hltadapterRC/src/HLTRateAdapterSum.h"
#include <oh/OHUtil.h>


#include <ipc/core.h>
#include <iostream>
#include "ers/ers.h"
#include <time.h>
#include <is/serveriterator.h>

#include <ipc/core.h>

#include <oh/core/HistogramData.h>
#include "TRP/TimePoint_IS.h"
#include <rc/RunParams.h>
#include <TH2F.h>
#include "hltadapter/src/HistoToTimePoint.h"


#include "TRP/L2SV.h"
#include "TRP/L2PU.h"
#include "TRP/EFD.h"


#include "TRP/Utils.h"


using namespace std;
using namespace boost;



namespace HltRateRC {

 
ERS_DECLARE_ISSUE(hltadapterRC, ContentIssue, "content issue", ERS_EMPTY)
  

//#define DEBUG

const char* L2PU_pattern_c = ".*(L2PU-[0-9]{4}).*";
const char* EFPT_pattern_c = ".*(pc-tdq-xpu-[0-9]{4}:[0-9]{1,2}).*";

float my_lvl1_out, my_lvl2_out, my_ef_out;
std::string tot_mode;

bool my_verb = true;


regex r_pat;
regex r_ServerNameg; // to match the IS server name

int m_hlt_event;

std::string mpattern_cpu;

static const char* monitoringX[15] = {"providers", "previous_providers",  "inserts", "unneded",
				     "begin", "end", "duration", "under", 
				      "over", "tot_inserts", "oldest", 
				      "newest", "l1_out", "l2_out", "ef_out" };
const std::vector<std::string> ListTimePoint::s_monitringX(monitoringX, monitoringX+15);
static const char* monitoringY[1] = {"value"};
const std::vector<std::string> ListTimePoint::s_monitringY(monitoringY, monitoringY+1);
GetTimeFromHisto *timeFromHisto(0);


/////////////////////////////////////////////////////////////////////////////
HLTRateAdapterSum::HLTRateAdapterSum(std::string name) 
  : daq::rc::Controllable(), //daq::rc::Controllable(name),
    m_partition(0),
    m_dict(0),
    m_sender(0),
    m_criteria(0),
    m_cache(0, 0, 0, 0){
  ERS_DEBUG( 0, "HLTRateAdapterSum::HLTRateAdapterSum()" );
  timeFromHisto = new GetTimeFromHisto((GetTimeFromHisto::TimeOption)m_timeFromHistoOption);
  my_lvl1_out = my_lvl2_out = my_ef_out = 0;
  m_hlt_event = 0;
  m_is_first_event = true;
  m_hlt_callback_rate = 0;
}




void HLTRateAdapterSum::SetOptions(std::string partition_name,
				   std::string server_nameIn, // -> "Histogramming-EF-.*"
				   std::string server_nameOut, // ISS_TRP
				   std::string name_obj,   // EF_Rate
				   std::string cpu_pattern, // "pc-tdq"
				   int num_steps,
				   int interval_size,
				   int force_publish_interval,
				   int thresh,
				   int timeFromHistoOption, 
				   const std::string& readyCondition, 
				   std::string mode, 
				   std::string histogram_name_regex){
  
  m_partitionName = partition_name;
  m_serverNameIn = server_nameIn;
  m_serverNameOut = server_nameOut; 
  m_nameObj = name_obj;
  m_publicationName = m_serverNameOut + "." + m_nameObj;
  m_threshold = thresh;
  m_timeFromHistoOption = timeFromHistoOption;
  r_pat = cpu_pattern.c_str(); 
  r_ServerNameg = server_nameIn.c_str();
  timeFromHisto = new GetTimeFromHisto((GetTimeFromHisto::TimeOption)m_timeFromHistoOption);
  parseReadyCondition(readyCondition);
  my_lvl1_out = my_lvl2_out = my_ef_out = 0;
  m_mode = mode;
  tot_mode = mode;
  m_hlt_event = 0;
  m_is_first_event = true;
  m_hlt_callback_rate = 0;
  m_histogram_name_regex = histogram_name_regex;
  m_cache.set(num_steps, interval_size, force_publish_interval,m_readyCondition );
}


  void HLTRateAdapterSum::configure(const daq::rc::TransitionCmd & args){
  ERS_INFO("in configure ");
}

  void HLTRateAdapterSum::connect(const daq::rc::TransitionCmd & args){
  ERS_INFO("in connect ");

}

  //  void HLTRateAdapterSum::startTrigger(std::string & /*s*/){
  void HLTRateAdapterSum::prepareForRun(const daq::rc::TransitionCmd & args){
  ERS_INFO("In prepareForRun");

 if (!m_partition) m_partition = new IPCPartition( m_partitionName );
 my_rec = new ISInfoReceiver(*m_partition); // this is serazlized
 m_it = new ISServerIterator(*m_partition);
 m_criteria = new ISCriteria(m_histogram_name_regex, oh::HistogramData<float>::type(), 
			     ISCriteria::AND ); // name obj is ".*Rate10s"
 m_dict = new ISInfoDictionary(*m_partition );
 m_sender = new TimePointAsyncSender(m_dict, m_publicationName);
 unsigned count(0);
 iss_vec.clear();
  while ((*m_it)()) {
    cmatch res_ServerNameg;
    if(regex_search(m_it->name(), res_ServerNameg, r_ServerNameg)){ // match the server name
      iss_vec.push_back(m_it->name());
      try {
	my_rec->subscribe(m_it->name(), *m_criteria ,
		       &HLTRateAdapterSum::HLTcallback_rates, this);
      } catch(...){
	ers::warning(hltadapterRC::ContentIssue(ERS_HERE, "Cannot subscribe to : " + boost::lexical_cast<std::string> (m_it->name()) ));
      }
      //      ERS_DEBUG(3, "Subscribing to IS server: " << it.name());
      ERS_INFO("Subscribing to IS server: " << m_it->name());
      count++;
    }
  }
  ERS_INFO("Subscribed to: " << count << " IS servers");
  
  // subscribe to RunCtrlStatistics to get overall rates
  
  ISCriteria L2SV_crit("L2SV-SUM", L2SV::type(), ISCriteria::AND );
  ISCriteria L2PU_crit("L2PU-SUM", L2PU::type(), ISCriteria::AND );
  ISCriteria  EFD_crit( "EFD-SUM",  EFD::type(), ISCriteria::AND );
  try {
    my_rec->subscribe("RunCtrlStatistics",  L2SV_crit, &HLTRateAdapterSum::HLTcallback_L2SV, this);
  } catch(...) {
    ers::warning( hltadapterRC::ContentIssue(ERS_HERE, "Cannot subscribe to RunCtrlStatistics" ));
  }
  try {
    my_rec->subscribe("RunCtrlStatistics",  L2PU_crit, &HLTRateAdapterSum::HLTcallback_L2PU, this);
  } catch(...){
    ers::warning(hltadapterRC::ContentIssue(ERS_HERE, "Cannot subscribe to RunCtrlStatistics" ));
  }
  try {
    my_rec->subscribe("RunCtrlStatistics",   EFD_crit, &HLTRateAdapterSum::HLTcallback_EFD, this);
  } catch(...){
    ers::warning(hltadapterRC::ContentIssue(ERS_HERE, "Cannot subscribe to RunCtrlStatistics" ));
  }

  
}

  //  void HLTRateAdapterSum::stopTrigger(std::string & /*s*/){
  void HLTRateAdapterSum::stopROIB(const daq::rc::TransitionCmd & args){
  ERS_INFO("In StopROIB");

  ISCriteria L2SV_crit("L2SV-SUM", L2SV::type(), ISCriteria::AND );
  ISCriteria L2PU_crit("L2PU-SUM", L2PU::type(), ISCriteria::AND );
  ISCriteria  EFD_crit( "EFD-SUM",  EFD::type(), ISCriteria::AND );
  try {
    my_rec->unsubscribe("RunCtrlStatistics",L2SV_crit);
  } catch( daq::is::RepositoryNotFound &ex) {
    ers::warning(hltadapterRC::ContentIssue(ERS_HERE, "Repository not found " +
					  boost::lexical_cast<std::string> (ex.what())));
  } catch( daq::is::SubscriptionNotFound &ex){
    ers::warning(hltadapterRC::ContentIssue(ERS_HERE, "Subscription not found " +
					  boost::lexical_cast<std::string> (ex.what())));
  } catch(...){
    ers::warning(hltadapterRC::ContentIssue(ERS_HERE, "Maremma Maiala!"));
  }
  try {
    my_rec->unsubscribe("RunCtrlStatistics",L2PU_crit);
  } catch( daq::is::RepositoryNotFound &ex) {
    ers::warning(hltadapterRC::ContentIssue(ERS_HERE, "Repository not found " +
					  boost::lexical_cast<std::string> (ex.what())));
  } catch( daq::is::SubscriptionNotFound &ex){
    ers::warning(hltadapterRC::ContentIssue(ERS_HERE, "Subscription not found " +
					  boost::lexical_cast<std::string> (ex.what())));
  } catch(...){
    ers::warning(hltadapterRC::ContentIssue(ERS_HERE, "Maremma Maiala!"));
  }
  try {
    my_rec->unsubscribe("RunCtrlStatistics",EFD_crit);
  } catch( daq::is::RepositoryNotFound &ex) {
    ers::warning(hltadapterRC::ContentIssue(ERS_HERE, "Repository not found " +
					  boost::lexical_cast<std::string> (ex.what())));
  } catch( daq::is::SubscriptionNotFound &ex){
    ers::warning(hltadapterRC::ContentIssue(ERS_HERE, "Subscription not found " +
					  boost::lexical_cast<std::string> (ex.what())));
  } catch(...){
    ers::warning(hltadapterRC::ContentIssue(ERS_HERE, "Maremma Maiala!"));
  }
  
  ISCriteria my_crit(m_histogram_name_regex, oh::HistogramData<float>::type(), 
		     ISCriteria::AND );
  for(std::vector<std::string>::iterator its=iss_vec.begin();its != iss_vec.end();++its){
    regex r_ServerNameg;
    cmatch res_ServerNameg;
    r_ServerNameg = m_serverNameIn.c_str();
    std::cout << "match:" << *its << "  " << r_ServerNameg << std::endl;
    if(regex_search((*its).c_str() , res_ServerNameg, r_ServerNameg)){ // match the server name
      std::cout << "unsubscribing to:" << *its << std::endl;
      try {
	my_rec->unsubscribe(*its, my_crit);
      }catch ( daq::is::RepositoryNotFound &ex) {
	ers::warning(hltadapterRC::ContentIssue(ERS_HERE, "Repository not found " +
					      boost::lexical_cast<std::string> (ex.what())));
      } catch( daq::is::SubscriptionNotFound &ex){
	ers::warning(hltadapterRC::ContentIssue(ERS_HERE, "Subscription not found " +
					      boost::lexical_cast<std::string> (ex.what())));	
      } catch(...){
	ers::warning(hltadapterRC::ContentIssue(ERS_HERE, "Maremma Maiala!"));
      }
    }
  }
  m_cache.cleanup(); // this is crucial !!!
  delete m_sender; // this I don't know
  
}


  void HLTRateAdapterSum::disconnect(const daq::rc::TransitionCmd & args){
  ERS_INFO("In Disconnect");  

}


  void HLTRateAdapterSum::unconfigure(const daq::rc::TransitionCmd & args){
  ERS_INFO("In Unconfigure");  
}



/////////////////////////////////////////////////////////////////////////////
void HLTRateAdapterSum::HLTcallback_L2SV(ISCallbackInfo *isc){
  if(isc->reason() == ISInfo::Deleted) return;
  L2SV my_l2sv;
  if(m_is_first_event)
    ERS_INFO("First Event! In HLTcallback_L2SV " );
  
  isc->value(my_l2sv);
  my_lvl1_out = my_l2sv.IntervalEventRate;
}

void HLTRateAdapterSum::HLTcallback_L2PU(ISCallbackInfo *isc){
  if(isc->reason() == ISInfo::Deleted) return;
  if(m_is_first_event)
    ERS_INFO("First Event! In HLTcallback_L2PU " );
  L2PU my_l2pu;
  isc->value(my_l2pu);
  my_lvl2_out = my_l2pu.LVL2IntervalRate;
}


void HLTRateAdapterSum::HLTcallback_EFD(ISCallbackInfo *isc){
  if(isc->reason() == ISInfo::Deleted) return;
  if(m_is_first_event)
    ERS_INFO("First Event! In HLTcallback_EFD " );
  EFD my_efd;
  isc->value(my_efd);
  my_ef_out = my_efd.RateOut;
}



/////////////////////////////////////////////////////////////////////////////
void HLTRateAdapterSum::HLTcallback_rates(ISCallbackInfo *isc){
  if(isc->reason() == ISInfo::Deleted) return;
  if(m_is_first_event)
    ERS_INFO(" First Event: HLTcallback_rates" );
  m_is_first_event = false;
  my_verb=false;
  // 1) receive histos from all the PUs
  oh::HistogramData<float> hda; // do we need to create this object on each callback
  isc->value(hda);
  //  std::string h_name_tmp = isc->name(); // name of the histogram 
  std::string provider(isc->name());
  std::size_t firstdot(provider.find_first_of('.')+1);
  std::size_t lasttdot(provider.find_last_of('.'));  
  provider = provider.substr(firstdot, lasttdot - firstdot);
  ++m_hlt_callback_rate;
  if(m_hlt_callback_rate%2000==0) my_verb=true;
  if(my_verb){
    ERS_LOG("Provider is: " << provider <<  "  numcallback:" << m_hlt_callback_rate  << "  time:" << isc->time());
    time_t t = timeFromHisto->get(hda);
    OWLTime my_o(t);
    ERS_LOG("Time from histo is: " << my_o);
    //m_is_first_event = true;
    ERS_LOG("Dumping first TP from cache BEFORE inserting:");
    if(m_cache.list_htp.size()>=3){
      ERS_LOG(">> my_hlt_tp_0" <<  m_cache.list_htp[0] << "  1 " <<  
	       m_cache.list_htp[1] << "  2 :" <<  m_cache.list_htp[2]   );
    } else {
      ERS_LOG(">> Not enough list_htp to show:  Size is" << 
	       m_cache.list_htp.size() );
    }
  }
  
  m_cache.insert(hda, provider);
  
  if(my_verb){
    ERS_LOG("Dumping first TP from cache AFTER inserting:");
    if(m_cache.list_htp.size()>=3){
      ERS_LOG(">> my_hlt_tp_0" <<  m_cache.list_htp[0] << "  1 " <<  
	       m_cache.list_htp[1] << "  2 :" <<  m_cache.list_htp[2]   );
    } else {
      ERS_LOG(">> Not enough list_htp to show:  Size is" << 
	       m_cache.list_htp.size() );
    }
  }

  
  if ( m_cache.is_any_ready() ) {
    ++m_hlt_event;
    //ERS_DEBUG(3, "Cache got one rate ready " << m_cache );  
    if(m_hlt_event%1000==0){
      ERS_LOG("Event = " << m_hlt_event);
      ERS_LOG("============== Cache is READY to be published"); 
      ERS_LOG("Dumping1 Cache BEFORE getting" << m_cache );
    }
    ListTimePoint::Collected h = m_cache.get();
    if(h.first) {
       if(m_hlt_event%1000==0){
	 ERS_LOG("Dumping2 Cache AFTER getting and h.first valid" << m_cache );
//	  std::cout << "%%%%%" << h.first->print(std::cout) << std::endl;
       }
       m_sender->send(h.first);
    } else {
      //      ERS_DEBUG(3, "Got NULL rate TimePoint to send" );
      ERS_LOG("Got NULL rate TimePoint to send" );
    }
    if(h.second) {
      m_sender->send(h.second, m_publicationName+"_mon");
    } else {
      ERS_DEBUG(3, "Got NULL mon TimePoint to send" );
    }
#ifdef DEBUG
    std::cout << "netstat Before Publ EST:" << std::endl;
    system("netstat -a -t | grep \"ESTABLISHED\" | wc -l");
#endif

  } else {
    ERS_DEBUG(3, "Nothing ready" );
  }
} // end of callback hlt rates method


void HLTRateAdapterSum::parseReadyCondition(const std::string& cmd) {  
  m_readyCondition = 0; 

  if ( cmd.find("HistoryToShort") != std::string::npos )
    m_readyCondition |= ListTimePoint::HistoryToShort;
  
  if ( cmd.find("SecondBinFilled50") != std::string::npos )
    m_readyCondition |= ListTimePoint::SecondBinFilled50;  

  if ( cmd.find("SecondBinFilled90") != std::string::npos )
    m_readyCondition |= ListTimePoint::SecondBinFilled90;  

  if ( cmd.find("ThirdBinFilled50") != std::string::npos )
    m_readyCondition |= ListTimePoint::ThirdBinFilled50;

  if ( cmd.find("NewObjectsCount2") != std::string::npos )
    m_readyCondition |= ListTimePoint::NewObjectsCount2;    

  if ( cmd.find("NewObjectsCount3") != std::string::npos )
    m_readyCondition |= ListTimePoint::NewObjectsCount3;    

  if ( cmd.find("ForceAfterInterval") != std::string::npos )
    m_readyCondition |= ListTimePoint::ForceAfterInterval;    

  if ( cmd.find("HoldForInitialPeriod") != std::string::npos )
    m_readyCondition |= ListTimePoint::HoldForInitialPeriod;    

  //  if ( cmd.find("") != std::string::npos )
  //    m_readyCondition |= ListTimePoint::;    

  if (m_readyCondition == 0 ) {// nothing was set
    ERS_LOG("Conditions mask used to send histograms was 0, resetting it to the default!");
    m_readyCondition = ListTimePoint::HistoryToShort | ListTimePoint::SecondBinFilled90;
  }
  
  ERS_LOG("Conditions mask used to send histograms: " << std::hex  << "0x" <<  m_readyCondition);
  m_cache.setReadyCondition(m_readyCondition);
}



//////////////////////////////////////////////////////////////////////////////////
GetTimeFromHisto::GetTimeFromHisto(TimeOption option) 
  : m_option(option) {}

/////////////////////////////////////////////////////////////////////////////
time_t GetTimeFromHisto::get(const oh::HistogramData<float> & hda) const {

  if ( m_option == CurrentTime ) {
    ERS_DEBUG(3, "using current time as timestamp for histogram" );
    return time(0);
  }
  if ( m_option ==  PublicationTime ) {
    ERS_DEBUG(3, "using publication time as timestamp for histogram " <<  hda.time() );
    // ERS_LOG("using publication time as timestamp for histogram " <<  hda.time() );
    return hda.time().c_time();
  }



  // actual time of creation of histo is put in the last column
  // year, number of days of the year, hour, min and secs.

  int last_bin = hda.get_bin_count(oh::Axis::X);
  //  int labels_num = (int)hda.get_indices(oh::Axis::X).size();
  //  int last_bin = -1;
  //  if ( labels_num > 0 )
  //    last_bin = (int)hda.get_indices(oh::Axis::X)[labels_num-1];

  // check that last_bin in x corresponds to time label
  // and first to total
  if( hda.get_labels(oh::Axis::X).front() != "total" 
      || hda.get_labels(oh::Axis::X).back() != "time" 
      || last_bin < 1  ){ 
    throw hltadapterRC::ContentIssue(ERS_HERE, "Cannot get time and total rate from oh::HistogramData");
  } 
  
  struct tm time_ptr;
  time_ptr.tm_year = OWLTime(time(0)).year()-1900;
  time_ptr.tm_hour = (int)hda.get_bin_value(last_bin,2,0)-1;
  time_ptr.tm_mon  = 0;
  time_ptr.tm_mday = 0;
  time_ptr.tm_min =  (int)hda.get_bin_value(last_bin,3,0);
  time_ptr.tm_sec =  (int)hda.get_bin_value(last_bin,4,0);
  
  
  time_t t = OWLTime(time_ptr).c_time();
  unsigned yday = (int)hda.get_bin_value(last_bin,1,0)+1;
  t += yday * 24 * 3600;

  // according to:
  // http://cpptruths.blogspot.com/2005/10/return-value-optimization.html
  // it is better to create return value in place
  ERS_DEBUG(3, "Time extracted from histogram " << OWLTime(t) );
  return t;
}

/////////////////////////////////////////////////////////////////////////////
HLTTimePoint::HLTTimePoint() 
  : numInserts(0), numUnnededInserts(0), timeBegin(0), timeEnd(0) {
  cpu.clear();
}

/////////////////////////////////////////////////////////////////////////////
TimePoint_IS* HLTTimePoint::rate() const {
  if ( cpu.empty() ) 
    return 0;
  TimePoint_IS *t0 = new TimePoint_IS();  
  float trp_rate = -9;
  if(tot_mode == "l2corr"){
    if(t0->get("total","output",trp_rate)){
      t0->Scale(my_lvl2_out/trp_rate);
    }
  }
  if(tot_mode == "efcorr"){
    if(t0->get("total","output",trp_rate)){
      t0->Scale(my_ef_out/trp_rate);
    }
  }
  
  
  bool conv_stat = hltadapter_imp::convert(&sum,t0);   
  
  if (conv_stat) {

    // clear time stamp from the data
    t0->set("time", "input", 0);
    t0->set("time", "prescale", 0);
    t0->set("time", "raw", 0);
    t0->set("time", "output", 0);
    return t0;
  }
  else
    delete t0;
  return 0;
}

/////////////////////////////////////////////////////////////////////////////
bool HLTTimePoint::insert(const oh::HistogramData<float>& newdata, 
			  const std::string& source, const time_t& ctime) {
  bool ret = false;
  numInserts++;
  if ( cpu.empty() ) {
    // this is first time we need to insert rate data
    sum = newdata; // prepare structure
    cpu.insert(source);
    timeBegin = ctime;
    timeEnd   = ctime;
  }
  
  if ( cpu.find(source) == cpu.end() ) {
    // do real insert
    size_t sz = sum.get_bins_size( );
    for ( unsigned int i = 0; i < sz; ++i ) {
      sum.get_bins_array( )[i] += newdata.get_bins_array( )[i];
    }
    cpu.insert(source);
    ret = true;
    if ( ctime < timeBegin )
      timeBegin = ctime;
    if ( ctime > timeEnd )
      timeEnd = ctime;
    
  } else {
    numUnnededInserts++;
  }
  return ret;
}

/////////////////////////////////////////////////////////////////////////////
ostream& operator<<(ostream& str, const HLTTimePoint& p) {
  str << " num CPUs received: " << p.cpu.size()
      << " num inserts: " << p.numInserts
      << " num ~inserts: " << p.numUnnededInserts    
      << " begin: " << p.timeBegin
      << " end: " << p.timeEnd 
    //<< " total input rate:" << p.sum.get_bin_value(0,0,0)
      << std::endl;
  std::copy(p.cpu.begin(), p.cpu.end(), std::ostream_iterator<std::string>(str, " "));
  str << std::endl;
  return str;
}

/////////////////////////////////////////////////////////////////////////////
ListTimePoint::ListTimePoint(unsigned steps, unsigned interval, unsigned force_interval, 
			     unsigned cond) 
  : time_0(0), numSteps(steps), intervalSize(interval), list_htp(numSteps), 
    numInserts(0), 
    numEffectiveInserts(0),
    numUnder(0),
    numOver(0),
    m_forcePublishInterval(force_interval),
    m_conditionMask(cond), 
    m_initialPeriod(true)
{}

void ListTimePoint::set(unsigned steps, unsigned interval, unsigned force_interval, unsigned condition){
  numSteps = steps;
  intervalSize = interval;
  m_conditionMask = condition;
  m_forcePublishInterval = force_interval;
  list_htp.resize(numSteps);
}

void ListTimePoint::cleanup(){
  list_htp.clear();
  list_htp.resize(numSteps);
  numInserts = 0;
  numEffectiveInserts = 0;
  numCpuLastPublished = 0;
  numUnder = 0;
  numOver = 0;
  time_0 = 0;
}

/////////////////////////////////////////////////////////////////////////////
void ListTimePoint::insert(const oh::HistogramData<float>& data, const std::string& source) {
  time_t t = timeFromHisto->get(data);
  numInserts++;


  m_oldestSinceLastPublication = std::min(t, m_oldestSinceLastPublication);
  m_newestSinceLastPublication = std::max(t, m_newestSinceLastPublication);

  // first object we ever get initializes the time
  // the time_0 will be aligned with localtime % interval
  if ( time_0 == 0 ) {

    time_0 = t - OWLTime(t).sec()%intervalSize; 
    ERS_DEBUG(1, "Cache start time intialized to: " << OWLTime(time_0) << " " << OWLTime(t) << " s" <<  OWLTime(t).sec()%intervalSize );
  }
  // calculate the bin
  int bin = queue_bin(t);
  // 
  //  if(bin<0) bin = -1;  
  //  if(bin >= m_cache.num_steps ) bin = m_cache.num_steps; 

  ERS_DEBUG(2, "Time bin: " << bin << " queue start: " << OWLTime(time_0) << " histogram time: " << OWLTime(t) );
  numInserts++;
  
  if(bin < 0){
    ++numUnder;
  } else if(bin >= numSteps){
    ++numOver;
  } else {
    // we got right nuber of bins (note in all other cases we lose counts)
    HLTTimePoint& tp = list_htp.at(bin);
    bool has_insert = tp.insert(data, source, t);
    if ( has_insert ){
      numEffectiveInserts++;
    }

  }
}

/////////////////////////////////////////////////////////////////////////////
int ListTimePoint::queue_bin(time_t x) const {
  return (int)( (float)(x - time_0)/(float)intervalSize);
}

/////////////////////////////////////////////////////////////////////////////
bool ListTimePoint::is_any_ready() {

  unsigned in_first  = list_htp[0].cpu.size();
  unsigned in_second = list_htp[1].cpu.size();
  unsigned in_third  = list_htp[2].cpu.size();
  ERS_LOG("mask:" << std::hex << m_conditionMask << " in_first " << in_first << " in_second " << in_second << " in_third " << in_third );
  if ( m_conditionMask & SecondBinFilled50 && in_second > 0.5*in_first  ) {
    ERS_LOG("Publishing because in second bin there is already 50% of this what was in 1st: " 
	     << in_first << " in 2nd: " << in_second );
    return true;
  }
  
  if ( m_conditionMask & SecondBinFilled90 && in_second > 0.9*in_first  ) {
    ERS_LOG("Publishing because in second bin there is already 90% of this what was in 1st: " 
	     << in_first << " in 2nd: " << in_second );
    return true;
  }
  
  if ( m_conditionMask & ThirdBinFilled50 && in_third > 0.5*in_first  ) {
    ERS_LOG("Publishing because in second bin there is already 50% of this what was in 1st: " 
	     << in_first << " in 2nd: " << in_second );
    return true;
  }
  
  if ( m_conditionMask & NewObjectsCount2  && numEffectiveInserts > 2.* in_first ) {
    ERS_LOG("Publishing because there is already 2 times more histograms arrived that is in 1st slot " );
    return true; 
  }
  
  if ( m_conditionMask & NewObjectsCount3  && numEffectiveInserts > 3.* in_first ) {
    ERS_LOG("Publishing because there is already 2 times more histograms arrived that is in 1st slot " );
    return true; 
  }
  
  if ( m_conditionMask & ForceAfterInterval && list_htp[m_forcePublishInterval].cpu.empty() ) {
    ERS_LOG("Publishing because in :" << m_forcePublishInterval << " bin there are already entries" );
    return true; 
  }

  time_t temp_time = time(NULL);
  
  int bin = queue_bin(temp_time);

#ifdef DEBUG
  long int diff = temp_time-time_0; 
  std::cout << "temp_time:" << temp_time << "  m_cache.time_0:" << time_0 
	    << "   m_cache.m_interval_size:" << intervalSize 
	    <<"  m_cache.num_steps:" << numSteps << std::endl;
  std::cout << "diff:" << diff << "   bin:" <<  bin << std::endl; 
  std::cout << "Num Prev Cpu:" <<numCpuLastPublished  << std::endl;
  std::cout << "Num Act Cpu:" << list_htp[0].cpu.size() << std::endl;
#endif
    
  if   ( m_conditionMask & HistoryToShort && bin >= numSteps){
    ERS_LOG("Publishing because queue is to short to hold new entries as it starts: " 
	      << OWLTime(time_0) << " now: " << OWLTime(temp_time));
    return true;
  }

  return false;  
}

/////////////////////////////////////////////////////////////////////////////
std::pair<const TimePoint_IS*, const TimePoint_IS*> ListTimePoint::get() {
  // figure out which rate is ready to be published
  //  if ( ! is_any_ready() )
  //    return make_pair((const TimePoint_IS*)0,(const TimePoint_IS*)0);
  
  TimePoint_IS* monitoring = new TimePoint_IS();
  TimePoint_IS* rate(0);
  monitoring->format(ListTimePoint::s_monitringX, s_monitringY);

  {
    const HLTTimePoint& ready = list_htp.front();
    monitoring->set( "providers", "value", ready.cpu.size());
    monitoring->set( "previous_providers",  "value", numCpuLastPublished);
    monitoring->set( "inserts",   "value", ready.numInserts);    
    monitoring->set( "unneded",   "value", ready.numUnnededInserts);
    monitoring->set( "begin",     "value", OWLTime(ready.timeBegin).sec());
    monitoring->set( "begin",     "value", OWLTime(ready.timeEnd).sec());
    monitoring->set( "duration",  "value", ready.timeEnd - ready.timeBegin);

    monitoring->set( "under",     "value", numUnder);    
    monitoring->set( "over",      "value", numOver);
    monitoring->set( "oldest",       "value", time_0 - m_oldestSinceLastPublication); // this tells how many updates the whole queue got
    monitoring->set( "newest",       "value", m_newestSinceLastPublication - time_0 ); // this tells how many updates the whole queue got
    monitoring->set( "tot_inserts",  "value", numInserts); // this tells how many updates the whole queue got        
    monitoring->set( "l1_out",  "value", my_lvl1_out); 
    monitoring->set( "l2_out",  "value", my_lvl2_out); 
    monitoring->set( "ef_out",  "value", my_ef_out); 
    
    numCpuLastPublished = ready.cpu.size();   
    rate = ready.rate();
    // use current time_0 as timestamp for this rate measurement
    if (rate) rate->TimeStamp = OWLTime(time_0);
    monitoring->TimeStamp = OWLTime(time_0);
  }
  // we remove one slot but just afterwards add another one so queue lenght stays always the same
  list_htp.pop_front();
  list_htp.push_back(HLTTimePoint());

  // we need to advance in time (by configured interval)
  time_0 += intervalSize;

  // reset stats
  numInserts = 0;
  numEffectiveInserts = 0;
  numOver = 0;
  numUnder = 0;
  m_oldestSinceLastPublication = time_0;
  m_newestSinceLastPublication = time_0;
  m_initialPeriod=false; // after first publication we are not anymore ini initial period

  return make_pair(rate, monitoring);
}

/////////////////////////////////////////////////////////////////////////////
ostream& operator<<(ostream& str, const ListTimePoint& l) {
  str << " list size: " << l.list_htp.size() 
      << " configured size: " << l.numSteps 
      << " interval size: " << l.intervalSize
      << " arrived to late: " << l.numUnder 
      << " arrived to early: " << l.numOver 
      << std::endl;
  for(int it=0;it<3;++it){
    str << " list_htp[" << it << " ].cpu.size():" << l.list_htp[it].cpu.size();
  }
  str << std::endl;
  
  std::deque<HLTTimePoint>::const_iterator i;
  for ( i = l.list_htp.begin(); i != l.list_htp.end() ; ++i ) 
    if (i->cpu.size() != 0 )
      str << *i << std::endl;
  float rate = -1.;
  if((l.list_htp[0].rate())->get( "total", "input", rate)){
    str << "Total Input rate is:" << rate  << std::endl;
  } else {
    str << "Total input rate NO RATE" << std::endl;
  }
  
  return str;
}
} // end of name space
