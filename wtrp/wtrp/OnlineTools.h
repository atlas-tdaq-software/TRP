 /**
    @file OnlineTools.h

    Defines class wtools

    @author M. Medinnis
 */

#ifndef WTRP_ONLINETOOLS
#define WTRP_ONLINETOOLS

#include <vector>

#include "TRP/Branch.h"
#include "TRP/Tree.h"
#include "TRP/TimePoint_IS.h"

#include "TRP/TriggerMenuHelpers.h"

#include "wtrp/TrigConf.h"
#include "wtrp/tpointinfo.h"
#include "wtrp/pagedata.h"
#include "wtrp/rawpage.h"
#include "wtrp/runinfo.h"
#include "wtrp/statuspage.h"
#include "wtrp/streamtable.h"
#include "wtrp/grouptable.h"
#include "wtrp/datatable.h"
#include "wtrp/chaintable.h"
#include "wtrp/l1table.h"
#include "wtrp/cputable.h"

#include "wtrp/pot.h"

#include "wmi/outputstream.h"
#include "wmi/table.h"

#include "is/infodictionary.h"
#include "is/info.h"

#include "ipc/core.h"

namespace wtrp {

  /**
    Tools for use in the online environment.  Here are gathered tools
    which initialize the trigger configuration from the database or
    IS, fill tables from info gathered from IS and interface with wmi
    html formatting classes. The idea was to minimize the dependence
    of the PageData classes with online specific code in the hope that
    they could be used to build a version of wtrp which runs in an
    offline environment. Only wtrp_plugin and TPointInfo use wtools.
  */
  class wtools {

  public:
    /** Make a local copy of the name of the image file used to
	represent the bar graphs in the data tables. */
    static void SetBarFile(std::string name);
    
    /** Reads HLT prescales from IS and fills them into the trigger config (no longer used). */
    static bool UpdatePrescales(TC_level lvl, const ISInfoDictionary &dict, const std::string &serverName, TrigConf *tc);
    
    /** Extracts L1 prescales from L1 TimePoint and fills them into tc */
    static bool SetL1Prescales(TrigConf *tc);
    

    /** Zero the TAV rates of the L1 TimePoint for disabled triggers */
    /* added 17.6.2015 */
    static bool ZeroTAVforDisabledL1Trigs();

    /** Set up tc from the data base. This uses TriggerMenuHelpers (see confadapter). */
    static void SetConfiguration_DB(std::string trigcondb, unsigned int smk, unsigned int L1_psk, 
				    unsigned int HLT_psk, TrigConf *tc);
    
    /** Add the timeStamps from the L1, L2 and EF timePoints to the global preamble. */
    static void TimeStampsToPreamble(bool isRun2);

    /** Produce a wmi page from pageData and write to outStream */
    static void OutputWMIPage(daq::wmi::OutputStream* outStream, const PageData *pageData);
    
    /** Extract the names of streams from tp and copy them to streamNames. */
    static void ExtractStreamNames(const TimePoint_IS &tp, std::set<std::string> &streamNames);
    
    /** Extract the names of groups from tp and copy them to groupNames */
    static void ExtractGroupNames(const TimePoint_IS &tp, std::set<std::string> &groupNames);
    
    /** Fill in the rates summary table which appears on all pages. */
    static void FillGlobalRatesTable( bool isRun2 );
    
    /** Fill rawPage from timePoint */
    static void FillRawPage(RawPage &rawPage,  const TimePoint_IS &timePoint);
    
    /** Update the stream table with new rates */
    static void FillStreamTable(StreamTable &streamTable, bool IsRun2);
    
    /** Update the group table with new rates */
    static void FillGroupTable(GroupTable &groupTable, const TrigConf *tc );
    
    /** Update the L1 table with new rates */
    static void FillL1Table(L1Table &l1Table);
    
    /** Update the status page */
    static void FillStatusPage(StatusPage &statusPage);
    
    /** Update the chain tables with new rates */
    static void FillChainTables(std::map<std::string, ChainTable> &chainTables, 
				const TrigConf *trigConfig);
    
    /** Update the CPU table with new info */
    static void FillCPUTable(CPUTable &cpuTable);
    
    /** Produce a wmi page with the time histories of all parameters of a POT in tabular form */
    static void DumpPOTtoWMIPage(daq::wmi::OutputStream* outStream, 
				 const std::string &POTname, const std::string &fileName);
    
  private:

    struct RateInfo {
      float in;
      float out;
    };
    
    typedef struct RateInfo RateInfo;

    wtools(){;}

    /** Set up configurations based on either Run 1 or Run 2 configurations */
    static void Configure_Run1( confadapter_imp::TriggerMenuHelpers &tmh, TrigConf *tc );
    static void Configure_Run2( confadapter_imp::TriggerMenuHelpers &tmh, TrigConf *tc );


    /** Update the group table -- either Run 1 or Run 2 */
    static void FillGroupTable_Run1(GroupTable &groupTable);
    static void FillGroupTable_Run2(GroupTable &groupTable);

    /** Fill in the rates summary table which appears on all pages. */
    static void FillGlobalRatesTable_Run1();
    static void FillGlobalRatesTable_Run2();

    /** Update the chain tables -- either Run 1 or Run 2 */
    static void FillChainTables_Run1(std::map<std::string, ChainTable> &chainTables, 
				const TrigConf *trigConfig);
    static void FillChainTables_Run2(std::map<std::string, ChainTable> &chainTables, 
				const TrigConf *trigConfig);
    static void FillOneChainTable_Run2(const std::string &groupName,
				const std::string &potFileName,
				std::vector<std::string>::const_iterator &xl,
				const std::map<std::string, RateInfo> &groupRates,
				float inRate, float outRate, float preRate, float algoRate, float rawRate,
				std::map<std::string, ChainTable> &chainTables);

    /** Update the stream table with new rates */
    static void FillStreamTable_Run1(StreamTable &streamTable);
    static void FillStreamTable_Run2(StreamTable &streamTable);

    /** Add the timeStamps to the global preamble. */
    static void TimeStampsToPreamble_Run1();
    static void TimeStampsToPreamble_Run2();

    /** Get a branch from IS. Was used by UpdatePrescales and SetConfiguration_IS -- no longer used. */
    static int GetBranch(const ISInfoDictionary &dict, const std::string &serverName, 
			 const std::string &infoName, std::string &keyName, Branch &aBranch);
    
    /** Set up tc from IS info. No longer used. */ 
    static void SetConfiguration_IS(IPCPartition &partition, const std::string &serverName, TrigConf *tc);
    
    /** Construct a wmi html table from the default DataTable in pageData */
    static daq::wmi::Table *FillHTML_Table(const PageData *pageData); // allocates space. Be sure to delete!
    
    /** Construct a wmi html table from dataTable */
    static daq::wmi::Table *FillHTML_Table(const DataTable *dataTable); // ditto
    
    /** Set up L1 table structure from the L1 tp. The alternative is
	to use the DB. This was intended as a backup in case the DB setup
	failed but now is always used since it makes it easy to exclude
	disabled L1 items.*/
    static void FixupL1ConfigFromTP(L1Table &l1Table, TimePoint_IS &L1_tp);
    
    static const int maxBarPixels;
    static const int barHeight;
    static const int precision;

    static const int ratePrecision;
    static const std::ios_base::fmtflags fixOrSci;
    
    static const std::string barCode;
    static const float fracLim; // (just to keep bars from getting to long in case of a bug)
    static std::string barFile;
    

    static std::map<std::string, RateInfo> L2_groupRates; // filled by FillGroupTable, used by FillChainTables
    static std::map<std::string, RateInfo> EF_groupRates; // ditto
    static std::map<std::string, RateInfo> HLT_groupRates; // ditto

  };

}
#endif
