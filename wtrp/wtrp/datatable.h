 /**
    @file datatable.h

    Defines the DataTable class and associated classes.

    The DataTable class is used for defining data represented in the
    form of a table.  The table is composed of rows (class Row) and
    groups of columns (class ColumnGroup). Each column group is in
    turn composed of Columns. Each ColumnGroup has a key which
    uniquely identifies it and a header.  Each Column has a unique
    key, and a header. The table's cells hold strings.  (In fact, the
    original design called for the cells to hold either strings,
    vectors of strings or vectors of vectors of strings. The
    representation was meant to be always vectors of vectors of
    strings but for convenience there are member functions which allow
    access as any of the three types, as long as the user knows which
    type it is. This however was not made to work completely. However
    there remains some disabled code lines which reflect this fact. I
    hesiate to take them out since it might be useful later if a need
    appears to use DataTable to also include chain steps and
    algorithms.)

    To define a DataTable, first construct it, then define the columns
    and column groups using the respective constructors. Add the
    ColumnGroups to the DataTable in the order that they should appear
    in the finished table. All column groups and columns must be
    defined before the first row is added. Then add the rows. New rows
    can be appended at any time (although I have in mind that
    everything is set up at the beginning. Finally, fill in the data
    cell by cell. It is possible to replace the contents of any cell.

    @author M. Medinnis
 */

#ifndef WTRP_DATATABLE
#define WTRP_DATATABLE

#include <string>
#include <vector>
#include <map>

namespace wtrp {

  /** Differentate between string, vector of strings, vector of vector
      of strings. Only strings are implemented now. */
  enum ContentsType {con_S, con_VS, con_VVS}; 


  /** Class CellContents holds the actual data as well as a type
    indicator (enum ContentsType).  The data is held in the form of a
    string. (For the future this might become a vector of vectors of
    strings -- the most general supported type. In which case, the
    cell would infer which type of data it holds by the constructor
    called. To get the data (if this happens), first query the content
    type then call the appropriate GetCell_... function.
  */
  class CellContents {
    friend class Row;

  public:
        
    /** replace the cell contents with str */
    void Replace(const std::string &str); 
    //void Replace(const std::vector<std::string> &); 
    //void Replace(const std::vector<std::vector<std::string> > &); 
    
    /** Return the contents type (always string) */
    enum ContentsType GetContentsType(){return m_ct;}

    /** Return a contstant point to the contents string */
    const std::string &GetConstCell_S() const;
    //const std::vector<std::string> &GetConstCell_VS() const;
    //const std::vector<std::vector<std::string> > &GetConstCell_VVS() const;

  protected:
    /** Protected constructor. Only a Row object can construct */
    CellContents();
    enum ContentsType m_ct;
    //std::vector<std::vector<std::string> > m_contents;
    std::string m_contents;
    static const std::string m_outOfRange;
    static const std::string m_nocell;
  };
  
  
  /**
    The Column class defines the column by giving it a key, a header
    and type of data in the column. It doesn't actually hold the data
    itself. That's done by the Row class. The Column class is needed
    when defining a row.
  */
  class Column {
    
  public:
    /** Constructor. Parameters:
	-# k: unique key (within the Column's group) 
	-# h: the column header
	-# isSortable: if true, a button is inserted in the html to 
	allow column to be sorted is sortable copies
	-# isLink: if true, contents will be formatted as a link to a file named filePref + contents + .html
	-# filePref: if isLink is true, prefix for file name
	.
    */
    Column(std::string k, std::string h, bool isSortable = true, bool isLink = false, std::string filePref = ""):
      m_key(k), m_header(h), m_isSortable(isSortable), m_isLink(isLink), m_filePref(filePref) {;}

      /** Destructor (empty) */
      ~Column(){;}

      /** Return the column key */
    const std::string &GetKey() const {return m_key;}

    /** Return the column header */
    const std::string &GetHeader() const {return m_header;}

    /** True if column is sortable */
    bool IsSortable() const {return m_isSortable;}

    /** True if column contents should be embedded in a link */
    bool IsALink()const {return m_isLink;}

    /** If IsALink, this is prefixed to the contents to produce a file name */
    const std::string &GetFilePrefix() const {return m_filePref;}

    /** Dump table contents to std::cout */
    void Dump() const;

  protected:
    std::string m_key;
    std::string m_header;
    bool m_isSortable;
    bool m_isLink;
    std::string m_filePref;
  };
  
  class Row;


  /** A column group contains some number of columns (convenient for example for grouping
   the three trigger levels. A column group is identified by a key (must be unique in the
   DataTable) and has a string containing the header which is printed.
  */
  class ColumnGroup {
    friend class Row;

  public:
    /** Constructor taking key and header string as arguments */
    ColumnGroup(std::string k, std::string h): m_key(k), m_header(h){;}

      /** Add a column to the group. Parameters:
	  -# key: a key which is unique within the group
	  -# header: the string to print in the column header
	  -# isSortable: if the column should get a sort button when a sortable copy is made
	  -# isLink: true if the row contents should be turned into links
	  -# filePref: is isLink is true, the filename pointe to has this prefix
	  .
      */
      void AddColumn(const std::string &key, const std::string &header, bool isSortable=true, 
		     bool isLink=false, const std::string &filePref="");

      /** return a pointer to the column with the given key */
    const Column *GetColumn(const std::string &key) const;

    /** return a pointer to the nCol'th column (nCol corresponds to the adding order, starting with 0 */
    const Column *GetColumn(unsigned int nCol) const;

    /** return the number of columns in the group */
    unsigned int GetNColumns() const {return m_columns.size();}

    /** return a reference to the header */
    const std::string &GetHeader() const {return m_header;}

    /** return a reference to the key */
    const std::string &GetKey() const {return m_key;};

    /** dump the ColumnGroup */
    void Dump() const;
  protected:

    std::vector<Column> m_columns; // the order determines the order of the groups in the table
    std::string m_key;
    std::string m_header;
  };
  

  class DataTable;

  /** The Row class holds the data in a format defined by ColumnGroup and Column. See description for
   datatable.h for more detail.
  */
  class Row {

  public:

    /** Constructer, dt is the table to which it belongs. */
    Row(const DataTable *dt);

    /** Destructor (empty) */
    ~Row() {;}

    /** update the cell with the specified key in the specified group */
    void UpdateCell(const std::string &group, const std::string &key, const std::string &contents);

    //void UpdateCell(const std::string &group, const std::string &key, const std::vector<std::string> &contents);
    //void UpdateCell(const std::string &group, const std::string &key, const std::vector<std::vector<std::string> > &contents);

    /** return a pointer to the contents of the cell with the specified key in the specified group */
    const std::string                            *GetContents_S(const std::string &group, const std::string &key) const;
    //const std::vector<std::string>               &GetContents_VS(const std::string &group, const std::string &key) const;
    //const std::vector<std::vector<std::string> > &GetContents_VVS(const std::string &group, const std::string &key) const;
    const std::string *GetContents_S(unsigned int iCol) const;

    /** return the contents of cell with column number iCol -- if it
	is a link, put in the proper html formatting */
    void GetHTMLContents(unsigned int iCol, std::string &contents) const;

    /** return a pointer to the Column object of the iCol'th column */
    Column const* GetColumn(unsigned int iCol) const;

    /** Dump the contents */
    void Dump() const;

  protected:

    const CellContents *GetConstCell(const std::string &groupKey, const std::string &columnKey) const;
    CellContents *GetCell(const std::string &groupKey, const std::string &columnKey);
    const DataTable *m_dt;
    std::vector<CellContents > m_cells;
  };


  /** The DataTable, see comments in the datatable.h file documentation for details. */
  class DataTable {
    friend class Row;
    
  public:

    /** Constructor. Setting isSortable to true will result in an associated "Get sortable copy" button */
    DataTable( bool isSortable = true);
    
    /** Destructor (empty) */
    ~DataTable();

    /** Add an empty row */
    Row *AddRow();

    /** Remove the cell which contains "key" in the column given by groupKey and colKey */
    bool RemoveRow(const std::string &groupKey, const std::string &colKey, const std::string &key);

    /** Return a pointer to the ColumnGroup with the specified key */
    const ColumnGroup *GetColumnGroup(const std::string &key);

    /** Return a pointer to the Column specified by (groupKey, columnKey) */
    const Column *GetColumn(const std::string &groupKey, const std::string &columnKey);

    /** Add cg to the end of the vector of groups */
    void AddColumnGroup(const ColumnGroup &cg){m_columnGroups.push_back(cg);}

    /** Clear the table -- completely resets it */
    void Clear();

    /** Return the vector of ColumnGroups */
    const std::vector<ColumnGroup>  &GetColumnGroups() const {return m_columnGroups;}

    /** Return true if table should be sortable */
    bool IsSortable() const {return m_isSortable;}

    /** Make the table sortable/unsortable */
    void SetSortable(bool canSort) { m_isSortable = canSort;}

    /** Return the table number (corresponds to the contruction order) */
    unsigned int GetTableNum() const { return m_nTable;}

    /** Return the number of column groups in the table */
    unsigned int GetNColumnGroups()const {return m_columnGroups.size();}

    /** Return the total number of columns in the table */
    unsigned int GetNColumns() const;

    /** Return the number of rows in the table */
    unsigned int GetNRows()const {return m_rows.size();}

    /** Return the name of the javascript funtion used to make a new window for sortable copies */
    const std::string &GetMakeWindowFunction() const {return m_newWindowFunction;}

    /** Return the name of the file containing the javascript window making function */
    const std::string &GetMakeWindowFileName() const {return m_newWindowFileName;}

    /** Return the name of the file containing the javascript sorting function */
    const std::string &GetSortFileName() const {return m_sortFileName;}

    /** Get a string which combines both ColumnGroup header and Column header for iCol'th column */
    void GetCombinedHeader (unsigned int iCol, std::string &header) const;

    /** Return true if column is sortable */
    bool IsColumnSortable(unsigned int iCol) const;

    /** Set a flag which results in the table being output starting with the last Row (in the Row vector) */
    void SetReverseOutput(){m_reverse = true;}

    /** Return true if the rows should be output in reversed order */
    bool IsReversed(void) const {return m_reverse;}

    /** Return strings which are internal codes which indicate a link */
    static void GetLinkCodes(std::string &lCode, std::string &rCode){lCode = m_linkCode; rCode = m_refCode;}

    /** Return a pointer to the row vector */
    std::vector<Row> *GetRows(){return &m_rows;}

    /** Return a constant pointer to the row vector */
    const std::vector<Row> *GetRows() const {return &m_rows;}

    /** Return a pointer to the first row with "key" in the column specified by groupKey and colKey */
    Row *GetRow(const std::string &groupKey, const std::string &colKey, const std::string &key);

    /** Get a vector of rows which have "key" in the column specified by (groupKey, colKey) */
    unsigned int GetRows(const std::string &groupKey, const std::string &colKey, 
			 const std::string &key, std::vector<Row *> &theRows);

    /** Return a const pointer to the (first) row with contents "key" in column (groupKey, colKey) */
    const Row *GetConstRow(const std::string &groupKey, const std::string &colKey, const std::string &key) const;
    /** Dump the table */
    void Dump() const;

  protected:
    
    const static std::string m_newWindowFunction;
    const static std::string m_newWindowFileName;
    const static std::string m_sortFileName;

    static unsigned int m_numTables;
    unsigned int m_nTable;
    bool m_isSortable;
    bool m_reverse;

    // The order in this vector determines the order of column groups in the table
    std::vector<ColumnGroup> m_columnGroups;
    std::vector<Row> m_rows;
    static const std::string m_linkCode;// for specifying url links
    static const std::string m_refCode;// for specifying url links

};



} //wtrp namespace

#endif // WMIPLUGIN_DATATABLE
