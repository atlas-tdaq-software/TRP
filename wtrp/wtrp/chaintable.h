 /**
    @file chaintable.h

    Defines class ChainTable -- from which a wmi page for a group of chains is made.

    @author M. Medinnis
 */

#ifndef WTRP_CHAINTABLE
#define WTRP_CHAINTABLE

#include "wtrp/datatable.h"
#include "wtrp/pagedata.h"
#include "wtrp/TrigConf.h"

namespace wtrp {
  
  /** Inherits from PageData. One ChainTable is made for each group
   and one containing all chains. The chain tables assemble rate and
   prescale information from L1, L2 and EF and attempt to present it
   in a sensible way. */
  class ChainTable : public PageData {
    
  public:
    /** Default constructor. I don't remember why this is needed. It isn't explicitly called. */
    ChainTable(){;}

    /** Constructor, see PageData constructor for parameter definitions */

    ChainTable(const std::string &name, const std::string &title, const std::string &fileName);

    /** Destructor (empty) */
    ~ChainTable();

    /** Dump to std::cout */
    void Dump() const;

    /** Makes the full set of chain tables -- one per monitor group
	and one containing all chains. */
    static void MakeTableSet(std::map<std::string, ChainTable> &chainTables, const TrigConf *tc);

    /** Book a page of pots for each group. This is no longer
	used. Instead chain rate plots are booked in GroupTable */
    static void BookPOTs(std::map<std::string, ChainTable> &chainTables );


    /** Returns the name of the group containing all chains */
    static const std::string &GetAllChainsName(){return m_allChains;}

  private:
    const static std::string m_allChains;

    void MakeTable(const std::string &groupName, const TrigConf *tc);    
    void AddRow( const TrigConf *tc, const TC_chain &tc_chain );
    std::string m_groupName;
  };
  
}

#endif
