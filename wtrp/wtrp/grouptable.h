 /**
    @file grouptable.h

    Defines class GroupTable -- for information on monitoring groups

    @author M. Medinnis
 */

#ifndef WTRP_GROUPTABLE
#define WTRP_GROUPTABLE

#include "wtrp/pagedata.h"
#include "wtrp/datatable.h"
#include "wtrp/TrigConf.h"
#include "wtrp/chaintable.h"

namespace wtrp {

  /** For summarizing monitoring group rates at all 3 trigger levels. */ 
  class GroupTable : public PageData{
    
  public:
    /** Default constructor, shouldn't be used */
    GroupTable(){;}

    /** Constructor, see PageData constructor for parameter definitions. */
    GroupTable(const std::string &name, 
	       const std::string &title, 
	       const std::string &linkText, 
	       const std::string &fileName);

    /** Destructor (empty) */
    ~GroupTable();

    /** Dump to std::cout */
    void Dump();

    /** Format the table */
    void MakeTable(const TrigConf *tc);

    /** Boot POTs -- rate plots for each group at L2 and EF on separate pages (for Run 1) */
    void BookPOTs(const TrigConf *trigConf);


  private:

    /** Boot POTs for run1 or run2 configurations */
    void BookPOTs_Run1(const TrigConf *trigConf);
    void BookPOTs_Run2(const TrigConf *trigConf);

  };
    
} // namespace wtrp

#endif
