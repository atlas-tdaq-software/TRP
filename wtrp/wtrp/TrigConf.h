 /**
    @file TrigConf.h

    Hold the configuration information as a set of chains. Each chain
    is defined by a name (usually the EF_chain name) and includes
    prescales, passthroughs, stream and group names.  It can also hold
    on to the algorithm steps although this has not been completely
    implemented. (Making it work would also require some work in
    TriggerMenuHelpers.)  It is not the most economical way to
    represent chains but it's convenient for making tables for
    display.
    
    Four classes are defined: TC_step (not used), TC_chainInLevel,
    TC_chain and TrigConf.

    @author M. Medinnis
 */

#ifndef TRIGCONF
#define TRIGCONF

#include <vector>
#include <set>
#include <string>
#include <map>

#include <iostream>

#include "wtrp/chainspec.h"

namespace wtrp {
  

  /** Indicate the trigger level */
  enum TC_level {TC_L1, TC_L2, TC_EF, TC_HLT};

  /** Hold information on algorithm steps (not used). It might become useful at some point. */
  class TC_step {
   
  public:
    TC_step();
    
    TC_step(const std::vector<std::string> &algorithms, const std::vector<std::string> &outputTEs);
    
    void AppendAlgorithm(std::string algo){m_algorithms.push_back(algo);}
    void AppendOutputTE(std::string output_TE){m_outputTEs.push_back(output_TE);}
    const std::vector<std::string> *GetAlgorithms();
    const std::vector<std::string> *GetOutputTEs();
  private:
    std::vector<std::string> m_algorithms;
    std::vector<std::string> m_outputTEs;
  };
  
  
  /** Store the level-specific chain name, prescale and passthrough
      factor for a chain at a particular trigger level.
   */

  class TC_chainInLevel {
    
  public:
    TC_chainInLevel(const std::string &name, int prescale=-99, int passthrough=0);
    
    /** Set prescale */
    void SetPrescale(int prescale){ m_prescale = prescale;}

    /** Set passthrough */
    void SetPassthrough(int passthrough){m_passthrough = passthrough;}

    //void AppendStep(TC_step &step) {m_steps.push_back(step);}
    
    /** Return the trigger-level specific name */
    const std::string &GetName()const {return m_name;}

    /** Return the prescale factor */
    int GetPrescale() const {return m_prescale;}

    /** Return the passthrough factor */
    int GetPassthrough() const {return m_passthrough;}

    //const std::vector<TC_step> &GetSteps() const {return m_steps;}
    
  private:
    std::string m_name;
    int m_prescale;
    int m_passthrough;
    //std::vector<TC_step> m_steps;   
    
  };
  
  /** Defines a chain. Maintains TC_chainInLevel objects for each
      level, the chain name (usually the EF name) and the names of the
      groups and streams to which it belongs.  
  */
  
  class TC_chain {
    
  public:
    
    /** Constructors for run 1 and run 2 */
    TC_chain(const std::string &name, TC_chainInLevel &L1, TC_chainInLevel &L2, TC_chainInLevel &EF);
    TC_chain(const std::string &name, TC_chainInLevel &L1, TC_chainInLevel &HLT);
    
    /** Set the TC_chainInLevel object to lchain for level lvl */
    void SetChainInLevel(TC_level lvl, TC_chainInLevel &lchain);

    /** Add a stream name to the list of streams */
    void AddStream(const std::string &streamTag){m_streams.insert(streamTag);}

    /** Add a group name to the list of groups */
    void AddGroup(const std::string &groupName){m_groups.insert(groupName);}

    /** Set the chain name to name */
    void SetName(std::string name){m_name = name;}

    /** Set the prescale at level lvl */
    void SetLevelPrescale(TC_level lvl, int prescale);

    /** Return the chain name. */
    const std::string &GetName()const {return m_name;}

    /** Return the chain name for the specified level */
    const std::string &GetLevelName(TC_level lvl) const;

    /** Return the prescale factor */
    int GetLevelPrescale(TC_level lvl) const;

    /** Return the TC_ChainInLevel object for the given level */
    const TC_chainInLevel &GetChainInLevel(TC_level lvl) const ;

    /** Return the set of streams this chain contributes to */
    const std::set<std::string> &GetStreams() const {return m_streams;}

    /** Return the set of groups this chain belongs to */
    const std::set<std::string> &GetGroups() const {return m_groups;}
    void Dump(bool isRun2) const;
    
    /** For sorting. The order is determined by m_chainSpec */
    int operator<(const TC_chain &rhs) const;
    
  private:
    std::string m_name; // (== EF_name unless there is no EF chain)
    
    TC_chainInLevel m_L1;
    TC_chainInLevel m_L2;
    TC_chainInLevel m_EF;
    TC_chainInLevel m_HLT;

    ChainSpec m_chainSpec;

   std::set<std::string> m_streams;
   std::set<std::string> m_groups;
  };
  

  /** Hold on to the needed trigger configuration information. */
  class TrigConf{
    
  public:
    /** Constructor */
    TrigConf();
    
    /** Return or set the configuration type (run1 or run2) */
    bool IsRun1Configuration() const {return m_isRun1Configuration;}
    bool IsRun2Configuration() const {return m_isRun2Configuration;}
    void setRunConfiguration(bool isRun2) {m_isRun1Configuration = !isRun2; m_isRun2Configuration = isRun2;}

    /** Insert a chain into the chain set */
    void InsertChain(const TC_chain &aChain) { m_chains.insert(aChain);}

    /** Return the set of all chains */
    const std::set<TC_chain> &getChains()const {return m_chains;}

    /** Add a stream name to each of the chains specified in chainNames. Note that the chain
     names are specific to the level specified by lvl
    */
    void AddStream(TC_level lvl, const std::string &streamName, const std::vector<std::string> &chainNames);

    /** Add a group name to each of the chains specified in chainNames. Note that the chain
     names are specific to the level specified by lvl
    */
    void AddGroup(TC_level lvl, const std::string &groupName, const std::vector<std::string> &chainNames);

    /** Set the prescale factor for the specified chain name at level lvl */
    void SetPrescale(TC_level lvl, const std::string &name, int preScale);

    /** Set the stream prescale for the specified chain */
    void AddStreamPrescale(const std::string &stream, const std::string &chain, float prescale) {
      m_streamPrescales[stream + "_" + chain] = prescale;}

    /** Return the stream prescale for the specified chain, -1 if not found */
    float GetStreamPrescale(const std::string &stream, const std::string &chain) const; // return -1. if not found
    /** Make chain to group maps. They are used in setting up the chain tables */
    void MakeCh2GrpMaps();

    /** Return the total number of groups */
    unsigned int GetNGroups() const {return m_groups.size();}

    /** Return the total number of streams */
    unsigned int GetNStreams() const {return m_streams.size();}

    /** Return a reference to the set of group names */
    const std::set<std::string> &GetGroupNames() const {return m_groups;}

    /** Return a reference to the set of stream names */
    const std::set<std::string> &GetStreamNames() const {return m_streams;}

    /** Return the chain-->group map for level lvl */
    const std::map<std::string, std::set<std::string> > &GetChToGrpMap(TC_level lvl) const;

    /** Dump the configuration to std::cout */
    void Dump(bool isRun2);

  private:
    TC_chain *GetChain(TC_level lvl, const std::string &levelName);
    void GetMatchingChains(TC_level lvl, const std::string &levelName, std::vector<TC_chain *> &chains);

    bool m_isRun1Configuration;
    bool m_isRun2Configuration;

    std::set<TC_chain> m_chains;
    std::set<std::string> m_streams;
    std::set<std::string> m_groups;
    std::map<std::string, std::set<std::string> > m_L2_ch_to_group;
    std::map<std::string, std::set<std::string> > m_EF_ch_to_group;
    std::map<std::string, std::set<std::string> > m_HLT_ch_to_group;
    std::map<std::string, float > m_streamPrescales;
  };
  
  
} // end namespace wtrp
#endif
