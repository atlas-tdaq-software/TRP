 /**
    @file streamtable.h

    Defines class StreamTable -- summary of stream rates

    @author M. Medinnis
 */

#ifndef WTRP_STREAMTABLE
#define WTRP_STREAMTABLE

#include "wtrp/TrigConf.h"
#include "wtrp/pagedata.h"
#include "wtrp/datatable.h"
#include "wtrp/pot.h"

namespace wtrp {
  
  /** For summarizing the current stream rates and history plots */
  class StreamTable : public PageData {
    
  public:

    /** Constructor, see PageData constructor for parameter definitions. */
    StreamTable(const std::string &name, 
		const std::string &title, 
		const std::string &linkText, 
		const std::string &fileName);

    /** Destructor (empty) */
    ~StreamTable();

    /** Dump to std::cout */
    void Dump();

    /** Format the stream rates table */
    void MakeTable(const std::set<std::string> &L2streamNames, const std::set<std::string> &EFstreamNames);
    void MakeTable(const std::set<std::string> &HLTstreamNames);

    /** Book the stream rate POTs */
    void BookPOTs(  const std::set<std::string> &L2streamNames, const std::set<std::string> &EFstreamNames );
    void BookPOTs(  const std::set<std::string> &HLTstreamNames );

  private:

  };
  
}

#endif
