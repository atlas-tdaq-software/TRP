 /**
    @file rctlstats.h

    Defines class RCtlStats -- fetches, serves locally any needed info from RCTL IS servers.

    @author M. Medinnis
 */

#ifndef TRP_RCTL_STATS
#define TRP_RCTL_STATS

#include "TRP/L2SV.h"
#include "TRP/L2PU.h"
#include "TRP/SFI.h"
#include "TRP/SFO.h"


#include <map>
#include <set>
#include <string>
#include <owl/time.h>

#include "is/infodictionary.h"

namespace wtrp {

    /** Obtain ISInfo records from rctl servers. Specifically
	L2SV-SUM, L2PU-SUM, SFI-SUM, SFO-SUM. The variables from these
	servers which can be represented as a double are transferred
	to maps, one for each ISInfo type. They can be fetched locally
	with a call to GetVar. Note that local copies of the header
	files which define the ISInfo objects are used and the names
	of parameters are hard-wired into the code -- not exactly
	robust. */
  class RCtlStats {
    
  public:

    /** Destructor, deletes some heap-allocated maps. */ 
    ~RCtlStats();
   
    /** Get the current ISInfo objects from the IS servers. */
    static void GetFromServer(const ISInfoDictionary &dict);

    /** Return true if latest fetch was successful. */
    static bool UpdateValid(){ return m_allValid;}

    /** Set val to true if latest fetch was successful. */
    static bool GetUpdateStatus(unsigned int &val)
      { val = (m_allValid)? 1 : 0; return true;}

    /** Set val to the value of the variable specified by is_class and name */
    static bool GetVar(const std::string &is_class, const std::string &name, double &val);

    /** Return true if is_class is in the map */
    static bool isValid(const std::string &is_class){return (m_varMap.find(is_class) == m_varMap.end())? false : true;}

    /** Return true if the last fetch of is_class has a time stamp which is later than the previous one. */
    static bool isFresh(const std::string &is_class){
      return (m_freshMap.find(is_class) == m_freshMap.end())? false : m_freshMap[is_class];}

    /** Dump the contents of the maps */
    void Dump();

  private:

    /** Private constructor, only static functions are defined. */
    RCtlStats();

    /** Delete heap-allocated maps. */
    static void Reset();

    /** Get specified info from IS */
    static bool GetISInfo(const ISInfoDictionary &dict, const std::string &infoName, ISInfo &info);

    /** Return true if latest fetch of info is more recent than last fetch */
    static bool isFresh(const ISInfo &info);

    /** Load the L2SV map */
    static void SetL2SV( L2SV &l2sv);

    /** Load the L2PU map */
    static void SetL2PU( L2PU &l2pu);

    /** Load the SFI map */
    static void SetSFI( DF_IS_Info::SFI &sfi);

    /** Load the SFO map */
    static void SetSFO( DF_IS_Info::SFO &sfo);

    static bool m_allValid;

    static long long m_now; // set in GetISInfo to the current time in microsec since epoch

    // Only contains variables which can be represented as a double
    static std::map<std::string, std::map<std::string, double>* > m_varMap;
    static std::map<std::string, bool> m_freshMap;
  };
}

#endif
