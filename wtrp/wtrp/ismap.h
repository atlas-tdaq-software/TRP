 /**
    @file ismap.h

    Defines class ISMap -- fetches arbitrary values from IS for plotting

    @author M. Medinnis
 */

#ifndef TRP_ISMAP
#define TRP_ISMAP

#include <string>
#include <owl/time.h>

#include "ipc/core.h"
#include "is/infodictionary.h"

namespace wtrp {

  /** Fetches, serves locally any IS parameter which can be represented
      as a float (in fact only a subset of the possible IS types but that
      would be easy to change if needed). The parameter to be fetched
      must first be booked. All booked parameters are updated via a 
      call to fetch. All booked parameters are stored in a map. The
      current values can be retrieved by get.
  */
  class ISMap {
    
  public:
    /** Destructor (empty) */
    ~ISMap();

    /** Book a parameter using encoded string:
	EXT|<partition name>|<server name>|<object name>|<parameter name>|<parameter key>
	<parameter key> should be unique, it is used by Get to specify the parameter
     */

    class MapEntry {
    public:
      MapEntry(const std::string  &partition = "", 
	       const std::string  &server = "", 
	       const std::string  &object = "", 
	       const std::string  &parameter = "", 
	       const std::string  &key = "", 
	       bool deletable = false) :
	m_partition(partition), m_server(server),m_object(object), 
	m_parameter(parameter), m_key(key), m_deletable(deletable),
	m_updateTime(0), m_isValid(false), m_isFresh(false), m_value(0.)
	{};
	
	MapEntry(const MapEntry &ref)
	  {
	    m_partition =   ref.m_partition;
	    m_server =     ref.m_server;
	    m_object =     ref.m_object;
	    m_parameter =  ref.m_parameter;
	    m_key =        ref.m_key;
	    m_updateTime = ref.m_updateTime;
	    m_isValid =    ref.m_isValid;
	    m_isFresh =    ref.m_isFresh;
	    m_value =      ref.m_value;
	    m_deletable =  ref.m_deletable;
	  }

	bool Fetch();
	float Value() const {return m_value;}
	bool isValid() const {return m_isValid;}
	bool isFresh() const {return m_isFresh;}
	bool isDeletable() const {return m_deletable;}

    private:
      std::string m_partition;
      std::string m_server;
      std::string m_object;
      std::string m_parameter;
      std::string m_key;

      bool m_deletable;
      long long m_updateTime;
      bool m_isValid;
      bool m_isFresh;
      float m_value;
    };

    /** Book a parameter, giving info needed to fetch it */
    static void Book(const std::string &partition,
		     const std::string &server,
		     const std::string &object,
		     const std::string &parameter,
		     const std::string &key,
		     bool deletable = false);

    /** Get all booked parameters from IS */
    static bool Fetch();

    /** Return the current value of parameter specified by "key" */
    static float Get(const std::string &key);

    /** Returns true if the last fetch attempt was successful */
    static bool isValid(const std::string &key);

    /** Returns true if the most recent fetch got info which is more recent than the last */
    static bool isFresh(const std::string &key);

    /** Clear the map  (erase all entries flagged as deletable) */
    static void ClearMap();

  private:
    ISMap();

    static std::map<std::string, MapEntry> m_isMap;
  };
}

#endif
