 /**
    @file pot.h

    Defines the POT class

    @author M. Medinnis
 */

#ifndef _WTRP_POT_
#define _WTRP_POT_

#include <iostream>

#include "TGraph.h"

#include "wtrp/potbase.h"

/* Parameter Over Time, in case you are wondering */
/* For 1d graphs vs time */
namespace wtrp {

  /** Maintains the history of parameter values and produces line plots using ROOT. */
  class POT : public POTBase {

  public:
    /** Originally to designate trigger level, now broadened to indicate the origin of the data */
    enum PD_level {PD_none, PD_L1, PD_L2, PD_EF, PD_HLT, PD_RCTL, PD_L2CPU, PD_EFCPU, PD_LUMI, PD_ISMAP}; 

    static const std::string &GetPD_source( enum PD_level lev );

    /** Options for plots. PD_dotted produces a dotted
	rather than solid line. It doesn't look too great.
        PD_forceOut forces the plot onto the graph even if
        it is below threshhold (useful for configurable plots mainly. */
    enum PD_opt {PD_dotted = 1, PD_forceOut}; 

    /** Holds on to data needed for plotting particular parameters */
    struct pardef{
      /** Indicates the TimePoint_IS (other other source) of the parameter */
      enum PD_level level;
      /** x label of parameter as it appears in TimePoint_IS */
      std::string xlabel;
      /** y label of parameter as it appears in TimePoint_IS */
      std::string ylabel;
      
      enum PD_level ldenom;

      /** in case of a ratio, the x label of denominator as it appears in TimePoint_IS */
      std::string xdenom;

      /** in case of a ratio, the y label of denominator as it appears in TimePoint_IS */
      std::string ydenom;

      /** not used */
      float (*setFun)(const std::deque<float> &data_s, const std::deque<float> &data_l, std::string &newLegendName);

      /** Name appearing in legend */
      std::string legendName;
      unsigned int nConsecutiveInvalids;
      
      /** The data for the last 1/2 hour.  Must be synchronized with m_timeStamps_s
	  (i.e. must be updated for each call to UpdatePOTs) */
      std::deque<float> data_s; // short-term deque

      /** The data for the last 24 hours. Must be synchronized with m)timeStamps_l */
      std::deque<float> data_l; // long-term deque

      /** keeps partially built point before inserting in data_l */
      float transPoint; 

      /** number of valid points contributing to transPoint */
      unsigned int nValidTPoints; 

      /** A mask of options -- as defined in PD_opt */
      unsigned int optsMask;
    };
    typedef struct pardef ParDef;

    /** The only way to construct a POT. name must be unique, title
	appears in the title field of the plots */
    static POT *BookPOT(const std::string &name, const std::string &title);

    /** Add a parameter to the POT. 
	-# level - trigger level or other source
	-# xlabel - for TimePoints, the x label in the TP, otherwise see "level"-specific code
	-# ylabel - for TimePoints, the y label in the TP, otherwise see "level"-specific code
	-# legendName - obvious, no?
    */
    void AddParameter(enum PD_level level, const std::string &xlabel, const std::string &ylabel, const std::string &legendName);

    /** Add the ratio of two parameters to the POT. Numerator spec (as
	for AddParameter) followed by denominator spec */
    void AddNormalizedParameter(enum PD_level lNumer, const std::string &xNumer, const std::string &yNumer, 
				enum PD_level lDenom, const std::string &xDenom, const std::string &yDenom, 
				const std::string &legendName);

    /** Delete the parameter  with the corresponding legend name from the POT */
    void DeleteParameter(enum PD_level lNumer, const std::string &xNumer, const std::string &yNumer, 
			 enum PD_level lDenom, const std::string &xDenom, const std::string &yDenom );

    /** turn on or off the logOption */
    void SetLogOption(bool logOption){m_isLogPlot = logOption; }

    /** Parameters which haven't received an update in 24 hours are deleted unless this is set. */
    void SetNoDelete() {m_noDelete = true;}

    /** Set an option for the parameter corresponding to legendName */
    void SetOpt(const std::string &legendName, PD_opt opt);

    /** Set the vertical lower limit of the plot. */
    void SetFixedYMin(float ymin){m_fixedYMin = ymin;}

    /** Set the vertical upper limit of the plot */
    void SetFixedYMax(float ymax){m_fixedYMax = ymax;}
    
    /** Unset the minimum -- it will be adjusted automatically */
    void UnsetFixedYMin(){m_fixedYMin = std::numeric_limits<float>::max();}

    /** Unset the maximum -- it will be adjusted automatically */
    void UnsetFixedYMax(){m_fixedYMax = std::numeric_limits<float>::max();}

    /** Require that at least one plot be output even if it is empty. Needed for the global rates plot */
    void SetForceOutput(){m_forceOutput = true;}

    /** Dump to std::cout */
    void Dump() const;

    /** Returns a const reference to the vector of ParDefs. */
    const std::vector<ParDef> &GetPars() const {return m_parDefs;}

  private:


    POT(const std::string &name, const std::string &title);

    /** Called by POTBase::UpdatePOTs() after all info from IS has been fetched. */
    void Update(bool updateLong);

    /** Output all plots for this POT. Plot at most m_maxParsPerPlot parameters per plot. */
    bool GetPOTPlots(std::vector<PlotStuff> &plotStuff) const;

    /** Set up in MakeGraphs, used locally in GetPOTPlots (which calls MakeGraphs) */
    struct GraphStuff {
      TGraph *theGraph_s;
      TGraph *theGraph_l;
      TGraph *theDotGraph_s;
      TGraph *theDotGraph_l;
      std::string legEntry;
      float ymin;
      float ymax;
    };

    /** Produce TGraphs for all active parameters. */
    void MakeGraphs( std::vector<GraphStuff> &graphStuff ) const;

    /** Make the TCanvases and write them to disk, then clean up. */
    bool PrintAndReset(std::vector<GraphStuff>::iterator &ig, 
		       std::vector<GraphStuff>::iterator &igl, 
		       unsigned int nPlots, unsigned int plotNum, PlotStuff &aPotPlot) const;
      
    /** Make and draw the ame graph as given but with fewer points and polymarkers (to make hidden graphs visile */
    void MakeAndDrawDotGraph(const TGraph *gr,  TGraph **theDotGraph, int iPar, int dotSpacing) const;

    std::vector<ParDef> m_parDefs;

    static const std::vector<std::string> m_PD_source;

    bool m_isLogPlot;
    bool m_noDelete;
    float m_fixedYMin;
    float m_fixedYMax;
    bool m_forceOutput; // needed to make sure the global rate plot is output even if there are no rates to show

    const static int m_nLegendColumns = 4;
    const static int m_legendRowHeight = 18;
    const static int m_hisHeight = 400;
    const static double m_upperLimit; //no values above this get into plots 
  };
}
#endif
