 /**
    @file l1table.h

    Defines class L1Table -- for L1 rates, prescales, etc.

    @author M. Medinnis
 */

#ifndef WTRP_L1TABLE
#define WTRP_L1TABLE

#include "wtrp/pagedata.h"
#include "wtrp/datatable.h"
#include "wtrp/TrigConf.h"

namespace wtrp {
  
  /** For a summary of L1 rates, prescales, etc. */
  class L1Table : public PageData{
    
  public:
    /** Default constructor, shouldn't be used */
    //L1Table() : m_smk(0), m_L1psk(0) {;}

    /** Constructor, see PageData constructor for parameter definitions. */
    L1Table(const std::string &name, 
	    const std::string &title, 
	    const std::string &linkText, 
	    const std::string &fileName) : 
      PageData(name, title, linkText, fileName), m_smk(0), m_L1psk(0) {;}


    /** Destructor (empty) */
    ~L1Table();

    /** Dump to std::cout */
    void Dump();

    /** Format the table */
    void MakeTable();

    /** Book rate plots for each enabled L1 trigger */
    void BookPOTs();

    /** Return the value of the SMK from the last TP update. */
    unsigned int GetLastSMK() const { return m_smk;}

    /** Return the value of the L1_PSK from the last TP update. */
    unsigned int GetLastL1_PSK() const { return m_L1psk;}

    /** Set the SMK. */
    void SetSMK( unsigned int smk ) { m_smk = smk;}

    /** Set the L1_PSK. */
    void SetL1_PSK( unsigned int l1psk ) { m_L1psk = l1psk;}

    /** This is used by wtools::FixupL1ConfigFromTP to format the table */
    void AddEntry( const std::string &L1name );

    /** Return the L1 psk -- as it appears in the last fetched time point */
    
  private:
    unsigned int m_smk; // from the time point.
    unsigned int m_L1psk; // from the time point.
  };
    
} // namespace wtrp

#endif
