 /**
    @file pagedata.h

    PageData is the base class for all wtrp html pages.

    @author M. Medinnis
 */

#ifndef WTRP_PAGEDATA
#define WTRP_PAGEDATA

#include "wtrp/datatable.h"
#include "wtrp/pot.h"

#include <iostream>

namespace wtrp {

  class PageData;

  /** PageData is the base class for all wtrp html pages.  PageData
    objects have at most one DataTable and any number of POTs
    (parameter over time plots). In addition, at the top are links
    which are common to all pages, links which are only seen on this
    page, a title, a preamble and a "postamble". A PageData might also
    have links to other PageData objects which it owns and which
    contain only POTs. PageData hold the data for producing the pages
    but not the code for producing them in wmi -- that is in
    OnlineTools.cpp (wtools::OutputWMIPage).
  */
  class PageData {
    
  public:

    /** Needed for copying internal containers. Shouldn't be used otherwise. */
    PageData() : m_outputTableLast(false) {;} 

    /** Constructor. Parameters: 
	-# name -- of page (for reference), does not appear on displayed page. Must be unique. 
	-# title -- which will appear on the top of the page 
	-# linkText -- text to be displayed for links to this page 
	-# filename -- name of file which will contain the html 
	-# outputTableLast -- by default the DataTable appears before the POTs. 
	Setting this to true reverses the order.
    */
    PageData(const std::string &name, 
	     const std::string &title, 
	     const std::string &linkText, 
	     const std::string &fileName,
	     bool outputTableLast = false) : 
      m_pageName(name), m_pageTitle(title), m_linkText(linkText), m_fileName(fileName),
      m_outputTableLast(outputTableLast)
	{m_fileNameMap[m_pageName]=fileName;}
      
      /** Destructor -- deletes any owned PageData objects  */
    virtual ~PageData();

    /** Return a const pointer to the DataTable object */
    const DataTable *GetTable() const {return &m_dataTable;}

    /** Return a pointer to the DataTable object */
    DataTable *GetTable(){return &m_dataTable;}

    /** Return a pointer to the page specified by name */
    PageData *GetPage(const std::string &name);

    /** Return the value of m_outputTableLast. */
    bool OutputTableLast() const {return m_outputTableLast;}

    /** Returns the page name */
    const std::string &GetPageName() const {return m_pageName;}

    /** Returns the page title */
    const std::string &GetPageTitle() const {return m_pageTitle;}

    /** Returns the name of the html file for this page */
    const std::string &GetOutputFileName()const {return m_fileName;}

    /** Returns a vector of pots which will be displayed on this page */
    const std::vector<std::string> &GetPOTNames() const {return m_POTNames;}

    /** Returns a vector of any POT-only pages owned by this page */
    const std::vector<PageData*> &GetPOTPages() const {return m_POTPages;}

    /** Add a POT to this page */
    void AddPOT(const std::string &POTName);

    /** Add a POT page to this page */
    void AddPOTPage(PageData *thePage);

    /** Clear all POTs appearing on this page. Note that this is no longer used. */
    void ClearPOTs();

    /** Add a link which will appear only on this page. */
    void AddLocalLink(const std::string &fileName, const std::string &linkText)
    {m_localLinks[fileName] = linkText;}

    /** Formats a string containing all links (global and local) */
    void FormatLinkLine(std::string &linkLine) const;

    /** Clear all local links (not used) */
    void ClearLocalLinks(){m_localLinks.clear();}

    /** Add a line of text to the local preamble */
    void AddLocalPreambleLine(std::string theLine){m_localPreamble.push_back(theLine);}

    /** Return the vector of strings which make up the local preamble */
    const std::vector<std::string> &GetLocalPreamble() const {return m_localPreamble;}

    /** Clear the local preamble */
    void ClearLocalPreamble(){m_localPreamble.clear();}

    /** Add a line to the global (==> appears on all pages) preamble */
    static void AddGlobalPreambleLine(std::string theLine){m_globalPreamble.push_back(theLine);}

    /** Clear the global preamble */
    static void ClearGlobalPreamble(){m_globalPreamble.clear();}
    
    /** Return the vector of strings which make up the global preamble */
    static const std::vector<std::string> &GetGlobalPreamble() {return m_globalPreamble;}

    /** Set fileName to the name of the html file for the page named pageName (or "" if not found) */
    static void GetOutputFileName(const std::string &pageName, std::string &fileName);

    /** Add a link to all pages */
    static void AddGlobalLink(const std::string &fileName, const std::string &linkText );

    /** Clear the global links list (not used). */
    static void ClearGlobalLinks(){m_globalLinks.clear();}

    /** Add a line to the local preamble (seen only on this page) */
    void AddToLocalPostamble(const std::string str){ m_localPostamble.push_back(str);}

    /** Get the local preamble */
    const std::vector<std::string> &GetLocalPostamble() const {return m_localPostamble;}

    /** Construct the table which will hold the rate summary which appears on all pages. 
      The table is updated by wtools::FillGlobalRatesTable()*/
    static void MakeGlobalRatesTable(bool isRun2);

    /** Return a pointer to the rates summary table */
    static DataTable *GetGlobalRatesTable(){ return &m_globalRates; }

    /** for sorting the global links in a sensible way, hacky... */
    struct GlobalLinkSorter { 
      bool operator() (const std::string &lhs, const std::string &rhs) const;
    };
    friend struct globalLinkSorter;

  protected:
    static void MakeGlobalRatesTable_Run1();
    static void MakeGlobalRatesTable_Run2();


    const static std::string m_L2ChainPotFileName;
    const static std::string m_EFChainPotFileName;
    const static std::string m_HLTChainPotFileName;

    static std::map<std::string, unsigned int>  m_knownGlobalLinks;

    static std::map<std::string, std::string, class GlobalLinkSorter > m_globalLinks;
    static std::map<std::string, std::string> m_fileNameMap; // page name --> file name
    std::map<std::string, std::string> m_localLinks;
    
    std::string m_pageName;
    std::string m_pageTitle;
    std::string m_linkText;
    std::string m_fileName;
    bool m_outputTableLast; // if true, output the table at the bottom of the page

    static std::vector<std::string> m_globalPreamble; // add to all pages (before local preamble)-- fully formatted HTML lines.
    std::vector<std::string> m_localPreamble; // add only to this page. (after global preamble)
    std::vector<std::string> m_localPostamble; // add only to this page. (after global preamble)
    DataTable m_dataTable;
    std::vector<std::string> m_POTNames; // the POTs which appear on this page
    std::vector<PageData*> m_POTPages; // intended for pages displaying only POTs (no table)
    static DataTable m_globalRates;
  };
    
} // namespace wtrp

#endif
