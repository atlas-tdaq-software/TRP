 /**
    @file lhcinfo.h

    Defines class LHCInfo -- fetches, serves locally any needed LHC status flags

    @author M. Medinnis
 */

#ifndef TRP_LHCINFO
#define TRP_LHCINFO

#include <string>
#include <owl/time.h>

#include "ipc/core.h"
#include "is/infodictionary.h"

namespace wtrp {

  /** fetches, serves locally any needed LHC status flags. At present,
      only the stable beams flag. */
  class LHCInfo {
    
  public:
    /** Get from server */
    static void GetFromServer();

    /** Sets val to the current stable beam status. For the status history plot. */
    static bool GetStableBeamsFlag(unsigned int &val){val = m_stableBeams; return m_isValid;}

    /** True if the latch fetch attempt was successful */
    static bool isValid(){return m_isValid;}

  private:

    const static std::string m_lhcPartitionName;
    const static std::string m_lhcServer;
    const static std::string m_stableBeamsVariable;

    /** Private constructor, only static member functions are defined. */
    LHCInfo(){;}

    /** Fetch infoName from IS */
    static bool GetISInfo(const ISInfoDictionary &dict, const std::string &infoName, ISInfo &info);

    static bool m_isValid;

    static unsigned int m_stableBeams; // 0 == no, 1 == yes

  };
}

#endif
