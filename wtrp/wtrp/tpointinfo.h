 /**
    @file tpointinfo.h

    Defines the TPointInfo class which maintains the current TPoint_IS structures.

    @author M. Medinnis
 */

#ifndef WTRP_TPOINTINFO
#define WTRP_TPOINTINFO

#include <string>
#include "owl/time.h"
#include "TRP/TimePoint_IS.h"
#include "wtrp/TrigConf.h"
#include "is/infodictionary.h"

namespace wtrp {
  

  class TPointInfo;

  /**
     Hold on to the most recent Timepoint_IS objects fetched from the TRP server.

     Static references to the time points for each level and the CPU usage timepoints
     are maintained. All timepoints are fetched from IS by a call to Update().
  */
  class TPointInfo {
  public:
    
    /** Set the TimePoint_IS name (i.e. its name on the ISS_TRP server */
    void SetName(std::string newName){m_name = newName;}

    /** Get the TimePoint_IS name */
    const std::string &GetName() const {return m_name;}

    /** Fetch all TimePoint_IS structures from TRP server. The code
     attempts to fetch all TimePoint_IS's in the IS history list which
     have been created since the last update -- in a single call to
     getValues (see IS). It averages the result (for the trigger rates
     there are usually 6-8 time points in the average.) It adjusts
     m_pointsToFetch for the next update depending on whether it
     fetched too many or too few.
    */
    static bool Update(const ISInfoDictionary &dict, const TrigConf *tc);

    /** Returns true if the current TimePoint_IS is valid. */
    bool IsValid() const {return m_isValid;}

    /** Returns true if the time stamp of the current TimePoint_IS is later than
	the one(s) fetched the last time Update() was called.
    */
    bool IsFresh() const {return m_isFresh;}

    /** Set val to true if L1 TP is fresh.  For use by statpot (the
	status display on the global rates page) */
    static bool GetL1UpdateStatus(unsigned int &val)
      { val = m_L1Info.m_isFresh; return true;}

    /** Set val to true if L2 TP is fresh. 
	For use by statpot (the status display on the rates page) */
    static bool GetL2UpdateStatus(unsigned int &val)
      { val = m_L2Info.m_isFresh; return true;}

    /** Set val to true if EF TP is fresh. 
	For use by statpot (the status display on the rates page) */
    static bool GetEFUpdateStatus(unsigned int &val)
      { val = m_EFInfo.m_isFresh; return true;}

    /** Set val to true if HLT TP is fresh. 
	For use by statpot (the status display on the rates page) */
    static bool GetHLTUpdateStatus(unsigned int &val)
      { val = m_HLTInfo.m_isFresh; return true;}

    /** Set val to the run number as seen by TRP. This is no longer
	used (see runinfo.h) */
    static bool GetRunNumber(unsigned int &val)
      { val = m_current_run; return true;}

    /** Set val to the LB number as seen by TRP. This is no longer
	used (see runinfo.h) */
    static bool GetLumiBlock(unsigned int &val)
      { val = m_current_lumiBlock; return true;}

    /** Set val to the SMK as seen by TRP. This is no longer used (see runinfo.h) */
    static bool GetSMK(unsigned int &val)
      { val = m_current_smk; return true;}

    /** Set val to the L1 prescale key as seen by TRP. This is no
	longer used (see runinfo.h) */
    static bool GetL1_PSK(unsigned int &val)
      { val = m_current_L1_psk; return true;}

    /** Set val to the HLT prescale key as seen by TRP. This is no
	longer used (see runinfo.h) */
    static bool GetHLT_PSK(unsigned int &val)
      { val = m_current_HLT_psk; return true;}

    /** Useful for testing, otherwise not used */
    static void SetKeysForTest(unsigned int smk, unsigned int L1_psk, unsigned int HLT_psk)
      { m_current_smk = smk; m_current_L1_psk = L1_psk; m_current_HLT_psk = HLT_psk; 
	m_updateKeys = false; m_validKeys = true;}

    /** return true if SMK, L1_PSK and EF_PSK keys as seen by TRP are valid */
    static bool ValidKeys(){return m_validKeys;}

    /** Return run number as seen by TRP (no longer used). */
    static unsigned int GetRunNumber(){return m_current_run;}

    /** Return SMK as seen by TRP (no longer used). */
    static unsigned int GetSMK(){return m_current_smk;}

    /** Return L1 prescale key as seen by TRP (no longer used). */
    static unsigned int GetL1_PSK(){return m_current_L1_psk;}

    /** Return HLT prescale key as seen by TRP (no longer used). */
    static unsigned int GetHLT_PSK(){return m_current_HLT_psk;}

    /** Return a reference to the L1 TPointInfo object */
    static const TPointInfo &GetConstL1Info() {return m_L1Info;}

    /** Return a reference to the L2 TPointInfo object */
    static const TPointInfo &GetConstL2Info() {return m_L2Info;}

    /** Return a reference to the EF TPointInfo object */
    static const TPointInfo &GetConstEFInfo() {return m_EFInfo;}

    /** Return a reference to the HLT TPointInfo object */
    static const TPointInfo &GetConstHLTInfo() {return m_HLTInfo;}

    /** Return a reference to the L2 CPU info TPointInfo object. (Produced by CPU adapter) */
    static const TPointInfo &GetConstL2_CPUInfo() {return m_L2_CPUInfo;}

    /** Return a reference to the EF CPU info TPointInfo object. (Produced by CPU adapter) */
    static const TPointInfo &GetConstEF_CPUInfo() {return m_EF_CPUInfo;}

    /** Return a reference to the HLT CPU info TPointInfo object. (Produced by CPU adapter) */
    static const TPointInfo &GetConstHLT_CPUInfo() {return m_HLT_CPUInfo;}

    /** Return non-constant pointer to L1 TPointInfo object (because TimePoint_IS is not const correct */
    static TPointInfo &GetL1Info() {return m_L1Info;}

    /** Return non-constant pointer to L2 TPointInfo object (because TimePoint_IS is not const correct */
    static TPointInfo &GetL2Info() {return m_L2Info;}

    /** Return non-constant pointer to EF TPointInfo object (because TimePoint_IS is not const correct */
    static TPointInfo &GetEFInfo() {return m_EFInfo;}

    /** Return non-constant pointer to HLT TPointInfo object (because TimePoint_IS is not const correct */
    static TPointInfo &GetHLTInfo() {return m_HLTInfo;}

    /** Return non-constant pointer to L2_CPU TPointInfo object (because TimePoint_IS is not const correct */
    static TPointInfo &GetL2_CPUInfo() {return m_L2_CPUInfo;}

    /** Return non-constant pointer to EF_CPU TPointInfo object (because TimePoint_IS is not const correct */
    static TPointInfo &GetEF_CPUInfo() {return m_EF_CPUInfo;}

    /** Return non-constant pointer to HLT_CPU TPointInfo object (because TimePoint_IS is not const correct */
    static TPointInfo &GetHLT_CPUInfo() {return m_EF_CPUInfo;}

    /** Leaving m_timePoint exposed (public) is ugly. Consider making it private. */
    TimePoint_IS m_timePoint;

  private:
    /** Private constructor! Only specific cases are instantiated: those specified below as static members */
    TPointInfo(const std::string &name)
      : m_name(name), m_pointsToFetch(1), m_consecutiveFails(0), m_lastUpdate(0,0), m_isValid(false), m_isFresh(false) {;}

    bool GetTimePoints(const ISInfoDictionary &dict, bool fetchOneOnly = false);

    std::string m_name;
    int m_pointsToFetch;
    int m_consecutiveFails;
    OWLTime m_lastUpdate;
    bool m_isValid;
    bool m_isFresh; // set to true if at least one point is recieved which is more recent than last update

    static bool m_updateKeys;
    static bool m_validKeys;

    static TPointInfo m_L1Info;
    static TPointInfo m_L2Info;
    static TPointInfo m_EFInfo;
    static TPointInfo m_HLTInfo;
    static TPointInfo m_L2_CPUInfo;
    static TPointInfo m_EF_CPUInfo;
    static TPointInfo m_HLT_CPUInfo;

    // all of these are now available from runinfo, but they could be used for checking 
    static unsigned int m_current_run;
    static unsigned int m_current_lumiBlock;
    static unsigned int m_current_smk;
    static unsigned int m_current_L1_psk;
    static unsigned int m_current_HLT_psk;
  };   
} // namespace wtrp

#endif
