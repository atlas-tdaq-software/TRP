 /**
    @file chainspec.h

    Defines class ChainSpec (defines a sort order for chains in TrigConf)

    @author M. Medinnis
 */

#ifndef _CHAINSPEC_
#define _CHAINSPEC_

#include <string>
#include <vector>
#include <map>

namespace wtrp {

  
  /** This is to be used by TrigConf, mainly for sorting chains in
      some sensible way. Parse the trigger names of the 3 levels, hold
      on to the parse info. */
  class ChainSpec {

  public:
    ChainSpec(const std::string &L1, const std::string &L2, const std::string &EF): 
    m_isRun2(false), m_L1(m_L1tags, L1), m_L2(m_L2tags, L2), m_EF(m_EFtags, EF), m_HLT(m_HLTtags, ""){;}

    ChainSpec(const std::string &L1, const std::string &HLT): 
    m_isRun2(true), m_L1(m_L1tags, L1), m_L2(m_L2tags, ""), m_EF(m_EFtags, ""), m_HLT(m_HLTtags, HLT){;}
    
    bool operator<(const ChainSpec &rhs) const;
    bool operator==(const ChainSpec &rhs) const;

    void Dump() const;

  private:
    struct UnitSpec {
      UnitSpec( unsigned int n, const std::string &tag, unsigned int tagOrder,
		unsigned int thresh, bool isIsolated, const std::string &decorations ):
	m_n(n), m_tag(tag), m_tagOrder(tagOrder), m_thresh(thresh), m_isIsolated(isIsolated), m_decorations(decorations){;}
      bool operator<(const UnitSpec &rhs) const;
      bool operator==(const UnitSpec &rhs) const;
      void Dump() const;
      unsigned int m_n;
      std::string m_tag;
      unsigned int m_tagOrder;
      unsigned int m_thresh;
      bool m_isIsolated;
      std::string m_decorations;
    };
    struct LevelSpec {
      LevelSpec(const std::map<std::string, unsigned int> &tags, const std::string &name);
      bool operator<(const LevelSpec &rhs) const;
      bool operator==(const LevelSpec &rhs) const;
      void Dump() const;
      std::string m_name;
      std::vector<UnitSpec> m_units;
    };

    static std::map<std::string, unsigned int>  m_L1tags;
    static std::map<std::string, unsigned int>  m_L2tags;
    static std::map<std::string, unsigned int>  m_EFtags;
    static std::map<std::string, unsigned int>  m_HLTtags;

    bool m_isRun2;
    LevelSpec m_L1;
    LevelSpec m_L2;
    LevelSpec m_EF;
    LevelSpec m_HLT;
  };


}
#endif


