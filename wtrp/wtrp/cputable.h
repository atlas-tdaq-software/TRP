 /**
    @file cputable.h

    Defines class CPUTable -- for information on cpu usage from cpuadapter.

    @author M. Medinnis
 */

#ifndef WTRP_CPUTABLE
#define WTRP_CPUTABLE

#include "wtrp/datatable.h"
#include "wtrp/pagedata.h"
#include "wtrp/TrigConf.h"

namespace wtrp {
  
  /** Produce an info page and time history plots on cpu usage. */
  class CPUTable : public PageData {
    
  public:
    /** Default constructor (shouldn't be explicitly used). */
    CPUTable(){;}

    /** Constructor
	-# name: a unique name
	-# title: appears at top of generated page
	-# linkText: the text appears when a link to this page is made
	-# fileName: of the file which will hold the generated html page
    */
    CPUTable(const std::string &name, const std::string &title, const std::string &linkText, const std::string &fileName);

    /** Destructor (empty) */
    ~CPUTable();

    /** Dump to std::cout */
    void Dump() const;

    /** Formats the DataTable */
    void MakeTable(const TrigConf *tc);    

    /** Books CPU usage plots */
    void BookPOTs();

  private:
    /** Add a row to the DataTable */
    void AddRow( const TC_chain &tc_chain );
  };
  
}

#endif
