 /**
    @file configurator.h

    Defines classes Configurator, PlotSpec, RateSpec -- for configuring POTs from a configuration file.
    It is used for configuring the plots by RatePage. I try to keep in mind that it might be useful
    to generalize this to configuring pages.

    @author M. Medinnis
 */

#ifndef WTRP_CONFIGURATOR
#define WTRP_CONFIGURATOR

#include <string>
#include <vector>
#include <map>

#include <sys/types.h>

#include "wtrp/pot.h"

// This is a copy of what is in wmi/xml. Unfortunately that one isn't available at compile time
#include "wtrp/tinyxml.h" 

namespace wtrp {
  
  /** Decodes the configuration file and holds on to parameters needed for configuring POTs */
  class Configurator {
    
  public:
    
    /** RateSpec holds on to data needed for adding a parameter to a POT */
    class RateSpec {
      
    public:
      RateSpec(POT::PD_level level = POT::PD_none, 
	       const std::string &xlabel = "", 
	       const std::string &ylabel = "",
	       const std::string &legendName = "") :
	m_level(level), m_xlabel(xlabel), m_ylabel(ylabel), m_legendName(legendName)
	{;}
	
	POT::PD_level m_level;
	std::string m_xlabel;
	std::string m_ylabel;
	std::string m_legendName;
    };

    /** PlotSpec holds on to data needed for defining a POT and the POT's parameters. */
    class PlotSpec {
      
    public:
      PlotSpec(const std::string &name, const std::vector< std::string > &params);
      
      std::string m_name;
      std::string m_title;
      std::vector<std::string> m_options;
      std::vector<RateSpec> m_rateSpecs;
    };
    
    static void SetConfigFile( const std::string &configFileName )
      { m_configFileName = configFileName; }
    
    static bool UpdateConfiguration();
    
    static const std::vector< PlotSpec > &GetPlotSpecs()
      { return m_plotSpecs; }

    static const std::string &GetOptionLIN() {return m_optionLIN;}
    static const std::string &GetOptionLOG() {return m_optionLOG;}
    static const std::string &GetOptionMIN() {return m_optionMIN;}
    static const std::string &GetOptionVisible() {return m_optionVisible;}

    static std::string GetParam(const std::string &paramName) {
      std::map<std::string, std::string>::const_iterator p = m_genParams.find(paramName);
      if(p != m_genParams.end()) return p->second;
      return "";
    }

  private:
    
    /** Private constructor -- not meant to be instantiated */
    Configurator()
      {;}

    /** a utility for splitting strings into substrings separated by "sep". If sep is not given,
     the separator is assumed to be the first character of str */
    static size_t Split(const std::string str, std::vector<std::string> &substr, const std::string &sep = "");
    /** check the syntax of the plot and rate spec directives */
    static bool CheckSyntax( TiXmlNode* pluginNode );
    
    /** decode a plot spec. */
    static void AddPlot(const std::string &name, const std::string &value);

    /** decode a rate spec */
    static void AddRate(const std::string &name, const std::string &value);

    static std::string m_configFileName;
    static time_t    m_st_mtime; // Last time the config file was written
    
    static std::vector< PlotSpec > m_plotSpecs;
 
    static const std::string m_plotPrefix;
    static const std::string m_ratePrefix;
    static const std::string m_paramPrefix;
    static const std::string m_globalName;
    static const std::string m_optionLIN;
    static const std::string m_optionLOG;
    static const std::string m_optionMIN;
    static const std::string m_optionVisible;

    static std::map<std::string, std::string> m_genParams;

  };
  
}

#endif


