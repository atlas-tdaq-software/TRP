 /**
    @file ratepage.h

    Defines class RatePage -- for global rates, status POT

    @author M. Medinnis
 */

#ifndef WTRP_RATEPAGE
#define WTRP_RATEPAGE

#include "wtrp/pagedata.h"
#include "wtrp/pot.h"
#include "wtrp/configurator.h"

namespace wtrp {
  
  /** Generates the top level wtrp page with global rate plot and status history */
  class RatePage : public PageData {
    
  public:

    /** Default constructor, don't use. */
    RatePage(){;}

    /** Constructor, see PageData constructor for parameter definitions. */
    RatePage(const std::string &name, 
	     const std::string &title, 
	     const std::string &linkText, 
	     const std::string &fileName);

    /** Destructor (empty) */
    ~RatePage();

    /** Formats the change log table */
    void MakeTable();

    /** Books the global rates and status POTs. */
    void BookPOTs();

    /** Dump to std::cout */
    void Dump();

    /** Allows wtrp_plugin to give archive url from configuration info. */
    void SetArchivePath(const std::string &path) {m_archivePath = path;}

  private:
    std::string m_archivePath;

    /** Book and add a configurable POT to the page */
    void AddConfigurablePOT( std::vector< Configurator::PlotSpec >:: const_iterator &psit);

  };
  
}

#endif
