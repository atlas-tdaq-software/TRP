 /**
    @file wtrp_plugin.h
    The wtrp_plugin class inherits from wmi's PluginBase -- it manages updates.
    @author M. Medinnis

 */

#ifndef WTRP_PLUGIN_INCLUDED
#define WTRP_PLUGIN_INCLUDED

#include "wmi/pluginbase.h"
#include "wmi/htmloutputstream.h"
#include "wmi/table.h"

#include "owl/time.h"

#include "TRP/TimePoint_IS.h"

#include "wtrp/TrigConf.h"
#include "wtrp/rawpage.h"
#include "wtrp/statuspage.h"
#include "wtrp/ratepage.h"
#include "wtrp/streamtable.h"
#include "wtrp/grouptable.h"
#include "wtrp/chaintable.h"
#include "wtrp/l1table.h"
#include "wtrp/cputable.h"
#include "wtrp/tpointinfo.h"

using namespace wtrp;

namespace daq{

  namespace wmi{
    
/** The wmi plugin class for wtrp. It owns all pagedata objects apart
 * from those owned by the page objects themselves. The periodicAction
 * member function is called about once per minute. It manages the updates.
 */

    class wtrpPlugin:public PluginBase{
    public:
      /** The only constructor -- create empty page objects, set some defaults. */
      wtrpPlugin();

      /** Called periodically (about one minute after completion of last update) to manage updates */
      void periodicAction();

      /** Clean up any resources (delete a tmp directory) */
      void stop();

      /** Read and set configuration variables from the configuration file */
      void configure(const ::wmi::InfoParameter& params);

      /** Destructor */
      ~wtrpPlugin();

    private:

      /** Configure some of the pages, depending on which running period */
      void ConfigureRun1Pages(bool newConfiguration, 
			      bool &updateL2Raw, bool &updateEFRaw, 
			      bool &updateL2CPURaw, bool &updateEFCPURaw);
      void ConfigureRun2Pages(bool newConfiguration, 
			      bool &updateHLTRaw);

      void BookPOTs();
      
      bool m_isConfigured;

      std::string m_partitionName;
      std::string m_TRPserverName;
      std::string m_trigConDBName;
      std::string m_barFilePath;
      std::string m_barFileName;
      std::string m_archivePath;

      wtrp::TrigConf *m_triggerConfiguration;
      RatePage m_ratePage;
      StreamTable m_streamTable;
      GroupTable m_groupTable;
      L1Table m_l1Table;
      CPUTable m_cpuTable;
      RawPage m_L1raw;
      RawPage m_L2raw;
      RawPage m_EFraw;
      RawPage m_HLTraw;
      RawPage m_L2CPUraw;
      RawPage m_EFCPUraw;

      StatusPage m_WTRPstatus;
      std::map<std::string, ChainTable> m_chainTables;
    };
    
    /** Called by the wmi server to create the plugin */
    extern "C" PluginBase* create() {
      return new wtrpPlugin;
    }
    
    /** deletes the wtrp plugin object */
    extern "C" void destroy(PluginBase* p) {
      delete p;
    }
    
  }//end wmi namespace
}//end daq namespace

#endif

