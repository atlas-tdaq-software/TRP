 /**
    @file potbase.h

    Defines the POTBase class

    @author M. Medinnis
 */

#ifndef _WTRP_POTBASE_
#define _WTRP_POTBASE_

#include <iostream>
#include <deque>
#include <limits>

#include <sys/time.h>
#include <owl/time.h>
#include <owl/timer.h>

#include "TAxis.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TDatime.h"
#include "TH1F.h"
#include "TLegend.h"
#include "TPaveLabel.h"
#include "TROOT.h"
#include "TStyle.h"

#include "TimePoint_IS.h"
#include "wtrp/rctlstats.h"

#include "wmi/htmloutputstream.h"
#include "wmi/text.h"
#include "wmi/picture.h"
//#include "wmi/picturesvg.h"

/* Parameter Over Time, in case you are wondering */

namespace wtrp {

  /** The virtual base class for Parameter over Time (POT) plots. Each
   POT object contains the histories of typically many parameters
   values over the last approx 24 hours with a more fine-grained
   history of the last 30 minutes. Two classes inherit from POTBase:
   POT which produces line plots of parameters (usually rates) and
   statPOT which is intended for display of status parameters such as
   the status of servers or lumi block and run numbers. The time axis
   refers to the time at the beginning of the update.
  */

  class POTBase{ // base class for POTs

  public:

    /** Delete the parameter histories */
    virtual ~POTBase();

    /** Call for each update, before updating POTs */
    static bool Prepare();

    /** Return the name of the POT */
    const std::string &GetName(){return m_name;}

    /** Set the Visible flag (if false, plots are made but not linked to any page */
    void SetVisible(bool isVisible){ m_isVisible = isVisible;}

    /** return a pointer to the POT specified by name */
    static POTBase *GetPOT(const std::string &name);

    /** Delete the POT with given name */
    static void DeletePOT(const std::string &name);

    /** Dump the contents of all POTs */
    static void DumpPOTs();

    /** Update all POTs */
    static void UpdatePOTs();

    /** Helper class for holding on to variables filled in GetPOTPlots
	and used in POTBase::OutputPOT */
    struct PlotStuff {
      std::string fileName; 
      std::string caption; 
      unsigned int width; 
      unsigned int height;
      bool isVisible;
    };
    typedef struct POTBase::PlotStuff PlotStuff;

    /** Output the POT specified by potName to outStream */
    static bool OutputPOT(const std::string &potName, 
			  daq::wmi::OutputStream* outStream,
			  std::vector<PlotStuff> &invisiblePlots
			  );

    /** Dump the POT */
    virtual void Dump() const = 0;

    /** Return the value of m_now. m_now is set in UpdatePots at the beginning of each update */
    static const OWLTime &GetNow(){ return m_now; }

    /** For commissioning. Get the value of a couple of timers */
    static void GetTimers(double &timer1, double &timer2) { timer1 = m_timer1.totalTime(); timer2 = m_timer2.totalTime();}

    /** If parameters are not found during an update, add them to the
	list. This function returns a reference to the list. */
    static std::string &GetMissingParameters(){ return m_missingParameters;}

    /** Delete resources before exiting */
    static void TidyUp(bool deleteDir = false);

    /** Return a reference to the long-term time history deque */
    static const std::deque<long> &getLongTimeStamps() {return m_timeStamps_l;}

    /** Return the process start time in microsec since the beginning of the epoch. */
    static long long GetStartupTime() {return m_startupTime;}

    /** Return a reference to the reference time ( == startupTime) as an OWLTime object */
    static const OWLTime &GetReferenceTime() {return m_referenceTime;}

    /** Returns an html image map which is made by statpot and applied
        to all POTs. The name is misleading: there is only one image
        map in wtrp. */
    static const std::string *GetDefaultImageMap() { return m_defaultImageMap;}

    /** Get the name of the javascript used for attaching the image
     map to the images.  Note that wmi doesn't provide for image maps
     so the map is inserted in the html in plane text. When the user
     moves the cursor over an image for the first time,
     m_mapAttacherScript attaches the image map to all POTs on the
     page. */
    static const std::string &GetMapAttacherScript(){ return m_mapAttacherScript;}

  protected:

    POTBase(std::string name, std::string title);


    /** Change the title */
    void SetTitle(const std::string &newTitle) {m_title = newTitle;}

    /** Write the ROOT plots to a tmp directory and produce a vector of PlotStuff
	objects with needed details. */
    virtual bool GetPOTPlots(std::vector<PlotStuff> &plotStuff) const = 0;

    /** Updates all POTbase objects */
    virtual void Update(bool updateLong) = 0;

    /** Set ROOT style parameters */
    static void SetCommonStyleParameters(TStyle *theStyle);

    /** Convert time (as stored in the time history deques to a string:
	HH:MM (used by the image map generator */
    static void ConvertTimeToString(long time, std::string &timeString); // "HH:MM"

    /** Dump the base class variables */
    void DumpBase() const;

    /** Make a tmp directory used for outputting ROOT plots */
    static bool MakeTmpDir();

    static std::vector<POTBase*> m_thePOTs;
    static std::string m_tmpPath;

    static std::string *m_defaultImageMap;
    static const std::string m_mapAttacherScript;

    static TStyle *m_potStyle_s; // for the short-term plot
    static TStyle *m_potStyle_l; // for the long-term plot
    static TColor *m_potColor;
    static int m_graphColorOffset;

    std::string m_name;
    std::string m_title;
    /** If m_isVisible is false, the plot will be produced but not attached to any page */
    bool m_isVisible;

    static std::string m_missingParameters;
    static std::deque<long> m_timeStamps_s; // in seconds since m_startupTime;
    static std::deque<long> m_timeStamps_l;
    static unsigned int m_nWaiting; // # points waiting to be averaged
    static long m_timeWaiting;      // time sum (sec since m_startupTime) of waiting points

    const static unsigned int m_length_l; // max length of long-term deque
    const static unsigned int m_length_s; // max length of short-term deque

    const static unsigned int m_maxParsPerPlot;

    const static long long m_aCoolMill;
    const static unsigned int m_pointsToAve; // num. points to average for long-term deque

    const static long m_twentyFourHours;

    const static std::string  m_fileType; // e.g. svg or png
    const static unsigned int m_titleHeight;
    const static unsigned int m_canWidth;
    const static unsigned int m_hisTopMarg;
    const static unsigned int m_hisBottomMarg;
    const static unsigned int m_padWidth_l;
    const static unsigned int m_axisLabelSize;
    const static unsigned int m_leftMargin_l;
    const static unsigned int m_rightMargin_l;
    const static unsigned int m_leftMargin_s;
    const static unsigned int m_rightMargin_s;
    const static unsigned int m_font;
    const static          int m_lineWidth;
    const static          int m_dottedLine;
    const static        float m_dotSize;
    const static          int m_polyInPixels;


    static                int m_dotSpacing_s;
    static                int m_dotSpacing_l;

    static       unsigned int m_canWidthDiff; // to go from canvas size to picture size, subtract this
    static       unsigned int m_canHeightDiff;

    static long long m_startupTime;
    static OWLTime m_referenceTime;
    static OWLTime m_now;

    static OWLTimer m_timer1; // for timing tests
    static OWLTimer m_timer2; // for timing tests

  };
}
#endif
