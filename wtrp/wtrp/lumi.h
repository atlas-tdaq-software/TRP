 /**
    @file lumi.h

    Defines class Lumi -- fetches, serves locally any needed lumi info

    @author M. Medinnis
 */

#ifndef TRP_LUMI
#define TRP_LUMI

#include <string>
#include <owl/time.h>

#include "ipc/core.h"
#include "is/infodictionary.h"

namespace wtrp {

  /** Fetches, serves locally any needed lumi info. Presently, not
   used and will need further work to be useful -- for example to
   produce a plot dedicated to showing the instantaneous lumi from
   various detectors, for example. */
  class Lumi {
    
  public:
    /** Destructor (empty) */
    ~Lumi();

    /** Fetch all needed info from IS */
    static void GetFromServer();

    /** Set val to the current lumi (using IS source m_lumiVariable) */
    static bool GetLumi(double &val){val = m_lumi; return m_isFresh;}

    /** Return the current interaction rate inferred from m_lumi and
	an assumed cross section. */
    static bool GetInteractionRate(float &val){val = m_rate; return m_isFresh;}

    /** Returns true if the last fetch attempt was successful */
    static bool isValid(){return m_isValid;}

    /** Returns true if the most recent fetch got info which is more recent than the last */
    static bool isFresh(){return m_isFresh;}

  private:

    const static std::string m_lumiPartition;
    const static std::string m_lumiServer;
    const static std::string m_lumiVariable;

    const static double m_sigma; // inelastic sigma at injection energy -- temp. kloodge

    /** Private constructor, only static member functions are defined. */
    Lumi();

    /** Return the assumed inelastic cross section. This would need
	work before it can be used -- it just returns the cross
	section at 450 GeV */
    static double GetInelasticCrossSection(){return m_sigma;}

    static bool m_isValid;
    static bool m_isFresh;

    static long long m_now; // set in GetFromServer to the current time in microsec since epoch

    static double m_lumi;
    static float m_rate;

  };
}

#endif
