 /**
    @file statuspage.h

    Defines class StatusPage -- for producing page which summarizes some status info

    @author M. Medinnis
 */

#ifndef WTRP_STATUSPAGE
#define WTRP_STATUSPAGE

#include "TRP/TimePoint_IS.h"
#include "wtrp/pagedata.h"
#include "wtrp/datatable.h"
#include "wtrp/pot.h"

namespace wtrp {
  
  /** For producing a status page. At the moment, it's not very useful. */
  class StatusPage : public PageData {
    
  public:
    /** Default constructor (do not use) */
    StatusPage(){;}

    /** Constructor, see PageData constructor for parameter definitions. */
    StatusPage(const std::string &name, 
	       const std::string &title, 
	       const std::string &linkText, 
	       const std::string &fileName);

    /** Destructor (empty) */
    ~StatusPage();

    /** Dump to std::cout */
    void Dump();

    /** Format the status table */
    void MakeTable();

    /** Called by Online tools to get the names of rows in the table */
    const std::vector<std::string> &GetVars() const {return m_vars;}

    /** Formats an returns a list of parameters which the POT updater
	couldn't find. Not used at the moment. */
    void GetPostamble(std::string &postamble) const;

  private:

    std::vector<std::string> m_vars;
  };
  
}

#endif
