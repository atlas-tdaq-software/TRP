 /**
    @file rawpage.h

    Defines class RawPage -- for producing pages from TimePoints automatically

    @author M. Medinnis
 */

#ifndef WTRP_RAWPAGE
#define WTRP_RAWPAGE

#include "TRP/TimePoint_IS.h"
#include "wtrp/pagedata.h"
#include "wtrp/datatable.h"
#include "wtrp/pot.h"

namespace wtrp {
  
  /** for producing pages from TimePoints automatically */
  class RawPage : public PageData {
    
  public:
    /** Default constructor -- shouldn't be called */
    RawPage() : m_nEntries(0) {;}

    /** Constructor, see PageData constructor for parameter definitions. */
    RawPage(const std::string &name, 
	    const std::string &title, 
	    const std::string &linkText,
	    const std::string &fileName);

    /** Destructor (empty) */
    ~RawPage();

    /** Returns the number of x labels in the last TimePoint
	seen. Used by wtrp_plugin to decide if table needs to be
	reformatted. */
    unsigned int GetNEntries() { return m_nEntries; }

    /** Dump to std::cout */
    void Dump();

    /** Formats a table from a TimePoint */
    void MakeTable(const TimePoint_IS &tp);

  private:
    unsigned int m_nEntries; // the number of x labels -- used to check for changes
  };
  
}

#endif
