 /**
    @file runinfo.h

    Defines class RunInfo -- fetches, serves locally any needed info from RunInfo.

    @author M. Medinnis
 */

#ifndef WTRP_RUNINFO
#define WTRP_RUNINFO


#include <string>

#include "rc/RunParams.h"
//#include "rc/LuminosityInfo.h"
#include "TTCInfo/LumiBlock.h"

#include "is/infodictionary.h"
#include "is/infoany.h"

namespace wtrp {

      /** Fetches, serves locally any needed info from RunInfo.  Run
	  specific parameters like run number, run type but also
	  including the trigger configuration keys.
        */
  class RunInfo {
    
  public:
    /** Destructor (empty) */
    ~RunInfo();

    /** Fetch info from server. */
    static void GetFromServer(bool partitionValid, const ISInfoDictionary &dict);

    /** Return true if the latest fetch was successful */
    static bool UpdateValid(){ return m_runParamsValid && m_lumiValid;}

    /** Set val to true if all info successfully fetched */
    static bool GetUpdateStatus(unsigned int &val)
      { val = (m_runParamsValid && m_lumiValid); return val;}
    
    /** Return the current run number (for status display) */
    static bool GetRunNumber(unsigned int &runNumber);

    /** Return the current run type (for status display) */
    static bool GetRunType(std::string &runType);

    /** Return the current lumi block number (for status display) */
    static bool GetLumiBlock(unsigned int &lumiBlock);

    /** Return the super master key (for status display) */
    static bool GetSMK(unsigned int &val)    { val = m_smk; return m_smkValid;}

    /** Return the current L1 prescale key (for status display) */
    static bool GetL1_PSK(unsigned int &val) { val = m_L1psk; return m_L1pskValid;}

    /** Return the current HLT prescale key (for status display) */
    static bool GetHLT_PSK(unsigned int &val){ val = m_HLTpsk; return m_HLTpskValid;}

    /** Return the current bunch group key (for status display) */
    static bool GetBunchGroup(unsigned int &val){ val = m_bunchGroup; return m_bunchGroupValid;}

    /** Return the current value of the "ready for physics" flag (for status display) */
    static bool GetReady4Physics(unsigned int &val){ val = m_ready4Physics; return m_ready4PhysicsValid;}

    /** Return the status of the partition (for status display) */
    static bool GetPartitionValid(unsigned int &val){val = (m_partitionValid)? 1:0; return m_partitionValid;}

    /** Return true if the SMK, L1psk and HLTpsk are valid. */
    static bool ValidKeys(){return m_smkValid && m_L1pskValid && m_HLTpskValid;}

    /** Return the current run number, if valid, otherwise 0 */
    static unsigned int GetRunNumber(){return (m_runParamsValid)? m_runParams.run_number : 0;}

    /** Return the current SMK */
    static unsigned int GetSMK(){return m_smk;}
 
    /** Return the current L1 prescale key */
    static unsigned int GetL1_PSK(){return m_L1psk;}

    /** Return the current HLT prescale key */
    static unsigned int GetHLT_PSK(){return m_HLTpsk;}

    /** Return the current bunch group number */
    static unsigned int GetBunchGroup(){return m_bunchGroup;}

    /** For testing, set the trig conf keys as specified */
    static void SetKeysForTest(unsigned int smk, unsigned int L1_psk, unsigned int HLT_psk)
      { m_smk = smk; m_L1psk = L1_psk; m_HLTpsk = HLT_psk; m_updateKeys = false; 
	m_smkValid = true; m_L1pskValid = true; m_HLTpskValid = true;}

    /** Returns true if the SMK changed between the last two fetches */
    static bool SMKChanged(){return m_smkChanged;}

  private:

    RunInfo();

    /** Fetch the requested info from IS */
    static bool GetISInfo(const ISInfoDictionary &dict, const std::string &infoName, ISInfo &info);

    /** Fetch the specified trigger configuration key from IS */
    static bool FetchTrigConfigKey(const ISInfoDictionary &dict, const std::string &name, unsigned int &value);

    static bool m_updateKeys;
    static bool m_partitionValid;
    static bool m_runParamsValid;
    static bool m_lumiValid;
    static bool m_smkValid;
    static bool m_L1pskValid;
    static bool m_HLTpskValid;
    static bool m_bunchGroupValid;
    static bool m_ready4PhysicsValid;

    static RunParams m_runParams;
    //static LuminosityInfo m_lumiInfo;
    static LumiBlock m_lumiInfo;
    static unsigned int m_smk;
    static unsigned int m_L1psk;
    static unsigned int m_HLTpsk;
    static unsigned int m_bunchGroup;
    static unsigned int m_ready4Physics;

    static bool m_smkChanged;
  };
}

#endif
