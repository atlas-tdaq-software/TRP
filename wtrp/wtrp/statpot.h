 /**
    @file statpot.h

    Defines class StatPOT (status parameters over time)

    @author M. Medinnis
 */

#ifndef _WTRP_STATPOT_
#define _WTRP_STATPOT_

#include <iostream>
#include <vector>
#include <map>

#include <TBox.h>
#include <TText.h>

#include "wtrp/potbase.h"
#include "wtrp/datatable.h"

namespace wtrp {

  /** The StatPOT class is used to produce a display of parameters
      which don't change frequently over time. The only use of it so
      far is the "Status History" plot on the global rates page. It
      produces a table containing changes of interesting parameters
      (as defined by the user), there new values and the time at which
      they changed. It produces an html image map which is attached to
      all POTs.
  */
  class StatPOT : public POTBase {
    
  public:
    
    /** For making a map from integer parameter value to a string and a color */
    class SetupMapEntry {
    public:
      SetupMapEntry() : m_colorIndex(0) {;}
      SetupMapEntry(unsigned int colorIndex, const std::string &label) : m_colorIndex(colorIndex), m_label(label){;}
      unsigned int m_colorIndex;
      std::string m_label;
    };
    
    /** Class holding data needed for definition of displayed parameters. */
    struct pardef{
      /** label appearing in box on left of display */
      std::string axisLabel;

      /** pointer to function which returns the integer value of the parameter */
      bool (*GetValue)(unsigned int &theValue);

      /** pointer to function which returns the string value of the parameter */
      bool (*GetString)(std::string &theValue);

      /** A map from parameter value to string/color pair */
      std::map<unsigned int, struct SetupMapEntry> setupMap;

      /** If isToggle, the plotted color toggles when the paramter value changes */
      bool isToggle; // toggles don't use a setupMap

      /** If true, an entry is made in the history table whenever this parameter changes */
      bool tabulate; // make an entry in the changes table whenever a change is seen

      /** If true, a change in this parameter will cause a boundary in the html image map to be made */
      bool genImageMapBoundary;

      /** If true, a range of values is printed in the tool tip (e.g. 100 - 110) instead of a fixed value */
      bool genImageMapRange;

      /** An html image map boundary is generated if the difference between the value at the start of the
       last boundary and the current value differ by this amount. */
      int deltaBoundary; // generate an image map boundary when diff of deltaBoundary wrt first point(for LB)

      /** Data for the last 1/2 hour -- must be synchronized with m_timeStamps_s(_l)  (i.e. must be updated for each call to UpdatePOTs) */
      std::deque<unsigned int> data_s; // short-term deque

      /** Data for the last 24 hours */
      std::deque<unsigned int> data_l; // long-term deque

      /** (Optionally) maps parameter values to text strings and colors */
      std::map<unsigned int, std::string> textMap; // needed when GetString is used 

      /** For points awaiting transfer to the long-term plot */
      std::vector<unsigned int> transPoints; // keeps partially built point before inserting in data_l
    };
    typedef struct pardef ParDef;
    
    /** Parameter options.
	-# opt_toggle -- causes color to toggle between 2 values when parameter changes
	-# opt_tabulate -- causes an entry to be made in the history table when the parameter changes
	-# opt_genMapBoundary -- causes a boundary in the html image map to be generated on changes
	-# opt_mapRange -- causes the values at beginning & end of interval to be printed in the tooltip
    */
    enum StatOpts {opt_toggle=0x1, opt_tabulate=0x2, opt_genMapBoundary=0x4, opt_mapRange=0x8};

    /** Constructor -- name is unique, title will be printed on the top of the plot */
    static StatPOT *BookStatPOT(const std::string &name, const std::string &title);

    /** Add an integer-valued parameter. See ParDefs for definition of inputs */
    void AddParameter(bool (*GetValue)(unsigned int &theValue), const std::string &axisLabel,
		      const std::map<unsigned int, SetupMapEntry> &setupMap = emptyMap, 
		      unsigned int opts = 0u, int deltaBoundary = -1);

    /** Add a string-valued parameter. See ParDefs for definition of inputs */
    void AddStringParameter(bool (*GetString)(std::string &theValue), const std::string &axisLabel,
		      const std::map<unsigned int, SetupMapEntry> &setupMap = emptyMap, 
			    unsigned int opts = 0u);

    /** Set the history log pointer to changeLog. This is not an
     elegant solution but the only instance of a StatPOT is owned and
     set up by RatePage and passed to StatPOT which fills and
     maintains it.*/
    void SetChangeLog(DataTable *changeLog){m_changeLog = changeLog;}

    /** Enables generation of an html image map 
	-# name -- appears in the html
	-# GetURL -- a function which generates a target url for a map entry
	-# isDefault -- true if this is the map to be used for mapping all POTs by default
     */
    void EnableImageMapGeneration( const std::string& name, 
				   void(*GetURL)(const std::vector<std::string>&labels, 
						 const std::vector<unsigned int>&values,
						 std::string &url), bool isDefault );

    /** Returns  a const reference to the html image map */
    const std::string &GetImageMap() const {return m_theImageMap;}

    /** Formats and returns the appropriate URL for fetching the trigger configuration for 
     given SMK, L1_psk, HLT_psk */
    static void GetTrigConfURL
      (const std::vector<std::string> &labels, 
       const std::vector<unsigned int> &values,
       std::string &url);

    /** Dump contents to std::cout */
    void Dump() const;



  private:

    /** For holding on to correspondence between change log entries and time of entry -- used
     to determine when to delete the entry. */
    class ET_Entry {
    public:
      ET_Entry(unsigned int number, unsigned int time) : m_number(number), m_time(time) {;}
      unsigned int m_number;
      long m_time;
    };

    /** Hidden constructor */
    StatPOT(const std::string &name, const std::string &title);
    
    /** Update all parameters. */
    void Update(bool updateLong );

    /** Make the html image map. Called after statPot is updated -- in Update */
    void MakeImageMap();

    /** Supplies the text for an Area html tag */
    void MakeNewAreaTag(long startTime, long endTime, int startTimePix, int endTimePix, 
			const std::vector<std::string> &labels, 
			const std::vector<unsigned int> &startValues, 
			const std::vector<unsigned int> &nextValues, 
			std::string &newArea);
    
    /** Creates the plot and writes it to disk. */
    bool GetPOTPlots(std::vector<PlotStuff> &plotStuff) const;

    /** Called by GetPOTPlots for each parameter, does the necessary graphics for it. */
    void FillPlot(bool doShortPlot, const ParDef &par, unsigned int iPar, unsigned int nPar, 
		  double charHeightFrac,  double userPerPix, std::vector<TBox *> &boxes, 
		  std::vector<TText * > &labels, unsigned int *presentColorId = 0, 
		  std::string *presentText = 0) const;

    /** Sets up the boxes on the right side of the display which hold the current parameter values */
    void SetPresentValueBoxes(TPad *hisPad_s, double xmax_s, double parHeightFrac, double userPerPix_s, 
			      std::vector<TBox *> &presentValueBoxes, 
			      std::vector<TBox *> &fillBoxes,
			      std::vector<TText *> &presentValueTexts,
			      std::vector<unsigned int> &presentColorIds,
			      std::vector<std::string> &presentTexts) const;
    
    std::vector<ParDef> m_parDefs; 
    std::deque<ET_Entry> logEntries;

    DataTable *m_changeLog;
    unsigned int m_entryCounter;
    static unsigned int m_invalid;
    
    bool m_generateImageMap;
    std::string m_imageMapName;
    std::string m_theImageMap;
    void (*m_GetURL)
      (const std::vector<std::string> &labels, 
       const std::vector<unsigned int> &values,
       std::string &url);

    const static unsigned int m_invalidColorId = kGray+2;
    const static unsigned int m_toggleColorLight = kCyan-9;
    const static unsigned int m_toggleColorDark = kCyan+1;
    const static unsigned int m_heightPerPar = 20;
    const static unsigned int m_yLabelsBoxWidth = 75;
    const static unsigned int m_yLabelsBoxFill = kCyan-10;
    const static unsigned int m_presentValueBoxWidth = 60;
    const static unsigned int m_imageMapHeight = 500;

    const static double m_charYOffset;
    const static std::string m_urlTrigConf;

    const static std::map<unsigned int, SetupMapEntry> emptyMap;

  };
}
#endif
