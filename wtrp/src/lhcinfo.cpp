#include "wtrp/lhcinfo.h"

#include <iostream>
#include <sstream>
#include "is/infoany.h"
#include "ddc/DdcIntInfo.h"

namespace wtrp {

  const std::string LHCInfo::m_lhcPartitionName = "initial";
  const std::string LHCInfo::m_lhcServer = "LHC";
  const std::string LHCInfo::m_stableBeamsVariable = m_lhcServer + ".StableBeamsFlag";
  
  bool LHCInfo::m_isValid = false;
  unsigned int LHCInfo::m_stableBeams;
  
  void LHCInfo::GetFromServer() {
    
    m_isValid = false;
     
    // Create an instance of the partition
    IPCPartition lhcPartition(m_lhcPartitionName);
    
    // Create the IS dictionary 
    ISInfoDictionary dict(lhcPartition);
    
    //ISInfoAny info;
    ddc::DdcIntInfo info;
    if (!GetISInfo(dict, m_stableBeamsVariable, info)) {
      //std::cout << "LHCInfo::GetFromServer -- " << name << " not found" << std::endl;
      return;
    } else {
      //std::cout << info.value << std::endl;
      m_stableBeams = (info.value > 0)? 1 : 0;
      m_isValid = true;
    }
  }
  
  bool LHCInfo::GetISInfo(const ISInfoDictionary &dict, const std::string &infoName, ISInfo &info)
  {
    try {
      dict.getValue( infoName, info );
    }
    catch (daq::is::InvalidName &) {
      //std::cout << "LHCInfo:GetISInfo: failed to get " << infoName << ": InvalidName" << std::endl;
      return false;
    }
    catch (daq::is::RepositoryNotFound &) {
      //std::cout << "LHCInfo:GetISInfo: failed to get " << infoName << ": Repository not found" << std::endl;
      return false;
    }
    catch (daq::is::InfoNotFound &) {
      //std::cout << "LHCInfo:GetISInfo: failed to get " << infoName << ": Info not found" << std::endl;
      return false;
    }
    catch (daq::is::InfoNotCompatible &) {
      //std::cout << "LHCInfo:GetISInfo: failed to get " << infoName << ": Info not compatible" << std::endl;
      return false;
    }
    return true;
  }
  
}
