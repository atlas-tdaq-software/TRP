#include "wtrp/OnlineTools.h"


#include "wmi/utils.h"
#include "wmi/text.h"
#include "wmi/picture.h"
#include "wmi/link.h"

#include "is/infodictionary.h"
#include "is/infoiterator.h"

#include <owl/time.h>

#include <algorithm>
#include <iostream>
#include <iomanip>
#include <sstream>

using namespace daq::wmi;


namespace wtrp {

  const int wtools::maxBarPixels = 100;
  const int wtools::barHeight = 15;
  const int wtools::precision = 4;

  const int wtools::ratePrecision = 4;
  const std::ios_base::fmtflags wtools::fixOrSci = std::ios_base::fixed;
  
  const std::string wtools::barCode = "MakeBar";
  const float wtools::fracLim = 1.01; // (just to keep bars from getting to long in case of a bug)
  std::string wtools::barFile;
  
  std::map<std::string, wtools::RateInfo> wtools::L2_groupRates; // filled by FillGroupTable, used by FillChainTables
  std::map<std::string, wtools::RateInfo> wtools::EF_groupRates; // ditto
  
  std::map<std::string, wtools::RateInfo> wtools::HLT_groupRates; // ditto
  
  void wtools::SetBarFile(std::string name) {barFile = name;}

    
  void wtools::SetConfiguration_DB(std::string trigcondb, unsigned int smk, unsigned int L1_psk, unsigned int HLT_psk, TrigConf *tc)
  {
    std::cout << "SetConfiguration_DB start" << std::endl;

    confadapter_imp::TriggerMenuHelpers tmh(trigcondb);

    try {
      std::cout << "attempting to fetch configuration corresponding to keys: " << smk
		<< ", " << L1_psk << ", " << HLT_psk << std::endl;

      tmh.getConfiguration(smk, HLT_psk, L1_psk);
    }
    catch(std::runtime_error &e) {
      std::cout << "SetConfiguration_DB: failed to retrieve trigger configuration -- " << e.what() << std::endl;
      return;
    }
    catch(...) {
      std::cout << "SetConfiguration_DB: failed to retrieve trigger configuration, unknown error" << std::endl;
      return;
    }
    
    if(tmh.lhcRun()>=2) {
      Configure_Run2(tmh, tc);
    } else {
      Configure_Run1(tmh, tc);
    }
  }
 
  void wtools::Configure_Run2( confadapter_imp::TriggerMenuHelpers &tmh, TrigConf *tc )
  {
    tc->setRunConfiguration(true);

    const confadapter_imp::TriggerMenuHelpers::Mapping & m_hlt_l1 = tmh.getMapping("hlt_l1");
    confadapter_imp::TriggerMenuHelpers::Mapping::const_iterator ihlt_l1;
    for(ihlt_l1 = m_hlt_l1.begin(); ihlt_l1 != m_hlt_l1.end(); ihlt_l1++) {
      const std::string &hlt_name = ihlt_l1->first;
      const std::string &l1_name = ( (ihlt_l1->second).size() > 0)? (ihlt_l1->second)[0] : "unknown";
      
      TC_chainInLevel L1(l1_name);
      TC_chainInLevel HLT(hlt_name);
      TC_chain tc_chain(hlt_name, L1, HLT);
      tc->InsertChain(tc_chain);
    }

    const confadapter_imp::TriggerMenuHelpers::Mapping & m_str_hlt = tmh.getMapping("str_hlt");
    //std::cout << " str_hlt map" << std::endl;
    //tmh.printMapping(m_str_hlt);
    confadapter_imp::TriggerMenuHelpers::Mapping::const_iterator istr_hlt;
    for(istr_hlt = m_str_hlt.begin(); istr_hlt != m_str_hlt.end(); istr_hlt++) {
      tc->AddStream(TC_HLT, istr_hlt->first, istr_hlt->second);

      // add to the stream prescale map
      const confadapter_imp::TriggerMenuHelpers::MapStrInt & streamPrescales = tmh.getStreamPrescalesMap( istr_hlt->first );
      for( confadapter_imp::TriggerMenuHelpers::MapStrInt::const_iterator 
              isc = streamPrescales.begin(); isc != streamPrescales.end(); isc++) {
         tc->AddStreamPrescale(istr_hlt->first, isc->first, isc->second);
      }
    }

    const confadapter_imp::TriggerMenuHelpers::Mapping & m_gr_hlt = tmh.getMapping("gr_hlt");
    //std::cout << " gr_hlt map" << std::endl;
    //tmh.printMapping(m_gr_hlt);
    confadapter_imp::TriggerMenuHelpers::Mapping::const_iterator igr_hlt;
    for(igr_hlt = m_gr_hlt.begin(); igr_hlt != m_gr_hlt.end(); igr_hlt++) {

      // 13.4.2010 Insert only the rate groups
      //if( (igr_hlt->first).substr(0,5) == "Rate:") {
      if( (igr_hlt->first).substr(0,5) == "RATE:" || (igr_hlt->first).substr(0,5) == "Rate:") 
	{ // Rate changed to RATE in run 2(!)
	  tc->AddGroup(TC_HLT, (igr_hlt->first).substr(5), igr_hlt->second);
	}
    }
    

    const confadapter_imp::TriggerMenuHelpers::Mapping & m_hlt_ps = tmh.getMapping("hlt_ps");
    //std::cout << " hlt_ps map" << std::endl;
    //tmh.printMapping(m_hlt_ps);
    confadapter_imp::TriggerMenuHelpers::Mapping::const_iterator i_hlt_ps;
    for(i_hlt_ps = m_hlt_ps.begin(); i_hlt_ps != m_hlt_ps.end(); i_hlt_ps++) {
      if(i_hlt_ps->second.size() > 0){
	std::stringstream ss(i_hlt_ps->second[0]);
	int ipre;
	ss >> ipre;
	tc->SetPrescale(TC_HLT, i_hlt_ps->first, ipre );
      }
    }
  }

  void wtools::Configure_Run1( confadapter_imp::TriggerMenuHelpers &tmh, TrigConf *tc )
  {
    tc->setRunConfiguration(false);
    const confadapter_imp::TriggerMenuHelpers::Mapping & m_ef_l2 = tmh.getMapping("ef_l2");
    const confadapter_imp::TriggerMenuHelpers::Mapping & m_l2_l1 = tmh.getMapping("l2_l1");      
    confadapter_imp::TriggerMenuHelpers::Mapping::const_iterator ief_l2;
    for(ief_l2 = m_ef_l2.begin(); ief_l2 != m_ef_l2.end(); ief_l2++) {
      const std::string &ef_name = ief_l2->first;
      const std::string &l2_name = ( (ief_l2->second).size() > 0)? (ief_l2->second)[0] : "unknown";
      confadapter_imp::TriggerMenuHelpers::Mapping::const_iterator el2_l1 = m_l2_l1.find(l2_name);
      const std::string &l1_name = (el2_l1 != m_l2_l1.end())? (el2_l1->second)[0] : "unknown";
      //std::cout << "full chain: " << l1_name << " --> " << l2_name << " --> " << ef_name << std::endl;
      
      TC_chainInLevel L1(l1_name);
      TC_chainInLevel L2(l2_name);
      TC_chainInLevel EF(ef_name);
      TC_chain tc_chain(ef_name, L1, L2, EF);
      tc->InsertChain(tc_chain);
    }

    const confadapter_imp::TriggerMenuHelpers::Mapping & m_str_ef = tmh.getMapping("str_ef");
    confadapter_imp::TriggerMenuHelpers::Mapping::const_iterator istr_ef;
    for(istr_ef = m_str_ef.begin(); istr_ef != m_str_ef.end(); istr_ef++) {
      tc->AddStream(TC_EF, istr_ef->first, istr_ef->second);

      // add to the stream prescale map
      const confadapter_imp::TriggerMenuHelpers::MapStrInt &  streamPrescales = tmh.getStreamPrescalesMap(istr_ef->first );
      for(confadapter_imp::TriggerMenuHelpers::MapStrInt::const_iterator 
             isc = streamPrescales.begin(); isc != streamPrescales.end(); isc++) {
         tc->AddStreamPrescale(istr_ef->first, isc->first, isc->second);
      }
    }

    const confadapter_imp::TriggerMenuHelpers::Mapping & m_str_l2 = tmh.getMapping("str_l2");
    confadapter_imp::TriggerMenuHelpers::Mapping::const_iterator istr_l2;
    for(istr_l2 = m_str_l2.begin(); istr_l2 != m_str_l2.end(); istr_l2++) {
      tc->AddStream(TC_L2, istr_l2->first, istr_l2->second);
    }
    
    
    const confadapter_imp::TriggerMenuHelpers::Mapping & m_gr_ef = tmh.getMapping("gr_ef");
    confadapter_imp::TriggerMenuHelpers::Mapping::const_iterator igr_ef;
    for(igr_ef = m_gr_ef.begin(); igr_ef != m_gr_ef.end(); igr_ef++) {
      // 13.4.2010 Insert only the rate groups
      if( (igr_ef->first).substr(0,5) == "Rate:") {
	tc->AddGroup(TC_EF, (igr_ef->first).substr(5), igr_ef->second);
      }
    }

    const confadapter_imp::TriggerMenuHelpers::Mapping & m_gr_l2 = tmh.getMapping("gr_l2");
    confadapter_imp::TriggerMenuHelpers::Mapping::const_iterator igr_l2;
    for(igr_l2 = m_gr_l2.begin(); igr_l2 != m_gr_l2.end(); igr_l2++) {
      if( (igr_l2->first).substr(0,5) == "RATE:") {
	tc->AddGroup(TC_L2, (igr_l2->first).substr(5), igr_l2->second);
      }
    }

    const confadapter_imp::TriggerMenuHelpers::Mapping & m_ef_ps = tmh.getMapping("ef_ps");
    confadapter_imp::TriggerMenuHelpers::Mapping::const_iterator i_ef_ps;
    for(i_ef_ps = m_ef_ps.begin(); i_ef_ps != m_ef_ps.end(); i_ef_ps++) {
      if(i_ef_ps->second.size() > 0){
	std::stringstream ss(i_ef_ps->second[0]);
	int ipre;
	ss >> ipre;
	tc->SetPrescale(TC_EF, i_ef_ps->first, ipre );
      }
    }

    const confadapter_imp::TriggerMenuHelpers::Mapping & m_l2_ps = tmh.getMapping("l2_ps");
    confadapter_imp::TriggerMenuHelpers::Mapping::const_iterator i_l2_ps;
    for(i_l2_ps = m_l2_ps.begin(); i_l2_ps != m_l2_ps.end(); i_l2_ps++) {
      if(i_l2_ps->second.size() > 0){
	std::stringstream ss(i_l2_ps->second[0]);
	int ipre;
	ss >> ipre;
	tc->SetPrescale(TC_L2, i_l2_ps->first, ipre );
      }
    }

  }

  bool wtools::ZeroTAVforDisabledL1Trigs() {

    TimePoint_IS &L1_tp = TPointInfo::GetL1Info().m_timePoint;
    
    if(L1_tp.TimeStamp.year() < 2000)      
      return false;
    
    for( std::vector<std::string>::iterator ix = L1_tp.XLabels.begin(); ix != L1_tp.XLabels.end(); ix++) {
      float val;
      if (!L1_tp.get(*ix, "PS", val)) {
	std::cout << "wtools::ZeroTAVforDisabledTrigs this is really screwy" << std::endl;
	continue;
      }
      if(val < -0.5) {
	L1_tp.set(*ix, "TAV", 0.);
      }
    }

    return true;
  }

   
  bool wtools::SetL1Prescales(TrigConf *tc)
  {
    TimePoint_IS &L1_tp = TPointInfo::GetL1Info().m_timePoint;
    
    if(L1_tp.TimeStamp.year() < 2000)      
      return false;
    
    for( std::vector<std::string>::iterator ix = L1_tp.XLabels.begin(); ix != L1_tp.XLabels.end(); ix++) {
      float val;
      if (!L1_tp.get(*ix, "PS", val)) {
	std::cout << "wtools::SetL1Prescales this is really screwy" << std::endl;
	continue;
      }
      int prescale = static_cast<int>(val);
      tc->SetPrescale(TC_L1, *ix, prescale);
    }
    return true;
  }

  
  void wtools::OutputWMIPage(daq::wmi::OutputStream* outStream, const PageData *pageData)
  {
    
    const std::string &fileName = pageData->GetOutputFileName();
    if(fileName == "") {
      std::cout << "OutputWMIPage:: No file name" << std::endl;
      return;
    }
    
    
    Table *globalRatesTable = wtools::FillHTML_Table(pageData->GetGlobalRatesTable());
    
    Table *HTML_table = wtools::FillHTML_Table(pageData);
    bool isSortable = pageData->GetTable()->IsSortable();
    unsigned int tableID = pageData->GetTable()->GetTableNum();
    
    const std::vector<std::string> &POTNames = pageData->GetPOTNames();
    /*
      if( HTML_table  == 0 && POTNames.size() == 0) {
      std::cout << "OutputWMIPage:: no data table, no POTs" << std::endl;
      return;
      }
    */
    
    int file_id = outStream->open(fileName);
    
    // put in a script tag if isSortable
    if(isSortable) {
      outStream->getFileDescription(file_id)->addScriptFile( pageData->GetTable()->GetMakeWindowFileName() );
      outStream->getFileDescription(file_id)->addScriptFile( pageData->GetTable()->GetSortFileName() );
    }
    
    // put in a script tag if page contains any pots
    if(POTNames.size() != 0) {
      outStream->getFileDescription(file_id)->addScriptFile("wtrp_scripts/" + POTBase::GetMapAttacherScript() + ".js");
    }
    std::string links;
    pageData->FormatLinkLine(links);

    if(links != "") {
      Text linkLine(links);
      outStream->write(linkLine);
    }

    Text center("<center>");
    outStream->write(center);

      
    const std::string sTitle = pageData->GetPageTitle();
    Text title( "<h1>" + sTitle  + "</h1>");
    outStream->write(title);

    const std::vector<std::string> &globalPreamble = pageData->GetGlobalPreamble();
    
    for(std::vector<std::string>::const_iterator prit = globalPreamble.begin(); prit != globalPreamble.end(); prit++) {
      Text aLine(*prit); 
      outStream->write(aLine );
    }
	
    if(globalRatesTable != 0) {
      Text rateHeader("<h4>Global rates (Hz) </h4>");
      outStream->write(rateHeader);
      
      outStream->write( *globalRatesTable );
      delete globalRatesTable;
      Text lineSpace("<br>");
      outStream->write(lineSpace);
    }

    const std::vector<std::string> &localPreamble = pageData->GetLocalPreamble();
    
    for(std::vector<std::string>::const_iterator prit = localPreamble.begin(); prit != localPreamble.end(); prit++) {
      Text aLine(*prit); 
      outStream->write(aLine );
    }
    
    if(!pageData->OutputTableLast() && HTML_table != 0) {
      if(isSortable) {
	//outStream->getFileDescription(10)->addScriptFile("js/ext/tree/reorder.js")
	std::stringstream ss;
	

	ss << "<button name=\"tid_" 
	   << tableID 
	   << "\" onclick=\"" 
	   << pageData->GetTable()->GetMakeWindowFunction() 
	   << "( 'tid_" 
	   << tableID 
	   << "')\"> Get sortable copy </button>";

	Text aButton( ss.str() );
	outStream->write(aButton);
      }

      outStream->write( *HTML_table);
      delete HTML_table;
    }
    std::vector< POTBase::PlotStuff> invisiblePlots;
    if(POTNames.size() != 0) {
      for(std::vector<std::string>::const_iterator ipn = POTNames.begin(); ipn != POTNames.end(); ipn++) {
	if( !POTBase::OutputPOT(*ipn, outStream, invisiblePlots))
	  break;
	
      }
      const std::string *theImageMap = POTBase::GetDefaultImageMap();
      if(theImageMap != 0 && *theImageMap != "") {
	Text textImageMap( *theImageMap);
	outStream->write( textImageMap );
      }
    }

    if(pageData->OutputTableLast() && HTML_table != 0) {
	
      if(isSortable) {
	std::stringstream ss;
	ss << "<button onclick=\"" + pageData->GetTable()->GetMakeWindowFunction()  
	  + "( 'tid_" << tableID << "')\"> Get sortable copy </button>";
	
	Text aButton( ss.str() );
	outStream->write(aButton);
      }
      
      outStream->write( *HTML_table);
      delete HTML_table;
    }

    Text endCenter("</center>");
    outStream->write(endCenter);
    
    std::vector<std::string> localPostamble = pageData->GetLocalPostamble();
    for(std::vector<std::string>::const_iterator prit = localPostamble.begin(); 
	prit != localPostamble.end(); prit++) {
      Text aLine(*prit); 
      outStream->write(aLine );
    }
    
    outStream->close();
    const std::vector<PageData*> &potPages = pageData->GetPOTPages();
    
    // If the page has any invisible plots, output on a separate page
    if(invisiblePlots.size() != 0) {
      outStream->open(fileName + ".invisiblePlots.html");
      for(std::vector<wtrp::POTBase::PlotStuff>::const_iterator ipo = invisiblePlots.begin(); 
	  ipo != invisiblePlots.end(); ipo++) {
	
	daq::wmi::Picture pic(ipo->fileName, ipo->width, ipo->height);
	pic.setNeedCopy(true);
	outStream->write(pic);
      }
      outStream->close();
    }

    for(std::vector<PageData*>::const_iterator ipp = potPages.begin(); ipp != potPages.end(); ipp++) {
      OutputWMIPage(outStream, *ipp );
    }
    /*
      double timer1, timer2;
      POTBase::GetTimers(timer1, timer2);
      std::cout << "POT timers: " << timer1 << ", " << timer2 << std::endl;
    */
  }
  
  Table  *wtools::FillHTML_Table(const PageData *pageData)
  {
    
    const DataTable *dataTable = pageData->GetTable();
    return FillHTML_Table(dataTable);
  }
  
  Table  *wtools::FillHTML_Table(const DataTable *dataTable)
  {
    
    bool isSortable = dataTable->IsSortable();
    unsigned int tableID = dataTable->GetTableNum();
    
    if(dataTable->GetNRows() == 0)
      return 0;
    
    std::string linkCode;
    std::string refCode;
    DataTable::GetLinkCodes(linkCode, refCode);
    
    unsigned int nRows = dataTable->GetNRows(); 
    unsigned int nCols = dataTable->GetNColumns();
    
    if(nCols == 0 || nRows == 0) {
      std::cout << "FillHTML_Table: no data" << std::endl;
      return 0;
    }
    Table *HTML_table = new Table(nCols, nRows+1); // one row for header

    if (HTML_table == 0) {
      std::cout << "FillHTML_Table: unable to allocate space for Table" << std::endl;
      return 0;
    }
    
    HTML_table->setBorder(1);
    HTML_table->setBGColor( Color(247, 252, 252) );
    
    // Put in the header
    bool firstCell = true;
    for(unsigned int iCol = 0; iCol < nCols; iCol++) {
      std::string header="";
      dataTable->GetCombinedHeader(iCol, header);
      
      std::string cellText = "";
      // insert the table id as a parameter in the first cell of sortable tables
      if(isSortable && firstCell) {
	std::stringstream ss;
	ss << "<param name=\"wtrp_id\" value=\"tid_" << tableID << "\">";
	firstCell = false;
	cellText = ss.str();
      }
      // mark any unsortable columns
      if(isSortable && !dataTable->IsColumnSortable(iCol) ) {
	cellText += "<!--wtrp_nosort-->";
      }
      cellText += header;
      HTML_table->addElement(iCol+1, 1, new Text(cellText));
    }

    // Now the data
    bool isReversed = dataTable->IsReversed();
    
    const std::vector<Row> *rows = dataTable->GetRows();
    
    for(unsigned int jRow = 0; jRow < nRows; jRow++) {
      int iRow = (isReversed)? nRows - jRow - 1: jRow;
      
      for(unsigned int iCol = 0; iCol < nCols; iCol++) {
	std::string contents;
	(*rows)[iRow].GetHTMLContents(iCol, contents);
	
	
	if( contents.substr(0,barCode.size()) == barCode) {
	  std::stringstream ss(contents.substr(barCode.size()) );
	  int barLength;
	  ss >> barLength;
	  Picture *bar = new Picture(barFile, barLength, barHeight);
	  HTML_table->addElement(iCol+1, jRow+2, bar);
	} else if( contents.substr(0, linkCode.size()) == linkCode) {
	  size_t refPos = contents.find(refCode);
	  std::string text = contents.substr(linkCode.size(), refPos - linkCode.size() );
	  std::string url = contents.substr(refPos + refCode.size());
	  HTML_table->addElement(iCol+1, jRow+2, new Link(url, text));
	} else {
	  HTML_table->addElement(iCol+1, jRow+2, new Text(contents));
	}
      }
    }
    return HTML_table;
  }

  void wtools::ExtractStreamNames(const TimePoint_IS &tp, std::set<std::string> &streamNames)
  {
    std::vector<std::string>::const_iterator xl;
    
    for( xl = tp.XLabels.begin(); xl != tp.XLabels.end(); xl++)
      if( xl->substr(0,4) == "str_") {
	streamNames.insert(xl->substr(4));
      }
    
  }

  void wtools::ExtractGroupNames(const TimePoint_IS &tp, std::set<std::string> &groupNames)
  {
    std::vector<std::string>::const_iterator xl;
    
    for( xl = tp.XLabels.begin(); xl != tp.XLabels.end(); xl++)
      if( xl->substr(0,4) == "grp_") {
	groupNames.insert(xl->substr(4));
      }
  }

  void wtools::FillGlobalRatesTable( bool isRun2 )
  { //fgrt start
    if(isRun2)
      FillGlobalRatesTable_Run2();
    else
      FillGlobalRatesTable_Run1();
  }

  void wtools::FillGlobalRatesTable_Run1()
  {
    DataTable *ratesTable = PageData::GetGlobalRatesTable();
    
    Row *TRP_Row = ratesTable->GetRow("rates", "source", "TRP");
    Row *RCTL_Row = ratesTable->GetRow("rates", "source", "RCTL");
    if(TRP_Row == 0 && RCTL_Row == 0) {
      std::cout << "FillGlobalRatesTable -- TRP_Row and or RCTL_Row don't exist" << std::endl;
      return;
    }
    
    std::string rctl_L2in = "U", rctl_EFin = "U", rctl_EFout = "U";
    double dval;
    
    RCTL_Row->UpdateCell("rates", "CTPin", "--" );
    
    if( RCtlStats::GetVar("L2PU", "LVL2IntervalRate", dval) ) {
      std::stringstream ss;
      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << dval;
      rctl_L2in = ss.str();
    }
    RCTL_Row->UpdateCell("rates", "L2in", rctl_L2in );
    
    if( RCtlStats::GetVar("SFI",  "ActualEoERate", dval) ) {
      std::stringstream ss;
      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << dval;
      rctl_EFin = ss.str();
    }
    RCTL_Row->UpdateCell("rates", "EFin", rctl_EFin );
    
    if( RCtlStats::GetVar("SFO",  "CurrentEventSavedRate", dval) ) {
      std::stringstream ss;
      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << dval;
      rctl_EFout = ss.str();
    }
    RCTL_Row->UpdateCell("rates", "EFout", rctl_EFout );


    TPointInfo &L1_tpi    = TPointInfo::GetL1Info();
    TPointInfo &L2_tpi    = TPointInfo::GetL2Info();
    TPointInfo &EF_tpi    = TPointInfo::GetEFInfo();
    
    TimePoint_IS &L1_tp    = L1_tpi.m_timePoint;
    TimePoint_IS &L2_tp    = L2_tpi.m_timePoint;
    TimePoint_IS &EF_tp    = EF_tpi.m_timePoint;
    
    bool L1valid    = L1_tpi.IsValid();
    bool L2valid    = L2_tpi.IsValid();
    bool EFvalid    = EF_tpi.IsValid();
      
    std::string L1inTotal = "U", L2inTotal = "U", EFinTotal = "U", EFoutTotal = "U";
    
    float fval;
    if(L1valid && L1_tp.get("L1A", "TBP", fval) ) {
      std::stringstream ss;
      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << fval;
      L1inTotal = ss.str();
    }

    TRP_Row->UpdateCell("rates", "CTPin", L1inTotal );
    
    if (L2valid && L2_tp.get("total", "input", fval) ) {
      std::stringstream ss;
      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << fval;
      L2inTotal = ss.str();
    }
    TRP_Row->UpdateCell("rates", "L2in", L2inTotal );
    
    if(EFvalid && EF_tp.get("total", "input", fval) ) {
      std::stringstream ss;
      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << fval;
      EFinTotal = ss.str();
    }
    TRP_Row->UpdateCell("rates", "EFin", EFinTotal );
    
    if(EFvalid && EF_tp.get("total", "output", fval) ) {
      std::stringstream ss;
      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << fval;
      EFoutTotal = ss.str();
    }
    TRP_Row->UpdateCell("rates", "EFout", EFoutTotal );
    
  }

  void wtools::FillGlobalRatesTable_Run2()
  {
    DataTable *ratesTable = PageData::GetGlobalRatesTable();
    
    Row *TRP_Row = ratesTable->GetRow("rates", "source", "TRP");
    Row *RCTL_Row = ratesTable->GetRow("rates", "source", "RCTL");
    if(TRP_Row == 0 && RCTL_Row == 0) {
      std::cout << "FillGlobalRatesTable -- TRP_Row and or RCTL_Row don't exist" << std::endl;
      return;
    }
    
    std::string rctl_HLTin = "U", rctl_HLTout = "U";
    double dval;
    
    RCTL_Row->UpdateCell("rates", "CTPin", "--" );
    
    if( RCtlStats::GetVar("HLTPU", "HLTIntervalRate", dval) ) {
      std::stringstream ss;
      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << dval;
      rctl_HLTin = ss.str();
    }
    RCTL_Row->UpdateCell("rates", "HLTin", rctl_HLTin );
    
    if( RCtlStats::GetVar("SFO",  "CurrentEventSavedRate", dval) ) {
      std::stringstream ss;
      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << dval;
      rctl_HLTout = ss.str();
    }
    RCTL_Row->UpdateCell("rates", "HLTout", rctl_HLTout );


    TPointInfo &L1_tpi    = TPointInfo::GetL1Info();
    TPointInfo &HLT_tpi    = TPointInfo::GetHLTInfo();
    
    TimePoint_IS &L1_tp    = L1_tpi.m_timePoint;
    TimePoint_IS &HLT_tp    = HLT_tpi.m_timePoint;
    
    bool L1valid    = L1_tpi.IsValid();
    bool HLTvalid    = HLT_tpi.IsValid();
      
    std::string L1inTotal = "U", HLTinTotal = "U", HLToutTotal = "U";
    
    float fval;
    if(L1valid && L1_tp.get("L1A", "TBP", fval) ) {
      std::stringstream ss;
      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << fval;
      L1inTotal = ss.str();
    }

    TRP_Row->UpdateCell("rates", "CTPin", L1inTotal );
    
    if (HLTvalid && HLT_tp.get("total", "input", fval) ) {
      std::stringstream ss;
      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << fval;
      HLTinTotal = ss.str();
    }
    TRP_Row->UpdateCell("rates", "HLTin", HLTinTotal );
    
    if(HLTvalid && HLT_tp.get("total", "output", fval) ) {
      std::stringstream ss;
      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << fval;
      HLToutTotal = ss.str();
    }
    TRP_Row->UpdateCell("rates", "HLTout", HLToutTotal );
    
  }

  void wtools::TimeStampsToPreamble(bool isRun2)
  {
    if(isRun2)
      TimeStampsToPreamble_Run2();
    else
      TimeStampsToPreamble_Run1();
  }

  void wtools::TimeStampsToPreamble_Run1() {

    TimePoint_IS &L1_tp = TPointInfo::GetL1Info().m_timePoint;
    TimePoint_IS &L2_tp = TPointInfo::GetL2Info().m_timePoint;
    TimePoint_IS &EF_tp = TPointInfo::GetEFInfo().m_timePoint;

    std::string timeStamps = "Latest TRP time stamps -- L1: ";
    std::stringstream tss;
    
    if(L1_tp.TimeStamp.year() > 2000) {
      tss << L1_tp.TimeStamp;
      timeStamps += tss.str() + ", L2: ";
      tss.str("");
    } else {
      timeStamps += "invalid, L2: ";
    }
    
    if(L2_tp.TimeStamp.year() > 2000) {
      tss << L2_tp.TimeStamp;
      timeStamps += tss.str() + ", EF: ";
      tss.str("");
    } else {
      timeStamps += "invalid, EF: ";
    }

    if(EF_tp.TimeStamp.year() > 2000) {
      tss << EF_tp.TimeStamp;
      timeStamps += tss.str() + "<br>";
      tss.str("");
    } else {
      timeStamps += "invalid <br> ";
    }

    PageData::AddGlobalPreambleLine(timeStamps);
  }

  void wtools::TimeStampsToPreamble_Run2() {

    TimePoint_IS &L1_tp = TPointInfo::GetL1Info().m_timePoint;
    TimePoint_IS &HLT_tp = TPointInfo::GetHLTInfo().m_timePoint;

    std::string timeStamps = "Latest TRP time stamps -- L1: ";
    std::stringstream tss;
    
    if(L1_tp.TimeStamp.year() > 2000) {
      tss << L1_tp.TimeStamp;
      timeStamps += tss.str() + ", HLT: ";
      tss.str("");
    } else {
      timeStamps += "invalid, HLT: ";
    }
    
    if(HLT_tp.TimeStamp.year() > 2000) {
      tss << HLT_tp.TimeStamp;
      timeStamps += tss.str() + "<br>";
      tss.str("");
    } else {
      timeStamps += "invalid <br> ";
    }

    PageData::AddGlobalPreambleLine(timeStamps);
  }

  void wtools::FillRawPage(RawPage &rawPage, const TimePoint_IS &timePoint) {
    
    TimePoint_IS &tp = const_cast<TimePoint_IS &>(timePoint);
    
    DataTable *dataTable = rawPage.GetTable();
    if(dataTable == 0) {
      std::cout << "wtools::FillRawPage -- dataTable doesn't exist " << std::endl;
      return;
    }

      
    rawPage.ClearLocalPreamble();
    std::stringstream tss;
    tss << timePoint.TimeStamp;
    rawPage.AddLocalPreambleLine( "Time stamp: " + tss.str() + "<br>");
    
    std::ostringstream ost;
    std::vector<std::string>::const_iterator xl;
    for( xl = tp.XLabels.begin(); xl != tp.XLabels.end(); xl++) {
      
      Row *theRow = dataTable->GetRow("tp", "name", *xl);
      if(theRow == 0) {
	//std::cout << "wtools::FillRawPage -- can't find row: " << *xl << std::endl;
	continue;
      }
      std::vector<std::string>::const_iterator yl;
      for( yl = tp.YLabels.begin(); yl != tp.YLabels.end(); yl++) {
	
	float val;
	std::string sval = "U";
	if(tp.get(*xl, *yl, val)) {
	  ost.str("");
	  ost << val;
	  sval = ost.str();
	  theRow->UpdateCell("tp", *yl, sval );
	}
      }
    }
  }

  void wtools::FillStreamTable(StreamTable &streamTable, bool isRun2)
  {
    if(isRun2)
      FillStreamTable_Run2(streamTable);
    else
      FillStreamTable_Run1(streamTable);
  }

  void wtools::FillStreamTable_Run1(StreamTable &streamTable)
  {
    TimePoint_IS &L2_tp = TPointInfo::GetL2Info().m_timePoint;
    TimePoint_IS &EF_tp = TPointInfo::GetEFInfo().m_timePoint;
    
    streamTable.ClearLocalPreamble();
    
    DataTable *dataTable = streamTable.GetTable();
    
    if(dataTable == 0) {
      std::cout << "FillStreamTable: attempt to fill a non-existant stream table" << std::endl;
      return;
    }
      
    std::stringstream totRates;
    
    std::vector<Row> *rows = dataTable->GetRows();
    float highestRate = 0;
    std::vector<float> rates;
    
    for(unsigned int iRow = 0; iRow < rows->size(); iRow++) {
      const std::string *streamLevel = (*rows)[iRow].GetContents_S("str", "lvl");
      const std::string *streamName = (*rows)[iRow].GetContents_S("str", "str");
      
      std::stringstream ss;
      
      float outRate = 0;
      
      if( *streamLevel == "L2") {
	L2_tp.get("str_" + *streamName, "output", outRate);
      } else {
	EF_tp.get("str_" + *streamName, "output", outRate);
      }

      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << outRate;
      (*rows)[iRow].UpdateCell("str", "frac_out", ss.str() );
      ss.str("");
      
      rates.push_back(outRate);
      highestRate = (outRate > highestRate)? outRate : highestRate;
    }
    // now fill the bars, normalized to the highest rate
    for(unsigned int iRow = 0; iRow < rows->size(); iRow++) {
      
      std::stringstream ss;

      if(highestRate < .0000001) {
	(*rows)[iRow].UpdateCell("str", "bar_out", "&nbsp;" );
      } else {
	float fracRate = rates[iRow] / highestRate;
	ss << barCode << static_cast<int>(fracRate * maxBarPixels);
	(*rows)[iRow].UpdateCell("str", "bar_out", ss.str() );
      }
      ss.str("");
    }
  }

  void wtools::FillStreamTable_Run2(StreamTable &streamTable)
  {
    TimePoint_IS &HLT_tp = TPointInfo::GetHLTInfo().m_timePoint;
    
    streamTable.ClearLocalPreamble();
    
    DataTable *dataTable = streamTable.GetTable();
    
    if(dataTable == 0) {
      std::cout << "FillStreamTable: attempt to fill a non-existant stream table" << std::endl;
      return;
    }
      
    std::stringstream totRates;
    
    std::vector<Row> *rows = dataTable->GetRows();
    float highestRate = 0;
    std::vector<float> rates;
    
    for(unsigned int iRow = 0; iRow < rows->size(); iRow++) {
      //const std::string *streamLevel = (*rows)[iRow].GetContents_S("str", "lvl");
      const std::string *streamName = (*rows)[iRow].GetContents_S("str", "str");
      
      std::stringstream ss;
      
      float outRate = 0;
      
      HLT_tp.get("str_" + *streamName, "output", outRate);


      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << outRate;
      (*rows)[iRow].UpdateCell("str", "frac_out", ss.str() );
      ss.str("");
      
      rates.push_back(outRate);
      highestRate = (outRate > highestRate)? outRate : highestRate;
    }
    // now fill the bars, normalized to the highest rate
    for(unsigned int iRow = 0; iRow < rows->size(); iRow++) {
      
      std::stringstream ss;

      if(highestRate < .0000001) {
	(*rows)[iRow].UpdateCell("str", "bar_out", "&nbsp;" );
      } else {
	float fracRate = rates[iRow] / highestRate;
	ss << barCode << static_cast<int>(fracRate * maxBarPixels);
	(*rows)[iRow].UpdateCell("str", "bar_out", ss.str() );
      }
      ss.str("");
    }
  }
    

     
 void wtools::FillGroupTable(GroupTable &groupTable, const TrigConf *tc )
  {

    if(tc->IsRun1Configuration() )
      FillGroupTable_Run1(groupTable );
    else
      FillGroupTable_Run2(groupTable );
    
  }

  void wtools::FillGroupTable_Run1(GroupTable &groupTable ) {

    TimePoint_IS &L2_timePoint = TPointInfo::GetL2Info().m_timePoint;
    TimePoint_IS &EF_timePoint = TPointInfo::GetEFInfo().m_timePoint;

    groupTable.ClearLocalPreamble();
    
    DataTable *dataTable = groupTable.GetTable();
    
    L2_groupRates.clear();
    EF_groupRates.clear();
    
    if(dataTable == 0) {
      std::cout << "FillGroupTable: attempt to fill a non-existant group table" << std::endl;
      return;
    }
    

    float L2inTotal = 0;
    float EFinTotal = 0;
    float L2outTotal = 0;
    float EFoutTotal = 0;
    
    TimePoint_IS &L2_tp = const_cast<TimePoint_IS &>(L2_timePoint);
    TimePoint_IS &EF_tp = const_cast<TimePoint_IS &>(EF_timePoint);
    
    L2_tp.get("total", "input", L2inTotal);
    L2_tp.get("total", "output", L2outTotal);
    EF_tp.get("total", "input", EFinTotal);
    EF_tp.get("total", "output", EFoutTotal);

    {
      RateInfo ri;
      ri.in = L2inTotal;
      ri.out = L2outTotal;
      L2_groupRates[ChainTable::GetAllChainsName()] = ri;
      
      ri.in = EFinTotal;
      ri.out = EFoutTotal;
      EF_groupRates[ChainTable::GetAllChainsName()] = ri;
    }
    
    std::string L2potFileName = "", EFpotFileName = "";
    PageData::GetOutputFileName("L2out", L2potFileName);
    PageData::GetOutputFileName("EFout", EFpotFileName);
    
    //std::cout << "totals L2in, L2out, EFin, EFout: " << L2inTotal << ", "
    //		<< L2outTotal << ", "<< EFinTotal << ", "<< EFoutTotal << std::endl;
    
    std::vector<Row> *rows = dataTable->GetRows();
    std::string pref = "grp_";
    for(unsigned int iRow = 0; iRow < rows->size(); iRow++) {
      const std::string *groupName = (*rows)[iRow].GetContents_S("grp", "grp");
      
      std::stringstream ss;	  
      float fracRate = 0.;
      
      float L2inRate = 0, L2outRate = 0;
      
      if(*groupName == ChainTable::GetAllChainsName()){
	L2inRate = L2inTotal;
	L2outRate = L2outTotal;
      } else {
	L2_tp.get("grp_" + *groupName, "input", L2inRate);
	L2_tp.get("grp_" + *groupName, "output", L2outRate);
      }

      RateInfo ri;
      ri.in = L2inRate;
      ri.out = L2outRate;
      L2_groupRates[*groupName] = ri;
      
      fracRate = (L2inTotal > .00001)? L2inRate/L2inTotal : 0.;
      ss.str("");
      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << L2inRate;
      (*rows)[iRow].UpdateCell("L2", "frac_in", ss.str() );
      
      if(fracRate < fracLim) {
	ss.str("");
	ss << barCode << static_cast<int>(fracRate * maxBarPixels);
	(*rows)[iRow].UpdateCell("L2", "bar_in", ss.str() );
      } else
	(*rows)[iRow].UpdateCell("L2", "bar_in", "> 1" );
	
      fracRate = (L2outTotal > .00001)? L2outRate/L2outTotal : 0.;
	  
      ss.str("");
      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << L2outRate;
      std::string valL2String = ss.str();
      if(L2potFileName != "")
	valL2String = "<a href = \"" + L2potFileName + "#" + *groupName + "\">" + valL2String + "</a>";
      (*rows)[iRow].UpdateCell("L2", "frac_out", valL2String );
      
      ss.str("");
      if(fracRate < fracLim) {
	ss << barCode << static_cast<int>(fracRate * maxBarPixels);
	(*rows)[iRow].UpdateCell("L2", "bar_out", ss.str() );
      } else
	(*rows)[iRow].UpdateCell("L2", "bar_out", "> 1" );
      
      
      float EFinRate = 0, EFoutRate = 0;
      
      if(*groupName == ChainTable::GetAllChainsName()){
	EFinRate = EFinTotal;
	EFoutRate = EFoutTotal;
      } else {
	EF_tp.get("grp_" + *groupName, "input", EFinRate);
	EF_tp.get("grp_" + *groupName, "output", EFoutRate);
      }



      ri.in = EFinRate;
      ri.out = EFoutRate;
      EF_groupRates[*groupName] = ri;
      
      fracRate = (EFinTotal > .00001)? EFinRate/EFinTotal : 0.;
      
      ss.str("");
      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << EFinRate;
      (*rows)[iRow].UpdateCell("EF", "frac_in", ss.str() );
      
      ss.str("");
      if(fracRate < fracLim) {
	ss << barCode << static_cast<int>(fracRate * maxBarPixels);
	(*rows)[iRow].UpdateCell("EF", "bar_in", ss.str() );
      } else
	(*rows)[iRow].UpdateCell("EF", "bar_in", "> 1" );
      
      fracRate = (EFoutTotal > .00001)? EFoutRate/EFoutTotal : 0.;

      ss.str("");
      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << EFoutRate;
      std::string valEFString = ss.str();
      if(EFpotFileName != "")
	valEFString = "<a href = \"" + EFpotFileName + "#" + *groupName + "\">" + valEFString + "</a>";
      (*rows)[iRow].UpdateCell("EF", "frac_out", valEFString );
      
      ss.str("");
      if(fracRate < fracLim) {
	ss << barCode << static_cast<int>(fracRate * maxBarPixels);
	(*rows)[iRow].UpdateCell("EF", "bar_out", ss.str() );
      } else
	(*rows)[iRow].UpdateCell("EF", "bar_out", "> 1" );
    }
    //groupTable.Dump();
  }

  void wtools::FillGroupTable_Run2(GroupTable &groupTable ) {

    TimePoint_IS &HLT_timePoint = TPointInfo::GetHLTInfo().m_timePoint;

    groupTable.ClearLocalPreamble();
    
    DataTable *dataTable = groupTable.GetTable();
    
    HLT_groupRates.clear();
    
    if(dataTable == 0) {
      std::cout << "FillGroupTable: attempt to fill a non-existant group table" << std::endl;
      return;
    }
    

    float HLTinTotal = 0;
    float HLToutTotal = 0;
    
    TimePoint_IS &HLT_tp = const_cast<TimePoint_IS &>(HLT_timePoint);
    
    HLT_tp.get("total", "input", HLTinTotal);
    HLT_tp.get("total", "output", HLToutTotal);

    {
      RateInfo ri;
      ri.in = HLTinTotal;
      ri.out = HLToutTotal;
      HLT_groupRates[ChainTable::GetAllChainsName()] = ri;
    }
    
    std::string HLTpotFileName = "";
    PageData::GetOutputFileName("HLTout", HLTpotFileName);
    
    //std::cout << "totals HLTin, HLTout: " << HLTinTotal << ", "
    //		<< HLToutTotal << std::endl;
    
    std::vector<Row> *rows = dataTable->GetRows();
    std::string pref = "grp_";
    for(unsigned int iRow = 0; iRow < rows->size(); iRow++) {
      const std::string *groupName = (*rows)[iRow].GetContents_S("grp", "grp");
      
      std::stringstream ss;	  
      float fracRate = 0.;
      
      float HLTinRate = 0, HLToutRate = 0;
      
      if(*groupName == ChainTable::GetAllChainsName()){
	HLTinRate = HLTinTotal;
	HLToutRate = HLToutTotal;
      } else {
	HLT_tp.get("grp_" + *groupName, "input", HLTinRate);
	HLT_tp.get("grp_" + *groupName, "output", HLToutRate);
      }

      RateInfo ri;
      ri.in = HLTinRate;
      ri.out = HLToutRate;
      HLT_groupRates[*groupName] = ri;
      

      fracRate = (HLTinTotal > .00001)? HLTinRate/HLTinTotal : 0.;
      ss.str("");
      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << HLTinRate;
      (*rows)[iRow].UpdateCell("HLT", "frac_in", ss.str() );
      
      if(fracRate < fracLim) {
	ss.str("");
	ss << barCode << static_cast<int>(fracRate * maxBarPixels);
	(*rows)[iRow].UpdateCell("HLT", "bar_in", ss.str() );
      } else
	(*rows)[iRow].UpdateCell("HLT", "bar_in", "> 1" );
	
      fracRate = (HLToutTotal > .00001)? HLToutRate/HLToutTotal : 0.;
	  
      ss.str("");
      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << HLToutRate;
      std::string valHLTString = ss.str();
      if(HLTpotFileName != "")
	valHLTString = "<a href = \"" + HLTpotFileName + "#" + *groupName + "\">" + valHLTString + "</a>";
      (*rows)[iRow].UpdateCell("HLT", "frac_out", valHLTString );
      
      ss.str("");
      if(fracRate < fracLim) {
	ss << barCode << static_cast<int>(fracRate * maxBarPixels);
	(*rows)[iRow].UpdateCell("HLT", "bar_out", ss.str() );
      } else
	(*rows)[iRow].UpdateCell("HLT", "bar_out", "> 1" );
      
    }
    //groupTable.Dump();
  }

  void wtools::FillL1Table(L1Table &l1Table) {
    
    TPointInfo &L1_tpi    = TPointInfo::GetL1Info();
    
    if( !L1_tpi.IsValid() )
      return;

    TimePoint_IS &L1_tp  = L1_tpi.m_timePoint;

    /*
      The keys are no longer in the L1 timepoint metadata. Since
      key changes are not synchronized with timepoint generation,
      it is possible to set up the L1 table from a timepoint made
      with a previous key. So we reset it each time.
    */

    l1Table.MakeTable();
    FixupL1ConfigFromTP(l1Table, L1_tp);
    
    float L1_totaloutput = 0.;
    L1_tp.get("L1A", "TAV", L1_totaloutput);

    DataTable *dataTable = l1Table.GetTable();
    std::vector<Row> *rows = dataTable->GetRows();

    std::string L1FileName = "";
    PageData::GetOutputFileName("L1", L1FileName);

    std::stringstream ss;	  

    for(std::vector<Row>::iterator ir = rows->begin(); ir != rows->end(); ir++){ 
      const std::string *name = ir->GetContents_S("l1", "name");
      float val;
      if( !L1_tp.get(*name, "PS", val) ) {
	//std::cout << "wtools::FillL1Table -- Couldn't find timePoint entry for " << *name << std::endl;
	ir->UpdateCell("l1", "presc", "U" );
	ir->UpdateCell("l1", "TBP", "U" );
	ir->UpdateCell("l1", "TAP", "U" );
	ir->UpdateCell("l1", "TAV", "U" );
	ir->UpdateCell("l1", "bar_out", "&nbsp;" );
	continue;
      }
	
      ss.str("");
      ss << std::setprecision(precision) << val;
      ir->UpdateCell("l1", "presc", ss.str() );

      float tbp, tap, tav;
      L1_tp.get(*name, "TBP", tbp);
      L1_tp.get(*name, "TAP", tap);
      L1_tp.get(*name, "TAV", tav);
      
      ss.str("");
      ss << std::setprecision(precision) << tbp;
      ir->UpdateCell("l1", "TBP", ss.str() );
      
      ss.str("");
      ss << std::setprecision(precision) << tap;
      ir->UpdateCell("l1", "TAP", ss.str() );
      
      ss.str("");
      ss << std::setprecision(precision) << tav;
      std::string valString = ss.str();
      valString = "<a href = \"" + L1FileName + "#" + *name + "\">" + valString + "</a>";
      ir->UpdateCell("l1", "TAV", valString );
      
      if(L1_totaloutput > .000001) {
	ss.str("");
	float tmp = tav / L1_totaloutput;
	if(tmp < fracLim) {
	  ss << barCode << static_cast<int> (tmp * maxBarPixels);
	  ir->UpdateCell("l1", "bar_out", ss.str() );
	} else
	  ir->UpdateCell("l1", "bar_out", "> 1" );
	
      } else
	ir->UpdateCell("l1", "bar_out", "&nbsp;" );
    }
  }

  void wtools::FillStatusPage(StatusPage &statusPage)
  {
    bool rc_L1 = TPointInfo::GetL1Info().IsValid();
    bool rc_L2 = TPointInfo::GetL2Info().IsValid();
    bool rc_EF = TPointInfo::GetEFInfo().IsValid();
    
    TimePoint_IS *L1_tp = &TPointInfo::GetL1Info().m_timePoint;
    TimePoint_IS *L2_tp = &TPointInfo::GetL2Info().m_timePoint;
    TimePoint_IS *EF_tp = &TPointInfo::GetEFInfo().m_timePoint;

    const std::vector<std::string> &vars = statusPage.GetVars();
    
    DataTable *dataTable = statusPage.GetTable();
    
    for(int i = 0; i < 3; i++) {
      
      TimePoint_IS *tp = L1_tp;
      std::string colName = "L1";
      bool isValid = rc_L1;
      
      switch (i) {
      case 0:
	break;
	
      case 1:
	tp = L2_tp;
	colName = "L2";
	isValid = rc_L2;
	break;
	
      case 2:
	tp = EF_tp;
	colName = "EF";
	isValid = rc_EF;
	break;
	  
      default:
	colName = "oops";
	isValid = false;
	break;
      }
	
      if(!isValid)
	continue;

      for(unsigned int var = (unsigned int) 0; var < vars.size(); var++) {
	std::ostringstream ost;
	  
	Row *theRow = dataTable->GetRow("var", "name", vars[var]);

	if(theRow == 0) {
	  std::cout << "FillStatus page, no row named : " << vars[var] << std::endl;
	  break;
	}
	  
	switch (var) {
	case 0:
	  ost << tp->RunNumber;
	  break;
	    
	case 1:
	  ost << tp->TimeStamp;
	  break;
	    
	case 2:
	  ost << tp->LumiBlock;
	  break;
	    
	case 3:
	  if(tp->MetaData.size() == 3)
	    ost << tp->MetaData[0];
	  else
	    ost << "?";
	  break;
	    
	case 4:
	  if(tp->MetaData.size() == 3)
	    ost << tp->MetaData[1];
	  else
	    ost << "?";
	  break;
	    
	case 5:
	  if(tp->MetaData.size() == 3)
	    ost << tp->MetaData[2];
	  else
	    ost << "?";
	  break;
	  
	case 6:
	  ost << tp->YLabels.size();
	  break;
	  
	case 7:
	  ost << tp->XLabels.size();
	  break;
	}
	  
	//std::cout << "updating " << colName << ", " << vars[var] << ", " << ost.str() << std::endl;
	theRow->UpdateCell("var", colName, ost.str() );
	ost.str("");
      }
    }
  }
    

  void wtools::FillChainTables(std::map<std::string, ChainTable> &chainTables, 
			       const TrigConf *trigConfig)
  {

    if( trigConfig->IsRun1Configuration() )
      FillChainTables_Run1(chainTables, trigConfig);
    else
      FillChainTables_Run2(chainTables, trigConfig);
  }


  void wtools::FillChainTables_Run1(std::map<std::string, ChainTable> &chainTables, 
			       const TrigConf *trigConfig) 
  {
    TimePoint_IS &L1_timePoint = TPointInfo::GetL1Info().m_timePoint;
    TimePoint_IS &L2_timePoint = TPointInfo::GetL2Info().m_timePoint;
    TimePoint_IS &EF_timePoint = TPointInfo::GetEFInfo().m_timePoint;

    /*
      If periodicAction needs to be sped up, this is the place to look first. 
      A mapping from chain name to the cells which need updating would help a lot.
    */
      
    const std::map<std::string, std::set<std::string> > &L2_chToGrp = trigConfig->GetChToGrpMap(TC_L2);
    const std::map<std::string, std::set<std::string> > &EF_chToGrp = trigConfig->GetChToGrpMap(TC_EF);
    
    std::string L2potFileName = "", EFpotFileName = "";
    PageData::GetOutputFileName("L2redux", L2potFileName);
    PageData::GetOutputFileName("EFredux", EFpotFileName);
    
    float L1_totaloutput = 0.;
    L1_timePoint.get("L1A", "TAV", L1_totaloutput);
    
    // loop over the chain table pages
    for( std::map<std::string, ChainTable>::iterator chit = chainTables.begin();
	 chit != chainTables.end(); chit++) { //chaintable loop
      chit->second.ClearLocalPreamble();
      
      // Fill in title and group rate info -- L2 then EF
      std::string rateString = "<h4> HLT rates (this group) --";
      const int ratePrecision = 2;
      
      std::map<std::string, RateInfo>::const_iterator rL2 = L2_groupRates.find(chit->first);
      if(rL2 != L2_groupRates.end()) {
	float inRate = rL2->second.in;
	float outRate = rL2->second.out;
	
	std::stringstream totRates;
	totRates << "L2 input: " << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci)
		 << inRate <<  ",&nbsp;L2 output: " << outRate;
	rateString += totRates.str();
      } else {
	rateString += "L2 group rate not found";
      }

      std::map<std::string, RateInfo>::const_iterator rEF = EF_groupRates.find(chit->first);
      if(rEF != EF_groupRates.end()) {
	float inRate = rEF->second.in;
	float outRate = rEF->second.out;
	
	std::stringstream totRates;
	totRates << "&nbsp; EF input: " << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) 
		 << inRate <<  ",&nbsp; EF output: " << outRate;
	rateString += totRates.str();
      } else {
	rateString += "EF group rate not found";
      }
      chit->second.AddLocalPreambleLine(rateString + "</h4>");

	
    }
      
    for(int i = 0; i < 2; i++) { // Loop over L2 and EF
      
      TimePoint_IS &timePoint =                                      (i == 0)? L2_timePoint  : EF_timePoint;
      const std::map<std::string, RateInfo> &groupRates  =           (i == 0)? L2_groupRates : EF_groupRates;
      const std::map<std::string, std::set<std::string> > &chToGrp = (i == 0)? L2_chToGrp    : EF_chToGrp;
      std::string colGroupName =                                     (i == 0)? "L2"          : "EF";
      const std::string &potFileName =                               (i == 0)? L2potFileName : EFpotFileName;

      const std::string grpPref = "grp_";
      const std::string strPref = "str_";
      const std::string totPref = "total";
      const std::string intPref = "interval";
      const std::string timPref = "time";
      const std::string nulDef  = "xxx";

      
      // Loop over the time point entries
      std::vector<std::string>::const_iterator xl;
      for( xl = timePoint.XLabels.begin(); xl != timePoint.XLabels.end(); xl++) {
	
	if(xl->substr(0,grpPref.size()) == grpPref ||
	   xl->substr(0,strPref.size()) == strPref ||
	   xl->substr(0,totPref.size()) == totPref ||
	   xl->substr(0,intPref.size()) == intPref ||
	   xl->substr(0,timPref.size()) == timPref ||
	   xl->substr(0,timPref.size()) == nulDef
	   )
	  continue;
	  
	const std::string chainName = *xl;
	
	float inRate = 0., outRate = 0., preRate = 0., algoRate = -1, rawRate = 0.;
	timePoint.get(*xl, "input",    inRate);
	timePoint.get(*xl, "output",   outRate);

	timePoint.get(*xl, "prescaled", preRate);
	// 13.04.12 -- the name will change in the near future but for backward compatibility...
	if( (preRate > -.0000001) && (preRate < .0000001)) timePoint.get(*xl, "prescale", preRate);

	timePoint.get(*xl, "raw",      rawRate);
	if( std::find(timePoint.XLabels.begin(), timePoint.XLabels.end(), "algoIn") != timePoint.XLabels.end())
	  timePoint.get(*xl, "algoIn",      algoRate);

	//find the groups to which this chain belongs
	const std::map<std::string, std::set<std::string> >::const_iterator cgit = chToGrp.find(chainName);
	
	if(  cgit == chToGrp.end() ) {
	  if(RunInfo::SMKChanged() )
	    std::cout << "No chain to group table found for: " << chainName << std::endl;
	  continue;
	}
	
	// loop over the groups to which this chain belongs
	std::set<std::string>::const_iterator grit;
	bool allChainsTablesFilled = false;
	for(grit = cgit->second.begin(); !allChainsTablesFilled; grit++) {
	  
	  // is this a real group or the pseudo group: AllChains?
	  const std::string &groupName = (grit != cgit->second.end())? *grit : ChainTable::GetAllChainsName();
	  allChainsTablesFilled = ( (grit == cgit->second.end()));

	  // get the group rates
	  std::map<std::string, RateInfo>::const_iterator ri = groupRates.find(groupName);
	  if(  ri == groupRates.end()) {
	    std::cout << "No group rates found for group: " << groupName << std::endl;
	    continue;
	  }
	  float grInRate = ri->second.in;
	  float grOutRate = ri->second.out;
	  
	  // get the chain table corresponding to this group
	  std::map<std::string, ChainTable>::iterator chit = chainTables.find(groupName);
	  if( chit == chainTables.end()) {
	    std::cout << "No chain table found for: " << groupName << std::endl;
	    continue;
	  }
	  
	  DataTable *dt = chit->second.GetTable();
	  
	  std::vector<Row *> theRows;
	  unsigned int nFound = dt->GetRows(colGroupName, "name", chainName,  theRows);
	  
	  if(nFound == 0) {
	    std::cout << "No row found in chain table for: " << colGroupName << ":" << chainName << std::endl;
	    continue;
	  }
	  
	  for( std::vector<Row *>::iterator rit = theRows.begin(); rit != theRows.end(); rit++) {
	    Row *theRow = *rit;
	    /*
	      std::cout << "grInRate, grOutRate, inRate, rawRate: " << grInRate << ", " << grOutRate << ", "
	      << inRate << ", " << rawRate << std::endl;
	    */
	      
	    float fracIn  = ( grInRate   > .0000001)? inRate/grInRate : 0.;
	    float fracOut = ( grOutRate  > .0000001)? outRate/grOutRate : 0.;
	    float fracAcc = ( preRate    > .0000001)? rawRate/preRate : 0.;
	    if(algoRate > .0000001) fracAcc = rawRate/algoRate;
	    
	    std::stringstream ss;
	    ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << inRate;
	    theRow->UpdateCell(colGroupName, "frac_in", ss.str() );
	    ss.str("");
	    
	    ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << outRate;
	    theRow->UpdateCell(colGroupName, "frac_out", ss.str() );
	    ss.str("");
	      
	    ss << std::setprecision(precision) << std::setiosflags(std::ios_base::fixed) << fracAcc;
	    std::string valString = ss.str();
	    if(potFileName != "")
	      valString = "<a href = \"" + potFileName + "#" + *xl + "\">" + valString + "</a>";
	    theRow->UpdateCell(colGroupName, "frac_raw", valString );
	    ss.str("");
	      
	    if(fracIn < fracLim) {
	      ss << barCode << static_cast<int>(fracIn * maxBarPixels);
	      theRow->UpdateCell(colGroupName, "bar_in", ss.str() );
	      ss.str("");
	    } else
	      theRow->UpdateCell(colGroupName, "bar_in", "> 1" );
		
	    if(fracOut < fracLim) {
	      ss << barCode << static_cast<int>(fracOut * maxBarPixels);
	      theRow->UpdateCell(colGroupName, "bar_out", ss.str() );
	      ss.str("");
	    } else
	      theRow->UpdateCell(colGroupName, "bar_out", "> 1" );
	      
	    if(i == 0) { // if L2, fill in the L1 info
	      const std::string *L1Name = theRow->GetContents_S("L1", "name");
	      float L1_rate=0., L1_ps=0.;
	      TimePoint_IS &L1_tp = const_cast<TimePoint_IS &>(L1_timePoint);
	      L1_tp.get(*L1Name, "TAV", L1_rate);
	      float fracL1 = (L1_totaloutput > .00001)? L1_rate/L1_totaloutput : 0.;
		
	      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << L1_rate;
	      theRow->UpdateCell("L1", "frac_out", ss.str() );
	      ss.str("");
	      
	      if(fracL1 < fracLim) {
		ss << barCode << static_cast<int>(fracL1 * maxBarPixels);
		theRow->UpdateCell("L1", "bar_out", ss.str() );
		ss.str("");
	      } else 
		theRow->UpdateCell("L1", "bar_out", "> 1" );

	      L1_tp.get(*L1Name, "PS", L1_ps);
	      ss << std::setprecision(precision) << std::setiosflags(std::ios_base::fixed) << L1_ps;
	      theRow->UpdateCell("L1", "presc", ss.str() );
	      ss.str("");
	    }
	  }
	}
      }
    }
  }

  void wtools::FillOneChainTable_Run2(const std::string &groupName,
				      const std::string &potFileName,
				      std::vector<std::string>::const_iterator &xl,
				      const std::map<std::string, RateInfo> &groupRates,
				      float inRate, float outRate, float preRate, 
				      float algoRate, float rawRate,
				      std::map<std::string, ChainTable> &chainTables)
  {
    std::string colGroupName = "HLT";
    const std::string chainName = *xl;
    TimePoint_IS &L1_timePoint = TPointInfo::GetL1Info().m_timePoint;
    float L1_totaloutput = 0.;
    L1_timePoint.get("L1A", "TAV", L1_totaloutput);

    // get the group rates
    std::map<std::string, RateInfo>::const_iterator ri = groupRates.find(groupName);
    if(  ri == groupRates.end()) {
      if(RunInfo::SMKChanged() )
	std::cout << "No group rates found for chain: " << groupName << std::endl;
      return;
    }
    float grInRate = ri->second.in;
    float grOutRate = ri->second.out;
    
    // get the chain table corresponding to this group
    std::map<std::string, ChainTable>::iterator chit = chainTables.find(groupName);
    if( chit == chainTables.end()) {
      if(RunInfo::SMKChanged() )
	std::cout << "No chain table found for: " << groupName << std::endl;
      return;
    }
    
    DataTable *dt = chit->second.GetTable();
    
    std::vector<Row *> theRows;
    unsigned int nFound = dt->GetRows(colGroupName, "name", chainName,  theRows);
    
    if(nFound == 0) {
      if(RunInfo::SMKChanged() )
	std::cout << "No row found in chain table for: " << colGroupName << ":" << chainName << std::endl;
      return;
    }
    
    for( std::vector<Row *>::iterator rit = theRows.begin(); rit != theRows.end(); rit++) {
      Row *theRow = *rit;
      /*
	std::cout << "grInRate, grOutRate, inRate, rawRate: " << grInRate << ", " << grOutRate << ", "
	<< inRate << ", " << rawRate << std::endl;
      */
      
      float fracIn  = ( grInRate   > .0000001)? inRate/grInRate : 0.;
      float fracOut = ( grOutRate  > .0000001)? outRate/grOutRate : 0.;
      float fracAcc = ( preRate    > .0000001)? rawRate/preRate : 0.;
      if(algoRate > .0000001) fracAcc = rawRate/algoRate;
      
      std::stringstream ss;
      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << inRate;
      theRow->UpdateCell(colGroupName, "frac_in", ss.str() );
      ss.str("");
      
      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << outRate;
      theRow->UpdateCell(colGroupName, "frac_out", ss.str() );
      ss.str("");
      
      ss << std::setprecision(precision) << std::setiosflags(std::ios_base::fixed) << fracAcc;
      std::string valString = ss.str();
      if(potFileName != "")
	valString = "<a href = \"" + potFileName + "#" + *xl + "\">" + valString + "</a>";
      theRow->UpdateCell(colGroupName, "frac_raw", valString );
      ss.str("");
      
      if(fracIn < fracLim) {
	ss << barCode << static_cast<int>(fracIn * maxBarPixels);
	theRow->UpdateCell(colGroupName, "bar_in", ss.str() );
	ss.str("");
      } else
	theRow->UpdateCell(colGroupName, "bar_in", "> 1" );
      
      if(fracOut < fracLim) {
	ss << barCode << static_cast<int>(fracOut * maxBarPixels);
	theRow->UpdateCell(colGroupName, "bar_out", ss.str() );
	ss.str("");
      } else
	theRow->UpdateCell(colGroupName, "bar_out", "> 1" );
      
      const std::string *L1Name = theRow->GetContents_S("L1", "name");
      float L1_rate=0., L1_ps=0.;
      TimePoint_IS &L1_tp = const_cast<TimePoint_IS &>(L1_timePoint);
      L1_tp.get(*L1Name, "TAV", L1_rate);
      float fracL1 = (L1_totaloutput > .00001)? L1_rate/L1_totaloutput : 0.;
      
      ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << L1_rate;
      theRow->UpdateCell("L1", "frac_out", ss.str() );
      ss.str("");
      
      if(fracL1 < fracLim) {
	ss << barCode << static_cast<int>(fracL1 * maxBarPixels);
	theRow->UpdateCell("L1", "bar_out", ss.str() );
	ss.str("");
      } else 
	theRow->UpdateCell("L1", "bar_out", "> 1" );
      
      L1_tp.get(*L1Name, "PS", L1_ps);
      ss << std::setprecision(precision) << std::setiosflags(std::ios_base::fixed) << L1_ps;
      theRow->UpdateCell("L1", "presc", ss.str() );
      ss.str("");
    }
  }

  void wtools::FillChainTables_Run2(std::map<std::string, ChainTable> &chainTables, 
			       const TrigConf *trigConfig)  {

    TimePoint_IS &L1_timePoint = TPointInfo::GetL1Info().m_timePoint;
    TimePoint_IS &HLT_timePoint = TPointInfo::GetHLTInfo().m_timePoint;

    /*
      If periodicAction needs to be sped up, this is the place to look first. 
      A mapping from chain name to the cells which need updating would help a lot.
    */
      
    const std::map<std::string, std::set<std::string> > &HLT_chToGrp = trigConfig->GetChToGrpMap(TC_HLT);
    
    std::string HLTpotFileName = "";
    PageData::GetOutputFileName("HLTredux", HLTpotFileName);
    
    float L1_totaloutput = 0.;
    L1_timePoint.get("L1A", "TAV", L1_totaloutput);
    
    // loop over the chain table pages
    for( std::map<std::string, ChainTable>::iterator chit = chainTables.begin();
	 chit != chainTables.end(); chit++) { //chaintable loop
      chit->second.ClearLocalPreamble();
      
      // Fill in title and group rate info 
      std::string rateString = "<h4> HLT rates (this group) --";
      const int ratePrecision = 2;
      

      std::map<std::string, RateInfo>::const_iterator rHLT = HLT_groupRates.find(chit->first);
      if(rHLT != HLT_groupRates.end()) {
	float inRate = rHLT->second.in;
	float outRate = rHLT->second.out;
	
	std::stringstream totRates;
	totRates << "HLT input: " << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci)
		 << inRate <<  ",&nbsp;L2 output: " << outRate;
	rateString += totRates.str();
      } else {
	rateString += "HLT group rate not found";
      }


      chit->second.AddLocalPreambleLine(rateString + "</h4>");

	
    }
      
    TimePoint_IS &timePoint = HLT_timePoint;
    const std::map<std::string, RateInfo> &groupRates  = HLT_groupRates;
    const std::map<std::string, std::set<std::string> > &chToGrp = HLT_chToGrp;
    std::string colGroupName = "HLT";
    const std::string &potFileName = HLTpotFileName;

    const std::string grpPref = "grp_";
    const std::string strPref = "str_";
    const std::string totPref = "total";
    const std::string intPref = "interval";
    const std::string timPref = "time";
    const std::string nulDef  = "xxx";

      
    // Loop over the time point entries
    std::vector<std::string>::const_iterator xl;
    for( xl = timePoint.XLabels.begin(); xl != timePoint.XLabels.end(); xl++) {
	
      
      if(xl->substr(0,grpPref.size()) == grpPref ||
	 xl->substr(0,strPref.size()) == strPref ||
	 xl->substr(0,totPref.size()) == totPref ||
	 xl->substr(0,intPref.size()) == intPref ||
	 xl->substr(0,timPref.size()) == timPref ||
	 xl->substr(0,timPref.size()) == nulDef
	 )
	continue;
	  
      const std::string chainName = *xl;
	
      float inRate = 0., outRate = 0., preRate = 0., algoRate = -1, rawRate = 0.;
      timePoint.get(*xl, "input",    inRate);
      timePoint.get(*xl, "output",   outRate);

      timePoint.get(*xl, "prescaled", preRate);
      // 13.04.12 -- the name will change in the near future but for backward compatibility...
      if( (preRate > -.0000001) && (preRate < .0000001)) timePoint.get(*xl, "prescale", preRate);

      timePoint.get(*xl, "raw",      rawRate);
      if( std::find(timePoint.XLabels.begin(), timePoint.XLabels.end(), "algoIn") != timePoint.XLabels.end())
	timePoint.get(*xl, "algoIn",      algoRate);

      //find the groups to which this chain belongs
      const std::map<std::string, std::set<std::string> >::const_iterator cgit = chToGrp.find(chainName);
      
      if(  cgit == chToGrp.end() ) {
	if(RunInfo::SMKChanged() )
	  std::cout << "No chain to group table found for: " << chainName << std::endl;
	continue;
      }
	
      // Fill the AllChains table
      FillOneChainTable_Run2(ChainTable::GetAllChainsName(),
			     potFileName,
			     xl,
			     groupRates,
			     inRate, outRate, preRate, algoRate,rawRate,
			     chainTables);


      // loop over the groups to which this chain belongs
      std::set<std::string>::const_iterator grit;
      for(grit = cgit->second.begin(); grit != cgit->second.end(); grit++) {
	  
	const std::string &groupName = *grit;
	
	FillOneChainTable_Run2(groupName,
			     potFileName,
			     xl,
			     groupRates,
			     inRate, outRate, preRate, algoRate,rawRate,
			     chainTables);

      }
    }
  }


  void wtools::FillCPUTable(CPUTable &cpuTable)
  {
      
    cpuTable.ClearLocalPreamble();
    
    if( !TPointInfo::GetL2_CPUInfo().IsValid() || !TPointInfo::GetEF_CPUInfo().IsValid())
      return;
    
    TimePoint_IS &L2CPU_tp  = TPointInfo::GetL2_CPUInfo().m_timePoint;
    TimePoint_IS &EFCPU_tp  = TPointInfo::GetEF_CPUInfo().m_timePoint;
    
    TimePoint_IS &L2_timePoint = TPointInfo::GetL2Info().m_timePoint;
    TimePoint_IS &EF_timePoint = TPointInfo::GetEFInfo().m_timePoint;

    bool L2isValid = TPointInfo::GetL2Info().IsValid();
    bool EFisValid = TPointInfo::GetEFInfo().IsValid();

    if(L2isValid) {
      if(L2CPU_tp.MetaData.size() == 1) {
	cpuTable.AddLocalPreambleLine( "L2 " + L2CPU_tp.MetaData[0] + "<br>");
      }
    }

    if(EFisValid) {
      if(EFCPU_tp.MetaData.size() == 1) {
	cpuTable.AddLocalPreambleLine( "EF " + EFCPU_tp.MetaData[0] + "<br>");
      }
    }

    std::vector<float> L2totCPUvec, EFtotCPUvec;

    float L2MaxCPU = 0, EFMaxCPU = 0;

    DataTable *dataTable = cpuTable.GetTable();
    std::vector<Row> *rows = dataTable->GetRows();
    
    std::string cpuFileName = "";
    PageData::GetOutputFileName("CPU", cpuFileName);
    
    std::string L2potFileName = "", EFpotFileName = "";
    PageData::GetOutputFileName("L2cpu", L2potFileName);
    PageData::GetOutputFileName("EFcpu", EFpotFileName);
    
    std::stringstream ss;	  
    
    for(std::vector<Row>::iterator ir = rows->begin(); ir != rows->end(); ir++){ 
      const std::string *L2name = ir->GetContents_S("L2", "name");
      
      float val;
      
      if( L2name == 0 || !L2CPU_tp.get(*L2name, "TotCPU", val) ) {
	//std::cout << "wtools::FillCPUTable -- Couldn't find timePoint entry for " << *L2name << std::endl;
	ir->UpdateCell("L2", "PerCall", "U" );
	ir->UpdateCell("L2", "FracOF", "U" );
	ir->UpdateCell("L2", "Time90", "U" );
	ir->UpdateCell("L2", "inRate", "U" );
	ir->UpdateCell("L2", "TotCPU", "U" );
	ir->UpdateCell("L2", "bar_tot", "&nbsp;" );
	L2totCPUvec.push_back(0.);
      } else {

	L2totCPUvec.push_back(val);
	L2MaxCPU = (L2MaxCPU > val)? L2MaxCPU : val;
	
	ss.str("");
	ss << std::setprecision(precision) << val;
	std::string valString = ss.str();
	if(L2potFileName != "")
	  valString = "<a href = \"" + L2potFileName + "#" + *L2name + "\">" + valString + "</a>";
	ir->UpdateCell("L2", "TotCPU", valString );
	
	L2CPU_tp.get(*L2name, "PerCall", val);
	ss.str("");
	ss << std::setprecision(precision) << val;
	ir->UpdateCell("L2", "PerCall", ss.str() );
	
	L2CPU_tp.get(*L2name, "FracOF", val);
	ss.str("");
	ss << std::setprecision(precision) << val;
	ir->UpdateCell("L2", "FracOF", ss.str() );
	
	L2CPU_tp.get(*L2name, "Time90", val);
	ss.str("");
	ss << std::setprecision(precision) << val;
	ir->UpdateCell("L2", "Time90", ss.str() );
	
	if(L2isValid) {
	  if( std::find(L2_timePoint.XLabels.begin(), L2_timePoint.XLabels.end(), "algoIn") != L2_timePoint.XLabels.end())
	    L2_timePoint.get(*L2name, "algoIn", val);
	  else
	    L2_timePoint.get(*L2name, "prescale", val);
	  
	  ss.str("");
	  ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << val;
	  ir->UpdateCell("L2", "inRate", ss.str() );
	  
	} else {
	  ir->UpdateCell("L2", "inRate", "U");
	}
      }
	
      const std::string *EFname = ir->GetContents_S("EF", "name");

      if( EFname == 0 || !EFCPU_tp.get(*EFname, "TotCPU", val) ) {
	//std::cout << "wtools::FillCPUTable -- Couldn't find timePoint entry for " << *EFname << std::endl;
	ir->UpdateCell("EF", "PerCall", "U" );
	ir->UpdateCell("EF", "FracOF", "U" );
	ir->UpdateCell("EF", "Time90", "U" );
	ir->UpdateCell("EF", "TotCPU", "U" );
	ir->UpdateCell("EF", "inRate", "U" );
	ir->UpdateCell("EF", "bar_tot", "&nbsp;" );
	EFtotCPUvec.push_back(0.);
      } else {

	EFtotCPUvec.push_back(val);
	EFMaxCPU = (EFMaxCPU > val)? EFMaxCPU : val;
	  
	ss.str("");
	ss << std::setprecision(precision) << std::setiosflags(std::ios_base::fixed) << val;
	std::string valString = ss.str();
	if(EFpotFileName != "")
	  valString = "<a href = \"" + EFpotFileName + "#" + *EFname + "\">" + valString + "</a>";
	ir->UpdateCell("EF", "TotCPU", valString );

	EFCPU_tp.get(*EFname, "PerCall", val);
	ss.str("");
	ss << std::setprecision(precision) << val;
	ir->UpdateCell("EF", "PerCall", ss.str() );

	EFCPU_tp.get(*EFname, "FracOF", val);
	ss.str("");
	ss << std::setprecision(precision) << val;
	ir->UpdateCell("EF", "FracOF", ss.str() );
	
	EFCPU_tp.get(*EFname, "Time90", val);
	ss.str("");
	ss << std::setprecision(precision) << val;
	ir->UpdateCell("EF", "Time90", ss.str() );

	if(EFisValid) {
	  if( std::find(EF_timePoint.XLabels.begin(), EF_timePoint.XLabels.end(), "algoIn") != EF_timePoint.XLabels.end())
	    EF_timePoint.get(*EFname, "algoIn", val);
	  else
	    EF_timePoint.get(*EFname, "prescale", val);
	  
	  ss.str("");
	  ss << std::setprecision(ratePrecision) << std::setiosflags(fixOrSci) << val;
	  ir->UpdateCell("EF", "inRate", ss.str() );
	  
	} else {
	  ir->UpdateCell("EF", "inRate", "U");
	}
      }
	

    }

    // fill in the bars

    int iRow = 0;
    ss.str("");
    for(std::vector<Row>::iterator ir = rows->begin(); ir != rows->end(); ir++){ 
      
      float val = (L2MaxCPU > .000001)? L2totCPUvec[iRow] / L2MaxCPU : 0.;
      if(val < fracLim) {
	ss << barCode << static_cast<int>(val * maxBarPixels);
	ir->UpdateCell("L2", "bar_tot", ss.str() );
	ss.str("");
      } else
	ir->UpdateCell("L2", "bar_tot", "> 1" );

      val = (EFMaxCPU > .000001)? EFtotCPUvec[iRow] / EFMaxCPU : 0.;
      if(val < fracLim) {
	ss << barCode << static_cast<int>(val * maxBarPixels);
	ir->UpdateCell("EF", "bar_tot", ss.str() );
	ss.str("");
      } else
	ir->UpdateCell("EF", "bar_tot", "> 1" );
      
      iRow++;  
    }
  }

  void wtools::FixupL1ConfigFromTP(L1Table &l1Table, TimePoint_IS &L1_tp)
  {

    if(L1_tp.XLabels.size() <= 1)
      return;

    /* This sets up the columns */
    l1Table.MakeTable();

    /* Now add the rows */
    for( std::vector<std::string>::iterator ix = L1_tp.XLabels.begin(); ix != L1_tp.XLabels.end(); ix++) {
      float val;
      if (!L1_tp.get(*ix, "PS", val)) {
	std::cout << "wtools::FixupL1ConfigFromTP: no PS column for \"" << *ix << 
	  "\" found, very strange" << std::endl;
	continue;
      }
      if(val < 0.5)
	continue;
      l1Table.AddEntry( *ix );
    }
    l1Table.BookPOTs();
  }

    
  void wtools::DumpPOTtoWMIPage(daq::wmi::OutputStream* outStream, 
				const std::string &POTname, const std::string &fileName)
  {
    const POTBase *theBasePOT = POTBase::GetPOT(POTname);
    
    if(theBasePOT == 0)
      return;
    
    const POT *thePOT = dynamic_cast<const POT *>( theBasePOT );
    
    if(thePOT == 0) {
      std::cout << "DumpPOTtoWMIPage: Attempt to dump sthg which is not a POT" << std::endl;
      return;
    }
    
    const std::vector<POT::ParDef> &thePars = thePOT->GetPars();
    
    const std::deque<long> &timeStamps = POTBase::getLongTimeStamps();
    long long startupTime = POTBase::GetStartupTime();
    //std::cout << "sizes: " << thePars.size() << ", " << timeStamps.size() << std::endl;
    
    Table *pot_table = new Table(1 + thePars.size(), timeStamps.size() + 1 );
    if(pot_table == 0) {
      std::cout << "DumpPOTtoWMIPage: couldn't get a Table" << std::endl;
      return;
    }
    pot_table->setBorder(1);
    pot_table->setBGColor( Color(247, 252, 252) );
    
    // Header
    pot_table->addElement(1, 1, new Text("Time"));	
    int hcol = 2;

    for( std::vector<POT::ParDef>::const_iterator itpd = thePars.begin();
	 itpd != thePars.end(); itpd++, hcol++) {
      pot_table->addElement(hcol, 1, new Text( itpd->legendName ));
    }
    
    // Data
    int row = 0;
    for( std::deque<long>::const_iterator itst = timeStamps.begin(); 
	 itst != timeStamps.end(); itst++, row++) {
      long long theTime = startupTime/1000000 + (*itst);
      OWLTime ot = OWLTime(theTime);
      pot_table->addElement(1, row+2, new Text(ot.str()));	
      int col = 2;
      for(std::vector<POT::ParDef>::const_iterator itpd = thePars.begin(); 
	  itpd != thePars.end(); itpd++, col++) {
	
	std::stringstream ss;
	ss << itpd->data_l[row];
	pot_table->addElement(col, row+2, new Text( ss.str() ));	
      }
    }

    outStream->open(fileName);
    Text title("<h2> Dump of " + POTname + "</h2><br>");
    outStream->write(title);
    outStream->write(*pot_table);
    delete pot_table;
    outStream->close();
  }
    


} // end namespace wtrp
