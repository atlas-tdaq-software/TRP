#include "wtrp/lumi.h"

#include <iostream>
#include "is/infoany.h"
#include "is/infoiterator.h"

namespace wtrp {

  const std::string Lumi::m_lumiPartition = "OLC";
  const std::string Lumi::m_lumiServer = "OLC";
  const std::string Lumi::m_lumiVariable = m_lumiServer + ".OCApp/ATL_instLum";

  const double Lumi::m_sigma = 40 * 10^-27; // inelastic sigma at injection energy -- temp. kloodge

  bool Lumi::m_isValid = false;
  bool Lumi::m_isFresh = false;
  long long Lumi::m_now;
  double Lumi::m_lumi;
  float Lumi::m_rate;

  Lumi::Lumi()
  { ; }
  
  Lumi::~Lumi() {
  }

  void Lumi::GetFromServer() {
    
    m_isValid = false;
    m_isFresh = false;

    m_now = OWLTime().total_mksec_utc();

    // Create the instance of partition
    IPCPartition partition(m_lumiPartition);

    try {
      //ISInfoIterator ii( partition, m_lumiServer );
      //const ISCriteria & = ISCriteria( ".*" ) is the 3-rd parameter to the ISInfoIterator constructor
      const ISCriteria& criteria = ISCriteria( ".*" );
      ISInfoIterator ii( partition, m_lumiServer, criteria);
      while( ii() ) {
	if(ii.name() == m_lumiVariable) {
	  ISInfoAny isa;
	  ii.value( isa );
	  int attr_number = isa.countAttributes( );
	   for ( int i = 0; i < attr_number; i++ ) {
	     if(isa.getAttributeType() == ISType::Double) {
	       isa >> m_lumi;
	       m_isValid = true;
	       m_rate = static_cast<float> (m_lumi * GetInelasticCrossSection());
	       if( (m_now - ii.time().total_mksec_utc()) < 60000000)
		 m_isFresh = true;
	       break;
	     }
	   }
	   break;
	}
      }
    } 
    catch(daq::is::RepositoryNotFound &) {
      //std::cout << "Lumi:GetFromServer: failed to get Lumi: Repository not found" << std::endl;
      m_isValid = false;
      m_isFresh = false;
      return;
    }
    catch(daq::is::InvalidCriteria &) {
      //std::cout << "Lumi:GetFromServer: failed to get Lumi: Invalid criterea" << std::endl;
      m_isValid = false;
      m_isFresh = false;
      return;
    }
    
    //Dump();
  }

}
