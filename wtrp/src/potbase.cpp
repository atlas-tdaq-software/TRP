
#include "wtrp/pot.h"


#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <set>
#include <sstream>


namespace wtrp {

  /* Doesn't show up well on laptop screens
  struct {
    std::string name;
    float r;
    float b;
    float g;
  } graphColorMap[] = {
    {"pot_black",            0,   0,   0},
    {"pot_orange",         230, 159,   0},
    {"pot_sky_blue",        86, 180, 233},
    {"pot_bluish_green",     0, 158, 115},
    {"pot_vermillion",     213,  94,   0},
    {"pot_reddish_purple", 204, 121, 167},
    {"pot_yellow",         240, 228,  66},
    {"pot_blue",             0, 114, 178},
    {"",                 0,   0,   0}
  };
  */

  static struct {
    std::string name;
    float r;
    float b;
    float g;
  } graphColorMap[] = {
    {"pot_orange",        0xFF, 0x99, 0x33},
    {"pot_red",           0xFF, 0x33, 0x33},
    {"pot_moss_green",    0x33, 0x66, 0x00},
    {"pot_dusky_pink",    0xCC, 0x99, 0x99},
    {"pot_grey",          0xAA, 0xAA, 0xAA},
    {"pot_blue",          0x00, 0xCC, 0xFF},
    {"pot_purple",        0x99, 0x00, 0xCC},
    {"pot_turquoise",     0x00, 0x99, 0x99},
    {"pot_lime_green",    0x66, 0xFF, 0x33},
    {"pot_black",         0x00, 0x00, 0x00},
    {"",                  0,    0,    0}
  };


  static int InitGraphColorMap()
  {
    int offset = 1200;
    for(int i = 0; graphColorMap[i].name != ""; i++) {
      new TColor(offset + i, graphColorMap[i].r, graphColorMap[i].b,  graphColorMap[i].g, graphColorMap[i].name.c_str()); 
    }
    return offset;
  }

  // static variables:

  const unsigned int POTBase::m_length_l = 480; // max length of long-term deque
  const unsigned int POTBase::m_length_s = 30; // max length of short-term deque
  
  const unsigned int POTBase::m_maxParsPerPlot = 8;
  
  const long long POTBase::m_aCoolMill = 1000000;
  const unsigned int POTBase::m_pointsToAve = 3; // num. points to average for long-term deque
  
  const long POTBase::m_twentyFourHours = 24 * 60 * 60;
  
  // for tests
  //const unsigned int POTBase::m_pointsToAve = 2; // num. points to average for long-term deque
  //const long POTBase::m_twentyFourHours = 120;
  
  const std::string  POTBase::m_fileType = "png"; // e.g. svg or png
  const unsigned int POTBase::m_titleHeight = 25;
  const unsigned int POTBase::m_canWidth = 1200;
  const unsigned int POTBase::m_hisTopMarg = 10;
  const unsigned int POTBase::m_hisBottomMarg = 30;
  const unsigned int POTBase::m_padWidth_l = 900;
  const unsigned int POTBase::m_axisLabelSize = 17;
  const unsigned int POTBase::m_leftMargin_l  = 60;
  const unsigned int POTBase::m_rightMargin_l = 1;
  const unsigned int POTBase::m_leftMargin_s  = 1;
  const unsigned int POTBase::m_rightMargin_s = 40;
  const unsigned int POTBase::m_font = 20;
  const          int POTBase::m_lineWidth = 2;
  const          int POTBase::m_dottedLine = 9; // actually long dashes
  const        float POTBase::m_dotSize = 0.75;
  const          int POTBase::m_polyInPixels = 8; // conversion between polymarker size and pixels

  // these give the number of updates between plotting dots

  int POTBase::m_dotSpacing_l = (m_padWidth_l - m_leftMargin_l - m_rightMargin_s)
                                / (m_maxParsPerPlot * m_dotSize * m_polyInPixels);
  int POTBase::m_dotSpacing_s = (m_canWidth - m_padWidth_l - 
                                 m_leftMargin_l - m_rightMargin_s)
                                 / (m_maxParsPerPlot * m_dotSize * m_polyInPixels);
    

  const std::string POTBase::m_mapAttacherScript = "attachmap";

  std::string* POTBase::m_defaultImageMap;

  unsigned int POTBase::m_canWidthDiff;
  unsigned int POTBase::m_canHeightDiff;

  std::vector<POTBase*> POTBase::m_thePOTs;
  TStyle *POTBase::m_potStyle_s = 0;
  TStyle *POTBase::m_potStyle_l = 0;
  TColor *POTBase::m_potColor = 0;
  int POTBase::m_graphColorOffset = InitGraphColorMap();

  std::string POTBase::m_tmpPath = "";
  std::string POTBase::m_missingParameters;

  std::deque<long> POTBase::m_timeStamps_s;
  std::deque<long> POTBase::m_timeStamps_l;
  long POTBase::m_timeWaiting = 0;
  unsigned int POTBase::m_nWaiting;
  long long POTBase::m_startupTime = 0;
  OWLTime POTBase::m_referenceTime;
  OWLTime POTBase::m_now(OWLTime::Seconds);

  OWLTimer POTBase::m_timer1; // for timing tests
  OWLTimer POTBase::m_timer2; // for timing tests

  POTBase::~POTBase() {
  }

  void POTBase::SetCommonStyleParameters(TStyle *theStyle)
  {
    theStyle->SetCanvasColor(0);
    theStyle->SetCanvasBorderMode(0);
    theStyle->SetCanvasBorderSize(4);
    //theStyle->SetFrameFillColor(1001);
    theStyle->SetFrameFillColor(0);
    theStyle->SetLabelFont(m_font);

    theStyle->SetPadBorderMode(0);
    theStyle->SetPadColor(0);
    theStyle->SetPadBorderMode(0);
    theStyle->SetPadBorderSize(0);
    theStyle->SetPadTopMargin(.0);
    

    theStyle->SetTitleColor(0);
    theStyle->SetOptStat("");
    
  }

  POTBase::POTBase(std::string name, std::string title) :
    m_name(name), m_title(title), m_isVisible(true) {

    m_thePOTs.push_back( this );

    if(m_potStyle_s == 0) {

      //std::cout << "setting up a ROOT style" << std::endl; 

      TColor::CreateColorWheel();
      //m_potColor = new TColor(1001, 247, 252, 252, "potGray");
      m_potColor = gROOT->GetColor(0); // white color
      m_potStyle_s = new TStyle("PotStyle_s","Style for short term POTs");
      if(m_potStyle_s == 0) {
	std::cout << "failed to get a new TStyle" << std::endl;
	return;
      }
      SetCommonStyleParameters(m_potStyle_s);
      m_potStyle_s->SetPadLeftMargin( double(m_leftMargin_s)/double(m_canWidth - m_padWidth_l) );
      m_potStyle_s->SetPadRightMargin( double(m_rightMargin_s)/double(m_canWidth - m_padWidth_l) );

      struct timeval now;
      gettimeofday(&now, 0); 
      //OWLTime m_referenceTime(now.tv_sec, now.tv_usec);
      OWLTime m_referenceTime(now.tv_sec);
      m_startupTime = m_referenceTime.total_mksec_utc();

      //std::cout << "Startup time: " << m_startupTime << ", month: " <<  m_referenceTime.month() << std::endl;
      
      TDatime da(m_referenceTime.year(), 
		 //m_referenceTime.month()+1, //OWL returns a range 0-11, TDatime expects 1-12
		 m_referenceTime.month(), // since tdaq-05-04-00 the range is 1-12
		 m_referenceTime.day(), 
		 m_referenceTime.hour(), 
		 m_referenceTime.min(), 
		 m_referenceTime.sec());

      /*
      std::cout << "the months: " << m_referenceTime.month() << ", " << da.GetMonth()
		<< ", " << da.AsString() << std::endl;
      */

      m_potStyle_s->SetTimeOffset(da.Convert());



      m_potStyle_l = new TStyle("PotStyle_l","Style for long term POTs");
      SetCommonStyleParameters(m_potStyle_l);
      m_potStyle_l->SetPadLeftMargin( double(m_leftMargin_l)/double(m_padWidth_l) );
      m_potStyle_l->SetPadRightMargin( double(m_rightMargin_l)/double(m_padWidth_l) );
      m_potStyle_l->SetTimeOffset(da.Convert());


      // Figure out the difference between the given canvas width/height and the actual
      TCanvas *testCan = new TCanvas("test", "test", 1000, 1000);
      m_canWidthDiff =  1000 - testCan->GetWw();
      m_canHeightDiff = 1000 - testCan->GetWh();
      delete testCan;
    }

  }

  POTBase *POTBase::GetPOT(const std::string &name)
  {
    for( std::vector<POTBase*>::iterator ip = m_thePOTs.begin(); ip != m_thePOTs.end(); ip++){
      if(name == (*ip)->GetName())
	return *ip;
    }
    return 0;
  }

  void POTBase::DeletePOT(const std::string &name)
  {
    for( std::vector<POTBase*>::iterator ip = m_thePOTs.begin(); ip != m_thePOTs.end(); ip++){
      if(name == (*ip)->GetName()) {
	delete *ip;
	m_thePOTs.erase( ip );
	break;
      }
    }
  }

  void POTBase::UpdatePOTs() 
  {

    struct timeval now;
    gettimeofday(&now, 0); 
    m_now = OWLTime(now.tv_sec);
    //m_now = OWLTime(now.tv_sec, now.tv_usec);

    //std::cout << "m_now: " << m_now.total_mksec_utc() << std::endl;

    long nowDiff = static_cast<long> ( (m_now.total_mksec_utc() - m_startupTime) / m_aCoolMill);
    m_timeStamps_s.push_back( nowDiff );
    if( m_timeStamps_s.size() > m_length_s)
      m_timeStamps_s.pop_front();

    m_timeWaiting += nowDiff;
    m_nWaiting++;

    bool updateLong = false;
    if(m_nWaiting == m_pointsToAve) {

      updateLong = true;
      m_timeWaiting /= m_nWaiting;
      m_timeStamps_l.push_back(m_timeWaiting);
      m_timeWaiting = 0;
      m_nWaiting = 0;
      if(m_timeStamps_l.size() > m_length_l)
	m_timeStamps_l.pop_front();
    }

    for( std::vector<POTBase*>::iterator ip = m_thePOTs.begin(); ip != m_thePOTs.end(); ip++){ // POT loop
      (*ip)->Update( updateLong );
    }
  }

  bool POTBase::Prepare() {
    m_missingParameters = "";

    return MakeTmpDir();
  }

  bool POTBase::MakeTmpDir() {

    /* 
       Make a scratch directory if needed. Check if it already exists and is writable.
       If not, try to make one, possibly with a new name if an existing one
       is found which is not writable. Set m_tmpPath either to the directory name or
       "" if unsuccessful. If m_tmpPath is non-zero on entry, just return.
    */

    if(m_tmpPath != "")
      return true;

    int rc;
      
    uid_t my_id = getuid();
    gid_t my_gp = getgid();
    
    std::stringstream dss;
    dss << "/tmp/wtrp_pots_" << my_gp << "_" << my_id; 
    m_tmpPath = dss.str();

    rc = access(m_tmpPath.c_str(), F_OK);
      
    if(rc != 0) { // directory does not exist. Try to make it;
      
      rc = access("/tmp", W_OK); // check if tmp is writable
      
      if(rc == 0) {
	rc = mkdir(m_tmpPath.c_str(), S_IFDIR | S_IRWXU | S_IRWXG | S_IWGRP);
	rc = chmod(m_tmpPath.c_str(), S_IRWXU | S_IRWXG | S_IWGRP | S_IXOTH);
      }
    }
    
    rc = access(m_tmpPath.c_str(), W_OK);
    
    if(rc != 0) {
      time_t now;
      time(&now);
      
      m_tmpPath += "_" + now;
      rc = mkdir(m_tmpPath.c_str(), S_IFDIR | S_IRWXU | S_IRWXG | S_IWGRP);
      rc = chmod(m_tmpPath.c_str(), S_IRWXU | S_IRWXG | S_IWGRP | S_IXOTH);
      
      if(rc != 0) {
	std::cout << "POTBase::MakeTmpDir -- unable to get temporary file space" << std::endl;
	m_tmpPath = "";
	return false;
      }
    }
    // needed to change an existing directory before I remembered to also set permission for other
    rc = chmod(m_tmpPath.c_str(), S_IRWXU | S_IRWXG | S_IWGRP | S_IXOTH | S_IROTH | S_IWOTH); 
    std::cout << "POTBase::MakeTmpDir -- using temporary directory: " << m_tmpPath << std::endl;

    return true;
  }


  void POTBase::DumpPOTs()
  {

    std::cout << "Dump of POTs, Number of POTs: " << m_thePOTs.size() << std::endl;
    std::cout << "Reference time: " << m_referenceTime << std::endl;

    for( std::vector<POTBase*>::const_iterator ip = m_thePOTs.begin(); ip != m_thePOTs.end(); ip++)
      (*ip)->Dump();
  }

  void POTBase::DumpBase() const
  {
    std::cout << "POT name: " << m_name
	      << " title: " << m_title
	      << " length (short term plot): " << m_length_s
	      << " length (long term plot): " << m_length_l
	      << std::endl;

    std::cout << "Number of time points (short term): " << m_timeStamps_s.size() << " the time stamps: " << std::endl;

    int ntp_s = 0;
    for(std::deque<long>::const_iterator ip =  m_timeStamps_s.begin(); ip != m_timeStamps_s.end(); ip++){
      std::cout << *ip << ", ";
      if( ( (ntp_s++) % 10) == 0) std::cout << std::endl;
    }
    std::cout << std::endl;

    std::cout << "Number of time points (long term): " << m_timeStamps_l.size() << " the time stamps: " << std::endl;

    int ntp_l = 0;
    for(std::deque<long>::const_iterator ip =  m_timeStamps_l.begin(); ip != m_timeStamps_l.end(); ip++){
      std::cout << *ip << ", ";
      if( ((ntp_l++) % 10) == 0) std::cout << std::endl;
    }
    std::cout << std::endl;

  }


  // return false if a failure occurs which implies there is no sense in trying to output any other POTs
  bool POTBase::OutputPOT( const std::string &potName, 
			   daq::wmi::OutputStream* outStream,
			   std::vector<PlotStuff> &invisiblePlots
			   )
  {

    if(m_tmpPath == "") {
      //std::cout << "POT::OutputPOT -- no tmp directory" << std::endl;
      return false;
    }
    
    const POTBase *thePOT = 0;
    
    for( std::vector<POTBase*>::iterator ip = m_thePOTs.begin(); ip != m_thePOTs.end(); ip++){
      if( (*ip)->m_name == potName) {
	thePOT = *ip;
	break;
      }
    }
    /*
    if ( thePOT->m_name == "globalStatus")
      thePOT->Dump();
    */
    if( thePOT == 0 ) {
      daq::wmi::Text errMess("<p> POT " + potName + " not found");
      outStream->write(errMess);
      return true;
    }

    std::vector<PlotStuff> plotStuff;
    std::vector<std::string> captions;
    
    m_timer1.start();

    bool isOK = thePOT->GetPOTPlots(plotStuff);

    m_timer1.stop();

    if( !isOK){
      daq::wmi::Text errMess("<p> POT " + potName + " not available");
      outStream->write(errMess );
      return true;
    }
    int ipl = 0;
    for(std::vector<PlotStuff>::const_iterator ipo = plotStuff.begin(); ipo != plotStuff.end(); ipo++,ipl++) {
      
      if(!ipo->isVisible) {
	invisiblePlots.push_back(*ipo);
	continue;
      }
      daq::wmi::Text beforePlot(ipo->caption + "\n");
      
      outStream->write(beforePlot);
    

      // SVG graphics needs to be imbeded in something which gives it a size.
      if(m_fileType == "svg") {
	//daq::wmi::Text tabCellBegin("<table><tr><td width=100%>");
	//outStream->write(tabCellBegin);
	std::string shortFileName = ipo->fileName;
	size_t lastSlash = shortFileName.rfind("/");
	if(lastSlash != std::string::npos)
	  shortFileName = shortFileName.substr(lastSlash+1);
	daq::wmi::Text objectBegin("\n<object data=\"" + shortFileName);
	outStream->write(objectBegin);
	daq::wmi::Text objectEnd("\" type=\"image/svg+xml\" width=\"95%\" class=\"img\"></object>)\n");
	outStream->write(objectEnd);

	daq::wmi::Text commentBegin("<!-- ");
	outStream->write(commentBegin);
	daq::wmi::Picture pic(ipo->fileName, ipo->width, ipo->height);
	pic.setNeedCopy(true);
	outStream->write(pic);
	daq::wmi::Text commentEnd("-->\n");
	outStream->write(commentEnd);


	//daq::wmi::Text tabCellEnd("<table><tr><td width=100%>");
	//outStream->write(tabCellEnd);

      } else {

	// imbed the picture into a div so that a script can be called to attach an image map
	daq::wmi::Text divBegin("<div  onmouseover=\"" + m_mapAttacherScript + "()\">\n");
	outStream->write(divBegin);
     	daq::wmi::Picture pic(ipo->fileName, ipo->width, ipo->height);
	pic.setNeedCopy(true);
	outStream->write(pic);
	daq::wmi::Text divEnd("</div>\n");
	outStream->write(divEnd);
	
      }

      //outStream->write(afterPlot);
    }
    return true;
  }

  void POTBase::TidyUp(bool deleteDir)
  {

    //std::cout << "TidyUP -- deleteDir: " << deleteDir << ", directory: " << m_tmpPath << std::endl;

    if(m_tmpPath.substr(0,4) == "")
      return;

    std::string comm = "rm -f " + m_tmpPath + "/*";
    system(comm.c_str());
    if(deleteDir) {
      std::cout << "Deleting tmp directory" << std::endl;
      if(m_tmpPath.substr(0,4) != "")
	rmdir(m_tmpPath.c_str());
      m_tmpPath = "";
    }
  }
  
  void POTBase::ConvertTimeToString(long time, std::string &timeString)
  {
    time_t t = time + static_cast<time_t>(m_startupTime/m_aCoolMill);
    OWLTime ot = OWLTime(t);
    unsigned short hour = ot.hour();
    unsigned short min = ot.min();
    std::stringstream ss;
    ss << std::setw(2) << std::right << std::setfill('0') << hour << ":" << 
      std::setw(2) << std::right << std::setfill('0') << min;
    timeString = ss.str();
    
    // std::cout << "ConvertTimeString: " << ot << " " << timeString << std::endl;
  }

}
