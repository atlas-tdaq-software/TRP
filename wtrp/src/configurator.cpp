#include "wtrp/configurator.h"
#include "wtrp/ismap.h"
#include "wtrp/tpointinfo.h"

#include <sys/stat.h>
#include <unistd.h>

#include <iostream>
#include <sstream>
#include <ctype.h>


namespace wtrp {
 
  std::string Configurator::m_configFileName = "";
  time_t    Configurator::m_st_mtime = 0; // Last time the config file was written
  std::vector< Configurator::PlotSpec > Configurator::m_plotSpecs;
  std::map<std::string, std::string> Configurator::m_genParams;

  const std::string Configurator::m_plotPrefix = "PLOT:";
  const std::string Configurator::m_ratePrefix = "RATE:";
  const std::string Configurator::m_paramPrefix = "PARAM:";
  const std::string Configurator::m_globalName = "globalRates_0";

  const std::string Configurator::m_optionLIN = "LIN";
  const std::string Configurator::m_optionLOG = "LOG";
  const std::string Configurator::m_optionMIN = "MIN=";
  const std::string Configurator::m_optionVisible = "HIDE";

  Configurator::PlotSpec::PlotSpec(const std::string &name, const std::vector< std::string > &params) :
    m_name(name)
  {
    if(params.size() == 0)
      m_title = "No title given";
    else
      m_title = params[0];

    std::vector<std::string>::const_iterator parit = params.begin();
    if(parit++ != params.end()) {
      for(; parit != params.end(); parit++)
	m_options.push_back( *parit );
    }
  }

  size_t Configurator::Split(const std::string str, std::vector<std::string> &substr, const std::string &sep)
  {

    std::string xsep = sep;
    size_t pos = 0;

    if(xsep == "") {
      xsep = str.substr(0,1);
      pos = 1;
    }

    for(size_t len = 0; pos < str.size(); pos += len + xsep.size()  ) {
      size_t next = str.find(xsep, pos);
      len = (next != std::string::npos)? next-pos : str.size()-pos;
      substr.push_back(str.substr(pos, len));
    }

    return substr.size();
  }

  void Configurator::AddPlot(const std::string &name, const std::string &value) {
    
    //std::cout << "Configurator::AddPlot called to add a plot: " << name << ", value: " << value << std::endl;
    
    /* Get the plot name, check that it isn't already on the list */
    
    std::string plotName = name.substr(m_plotPrefix.size());
    
    for(std::vector< PlotSpec >::const_iterator psit = m_plotSpecs.begin();
	psit != m_plotSpecs.end(); psit++) {
      
      if(psit->m_name == plotName) {
	std::cout << "Configurator::AddPlot: plot name (\"" << plotName << 
	  "\") already found, not adding" << std::endl;
	
	return;
      }
    }
    std::vector<std::string> subs;
    Split(value, subs, "|");

    m_plotSpecs.push_back( PlotSpec(plotName, subs) );
  }


  void Configurator::AddRate(const std::string &name, const std::string &value)
  {

    //std::cout << "Configurator::AddRate called to add rate to a configurable rate plot." << std::endl;
    //std::cout << "        parameter name: " << name << ", value: " << value << std::endl;

    /* Get the plot name */
    std::string plotName = name.substr(m_ratePrefix.size());
    
    /* Check that plot has already been added, if not do nothing */

    std::vector<RateSpec> *thePlotSpecs = 0;
    for(std::vector< PlotSpec >::iterator psit = m_plotSpecs.begin();
	psit != m_plotSpecs.end(); psit++) {

      if( psit->m_name == plotName ) {
	thePlotSpecs = &psit->m_rateSpecs;
	break;
      }
    }

    if(thePlotSpecs == 0) {
      std::cout << "RatePate::AddRate: attempt to add a rate to a non-existent plot. name, value:" <<
	name << ", " << value << std::endl;

      return;
    }


    /* Decode the value string, set up the corresponding RateSpec */
    std::vector<std::string> subs;
    size_t nSubs = Split(value, subs);

    /*
    int nField = 0;
    for(std::vector<std::string>::const_iterator isubs = subs.begin(); isubs != subs.end(); isubs++) {
      std::cout << "Configurator::AddRate: after splitting -- field, field  " 
		<< nField << " is " << *isubs << std::endl;
      nField++;
    }
    */

    RateSpec rateSpec;

    if(subs[0] == "EXT") {
      if(nSubs != 6) {
	std::cout << "ERROR: Configurator::AddRate: " << value 
		  << "does not have the correct number of fields, should be 6, but is " 
		  << nSubs << std::endl;
	return;
      }

      std::string rateKey = subs[1] + ":" + subs[2] + ":" + subs[3] + ":" + subs[4];

      rateSpec.m_level = POT::PD_ISMAP;
      rateSpec.m_xlabel = rateKey;
      rateSpec.m_ylabel = "";
      rateSpec.m_legendName = subs[5];
      
      ISMap::Book(subs[1], subs[2], subs[3], subs[4], rateKey, true);
      
      
    } else if(subs[0] == "TRP") {
      if(nSubs != 5) {
	std::cout << "ERROR: Configurator::AddRate: " << value 
		  << "does not have the correct number of fields, should be 5, but is " 
		  << nSubs << std::endl;
	return;
      }
      
      const std::string &L1Name = TPointInfo::GetL1Info().GetName();
      const std::string &L2Name = TPointInfo::GetL2Info().GetName();
      const std::string &EFName = TPointInfo::GetEFInfo().GetName();
      const std::string &HLTName = TPointInfo::GetHLTInfo().GetName();
      
      if(subs[1] == L1Name)
	rateSpec.m_level = POT::PD_L1;
      else if(subs[1] == L2Name)
	rateSpec.m_level = POT::PD_L2;
      else if(subs[1] == EFName)
	rateSpec.m_level = POT::PD_EF;
      else if(subs[1] == HLTName)
	rateSpec.m_level = POT::PD_HLT;
      else {
	rateSpec.m_level = POT::PD_none;
	std::cout << "ERROR: Configurator::AddRate: " << subs[1] <<
	  " (objectName) does not correspond to any supported TRP timePoint" <<std::endl;
	return;
      }

      rateSpec.m_xlabel = subs[2];
      rateSpec.m_ylabel = subs[3];
      rateSpec.m_legendName = subs[4];


    } else {
      std::cout << "ERROR: Configurator::AddRate: first field must be either TRP or EXT -- found: " 
		<< value << std::endl;
      return;
    }
    //std::cout << "Configurator::AddRate: \"" << subs[4] << "\" will be added to plot " << plotName << std::endl;
    thePlotSpecs->push_back(rateSpec);
  }

  bool Configurator::UpdateConfiguration()
  {

    static bool firstCall = true;

    if( m_configFileName == "" ) {
      if(firstCall)
	std::cout << "ERROR: Configurator::UpdateConfiguration called but file name was not set. " << std::endl;
      return false;
    }
    
    /* Get  the config file status structure */
    struct stat fileStat;
    int returnCode =  stat(m_configFileName.c_str() , &fileStat);
    if( returnCode != 0) {
      if(firstCall)
	std::cout << "ERROR: Configurator::CheckSyntax: unable to stat the config file, " <<
	  " Check that the value of the configFileName parameter in the config file is correct" <<
	  std::endl;
      return false;
    }
    firstCall = false;

    if(fileStat.st_mtime == m_st_mtime) 
      return false;

    std::cout << "Configurator::UpdateConfiguration: updating configurable plots" << std::endl;

    m_st_mtime = fileStat.st_mtime;

      
    TiXmlDocument doc( m_configFileName.c_str() );
    bool loadOkay = doc.LoadFile();
    if ( !loadOkay ){
      std::cout << "ERROR: Configurator::UpdateConfiguration: Could not load config file" <<
	" (maybe, a synatx issue?) aborting update, file name: "<< 
	m_configFileName << std::endl;
      return false;
    }
    
    TiXmlNode* node = 0;
    TiXmlElement* Element = 0;
    TiXmlElement* plNode = 0;
    node = doc.FirstChild( "setup" );
    if(!node){
      std::cout 
	<< "ERROR: Configurator::UpdateConfiguration: No root element in configuration file, aborting update" 
	<< std::endl;
      return false;
    }
    
    node = node->FirstChild("plugins");
    if(!node){
      std::cout <<  "ERROR: Configurator::UpdateConfiguration: Invalid plugins in config file, aborting update" 
		<< std::endl;
      return false;
    }
    plNode = node->FirstChildElement("plugin");
    if(!plNode){
      std::cout <<  "ERROR: Configurator::UpdateConfiguration: Error plugin directive not found, aborting update" 
		<< std::endl;
      return false;
    }
    
    if (!CheckSyntax( plNode ) ) {
      std::cout << "Configurator::UpdateConfiguration: Syntax checking failed, update aborted." << std::endl;
      return false;
    }

    /* Should be safe to reset the plotSpecs and ISMap now */
    m_plotSpecs.clear();
    m_genParams.clear();
    //std::cout << "Configurator:: About to clear map" << std::endl;
    ISMap::ClearMap();

    /* Look at each parameter, if a plot or rate spec, store it */
    for( Element = plNode->FirstChildElement("parameters"); 
	 Element != 0;
	 Element = Element->NextSiblingElement("parameters") ) {
      
      std::string name = Element->Attribute("name");
      std::string value = Element->Attribute("value");
      
      if( name.substr(0, m_plotPrefix.size()).compare(m_plotPrefix) == 0) {
	AddPlot(name, value);
      } else if( name.substr(0, m_ratePrefix.size()).compare(m_ratePrefix) == 0) {
	AddRate(name, value);
      } else if( name.substr(0, m_paramPrefix.size()).compare(m_paramPrefix) == 0) {
	m_genParams[name.substr(m_paramPrefix.size())] = value;
	std::cout << "Found parameter: " << name.substr(m_paramPrefix.size()) 
		  << ", Value: " << value << std::endl;
      }
    }
    std::cout << "Configurator::UpdateConfiguration, done" << std::endl;
    return true;
  } 

  bool Configurator::CheckSyntax( TiXmlNode* pluginNode )
  {

    std::vector<std::string> knownPlotNames;
    
    TiXmlElement* Element = 0;

    bool firstPlot = true;
    for( Element = pluginNode->FirstChildElement("parameters"); 
	 Element != 0;
	 Element = Element->NextSiblingElement("parameters") ) {
      
      std::string name = Element->Attribute("name");
      std::string value = Element->Attribute("value");
      
      // Checks for plot directives
      if( name.substr(0, m_plotPrefix.size()).compare(m_plotPrefix) == 0) {
	if(value == "") {
	  std::cout 
	    << "Configurator::CheckSyntax: Error --Attempt to book with blank value parameter for "
	    << name << std::endl;
	  return false;
	}
	
	std::string plotName = name.substr(m_plotPrefix.size());
	if(plotName == "") {
	  std::cout 
	    << "Configurator::CheckSyntax: Error --Attempt to book with no name "
	    << name << std::endl;
	  return false;
	}
	
	if(firstPlot) {
	  if(name.substr(m_plotPrefix.size()) != m_globalName) {
	    std::cout << "Configurator::CheckSyntax: Error -- the first specified plot must be named "
		      << m_globalName 
		      << ", instead found: " << name.substr(m_plotPrefix.size())
		      << std::endl;
	    return false;
	  }
	  firstPlot = false;
	}

	knownPlotNames.push_back(plotName);
	
	std::vector<std::string> subs;
	Split(value, subs, "|");
	
	std::vector< std::string >::const_iterator subit = subs.begin();

	if(subit == subs.end()) {
	  std::cout 
	    << "Configurator::CheckSyntax: Error -- Attempt to book with empty value field "
	    << name << std::endl;
	  return false;
	}

	for(subit++ ; subit != subs.end(); subit++) {
	  if( *subit != m_optionLIN &&
	      *subit != m_optionLOG &&
	      *subit != m_optionVisible &&
	      subit->substr(0, m_optionMIN.length() ) != m_optionMIN ) {
	    std::cout << "Configurator::CheckSyntax: Error -- unrecognized option: " << *subit <<
	      " for " << name << std::endl;
	    return false;
	  }
	  if(subit->substr(0, m_optionMIN.length() ) == m_optionMIN ) {

	    std::string minstr = subit->substr(m_optionMIN.length());
	    const char *minchars = minstr.c_str();

	    bool hasDecimalPoint = false;
	    bool isNum = true;
	    for(int ic = 0; minchars[ic] != '\0'; ic++) {
	      char c = minchars[ic];
	      if( (ic == 0) && ( c == '+' || c == '-') )
		continue;

	      if( isdigit(c) )
		continue;

	      if(c == '.') {
		if(!hasDecimalPoint) {
		  hasDecimalPoint = true;
		  continue;
		}
	      }
	      isNum = false;
	      break;
	    }

	    if(!isNum) {
	      std::cout << minchars << " cannot be converted to a floating point number for YMIN in plot " 
			<< plotName << std::endl;
	      return false;
	    }

	  }
	}

	// Checks for rate directives
      } else if( name.substr(0, m_ratePrefix.size()).compare(m_ratePrefix) == 0) {
	
	
	/* Get the plot name */
	std::string plotName = name.substr(m_ratePrefix.size());
	
	/* Check that plot has already been added, if not do nothing */
	
	bool found = false;
	for (std::vector<std::string>::const_iterator pnit = knownPlotNames.begin();
	     pnit != knownPlotNames.end(); pnit++) {
	  
	  if( *pnit == plotName ) {
	    found = true;
	    break;
	  }
	}
	
	if(!found) {
	  std::cout << "Configurator::CheckSyntax: Error -- no plot spec found for name: << "  
		    << name << " value: " << value << std::endl;
	  return false;
	}
	
	std::vector<std::string> subs;
	size_t nSubs = Split(value, subs);
	
	if(subs[0] == "EXT") {
	  if(nSubs != 6) {
	    std::cout << "ERROR: Configurator::CheckSyntax: " << value 
		      << "does not have the correct number of fields, should be 6, but is " 
		      << nSubs << std::endl;
	    return false;
	  }
	} else if(subs[0] == "TRP") {
	  if(nSubs != 5) {
	    std::cout << "ERROR: Configurator::CheckSyntax: " << value 
		      << "does not have the correct number of fields, should be 5, but is " 
		      << nSubs << std::endl;
	    return false;
	  }
	  
	  const std::string &L1Name = TPointInfo::GetL1Info().GetName();
	  const std::string &L2Name = TPointInfo::GetL2Info().GetName();
	  const std::string &EFName = TPointInfo::GetEFInfo().GetName();
	  const std::string &HLTName = TPointInfo::GetHLTInfo().GetName();
	  
	  if(subs[1] != L1Name &&
	     subs[1] != L2Name &&
	     subs[1] != EFName &&
	     subs[1] != HLTName ) {
	    std::cout << "ERROR: Configurator::CheckSyntax: " << subs[1] <<
	      " does not correspond to any supported TRP timePoint in " << value << std::endl;
	    return false;
	  }
	} else {
	  std::cout << "ERROR: Configurator::CheckSyntax: invalid prefix for: " << value << std::endl;
	  return false;

	}
	  // Check for a parameter directive
      }  else if( name.substr(0, m_paramPrefix.size()).compare(m_paramPrefix) == 0) {
	
	std::string paramName = name.substr(m_paramPrefix.size());
	if(paramName == "") {
	  std::cout 
	    << "Configurator::CheckSyntax: Error --Attempt to define parameter with no name "
	    << name << std::endl;
	  return false;
	}
	
      }
    }
    return true;
  }
  
  
}
