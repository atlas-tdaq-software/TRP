#include "wtrp/TrigConf.h"

namespace wtrp {
  
  TC_step::TC_step(): m_algorithms(0), m_outputTEs(0) {;}
  
  TC_step::TC_step(const std::vector<std::string> &algorithms, const std::vector<std::string> &outputTEs) : 
    m_algorithms(algorithms), m_outputTEs(outputTEs) {;}
  
  TC_chainInLevel::TC_chainInLevel(const std::string &name, int prescale, int passthrough)
    :m_name(name), m_prescale(prescale), m_passthrough(passthrough) {;}
  
  const TC_chainInLevel &TC_chain::GetChainInLevel(TC_level lvl) const
  {
    if(lvl == TC_L1) return m_L1;
    if(lvl == TC_L2) return m_L2;
    if(lvl == TC_EF) return m_EF;
    return m_HLT;
  }

  TC_chain::TC_chain(const std::string &name, TC_chainInLevel &L1, TC_chainInLevel &L2, TC_chainInLevel &EF)
    : m_name(name), m_L1(L1), m_L2(L2), m_EF(EF), m_HLT("", 0, 0),
      m_chainSpec(L1.GetName(), L2.GetName(), EF.GetName()) 
  {;}

  TC_chain::TC_chain(const std::string &name, TC_chainInLevel &L1, TC_chainInLevel &HLT)
    : m_name(name), m_L1(L1), m_L2("", 0, 0), m_EF("", 0, 0), m_HLT(HLT), m_chainSpec(L1.GetName(), HLT.GetName()) 
  {;}

  void TC_chain::SetChainInLevel(TC_level lvl, TC_chainInLevel &lchain)
  {
    if(lvl == TC_L1) m_L1 = lchain;
    if(lvl == TC_L2) m_L2 = lchain;
    if(lvl == TC_EF) m_EF = lchain;
    m_HLT = lchain;
  }

  void TC_chain::SetLevelPrescale(TC_level lvl, int prescale)
  {

    switch (lvl) {

    case TC_L1:
      m_L1.SetPrescale(prescale);
      break;
      
    case TC_L2:
      m_L2.SetPrescale(prescale);
      break;
      
    case TC_EF:
      m_EF.SetPrescale(prescale);
      break;

    case TC_HLT:
      m_HLT.SetPrescale(prescale);
      break;
    }

  }

  const std::string &TC_chain::GetLevelName(TC_level lvl) const
  {
    if(lvl == TC_HLT)  return m_HLT.GetName();
    if(lvl == TC_EF)   return m_EF.GetName();
    if(lvl == TC_L2)   return m_L2.GetName();
    return m_L1.GetName();
  }

  int TC_chain::GetLevelPrescale(TC_level lvl) const
  {
    if(lvl == TC_HLT) return m_HLT.GetPrescale();
    if(lvl == TC_EF)  return m_EF.GetPrescale();
    if(lvl == TC_L2)  return m_L2.GetPrescale();
    return m_L1.GetPrescale();
  }


  void TC_chain::Dump(bool isRun2) const {
    if(!isRun2) {
      std::cout << "   chain name: " << m_name
		<< " L1 name: " <<   m_L1.GetName()
		<< " L2 name: " <<   m_L2.GetName()
		<< " EF name: " <<   m_EF.GetName()
		<< std::endl;
    } else {
      std::cout << "   chain name: " << m_name
		<< " L1 name: " <<   m_L1.GetName()
		<< " HLT name: " <<   m_HLT.GetName()
		<< std::endl;
    }

    std::set<std::string>::const_iterator sit;

    std::cout << "      Groups: ";
    for(sit = m_groups.begin(); sit != m_groups.end(); sit++)
      std::cout << *sit << ", ";
    std::cout << std::endl;

    std::cout << "      Streams: ";
    for(sit = m_streams.begin(); sit != m_streams.end(); sit++)
      std::cout << *sit << ", ";
    std::cout << std::endl;

  }

  int TC_chain::operator<(const TC_chain &rhs) const
  {
    if(m_chainSpec < rhs.m_chainSpec ) return 1;
    return 0;
  }
  
  TrigConf::TrigConf(){;}
  
  void TrigConf::AddStream(TC_level lvl, const std::string &streamName, const std::vector<std::string> &chainNames)
  {
    m_streams.insert(streamName);

    std::vector<std::string>::const_iterator chit;
    for(chit = chainNames.begin(); chit != chainNames.end(); chit++) {

      bool found = false;
      for(std::set<TC_chain>::const_iterator  mchit = m_chains.begin(); mchit != m_chains.end(); mchit++) {
	if( mchit->GetLevelName(lvl) == *chit) {
	  found = true;
	  TC_chain &chain = (const_cast<TC_chain&>(*mchit)); 
	  chain.AddStream(streamName);
	}
      }
      if(!found) {
	std::cout << "TrigConf warning: no chain corresponding to " 
		  << *chit << " in stream " << streamName << " found" << std::endl;
      }
    }
  }


  void TrigConf::AddGroup(TC_level lvl, const std::string &groupName, const std::vector<std::string> &chainNames)
  {
    m_groups.insert(groupName);

    std::vector<std::string>::const_iterator chit;
    for(chit = chainNames.begin(); chit != chainNames.end(); chit++) {

      bool found = false;
      for(std::set<TC_chain>::const_iterator  mchit = m_chains.begin(); mchit != m_chains.end(); mchit++) {
	if( mchit->GetLevelName(lvl) == *chit) {
	  found = true;
	  TC_chain &chain = (const_cast<TC_chain&>(*mchit)); // I have no idea why this cast is needed!
	  chain.AddGroup(groupName);
	}
      }
      if(!found) {
	std::cout << "TrigConf warning: no chain corresponding to " 
		  << *chit << " in group " << groupName << " found" << std::endl;
      }
    }
  }
  void TrigConf::MakeCh2GrpMaps()
  {
    if(IsRun1Configuration()) {
      m_L2_ch_to_group.clear();
      m_EF_ch_to_group.clear();
    } else {
      m_HLT_ch_to_group.clear();
    }
    std::set<TC_chain>::const_iterator tcit;
    for(tcit = m_chains.begin(); tcit != m_chains.end(); tcit++) {
      const std::set<std::string> groups = tcit->GetGroups();
      std::set<std::string>::iterator grit;
      for(grit = groups.begin(); grit != groups.end(); grit++) {
	if(IsRun1Configuration()) {
	  m_L2_ch_to_group[ tcit->GetChainInLevel(TC_L2).GetName() ].insert(*grit);
	  m_EF_ch_to_group[ tcit->GetChainInLevel(TC_EF).GetName() ].insert(*grit);
	} else {
	  m_HLT_ch_to_group[ tcit->GetChainInLevel(TC_HLT).GetName() ].insert(*grit);
	}
      }
    }
    //Dump(IsRun2Configuration());
  }

  const std::map<std::string, std::set<std::string> > &TrigConf::GetChToGrpMap(TC_level lvl) const {

    if(IsRun1Configuration()) {
      if(lvl == TC_L2)
	return m_L2_ch_to_group;
      return m_EF_ch_to_group;
    }
    return m_HLT_ch_to_group;
  }

  void TrigConf::SetPrescale(TC_level lvl, const std::string &name, int preScale)
  {
    std::vector<TC_chain *> theChains;

    GetMatchingChains(lvl, name, theChains);

    if(theChains.size() == 0 && lvl != TC_L1){
      std::cout << "TrigConf::SetPrescale -- no chains match " << name << " (level=" << lvl << ")" << std::endl;
      return;
    }

    for(std::vector<TC_chain *>::iterator itc = theChains.begin(); itc != theChains.end(); itc++) {
      (*itc)->SetLevelPrescale(lvl, preScale);
    }
  }
  float TrigConf::GetStreamPrescale(const std::string &stream, const std::string &chain) const
  {
    std::map<std::string, float >::const_iterator sp = m_streamPrescales.find(stream + "_" + chain);
    if( sp == m_streamPrescales.end())
      return -1;

    return sp->second;
  }

  TC_chain *TrigConf::GetChain(TC_level lvl, const std::string &levelName)
  {
    std::set<TC_chain>::iterator tcit;
    for(tcit = m_chains.begin(); tcit != m_chains.end(); tcit++) {
      const std::string &name = tcit->GetLevelName(lvl);
      if(name == levelName)
	return const_cast<TC_chain *>(&(*tcit)); // no idea why const_cast is needed
    }
    return 0;
  }

  void TrigConf::GetMatchingChains(TC_level lvl, const std::string &levelName, std::vector<TC_chain *> &chains)
  {
    chains.clear();

    std::set<TC_chain>::iterator tcit;
    for(tcit = m_chains.begin(); tcit != m_chains.end(); tcit++) {
      const std::string &name = tcit->GetLevelName(lvl);
      if(name == levelName)
	chains.push_back( const_cast<TC_chain *>(&(*tcit)) ); // once again, the compiler need const_cast. Why??
    }
  }


  void TrigConf::Dump(bool isRun2)
  {
    std::cout << "******** Begin dump of TrigConf **********" << std::endl;
    std::set<TC_chain>::const_iterator chit;
    for(chit = m_chains.begin(); chit != m_chains.end(); chit++) {
      chit->Dump(isRun2);
    }

    if( !isRun2 ) {
      std::cout << "** L2 chain to group map  **" << std::endl;
      std::map<std::string, std::set<std::string> >::const_iterator cgit;
      for(cgit = m_L2_ch_to_group.begin(); cgit != m_L2_ch_to_group.end(); cgit++) {
	std::cout << cgit->first << ": ";
	for(std::set<std::string>::const_iterator sit = cgit->second.begin(); sit != cgit->second.end(); sit++)
	  std::cout << *sit << " ";
	std::cout << std::endl;
      }

      std::cout << "** EF chain to group map  **" << std::endl;
      for(cgit = m_EF_ch_to_group.begin(); cgit != m_EF_ch_to_group.end(); cgit++) {
	std::cout << cgit->first << ": ";
	for(std::set<std::string>::const_iterator sit = cgit->second.begin(); sit != cgit->second.end(); sit++)
	  std::cout << *sit << " ";
	std::cout << std::endl;
      }
    } else {
      std::cout << "** HLT chain to group map  **" << std::endl;
      std::map<std::string, std::set<std::string> >::const_iterator cgit;
      for(cgit = m_HLT_ch_to_group.begin(); cgit != m_HLT_ch_to_group.end(); cgit++) {
	std::cout << cgit->first << ": ";
	for(std::set<std::string>::const_iterator sit = cgit->second.begin(); sit != cgit->second.end(); sit++)
	  std::cout << *sit << " ";
	std::cout << std::endl;
      }
    }


    std::cout << "******** End dump of TrigConf **********" << std::endl;
  }

} // end namespace wtrp
  /*
  {
  }
[]
  */
