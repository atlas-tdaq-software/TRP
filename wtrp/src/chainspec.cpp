#include <iostream>

#include "wtrp/chainspec.h"

namespace wtrp {

  static const char *L1tags[] = {"MU", "EM", "JE", "J", "FJ", "TAU", "TE", "XE", "CALREQ", 
				 "LUCID", "BCM", "MBTS", "RD0", "EMPTY", ""};

  static const char *L2tags[] = {"MU", "mu", "em", "e", "g", "Zee", "Jpsiee", "JE", "J", 
				 "j", "L1J", "FJ", "tau", "tauNoCut", "te", "xe", "stau", "b", "MbRndm", "MbSp", "MbSpTrk", ""};

  static const char *EFtags[] = {"MU", "mu", "em", "e", "g", "Zee", "Jpsiee", "JE", "J", 
				 "j", "L1J", "FJ", "tau", "tauNoCut", "te", "xe", "stau", "b", "MbRndm", "MbSp", "MbSpTrk", ""};

  static const char *HLTtags[] = {"MU", "mu", "em", "e", "g", "Zee", "Jpsiee", "JE", "J", 
				 "j", "L1J", "FJ", "tau", "tauNoCut", "te", "xe", "stau", "b", "MbRndm", "MbSp", "MbSpTrk", ""};

  static std::map<std::string, unsigned int> InitTagVector(const char *tags[])
  {
    unsigned int n = 0;
    std::map<std::string, unsigned int> stags;
    for( const char **tag = tags; *tag[0] != '\0'; tag++) {
      stags[*tag] = n++;
    }
    stags[""] = n;

    return stags;
  }

  std::map<std::string, unsigned int>  ChainSpec::m_L1tags  = InitTagVector(L1tags);
  std::map<std::string, unsigned int>  ChainSpec::m_L2tags  = InitTagVector(L2tags);
  std::map<std::string, unsigned int>  ChainSpec::m_EFtags  = InitTagVector(EFtags);
  std::map<std::string, unsigned int>  ChainSpec::m_HLTtags = InitTagVector(HLTtags);

  bool ChainSpec::operator< (const ChainSpec& rhs) const
  {
 
    if( m_L1 < rhs.m_L1) {
      if(rhs.m_L1 < m_L1) {
      }
      return true;
    }

    if( !(m_L1 == rhs.m_L1)) {
      return false;
    }

    if(!m_isRun2) {
      if( m_L2 < rhs.m_L2) {
	return true;
      }

      if( !(m_L2 == rhs.m_L2)) {
	return false;
      }

      if( m_EF < rhs.m_EF) {
	return true;
      }

      if( !(m_EF == rhs.m_EF)) {
	return false;
      }
    } else {
      if( m_HLT < rhs.m_HLT) {
	return true;
      }

      if( !(m_HLT == rhs.m_HLT)) {
	return false;
      }
    }

    std::cout << "ChainsSpec: found two equal chains -- shouldn't happen: " << std::endl;
    Dump();
    rhs.Dump();
    return false;
  }
  
  bool ChainSpec::operator==(const ChainSpec &rhs) const
  {
    if( !(m_L1 == rhs.m_L1) )
      return false;

    if(!m_isRun2) {
      if( !(m_L2 == rhs.m_L2) )
	return false;
      
      if( !(m_EF == rhs.m_EF) )
	return false;
    } else {
      if( !(m_HLT == rhs.m_HLT) )
	return false;
    }
    
    std::cout << "ChainsSpec ==: found two equal chains -- shouldn't happen: " << std::endl;
    Dump();
    rhs.Dump();
    return true;
  }

  
  bool ChainSpec::LevelSpec::operator<(const LevelSpec &rhs) const
  {
    if(m_units.size() < rhs.m_units.size())
      return true;

    if(m_units.size() > rhs.m_units.size())
      return false;

    std::vector<UnitSpec>::const_iterator irhs = rhs.m_units.begin();
    for(std::vector<UnitSpec>::const_iterator ilhs = m_units.begin();
	ilhs != m_units.end(); ilhs++, irhs++) {

      if( *ilhs < *irhs)
	return true;
      else if( *ilhs == *irhs)
	continue;
      return false;

    }
    return false;
  }

  bool ChainSpec::LevelSpec::operator==(const LevelSpec &rhs) const
  {
    if(m_units.size() != rhs.m_units.size())
      return false;

    std::vector<UnitSpec>::const_iterator irhs = rhs.m_units.begin();
    for(std::vector<UnitSpec>::const_iterator ilhs = m_units.begin();
	ilhs != m_units.end(); ilhs++, irhs++) {
      if( !( *ilhs == *irhs ) )
	return false;
    }
    return true;
  }

  bool ChainSpec::UnitSpec::operator<(const UnitSpec &rhs) const
  {
    if( *this == rhs)
      return false;

    if(m_tagOrder < rhs.m_tagOrder)
      return true;

    if(m_tagOrder > rhs.m_tagOrder)
      return false;

    if(m_n < rhs.m_n)
      return true;

    if(m_n > rhs.m_n)
      return false;

    if(m_thresh < rhs.m_thresh)
      return true;

    if(m_thresh > rhs.m_thresh)
      return false;

    if(!m_isIsolated && rhs.m_isIsolated)
      return true;

    if(m_isIsolated && !rhs.m_isIsolated)
      return false;

    return m_decorations < rhs.m_decorations;

  }
  bool ChainSpec::UnitSpec::operator==(const UnitSpec &rhs) const
  {

    if(m_n == rhs.m_n && m_tagOrder == rhs.m_tagOrder && m_thresh == rhs.m_thresh 
       && m_isIsolated == rhs.m_isIsolated && m_decorations == rhs.m_decorations)
      return true;

    return false;
  }

  ChainSpec::LevelSpec::LevelSpec(const std::map<std::string, unsigned int> &tags, const std::string &name) : m_name(name)
  {
    if(name.size() < 4) {
      std::map<std::string, unsigned int>::const_iterator nulTag = tags.find("");
      m_units.push_back( UnitSpec(0, "", nulTag->second, 0, false, name) );
      return;
    }

    unsigned int p = 3;

    unsigned int nUnits = 0;
    while(p < name.size() ) { // extract units
      
      // Look for the mulitplicity
      unsigned int mlength = 0;
      unsigned int mult = 0;
      while( (name.substr(p + mlength, 1).c_str()[0] >= '0') && (name.substr(p + mlength, 1).c_str()[0] <= '9') ) {
	mult = (10 * mult) + name.substr(p + mlength, 1).c_str()[0] - '0';
	mlength++;
      }
      p += mlength;
      mult = (mult == 0)? 1 : mult;
      
      // Find the longest matching tag
      unsigned int length = 0;
      const std::string *match = 0;
      for(std::map<std::string, unsigned int>::const_iterator it = tags.begin(); it != tags.end(); it++) {
	if( ( name.substr(p, it->first.size()) == it->first ) && it->first.size() > length) {
	  match = &(it->first);
	  length = it->first.size();
	}
      }

      // Require a tag to be followed either by nothing, "_", or a number
      if(match != 0 && name.size() > p + length) {
	char nextChar = name.substr(p+length, 1).c_str()[0];
	if( (nextChar != '_') && ( (nextChar < '0') || (nextChar > '9') ) ) {
	  match = 0;
	  length = 0;
	}
      }

      if(match == 0) { // add the rest of the string as a decoration to the last unit if it exists, or, create a unit...
	if(nUnits > 0) {
	  m_units[nUnits-1].m_decorations = name.substr(p);
	} else {
	  std::map<std::string, unsigned int>::const_iterator nulTag = tags.find("");
	  m_units.push_back( UnitSpec(mult, "", nulTag->second, 0, false, name));
	}
	break;
      }
      std::map<std::string, unsigned int>::const_iterator iTagOrder = tags.find(*match);
      unsigned int tagOrder = iTagOrder->second;

      p += match->size();
      
      // Look for the threshold
      unsigned int tlength = 0;
      unsigned int thresh = 0;
      while( (name.substr(p + tlength, 1).c_str()[0] >= '0') && (name.substr(p + tlength, 1).c_str()[0] <= '9') ) {
	thresh = (10 * thresh) + name.substr(p + tlength, 1).c_str()[0] - '0';
	tlength++;
      }
      
      p += tlength;
      
      // Isolated?
      bool isolated = false;
      if( ( (name.substr(0,2) == "L1") && name.substr(p,1) == "I") ||
	  ( (name.substr(0,2) != "L1") && name.substr(p,1) == "i") ) {
	isolated = true;
	p++;
      }
      
      m_units.push_back( UnitSpec(mult, *match, tagOrder, thresh, isolated, "") );
      nUnits++;

      if( name.substr(p,1) == "_")
	p++;
    }
    //Dump();
  }

  void ChainSpec::Dump() const {

    std::cout << "ChainSpec dump" << std::endl;

    std::cout << "  Level 1:" << std::endl;
    m_L1.Dump();

    if(!m_isRun2) {
      std::cout << "  Level 2:" << std::endl;
      m_L2.Dump();

      std::cout << "  EF:" << std::endl;
      m_EF.Dump();
    } else {
      std::cout << "  HLT:" << std::endl;
      m_HLT.Dump();
    }
  }
  
  void ChainSpec::LevelSpec::Dump() const
  {
    std::cout << "name: " << m_name << ",  Number of units: " << m_units.size() << std::endl;
    for(std::vector<UnitSpec>::const_iterator iu = m_units.begin(); iu != m_units.end(); iu++)
      iu->Dump();
  }

  void ChainSpec::UnitSpec::Dump() const
  {
    std::cout << "multiplicity: " << m_n << ", tag: " << m_tag << ", m_tagOrder: " << m_tagOrder << "' threshold: " 
	      << m_thresh << ", isolated: " << m_isIsolated << ", decorations: " << m_decorations << std::endl;
  }

}

