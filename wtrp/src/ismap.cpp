#include "wtrp/ismap.h"

#include <iostream>
#include <string>
#include "ipc/partition.h"
#include "ipc/core.h"

#include <is/info.h>
#include <is/infoT.h>
#include <is/inforeceiver.h>
#include "is/infodictionary.h"
#include "is/infoany.h"
#include "is/infodynany.h"
#include "is/type.h"
#include <owl/time.h>


namespace wtrp {


  std::map<std::string, ISMap::MapEntry> ISMap::m_isMap;

  void ISMap::Book(const std::string &partition,
		   const std::string &server,
		   const std::string &object,
		   const std::string &parameter,
		   const std::string &key,
		   bool deletable)
  {

    std::cout << "ISMap::Book: booking parameter with key: " << key << ", partition: " << partition <<
      ", server: " << server << ", object: " << object << ", parameter: " << parameter << std::endl;

    if(m_isMap.find(key) != m_isMap.end()) {
      std::cout << "ISMap::Book - info: " << key << " is already in map" << std::endl;
      return;
    }
    m_isMap[key] = MapEntry(partition, server, object, parameter, key, deletable);
  } 
  
  /** Get all booked parameters from IS */
  bool ISMap::Fetch()
  {
    bool allOkay = true;

    //std::cout << "ISMap::Fetch was called " << std::endl;

    for(std::map<std::string, MapEntry>::iterator ime = m_isMap.begin(); 
	ime != m_isMap.end(); ime++ ) {
      if( !(ime->second).Fetch() ) allOkay = false;
      // std::cout << "ISMAP::Fetch: Fetching " << ime->first << ", allOkay: " << allOkay << std::endl;
    }
    return allOkay;
  }

  float ISMap::Get(const std::string &key)
  {
    //std::cout << "ISMap::Get: looking for " << key << std::endl;
    if( m_isMap.find(key) != m_isMap.end())
      return m_isMap[key].Value();
    std::cout << "ISMap::Get: not found" << std::endl;
    return 0.;
  }

  bool ISMap::isValid(const std::string &key) {
    if( m_isMap.find(key) != m_isMap.end())
      return m_isMap[key].isValid();

    return false;
  }

  bool ISMap::isFresh(const std::string &key) {
    if( m_isMap.find(key) != m_isMap.end())
      return m_isMap[key].isFresh();

    return false;
  }

  void ISMap::ClearMap()
  {

    bool keepGoing = true;
    while( keepGoing ) {

      keepGoing = false;
      
      for(std::map<std::string, MapEntry>::iterator ime = m_isMap.begin();
	  ime != m_isMap.end(); ime++ ) {

	if( ime->second.isDeletable() ) {
	  m_isMap.erase( ime );
	  keepGoing = true;
	  break;
	}
      }
    }
  }

  bool ISMap::MapEntry::Fetch()
  {
    m_isValid = false;
    m_isFresh = false;

    /*
    std::cout << "ISMap::MapEntry: attempting to fetch -- partition: " <<
      m_partition << ", server: " << 
      m_server << ", object: " << 
      m_object << ", parameter: " << 
      m_parameter << ", key: " << 
      m_key <<
      std::endl;
    */

    IPCPartition partition(m_partition);

    if( !partition.isValid() ) {
      //std:: cout << "Partition " << m_partition << " is not valid." << std::endl;
      return false;
    }
    
    ISInfoDynAny da;
    
    std::string servOb = m_server + "." + m_object;

    try {
      ISInfoDictionary dict(m_partition);

      dict.getValue( servOb, da );
      
    }
    catch(daq::is::InvalidName&) {
      //std::cout << "dict.getValue: " << servOb << " is an invalid name" << std::endl;
      return false;
    }
    catch(daq::is::RepositoryNotFound&) {
      //std::cout  << "dict.getValue: " << servOb << " -- Repository not found" << std::endl;
      return false;
    }
    catch(daq::is::InfoNotFound&) {
      //std::cout  << "dict.getValue: " << servOb  << " -- Info not found" << std::endl;
      return false;
    }
    catch(daq::is::InfoNotCompatible&) {
      //std::cout  << "dict.getValue: " << servOb  << " -- Info not compatible" << std::endl;
      return false;
    }
    
    ISType::Basic info_type = ISType::Boolean;
    
    try {
      info_type = da.getAttributeDescription(m_parameter).typeCode();
    }
    catch (daq::is::DocumentNotFound&) {
      std::cout << "ISInfoDictionary::getAttributeDescrtion -- Document not found for " 
		<< m_key << std::endl;
      return false;
    }
    catch (daq::is::AttributeNotFound&) {
      std::cout << "ISInfoDictionary::getAttributeDescrtion -- Attribute not found for " 
		<< m_key << std::endl;
      return false;
    }
    
    
    //std::cout << m_key << " is of Type " << info_type << std::endl;
    
    float float_value = 0.;
    
    switch (info_type) {
      
    case ISType::S32:
      float_value = static_cast<float>( da.getAttributeValue<int>( m_parameter ) );
      break;
      
    case ISType::U32:
      float_value = static_cast<float>( da.getAttributeValue<unsigned int>( m_parameter ) );
      break;
      
    case ISType::S64:
      float_value = static_cast<float>( da.getAttributeValue<int64_t>( m_parameter ) );
      break;
      
    case ISType::U64:
      float_value = static_cast<float>( da.getAttributeValue<uint64_t>( m_parameter ) );
      break;
      
    case ISType::Float:
      float_value = da.getAttributeValue<float>( m_parameter );
      break;
      
    case ISType::Double:
      float_value = static_cast<float>( da.getAttributeValue<double>( m_parameter ) );
      break;

    default:
      std::cout << "Type (" << info_type << ") of " << m_key << " is not supported." << std::endl;
      return false;
    }
    
    //std::cout << "The value of " << m_key << " is " << float_value << std::endl;
  
    m_value = float_value;
    m_isValid = true;

    long long updateTime = da.time().total_mksec_utc();

    //std::cout << "Update time: " << da.time() << ", time diff: " << updateTime - m_updateTime << std::endl;

    if( (updateTime - m_updateTime) > 1)
      m_isFresh = true;
    m_updateTime = updateTime;

    return true;
  }

}
