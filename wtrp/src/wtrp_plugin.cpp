#include "wtrp/wtrp_plugin.h"
#include "wtrp/TrigConf.h"
#include "wtrp/OnlineTools.h"
#include "wtrp/rctlstats.h"
#include "wtrp/runinfo.h"
#include "wtrp/tpointinfo.h"
#include "wtrp/lumi.h"
#include "wtrp/lhcinfo.h"
#include "wtrp/ismap.h"
#include "wtrp/configurator.h"

#include "wmi/utils.h"
#include "wmi/text.h"
#include "wmi/picture.h"

#include "is/infodictionary.h"
#include "ipc/core.h"
#include <utility>

#include <iostream>
#include <sstream>

using namespace daq::wmi;
using namespace wtrp;


wtrpPlugin::wtrpPlugin() : 
  m_isConfigured(false), 
  m_triggerConfiguration(0),
  m_ratePage("totalrates", "Total rates", "Global rates", "index.html"),
  m_streamTable("streams", "Stream rates", "Stream rates", "streams.html"),
  m_groupTable("groups","Group rates", "Group rates", "groups.html"),
  m_l1Table("L1","L1", "L1 rates", "l1.html"),
  m_L1raw("L1raw", "L1 Raw input", "L1raw", "l1raw.html"),
  m_L2raw("L2raw", "L2 Raw input", "L2raw", "l2raw.html"),
  m_EFraw("EFraw", "EF Raw input", "EFraw", "efraw.html"),
  m_HLTraw("HLTraw", "HLT Raw input", "HLTraw", "hltraw.html"),
  m_WTRPstatus("Wstatus", "WTRP status", "WTRP status", "WTRPstatus.html")
{

  m_partitionName  =  "MyTestPartitionMM";
  m_TRPserverName = "UTripServerIS";
  m_trigConDBName = "TRIGGERDB_RUN3";
  m_barFilePath = "/afs/cern.ch/user/m/medinnis/wtrp/wtrp/data/";
  m_barFileName = "blsq.gif";
  m_archivePath = "https://atlas-trigconf.cern.ch/wtrp_archive/";
}

wtrpPlugin::~wtrpPlugin(){
  delete m_outStream;
}

void wtrpPlugin::periodicAction(){

  static bool setL1Prescales = false;

  static bool updateL1Raw = true;


  static bool updateHLTRaw = true;

  if(!m_isConfigured) {
    std::cout << "wtrpPlugin: periodicAction without having been configured" << std::endl;
    return;
  }

  //std::cout << "wtrpPlugin: periodicAction" << std::endl;
  
  
  //IPCCore::init( ac, av );
  
  // Create an instance of the partition

  IPCPartition partition(m_partitionName);
  
  // Create the IS dictionary for the specific partitition
  ISInfoDictionary dict(partition);
  


  unsigned int old_smk = RunInfo::GetSMK();
  unsigned int old_L1_psk = RunInfo::GetL1_PSK(); 
  unsigned int old_HLT_psk = RunInfo::GetHLT_PSK();

  // If this is a test, hardwire the keys
  if(m_partitionName  ==  "MyTestPartitionMM") {
    //RunInfo::SetKeysForTest(375, 540, 401);
    //RunInfo::SetKeysForTest(1, 3, 2); // This is for running on cosmic test data using Takanori's db
    //RunInfo::SetKeysForTest(674, 1020, 962); // was run online in December 2009
    //RunInfo::SetKeysForTest(551, 82, 372); // taken from https://twiki.cern.ch/twiki/bin/viewauth/Atlas/M4Exam    //    RunInfo::SetKeysForTest(1604 , 10847, 8466); // used for online cosmic run 233328
    RunInfo::SetKeysForTest(2013 , 56, 53); // uses TRIGGERDBR2
  }

  // Read information from servers
  //std::cout << "going to update tpoints " << std::endl;
  TPointInfo::Update(dict, (const TrigConf *) m_triggerConfiguration ); 
  wtools::ZeroTAVforDisabledL1Trigs();

  RCtlStats::GetFromServer(dict);
  RunInfo::GetFromServer(partition.isValid(), dict);
  //Lumi::GetFromServer();
  LHCInfo::GetFromServer();
  ISMap::Fetch();

  unsigned int new_smk = RunInfo::GetSMK();
  unsigned int new_L1_psk = RunInfo::GetL1_PSK(); 
  unsigned int new_HLT_psk = RunInfo::GetHLT_PSK();


  /* If keys have changed, fetch a new configuration */

  // Assume this is a run2 type configuration unless the trigger has been configured and says otherwise
  //bool isRun2 = true;
  //if( (m_triggerConfiguration != 0) && m_triggerConfiguration->IsRun1Configuration())
  //  isRun2 = false;

  static bool reconfigure = true, trigConIsConfigured = false;

  if((new_smk != old_smk) || 
     (new_L1_psk != old_L1_psk) || 
     (new_HLT_psk != old_HLT_psk)) 
    {
      reconfigure = true;
      updateL1Raw = true;
      //if(!isRun2) {
      //updateL2Raw = true;
      //updateEFRaw = true;

      //updateHLTRaw = false;
      //} else {
      //updateL2Raw = false;
      //updateEFRaw = false;
      updateHLTRaw = true;
	//}
    }


  // A little test
  /*
  static int iCalls = 0;
  if(++iCalls == 3)
    reconfigure = true;
  */

  bool newConfiguration = false;

  bool rc_L1 = TPointInfo::GetL1Info().IsValid();
  //bool rc_L2 = (isRun2)? false : TPointInfo::GetL2Info().IsValid();
  //bool rc_EF = (isRun2)? false : TPointInfo::GetEFInfo().IsValid();
  //bool rc_HLT = (!isRun2)? false : TPointInfo::GetHLTInfo().IsValid();
  //bool rc_HLT = TPointInfo::GetHLTInfo().IsValid();

  if( RunInfo::ValidKeys() && reconfigure) {

    setL1Prescales = true;

    std::cout << "wtrp: New keys: " << new_smk << ", " << new_L1_psk << ", " << new_HLT_psk << " -- Update trig config" 
	      <<std::endl;

    if( m_triggerConfiguration != 0) {
      delete m_triggerConfiguration;
      m_triggerConfiguration = 0;
    }

    try{ 
      m_triggerConfiguration = new TrigConf();
      wtools::SetConfiguration_DB("TRIGGERDBR2", new_smk, new_L1_psk, new_HLT_psk, m_triggerConfiguration);
      m_triggerConfiguration->MakeCh2GrpMaps();
      reconfigure = false;
      //m_triggerConfiguration->Dump();
    }
    catch(...) {
      std::cout << "wtrp: no trigger configuration information is available" << std::endl;
      delete m_triggerConfiguration;
      m_triggerConfiguration = 0;
    }

    newConfiguration = true;
    trigConIsConfigured = true;
  }

  if(trigConIsConfigured && setL1Prescales && rc_L1) {
    setL1Prescales = wtools::SetL1Prescales( m_triggerConfiguration );
  }

  
  static bool configureNoConfigPages = true; // pages independent of trigger configuration
  if(configureNoConfigPages) {
    //PageData::MakeGlobalRatesTable( isRun2 );
    PageData::MakeGlobalRatesTable( true );
    m_WTRPstatus.MakeTable();
    m_ratePage.MakeTable();
    //m_ratePage.BookPOTs();
    configureNoConfigPages = false;
  }

  // Update the configuration of configurable POTs, if needed */ 
  if (Configurator::UpdateConfiguration() )
    m_ratePage.BookPOTs();

  // configure the streams page if either the raw L2 or raw EF page is valid and it is needed.
  // If only one of the raw pages is valid, keep trying on the next update
  // static bool configureStreams = true; not needed since run2


  // Note that configureStreams = true also triggers reconfiguration of the raw pages
  //if(!isRun2 && (configureStreams || newConfiguration) ) {
  //  ConfigureRun1Pages(newConfiguration, updateL2Raw, updateEFRaw, 
  //		       updateL2CPURaw, updateEFCPURaw);
  //  if(rc_L2 && rc_EF) configureStreams = false;
  //} else if(isRun2 && (configureStreams || newConfiguration) ) {
  ConfigureRun2Pages(newConfiguration, updateHLTRaw) ;
  //if(rc_HLT)
  //  configureStreams = false;
  //}

  if( rc_L1 && updateL1Raw ){
    m_L1raw.MakeTable(TPointInfo::GetL1Info().m_timePoint);
    updateL1Raw = false;
  }


  if( newConfiguration ) {
    BookPOTs();
  }
  
  if (!POTBase::Prepare() ) {
    std::cout << "WTRP -- POT::Prepared failed ==> no POTs" << std::endl;
  }


  POTBase::UpdatePOTs();
  
  PageData::ClearGlobalPreamble();

  //wtools::FillGlobalRatesTable( isRun2 );
  //wtools::TimeStampsToPreamble( isRun2 );
  wtools::FillGlobalRatesTable( true );
  wtools::TimeStampsToPreamble( true );

  //if(!isRun2) {
  //  wtools::FillRawPage(m_L1raw, TPointInfo::GetL1Info().m_timePoint);
  //  wtools::FillRawPage(m_L2raw, TPointInfo::GetL2Info().m_timePoint);
  //  wtools::FillRawPage(m_L2CPUraw, TPointInfo::GetL2_CPUInfo().m_timePoint);
  //  wtools::FillRawPage(m_EFCPUraw, TPointInfo::GetEF_CPUInfo().m_timePoint);
  //} else {
  wtools::FillRawPage(m_L1raw, TPointInfo::GetL1Info().m_timePoint);
  wtools::FillRawPage(m_EFraw, TPointInfo::GetEFInfo().m_timePoint);
  wtools::FillRawPage(m_HLTraw, TPointInfo::GetHLTInfo().m_timePoint);
  //}

  wtools::FillStatusPage(m_WTRPstatus);
  
  if(m_triggerConfiguration != 0) {
    
    //wtools::FillStreamTable(m_streamTable, isRun2);
    wtools::FillStreamTable(m_streamTable, true);
    wtools::FillGroupTable(m_groupTable, (const TrigConf *) m_triggerConfiguration);
    wtools::FillL1Table(m_l1Table);
    wtools::FillChainTables(m_chainTables, m_triggerConfiguration);

    //if(!isRun2)
    //  wtools::FillCPUTable(m_cpuTable);
  }
    
    //m_l1Table.Dump();

    //std::cout << "Outputting wmi pages" << std::endl;

  wtools::OutputWMIPage( m_outStream, &m_ratePage);

  if(m_triggerConfiguration != 0) {
    wtools::OutputWMIPage( m_outStream, &m_streamTable);
    wtools::OutputWMIPage( m_outStream, &m_groupTable);
    //if(!isRun2)
    //  wtools::OutputWMIPage( m_outStream, &m_cpuTable);
    
    for(std::map<std::string, ChainTable>::const_iterator ctit = m_chainTables.begin();
	ctit != m_chainTables.end(); ctit++) {
      wtools::OutputWMIPage(m_outStream, &( (*ctit).second) );
    }
  }



  wtools::OutputWMIPage( m_outStream, &m_l1Table);
  wtools::OutputWMIPage( m_outStream, &m_L1raw);
  
  //if(!isRun2) {
  //  wtools::OutputWMIPage( m_outStream, &m_L2raw);
  //  wtools::OutputWMIPage( m_outStream, &m_EFraw);
  //  wtools::OutputWMIPage( m_outStream, &m_L2CPUraw);
  //  wtools::OutputWMIPage( m_outStream, &m_EFCPUraw);
  //} else {
  wtools::OutputWMIPage( m_outStream, &m_HLTraw);
  //}


  wtools::OutputWMIPage( m_outStream, &m_WTRPstatus);
    
  // this is for the archive
  const std::string dumpPage("globalratedump.html");
  wtools::DumpPOTtoWMIPage(m_outStream, "globalRates_0", dumpPage);
  m_WTRPstatus. AddLocalLink(dumpPage, "Global rates dump" );

  // make sure the black square ends up in the destination directory
  m_outStream->open("blksq.html");
  Picture pix(m_barFilePath + m_barFileName, 1, 1);
  pix.setNeedCopy(true);
  m_outStream->write( pix );
  m_outStream->close();

  POTBase::TidyUp();
}

void wtrpPlugin::ConfigureRun1Pages(bool newConfiguration,
				    bool &updateL2Raw, bool &updateEFRaw, 
				    //bool &updateL2CPURaw, bool &updateEFCPURaw) 
				    bool &, bool &) 
{
  
  bool rc_L2 = TPointInfo::GetL2Info().IsValid();
  bool rc_EF = TPointInfo::GetEFInfo().IsValid();
  //bool rc_L2CPU = TPointInfo::GetL2_CPUInfo().IsValid();
  //bool rc_EFCPU = TPointInfo::GetEF_CPUInfo().IsValid();

  std::set<std::string> L2streamNames;
  std::set<std::string> EFstreamNames;
    


  if( rc_L2 ) {
    TimePoint_IS &L2 = TPointInfo::GetL2Info().m_timePoint;
    wtools::ExtractStreamNames(L2, L2streamNames);
  }
    
  if( rc_EF ) {
    TimePoint_IS &EF = TPointInfo::GetEFInfo().m_timePoint;
    wtools::ExtractStreamNames(EF, EFstreamNames);
  }

  m_streamTable.MakeTable(L2streamNames, EFstreamNames);
  m_streamTable.BookPOTs(L2streamNames, EFstreamNames);

  if(newConfiguration) {
    m_groupTable.MakeTable( m_triggerConfiguration );
    ChainTable::MakeTableSet( m_chainTables, m_triggerConfiguration);
    //m_cpuTable.MakeTable(m_triggerConfiguration);
  }
  
  if( rc_L2 && updateL2Raw ) {
    m_L2raw.MakeTable(TPointInfo::GetL2Info().m_timePoint);
    updateL2Raw = false;
  }

  if( rc_EF && updateEFRaw ) {
    m_EFraw.MakeTable(TPointInfo::GetEFInfo().m_timePoint);
    updateEFRaw = false;
  }

  //if( rc_L2CPU && updateL2CPURaw ) {
  //  m_L2CPUraw.MakeTable(TPointInfo::GetL2_CPUInfo().m_timePoint);
  //  updateL2CPURaw = false;
  //}

  //if( rc_EFCPU && updateEFCPURaw ) {
  //  m_EFCPUraw.MakeTable(TPointInfo::GetEF_CPUInfo().m_timePoint);
  //  updateEFCPURaw = false;
  // }

}

void wtrpPlugin::ConfigureRun2Pages(bool newConfiguration,
				    bool &updateHLTRaw) 
{
  std::set<std::string> HLTstreamNames;
    

  bool rc_HLT = TPointInfo::GetHLTInfo().IsValid();

  //std::cout << "configuring for run 2, newcon, rc_HLT: " << newConfiguration << ", " << rc_HLT << std::endl;

  if(rc_HLT) {
    TimePoint_IS &HLT = TPointInfo::GetHLTInfo().m_timePoint;
    wtools::ExtractStreamNames(HLT, HLTstreamNames);
    m_streamTable.MakeTable(HLTstreamNames);
    m_streamTable.BookPOTs(HLTstreamNames);
  }

  if(newConfiguration) {
    //std::cout << "configuring group and chain tables for run 2" << std::endl;
    m_groupTable.MakeTable( m_triggerConfiguration );
    ChainTable::MakeTableSet( m_chainTables, m_triggerConfiguration);
    //m_cpuTable.MakeTable(m_triggerConfiguration);
  }
  

  if(rc_HLT) {
    m_HLTraw.MakeTable(TPointInfo::GetHLTInfo().m_timePoint);
    updateHLTRaw = false;
  }

}




void wtrpPlugin::stop(){
  std::cout << "stop was called" << std::endl;
  POTBase::TidyUp(true);
}

void wtrpPlugin::configure(const ::wmi::InfoParameter& params){

  /* Note that POT configurations are decoded by Configurator::UpdateConfiguration, not here. */

  for( unsigned int i =0; i< params.length();i++){
    ::wmi::Parameter par = params[i];
    std::string st_name = std::string(par.name);
    std::string st_value = std::string(par.value);

    if(st_name.compare("partitionName")==0){
      m_partitionName = st_value.data();
    } else if( st_name.compare("TRPserverName")==0){
      m_TRPserverName = st_value.data();
    } else if( st_name.compare("L1infoName")==0){
      TPointInfo::GetL1Info().SetName( st_value.data() );
    } else if( st_name.compare("L2infoName")==0){
      TPointInfo::GetL2Info().SetName( st_value.data() );
    } else if( st_name.compare("EFinfoName")==0){
      TPointInfo::GetEFInfo().SetName( st_value.data() );
    } else if( st_name.compare("HLTinfoName")==0){
      TPointInfo::GetHLTInfo().SetName( st_value.data() );
      //} else if( st_name.compare("L2CPUName")==0){
      //TPointInfo::GetL2_CPUInfo().SetName( st_value.data() );
      //} else if( st_name.compare("EFCPUName")==0){
      //TPointInfo::GetEF_CPUInfo().SetName( st_value.data() );
    } else if( st_name.compare("barFilePath")==0){
      m_barFilePath = st_value.data();
    } else if( st_name.compare("barFileName")==0){
      m_barFileName = st_value.data();
    } else if( st_name.compare("archivePath")==0) {
      m_archivePath = st_value.data();
    } else if( st_name.compare("configPlotFile")==0) {
      Configurator::SetConfigFile( st_value.data() );
    }
	       
  }
  wtools::SetBarFile(m_barFileName);
  m_ratePage.SetArchivePath(m_archivePath);
  m_isConfigured = true;
}

void wtrpPlugin::BookPOTs()
{
  //POT::ClearPOTs();
  
  std::cout << "Booking POTs" << std::endl;
  
  m_groupTable.BookPOTs(m_triggerConfiguration);
  //m_l1Table.BookPOTs();
  //m_cpuTable.BookPOTs();
}
