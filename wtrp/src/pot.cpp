
#include "wtrp/pot.h"
#include "wtrp/tpointinfo.h"
#include "wtrp/rctlstats.h"
#include "wtrp/lumi.h"
#include "wtrp/ismap.h"
#include "wtrp/configurator.h"
#include <stdio.h>
#include <sys/time.h>

#include <TError.h>

#include <set>
#include <limits>
#include <sstream>


namespace wtrp {

  const bool isATest = false;

  // static variables:
  const double POT::m_upperLimit = 5.e7; //no values above this get into plots (can be overridden, see code) 

  static std::vector<std::string> InitPD_source() {
    std::vector< std::string > source;
    source.push_back("no source");
    source.push_back("TRP_TP L1");
    source.push_back("TRP_TP L2");
    source.push_back("TRP_TP EF");
    source.push_back("TRP_TP HLT");
    source.push_back("Run control");
    source.push_back("TRP_TP L2CPU");
    source.push_back("TRP_TP EFCPU");
    source.push_back("LUMI");
    source.push_back("ISMap");

    source.push_back("Bad source spec");

    return source;
  }

  const std::vector<std::string> POT::m_PD_source = InitPD_source();

  const std::string &POT::GetPD_source( enum PD_level lev)
  {
    return m_PD_source[lev];
  }



  POT *POT::BookPOT(const std::string &name, const std::string &title)
  {
    POTBase *thePOTBase = GetPOT(name);

    if(thePOTBase != 0) {
      POT *thePOT = dynamic_cast<POT*>(thePOTBase);
      if( thePOT != 0)
	return thePOT;
      else {
	std::cout << "BookPOT: attempt to book a POT with same name as an existing on of a different type" << std::endl;
	return 0;
      }
    }

    POT *thePOT = new POT(name, title);
    return thePOT;

  }

  POT::POT(const std::string &name, const std::string &title) :
    POTBase(name, title), m_isLogPlot(false), m_noDelete(false), 
    m_fixedYMin(std::numeric_limits<float>::max()), 
    m_fixedYMax(std::numeric_limits<float>::max()), 
    m_forceOutput(false) {

  }

  void POT::AddParameter(enum PD_level level, const std::string &xlabel, const std::string &ylabel, const std::string &legendName){


    // If it is already booked, don't do anything except for setting the legend name
    for( std::vector<ParDef>::iterator ip =  m_parDefs.begin(); ip != m_parDefs.end(); ip++) {
      if( (level == ip->level) && (xlabel == ip->xlabel) && (ylabel == ip->ylabel) ) {
	ip->legendName = legendName;
	return;
      }
      // Don't allow two parameters with the same legend Name
      if(legendName == ip->legendName) {
	std::cout << "POT::AddParameter: WARNING -- attempt to add a parameter with the same "
		  << "legend name as a previous one. Legend name: " << legendName << std::endl;
	return;
      }
    }

    ParDef pd;
    pd.level = level;
    pd.xlabel = xlabel;
    pd.ylabel = ylabel;

    pd.ldenom = PD_none;
    pd.xdenom = "";
    pd.ydenom = "";
    pd.setFun = 0;

    pd.legendName = legendName;

    pd.nConsecutiveInvalids = 0;
    pd.transPoint = 0.;
    pd.nValidTPoints = 0;
    pd.optsMask = 0;

    // synchronize with m_timeStamps
    for( unsigned int i = 0; i < m_timeStamps_s.size(); i++)
      pd.data_s.push_back( std::numeric_limits<float>::max() ); 

    for( unsigned int i = 0; i < m_timeStamps_l.size(); i++)
      pd.data_l.push_back( std::numeric_limits<float>::max() ); 

    m_parDefs.push_back(pd);
    
  }

  void POT::AddNormalizedParameter(
				   enum PD_level lNumer, const std::string &xNumer, 
				   const std::string &yNumer,
				   enum PD_level lDenom, const std::string &xDenom, 
				   const std::string &yDenom, 
				   const std::string &legendName){

    // If it is already booked, don't do anything
    for( std::vector<ParDef>::const_iterator ip =  m_parDefs.begin(); ip != m_parDefs.end(); ip++)
      if( (lNumer == ip->level)  && (xNumer == ip->xlabel) && (yNumer == ip->ylabel)  &&
	  (lDenom == ip->ldenom) && (xDenom == ip->xdenom) && (yDenom == ip->ydenom) )
	return;

    ParDef pd;
    pd.level = lNumer;
    pd.xlabel = xNumer;
    pd.ylabel = yNumer;

    pd.ldenom = lDenom;
    pd.xdenom = xDenom;
    pd.ydenom = yDenom;
    pd.setFun = 0;

    pd.legendName = legendName;

    pd.nConsecutiveInvalids = 0;
    pd.transPoint = 0.;
    pd.nValidTPoints = 0;
    pd.optsMask = 0;

    // synchronize with m_timeStamps
    for(unsigned int i = 0; i < m_timeStamps_s.size(); i++)
      pd.data_s.push_back(std::numeric_limits<float>::max() );

    for(unsigned int i = 0; i < m_timeStamps_l.size(); i++)
      pd.data_l.push_back(std::numeric_limits<float>::max() );

    m_parDefs.push_back(pd);

  }
  
  void POT::DeleteParameter(enum PD_level lNumer, const std::string &xNumer, const std::string &yNumer, 
			    enum PD_level lDenom, const std::string &xDenom, const std::string &yDenom )
  {
    // Find and erase
    for( std::vector<ParDef>::iterator ip =  m_parDefs.begin(); ip != m_parDefs.end(); ip++)
      if( lNumer == ip->level  && 
	  xNumer == ip->xlabel &&
	  yNumer == ip->ylabel &&
	  lDenom == ip->ldenom &&
	  xDenom == ip->xdenom &&
	  yDenom == ip->ydenom) {
	m_parDefs.erase(ip);
	break;
      }
  }


  void POT::SetOpt(const std::string &legendName, PD_opt opt)
  {
    for( std::vector<ParDef>::iterator ip =  m_parDefs.begin(); ip != m_parDefs.end(); ip++) {
      if( ip->legendName == legendName){
	ip->optsMask |= opt;
	return;
      }
    }
    std::cout << "POT::SetOpt warning: " << legendName << " not found" << std::endl;
  }


  void POT::Update( bool updateLong ) { // return true if a cleanup should be made

    //static int nUpdates = 0;

    TPointInfo &L1_tpi    = TPointInfo::GetL1Info();
    TPointInfo &L2_tpi    = TPointInfo::GetL2Info();
    TPointInfo &EF_tpi    = TPointInfo::GetEFInfo();
    TPointInfo &HLT_tpi   = TPointInfo::GetHLTInfo();
    TPointInfo &L2CPU_tpi = TPointInfo::GetL2_CPUInfo();
    TPointInfo &EFCPU_tpi = TPointInfo::GetEF_CPUInfo();

    TimePoint_IS &L1_tp    = L1_tpi.m_timePoint;
    TimePoint_IS &L2_tp    = L2_tpi.m_timePoint;
    TimePoint_IS &EF_tp    = EF_tpi.m_timePoint;
    TimePoint_IS &HLT_tp   = HLT_tpi.m_timePoint;
    TimePoint_IS &L2CPU_tp = L2CPU_tpi.m_timePoint;
    TimePoint_IS &EFCPU_tp = EFCPU_tpi.m_timePoint;

    bool L1valid    = L1_tpi.IsValid();
    bool L2valid    = L2_tpi.IsValid();
    bool EFvalid    = EF_tpi.IsValid();
    bool HLTvalid   = HLT_tpi.IsValid();
    bool L2CPUvalid = L2CPU_tpi.IsValid();
    bool EFCPUvalid = EFCPU_tpi.IsValid();

    bool scheduleCleanup = false;

    // overrid the upperLimit if one is given in the plot configuration file
    std::string sUpperLimit = Configurator::GetParam("UPPER_PLOT_LIMIT");
    float upperLimit = m_upperLimit;
    if(sUpperLimit != "") {
      std::stringstream ss(sUpperLimit);
      ss >> upperLimit;
      if(ss.fail()) upperLimit = m_upperLimit;
    } 

    int id_count = 0;
    for(std::vector<ParDef>::iterator id = m_parDefs.begin(); id != m_parDefs.end(); id++){ // param loop
      float theVal = std::numeric_limits<float>::max();
      id_count++;
      switch (id->level) {
	
      case PD_L1:
	if(!L1valid) break;
	if( !L1_tp.get(id->xlabel, id->ylabel, theVal) ){
	  std::stringstream mpss;
	  mpss  << " (" << id->level << ", " << id->xlabel << ", " << id->ylabel << ") " << std::endl;
	  POTBase::GetMissingParameters() += mpss.str();
	  break;
	}
	break;
	
      case PD_L2:
	if(!L2valid) break;
	if( !L2_tp.get(id->xlabel, id->ylabel, theVal) ){
	  std::stringstream mpss;
	  mpss  << " (" << id->level << ", " << id->xlabel << ", " << id->ylabel << ") " << std::endl;
	  POTBase::GetMissingParameters() += mpss.str();
	  break;
	}
	break;
	
      case PD_EF:
	if(!EFvalid) break;
	if( !EF_tp.get(id->xlabel, id->ylabel, theVal) ){
	  std::stringstream mpss;
	  mpss  << " (" << id->level << ", " << id->xlabel << ", " << id->ylabel << ") " << std::endl;
	  POTBase::GetMissingParameters() += mpss.str();
	  break;
	}
	break;
	
      case PD_HLT:
	if(!HLTvalid) break;

	if( !HLT_tp.get(id->xlabel, id->ylabel, theVal) ){
	  std::stringstream mpss;
	  mpss  << " (" << id->level << ", " << id->xlabel << ", " << id->ylabel << ") " << std::endl;
	  POTBase::GetMissingParameters() += mpss.str();
	  break;
	}
	break;
	
      case PD_RCTL:
	double dVal;
	if( !RCtlStats::isFresh(id->xlabel) ) break;
	if( !RCtlStats::GetVar(id->xlabel, id->ylabel, dVal) ) {
	  std::stringstream mpss;
	  mpss  << " (" << id->level << ", " << id->xlabel << ", " << id->ylabel << ") " << std::endl;
	  POTBase::GetMissingParameters() += mpss.str();
	  break;
	}
	theVal = float(dVal);
	break;
	
      case PD_L2CPU:
	if(!L2CPUvalid) break;
	if( !L2CPU_tp.get(id->xlabel, id->ylabel, theVal) ){
	  std::stringstream mpss;
	  mpss  << " (" << id->level << ", " << id->xlabel << ", " << id->ylabel << ") " << std::endl;
	  POTBase::GetMissingParameters() += mpss.str();
	  break;
	}
	break;
	
      case PD_EFCPU:
	if(!EFCPUvalid) break;
	if( !EFCPU_tp.get(id->xlabel, id->ylabel, theVal) ){
	  std::stringstream mpss;
	  mpss  << " (" << id->level << ", " << id->xlabel << ", " << id->ylabel << ") " << std::endl;
	  POTBase::GetMissingParameters() += mpss.str();
	  break;
	}
	break;
	
      case PD_LUMI:
	if( !Lumi::GetInteractionRate(theVal)) {
	    theVal = 0.;
	    break;
	}
	break;

      case PD_ISMAP:
	if( ISMap::isFresh(id->xlabel))
	  theVal = ISMap::Get(id->xlabel);

	break;

      default:
	std::cout << "UpdatePOTs WARNING: no level assigned to " 
		  << id->xlabel << ", " << id->ylabel << std::endl;
	break;
	
      }
      if(isATest) {
	theVal = 100 * (id_count+1);
      }
      float theDenom = std::numeric_limits<float>::max();
      
      if(theVal != std::numeric_limits<float>::max() ) {
	switch (id->ldenom) {
	  
	case PD_none:
	  break;
	  
	case PD_L1:
	  if(!L1valid) break;
	  if( !L1_tp.get(id->xdenom, id->ydenom, theDenom) ){
	    std::stringstream mpss;
	    mpss  << " (" << id->ldenom << ", " << id->xdenom << ", " << id->ydenom << ") " << std::endl;
	    POTBase::GetMissingParameters() += mpss.str();
	    theVal = std::numeric_limits<float>::max();
	    break;
	  }
	  theVal = (theDenom > .0000001)? theVal/theDenom : 0.;
	  break;
	  
	case PD_L2:
	  if(!L2valid) break;
	  if( !L2_tp.get(id->xdenom, id->ydenom, theDenom) ){
	    std::stringstream mpss;
	    mpss  << " (" << id->ldenom << ", " << id->xdenom << ", " << id->ydenom << ") " << std::endl;
	    POTBase::GetMissingParameters() += mpss.str();
	    theVal = std::numeric_limits<float>::max();
	    break;
	  }
	  theVal = (theDenom > .0000001)? theVal/theDenom : 0.;
	  break;
	  
	case PD_EF:
	  if(!EFvalid) break;
	  if( !EF_tp.get(id->xdenom, id->ydenom, theDenom) ){
	    std::stringstream mpss;
	    mpss  << " (" << id->ldenom << ", " << id->xdenom << ", " << id->ydenom << ") " << std::endl;
	    POTBase::GetMissingParameters() += mpss.str();
	    theVal = std::numeric_limits<float>::max();
	    break;
	  }
	  theVal = (theDenom > .0000001 )? theVal/theDenom : 0.;
	  break;
	  
	case PD_HLT:
	  if(!HLTvalid) break;
	  if( !HLT_tp.get(id->xdenom, id->ydenom, theDenom) ){
	    std::stringstream mpss;
	    mpss  << " (" << id->ldenom << ", " << id->xdenom << ", " << id->ydenom << ") " << std::endl;
	    POTBase::GetMissingParameters() += mpss.str();
	    theVal = std::numeric_limits<float>::max();
	    break;
	  }
	  theVal = (theDenom > .0000001 )? theVal/theDenom : 0.;
	  break;
	  
	case PD_RCTL:
	  double dDenom;
	  if( !RCtlStats::GetVar(id->ydenom, id->xdenom, dDenom) ) {
	    std::stringstream mpss;
	    mpss  << " (" << id->ldenom << ", " << id->xdenom << ", " << id->ydenom << ") " << std::endl;
	    POTBase::GetMissingParameters() += mpss.str();
	    theVal = 0.;
	    break;
	  }
	  theVal = (theDenom > .0000001)? theVal/float(theDenom) : 0.;
	  break;

	case PD_L2CPU:
	  if(!L2CPUvalid) break;
	  if( !L2CPU_tp.get(id->xdenom, id->ydenom, theDenom) ){
	    std::stringstream mpss;
	    mpss  << " (" << id->ldenom << ", " << id->xdenom << ", " << id->ydenom << ") " << std::endl;
	    POTBase::GetMissingParameters() += mpss.str();
	    theVal = std::numeric_limits<float>::max();
	    break;
	  }
	  theVal = (theDenom > .0000001)? theVal/theDenom : 0.;
	  break;
	  
	case PD_EFCPU:
	  if(!EFCPUvalid) break;
	  if( !EFCPU_tp.get(id->xdenom, id->ydenom, theDenom) ){
	    std::stringstream mpss;
	    mpss  << " (" << id->ldenom << ", " << id->xdenom << ", " << id->ydenom << ") " << std::endl;
	    POTBase::GetMissingParameters() += mpss.str();
	    theVal = std::numeric_limits<float>::max();
	    break;
	  }
	  theVal = (theDenom > .0000001 )? theVal/theDenom : 0.;
	  break;

	case PD_LUMI:
	  if( !Lumi::GetInteractionRate(theDenom)) {
	    theVal = std::numeric_limits<float>::max();
	  } else {
	    theVal = (theDenom > .0000001)? theVal/float(theDenom) : 0.;
	  }
	  break;


	case PD_ISMAP:
	  if( ISMap::isFresh(id->xdenom))
	    theVal = ISMap::Get(id->xdenom);
	  break;


	}
      }

      if(isATest)
	theVal = 100 * id_count+1;

      /*
      if(id->level == PD_LUMI)
	std::cout << "theVal " << id->xlabel << ", " << id->ylabel << ", " << theVal << std::endl;
      */

      if(theVal == std::numeric_limits<float>::max() ) {
	id->nConsecutiveInvalids++;
	if( (id->nConsecutiveInvalids >= m_length_l * m_pointsToAve) && !m_noDelete)
	  scheduleCleanup = true;
      } else
	id->nConsecutiveInvalids = 0;
      
      if(theVal != std::numeric_limits<float>::max() && theVal > upperLimit )
	theVal = upperLimit;

      if(m_isLogPlot && theVal < 0.) {
	
	if(m_fixedYMin != std::numeric_limits<float>::max())
	  theVal = m_fixedYMin * 0.5;
	else
	  theVal = .000001;
	
      }

      if(id->level == PD_LUMI)
	std::cout << "theVal " << id->xlabel << ", " << id->ylabel << ", " << theVal << std::endl;

      id->data_s.push_back(theVal);
      if( id->data_s.size() > m_length_s)
	id->data_s.pop_front();
      
      if(theVal != std::numeric_limits<float>::max()) {
	id->transPoint += theVal;
	id->nValidTPoints++;
      }
      if(updateLong) {
	if(id->nValidTPoints > 0) {
	  id->transPoint /= id->nValidTPoints;
	  id->data_l.push_back(id->transPoint);
	} else
	  id->data_l.push_back( std::numeric_limits<float>::max() );
	
	id->transPoint = 0;
	id->nValidTPoints = 0;
	
	if( id->data_l.size() > m_length_l)
	  id->data_l.pop_front();
      }
      
    }
    if(scheduleCleanup) {
      std::vector<ParDef>::iterator id = m_parDefs.begin();
      while(true) {
	for( id = m_parDefs.begin(); id != m_parDefs.end(); id++){
	  if(id->nConsecutiveInvalids >= (m_length_l * m_pointsToAve) && !m_noDelete) {
	    m_parDefs.erase(id);
	    break;
	  }
	}
	if( id == m_parDefs.end())
	  break;
      }
    }
  }


  void POT::Dump() const
  {
    std::cout << "POT type: POT, number of parameters: " << m_parDefs.size()
	      << std::endl;

    DumpBase();

    for(std::vector<ParDef>::const_iterator id =  m_parDefs.begin(); id != m_parDefs.end(); id++){
      std::cout << "Parameter level, x, y: " << id->level << ", " << id->xlabel << ", " << id->ylabel;
      if(id->ldenom != PD_none)
	std::cout << ", Denominator level, x, y: " << id->ldenom << ", " << id->xdenom << ", " << id->ydenom;
      std::cout << std::endl;

      std::cout << "  Number of data points (short term): " << id->data_s.size() << ", the data: " << std::endl;

      int nd_s = 0;
      for(std::deque<float>::const_iterator ip =  id->data_s.begin(); ip != id->data_s.end(); ip++){
	std::cout << *ip << ", ";
	if( (nd_s++ % 10) == 0) std::cout << std::endl;
      }
      std::cout << std::endl;

      std::cout << "  Number of data points (long term): " << id->data_l.size() << ", the data: " << std::endl;

      int nd_l = 0;
      for(std::deque<float>::const_iterator ip =  id->data_l.begin(); ip != id->data_l.end(); ip++){
	std::cout << *ip << ", ";
	if( (nd_l++ % 10) == 0) std::cout << std::endl;
      }
      std::cout << std::endl;

    }

  }

  void POT::MakeGraphs( std::vector<GraphStuff> &graphStuff ) const
  {
    std::vector<float> xvals_s, xvals_l;
    std::vector<float> yvals_s, yvals_l;

    float yThresh = (m_fixedYMin != std::numeric_limits<float>::max())? m_fixedYMin : .0000001;

    for( std::deque<long>::const_iterator its_s = m_timeStamps_s.begin(); its_s != m_timeStamps_s.end(); its_s++)
      xvals_s.push_back( *its_s);

    for( std::deque<long>::const_iterator its_l = m_timeStamps_l.begin(); its_l != m_timeStamps_l.end(); its_l++)
      xvals_l.push_back( *its_l);

    for(std::vector<ParDef>::const_iterator itpar = m_parDefs.begin(); itpar != m_parDefs.end(); itpar++) {
      // loop over parameters

      float ymin = std::numeric_limits<float>::max(), ymax = std::numeric_limits<float>::min();
      
      bool isNotEmpty_s = false;
      
      yvals_s.clear();
      for(std::deque<float>::const_iterator ip =  itpar->data_s.begin(); ip != itpar->data_s.end(); ip++){
	
	float yval = *ip;
	if(yval == std::numeric_limits<float>::max() )
	  yval = 0.;
	
	if(yval > yThresh)
	  isNotEmpty_s = true;
	
	yvals_s.push_back(yval);
	

	ymin = (yval < ymin)? yval : ymin;
	ymax = (yval > ymax)? yval : ymax;

      }
      
      bool isNotEmpty_l = false;
      
      yvals_l.clear();
      for(std::deque<float>::const_iterator ip =  itpar->data_l.begin(); ip != itpar->data_l.end(); ip++){
	
	float yval = *ip;
	if(yval == std::numeric_limits<float>::max() )
	  yval = 0.;
	
	if(yval > yThresh )
	  isNotEmpty_l = true;
	
	yvals_l.push_back(yval);
	
	ymin = (yval < ymin)? yval : ymin;
	ymax = (yval > ymax)? yval : ymax;
	
      }
      
      if( m_fixedYMin != std::numeric_limits<float>::max())
	ymin = m_fixedYMin;
	
      if( m_fixedYMax != std::numeric_limits<float>::max())
	ymax = m_fixedYMax;
	
      if(isNotEmpty_s || isNotEmpty_l || (itpar->optsMask & PD_forceOut) ) {
	
	GraphStuff gs;
	gs.theDotGraph_s = 0;
	gs.theDotGraph_l = 0;

	if(yvals_s.size() > 0) {
	  float *x_s = &(xvals_s.front());
	  float *y_s = &(yvals_s.front());
	  gs.theGraph_s = new TGraph(yvals_s.size(), x_s, y_s);
	} else
	  gs.theGraph_s = 0;
	
	if(yvals_l.size() > 0) {
	  float *x_l = &(xvals_l.front());
	  float *y_l = &(yvals_l.front());
	  gs.theGraph_l = new TGraph(yvals_l.size(), x_l, y_l);
	} else
	  gs.theGraph_l = 0;
	
	gs.ymin = ymin;
	gs.ymax = ymax;
	gs.legEntry = itpar->legendName;
	
	graphStuff.push_back(gs);
      }
    }
  }
  
  bool POT::GetPOTPlots(std::vector<PlotStuff> &plotStuff) const
  {

    plotStuff.clear();

    if(m_parDefs.size()== 0) {
      //std::cout << "no parameters" << std::endl;
      return false;
    }

    if(m_timeStamps_s.size() == 0 && m_timeStamps_l.size() == 0) {
      std::cout << "no time stamps" << std::endl;
      return false;
    }

    if(m_tmpPath == "") {
      std::cout << "no temporary storage" << std::endl;
      return false;
    }

    unsigned int plotNum = 0;
    //std::cout << "outputting plots " << m_name << std::endl;

    std::vector<GraphStuff> theGraphStuff;

    MakeGraphs( theGraphStuff );


    unsigned int nPlots = (theGraphStuff.size() == 0)? 0 : (theGraphStuff.size() - 1)/ m_maxParsPerPlot + 1;
 
    PlotStuff aPotPlot;
    std::string fileName = "";

    std::vector<GraphStuff>::iterator ig = theGraphStuff.begin();

    std::vector<GraphStuff>::iterator igl = theGraphStuff.end();
    if(theGraphStuff.size() > m_maxParsPerPlot)
      igl = ig + m_maxParsPerPlot;

    
    while(true) {
      // loop over graphStuff
    
      PrintAndReset(ig, igl, nPlots, plotNum, aPotPlot);

      aPotPlot.isVisible = m_isVisible;
      plotStuff.push_back(aPotPlot);

      plotNum++;
      
      if(igl == theGraphStuff.end())
	break;
      
      ig = igl;
      
      if( static_cast<unsigned int>(theGraphStuff.end() - igl) > m_maxParsPerPlot)
	igl += m_maxParsPerPlot;
      else
	igl = theGraphStuff.end();
      
    }
    return true;
  }
      


  bool POT::PrintAndReset(std::vector<GraphStuff>::iterator &ig, 
			  std::vector<GraphStuff>::iterator &igl, 
			  unsigned int nPlots, unsigned int plotNum, PlotStuff &aPotPlot) const
  {
    TCanvas *can = 0;
    TPad *hisPad_s = 0;
    TPad *hisPad_l = 0;
    TH1F *h_pot_s = 0;
    TH1F *h_pot_l = 0;
    TPaveLabel *lab = 0;
    TLegend *leg = 0;
    
    std::string caption = "";

    float xmin_s = m_timeStamps_s.front();
    float xmax_s = m_timeStamps_s.back();
 
    float xmin_l = xmin_s, xmax_l = xmax_s;
    if(m_timeStamps_l.size() > 0) {
      xmin_l = m_timeStamps_l.front();
      xmax_l = m_timeStamps_l.back();
    }

    // used for setting plot limits 
    xmin_l -= 1.;
    xmax_l += 1.;
    xmin_s -= 1.;
    xmax_s += 1.;

    std::stringstream sPlotNum;
    sPlotNum << plotNum;
	
    caption = "<p>" + m_title;
    std::string title = m_title;

    if(nPlots > 1) {
      caption += "(" + sPlotNum.str() + ")";
      title += " (" + sPlotNum.str() + ")";
    }
    caption += ": ";
	
    unsigned int nParsThisPlot = ((igl - ig) != 0)? igl - ig : 1 ;

    int legHeight = ( (nParsThisPlot - 1) /m_nLegendColumns + 1) * m_legendRowHeight;
    int canHeight = m_titleHeight + m_hisHeight + legHeight;
	
    aPotPlot.width = m_canWidth;
    aPotPlot.height = canHeight;

    //float titleFrac = static_cast<float>(m_titleHeight)/canHeight;
    double legFrac = static_cast<double>(legHeight) / (m_hisHeight);
    double hisFrac = static_cast<double>(canHeight-m_titleHeight)/canHeight;
    
    m_potStyle_s->SetPadBottomMargin( legFrac + double(m_hisBottomMarg) / double(m_hisHeight));
    m_potStyle_s->SetLabelSize(double(m_axisLabelSize)/double(m_hisHeight), "X");
    m_potStyle_s->SetLabelSize(0, "Y");
    
    m_potStyle_l->SetPadBottomMargin( legFrac + double(m_hisBottomMarg) / double(m_hisHeight));
    m_potStyle_l->SetLabelSize(double(m_axisLabelSize)/double(m_hisHeight), "X");
    m_potStyle_l->SetLabelSize(double(m_axisLabelSize)/double(m_hisHeight), "Y");
    
    gROOT->SetStyle("PotStyle_s");
    
    std::string canName = "can_" + m_name + sPlotNum.str();
    can = new TCanvas(canName.c_str() , m_title.c_str(), 
		      m_canWidth + m_canWidthDiff, canHeight + m_canHeightDiff);

    lab = new TPaveLabel(0.0, hisFrac, 1.0, 1.0, title.c_str());
    lab->SetBorderSize(0);
    lab->SetFillColor(0);
    
    can->cd();
    leg = new TLegend(0., 0., 1., legFrac, "", "NDC");
    leg->SetNColumns(m_nLegendColumns);
    leg->SetBorderSize(0);
    leg->SetFillColor(0);
    

    // add entries to the legend and caption
    for(std::vector<GraphStuff>::iterator igc = ig; igc != igl; igc++){
      leg->AddEntry(igc->theGraph_s, igc->legEntry.c_str(), "l");

      caption += "<a name = \"" + igc->legEntry + "\">" +  igc->legEntry + "</a>";
        
      if(igc != igl-1 )
	caption += ", ";
    }
    aPotPlot.caption = caption;

    // set up the short term pad and histogram
    std::string padName_s = "pad_s_" + m_name + sPlotNum.str();
    double longFrac = double(m_padWidth_l) / double(m_canWidth);
    hisPad_s = new TPad(padName_s.c_str(), "short term histogram", longFrac, 0.0, 1.0, hisFrac);
    hisPad_s->SetGrid();
    hisPad_s->SetTicks(1,1);
    hisPad_s->cd(0);
    
    std::string hname_s = "h_s_" + m_name + "_" + sPlotNum.str();
    h_pot_s = new TH1F(hname_s.c_str(), "", 1, xmin_s, xmax_s);
    h_pot_s->UseCurrentStyle();
    h_pot_s->GetXaxis()->SetTimeDisplay(1);
    h_pot_s->GetXaxis()->SetTimeFormat("%H:%M");
    h_pot_s->GetXaxis()->SetNdivisions(503);
    //h_pot_s->GetXaxis()->SetLabelSize(0) ;
    h_pot_s->SetStats(0);
    
    // set up the long term pad and histogram
    gROOT->SetStyle("PotStyle_l");
    can->cd();
    std::string padName_l = "pad_l_" + m_name + sPlotNum.str();
    hisPad_l = new TPad(padName_l.c_str(), "long term histogram", 0., 0.0, longFrac, hisFrac);
    hisPad_l->SetGrid();
    hisPad_l->SetTicks(1,1);
    hisPad_l->cd(0);
    
    std::string hname_l = "h_l_" + m_name + "_" + sPlotNum.str();
    h_pot_l = new TH1F(hname_l.c_str(), "", 1, xmin_l, xmax_l);
    h_pot_l->UseCurrentStyle();
    h_pot_l->GetXaxis()->SetTimeDisplay(1);
    h_pot_l->GetXaxis()->SetTimeFormat("%H:%M");
    h_pot_l->GetXaxis()->SetNdivisions(407);
    h_pot_l->SetStats(0);


    // determine the upper and lower limits of the histograms
    float ymin = std::numeric_limits<float>::max(), ymax = std::numeric_limits<float>::min();

    for(std::vector<GraphStuff>::iterator igc = ig; igc != igl; igc++){

      ymin = (igc->ymin < ymin)? igc->ymin : ymin;
      ymax = (igc->ymax > ymax)? igc->ymax : ymax;
    }

    if(ymin == std::numeric_limits<float>::max())
      ymin = 0;

    if(m_fixedYMin != std::numeric_limits<float>::max())
      ymin = m_fixedYMin;

    if(m_fixedYMax != std::numeric_limits<float>::max())
      ymin = m_fixedYMax;

    if(ymax == std::numeric_limits<float>::min()) {
      ymax = 1;
    } else {
      ymin *= 0.9;
      ymin = (ymin < 0.)? 0. : ymin;
    }

    if(ymin == ymax) {
      if(ymax == 0) 
	ymax = 1;
      else {
	ymin *= 0.9;
	ymax *= 1.1;
      }
    }

    if(m_fixedYMin != std::numeric_limits<float>::max()) {
      ymax += (ymax - ymin) * .1;
      ymax = ( (ymax - ymin) < 0.1)? ymin + 0.1 : ymax;
    }

    hisPad_s->SetLogy(0);
    hisPad_l->SetLogy(0);
    
    if(m_isLogPlot){
      if(m_fixedYMin != std::numeric_limits<float>::max()) {
	ymin = m_fixedYMin;
      } else if( ymin < .001) {
	ymin = .001;
      }
      
      ymax *= 2.0;
      //ymax = ( (ymax/ymin) < 11.)?  ymin * 11. : ymax; 
      hisPad_s->SetLogy(1);
      hisPad_l->SetLogy(1);
    }

    h_pot_s->SetMinimum(ymin);
    h_pot_s->SetMaximum(ymax);
    h_pot_l->SetMinimum(ymin);
    h_pot_l->SetMaximum(ymax);

    // Draw everything

    can->cd();
    can->Draw();
    lab->Draw();
    hisPad_s->Draw();
    hisPad_l->Draw();

    hisPad_s->cd();
    h_pot_s->Draw();
    int iPar = 0;
    for(std::vector<GraphStuff>::iterator igc = ig; igc != igl; igc++, iPar++) {
      if(igc->theGraph_s != 0) {
	(igc->theGraph_s)->SetLineColor(m_graphColorOffset+iPar);
	(igc->theGraph_s)->SetLineWidth(m_lineWidth);
	(igc->theGraph_s)->Draw("L");
	MakeAndDrawDotGraph(igc->theGraph_s, &igc->theDotGraph_s, iPar, m_dotSpacing_s);

      }
    }
    hisPad_l->cd();
    h_pot_l->Draw();
    iPar = 0;
    for(std::vector<GraphStuff>::iterator igc = ig; igc != igl; igc++, iPar++){
      if(igc->theGraph_l != 0) {
	(igc->theGraph_l)->SetLineColor(m_graphColorOffset+iPar);
	(igc->theGraph_l)->SetLineWidth(m_lineWidth);
	(igc->theGraph_l)->Draw("L");
	MakeAndDrawDotGraph(igc->theGraph_l, &igc->theDotGraph_l, iPar, m_dotSpacing_l);
      }
    }

    can->cd();
    leg->Draw();

    aPotPlot.fileName = m_tmpPath + "/" + "pot_" + m_name;

    if(nPlots > 1 && plotNum != 0) {
      aPotPlot.fileName += "_" + sPlotNum.str();
    }
    aPotPlot.fileName += "." + m_fileType;

    m_timer2.start();
    gErrorIgnoreLevel = kWarning;
    can->Print(aPotPlot.fileName.c_str(), m_fileType.c_str());
    m_timer2.stop();

    // Reset
    delete can;
    // hisPad is deleted by the can destructor
    delete lab;
    delete leg;
    delete h_pot_s;
    delete h_pot_l;
    

    for(std::vector<GraphStuff>::iterator igc = ig; igc != igl; igc++){

      if(igc->theGraph_s != 0)
	delete igc->theGraph_s;

      if( igc->theDotGraph_s != 0) {
	delete ( igc->theDotGraph_s );
	igc->theDotGraph_s = 0;
      }

      if(igc->theGraph_l != 0)
	delete igc->theGraph_l;

      if( igc->theDotGraph_l != 0) {
	delete ( igc->theDotGraph_l );
	igc->theDotGraph_l = 0;
      }

    }

    return true;
  }

  void POT::MakeAndDrawDotGraph(const TGraph *gr, TGraph **theDotGraph, int iPar, int dotSpacing) const
  {
    int nPoints = gr->GetN();
    Double_t *x = gr->GetX();
    Double_t *y = gr->GetY();

    if(nPoints -iPar * dotSpacing < 1) {
      return;
    }

    int nextDot = dotSpacing * m_maxParsPerPlot;
    std::vector<Double_t> newX, newY;

    int nNewPoints=0;
    for(int iPoint = iPar * dotSpacing ; iPoint < nPoints; iPoint += nextDot) {
      nNewPoints++;
      newX.push_back(x[iPoint]);
      newY.push_back(y[iPoint]);
    }

    Double_t *px = &(newX.front());
    Double_t *py = &(newY.front());

    *theDotGraph = new TGraph(nNewPoints, px, py);
    (*theDotGraph)->SetMarkerColor(m_graphColorOffset+iPar);
    (*theDotGraph)->SetMarkerStyle(21); // 21 = filled square
    (*theDotGraph)->SetMarkerSize(m_dotSize);
    (*theDotGraph)->Draw("P");

  }

}
  
