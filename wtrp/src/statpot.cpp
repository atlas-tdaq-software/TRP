#include "wtrp/statpot.h"
#include "wtrp/runinfo.h"

#include <sstream>

namespace wtrp {

  unsigned int StatPOT::m_invalid = std::numeric_limits<unsigned int>::max();
  const double StatPOT::m_charYOffset =  .2;
  const std::string StatPOT::m_urlTrigConf = "http://atlas-trigconf.cern.ch";

  const std::map<unsigned int, StatPOT::SetupMapEntry> StatPOT::emptyMap;

  StatPOT *StatPOT::BookStatPOT(const std::string &name, const std::string &title)
  {

    POTBase *thePOTBase = GetPOT(name);
    if(thePOTBase != 0) {
      StatPOT *thePOT = dynamic_cast<StatPOT*>(thePOTBase);
      if( thePOT != 0)
	return thePOT;
      else {
	std::cout << "BookPOT: attempt to book a POT with same name as an existing on of a different type" << std::endl;
	return 0;
      }
    }
    
    StatPOT *thePOT = new StatPOT(name, title);
    return thePOT;
  }

  StatPOT::StatPOT(const std::string &name, const std::string &title) : 
    POTBase(name, title), m_changeLog(0), m_entryCounter(0), m_generateImageMap(false), m_GetURL(0)
  {;}

  void StatPOT::AddParameter(bool (*GetValue)(unsigned int &theValue), const std::string &axisLabel,
			     const std::map<unsigned int, SetupMapEntry> &setupMap, unsigned int opts,
			     int deltaBoundary)
  {
    // Disallow axisLabel to be the same as for an already booked parameter.
    for(std::vector<ParDef>::const_iterator pdit = m_parDefs.begin(); pdit != m_parDefs.end(); pdit++){
      if( pdit->axisLabel == axisLabel)
	return;
    }

    ParDef pd;
    
    pd.axisLabel = axisLabel;
    pd.GetValue = GetValue;
    pd.GetString = 0;
    pd.isToggle =  ((opts & opt_toggle) != 0);
    pd.tabulate = ((opts & opt_tabulate) != 0);
    pd.genImageMapBoundary = ((opts & opt_genMapBoundary) != 0);
    pd.genImageMapRange = ((opts & opt_mapRange) != 0);
    pd.setupMap = setupMap;
    pd.deltaBoundary = deltaBoundary;
    // add a special entry for invalid updated
    std::string na = "N/A";
    SetupMapEntry inv(m_invalid, na);
    pd.setupMap[m_invalid] = inv;

    m_parDefs.push_back(pd);

  }
  
  void StatPOT::AddStringParameter(bool (*GetString)(std::string &theValue), const std::string &axisLabel,
			     const std::map<unsigned int, SetupMapEntry> &setupMap, unsigned int opts)
  {

    ParDef pd;

    pd.axisLabel = axisLabel;
    pd.GetValue = 0;
    pd.GetString = GetString;
    pd.isToggle =  true;
    pd.tabulate = ((opts & opt_tabulate) != 0);
    pd.setupMap = setupMap;
    pd.genImageMapBoundary = false;
    pd.genImageMapRange = false;
    pd.deltaBoundary = -1;
    // add a special entry for invalid updated
    std::string na = "N/A";
    SetupMapEntry inv(m_invalid, na);
    pd.setupMap[m_invalid] = inv;

    m_parDefs.push_back(pd);

  }
  
  void StatPOT::EnableImageMapGeneration( const std::string& name, 
				 void(*GetURL)(const std::vector<std::string>&labels, 
					       const std::vector<unsigned int>&values,
					       std::string &url), bool isDefault )
  { 
    m_imageMapName = name; 
    m_generateImageMap = true; 
    m_GetURL = GetURL;

    if(isDefault) 
      POTBase::m_defaultImageMap = &m_theImageMap;
}

  void StatPOT::GetTrigConfURL(
			       const std::vector<std::string> &labels, 
			       const std::vector<unsigned int> &values,
			       std::string &url)
  {
    int smk = 0, l1_psk = 0, hlt_psk = 0;

    std::vector<unsigned int>::const_iterator iv = values.begin();
    for( std::vector<std::string>::const_iterator il = labels.begin(); il != labels.end(); il++, iv++) {

      if( *il == "SMK")
	smk = *iv;
      else if( *il == "L1 PSK")
	l1_psk = *iv;
      else if( *il == "HLT PSK")
	hlt_psk = *iv;

    }
    std::stringstream ss;
    
    ss << m_urlTrigConf 
       << "/run/smkey/"  << smk 
       << "/l1key/"      << l1_psk 
       << "/hltkey/"     << hlt_psk
       << "/";
    
    url = ss.str();

  }


  void StatPOT::Dump() const
  {
    std::cout << "POT type: StatPOT, number of parameters: " << m_parDefs.size()  << std::endl;

    DumpBase();

    for(std::vector<ParDef>::const_iterator id =  m_parDefs.begin(); id != m_parDefs.end(); id++){
      std::cout << "Label: " << id->axisLabel
		<< "isToggle: " << id->isToggle 
		<< ", tabulate: " << id->tabulate 
		<< ", Nentries in setup map: "
		<< id->setupMap.size() << std::endl;

      std::cout << "  Number of data points (short term): " << id->data_s.size() << ", the data: " << std::endl;

      int nd_s = 0;
      for(std::deque<unsigned int>::const_iterator ip =  id->data_s.begin(); ip != id->data_s.end(); ip++){
	std::cout << *ip << ", ";
	if( (nd_s++ % 10) == 0) std::cout << std::endl;
      }
      std::cout << std::endl;

      std::cout << "  Number of data points (long term): " << id->data_l.size() << ", the data: " << std::endl;

      int nd_l = 0;
      for(std::deque<unsigned int>::const_iterator ip =  id->data_l.begin(); ip != id->data_l.end(); ip++){
	std::cout << *ip << ", ";
	if( (nd_l++ % 10) == 0) std::cout << std::endl;
      }
      std::cout << std::endl;
    }
  }
  
  
  void StatPOT::Update(bool updateLong )
  {


    for(std::vector<ParDef>::iterator id = m_parDefs.begin(); id != m_parDefs.end(); id++){ // param loop

      unsigned int theVal = 0;
      bool isValid = false;
      bool hasChanged = false;
      std::string stringVal = "";

      if( id->GetValue != 0) {
	isValid = id->GetValue(theVal);

	hasChanged = (id->data_s.size() == 0) || (theVal != id->data_s.back());
	std::stringstream ss;
	ss << theVal;
	stringVal = ss.str();
      } else if( id->GetString != 0) {
	isValid = id->GetString(stringVal);
	if(!isValid)
	  stringVal = "N/A";
	hasChanged = ( (id->data_s.size() == 0) || (stringVal != id->textMap[id->data_s.back()]));
	if(hasChanged) {
	  theVal = (id->data_s.size() == 0)? 0 : id->data_s.back() + 1;
	  id->textMap[theVal] = stringVal;
	} else {
	  theVal = id->data_s.back();
	}
      }

      if(id->tabulate && hasChanged ) {
	m_entryCounter++;
	
	long now = m_timeStamps_s.back();
	logEntries.push_back(ET_Entry(m_entryCounter, now));
	
	Row *aRow = m_changeLog->AddRow();
	std::stringstream ss;
	ss << m_entryCounter;
	aRow->UpdateCell("log", "entry", ss.str() );
	ss.str("");
	ss << m_now;
	aRow->UpdateCell("log", "time", ss.str() );
	ss.str("");
	aRow->UpdateCell("log", "text", id->axisLabel );

	aRow->UpdateCell("log", "value", stringVal );
	
	{
	  unsigned int smk, l1psk, hltpsk;
	  if(RunInfo::GetSMK(smk) && RunInfo::GetL1_PSK(l1psk) && RunInfo::GetHLT_PSK(hltpsk)) {
	    ss.str("");
	    /*
 	    ss << "<a href = \"http://trigconf.cern.ch/onlinemenus?smkey=" << smk 
	       << "&hltkey=" << hltpsk << "&l1key=" << l1psk << "\">" 
	       << smk << ", " << l1psk << ", " << hltpsk << "</a>";
	    */
	      //http://atlas-trigconf.cern.ch/run/smkey/713/l1key/1097/hltkey/1029/ 
 	    ss << "<a href = \"" << m_urlTrigConf 
	       << "/run/smkey/"  << smk 
	       << "/l1key/"      << l1psk 
	       << "/hltkey/"     << hltpsk 
	       << "/\">" 
	       << smk << ", " << l1psk << ", " << hltpsk << "/</a>";
	    aRow->UpdateCell("log", "keys", ss.str() );
	  } else {
	    aRow->UpdateCell("log", "keys", "U" );
	  }
	}

	// See if anything needs to be deleted
	while( now - logEntries.front().m_time > m_twentyFourHours) {
	  ss.str("");
	  ss << logEntries.front().m_number;
	  if (!m_changeLog->RemoveRow("log", "entry", ss.str() )) {
	    std::cout << "StatPOT Update warning: couldn't find log table row" << std::endl;
	  }
	  logEntries.pop_front();
	}
      }
      id->data_s.push_back(theVal);
      if( id->data_s.size() > m_length_s)
	id->data_s.pop_front();
      
      id->transPoints.push_back(theVal);

      if(updateLong) {
	/*
	 If the most recent point in transPoints is different from the last point in data_l, use it.
	 If not, check if any of the other points differ from the most recent point -- if so use the 
	 latest different point
	*/
	theVal = id->transPoints.back();
	if(id->data_l.size() != 0 && theVal == id->data_l.back() ) {
	  for(std::vector<unsigned int>::reverse_iterator itp = id->transPoints.rbegin();
	      itp != id->transPoints.rend(); itp++) {
	    if(*itp != theVal) {
	      theVal = *itp;
	      break;
	    }
	  }
	}
	id->data_l.push_back(theVal);
	id->transPoints.clear();
	
	if( id->data_l.size() > m_length_l) {
	  unsigned int frontVal = id->data_l.front();
	  id->data_l.pop_front();

	  if(id->GetString != 0 && frontVal != id->data_l.front())
	    id->textMap.erase(frontVal);
	}
      }

    }

    if(m_generateImageMap)
      MakeImageMap();

  }
  
  void StatPOT::MakeImageMap()
  {

    if(m_timeStamps_s.size() < 2 ) {
      m_theImageMap = "";
      return;
    }
    
    m_theImageMap = "<map name=\"" + m_imageMapName + "\">\n";
    
    for(int ils = 0; ils < 2; ils++) { // loop: long plot then short plot
      
      std::deque<long> &timeStamps = (ils == 0)? m_timeStamps_l : m_timeStamps_s;
      
      if(timeStamps.size() < 2)
	continue;

      // Set the initial time (in pixels) to the left margins
      int offsetInPix = (ils == 0)?  m_leftMargin_l : m_padWidth_l + m_leftMargin_s;

      // get the user coordinate to pixel ratio
      double xmin = timeStamps.front() - 1;
      double xmax = timeStamps.back() + 1;

      double pixPerTime = 1;

      if(ils == 0)
	pixPerTime = double(m_padWidth_l - m_leftMargin_l - m_rightMargin_l) / (xmax - xmin);
      else
	pixPerTime = double(m_canWidth - m_padWidth_l - m_leftMargin_s - m_rightMargin_s) / (xmax - xmin) ;


      // Set up labels and start values from oldest time point
      std::vector<unsigned int> startValues;
      std::vector<std::string> labels;
      
      for(std::vector<ParDef>::iterator id = m_parDefs.begin(); id != m_parDefs.end(); id++){ // param loop
	
	std::deque<unsigned int> &data = (ils == 0)? id->data_l : id->data_s;
	startValues.push_back(data.front());
	labels.push_back(id->axisLabel);
	
      }
      
      // Loop over time points, generate new areas when needed
      int iTime = 0;
      int startTimePix = offsetInPix;
      long startTime = static_cast<long>(xmin);
      long endTime = static_cast<long> (xmax);
      
      std::vector<unsigned int> nextValues;
      
      for(std::deque<long>::const_iterator its = timeStamps.begin(); 
	  its != timeStamps.end(); its++, iTime++) {
	
	int ip = 0;
	nextValues.clear();
	bool hasChanged = false;
	for(std::vector<ParDef>::iterator id = m_parDefs.begin(); 
	    id != m_parDefs.end(); id++, ip++){ // param loop

	  std::deque<unsigned int> &data = (ils == 0)? id->data_l : id->data_s;
	  
	  nextValues.push_back(data[iTime]);
	  
	  if(id->genImageMapBoundary || id->genImageMapRange) {
	    if(data[iTime] != startValues[ip]) {
	      if(id->deltaBoundary < 0 || data[iTime] >= startValues[ip] + id->deltaBoundary)
		hasChanged = true;
	    }
	  }
	}

	if(hasChanged) {
	  
	  std::string newArea;
	  endTime = *its;
	  int endTimePix = offsetInPix + static_cast<int>
	    ( (static_cast<double>(endTime) - xmin)* pixPerTime);
	  
	  MakeNewAreaTag(startTime, endTime, startTimePix, endTimePix, labels, 
			 startValues, nextValues, newArea);
	  m_theImageMap += newArea;
	  startValues = nextValues;
	  startTime = endTime;
	  startTimePix = endTimePix;
	}

      }

      if(startValues.size() > 0) {
	std::string newArea;
	endTime = static_cast<int>(xmax);
	int endTimePix = offsetInPix + 
	  static_cast<int>( (static_cast<double>(endTime) - xmin) * pixPerTime);

	MakeNewAreaTag(startTime, endTime, startTimePix, endTimePix, labels,
		       startValues, nextValues, newArea);
	m_theImageMap += newArea;
      }
    }
    m_theImageMap += "</map>\n";

    //std::cout << "The image map --- \n" << m_theImageMap << std::endl;

  }

  void StatPOT::MakeNewAreaTag(long startTime, long endTime, int startTimePix, int endTimePix,
			       const std::vector<std::string> &labels, 
			       const std::vector<unsigned int> &startValues, 
			       const std::vector<unsigned int> &nextValues, 
			       std::string &newArea)
  {
    // Set up the title string
    std::string start, stop;
    POTBase::ConvertTimeToString(startTime, start);
    POTBase::ConvertTimeToString(endTime, stop);
    std::string title = "title=\"" + start + "--" + stop + ": ";
    
    std::vector<unsigned int>::const_iterator iv = startValues.begin();
    std::vector<unsigned int>::const_iterator in = nextValues.begin();
    bool firstTime = true;
    for(std::vector<ParDef>::const_iterator id = m_parDefs.begin(); 
	id != m_parDefs.end(); id++, iv++, in++) {

      if(id->genImageMapBoundary || id->genImageMapRange) {
	if(!firstTime) {
	  title += "; ";
	}
	std::stringstream ss;
	ss << id->axisLabel << ":" << *iv;
	if(id->genImageMapRange > 0)
	  ss << "--" << *in;
	title += ss.str();
	firstTime = false;
      }
    }
    title += "\"";

    std::stringstream ss;
    ss << "<area shape=\"rect\" coords=\"" <<
      startTimePix << "," << m_titleHeight << "," << endTimePix << "," << m_imageMapHeight << "\" " << title;
    if(m_GetURL != 0) {
      std::string url;
      m_GetURL( labels, startValues, url);
      ss << " href=\"" << url << "\"";
    }
    ss << "/>\n";

    newArea = ss.str();
    //std::cout << "newArea: " << newArea << std::endl;
}
  bool StatPOT::GetPOTPlots(std::vector<PlotStuff> &plotStuff) const
  {


    plotStuff.clear();

    if(m_parDefs.size()== 0) {
      //std::cout << "no parameters" << std::endl;
      return false;
    }

    if(m_timeStamps_s.size() == 0 && m_timeStamps_l.size() == 0) {
      std::cout << "no time stamps" << std::endl;
      return false;
    }

    if(m_tmpPath == "") {
      std::cout << "no temporary storage" << std::endl;
      return false;
    }

    //std::cout << "outputting plots " << m_name << std::endl;

    TCanvas *can = 0;
    TPad *hisPad_s = 0;
    TPad *hisPad_l = 0;
    TH1F *h_pot_s = 0;
    TH1F *h_pot_l = 0;

    TPaveLabel *lab = 0;

    std::string caption = "";

    double xmin_s = m_timeStamps_s.front();
    double xmax_s = m_timeStamps_s.back();

 
    double xmin_l = xmin_s, xmax_l = xmax_s;
    if(m_timeStamps_l.size() > 0) {
      xmin_l = m_timeStamps_l.front();
      xmax_l = m_timeStamps_l.back();
    }

    // used for setting plot limits 
    xmin_l -= 1.;
    xmax_l += 1.;
    xmin_s -= 1.;
    xmax_s += 1.;

    double userPerPix_s = 
      (xmax_s -xmin_s) / double(m_canWidth - m_padWidth_l - m_leftMargin_s - m_rightMargin_s);

    double userPerPix_l = (xmax_l -xmin_l) / double(m_padWidth_l - m_leftMargin_l - m_rightMargin_l);

    PlotStuff aPotPlot;

    caption = "<p>" + m_title += ": ";
    std::string title = m_title;

    unsigned int hisHeight = m_hisBottomMarg + m_hisTopMarg +m_parDefs.size() * m_heightPerPar;
    unsigned int canHeight = m_titleHeight + hisHeight;
	
    aPotPlot.width = m_canWidth;
    aPotPlot.height = canHeight;
    aPotPlot.isVisible = true;

    //double titleFrac = static_cast<double>(m_titleHeight)/canHeight;
    double hisFrac = static_cast<double>(canHeight-m_titleHeight)/canHeight;

    m_potStyle_s->SetPadBottomMargin(double(m_hisBottomMarg) / double(hisHeight));
    m_potStyle_s->SetLabelSize(double(m_axisLabelSize)/double(hisHeight), "X");
    m_potStyle_s->SetLabelSize(double(m_axisLabelSize)/double(hisHeight), "Y");
    
    m_potStyle_l->SetPadBottomMargin(double(m_hisBottomMarg) / double(hisHeight));
    m_potStyle_l->SetLabelSize(double(m_axisLabelSize)/double(hisHeight), "X");
    m_potStyle_l->SetLabelSize(double(m_axisLabelSize)/double(hisHeight), "Y");


    gROOT->SetStyle("PotStyle_s");

    std::string canName = "can_" + m_name;
    can = new TCanvas(canName.c_str() , m_title.c_str(), 
		      m_canWidth + m_canWidthDiff, canHeight + m_canHeightDiff);
	
    lab = new TPaveLabel(0.0, hisFrac, 1.0, 1.0, title.c_str());
    lab->SetBorderSize(0);
    lab->SetFillColor(0);
	
    can->cd();

    std::string padName_s = "pad_s_" + m_name;
    double longFrac = double(m_padWidth_l) / double(m_canWidth);
    hisPad_s = new TPad(padName_s.c_str(), "short term histogram", longFrac, 0.0, 1.0, hisFrac);
	
    hisPad_s->cd(0);
	
    std::string hname_s = "h_s_" + m_name;
    h_pot_s = new TH1F(hname_s.c_str(), "", 1, xmin_s, xmax_s);
    h_pot_s->UseCurrentStyle();
    h_pot_s->GetXaxis()->SetTimeDisplay(1);
    h_pot_s->GetXaxis()->SetTimeFormat("%H:%M");
    h_pot_s->GetXaxis()->SetNdivisions(503);
    h_pot_s->SetStats(0);
    h_pot_s->GetYaxis()->SetNdivisions(0);

    h_pot_s->SetMinimum(0.);
    h_pot_s->SetMaximum( m_parDefs.size() );

    gROOT->SetStyle("PotStyle_l");
    can->cd();
    std::string padName_l = "pad_l_" + m_name;
    hisPad_l = new TPad(padName_l.c_str(), "long term histogram", 0., 0.0, longFrac, hisFrac);
    hisPad_l->cd(0);
 
    std::string hname_l = "h_l_" + m_name;
    h_pot_l = new TH1F(hname_l.c_str(), "", 1, xmin_l, xmax_l);
    h_pot_l->UseCurrentStyle();
    h_pot_l->GetXaxis()->SetTimeDisplay(1);
    h_pot_l->GetXaxis()->SetTimeFormat("%H:%M");
    h_pot_l->GetXaxis()->SetNdivisions(407);
    h_pot_l->SetStats(0);
    h_pot_l->GetYaxis()->SetNdivisions(0);
    
    h_pot_l->SetMinimum(0.);
    h_pot_l->SetMaximum( m_parDefs.size() );

    std::vector<TBox *> boxes_s, boxes_l;
    std::vector<TText *> labels_s, labels_l;
    std::vector<TText *> axisLabels;

    double parHeightFrac = double(m_heightPerPar) / double(hisHeight);
    double charHeightFrac = 0.9 * parHeightFrac;

    unsigned int iPar = 0, nPar =  m_parDefs.size();

    // for the y-axis labels (In a TBox rather than the usual axis labels since the hist should be the same size as the others.
    hisPad_l->cd(0);
    double labelsBoxStart = xmin_l - m_leftMargin_l * userPerPix_l * 0.8;
    double labelsBoxEnd = labelsBoxStart + m_yLabelsBoxWidth * userPerPix_l;
    TBox *yLabelsBox = new TBox(labelsBoxStart, 0, labelsBoxEnd, nPar);
    yLabelsBox->SetFillColor(m_yLabelsBoxFill);
    double labOffset = 3 * userPerPix_l; 

    std::vector<unsigned int> presentColorIds;
    std::vector<std::string> presentTexts;
    
    for(std::vector<ParDef>::const_iterator itpar = m_parDefs.begin(); itpar != m_parDefs.end(); itpar++, iPar++) {
      // loop over parameters
      
      hisPad_s->cd();
      unsigned int presentColorId = 0;
      std::string presentText;
      FillPlot( true, *itpar, iPar, nPar, charHeightFrac, userPerPix_s, boxes_s, labels_s, &presentColorId, &presentText);
      presentColorIds.push_back(presentColorId);
      presentTexts.push_back(presentText);

      hisPad_l->cd(0);
      FillPlot( false, *itpar, iPar, nPar, charHeightFrac, userPerPix_l, boxes_l, labels_l);


      TText *axisLab = new TText(labelsBoxStart + labOffset, iPar + m_charYOffset , itpar->axisLabel.c_str());
      axisLab->SetTextFont(m_font);
      axisLab->SetTextSize( charHeightFrac );
      axisLabels.push_back(axisLab);

      std::string p = itpar->axisLabel;
      caption += "<a name = \"" + p + "\">" +  p + "</a>";
      
      if(iPar != nPar - 1 )
	caption += ", ";
      
    }

    std::vector<TBox *> presentValueBoxes;
    std::vector<TBox *> fillBoxes;
    std::vector<TText *> presentValueTexts;
    SetPresentValueBoxes(hisPad_s, xmax_s, parHeightFrac, userPerPix_s, 
			 presentValueBoxes, fillBoxes, presentValueTexts, presentColorIds, presentTexts);

    if(nPar != 0) {
	  
      caption += "<br>";
      aPotPlot.caption = caption;
      
    
      can->cd();
      can->Draw();
      lab->Draw();
      hisPad_s->Draw();
      hisPad_l->Draw();

      hisPad_s->cd();
      h_pot_s->Draw();
      for(std::vector<TBox *>::iterator ib = boxes_s.begin(); ib != boxes_s.end(); ib++) 
	(*ib)->Draw();
      for(std::vector<TText *>::iterator it = labels_s.begin(); it != labels_s.end(); it++) 
	(*it)->Draw();

      hisPad_l->cd();
      h_pot_l->Draw();
      for(std::vector<TBox *>::iterator ib = boxes_l.begin(); ib != boxes_l.end(); ib++) 
	(*ib)->Draw();
      for(std::vector<TText *>::iterator it = labels_l.begin(); it != labels_l.end(); it++) 
	(*it)->Draw();

      yLabelsBox->Draw();
      for(std::vector<TText *>::iterator it = axisLabels.begin(); it != axisLabels.end(); it++) 
	(*it)->Draw();


      hisPad_s->cd();

      for(std::vector<TBox *>::iterator ib = fillBoxes.begin(); ib != fillBoxes.end(); ib++) {
	(*ib)->Draw();
      }

      for(std::vector<TBox *>::iterator ib = presentValueBoxes.begin(); ib != presentValueBoxes.end(); ib++) {
	(*ib)->Draw();
      }
      for(std::vector<TText *>::iterator it = presentValueTexts.begin(); it != presentValueTexts.end(); it++) 
	(*it)->Draw();

      std::string fileName = m_tmpPath + "/" + "pot_" + m_name + "." + m_fileType;

      aPotPlot.fileName = fileName;

      if(m_isVisible)
	plotStuff.push_back(aPotPlot);

      can->Print(fileName.c_str(), m_fileType.c_str());
      

      delete can;
      // hisPad is deleted by the can destructor
      delete lab;
      delete h_pot_s;
      delete h_pot_l;
      delete yLabelsBox;

      for(std::vector<TBox *>::iterator ib = boxes_s.begin(); ib != boxes_s.end(); ib++) 
	delete *ib;

      for(std::vector<TText *>::iterator it = labels_s.begin(); it != labels_s.end(); it++) 
	delete *it;

      for(std::vector<TBox *>::iterator ib = boxes_l.begin(); ib != boxes_l.end(); ib++) 
	delete *ib;

      for(std::vector<TText *>::iterator it = labels_l.begin(); it != labels_l.end(); it++) 
	delete *it;

      for(std::vector<TText *>::iterator it = axisLabels.begin(); it != axisLabels.end(); it++) 
	delete *it;


      for(std::vector<TBox *>::iterator ib = fillBoxes.begin(); ib != fillBoxes.end(); ib++) 
	delete *ib;
      for(std::vector<TBox *>::iterator ib = presentValueBoxes.begin(); ib != presentValueBoxes.end(); ib++) 
	delete *ib;
      for(std::vector<TText *>::iterator it = presentValueTexts.begin(); it != presentValueTexts.end(); it++) 
	delete *it;

    }
    return true;
  }

  void StatPOT::FillPlot(bool doShortPlot, const ParDef &par, unsigned int iPar, unsigned int nPar, double charHeightFrac, 
			 double userPerPix, std::vector<TBox *> &boxes, std::vector<TText * > &labels,
			 unsigned int *presentColorId, std::string *presentText) const {

    const std::deque<long> &timeStamps = (doShortPlot)? m_timeStamps_s : m_timeStamps_l;
    const std::deque<unsigned int> &data = (doShortPlot)? par.data_s : par.data_l;

    if( timeStamps.size() == 0)
      return;

    // the variables need to be defined but the values given here aren't actually used -- they are reset at the end of the loop
    double ymin = iPar;
    double ymax = ymin+1;
    double xmin = timeStamps.front();
    double xmax = xmin;

    double xRightEdge = timeStamps.back();
    double xTextStartAfter = xmin-1.;
    double xTextEdgeLongPlot = xmin + (m_yLabelsBoxWidth - m_leftMargin_l*0.5) * userPerPix; // with a little spare hopefully

    unsigned int iVal = data.front();
    bool isValid = (iVal != m_invalid);
    std::string text;
    unsigned int colorId = 1;

    bool firstIteration = true;
    std::deque<long>::const_iterator its = timeStamps.begin();
    for(std::deque<unsigned int>::const_iterator ip =  data.begin(); ip != data.end(); ip++, its++, firstIteration=false) {
      
      if(!firstIteration) {
	xmax = *its;

	if( *ip != iVal || ((ip == data.end()-1) && (xmax != xmin) ) ) {
	  
	  TBox *aBox = new TBox(xmin, ymin, xmax, ymax);
	  aBox->SetFillColor(colorId);
	  if( !isValid )
	    aBox->SetFillStyle(3005);
	  
	  boxes.push_back(aBox);
	  
	  if(text != "") {
	    double textLength = text.size() * charHeightFrac *nPar * m_heightPerPar * userPerPix * .6;
	    double xText = xmin + charHeightFrac * userPerPix * .4;

	    if(!doShortPlot && xText < xTextEdgeLongPlot && (xTextEdgeLongPlot + textLength < xmax) )
	      xText = xTextEdgeLongPlot; // not too close to left margin, if possible

	    if(xText > xTextStartAfter && xText + textLength < xRightEdge) { 
	      // not too close to last label printed or too close to right margin
	      TText *aText = new TText(xText, ymin + m_charYOffset, text.c_str() );
	      aText->SetTextFont(m_font);
	      aText->SetTextSize( charHeightFrac );
	      aText->SetTextColor(kBlack);
	      labels.push_back(aText);
	      xTextStartAfter = xText + 1.5 * textLength;
	    }
	  }
	}
      }
      
      if( *ip != iVal || firstIteration) {
	xmin = xmax;
	iVal = *ip;
	
	// Set up for next point
      
	if(par.isToggle) {
	  colorId = (colorId == m_toggleColorLight)? m_toggleColorDark : m_toggleColorLight;
	  if(par.GetString == 0) {
	    std::stringstream ss;
	    ss << iVal;
	    text = ss.str();
	  } else {
	    std::map<unsigned int, std::string>::const_iterator itx = par.textMap.find(iVal);
	    text = (itx != par.textMap.end())? itx->second : "bug";
	  }
	} else {
	  std::map<unsigned int, StatPOT::SetupMapEntry>::const_iterator anE = (par.setupMap).find(iVal);
	  if(anE != par.setupMap.end() ) {
	    colorId = anE->second.m_colorIndex;	
	    text = anE->second.m_label;
	  }
	}
      }

      if(ip == data.end()-1 && presentText != 0) {
	*presentColorId = colorId;
	*presentText = text;
      }
    }
  }


  void StatPOT::SetPresentValueBoxes(TPad *hisPad_s, double xmax_s, double parHeightFrac, double userPerPix_s, 
				     std::vector<TBox *> &presentValueBoxes, 
				     std::vector<TBox *> &fillBoxes,
				     std::vector<TText *> &presentValueTexts,
				     std::vector<unsigned int> &presentColorIds,
				     std::vector<std::string> &presentTexts) const 
  {
    hisPad_s->cd();

    double boxXStart = xmax_s - (m_presentValueBoxWidth - m_rightMargin_s * 0.7)  * userPerPix_s;
    double boxXEnd   = boxXStart + m_presentValueBoxWidth * userPerPix_s;
    double boxCenter = (boxXStart + boxXEnd) * 0.5;

    double boxHeight = 0.75;
    double boxYOffset = (1. - boxHeight) * 0.5;
    double textHeight = parHeightFrac * boxHeight;
    double textYOffset = 0.1 * boxHeight;
    double charSizeX = textHeight * m_parDefs.size() * m_heightPerPar * userPerPix_s * 0.5; 
    unsigned int iPar = 0;
    for(std::vector<ParDef>::const_iterator itpar = m_parDefs.begin(); itpar != m_parDefs.end(); itpar++, iPar++) {

      double boxYStart = iPar + boxYOffset;
      TBox *aBox = new TBox(boxXStart, boxYStart, boxXEnd, boxYStart + boxHeight);
      unsigned int lineColor = (presentColorIds[iPar] == 1)? 16 : 1;
      aBox->SetLineColor(lineColor);
      aBox->SetLineWidth(2);
      aBox->SetLineStyle(1);
      aBox->SetFillStyle(0);
      presentValueBoxes.push_back(aBox);
      TBox *fillBox = new TBox(boxXStart, boxYStart, boxXEnd, boxYStart + boxHeight);
      fillBox->SetLineStyle(0);
      fillBox->SetFillStyle(1001);
      fillBox->SetFillColor(presentColorIds[iPar] );
      fillBoxes.push_back(fillBox);

      if(presentTexts[iPar] != "") {
	double xText = boxCenter - presentTexts[iPar].size() * charSizeX * 0.5;
	TText *aText = new TText(xText, boxYStart + textYOffset , presentTexts[iPar].c_str() );
	aText->SetTextFont(m_font);
	aText->SetTextSize( textHeight );
	presentValueTexts.push_back(aText);
      }
    }
  }


}
