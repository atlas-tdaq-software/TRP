#include "wtrp/grouptable.h"
#include "wtrp/TrigConf.h"
#include <iostream>

namespace wtrp {

  GroupTable::GroupTable(const std::string &name, 
			 const std::string &title, 
			 const std::string &linkText, 
			 const std::string &fileName) : 
    PageData(name, title, linkText, fileName){;}

  void GroupTable::MakeTable(const TrigConf *tc)
  {
    

    AddGlobalLink(m_fileName, m_linkText);
    m_dataTable.Clear();

    std::vector<ColumnGroup> colGroups;

    ColumnGroup cg_group("grp", "");
    cg_group.AddColumn("grp", "Group", true, true, "ch_");
    m_dataTable.AddColumnGroup(cg_group);


    if(tc->IsRun1Configuration() ) {
      ColumnGroup L2("L2", "L2");
      L2.AddColumn("frac_in", "In rate");
      L2.AddColumn("bar_in", "In frac", false);
      L2.AddColumn("frac_out", "Out rate");
      L2.AddColumn("bar_out", "Out frac", false);
      m_dataTable.AddColumnGroup(L2);

      ColumnGroup EF("EF", "EF");
      EF.AddColumn("frac_in", "In rate");
      EF.AddColumn("bar_in", "In frac", false);
      EF.AddColumn("frac_out", "Out rate");
      EF.AddColumn("bar_out", "Out frac", false);
      m_dataTable.AddColumnGroup(EF);
    } else {
      ColumnGroup HLT("HLT", "HLT");
      HLT.AddColumn("frac_in", "In rate");
      HLT.AddColumn("bar_in", "In frac", false);
      HLT.AddColumn("frac_out", "Out rate");
      HLT.AddColumn("bar_out", "Out frac", false);
      m_dataTable.AddColumnGroup(HLT);
    }

    {
      const std::string &all = ChainTable::GetAllChainsName();
      Row *aRow = m_dataTable.AddRow();
      aRow->UpdateCell("grp", "grp", all);
    }

    const std::set<std::string> &groupNames = tc->GetGroupNames();

    std::set<std::string>::const_iterator strit;

    for(strit = groupNames.begin(); strit != groupNames.end(); strit++){
      Row *aRow = m_dataTable.AddRow();
      aRow->UpdateCell("grp", "grp", *strit);
    }

  }

  GroupTable::~GroupTable(){;}

  void GroupTable::Dump() {
    std::cout << "****** The Group Table  ****** " << std::endl;
    m_dataTable.Dump();
  }

  void GroupTable::BookPOTs(const TrigConf *trigConf) {

    if(trigConf->IsRun1Configuration())
      BookPOTs_Run1(trigConf);
    else
      BookPOTs_Run2(trigConf);
  }
  void GroupTable::BookPOTs_Run1(const TrigConf *trigConf) {

    // Check if pot pages and pots already exist. Only create new ones if they do. Leave existing things alone.

    PageData *L2OutputPage = GetPage("L2out");
    if( L2OutputPage == 0) {
      L2OutputPage = new PageData("L2out", 
				  "L2 Output fractions by group vs. time", 
				  "L2 output vs. time",
				  "L2groupsOut.html");
      AddPOTPage(L2OutputPage);
      AddLocalLink(L2OutputPage->GetOutputFileName(), L2OutputPage->GetPageTitle());
    }

    PageData *EFOutputPage = GetPage("EFout");
    if( EFOutputPage == 0) {
      EFOutputPage = new PageData("EFout", 
				  "EF Output fractions by group vs. time", 
				  "EF output vs. time",
				  "EFgroupsOut.html");
      AddPOTPage(EFOutputPage);
      AddLocalLink(EFOutputPage->GetOutputFileName(), EFOutputPage->GetPageTitle());
    }

    std::string L2POTName = "grL2";
    POT *L2grFracs = POT::BookPOT(L2POTName, "Fractional rate per L2 group " );
    if(L2OutputPage != 0) {
      L2OutputPage->AddPOT(L2POTName);
      L2grFracs->SetLogOption(true);
      L2grFracs->SetFixedYMin(1.e-5);
    }

    std::string EFPOTName = "grEF";
    POT *EFgrFracs = POT::BookPOT(EFPOTName, "Fractional rate per EF group ");
    if(EFOutputPage != 0) {
      EFOutputPage->AddPOT(EFPOTName);
      EFgrFracs->SetLogOption(true);
      EFgrFracs->SetFixedYMin(1.e-5);
    }

    const std::set<std::string> &groupNames = trigConf->GetGroupNames();

    for(std::set<std::string>::const_iterator ig = groupNames.begin(); ig 
	  != groupNames.end(); ig++) {
      if(L2grFracs != 0)
	L2grFracs->AddNormalizedParameter(POT::PD_L2, "grp_" + *ig, "output", POT::PD_L2, "total", "output", *ig);
      if(EFgrFracs != 0)
	EFgrFracs->AddNormalizedParameter(POT::PD_EF, "grp_" + *ig, "output", POT::PD_EF, "total", "output", *ig);
    }

    // Book the chain rate plots 

    const std::set<TC_chain> &tc_chains = trigConf->getChains();
    PageData *L2ChainReduxPage = GetPage("L2redux");
    if( L2ChainReduxPage == 0) {
      L2ChainReduxPage = new PageData("L2redux", 
				      "L2 chain output rate vs. time", 
				      "L2 Chain acceptances vs. time",
				      m_L2ChainPotFileName);
      if(L2ChainReduxPage != 0) {
	AddPOTPage(L2ChainReduxPage);
	AddLocalLink(L2ChainReduxPage->GetOutputFileName(), L2ChainReduxPage->GetPageTitle());
      }
    }

    PageData *EFChainReduxPage = GetPage("EFredux");
    if( EFChainReduxPage == 0) {
      EFChainReduxPage = new PageData("EFredux", 
				      "EF chain output rate vs. time", 
				      "EF Chain acceptances vs. time",
				      m_EFChainPotFileName);

      if(EFChainReduxPage != 0) {
	AddPOTPage(EFChainReduxPage);
	AddLocalLink(EFChainReduxPage->GetOutputFileName(), EFChainReduxPage->GetPageTitle());
      }
    }

    POT *L2redux = POT::BookPOT("chL2", "L2 raw chain output rate " );
    if(L2redux != 0) {
      L2ChainReduxPage->AddPOT("chL2");
      L2redux->SetLogOption(true);
      L2redux->SetFixedYMin(1.e-1);
    }

    POT *EFredux = POT::BookPOT("chEF", "EF raw chain output rate " );
    if(EFredux != 0) {
      EFChainReduxPage->AddPOT("chEF");
      EFredux->SetLogOption(true);
      EFredux->SetFixedYMin(1.e-1);
    }  
    std::set<TC_chain>::const_iterator tccit;


    for(tccit = tc_chains.begin(); tccit != tc_chains.end(); tccit++) {

      const std::string &L2name = tccit->GetLevelName(TC_L2);
      if(L2redux != 0)
	L2redux->AddParameter(POT::PD_L2, L2name, "raw", L2name);

      const std::string &EFname = tccit->GetLevelName(TC_EF);
      if(EFredux != 0)
	EFredux->AddParameter(POT::PD_EF, EFname, "raw", EFname);
    }

  }


  void GroupTable::BookPOTs_Run2(const TrigConf *trigConf) {

    // Check if pot pages and pots already exist. Only create new ones if they do. Leave existing things alone.

    PageData *HLTOutputPage = GetPage("HLTout");
    if( HLTOutputPage == 0) {
      HLTOutputPage = new PageData("HLTout", 
				  "HLT Output fractions by group vs. time", 
				  "HLT output vs. time",
				  "HLTgroupsOut.html");
      AddPOTPage(HLTOutputPage);
      AddLocalLink(HLTOutputPage->GetOutputFileName(), HLTOutputPage->GetPageTitle());
    }

    std::string HLTPOTName = "grHLT";
    POT *HLTgrFracs = POT::BookPOT(HLTPOTName, "Fractional rate per HLT group " );
    if(HLTOutputPage != 0) {
      HLTOutputPage->AddPOT(HLTPOTName);
      HLTgrFracs->SetLogOption(true);
      HLTgrFracs->SetFixedYMin(1.e-5);
    }

    const std::set<std::string> &groupNames = trigConf->GetGroupNames();

    for(std::set<std::string>::const_iterator ig = groupNames.begin(); ig 
	  != groupNames.end(); ig++) {
      if(HLTgrFracs != 0)
	HLTgrFracs->AddNormalizedParameter(POT::PD_HLT, "grp_" + *ig, "output", 
					   POT::PD_HLT, "total", "output", *ig);

    }

    // Book the chain rate plots 

    const std::set<TC_chain> &tc_chains = trigConf->getChains();
    PageData *HLTChainReduxPage = GetPage("HLTredux");
    if( HLTChainReduxPage == 0) {
      HLTChainReduxPage = new PageData("HLTredux", 
				      "HLT chain output rate vs. time", 
				      "HLT Chain acceptances vs. time",
				      m_HLTChainPotFileName);
      if(HLTChainReduxPage != 0) {
	AddPOTPage(HLTChainReduxPage);
	AddLocalLink(HLTChainReduxPage->GetOutputFileName(), HLTChainReduxPage->GetPageTitle());
      }
    }

    POT *HLTredux = POT::BookPOT("chHLT", "HLT raw chain output rate " );
    if(HLTredux != 0) {
      HLTChainReduxPage->AddPOT("chHLT");
      HLTredux->SetLogOption(true);
      HLTredux->SetFixedYMin(1.e-1);
    }

    std::set<TC_chain>::const_iterator tccit;
    for(tccit = tc_chains.begin(); tccit != tc_chains.end(); tccit++) {

      const std::string &HLTname = tccit->GetLevelName(TC_HLT);
      if(HLTredux != 0)
	HLTredux->AddParameter(POT::PD_HLT, HLTname, "raw", HLTname);

    }

  }
}

