#include "wtrp/cputable.h"
#include "wtrp/TrigConf.h"
#include <iostream>
#include <sstream>

namespace wtrp {

  CPUTable::CPUTable(const std::string &name, 
		     const std::string &title, 
		     const std::string &linkText, 
		     const std::string &fileName) : 
    PageData(name, title, linkText, fileName){;}

  void CPUTable::MakeTable(const TrigConf *tc) 
  {
    AddGlobalLink(m_fileName, m_linkText);

    m_dataTable.Clear();

    ColumnGroup L1("L1", "L1");
    L1.AddColumn("name", "Name");
    L1.AddColumn("presc", "Presc.");
    m_dataTable.AddColumnGroup(L1);

    ColumnGroup L2("L2", "L2");
    L2.AddColumn("name", "Name");
    L2.AddColumn("presc", "Presc.");
    L2.AddColumn("FracOF", "Overflows");
    L2.AddColumn("PerCall", "CPU/call");
    L2.AddColumn("Time90", "90%time");
    L2.AddColumn("inRate", "In. rate");
    L2.AddColumn("TotCPU", "totCPU");
    L2.AddColumn("bar_tot", "relCPU", false);
    m_dataTable.AddColumnGroup(L2);

    ColumnGroup EF("EF", "EF");
    EF.AddColumn("name", "Name");
    EF.AddColumn("presc", "Presc.");
    EF.AddColumn("FracOF", "Overflows");
    EF.AddColumn("PerCall", "CPU/call");
    EF.AddColumn("Time90", "90%time");
    EF.AddColumn("inRate", "In. rate");
    EF.AddColumn("TotCPU", "totCPU");
    EF.AddColumn("bar_tot", "relCPU", false); 
    m_dataTable.AddColumnGroup(EF);


    const std::set<TC_chain> &tc_chains = tc->getChains();
    std::set<TC_chain>::const_iterator tccit;

    for(tccit = tc_chains.begin(); tccit != tc_chains.end(); tccit++) {
      AddRow( *tccit );
    }
  }

  CPUTable::~CPUTable(){;}


  void CPUTable::AddRow( const TC_chain &tc_chain )
  {
    Row *row = m_dataTable.AddRow();

    std::string L1name = tc_chain.GetLevelName(TC_L1);
    std::size_t pos = L1name.find(",");
    if(pos != std::string::npos) {
      L1name.insert(pos+1, "...<!-- ");
      L1name += "-->";
    }
    row->UpdateCell("L1" , "name", L1name);
    row->UpdateCell("L2" , "name", tc_chain.GetLevelName(TC_L2));
    row->UpdateCell("EF" , "name", tc_chain.GetLevelName(TC_EF));

    int prescale;
    std::stringstream ss;

    prescale = tc_chain.GetLevelPrescale(TC_L1);
    ss << prescale;
    row->UpdateCell("L1", "presc", ss.str());
    ss.str("");

    prescale = tc_chain.GetLevelPrescale(TC_L2);
    ss << prescale;
    row->UpdateCell("L2", "presc", ss.str());
    ss.str("");

    prescale = tc_chain.GetLevelPrescale(TC_EF);
    ss << prescale;
    row->UpdateCell("EF", "presc", ss.str());
    ss.str("");

  }

  void CPUTable::Dump() const
  {
    m_dataTable.Dump();
  }

  void CPUTable::BookPOTs()
  {
    std::string L2POTName = "L2cpu";
    
    PageData *L2CPUPage = GetPage("L2cpu");
    if( L2CPUPage == 0) {

      L2CPUPage = new PageData(L2POTName, 
                                  "L2 CPU usage vs. time", 
                                  "L2 CPU usage vs. time",
                                  "L2cpuPOT.html");
      AddPOTPage(L2CPUPage);
      AddLocalLink(L2CPUPage->GetOutputFileName(), L2CPUPage->GetPageTitle());
    }

    std::string EFPOTName = "EFcpu";
    PageData *EFCPUPage = GetPage("EFcpu");
    if( EFCPUPage == 0) {
      EFCPUPage = new PageData(EFPOTName, 
                                  "EF CPU usage vs. time", 
                                  "EF CPU usage vs. time",
                                  "EFcpuPOT.html");
      AddPOTPage(EFCPUPage);
      AddLocalLink(EFCPUPage->GetOutputFileName(), EFCPUPage->GetPageTitle());
    }


    POT *L2cpu = POT::BookPOT(L2POTName, "CPU usage (millisec per sec of real time)");
    if(L2cpu == 0) {
      std::cout << "CPUTable: failed to book L2 cpu usage pot" << std::endl;
    } else if(L2CPUPage != 0) {
      L2CPUPage->AddPOT(L2POTName);
      L2cpu->SetLogOption(true);
      L2cpu->SetFixedYMin(0.09);
    }

    POT *EFcpu = POT::BookPOT(EFPOTName, "CPU usage (millisec per sec of real time)" );
    
    if(EFcpu == 0) {
      std::cout << "CPUTable: failed to book EF cpu usage pot" << std::endl;
    } else if(EFCPUPage != 0) {
      EFCPUPage->AddPOT(EFPOTName);
      EFcpu->SetLogOption(true);
      EFcpu->SetFixedYMin(0.09);
    }
    
    unsigned int nRows = m_dataTable.GetNRows();
    const std::vector<Row> *rows = m_dataTable.GetRows();
    
    for(unsigned int iRow = 0; iRow < nRows; iRow++) {
      
      const std::string *L2name = (*rows)[iRow].GetContents_S("L2", "name");
      const std::string *EFname = (*rows)[iRow].GetContents_S("EF", "name");
      
      if(L2cpu != 0)
	L2cpu->AddParameter(POT::PD_L2CPU, *L2name, "TotCPU", *L2name);
      
      if(EFcpu != 0)
	EFcpu->AddParameter(POT::PD_EFCPU, *EFname, "TotCPU", *EFname);
    }
  }

}
