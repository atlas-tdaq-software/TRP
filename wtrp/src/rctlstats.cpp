#include "wtrp/rctlstats.h"
#include <iostream>

namespace wtrp {

  std::map<std::string, std::map<std::string, double>* > RCtlStats::m_varMap;
  std::map<std::string, bool> RCtlStats::m_freshMap;
  bool RCtlStats::m_allValid;
  long long RCtlStats::m_now;

  RCtlStats::RCtlStats()
  { ; }
  
  RCtlStats::~RCtlStats() {
    Reset();
  }

  void RCtlStats::Reset(){

    for (std::map<std::string, std::map<std::string, double>* >::iterator iv =  m_varMap.begin();
	 iv != m_varMap.end(); iv++)
      delete iv->second;

    m_varMap.clear();
  }
  
  void RCtlStats::GetFromServer(const ISInfoDictionary &dict) {
    
    Reset();
    m_freshMap.clear();
    m_allValid = true;

    m_now = OWLTime().total_mksec_utc();

    L2SV l2sv;
    if (GetISInfo(dict, "RunCtrlStatistics.L2SV-SUM", l2sv)){
      SetL2SV(l2sv);	 
    } else {
      m_allValid = false;
      //std::cout << "RCtlStats: L2SV-SUM not found" << std::endl;
    }

    L2PU l2pu;
    if (GetISInfo(dict, "RunCtrlStatistics.L2PU-SUM", l2pu)){
      SetL2PU(l2pu);
    } else {
      m_allValid = false;
      //std::cout << "RCtlStats: L2PU-SUM not found" << std::endl;
    }

    DF_IS_Info::SFI sfi;
    if (GetISInfo(dict, "RunCtrlStatistics.SFI-SUM", sfi)){
      SetSFI(sfi);
    } else {
      m_allValid = false;
      //std::cout << "RCtlStats: SFI-SUM not found" << std::endl;
    }

    DF_IS_Info::SFO sfo;
    if (GetISInfo(dict, "RunCtrlStatistics.SFO-SUM", sfo)){
      SetSFO(sfo);
    } else {
      m_allValid = false;
      //std::cout << "RCtlStats: EFD-SUM not found" << std::endl;
    }

    //Dump();
  }

  bool RCtlStats::isFresh(const ISInfo &info)
  {
    // the update rate for these servers is once per 12 seconds. It's enough to check 
    // if this update happened in the last minute

    long long updateTime = info.time().total_mksec_utc();

    /*
    long long tsince = (m_now - updateTime) / 1000000;
    std::cout << "Time since last update: " <<  tsince << std::endl;
    */

    if ((m_now - updateTime) > 60000000)
      return false;

    return true;
  }

  bool RCtlStats::GetVar(const std::string &is_class, const std::string &name, double &val) { 

    std::map<std::string, std::map<std::string, double>* >::const_iterator iMap = m_varMap.find(is_class);

    if(iMap == m_varMap.end() )
      return false;

    const std::map<std::string, double> *aMap = (*iMap).second;
    std::map<std::string, double>::const_iterator a = (*aMap).find(name);
    if( a == (*aMap).end() )
      return false;

    val = (*a).second;
      return true;
  }

  void RCtlStats::SetL2SV( L2SV &l2sv) {

    std::map<std::string, double> *l2sv_map = new std::map<std::string, double>;
    if(l2sv_map == 0)
      return;

    m_freshMap["L2SV"] = isFresh(l2sv);

    (*l2sv_map)["errors"]              =  double(l2sv.errors);
    (*l2sv_map)["disableCnt"]          =  double(l2sv.disableCnt);
    (*l2sv_map)["DFM_XOFF"]            =  double(l2sv.DFM_XOFF);
    (*l2sv_map)["accumulatedXoffTime"] =        (l2sv.accumulatedXoffTime); 
    (*l2sv_map)["LVL1_events"]         =  double(l2sv.LVL1_events);
    (*l2sv_map)["LVL2_events"]         =  double(l2sv.LVL2_events);
    (*l2sv_map)["AcceptedEvents"]      =  double(l2sv.AcceptedEvents);
    (*l2sv_map)["RejectedEvents"]      =  double(l2sv.RejectedEvents);
    (*l2sv_map)["ForcedAccepts"]       =  double(l2sv.ForcedAccepts);
    (*l2sv_map)["IntervalTime"]        =        (l2sv.IntervalTime);
    (*l2sv_map)["IntervalEventRate"]   =        (l2sv.IntervalEventRate);
    (*l2sv_map)["AvgEventRate"]        =        (l2sv.AvgEventRate);
    
    m_varMap["L2SV"] = l2sv_map;
  }
  

  void RCtlStats::SetL2PU( L2PU &l2pu){

    std::map<std::string, double> *l2pu_map = new std::map<std::string, double>;
    if(l2pu_map == 0)
      return;

    m_freshMap["L2PU"] = isFresh(l2pu);
    
    (*l2pu_map)["Errors"]           = double(l2pu.Errors);
    (*l2pu_map)["disableCnt"]       = double(l2pu.disableCnt);
    (*l2pu_map)["NumLVL1Results"]   = double(l2pu.NumLVL1Results);
    (*l2pu_map)["NumLVL2Accepts"]   = double(l2pu.NumLVL2Accepts);
    (*l2pu_map)["NumLVL2Rejects"]   = double(l2pu.NumLVL2Rejects);
    (*l2pu_map)["NumLVL2Decisions"] = double(l2pu.NumLVL2Decisions);
    (*l2pu_map)["IntervalTime"]     =       (l2pu.IntervalTime);
    (*l2pu_map)["LVL2IntervalRate"] =       (l2pu.LVL2IntervalRate);
    (*l2pu_map)["LVL2AverageRate"]  =       (l2pu.LVL2AverageRate);
    (*l2pu_map)["TotalDataRate"]    =       (l2pu.TotalDataRate);

    m_varMap["L2PU"] = l2pu_map;
  }

  void RCtlStats::SetSFI( DF_IS_Info::SFI &sfi){

    std::map<std::string, double> *sfi_map = new std::map<std::string, double>;
    if(sfi_map == 0)
      return;

    m_freshMap["SFI"] = isFresh(sfi);
    
    (*sfi_map)["EventsAssigned"]             = double(sfi.EventsAssigned);
    (*sfi_map)["EventsCreated"]              = double(sfi.EventsCreated);
    (*sfi_map)["EventsBuilt"]                = double(sfi.EventsBuilt);
    (*sfi_map)["EventsIncompletelyBuilt"]    = double(sfi.EventsIncompletelyBuilt);
    (*sfi_map)["EventsDeleted"]              = double(sfi.EventsDeleted);
    (*sfi_map)["ActualEoERate"]              =       (sfi.ActualEoERate);
    (*sfi_map)["RunAverageEoERate"]          =       (sfi.RunAverageEoERate);
    (*sfi_map)["ActualDeletedEventRate"]     =       (sfi.ActualDeletedEventRate);
    (*sfi_map)["RunAverageDeletedEventRate"] =       (sfi.RunAverageDeletedEventRate);
    (*sfi_map)["NumBusyCount"]               = double(sfi.NumBusyCount);
    (*sfi_map)["NumNonBusyMessages"]         = double(sfi.NumNonBusyMessages);
    (*sfi_map)["NumRequests"]                = double(sfi.NumRequests);
    (*sfi_map)["ActualRequestRate"]          =       (sfi.ActualRequestRate);
    (*sfi_map)["NumReasks"]                  = double(sfi.NumReasks);
    (*sfi_map)["NumTimeouts"]                = double(sfi.NumTimeouts);
    (*sfi_map)["NumFragments"]               = double(sfi.NumFragments);
    (*sfi_map)["NumFragmentsNotInserted"]    = double(sfi.NumFragmentsNotInserted);
    (*sfi_map)["EventDuplicWarnings"]        = double(sfi.EventDuplicWarnings);
    (*sfi_map)["NumMissingL2Results"]        = double(sfi.NumMissingL2Results);
    (*sfi_map)["NumBCIDErrors"]              = double(sfi.NumBCIDErrors);
    (*sfi_map)["Payload"]                    =       (sfi.Payload);
    (*sfi_map)["ActualPayloadRate"]          =       (sfi.ActualPayloadRate);
    (*sfi_map)["RunAveragePayloadRate"]      =       (sfi.RunAveragePayloadRate);
    (*sfi_map)["FragmentPayload"]            =       (sfi.FragmentPayload);
    (*sfi_map)["EventPayload"]               =       (sfi.EventPayload);
    (*sfi_map)["EventsCheckedForMonitoring"] = double(sfi.EventsCheckedForMonitoring);
    (*sfi_map)["MonitoredEventCount"]        = double(sfi.MonitoredEventCount);
    (*sfi_map)["ActualMonitoredEventRate"]   =       (sfi.ActualMonitoredEventRate);
    (*sfi_map)["Number_of_disabled_ROSs"]    = double(sfi.Number_of_disabled_ROSs);

    m_varMap["SFI"] = sfi_map;

  }

  void RCtlStats::SetSFO( DF_IS_Info::SFO &sfo){

    std::map<std::string, double> *sfo_map = new std::map<std::string, double>;
    if(sfo_map == 0)
      return;

    m_freshMap["SFO"] = isFresh(sfo);

    (*sfo_map)["EventsReceived"]           = double(sfo.EventsReceived);
    (*sfo_map)["EventsSaved"]              = double(sfo.EventsSaved);
    (*sfo_map)["DataVolumeReceived"]       =       (sfo.DataVolumeReceived);
    (*sfo_map)["DataVolumeSaved"]          =       (sfo.DataVolumeSaved);
    (*sfo_map)["CurrentEventReceivedRate"] =       (sfo.CurrentEventReceivedRate);
    (*sfo_map)["CurrentDataReceivedRate"]  =       (sfo.CurrentDataReceivedRate);
    (*sfo_map)["CurrentEventSavedRate"]    =       (sfo.CurrentEventSavedRate);
    (*sfo_map)["CurrentDataSavedRate"]     =       (sfo.CurrentDataSavedRate);
    (*sfo_map)["SamplingTime"]             =       (sfo.SamplingTime);

    m_varMap["SFO"] = sfo_map;
  }

  bool RCtlStats::GetISInfo(const ISInfoDictionary &dict, const std::string &infoName, ISInfo &info)
  {
    try {
      dict.getValue( infoName, info );
    }
    catch (daq::is::InvalidName&) {
      //std::cout << "rctlstats:GetISInfo: failed to get " << infoName << ": InvalidName" << std::endl;
      return false;
    }
    catch (daq::is::RepositoryNotFound&) {
      //std::cout << "rctlstats:GetISInfo: failed to get " << infoName << ": Repository not found" << std::endl;
      return false;
    }
    catch (daq::is::InfoNotFound&) {
      //std::cout << "rctlstats:GetISInfo: failed to get " << infoName << ": Info not found" << std::endl;
      return false;
    }
    catch (daq::is::InfoNotCompatible&) {
      //std::cout << "rctlstats:GetISInfo: failed to get " << infoName << ": Info not compatible" << std::endl;
      return false;
    }
    return true;
  }

  void RCtlStats::Dump() {

    std::cout << "************** Dump of RCtrlStats *******************" << std::endl;


    for(std::map<std::string, std::map<std::string, double>* >::iterator iMap = m_varMap.begin()
	  ; iMap != m_varMap.end(); iMap++) {

      std::cout << iMap->first << std::endl;
      
      std::map<std::string, double> *aMap = (*iMap).second;
      
      for(std::map<std::string, double>::iterator a = (*aMap).begin(); a != (*aMap).end(); a++){
	std::cout << "     " << a->first << ": " << a->second << std::endl;
      }
    }
    
  }
  
}
