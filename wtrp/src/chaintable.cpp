#include "wtrp/chaintable.h"
#include "wtrp/TrigConf.h"
#include <iostream>
#include <sstream>

namespace wtrp {

  const std::string ChainTable::m_allChains = "AllChains";

  ChainTable::ChainTable(const std::string &name, const std::string &title, const std::string &fileName) : 
    PageData(name, title, "", fileName){;}

  void ChainTable::MakeTable(const std::string &groupName, const TrigConf *tc) 
  {

    m_dataTable.Clear();
    
    m_groupName = groupName;

    ColumnGroup L1("L1", "L1");
    L1.AddColumn("name", "Name");
    L1.AddColumn("presc", "Presc.");
    L1.AddColumn("frac_out", "Out rate");
    L1.AddColumn("bar_out", "Out frac", false);
    m_dataTable.AddColumnGroup(L1);


    if(tc->IsRun1Configuration() ) {
      ColumnGroup L2("L2", "L2");
      L2.AddColumn("name", "Name");
      L2.AddColumn("presc", "Presc.");
      L2.AddColumn("frac_in", "In rate");
      L2.AddColumn("bar_in", "In frac", false);
      L2.AddColumn("frac_raw", "Acc.");
      L2.AddColumn("frac_out", "Out rate");
      L2.AddColumn("bar_out", "Out frac", false);
      m_dataTable.AddColumnGroup(L2);
      
      ColumnGroup EF("EF", "EF");
      EF.AddColumn("name", "Name");
      EF.AddColumn("presc", "Presc.");
      EF.AddColumn("frac_in", "In rate");
      EF.AddColumn("bar_in", "In frac", false);
      EF.AddColumn("frac_raw", "Acc.");
      EF.AddColumn("frac_out", "Out rate");
      EF.AddColumn("bar_out", "Out frac", false);
      EF.AddColumn("streams", "Streams", true);
      m_dataTable.AddColumnGroup(EF);
    } else {
      ColumnGroup HLT("HLT", "HLT");
      HLT.AddColumn("name", "Name");
      HLT.AddColumn("presc", "Presc.");
      HLT.AddColumn("frac_in", "In rate");
      HLT.AddColumn("bar_in", "In frac", false);
      HLT.AddColumn("frac_raw", "Acc.");
      HLT.AddColumn("frac_out", "Out rate");
      HLT.AddColumn("bar_out", "Out frac", false);
      HLT.AddColumn("streams", "Streams", true);
      m_dataTable.AddColumnGroup(HLT);
    }

  }
  ChainTable::~ChainTable(){;}


  void ChainTable::AddRow( const TrigConf *tc, const TC_chain &tc_chain )
  {
    Row *row = m_dataTable.AddRow();

    std::string L1name = tc_chain.GetLevelName(TC_L1);
    std::size_t pos = L1name.find(",");
    if(pos != std::string::npos) {
      L1name.insert(pos+1, "...<!-- ");
      L1name += "-->";
    }
    row->UpdateCell("L1" , "name", L1name);
    int prescale;
    std::stringstream ss;

    prescale = tc_chain.GetLevelPrescale(TC_L1);
    ss << prescale;
    row->UpdateCell("L1", "presc", ss.str());
    ss.str("");

    if(tc->IsRun1Configuration() ) {
      row->UpdateCell("L2" , "name", tc_chain.GetLevelName(TC_L2));
      prescale = tc_chain.GetLevelPrescale(TC_L2);
      ss << prescale;
      row->UpdateCell("L2", "presc", ss.str());
      ss.str("");

      row->UpdateCell("EF" , "name", tc_chain.GetLevelName(TC_EF));
      prescale = tc_chain.GetLevelPrescale(TC_EF);
      ss << prescale;
      row->UpdateCell("EF", "presc", ss.str());
      ss.str("");

    } else {
      row->UpdateCell("HLT" , "name", tc_chain.GetLevelName(TC_HLT));
      prescale = tc_chain.GetLevelPrescale(TC_HLT);
      ss << prescale;
      row->UpdateCell("HLT", "presc", ss.str());
      ss.str("");
    }

    const std::set<std::string> theStreams = tc_chain.GetStreams();
    std::string streamNames = "";
    bool first = true;
    for(std::set<std::string>::const_iterator is = theStreams.begin();
	is != theStreams.end(); is++) {

      float streamPrescale = tc->GetStreamPrescale(*is, tc_chain.GetLevelName(TC_HLT));

      if(!first) {
	streamNames += ", ";
      }
      //std::cout << "stream: " << *is << " chain:: " << tc_chain.GetLevelName(TC_HLT)
      //	<< " prescale: " << streamPrescale << std::endl; 

      streamNames += *is;
      if(streamPrescale > -0.5) {
	std::stringstream ss;
	ss << streamPrescale;
	streamNames += "(" + ss.str() + ")";
      }
      first = false;
    }
    if(tc->IsRun1Configuration() ) {
      row->UpdateCell("EF", "streams", streamNames);
    } else {
      row->UpdateCell("HLT", "streams", streamNames);
    }
  }

  void ChainTable::MakeTableSet(std::map<std::string, ChainTable> &chainTables, const TrigConf *tc)
  {
    chainTables.clear();

    // Make one group which has the full set of chains

    ChainTable cta(m_allChains, m_allChains, "ch_" + m_allChains + ".html");
    cta.MakeTable(m_allChains, tc);

    AddGlobalLink("ch_" + m_allChains + ".html", "Chain rates");

    if(tc->IsRun1Configuration() ) {
      cta.AddLocalLink(m_EFChainPotFileName, "EF chain rates");
      cta.AddLocalLink(m_L2ChainPotFileName, "L2 chain rates");
    } else {
      cta.AddLocalLink(m_HLTChainPotFileName, "HLT chain rates");
    }

    chainTables[m_allChains] = cta;

    const std::set<std::string> groupNames = tc->GetGroupNames();

    for(std::set<std::string>::const_iterator gnit = groupNames.begin(); 
	gnit != groupNames.end(); gnit++) {
      ChainTable ct(*gnit, *gnit, "ch_" + *gnit + ".html");
      ct.MakeTable(*gnit, tc);
      chainTables[*gnit] = ct;
    }

    const std::set<TC_chain> &tc_chains = tc->getChains();
    std::set<TC_chain>::const_iterator tccit;

    for(tccit = tc_chains.begin(); tccit != tc_chains.end(); tccit++) {

      chainTables[m_allChains].AddRow( tc, *tccit );

      const std::set<std::string> &groups = tccit->GetGroups();

      for(std::set<std::string>::const_iterator gnit = groups.begin(); 
	  gnit != groups.end(); gnit++) {
	if(chainTables.find( *gnit) == chainTables.end()) {
	  std::cout << "MakeTableSet: group " << *gnit << "is not in the map" << std::endl;
	  continue;
	}
	chainTables[*gnit].AddRow( tc, *tccit );
      }

    }

  }


  void ChainTable::Dump() const
  {
    m_dataTable.Dump();
  }

  // This is obsolete
  void ChainTable::BookPOTs(std::map<std::string, ChainTable> &chainTables )
  {
    for( std::map<std::string, ChainTable>::iterator chit = chainTables.begin();
	 chit != chainTables.end(); chit++) { //chaintable loop
      
      const DataTable *dt = chit->second.GetTable();
      unsigned int nRows = dt->GetNRows();
      if(nRows == 0)
	return;
      
      const std::vector<Row> *rows = dt->GetRows();
      
      
      
      std::string L2POTName = "chL2_" + chit->first;

      POT *L2ch = POT::BookPOT(L2POTName, "Reduction per L2 chain ");
      if(L2ch != 0)
	chit->second.AddPOT(L2POTName);
      
      std::string EFPOTName = "chEF_" + chit->first;
      POT *EFch = POT::BookPOT(EFPOTName, "Reduction per EF chain " );
      if(EFch != 0)
	chit->second.AddPOT(EFPOTName);

      for(unsigned int iRow = 0; iRow < nRows; iRow++) {
	
	const std::string *L2name = (*rows)[iRow].GetContents_S("L2", "name");
	const std::string *EFname = (*rows)[iRow].GetContents_S("EF", "name");
		
	if(L2ch != 0)
	  L2ch->AddNormalizedParameter(POT::PD_L2, *L2name, "output", POT::PD_L2, *L2name, "input", *L2name);
	if(EFch != 0)
	  EFch->AddNormalizedParameter(POT::PD_EF, *EFname, "output", POT::PD_EF, *EFname, "input", *EFname);
      }
    }
  }

}
