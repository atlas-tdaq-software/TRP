#include "wtrp/statuspage.h"
#include "wtrp/TrigConf.h"

#include <iostream>

namespace wtrp {

  StatusPage::StatusPage(const std::string &name, 
			 const std::string &title, 
			 const std::string &linkText, 
			 const std::string &fileName) : 
    PageData(name, title, linkText, fileName){

    m_vars.push_back("Run");
    m_vars.push_back("Time Stamp");
    m_vars.push_back("Lumi Block");
    m_vars.push_back("Master Key");
    m_vars.push_back("L1 Prescale Key");
    m_vars.push_back("HLT Prescale Key");
    m_vars.push_back("Number of columns");
    m_vars.push_back("Number of rows");

  }


  void StatusPage::MakeTable()
  {

    AddGlobalLink(m_fileName, m_linkText);
    m_dataTable.SetSortable(false);
    m_dataTable.Clear();
    
    ColumnGroup cg_var("var", "");
    cg_var.AddColumn("name", "xlabel");
    cg_var.AddColumn("L1", "L1");
    cg_var.AddColumn("L2", "L2");
    cg_var.AddColumn("EF", "EF");
    m_dataTable.AddColumnGroup(cg_var);

    for(std::vector<std::string>::const_iterator ivar = m_vars.begin(); ivar != m_vars.end(); ivar++) {
      Row *aRow = m_dataTable.AddRow();
      aRow->UpdateCell("var", "name", *ivar);
    }
  }

  StatusPage::~StatusPage() {;}

  void StatusPage::GetPostamble(std::string &postamble) const
  {
    postamble = "";
    const std::string mps = POTBase::GetMissingParameters();

    if(mps == "")
      return;

    postamble = "<br> Missing POT parameters: " + mps + "<br>";
      
  }


  void StatusPage::Dump() {
    std::cout << "***********Dump of the Status Table: " << m_pageName << " *************" << std::endl;
    m_dataTable.Dump();
  }

  
}
