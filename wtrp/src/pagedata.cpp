#include "wtrp/pagedata.h"

namespace wtrp {

  // This is for determining the order in which they appear on each page.
  static const char *knownGlobalLinks[] = {

    "index.html",
    "streams.html",
    "groups.html",
    "l1.html",
    "ch_AllChains.html",
    "cpupage.html",
    "l1raw.html",
    "l2raw.html",
    "l2cpuraw.html",
    "efraw.html",
    "efcpuraw.html",
    "WTRPstatus.html",
    ""
  };
  
  static std::map<std::string, unsigned int > InitGlobalLinkMap()
  {
    unsigned int n = 0;
    std::map<std::string, unsigned int > slinks;
    for( const char **link = knownGlobalLinks; **link != '\0'; link++) {
      slinks[*link] = n++;
    }
    slinks[""] = n;
    
    return slinks;
  }

  const std::string PageData::m_L2ChainPotFileName = "L2chainrate.html";
  const std::string PageData::m_EFChainPotFileName = "EFchainrate.html";
  const std::string PageData::m_HLTChainPotFileName = "HLTchainrate.html";

  std::map<std::string, unsigned int>  PageData::m_knownGlobalLinks = InitGlobalLinkMap();

  std::map<std::string, std::string, class PageData::GlobalLinkSorter> PageData::m_globalLinks;
  std::map<std::string, std::string> PageData::m_fileNameMap;
  std::vector<std::string> PageData::m_globalPreamble;
  DataTable PageData::m_globalRates;

  bool PageData::GlobalLinkSorter::operator() (const std::string &lhs, const std::string &rhs) const
  {
    std::map<std::string, unsigned int >::const_iterator mlhs = PageData::m_knownGlobalLinks.find(lhs);
    std::map<std::string, unsigned int >::const_iterator mrhs = PageData::m_knownGlobalLinks.find(rhs);

    if(mlhs != PageData::m_knownGlobalLinks.end() ) {
      if(mrhs != PageData::m_knownGlobalLinks.end() ) {
	return mlhs->second < mrhs->second;
      }
      return true;
    }
    if(mrhs != PageData::m_knownGlobalLinks.end() ) {
      return false;
    }
    return lhs < rhs; // alphabetic sort if neither is found
  }

  PageData::~PageData()
  {
    for(std::vector<PageData*>::iterator ip =  m_POTPages.begin(); ip != m_POTPages.end(); ip++)
      delete *ip;

    m_POTPages.clear();
  }
  
  PageData *PageData::GetPage(const std::string &name)
  {
    for(std::vector<PageData*>::iterator ip =  m_POTPages.begin(); ip != m_POTPages.end(); ip++)
      if ( (*ip)->GetPageName() == name)
	return *ip;

    return 0;
  }


  void PageData::GetOutputFileName(const std::string &pageName, std::string &fileName)
  {
    if(m_fileNameMap.find(pageName) == m_fileNameMap.end())
      fileName = "";
    else
      fileName = m_fileNameMap[pageName];
  }

  void PageData::AddGlobalLink(const std::string &fileName, const std::string &linkText )
  {
    if( m_globalLinks.find(fileName) == m_globalLinks.end() )
      m_globalLinks[fileName] = linkText;
  }

  void PageData::AddPOT(const std::string &POTName) {
    if( std::find(m_POTNames.begin(), m_POTNames.end(), POTName) == m_POTNames.end())
      m_POTNames.push_back(POTName);
  }

  void PageData::AddPOTPage(PageData *thePage){ 

    const std::string &myName = thePage->GetPageName();

    for(std::vector<PageData *>::iterator ipd = m_POTPages.begin(); ipd != m_POTPages.end(); ipd++) {
      const std::string &theName = (*ipd)->GetPageName();
      if(theName == myName) {
	delete *ipd;
	m_POTPages.erase(ipd);
	break;
      }
    }
    m_POTPages.push_back(thePage);
  }
  
  void PageData::ClearPOTs(){ 

    // note that the POTs themselves are deleted by POTS::ClearPOTs

    m_POTNames.clear(); 
    
    for(std::vector<PageData*>::iterator ip =  m_POTPages.begin(); ip != m_POTPages.end(); ip++)
      delete *ip;
    
    m_POTPages.clear();
  }

  void PageData::FormatLinkLine(std::string &linkLine) const {
    linkLine = "";
    unsigned int gsi = m_globalLinks.size()-1, gisi = 0;    
    for(std::map<std::string, std::string>::const_iterator il = m_globalLinks.begin(); il != m_globalLinks.end(); il++){
      linkLine += "<A HREF=\""  + il->first + "\">" + il->second + "</A>";
      if(gisi++ != gsi)
	linkLine += ", ";
    }
    if(linkLine.size() > 0)
      linkLine += "<br>";

    unsigned int lsi = m_localLinks.size()-1, lisi = 0;    
    for(std::map<std::string, std::string>::const_iterator il = m_localLinks.begin(); il != m_localLinks.end(); il++){
      linkLine += "<A HREF=\""  + il->first + "\">" + il->second + "</A>";
      if(lisi++ != lsi)
	linkLine += ", ";
    }

  }

  void PageData::MakeGlobalRatesTable(bool isRun2)
  {
    if(isRun2) {
      MakeGlobalRatesTable_Run2();
    } else {
      MakeGlobalRatesTable_Run1();
    }
  }

    void PageData::MakeGlobalRatesTable_Run1()
  {
    m_globalRates.Clear();
    
    ColumnGroup cg("rates", "");
    cg.AddColumn("source", "Source");
    cg.AddColumn("CTPin", "L1 out"); // the tag is a bit dated
    cg.AddColumn("L2in", "L2 In");
    cg.AddColumn("EFin", "EF In");
    cg.AddColumn("EFout", "EF Out");

    m_globalRates.AddColumnGroup(cg);
        
    std::set<std::string>::const_iterator rit;

    Row *TRP_Row = m_globalRates.AddRow();
    TRP_Row->UpdateCell("rates", "source", "TRP");

    Row *RCTL_Row = m_globalRates.AddRow();
    RCTL_Row->UpdateCell("rates", "source", "RCTL");
  }

    void PageData::MakeGlobalRatesTable_Run2()
  {
    m_globalRates.Clear();
    
    ColumnGroup cg("rates", "");
    cg.AddColumn("source", "Source");
    cg.AddColumn("CTPin", "L1 out"); // the tag is a bit dated
    cg.AddColumn("HLTin", "HLT In");
    cg.AddColumn("HLTout", "HLT Out");

    m_globalRates.AddColumnGroup(cg);
        
    std::set<std::string>::const_iterator rit;

    Row *TRP_Row = m_globalRates.AddRow();
    TRP_Row->UpdateCell("rates", "source", "TRP");

    Row *RCTL_Row = m_globalRates.AddRow();
    RCTL_Row->UpdateCell("rates", "source", "RCTL");
  }


}
