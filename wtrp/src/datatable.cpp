#include "wtrp/datatable.h"
#include <iostream>

namespace wtrp {

  const std::string DataTable::m_linkCode = "MakeLink";
  const std::string DataTable::m_refCode = "HREF:";
  const std::string DataTable::m_newWindowFunction = "makeNewWindow";
  const std::string DataTable::m_newWindowFileName = "wtrp_scripts/makenewwindow.js";
  const std::string DataTable::m_sortFileName = "wtrp_scripts/sort.js";

  unsigned int DataTable::m_numTables = 0;

  CellContents::CellContents() : m_ct(con_S) {
    m_contents = "U";
    //std::vector<std::string> v;
    //std::string s = "";
    //v.push_back(s);
    //m_contents.push_back(v);
  }

  const std::string CellContents::m_outOfRange = "out of range";

  void CellContents::Replace(const std::string &s)
  {
    m_contents = s;
 }

#ifdef _NOT_WORKING_
  void CellContents::Replace(const std::string &s)
  {
    m_ct = con_S;
    m_contents.erase(m_contents.begin(), m_contents.end()-1);
    std::vector<std::string> v;
    m_contents.push_back(v);
  }
  void CellContents::Replace(const std::vector<std::string> &vs)
  {
    m_ct = con_VS;
    m_contents.erase(m_contents.begin(), m_contents.end()-1);
    m_contents.push_back(vs);
  }

  void CellContents::Replace(const std::vector<std::vector<std::string> > &vvs)
  {
    m_ct = con_VVS;
    m_contents = vvs;
  }
  #endif

  const std::string &CellContents::GetConstCell_S() const
  {
    return m_contents;
  }

#ifdef _NOT_WORKING_
  const std::string &CellContents::GetConstCell_S() const
  {
    std::vector<std::vector<std::string> >::const_iterator vf = m_contents.begin();
    std::vector<std::string>::const_iterator i = (*vf).begin();
    return *(i);
  }

  const std::vector<std::string> &CellContents::GetConstCell_VS() const
  {
    std::vector<std::vector<std::string> >::const_iterator vf = m_contents.begin();
    return *(vf);
  }

  const std::vector<std::vector<std::string> > &CellContents::GetConstCell_VVS() const
  {
    return m_contents;
  }
#endif

  void Column::Dump() const {
    std::cout << "    Column -- key, header: " << m_key << ", " << m_header << std::endl;
  }

  void ColumnGroup::AddColumn(const std::string &key, const std::string &header, 
			      bool isSortable, bool isLink, const std::string &filePref)
  {
    m_columns.push_back( Column(key, header, isSortable, isLink, filePref) );	  
  }

  const Column *ColumnGroup::GetColumn(const std::string &key) const
  {
    
    for(std::vector<Column>::const_iterator i = m_columns.begin(); i != m_columns.end(); i++) {
      if( (*i).GetKey() == key)
	return &(*i);
    }
    return 0;
  }

  const Column *ColumnGroup::GetColumn(unsigned int nCol) const
  {
    if(nCol < m_columns.size()) return &(m_columns[nCol]);
    return 0;
  }

  void ColumnGroup::Dump() const 
  {
    std::cout <<   "Column group --  key, header: " << m_key << ", " << m_header << std::endl;

    std::vector<Column>::const_iterator cit;    
    for(cit = m_columns.begin(); cit != m_columns.end(); cit++)
      cit->Dump();
    
  }


  Row::Row(const DataTable *dt) : m_dt(dt)
  {
    int tot_cols = 0;

    
    const std::vector<ColumnGroup> &rcg = dt->GetColumnGroups();
    for( std::vector<ColumnGroup>::const_iterator icg = rcg.begin(); 
	 icg != rcg.end(); icg++) {
      tot_cols += (*icg).GetNColumns();
    }

    CellContents c = CellContents();
    for(int i = 0; i < tot_cols; i++) {
      m_cells.push_back(c);
    }
  }


  const CellContents *Row::GetConstCell(const std::string &groupKey, const std::string &columnKey) const
  {
    int i = 0;
    const std::vector<ColumnGroup> &rcg = m_dt->GetColumnGroups();
    for( std::vector<ColumnGroup>::const_iterator icg = rcg.begin(); 
	 icg != rcg.end(); icg++) {
      if( (*icg).GetKey() == groupKey) {
	const std::vector<Column> &cols = icg->m_columns;
	for(std::vector<Column>::const_iterator ic = cols.begin(); ic != cols.end(); ic++) {
	  if( (*ic).GetKey() == columnKey) {
	    return &(m_cells[i]);
	  }
	  i++;
	}
      }
      i += (*icg).GetNColumns();
    }
    std::cout << "datatable::Row, cell not found. groupKey, columnKey: " << groupKey << ", " << columnKey << std::endl;
    return 0;
  }

  CellContents *Row::GetCell(const std::string &groupKey, const std::string &columnKey)
  {
    return const_cast<CellContents *>(GetConstCell(groupKey, columnKey));
  }



  void Row::UpdateCell(const std::string &group, const std::string &key, const std::string &contents)
  {

    CellContents *cell = GetCell(group, key);
    if( cell != 0) {
      cell->Replace(contents);
    }else{
      std::cout << "Row::UpdateCell: no cell found corresponding to group " << group << "key " << key << std::endl;
    }
  }
#ifdef _NON_WORKING_
  void Row::UpdateCell(const std::string &group, const std::string &key, const std::vector<std::string> &contents)
  {
    CellContents *cell = GetCell(group, key);
    if( cell != 0)
      cell->Replace(contents);
  }
  void Row::UpdateCell(const std::string &group, const std::string &key, const std::vector<std::vector<std::string> > &contents)
  {
    CellContents *cell = GetCell(group, key);
    if( cell != 0)
      cell->Replace(contents);
  }
#endif

  const std::string *Row::GetContents_S(const std::string &group, const std::string &key) const
  {
    const CellContents *cc = GetConstCell(group, key);
    if(cc != 0)
      return &(cc->GetConstCell_S());

    return 0;
  }
#ifdef _NON_WORKING_
  const std::vector<std::string> &Row::GetContents_VS(const std::string &group, const std::string &key) const
  {
    return GetConstCell(group, key)->GetConstCell_VS();
  }

  const std::vector<std::vector<std::string> > &Row::GetContents_VVS(const std::string &group, const std::string &key) const
  {
    return GetConstCell(group, key)->GetConstCell_VVS();
  }
#endif

  const std::string *Row::GetContents_S(unsigned int iCol) const
  {
      if(iCol >= m_cells.size())
	return 0;

      return &(m_cells[iCol].GetConstCell_S());
  }


  void Row::GetHTMLContents(unsigned int iCol, std::string &contents) const
  {
      if(iCol >= m_cells.size())
	contents = CellContents::m_outOfRange;

      const std::string &con = m_cells[iCol].GetConstCell_S();

      // insert link codes is needed

      Column const* col = GetColumn(iCol);
      if(col != 0 && col->IsALink() ) {
	std::string linkCode, refCode;
	DataTable::GetLinkCodes(linkCode, refCode);
	contents = linkCode + con + refCode + col->GetFilePrefix() + con + ".html";
      } else {
	contents = con;
      }      
  }

  Column const*Row::GetColumn(unsigned int iCol) const
  {
    unsigned int i = 0;

    std::vector<ColumnGroup>::const_iterator cgit;
    const std::vector<ColumnGroup>  &groups = m_dt->GetColumnGroups();
    for(cgit = groups.begin(); cgit != groups.end(); cgit++) {
      int nCols = cgit->GetNColumns();
      if(iCol < i+nCols) {
	return cgit->GetColumn(iCol-i);
      }
      i += nCols;
    }
    return 0;
  }


  void Row::Dump() const
  {
    std::cout << "  Row (quick and dirty, assumed all cells contain strings)" << std::endl;
    std::vector<CellContents >::const_iterator ccit;
    int iCell = 0;
    for(ccit = m_cells.begin(); ccit != m_cells.end(); ccit++) {
      std::cout << "    " << iCell++ << ": " << &(*ccit) << " " << ccit->GetConstCell_S() << std::endl;
    }

  }

    
  DataTable::DataTable(bool isSortable) : m_isSortable(isSortable), m_reverse(false)
  { 
    m_nTable = ++m_numTables;
  }

  DataTable::~DataTable(){;}
    
  void DataTable::Clear()
  {
    m_columnGroups.clear();
    m_rows.clear();
  }


  Row *DataTable::AddRow()
  {
    Row r(this);
    m_rows.push_back( r );
    std::vector<Row>::iterator rit = (m_rows.end() - 1);
    return &(*rit);

  } 

  bool DataTable::RemoveRow(const std::string &groupKey, const std::string &colKey, const std::string &key)
  {
    std::vector<Row>::iterator rit;
    for(rit = m_rows.begin(); rit != m_rows.end(); rit++){
      const std::string *con = rit->GetContents_S(groupKey, colKey);
      if( (con != 0) && (*con == key) ) {
	m_rows.erase(rit);
	return true;
      }
    }
    return false;
  }

     
  const ColumnGroup *DataTable::GetColumnGroup(const std::string &key)
  {
    std::vector<ColumnGroup>::const_iterator cgit;
    for(cgit = m_columnGroups.begin(); cgit != m_columnGroups.end(); cgit++) {
      if( cgit->GetKey() == key)
	return &(*cgit);
    }
    return 0;
  }

  const Column *DataTable::GetColumn(const std::string &groupKey, const std::string &columnKey)
  {
  
    std::vector<ColumnGroup>::const_iterator cgit;
    for(cgit = m_columnGroups.begin(); cgit != m_columnGroups.end(); cgit++) {

      if( cgit->GetKey() == groupKey) {
	return cgit->GetColumn(columnKey);
      }

    }
    return 0;
  }

  // Get the first row which matches the given critereon
  Row *DataTable::GetRow(const std::string &groupKey, const std::string &colKey, const std::string &key)
  {
    std::vector<Row>::iterator rit;
    for(rit = m_rows.begin(); rit != m_rows.end(); rit++){
      const std::string *con = rit->GetContents_S(groupKey, colKey);
      if( (con != 0) && (*con == key) ) {
	return &(*rit);
      }
    }

    return 0;
  }
  // Get all matching rows. Return the number found
  unsigned int DataTable::GetRows(const std::string &groupKey, const std::string &colKey, 
		       const std::string &key, std::vector<Row *> &theRows)
  {
    theRows.clear();
    unsigned int nFound = 0;

    std::vector<Row>::iterator rit;
    for(rit = m_rows.begin(); rit != m_rows.end(); rit++){
      const std::string *con = rit->GetContents_S(groupKey, colKey);
      if( (con != 0) && (*con == key) ) {
	theRows.push_back( & (*rit) );
	nFound++;
      }
    }

    return nFound;

  }

  const Row *DataTable::GetConstRow(const std::string &groupKey, const std::string &colKey, const std::string &key) const
  {
    std::vector<Row>::const_iterator rit;
    for(rit = m_rows.begin(); rit != m_rows.end(); rit++){
      if( *(rit->GetContents_S(groupKey, colKey)) == key)
	return &(*rit);
    }
    return 0;

  }

  unsigned int DataTable::GetNColumns() const
  {
    int nCol = 0;
    std::vector<ColumnGroup>::const_iterator cgit;
    for(cgit = m_columnGroups.begin(); cgit != m_columnGroups.end(); cgit++)
      nCol += cgit->GetNColumns();

    return nCol;
  }

  void DataTable::GetCombinedHeader(unsigned int iCol, std::string &header) const
  // formats a string with columngroup header: column header for col i. 
  {
    header.clear();
    unsigned int i = 0;

    std::vector<ColumnGroup>::const_iterator cgit;
    for(cgit = m_columnGroups.begin(); cgit != m_columnGroups.end(); cgit++) {
      int nCols = cgit->GetNColumns();
      if(iCol < i+nCols) {
	const std::string &groupHeader = cgit->GetHeader();
	const Column *theCol = cgit->GetColumn(iCol-i);
	if(theCol != 0) {
	  const std::string colHeader = theCol->GetHeader();
	  if(groupHeader != "") {
	    header = groupHeader;
	    if(colHeader != "")
	      header += ": " + colHeader;
	  } else
	    header = colHeader;
	} else {
	  header = groupHeader;
	}
	break;
      }
      i += nCols;
    }
  }

  bool DataTable::IsColumnSortable(unsigned int iCol) const
  {
    unsigned int i = 0;

    std::vector<ColumnGroup>::const_iterator cgit;
    for(cgit = m_columnGroups.begin(); cgit != m_columnGroups.end(); cgit++) {
      int nCols = cgit->GetNColumns();
      if(iCol < i+nCols) {
	const Column *theCol = cgit->GetColumn(iCol-i);
	if(theCol != 0) {
	  return theCol->IsSortable();
	} else {
	  return false;
	}
	break;
      }
      i += nCols;
    }
    return false;
  }

  void DataTable::Dump() const
  {
    std::cout << "*********Begin Dump ****************" << std::endl;


    std::cout << "Column Groups" << std::endl;
    std::vector<ColumnGroup>::const_iterator cgit;
    for(cgit = m_columnGroups.begin(); cgit != m_columnGroups.end(); cgit++)
      cgit->Dump();

    std::cout << "Rows" << std::endl;
    std::vector<Row>::const_iterator rit;
    for(rit = m_rows.begin(); rit != m_rows.end(); rit++)
      rit->Dump();

    std::cout << "*********End Dump ****************" << std::endl;

  }
} // namespace wtrp
  

