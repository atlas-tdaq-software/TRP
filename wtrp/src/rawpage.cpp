#include "wtrp/rawpage.h"
#include "wtrp/TrigConf.h"

#include <iostream>

namespace wtrp {

  RawPage::RawPage(const std::string &name, 
		   const std::string &title, 
		   const std::string &linkText,
		   const std::string &fileName) : 
    PageData(name, title, linkText, fileName){;}

  void RawPage::MakeTable(const TimePoint_IS &tp)
  {

    AddGlobalLink(m_fileName, m_linkText);
    m_dataTable.Clear();
    

    m_nEntries = tp.XLabels.size();

    if( tp.XLabels.size() == 0 || tp.YLabels.size() == 0)
      return;

    ColumnGroup cg_raw("tp", "");
    
    cg_raw.AddColumn("name", "Name");

    std::vector<std::string>::const_iterator yl;
    for( yl = tp.YLabels.begin(); yl != tp.YLabels.end(); yl++) {
      cg_raw.AddColumn(*yl, *yl);
    }
    
    m_dataTable.AddColumnGroup(cg_raw);

    std::vector<std::string>::const_iterator xl;

    for( xl = tp.XLabels.begin(); xl != tp.XLabels.end(); xl++) {
      Row *aRow = m_dataTable.AddRow();
      aRow->UpdateCell("tp", "name", *xl);
    }

  }

  RawPage::~RawPage() {;}

  void RawPage::Dump() {
    std::cout << "***********Dump of the Raw Table: " << m_pageName << " *************" << std::endl;
    m_dataTable.Dump();
  }

  
}
