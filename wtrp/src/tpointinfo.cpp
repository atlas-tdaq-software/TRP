#include "wtrp/tpointinfo.h"
#include "wtrp/OnlineTools.h"


namespace wtrp {

  unsigned int TPointInfo::m_current_run = 0;
  unsigned int TPointInfo::m_current_lumiBlock = 0;
  unsigned int TPointInfo::m_current_smk = 0;
  unsigned int TPointInfo::m_current_L1_psk = 0;
  unsigned int TPointInfo::m_current_HLT_psk = 0;

  TPointInfo TPointInfo::m_L1Info("UTripServerIS.L1_Rate");
  TPointInfo TPointInfo::m_L2Info("UTripServerIS.L2_Rate");
  TPointInfo TPointInfo::m_EFInfo("UTripServerIS.EF_Rate");
  TPointInfo TPointInfo::m_HLTInfo("UTripServerIS.HLT_Rate");

  TPointInfo TPointInfo::m_L2_CPUInfo("UTripServerIS.L2_CPU");
  TPointInfo TPointInfo::m_EF_CPUInfo("UTripServerIS.EF_CPU");
  TPointInfo TPointInfo::m_HLT_CPUInfo("UTripServerIS.HLT_CPU");

  bool TPointInfo::m_validKeys = false;
  bool TPointInfo::m_updateKeys = true;

  bool TPointInfo::Update(const ISInfoDictionary &dict, const TrigConf *tc)
  {
    bool isValid = true;

    isValid |= m_L1Info.GetTimePoints(dict);

      // Set the run number, if L1Info is valid
    if(isValid) {
      m_current_run = m_L1Info.m_timePoint.RunNumber;
      m_current_lumiBlock = m_L1Info.m_timePoint.LumiBlock;
    }
    if(tc != 0 && tc->IsRun1Configuration()) {

      isValid |= m_L2Info.GetTimePoints(dict);
      isValid |= m_EFInfo.GetTimePoints(dict);
      isValid |= m_L2_CPUInfo.GetTimePoints(dict);
      isValid |= m_EF_CPUInfo.GetTimePoints(dict);

      // Set the run number, if L1Info is not valid
      if(!m_L1Info.m_isValid) {
	if(m_L2Info.m_isValid) {
	  m_current_run = m_L2Info.m_timePoint.RunNumber;
	  m_current_lumiBlock = m_L2Info.m_timePoint.LumiBlock;
	} else if(m_EFInfo.m_isValid) {
	  m_current_run = m_EFInfo.m_timePoint.RunNumber;
	  m_current_lumiBlock = m_EFInfo.m_timePoint.LumiBlock;
	}
      }
    } else {
      isValid |= m_HLTInfo.GetTimePoints(dict);
      isValid |= m_HLT_CPUInfo.GetTimePoints(dict);

      // Set the run number, if L1Info is not valid
      if(!m_L1Info.m_isValid) {
	if(m_HLTInfo.m_isValid) {
	  m_current_run = m_HLTInfo.m_timePoint.RunNumber;
	  m_current_lumiBlock = m_HLTInfo.m_timePoint.LumiBlock;
	}
      }
    }


    // Note that these keys are not used to decide if a configuration update is needed. 
    // That's done using the keys in runinfo.
    if(m_updateKeys) {

      m_validKeys = false;
      if(m_L1Info.m_timePoint.MetaData.size() > 3) {
	m_validKeys = true;

	std::stringstream ss(m_L1Info.m_timePoint.MetaData[0]);
	ss >> m_current_smk;
	ss.clear();
	
	ss.str(m_L1Info.m_timePoint.MetaData[1]);
	ss >> m_current_L1_psk;
	ss.clear();
	
	ss.str(m_L1Info.m_timePoint.MetaData[2]);
	ss >> m_current_HLT_psk;
      }
    }
    return isValid;
  }

  bool TPointInfo::GetTimePoints(const ISInfoDictionary &dict, bool fetchOneOnly)
  { 
    TimePoint_IS &tp = m_timePoint;
    m_isValid = false;
    m_isFresh = false;
    
    if(fetchOneOnly)
      m_pointsToFetch = 1;

    std::vector<TimePoint_IS> tpvec;
    
    try {
      dict.getValues( m_name, tpvec, m_pointsToFetch );
    }
    catch (daq::is::InvalidName & ) {
      //std::cout << "wtrpPlugin: failed to get " << m_name << ": InvalidName" << std::endl;
      return false;
    }
    catch (daq::is::RepositoryNotFound & ) {
      //std::cout << "wtrpPlugin: failed to get " << m_name << ": Repository not found" << std::endl;
      return false;
    }
    catch (daq::is::InfoNotFound & ) {
      //std::cout << "wtrpPlugin: failed to get " << m_name << ": Info not found" << std::endl;
      return false;
    }
    catch (daq::is::InfoNotCompatible & ) {
      //std::cout << "wtrpPlugin: failed to get " << m_name << ": Info not compatible" << std::endl;
      return false;
    }
    if(tpvec.size() == 0) {
      //std::cout << "wtrpPlugin: failed to get" << m_name << ": no time points returned" << std::endl;
      return false;
    }

    std::vector<TimePoint_IS>::iterator ritp = tpvec.begin(); // seems the latest time is first in the vector
    
    //std::cout << ritp->TimeStamp << " size " << ritp->Data.size() << std::endl;
    if(ritp->TimeStamp <= m_lastUpdate) {
      //std::cout << "things: " << fetchOneOnly << ", " << m_pointsToFetch << ", " << m_consecutiveFails << std::endl;

      if(fetchOneOnly || ++m_consecutiveFails > 1) {
	if(m_pointsToFetch > 2)
	  m_pointsToFetch--;
	return false;
      }
    } else {
      m_consecutiveFails = 0;
    }
    
    unsigned int dsize = 0;
    
    int nPoints = 0;
    int nOld = 0;
    OWLTime lastSeen(0,0); // for getting rid of possible duplicates
    for(; ritp != tpvec.end(); ritp++) {
      
      if(dsize == 0) 
	dsize = ritp->Data.size();
      
      if(dsize < 3)
	continue;
	  
      if(ritp->Data.size() != dsize) {
	std::cout << "warning, different sized time points in history, 1st 2nd: " 
		  << dsize << ", " << ritp->Data.size() << std::endl;
	continue;
      }

      //std::cout << ritp->TimeStamp << " size " << ritp->Data.size() << std::endl;
      if(ritp->TimeStamp <= m_lastUpdate){
	if(++nOld > 1) 
	  break;
	continue;
      } 

      if(ritp->TimeStamp == lastSeen)
	continue;
      lastSeen = ritp->TimeStamp;

      if(nPoints == 0) {
	tp = *ritp;
      } else {
	unsigned int iElement = 0;
	for(std::vector<float>::iterator id = ritp->Data.begin(); id != ritp->Data.end(); id++, iElement++) {
	  tp.Data[iElement] = ( tp.Data[iElement] * nPoints + *id ) / (nPoints + 1);
	}
      }
	m_isFresh = true;
	nPoints++;
    }

    m_lastUpdate = tp.TimeStamp;
    
    //std::cout << "Averaged over " << nPoints << " time points" << std::endl;
    
    if(!fetchOneOnly) {
      //std::cout << "nOld, m_pointsToFetch: " << nOld << ", " << m_pointsToFetch << std::endl;
      if(nOld == 0) {
	if(m_pointsToFetch < 10)
	  m_pointsToFetch++;
      } else if(nOld > 1) {
	if(m_pointsToFetch > 2)
	  m_pointsToFetch--;
      }
    }

    m_isValid = true;
    return true;
  }
    


} // namespace wtrp
