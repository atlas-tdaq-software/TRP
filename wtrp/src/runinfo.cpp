#include "wtrp/runinfo.h"
#include <iostream>
#include <sstream>


namespace wtrp {

  bool RunInfo::m_runParamsValid = false;
  bool RunInfo::m_lumiValid = false;
  bool RunInfo::m_smkValid = false;
  bool RunInfo::m_L1pskValid = false;
  bool RunInfo::m_HLTpskValid = false;
  bool RunInfo::m_updateKeys = true;
  bool RunInfo::m_smkChanged = true;
  bool RunInfo::m_bunchGroupValid = true;
  bool RunInfo::m_ready4PhysicsValid = false;
  bool RunInfo::m_partitionValid = false;

  RunParams RunInfo::m_runParams;
  //LuminosityInfo RunInfo::m_lumiInfo;
  LumiBlock RunInfo::m_lumiInfo;
  unsigned int RunInfo::m_smk = 0;
  unsigned int RunInfo::m_L1psk = 0;
  unsigned int RunInfo::m_HLTpsk = 0;
  unsigned int RunInfo::m_bunchGroup = 0;
  unsigned int RunInfo::m_ready4Physics = 0;

  RunInfo::RunInfo()
  { ; }
  
  RunInfo::~RunInfo() {; }

  /*
    TrigConfL1PsKey
    TrigConfSmKey
    TrigConfHltPsKey
  */

  void RunInfo::GetFromServer(bool partitionValid, const ISInfoDictionary &dict) {
    
    m_partitionValid = partitionValid;

    m_runParamsValid = true;
    if ( !GetISInfo(dict, "RunParams.RunParams", m_runParams)){
      //std::cout << "RunInfo: RunParams not found" << std::endl;
      m_runParamsValid = false;
    }
    
    m_lumiValid = true;
    //if (!GetISInfo(dict, "RunParams.LuminosityInfo", m_lumiInfo)){
    if (!GetISInfo(dict, "RunParams.LumiBlock", m_lumiInfo)){
      //std::cout << "RunInfo: LuminosityInfo not found" << std::endl;
      m_lumiValid = false;
    }

    unsigned int lastSMK = m_smk;
    if(m_updateKeys) {
      m_smkValid    = FetchTrigConfigKey(dict, "RunParams.TrigConfSmKey", m_smk);
      m_L1pskValid  = FetchTrigConfigKey(dict, "RunParams.TrigConfL1PsKey", m_L1psk);
      m_HLTpskValid = FetchTrigConfigKey(dict, "RunParams.TrigConfHltPsKey", m_HLTpsk);
    }

    m_smkChanged = (lastSMK != m_smk);

    {    
      ISInfoAny info;
      m_bunchGroupValid = false;
      //if ( GetISInfo(dict, "L1CT.ISCTPCOREBGKey", info)){ changed 17.6.2015

      if ( GetISInfo(dict, "RunParams.TrigConfL1BgKey", info)){
	if( info.type().entryType( 0 ) == ISType::U32 ) {
	  info >> m_bunchGroup;
	  m_bunchGroupValid = true;
	}
      }
    }

    {    
      ISInfoAny info;
      m_ready4PhysicsValid = false;
      if ( GetISInfo(dict, "RunParams.Ready4Physics", info)){
	//std::cout << info << std::endl;
	bool ready;
	info >> ready;
	//std::cout << "ready or not: " << ready << std::endl;
	m_ready4Physics = (ready)? 1 : 0;
	m_ready4PhysicsValid = true;
      } else {
	//std::cout << "GetISInfo is not amused " << std::endl;
      }
    }

  }

  bool RunInfo::FetchTrigConfigKey(const ISInfoDictionary &dict, const std::string &name, unsigned int &value) {

    ISInfoAny keyInfo;

    if (!GetISInfo(dict, name, keyInfo)) {
      //std::cout << "RunInfo::FetchTrigConfigKey -- " << name << " not found" << std::endl;
      return false;
    } else if( keyInfo.type().entryType( 0 ) != ISType::U32 ) {
      std::cout << "RunInfo::FetchTrigConfigKey: unexpected type for " << name << ": " 
		<< keyInfo.type().entryType( 0 ) << std::endl;
      return false;
    }

    keyInfo >> value;
    return true;
  }



  bool RunInfo::GetRunNumber(unsigned int &runNumber){
    if(!m_runParamsValid){
      runNumber = 0; 
      return false;
    } 
    runNumber=m_runParams.run_number;
    return true;
  }
  /*
  bool RunInfo::GetRunType(std::string &runType){
    
    static unsigned int nCalls = 0;

    std::stringstream ss;
    ss << "Type " << nCalls;
    runType = ss.str();

    nCalls++;
    return true;
  }
  */
  bool RunInfo::GetRunType(std::string &runType){
    if(!m_runParamsValid){
      runType = "N/A"; 
      return false;
    } 
    runType = m_runParams.run_type; 
    return true;
  }

  bool RunInfo::GetLumiBlock(unsigned int &lumiBlock) {
    if(!m_lumiValid) {
      lumiBlock = 0;
      return false;
    }
    lumiBlock = m_lumiInfo.LumiBlockNumber;
    return true;
  }

  bool RunInfo::GetISInfo(const ISInfoDictionary &dict, const std::string &infoName, ISInfo &info)
  {
    try {
      dict.getValue( infoName, info );
    }
    catch (daq::is::InvalidName&) {
      //std::cout << "rctlstats:GetISInfo: failed to get " << infoName << ": InvalidName" << std::endl;
      return false;
    }
    catch (daq::is::RepositoryNotFound&) {
      //std::cout << "rctlstats:GetISInfo: failed to get " << infoName << ": Repository not found" << std::endl;
      return false;
    }
    catch (daq::is::InfoNotFound&) {
      //std::cout << "rctlstats:GetISInfo: failed to get " << infoName << ": Info not found" << std::endl;
      return false;
    }
    catch (daq::is::InfoNotCompatible&) {
      //std::cout << "rctlstats:GetISInfo: failed to get " << infoName << ": Info not compatible" << std::endl;
      return false;
    }
    return true;
  }



}
