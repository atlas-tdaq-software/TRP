#include "wtrp/l1table.h"
#include <iostream>

namespace wtrp {

  void L1Table::MakeTable()
  {
    AddGlobalLink(m_fileName, m_linkText);
    m_dataTable.Clear();

    std::vector<ColumnGroup> colGroups;

    ColumnGroup l1_group("l1", "");
    l1_group.AddColumn("name", "Name");
    l1_group.AddColumn("presc", "Presc");
    l1_group.AddColumn("TBP", "Before PS");
    l1_group.AddColumn("TAP", "After PS");
    l1_group.AddColumn("TAV", "Output");
    l1_group.AddColumn("bar_out", "Out. Frac.", false);

    m_dataTable.AddColumnGroup(l1_group);

    /* 
       Fill in rows based on the time point (in OnlineTools) -- it's
       the only place where the prescales are known.
    */
    return;
  }

  L1Table::~L1Table(){;}

  void L1Table::Dump() {
    std::cout << "****** The L1 Table  ****** " << std::endl;
    m_dataTable.Dump();
  }


  void L1Table::BookPOTs() {

    std::string POTName = "L1";
    POT *L1rate = POT::BookPOT(POTName, "L1 rate (Hz) ");

    if(L1rate == 0)
      return;

    L1rate->SetLogOption(true);
    L1rate->SetFixedYMin(0.09);

    AddPOT(POTName);

    const std::vector<Row> *rows = m_dataTable.GetRows();

    for(std::vector<Row>::const_iterator ir = rows->begin(); ir != rows->end(); ir++){ 

      const std::string *name = ir->GetContents_S("l1", "name");
      if(L1rate != 0)
	L1rate->AddParameter(POT::PD_L1, *name, "TAV", *name);
    }
  }

  /* This is used by wtools::FixupL1ConfigFromTP in "emergencies" */
  void L1Table::AddEntry( const std::string &L1name )
  {
    Row *aRow = m_dataTable.AddRow();
    aRow->UpdateCell("l1", "name", L1name);
  }

}

