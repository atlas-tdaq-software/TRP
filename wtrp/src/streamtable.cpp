#include "wtrp/streamtable.h"
#include "wtrp/TrigConf.h"

#include <iostream>

namespace wtrp {

  StreamTable::StreamTable(const std::string &name, 
			   const std::string &title, 
			   const std::string &linkText, 
			   const std::string &fileName) : 
    PageData(name, title, linkText, fileName){;}

    void StreamTable::MakeTable(const std::set<std::string> &L2streamNames, const std::set<std::string> &EFstreamNames)
  {
    
    AddGlobalLink(m_fileName, m_linkText);

    m_dataTable.Clear();
    
    ColumnGroup cg_stream("str", "");
    cg_stream.AddColumn("lvl", "Lvl");
    cg_stream.AddColumn("str", "Stream Name");
    cg_stream.AddColumn("frac_out", "Out rate");
    cg_stream.AddColumn("bar_out", "Out frac", false);
    m_dataTable.AddColumnGroup(cg_stream);

    std::string typePhysics = "_physics";
    std::string typeExpress = "_express";
    std::string typeCalib = "_calibration";

    for( std::set<std::string>::const_iterator strit = EFstreamNames.begin(); strit != EFstreamNames.end(); strit++) {
      if( strit->rfind(typePhysics, strit->size() - typePhysics.size()) != std::string::npos) {

	Row *aRow = m_dataTable.AddRow();
	aRow->UpdateCell("str", "lvl", "EF");
	aRow->UpdateCell("str", "str", *strit);
      }
    }      
    for( std::set<std::string>::const_iterator strit = EFstreamNames.begin(); strit != EFstreamNames.end(); strit++) {
      if( strit->rfind(typePhysics, strit->size() - typePhysics.size()) == std::string::npos) {

	// if this is a calibration stream, check if it is also at L2. If so, don't include it here
	if( strit->rfind(typeCalib, strit->size() - typeCalib.size()) != std::string::npos) {
	  bool isAtL2 = false;
	  for( std::set<std::string>::const_iterator strit2 = L2streamNames.begin(); 
	       strit2 != L2streamNames.end(); strit2++) {
	    if( *strit == *strit2) {
	      isAtL2 = true;
	      break;
	    }
	  }
	  if(isAtL2) continue;
	}


	Row *aRow = m_dataTable.AddRow();
	aRow->UpdateCell("str", "lvl", "EF");
	aRow->UpdateCell("str", "str", *strit);
      }
    }
    for( std::set<std::string>::const_iterator strit = L2streamNames.begin(); strit != L2streamNames.end(); strit++) {
      if( (strit->rfind(typePhysics, strit->size() - typePhysics.size()) == std::string::npos) && 
	  (strit->rfind(typeExpress, strit->size() - typeExpress.size()) == std::string::npos)) {
	Row *aRow = m_dataTable.AddRow();
	aRow->UpdateCell("str", "lvl", "L2");
	aRow->UpdateCell("str", "str", *strit);
      }
    }  
  }

  void StreamTable::MakeTable(const std::set<std::string> &HLTstreamNames)
  {
    
    AddGlobalLink(m_fileName, m_linkText);

    m_dataTable.Clear();
    
    ColumnGroup cg_stream("str", "");
    cg_stream.AddColumn("lvl", "Lvl");
    cg_stream.AddColumn("str", "Stream Name");
    cg_stream.AddColumn("frac_out", "Out rate");
    cg_stream.AddColumn("bar_out", "Out frac", false);
    m_dataTable.AddColumnGroup(cg_stream);

    std::string typePhysics = "_physics";
    std::string typeExpress = "_express";
    std::string typeCalib = "_calibration";

    for( std::set<std::string>::const_iterator strit = HLTstreamNames.begin(); strit != HLTstreamNames.end(); strit++) {
      if( strit->rfind(typePhysics, strit->size() - typePhysics.size()) != std::string::npos) {

	Row *aRow = m_dataTable.AddRow();
	aRow->UpdateCell("str", "lvl", "HLT");
	aRow->UpdateCell("str", "str", *strit);
      }
    }      
    for( std::set<std::string>::const_iterator strit = HLTstreamNames.begin(); strit != HLTstreamNames.end(); strit++) {
      if( strit->rfind(typePhysics, strit->size() - typePhysics.size()) == std::string::npos) {

	Row *aRow = m_dataTable.AddRow();
	aRow->UpdateCell("str", "lvl", "HLT");
	aRow->UpdateCell("str", "str", *strit);
      }
    }
  }



  StreamTable::~StreamTable() {;}

  void StreamTable::Dump() {
    std::cout << "***********Dump of the Stream Table *************" << std::endl;
    m_dataTable.Dump();
  }

  void StreamTable::BookPOTs(  const std::set<std::string> &L2streamNames, const std::set<std::string> &EFstreamNames )
  {
    

    /* 
       First loop over the streams in the EF list and add the physics streams.
       Next loop over the EF streams again and add everthing else.
       Finally loop over the L2 streams and add everything but the physics streams.
    */

    std::string typePhysics = "_physics";

    /* The motivation for this plot is spoiled by the L2 calibration streams -- so it is not interesting

    POT *sfo_trp_rat = POT::BookPOT("sfo_trp_rat", "Ratio of event build rate to EF out rate");

    if(sfo_trp_rat != 0) {
      AddPOT("sfo_trp_rat");
      sfo_trp_rat->SetLogOption(false);
      sfo_trp_rat->SetFixedYMin(0.);
      sfo_trp_rat->SetFixedYMax(3.);
      sfo_trp_rat->AddNormalizedParameter(POT::PD_RCTL, "SFO",  "CurrentEventSavedRate",
					  POT::PD_EF, "total", "output", 
					  "redundancy factor");
    }
    */

    POT *streamRates = POT::BookPOT("StreamRates", "Rate per stream");

    if(streamRates != 0) {
      AddPOT("StreamRates");
    
      streamRates->SetLogOption(true);

      for(std::set<std::string>::const_iterator is = EFstreamNames.begin(); is != EFstreamNames.end(); is++) {
	if( is->rfind(typePhysics, is->size() - typePhysics.size()) != std::string::npos)
	  streamRates->AddParameter(POT::PD_EF, "str_" + *is, "output", "EF:" + *is);
      }
      
      for(std::set<std::string>::const_iterator is = EFstreamNames.begin(); is != EFstreamNames.end(); is++) {
	if( is->rfind(typePhysics, is->size() - typePhysics.size()) == std::string::npos)
	  streamRates->AddParameter(POT::PD_EF, "str_" + *is, "output", "EF:" + *is);
      }
      
      for(std::set<std::string>::const_iterator is = L2streamNames.begin(); is != L2streamNames.end(); is++) {
	if( is->rfind(typePhysics, is->size() - typePhysics.size()) == std::string::npos)
	  streamRates->AddParameter(POT::PD_L2, "str_" + *is, "output", "L2:" + *is);
      }
      
    }
  }
  // For run 2
  void StreamTable::BookPOTs(  const std::set<std::string> &HLTstreamNames )
  {
    

    /* 
       First loop over the streams in the HLT list and add the physics streams.
       Next loop over the HLT streams again and add everthing else.
    */

    std::string typePhysics = "_physics";

    /* The motivation for this plot is spoiled by the L2 calibration streams -- so it is not interesting

    POT *sfo_trp_rat = POT::BookPOT("sfo_trp_rat", "Ratio of event build rate to EF out rate");

    if(sfo_trp_rat != 0) {
      AddPOT("sfo_trp_rat");
      sfo_trp_rat->SetLogOption(false);
      sfo_trp_rat->SetFixedYMin(0.);
      sfo_trp_rat->SetFixedYMax(3.);
      sfo_trp_rat->AddNormalizedParameter(POT::PD_RCTL, "SFO",  "CurrentEventSavedRate",
					  POT::PD_EF, "total", "output", 
					  "redundancy factor");
    }
    */

    POT *streamRates = POT::BookPOT("StreamRates", "Rate per stream");

    if(streamRates != 0) {
      AddPOT("StreamRates");
    
      streamRates->SetLogOption(true);

      for(std::set<std::string>::const_iterator is = HLTstreamNames.begin(); 
	  is != HLTstreamNames.end(); is++) {
	if( is->rfind(typePhysics, is->size() - typePhysics.size()) != std::string::npos)
	  streamRates->AddParameter(POT::PD_HLT, "str_" + *is, "output", "HLT:" + *is);
      }
      
      for(std::set<std::string>::const_iterator is = HLTstreamNames.begin(); 
	  is != HLTstreamNames.end(); is++) {
	if( is->rfind(typePhysics, is->size() - typePhysics.size()) == std::string::npos)
	  streamRates->AddParameter(POT::PD_HLT, "str_" + *is, "output", "HLT:" + *is);
      }
    }
  }
}
