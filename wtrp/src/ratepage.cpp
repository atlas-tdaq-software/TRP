#include "wtrp/ratepage.h"
#include "wtrp/statpot.h"
#include "wtrp/tpointinfo.h"
#include "wtrp/rctlstats.h"
#include "wtrp/runinfo.h"
#include "wtrp/lumi.h"
#include "wtrp/lhcinfo.h"

#include <iostream>
#include <sstream>

namespace wtrp {

  RatePage::RatePage(const std::string &name, 
		     const std::string &title, 
		     const std::string &linkText, 
		     const std::string &fileName) : 
    PageData(name, title, linkText, fileName, true){
    
  }

  void RatePage::MakeTable()
  {
    AddGlobalLink(m_fileName, m_linkText);
    AddLocalLink(m_archivePath, "WTRP archive");
    m_dataTable.SetSortable(false);
    m_dataTable.SetReverseOutput();
    m_dataTable.Clear();
    
    ColumnGroup cg_var("log", "");
    cg_var.AddColumn("entry", "Entry");
    cg_var.AddColumn("time", "Time");
    cg_var.AddColumn("text", "Parameter");
    cg_var.AddColumn("value", "Changed to");
    cg_var.AddColumn("keys", "SMK, L1psk, HLTpsk");
    m_dataTable.AddColumnGroup(cg_var);

  }

  /*
  bool TestStatPOT(unsigned int &theValue) {
    static unsigned int iCalls = 0;
    theValue = iCalls++;
    return true;
  }

  bool TestStatPOT2(unsigned int &theValue) {
    static unsigned int iCalls = 0;
    theValue = iCalls/2;
    iCalls++;
    return true;
  }
  */

  void RatePage::AddConfigurablePOT( std::vector< Configurator::PlotSpec >:: const_iterator &psit) 
  {

    std::cout << "Booking pot " << psit->m_name << ", title: " << psit->m_title <<
      " with " << psit->m_rateSpecs.size() << " parameters " << std:: endl;
    
    POT *aPOT = POT::BookPOT(psit->m_name, psit->m_title);
    
    if(aPOT == 0)
      return;
    
    AddPOT(psit->m_name);
    
    aPOT->SetLogOption(true);
    aPOT->SetFixedYMin(0.9);
    aPOT->SetNoDelete();
    aPOT->SetForceOutput();
    aPOT->SetVisible(true);
    
    /* Set any options */
    std::string optionLIN = Configurator::GetOptionLIN();
    std::string optionLOG = Configurator::GetOptionLOG();
    std::string optionMIN = Configurator::GetOptionMIN();
    std::string optionVIS = Configurator::GetOptionVisible();

    for( std::vector<std::string>::const_iterator optit = psit->m_options.begin(); 
	 optit != psit->m_options.end(); optit++) {

      if( *optit == optionLIN) {
	std::cout << "RatePage::AddConfigurablePOT: setting linear scale for plot " << 
	  psit->m_name << std::endl;
	aPOT->SetLogOption(false);
      } else if( *optit == optionLOG) {
	std::cout << "RatePage::AddConfigurablePOT: setting log scale for plot " << 
	  psit->m_name << std::endl;
	aPOT->SetLogOption(true);
      } else if ( optit->substr(0, optionMIN.length() ) == optionMIN) {
	std::string minstr = optit->substr(optionMIN.length());
	float min = .9;
	std::stringstream ss( minstr );
	ss >> min;
	std::cout << "RatePage::AddConfigurablePOT: Setting minimum for " << psit->m_name << " to " << 
	  min << std::endl;
	aPOT->SetFixedYMin(min);
      } else if ( optit->substr(0, optionVIS.length() ) == optionVIS) {
	aPOT->SetVisible(false);
      } else {
	std::cout << "RatePage::AddConfigurablePOT: Unrecognized option: " << *optit << ", for plot " <<
	  psit->m_name << std::endl;
      }
    }

    /* Check if any parameters should be removed from aPOT */
    const std::vector<POT::ParDef> &pars = aPOT->GetPars();

    bool iterate = true;
    while(iterate) {
      std::vector<POT::ParDef>::const_iterator parit = pars.begin();
      for(; parit != pars.end(); parit++) {

	bool found = false;
	for( std::vector< Configurator::RateSpec>::const_iterator ime = psit->m_rateSpecs.begin();
	     ime != psit->m_rateSpecs.end(); ime++) {

	  if( ime->m_level  == parit->level &&
	      ime->m_xlabel == parit->xlabel &&
	      ime->m_ylabel == parit->ylabel ) {
	    found = true;
	    break;
	  }
	}	
	if(!found) {
	  std::cout << "RatePage::AddConfigurablePOT: Deleting " << parit->legendName << std::endl;
	  aPOT->DeleteParameter(parit->level, parit->xlabel, parit->ylabel, POT::PD_none, "", "" );
	  break;
	}
      }
      if(parit == pars.end())
	iterate = false;
    }
    

    /* Add parameters specified in m_rateSpecs */
    for( std::vector< Configurator::RateSpec>::const_iterator ime = psit->m_rateSpecs.begin();
	   ime != psit->m_rateSpecs.end(); ime++) {
      
      if(ime->m_level != POT::PD_none) {
	aPOT->AddParameter(ime->m_level, ime->m_xlabel, ime->m_ylabel, ime->m_legendName); 
	aPOT->SetOpt(ime->m_legendName, POT::PD_forceOut);

	if(ime->m_level == POT::PD_ISMAP) {
	  std::cout << "Adding ismap parameter -- ismap_key: " << ime->m_xlabel << 
	    ", legend: " << ime->m_legendName << std::endl;
	} else {
	  std::cout << "Adding TRP parameter -- source: " << POT::GetPD_source(ime->m_level) <<
	    ", x label: " << ime->m_xlabel << 
	    ", y label: "  << ime->m_ylabel << 
	    ", legend: " << ime->m_legendName <<
	    std::endl;
	}

      }
    }
    
  }

  void RatePage::BookPOTs()
  {

    std::cout << "RatePage::BookPOTs: booking POTs" << std::endl;

    const std::vector< Configurator::PlotSpec > &plotSpecs = Configurator::GetPlotSpecs();

    /* Check if any previously booked POTs are no longer configured -- if so, delete,
     This assumes that all POTs on the page (except StatPOTs) are configurable
    */

    static bool isFirstCall = true;

    bool iterate = true;
    while(iterate) {
      std::vector<std::string>::iterator pnit =  m_POTNames.begin();
      if(pnit != m_POTNames.end())
	pnit++; // Deleting the first plot (assumed to be the global rates plot) is not allowed

      for( ; pnit != m_POTNames.end(); pnit++) {
	POTBase *thePOTBase = POTBase::GetPOT( *pnit );
	const POT *thePOT = dynamic_cast<const POT *>( thePOTBase );
	if(thePOT == 0)
	  continue;
	bool found = false;
	for( std::vector< Configurator::PlotSpec >::const_iterator psit = plotSpecs.begin();
	     psit != plotSpecs.end(); psit++) {
	  if(psit->m_name == *pnit) {
	    found = true;
	    break;
	  }
	}
	if( !found ) {
	  POTBase::DeletePOT( *pnit );
	  m_POTNames.erase( pnit );
	  /* Since the iterator becomes invalid on an erase, break and do it again 
	   (could be done more efficiently of course, but there is no point.) */
	  break;
	}
      }

      if(pnit == m_POTNames.end())
	iterate = false;
    }

    /* 
       Add the first configurable POT followed by the status POT followed by any other POTs 
    */

    std::vector< Configurator::PlotSpec >::const_iterator psit = plotSpecs.begin();

    if(psit != plotSpecs.end())
      AddConfigurablePOT( psit );


    /*
      If this is not the first call to BookPlot, don't touch the StatPOT
    */

    if(isFirstCall) {

      StatPOT *stats = StatPOT::BookStatPOT("globalStatus", "Status History");
      stats->SetChangeLog(&m_dataTable);
      stats->EnableImageMapGeneration( "potmap", &StatPOT::GetTrigConfURL, true);
      
      AddPOT("globalStatus");
      
      std::map<unsigned int, struct StatPOT::SetupMapEntry> dummy;
      //stats->AddParameter( &TestStatPOT, "Toggle1" , dummy, StatPOT::opt_toggle);
      //stats->AddParameter( &TestStatPOT2, "Toggle2" , dummy, StatPOT::opt_toggle);
      
      stats->AddParameter(&RunInfo::GetRunNumber, "Run" , dummy, StatPOT::opt_toggle | StatPOT::opt_tabulate
			  | StatPOT::opt_genMapBoundary);
      stats->AddStringParameter(&RunInfo::GetRunType, "Run Type" , dummy, StatPOT::opt_tabulate);
      stats->AddParameter(&RunInfo::GetLumiBlock, "LB" , dummy, StatPOT::opt_toggle
			  | StatPOT::opt_genMapBoundary | StatPOT::opt_mapRange, 20);
      
      stats->AddParameter(&RunInfo::GetSMK, "SMK" , dummy, StatPOT::opt_toggle | StatPOT::opt_tabulate
			  | StatPOT::opt_genMapBoundary);
      
      stats->AddParameter(&RunInfo::GetL1_PSK, "L1 PSK" , dummy, StatPOT::opt_toggle | StatPOT::opt_tabulate
			  | StatPOT::opt_genMapBoundary);
      
      stats->AddParameter(&RunInfo::GetHLT_PSK, "HLT PSK" , dummy, StatPOT::opt_toggle | StatPOT::opt_tabulate
			  | StatPOT::opt_genMapBoundary);
      
      stats->AddParameter(&RunInfo::GetBunchGroup, "BunchGp" , dummy, StatPOT::opt_toggle | 
			  StatPOT::opt_tabulate | StatPOT::opt_genMapBoundary);
      
      
      std::map<unsigned int, struct StatPOT::SetupMapEntry> onOff;
      onOff[0] = StatPOT::SetupMapEntry(1, "");
      onOff[1] = StatPOT::SetupMapEntry(3, "");
      stats->AddParameter(&LHCInfo::GetStableBeamsFlag, "StableBm", onOff, 
			  StatPOT::opt_tabulate | StatPOT::opt_genMapBoundary);
      stats->AddParameter(&RunInfo::GetReady4Physics, "Rdy4Phys", onOff, 
			  StatPOT::opt_tabulate | StatPOT::opt_genMapBoundary);
      stats->AddParameter(&RunInfo::GetPartitionValid, "ATLpart.", onOff, 0);
      stats->AddParameter(&TPointInfo::GetL1UpdateStatus, "TRP-L1", onOff, 0);
      stats->AddParameter(&TPointInfo::GetHLTUpdateStatus, "TRP-HLT", onOff, 0);
    }
      
    /* Add the rest of the configurable POTs */
    if(psit++ != plotSpecs.end()) {
      for( ; psit != plotSpecs.end(); psit++) {
	AddConfigurablePOT(psit);
      }
    }    
    isFirstCall = false;

  }

  RatePage::~RatePage() {;}

  void RatePage::Dump() {
    std::cout << "***********Dump of the Rate Page *************" << std::endl;
  }

  
}
