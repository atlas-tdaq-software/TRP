/**
 * CHANGELOG
 * May 2017 - Daniel Zheng - Created PredAdapter.cpp to combine HLT and L1 Predadapters
 * May 2018 - Andy Aukerman - Cleanup, small adapation to allow multiparamater fits, fixed m_triggerDB and checkIn functions
 * August 2019 - Nick Felice - Functions for new IS data format, global offset, reformatting & organization of code
 *
 * The code is, for the most part, organized into 4 sections:

 * 2. Callbacks (callback functions for each IS type)
 * 3. Calculations (basic formatting and prediction calculations)
 * 4. Publishing (publish results into IS)
 */

#include "PredAdapter.h"

#include "TRP/Utils.h"
#include <ipc/core.h>
#include <iostream>
#include "ers/ers.h"
#include <time.h>
#include <regex.h>
#include "dqmf/is/Tag.h"
#include "is/inforeceiver.h"
#include "ers/ers.h"
#include "is/serveriterator.h"
#include <errno.h>
#include <stdlib.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <sys/stat.h>

ERS_DECLARE_ISSUE(predadapter, Issue, "conversion issue", )

namespace pt = boost::property_tree;

namespace {
    bool fileExists(const std::string& name) {
        struct stat buffer;   
        return (stat (name.c_str(), &buffer) == 0); 
    }
}


/**
 * Constructor
 * Creates the new object, and sets all parameters from PredAdapterMain.cpp
 */
PredAdapter::PredAdapter(std::string partition_name, std::string server_name, std::string object_name,
                        std::string triggerDBAlias, std::string L1_name_out, std::string hlt_name_out,
                        std::string trigger_groups,
                        bool debug) :
    m_partitionName(partition_name),
    m_objectName(object_name),
    m_serverName(server_name),
    m_l1OutputPartition(L1_name_out), 
    m_hltOutputPartition(hlt_name_out),
    m_triggerDB(triggerDBAlias),
    m_trigger_groups(trigger_groups),
    m_debug(debug)
{

    // Begin Constructor Code
    std::string alertprefix = "Xmon()::PredAdapter---> ";

    
    std::cout << alertprefix << "IRH the debug set is " << m_debug << std::endl;

}

/**
 * @brief Online Luminosity Calculator configuration
 * 
 * Sets the correct partition and server to read the luminosity
 * @param olcPartition
 * @param lumiISServer 
 */
void PredAdapter::setLumiPartition(std::string olcPartition, std::string lumiISServer) {
    ERS_DEBUG(0, "In Configuration");
    m_lumiPartition = olcPartition;
    m_olcIsServer = lumiISServer;
}

/**
 * @brief Reads the prediction file and fills vectors of L1 and HLT trigger items for which predictions will be calculated
 * 
 * It prepares adapter for downloading Trigger DB info and
 * subscribing to IS.
 * 
 * @param predictionFile 
 * @return success If configuration is successful
 * @return faliure If prediction file cannot be found
 */
bool PredAdapter::PredConfigure(const std::string & predictionFile){
    std::string alertprefix = "Xmon()::PredConfigure---> ";

    //set vector of trigger items
    //Flag::-1: can't make predict, -2: rate=0, no make flag,
    //3: OMG fatal!, 2: error, 1: warning, 0: Okay
    std::string     name;
    std::string     function;
    std::string     equation;
    std::string     ratio_equation;
    double          pred_parameters[10];
    double          error_parameters[10];
    double          ratio_pred_parameters[10];
    double          ratio_error_parameters[10];

    std::cout << alertprefix<< "Reading prediction file = " << predictionFile << std::endl;

    if ( !fileExists(predictionFile)) {
        std::cout << alertprefix << "The prediction file does not exist!" << std::endl;
        std::cout << alertprefix << "Aborting PredAdapter Configuration" << std::endl;
        return failure;
    }

    pt::ptree pt;
    pt::read_json(predictionFile, pt);

    //creates trigger ratio for each trigger listed under LVL1_ratios in prediction file
    for (pt::ptree::value_type& trigger: pt.get_child("LVL1_ratios")) {
        trigger_ratio ratio;
        ratio.nominator = trigger.second.get<std::string>("first");
        ratio.denominator = trigger.second.get<std::string>("second");
        ratio_equation = trigger.second.get<std::string>("function");
        std::cout << alertprefix << "Ratio for " << ratio.nominator << "/" << ratio.denominator << std::endl;
        for(int i=0; i<4; i++){
            std::string paramName = "parameters.p"+std::to_string(i);
            std::string errName = "parameters.err"+std::to_string(i);
            ratio_pred_parameters[i] = trigger.second.get<float>(paramName);
            ratio_error_parameters[i] = trigger.second.get<float>(errName);
        }
        ratio.CalcRatio();
        ratio.setFormula(ratio_equation, ratio_pred_parameters, ratio_error_parameters);
        trig_ratios.push_back(ratio);
    }
    std::cout << alertprefix << "LVL1 Ratios configuration successful." << std::endl;

    //creates trigger item for each L1 trigger with offline predictions
    for(pt::ptree::value_type& trigger: pt.get_child("LVL1_triggers")) {
        trigger_item  trig_tmp;
        trig_tmp.isHLT(false);
        std::cout << alertprefix << "NEW LVL1 Trigger" << std::endl;
        trig_tmp.L1_name = trigger.second.get<std::string>("name");
        std::cout << alertprefix << "Name: " << trig_tmp.L1_name << std::endl;
        equation = trigger.second.get<std::string>("function");
        std::cout << alertprefix << "Function: " << equation << std::endl;
        trig_tmp.in_List = trigger.second.get<bool>("goConfig");
        std::cout << alertprefix << "in List: " << trig_tmp.in_List << std::endl;
        for(int i=0; i<4; i++){
            std::string paramName = "parameters.p"+std::to_string(i);
            std::string errName = "parameters.err"+std::to_string(i);
            pred_parameters[i] = trigger.second.get<float>(paramName);
            error_parameters[i] = trigger.second.get<float>(errName);
        }
        trig_tmp.setFormula(equation, pred_parameters, error_parameters);
        l1trigs.push_back(trig_tmp);
    }

	//adds all the L1 triggers that do not have offline predictions but are still in menu and should have OTF predictions
	for(unsigned int i=0; i<l1names_in_menu.size(); i++){
		bool found = false;
		for(unsigned int j=0; j<l1trigs.size(); j++){
			if(l1trigs[j].L1_name.compare(l1names_in_menu[i]) == 0){
				found = true;
			}
		}
		if(!found){
			trigger_item trig_tmp;
			trig_tmp.isHLT(false);
			trig_tmp.L1_name = l1names_in_menu[i];
			trig_tmp.status = true;
			l1trigs.push_back(trig_tmp);

		}
	}
    std::cout << alertprefix << "LVL1 Triggers configuration successful." << std::endl;

    //creates trigger item for each HLT trigger with offline predictions
    for(pt::ptree::value_type& trigger: pt.get_child("HLT_triggers")) {
        trigger_item  trig_tmp;
        trig_tmp.isHLT(true);
        std::cout << alertprefix << "NEW HLT Trigger" << std::endl;
        trig_tmp.HLT_name = trigger.second.get<std::string>("name");
        std::cout << alertprefix << "Name: " << trig_tmp.L1_name << std::endl;
        equation = trigger.second.get<std::string>("function");
        std::cout << alertprefix << "Function: " << equation << std::endl;
        trig_tmp.in_List = trigger.second.get<bool>("goConfig");
        std::cout << alertprefix << "in List: " << trig_tmp.in_List << std::endl;
        for(int i=0; i<4; i++){
            std::string paramName = "parameters.p"+std::to_string(i);
            std::string errName = "parameters.err"+std::to_string(i);
            pred_parameters[i] = trigger.second.get<float>(paramName);
            error_parameters[i] = trigger.second.get<float>(errName);
        }
        trig_tmp.setFormula(equation, pred_parameters, error_parameters);
        hlttrigs.push_back(trig_tmp);

    }

	//adds all the HLT triggers that do not have offline predictions but are still in menu and should have OTF predictions
	for(unsigned int i=0; i<hltnames_in_menu.size(); i++){
		bool found = false;
		for(unsigned int j=0; j<hlttrigs.size(); j++){
			if(hlttrigs[j].HLT_name.compare(hltnames_in_menu[i]) == 0){
				found = true;
			}
		}
		if(!found){
			trigger_item trig_tmp;
			trig_tmp.isHLT(true);
			trig_tmp.HLT_name = hltnames_in_menu[i];
			trig_tmp.status = true;
			hlttrigs.push_back(trig_tmp);

		}
	}
    std::cout << alertprefix << "HLT Triggers configuration successful." << std::endl;
    setIndices();
    std::cout << alertprefix << "Indices setting successful." << std::endl;
    return success;
}

/**
 * @brief Sets the adapter to run in heavy ion mode
 * 
 * Will report rates = f(lumi), instead of pp method of rates = lumi*f(mu)
 * @param config_hi_flag True if running in HI mode, false otherwise
 */
void PredAdapter::SetHI(bool config_hi_flag) {
    m_isHIrun = config_hi_flag;
    if (m_isHIrun) {
        ERS_INFO( "Configured Xmon predadapter for HI Collisions." );
    }
    else {
        ERS_INFO( "Configured Xmon predadapter for pp Collisions." );
    }
}

/**
 * @brief Sets the adapter to run with the new Run 3 IS data format
 * 
 * @param config_IS_flag
 * Set newISFormat flag
 * Will determine how data is collected & published
 */
void PredAdapter::SetIS(bool config_IS_flag) {
    m_newISFormat = config_IS_flag;
    if (m_newISFormat) {
        ERS_LOG( "Configured Xmon predadapter for new IS data format." );
    }
    else {
        ERS_LOG( "Configured Xmon predadapter for old IS data format." );
    }
}

/**
 * @brief Sets the adapter to run with the technical run option, where HLT rates are not required.
 * 
 * @param isTechRun
 * Will allow xMon to run without HLT trigger data. 
 */
void PredAdapter::SetTest(bool isTechRun) {
    m_isTechRun = isTechRun;
    if (m_isTechRun) {
            ERS_LOG( "Configured Xmon predadapter for technical run." );
        }
        else {
            ERS_LOG( "Configured Xmon predadapter for normal run, not technical run." );
        }
}

/**
 * @brief Allows DB keys to be manually set for testing purposes.
 * 
 * This is only called if the keys are manually input from the command line, and not read
 * from a configuration file. It sets the class instance variables to correct values.
 * @param smk Supermasterkey
 * @param l1psk L1 Prescale Key
 * @param hltpsk HLT Prescale Key
 */
void PredAdapter::setDBKeysForTesting(unsigned int smk, unsigned int l1psk, unsigned int hltpsk) {
    m_smk = smk;
    m_l1k = l1psk;
    m_hltk = hltpsk;
}

/**
 * @brief Loops through all HLT trigger items and sets the correct prescale
 * 
 * @param hlt_ps HLT to prescale mapping
 */

void PredAdapter::updateHLTPrescales(const confadapter_imp::TriggerMenuHelpers::Mapping & hlt_ps)
{
    // udpate the configured HLT triggers with new prescales
    std::string alertprefix = "Updating prescales---> ";
    for (auto & trigger : hlttrigs) {
        if(!trigger.status) {
            continue;
        }
        //Match PS to HLT Trigger
        const std::vector<std::string> & vps = hlt_ps.at(trigger.HLT_name);
        for (auto hltps : hlt_ps) {
            if( vps[0].find("NA") == std::string::npos) {
                trigger.HLT_PS = std::stoi(vps[0]);
            }
        }
        std::cout << alertprefix << " Configured trigger " << trigger.HLT_name << " has HLT PS updated to " << trigger.HLT_PS << std::endl;
    }
}



/**
 * @brief Reads DB for the supermasterkey and prescale keys. 
 * It also reads the trigger menu
 * 
 * @return success If DB is successfully read.
 * @return failure If DB cannot be read for some reason.
 */
bool PredAdapter::readTriggerDBInfo(){
    std::string alertprefix = "Xmon()::readTriggerDBInfo---> ";

    // Try to get trigger keys if not externally provided
    if(m_smk==0) {
        try {
            trp_utils::GetTriggerKeys(m_partitionName, m_smk, m_hltk, m_l1k );
            std::cout << alertprefix << "SuperMaster Key == " << m_smk << std::endl;
        }
        catch (std::exception & ex) {
            std::cout << alertprefix << "Exception on getTriggerKeys: " << ex.what() << std::endl;
            return failure;
        }
    }
    if(!m_smk) {
        std::cout << alertprefix << "Supermasterkey is 0. No menu available" << std::endl;
        return failure;
    }

    //Sets global vars to results from trputils::GetTriggerKeys
    std::cout << alertprefix << "Trigger db " << m_triggerDB << " with smk " << m_smk << ", hlt psk " << m_hltk << " and l1 psk " <<m_l1k << std::endl;

    confadapter_imp::TriggerMenuHelpers menuHelper(m_partitionName, m_triggerDB);

    //Try to get the configuration of menu of the 3 keys
    try {
        menuHelper.getConfiguration(m_smk, m_hltk, 0);
    }
    catch (std::exception &ex) {
        ers::warning(predadapter::Issue(ERS_HERE, ex.what()));
        try {
            menuHelper.getConfigurationRun2(m_smk, m_hltk, 0);
        }
        catch (std::exception &ex2){
            ers::warning(predadapter::Issue(ERS_HERE, ex2.what()));
            return failure;
        }
    }
    ERS_LOG("Succeeded loading the trigger configuration");

    //Declare Mapping variables l1_hlt and hlt_ef, and get them from menu
    const auto & map_L1toHLT = menuHelper.getMultiMapping("HLT_L1");
    std::cout << alertprefix << "Got L1:HLT map of size " << map_L1toHLT.size() << std::endl;
    if (map_L1toHLT.size()==0) {
        ers::warning(predadapter::Issue(ERS_HERE, "Failed to retrieve L1:HLT map"));
        return failure;
    }
    menuHelper.printMultiMapping(map_L1toHLT, "L1 -> HLT name mapping: ");
    std::unordered_map<std::string,std::string> hlt2L1Map;
    for(auto & l1hlt : map_L1toHLT) {
        hlt2L1Map[l1hlt.second] = l1hlt.first;
		l1names_in_menu.push_back(l1hlt.first);
		hltnames_in_menu.push_back(l1hlt.second);

    }

    // udpate the configured HLT triggers with new prescales
    for (auto & trigger : hlttrigs) {
        bool inMenu = hlt2L1Map.count(trigger.HLT_name) > 0; // is the configured trigger in the menu

        // ignore non-existing triggers
        if(!inMenu) {
            trigger.status = 0;
            std::cout << alertprefix << " Configured trigger " << trigger.HLT_name << (inMenu ? "" : " (not part of the menu)") << std::endl;
            continue;
        }
        //Log HLT status as on for tech run so adapter does not stop
        if (m_isTechRun) {
            trigger.status = 1;
        }
        else {
            for (auto & it_L1mHLT : map_L1toHLT) {
                const std::string l1Name = it_L1mHLT.first;
                const std::string hltName = it_L1mHLT.second;
                if (hltName != trigger.HLT_name) {
                    continue;
                }
                trigger.status = 1;
                if (l1Name.find(",") == std::string::npos) { // no multi-item seed
                    trigger.L1_name = l1Name;
                }
                else if (l1Name == "L1_MU20,L1_MU21") { // seeded by L1_MU20, MU21
                    trigger.L1_name = "L1_MU20";
                }
                else {
                    trigger.status = 0; // disable multi-item seeded chains
                }
                break;
            }
        }
    }

    //Declare Mapping Variables for the prescale values, and get them from menu
    const auto & map_HLTtoPS = menuHelper.getMapping("hlt_ps");
    std::cout << alertprefix << "Got HLT:PS map of size " << map_HLTtoPS.size() << std::endl;
    if (map_HLTtoPS.size()==0) {
        ers::warning(predadapter::Issue(ERS_HERE, "Failed to retrieve HLT:PS map"));
        return failure;
    }
    menuHelper.printMapping(map_HLTtoPS, "HLT -> PS name mapping: ");
    updateHLTPrescales(map_HLTtoPS);

    //lists all active HLT triggers
    for (auto & trigger : hlttrigs) {
        if(!trigger.status) {
            continue;
        }
        std::cout << alertprefix << " Configured trigger " << trigger.HLT_name 
                << " is seeded by " << trigger.L1_name << " with L1 PS " << trigger.L1_PS
                << " and HLT PS " << trigger.HLT_PS << std::endl;
    }
    return success;
}

/**
 * @brief Related to global offset functionality, it configures reading from the reference histogram
 * 
 * @param refPath Relative path to the reference histogram, listed in the configuration file.
 * @return true If the reference hist is successfully configured.
 */
bool PredAdapter::RefConfigure(std::string refPath) {
    
    std::string alertprefix = "Xmon()::RefConfigure---> ";

    if(! fileExists(refPath)) {
        std::cout << alertprefix << "Reference file " << refPath << " does not exist" << std::endl;
        return failure;
    }

    TFile *f = new TFile(refPath.c_str());
    if (f->IsZombie()) {
        std::cout << alertprefix << "Error opening reference histogram file " << refPath << std::endl;
        return failure;
    }

    //data is stored in ROOT TTree, which is accessed here
    TTree *T = (TTree*)f->Get("t");
    TObjArray *branches = T->GetListOfBranches();

    //loops through every trigger and pulls data from the reference root file 
    //right now this is only for L1, we may need to implement this for HLT as well
    for(unsigned int i = 0; i < l1trigs.size(); i++) {
        std::string temp = l1trigs[i].L1_name;
        size_t pos = temp.find("-");
        //just reformatting string of L1 name
        while (pos != std::string::npos) {
            temp.replace(pos, 1, "_");
            pos = temp.find("-", pos + 1);
        }
        const char * name = temp.c_str();

        double trigger; //this references the data in the tree

        if (branches->FindObject(name) == NULL){
            continue;
        }

        T->SetBranchAddress(name, &trigger);


        int nentries = T->GetEntries();
        std::vector<double_t> ref_data;

        //fills ref_data, a vector for each trigger containing reference data from reference histogram
        for(int i=0; i<nentries; i++){
            T->GetEntry(i);
			if(trigger != 0.0){
				ref_data.push_back(trigger);
			}
        }

        //adds global offset data to the actual trigger items
        l1trigs[i].ref_data = ref_data;
        l1trigs[i].n_ref = ref_data.size();
        l1trigs[i].ref_config = true;
        std::cout << alertprefix << "Configured reference data for " << name << std::endl;
    }
    return success;
}

/**
 * @brief Loops through the HLT trigger items and checks the L1 dependencies of active HLT triggers
 */
void PredAdapter::CheckHLT() {
    //looping throigh HLT trigger items
    for (unsigned int i = 0; i < hlttrigs.size(); i++) {
        if (hlttrigs[i].status==0) {
            continue;
        }
        //for each active HLT, we loop through the L1 triggers, adding L1 triggers with no corresponding 
        //HLT to an extral1trigs trigger list
        for (unsigned int j = 0; j < l1trigs.size(); j++) {
            if (hlttrigs[i].L1_name == l1trigs[j].L1_name) {
                break;
            }
            if (j == l1trigs.size() - 1) {
                trigger_item trig_tmp;
                trig_tmp.isHLT(false);
                trig_tmp.L1_name = (hlttrigs[i].L1_name);
                extral1trigs.push_back(trig_tmp);
            }
        }
    }
}

/**
 * @brief For the global offset functionality, it gets list of desired trigger groups 
 * and fills a trigger list for each group.
 */
void PredAdapter::ParseTriggers() {
    std::string alertprefix = "Xmon()::ParseTriggers---> ";

    std::vector<std::string> group_array;
    const std::string delimiter = ",";
    size_t pos = 0;
    
    //goes through trigger groups and separates the strings into an array, i.e. group_array = ['Mu', 'L1Calo', 'Another group',...]
    while ((pos = m_trigger_groups.find(delimiter)) != std::string::npos) {
        std::string token = m_trigger_groups.substr(0, pos);
        group_array.push_back(token);
        m_trigger_groups.erase(0, pos + delimiter.length());
    }
    //at the end we add what's left of m_trigger_groups, last group name, because while statement only runs when pos != last index
    group_array.push_back(m_trigger_groups);
    
    std::cout << alertprefix << "Initializing " << group_array.size() << " groups of triggers:" << std::endl;
    //loops through the groups, makes a trigger list for each group, containing all triggers in that group
    //this is what we may want to change eventually by using preset trigger groups? (Manually do it once or a few times then make an option in config file)
    for (unsigned int i = 0; i < group_array.size(); i++) {
        //see all these functions in the trigger_list structure in PredAdapter.h
        trigger_group  trig_list_tmp;
        trig_list_tmp.incl_trigs.clear();
        trig_list_tmp.grp_name = group_array[i];
        trig_list_tmp.GroupTriggers(l1trigs);
        trig_list_tmp.SetBool(i);
        //trig_groups is a trigger list, declared in PredAdapter.h
        trig_groups.push_back(trig_list_tmp);
        std::cout << alertprefix << trig_groups[i].grp_name << " contains " << trig_groups[i].incl_trigs.size() << " triggers: " << std::endl;
        for (unsigned int j = 0; j < trig_groups[i].incl_trigs.size(); j++) {
            std::cout << alertprefix << "          " << trig_groups[i].incl_trigs[j]->L1_name << std::endl;
            std::cout << alertprefix << "In List?" << trig_groups[i].incl_trigs[j]->in_List << std::endl;
        }
    }
}

/**
 * @brief Subscribes to the correct IS partitions and servers.
 * 
 * @return true If subscriptions are successful
 */

bool PredAdapter::SubscribeToIS() {
    ERS_LOG( "subscribing to IS information on partition " << m_partitionName );

    IPCPartition partition(m_partitionName);
    m_receiver = std::make_unique<ISInfoReceiver>(partition);
    ERS_DEBUG( 0, "Subscribing to IS Servers");
    if (!m_newISFormat){
        std::cout << "XMON IS NOT CONFIGURED FOR OLD IS FORMAT. Exiting" << std::endl;
        return failure;
    }else{
    //subscriptions for new IS data format, need to subscribe to each trigger individually
        try {
            // HLT subscription
            for (const auto & trigger: hlttrigs) {
                try {
                    m_receiver->subscribe(m_serverName + '.' + trigger.HLT_name, (&PredAdapter::newcallback_hltrates), this);
                }
                catch (daq::is::AlreadySubscribed  &ex) {
                    ers::warning(predadapter::Issue(ERS_HERE, ex.what()));
                }
                catch(daq::is::InvalidCriteria& ex) {
                    ers::warning(predadapter::Issue(ERS_HERE, ex.what()));
                }
            }

            // L1 subscription
            for (const auto & trigger: l1trigs) {
                try {
                    m_receiver->subscribe(m_serverName + '.' + trigger.L1_name, (&PredAdapter::newcallback_l1rates), this);
                }
                catch (daq::is::AlreadySubscribed &ex) {
                    ers::warning(predadapter::Issue(ERS_HERE, ex.what()));
                }
                catch (daq::is::InvalidCriteria &ex) {
                    ers::warning(predadapter::Issue(ERS_HERE, ex.what()));
                }
            }
        
            // L1 subscription
            for (const auto & trigger: extral1trigs) {
                try {
                    m_receiver->subscribe(m_serverName + '.' + trigger.L1_name, (&PredAdapter::newcallback_l1rates_simple), this);
                }
                catch (daq::is::AlreadySubscribed  &ex) {
                    ers::warning(predadapter::Issue(ERS_HERE, ex.what()));
                }
                catch (daq::is::InvalidCriteria &ex) {
                    ers::warning(predadapter::Issue(ERS_HERE, ex.what()));
                }
            }
        }
        catch (daq::is::RepositoryNotFound &ex) {
            ers::warning(predadapter::Issue(ERS_HERE, "Cannot subscribe to rate updates. " + std::string(ex.what())));
            return failure;
        }
    }

    try {
        try {
            m_receiver->subscribe("RunParams.TrigConfL1PsKey", (&PredAdapter::callback_l1k), this);
        }
        catch (daq::is::AlreadySubscribed  &ex) {
            ers::warning(predadapter::Issue(ERS_HERE, ex.what()));
        }
        catch (daq::is::InvalidCriteria    &ex) {
            ers::warning(predadapter::Issue(ERS_HERE, ex.what()));
        }

        try {
            m_receiver->subscribe("RunParams.TrigConfHltPsKey", (&PredAdapter::callback_hltk), this);
        }
        catch (daq::is::AlreadySubscribed &ex) {
            ers::warning(predadapter::Issue(ERS_HERE, ex.what()));
        }
        catch (daq::is::InvalidCriteria &ex) {
            ers::warning(predadapter::Issue(ERS_HERE, ex.what()));
        }
    }
    catch (daq::is::RepositoryNotFound &ex) {
        ers::error(predadapter::Issue(ERS_HERE, "Cannot subscribe to PSK updates. " + std::string(ex.what())));
        return failure;
    } 

    IPCPartition olcPartition(m_lumiPartition);
    m_olcReceiver = std::make_unique<ISInfoReceiver>(olcPartition);
    
    ERS_LOG("Subscribing to OLC Servers");
    try {
        m_olcReceiver->subscribe(m_olcIsServer+"."+m_objectName, (&PredAdapter::callback_Mu), this);
    }
    catch (daq::is::RepositoryNotFound& ex) {
        ers::info(predadapter::Issue(ERS_HERE, "No luminosiy information available. Will not be able to provide luminosity-base rate predictions"));
        ers::warning(predadapter::Issue(ERS_HERE, ex.what()));
    }
    catch (daq::is::AlreadySubscribed &ex) {
        ers::warning(predadapter::Issue(ERS_HERE, ex.what()));
    }
    catch (daq::is::InvalidCriteria &ex) {
        ers::warning(predadapter::Issue(ERS_HERE, ex.what()));
    }

    return success;
}

void PredAdapter::Run() {
    std::string alertprefix = "Xmon()::Run---> ";
    if (m_debug) {
        std::cout << alertprefix<< "Run before receiver!!!"<<std::endl;
    }
    OWLSemaphore s;
    m_semaphore = &s;
    m_semaphore->wait();
    m_semaphore = nullptr;
    if (m_debug) {
        std::cout << alertprefix<< "Run after receiver!!!"<<std::endl;
    }
}

//=====================================================================================
// Callback Functions
//      Functions that are called when IS data objects are updated
//=====================================================================================


// --------------------------------------------------------------------------------
// L1 KEY
// --------------------------------------------------------------------------------
/**
 * @brief Callback for L1 prescale key
 * 
 * @param isc IS Callback Info
 */
void PredAdapter::callback_l1k(ISCallbackInfo *isc) {
    ERS_LOG("Callback for L1 psk");
    if (isc->reason() == ISInfo::Deleted) {
        return;
    }
    TrigConfL1PsKey l1_k;
    isc->value(l1_k);
    m_l1k = l1_k.L1PrescaleKey;
    ERS_LOG("New L1 psk (currently not used): " << m_l1k);
}

// --------------------------------------------------------------------------------
// HLT KEY
// --------------------------------------------------------------------------------
/**
 * @brief Callback for HLT prescale key
 * 
 * @param isc IS Callback Info
 */
void PredAdapter::callback_hltk(ISCallbackInfo *isc) {
    ERS_LOG("Callback for HLT psk");
    if (isc->reason() == ISInfo::Deleted) {
        return;
    }
    TrigConfHltPsKey hlt_k;
    isc->value(hlt_k);
    m_hltk = hlt_k.HltPrescaleKey;
    ERS_LOG("New HLT psk: " << m_hltk);
    confadapter_imp::TriggerMenuHelpers menuHelper(m_partitionName, m_triggerDB);
    try {
        menuHelper.getConfiguration(0, m_hltk, 0);
    }
    catch (std::exception &ex) { 
        ers::error(predadapter::Issue(ERS_HERE, ex.what()));
    }

    const auto & map_HLTtoPS = menuHelper.getMapping("hlt_ps");

    updateHLTPrescales(map_HLTtoPS);

    // Loop for each element in the hlt vector (hlttrigs) formed in the PredConfigure
    for (auto & chain : hlttrigs) {
        if(!chain.status) {
            continue;
        }
        std::string ps = map_HLTtoPS.at(chain.HLT_name)[0];
        chain.HLT_PS = std::stoi(ps);
    }
}

//---------------------------------------------------------------------------------
// L1 Prescales that we need but aren't in l1trigs
// --------------------------------------------------------------------------------
/**
 * @brief Sets prescales for all L1 that aren't in l1trigs, which includes all L1 for which HLT has no dependence.
 * 
 * @param isc IS Callback Info
 */
void PredAdapter::newcallback_l1rates_simple(ISCallbackInfo *isc) {
    if (isc->reason() == ISInfo::Deleted) return;
    if (m_debug) std::cout << "\n\n>>>>> Callback for " << isc->name() << std::endl;

    if(!TRIG_FORMATTED) setIndices();
    
    unsigned int index = m_extral1map[isc->name()];
    isc->value(extral1trigs[index].ISC_L1);
    //setting prescale from IS
    if (extral1trigs[index].status) {    
        extral1trigs[index].PS  = extral1trigs[index].ISC_L1.PS;

        //check prescale
        if (extral1trigs[index].PS >= 1) {
            extral1trigs[index].checkPS = 1;
        }
        else {
            extral1trigs[index].checkPS = 0;
        }

        extral1trigs[index].initialized = true;
    }
}

// --------------------------------------------------------------------------------
// L1 Rates (new)
// --------------------------------------------------------------------------------
/**
 * @brief New L1 rate callback function. Calculates predicitons and publishes to IS
 * 
 * @param isc 
 */
void PredAdapter::newcallback_l1rates(ISCallbackInfo *isc) {
    if (isc->reason() == ISInfo::Deleted) return;
    if (m_debug) std::cout << "\n\n>>>>> Callback for " << isc->name() << std::endl;
    
    if (!TRIG_FORMATTED) setIndices();
 
    unsigned int index = m_l1map[isc->name()];
    isc->value(l1trigs[index].ISC_L1);

    //if trigger is active, set the live rate, before and after prescale, as well as updating the prescale.
    if (l1trigs[index].status) {
        if (m_debug) std::cout << ">>>>> Status is true for " << isc->name() << std::endl;
        l1trigs[index].rate         = l1trigs[index].ISC_L1.TAP;
        l1trigs[index].rate_unpresc = l1trigs[index].ISC_L1.TBP;
        l1trigs[index].PS           = l1trigs[index].ISC_L1.PS;

        if (l1trigs[index].PS >= 1) {
            l1trigs[index].checkPS = 1;
        }
        else {
            l1trigs[index].checkPS = 0;
        }
    
        l1trigs[index].initialized = true;
    }                

    //calculates and sets predictions with trigger item
    if (l1trigs[index].initialized && LUM_INITIALIZED){
        if (m_debug) std::cout << ">>>>> Calculating prediction for " << isc->name() << std::endl;
        calculatePred_new(index); //this includes offline, OTF and global offset predictions.
        
        //Right now we only publish PBP, PAP, OTF, and global offset

        l1trigs[index].L1_IS_Pred.PBP     = l1trigs[index].pred_unpresc;
        l1trigs[index].L1_IS_Pred.PAP     = l1trigs[index].pred;
        l1trigs[index].L1_IS_Pred.OTFPRED = l1trigs[index].otfpred;
        l1trigs[index].L1_IS_Pred.PREDERROR = l1trigs[index].offlineErrorFlag;
        l1trigs[index].L1_IS_Pred.OTFERROR = l1trigs[index].onlineErrorFlag;
        l1trigs[index].L1_IS_Pred.OFFLINESIG = l1trigs[index].offlineSigma;
        l1trigs[index].L1_IS_Pred.ONLINESIG = l1trigs[index].onlineSigma;

        //publish to IS
        if(l1trigs[index].L1_IS_Pred.OTFPRED != 0 && m_smallDebugCounter < 5){ m_smallDebugOn = true;}

        L1_ISPublish(l1trigs[index].L1_name, l1trigs[index].L1_IS_Pred);
    }

    //publishes global offset information for trigger group, as long as there is enough data to compute with
    if (l1trigs[index].initialized && l1trigs[index].in_List && l1trigs[index].ks_vec.size()>3) {
        trig_groups[l1trigs[index].list_Index].CalcAvg();
        GO_ISPublish(trig_groups[l1trigs[index].list_Index].grp_name, trig_groups[l1trigs[index].list_Index].group_stats);
    }
}

// --------------------------------------------------------------------------------
// HLT Rates (new)
// --------------------------------------------------------------------------------
/**
 * @brief New callback function for HLT rates.
 * 
 * @param isc IS Callback info
 */
void PredAdapter::newcallback_hltrates(ISCallbackInfo *isc) {
    std::string alertprefix = "Xmon()::callback_hltrates---> ";
    if (isc->reason() == ISInfo::Deleted) return;
    if (m_debug) std::cout << "\n\n>>>>> Callback for " << isc->name() << std::endl;
	

    if (!TRIG_FORMATTED) setIndices();
    
    unsigned int index = m_hltmap[isc->name()];
    isc->value(hlttrigs[index].ISC_HLT);

    //setting rate and L1 prescale
    if (hlttrigs[index].status) {
        if (m_debug) std::cout << ">>>>> Status is true for " << isc->name() << std::endl;
        hlttrigs[index].rate  = hlttrigs[index].ISC_HLT.Output;
        
        if (hlttrigs[index].L1_name == l1trigs[hlttrigs[index].L1_index].L1_name) {
            hlttrigs[index].L1_PS = l1trigs[hlttrigs[index].L1_index].PS;
        }
        else if (hlttrigs[index].L1_name == extral1trigs[hlttrigs[index].L1_index].L1_name) {
            hlttrigs[index].L1_PS = extral1trigs[hlttrigs[index].L1_index].PS;
        }
        //if technical run, default L1_PS to 1
        else if (m_isTechRun) {
            hlttrigs[index].L1_PS = 1;
        }
        else {
            std::cout << "Error retrieving L1 prescale for HLT trigger " << hlttrigs[index].HLT_name << std::endl;
            std::cout << "Deactivating trigger" << std::endl;
            hlttrigs[index].status = false;
        }

        hlttrigs[index].initialized = true;
    }

    //calculates predicitons and publish to IS
    if (hlttrigs[index].initialized && LUM_INITIALIZED) {
        if (m_debug) std::cout << ">>>>> Calculating prediction for " << isc->name() << std::endl;
        calculate_HLT_new(index);

        // hlttrigs[index].HLT_IS_Pred.output  = hlttrigs[index].rate;
        // hlttrigs[index].HLT_IS_Pred.ps      = hlttrigs[index].HLT_PS;
        hlttrigs[index].HLT_IS_Pred.PRED    = hlttrigs[index].pred;
        // hlttrigs[index].HLT_IS_Pred.diff    = hlttrigs[index].diff;
        // hlttrigs[index].HLT_IS_Pred.nsigma  = hlttrigs[index].nSigma;
        // hlttrigs[index].HLT_IS_Pred.ratio   = hlttrigs[index].ratio;
        // hlttrigs[index].HLT_IS_Pred.sratio  = hlttrigs[index].sratio;
        // hlttrigs[index].HLT_IS_Pred.flag    = hlttrigs[index].flag;
        // hlttrigs[index].HLT_IS_Pred.Par0    = hlttrigs[index].p0;
        // hlttrigs[index].HLT_IS_Pred.Par1    = hlttrigs[index].p1;
        hlttrigs[index].HLT_IS_Pred.OTFPRED = hlttrigs[index].otfpred;
        hlttrigs[index].HLT_IS_Pred.PREDERROR = hlttrigs[index].offlineErrorFlag;
        hlttrigs[index].HLT_IS_Pred.OTFERROR = hlttrigs[index].onlineErrorFlag;
        hlttrigs[index].HLT_IS_Pred.OFFLINESIG = hlttrigs[index].offlineSigma;
        hlttrigs[index].HLT_IS_Pred.ONLINESIG = hlttrigs[index].onlineSigma;


        if(hlttrigs[index].HLT_IS_Pred.OTFPRED != 0 && m_smallDebugCounter < 5){ m_smallDebugOn = true;}
        HLT_ISPublish(hlttrigs[index].HLT_name, hlttrigs[index].HLT_IS_Pred);
    }
    //we could add global offset publishing here as well when we implement that for HLT
}

// --------------------------------------------------------------------------------
// Luminosity Data
// --------------------------------------------------------------------------------
/**
 * @brief IS Callback for luminosity and pileup.
 * 
 * @param isc IS Callback info.
 */
void PredAdapter::callback_Mu(ISCallbackInfo * isc) {
    std::string alertprefix = "Xmon()::callback_MU---> ";
    if (isc->reason() == ISInfo::Deleted) return;
    if (m_debug) std::cout << "\n\n>>>>> Callback for "<< isc->name()<<std::endl;

    ISInfoAny OLC_LumiData;
    isc->value(OLC_LumiData);
    double my_lum[7];
    for (int i = 0; i<7; ++i) {
        OLC_LumiData >> my_lum[i];
    }
    
    //Menzel 1/5/2022: These comments don't make sense. Why are there 3 different indices for Mu and lumi?

    //Mu is 5th and caliblumi is 7th
    // most likely return to these values for Run 3
    //myMu   = my_lum[4];
    //myLumi = my_lum[6];

    myMu   = my_lum[2];
    myLumi = my_lum[4];

    LUM_INITIALIZED = true;

    if (m_isHIrun) {
        myMu = myLumi;
        myLumi = 1.0;
    }

    if (m_debug) {
        std::cout << alertprefix << "myMu = " << myMu << std::endl;
        std::cout << alertprefix << "myLumi = " << myLumi << std::endl;
        std::cout << alertprefix << "End Callback Mu <<<<<" << std::endl;
    }
}

/**
 * @brief Sets the correct indices for each trigger item in each trigger list
 * 
 * For each trigger in hlttrigs:
 *      1. determine its l1 counterpart for L1_index
 *      2. determine its index in hlttrigs
 *      3. add trigname : index to m_hltmap
 * for each trigger in l1trigs:
 *      1. determine its index
 *      2. add trigname : index to l1map
 * ***Only used with new IS data format***
 */
void PredAdapter::setIndices() {
    std::string alertprefix = "Xmon()::setIndices---> ";
    //map HLT to L1
    for (unsigned int i = 0; i < hlttrigs.size(); i++) {
        bool L1MATCHEXISTS = false;
        for (unsigned int j = 0; j < l1trigs.size(); j++) {
            if (hlttrigs[i].L1_name == l1trigs[j].L1_name) {
                hlttrigs[i].L1_index = j;
                L1MATCHEXISTS = true;
                break;
            }
        }
        for (unsigned int j = 0; j < extral1trigs.size(); j++) {
            if (hlttrigs[i].L1_name == extral1trigs[j].L1_name) {
                hlttrigs[i].L1_index = j;
                L1MATCHEXISTS = true;
                break;
            }
        }
        //deactivates HLT if no corresponding L1 is found.
        if (!L1MATCHEXISTS) {
            hlttrigs[i].status = 0;
            std::cout << alertprefix << "Deactivating HLTTrig: " << hlttrigs[i].HLT_name << " - L1Trig: " << hlttrigs[i].L1_name << " -> not found" << std::endl;
        }
    }

    //set indices for triggers
    //create maps trigname : index
    for (unsigned int i = 0; i < hlttrigs.size(); i++) {
        hlttrigs[i].HLT_index = i;
        m_hltmap[m_serverName+'.'+hlttrigs[i].HLT_name] = i;
    }
    for (unsigned int i = 0; i < l1trigs.size(); i++) {
        l1trigs[i].L1_index = i;
        m_l1map[m_serverName+'.'+l1trigs[i].L1_name] = i;
        l1trigs[i].status = true;
    }
    for (unsigned int i = 0; i < extral1trigs.size(); i++) {
        extral1trigs[i].L1_index = i;
        m_extral1map[m_serverName+'.'+extral1trigs[i].L1_name] = i;
        extral1trigs[i].status = true;
    }
    
    TRIG_FORMATTED = true;
}

//=====================================================================================
//Calculations
//      Calculates values for online & offline trigger rate predictions,
//      as well as global offset
//=====================================================================================

/**
 * @brief Calculates all predictions for HLT.
 * 
 * Calculates PS, rate (both offline and OTF), errors, and global offset data.
 * Uses the new data format for Run 3. This calculates predictions for single trigger item.
 */
void PredAdapter::calculate_HLT_new(unsigned int index) {
    std::string alertprefix = "Xmon()::calculate_HLT_new---> ";
    if (m_debug) {
        std::cout << alertprefix << "HLT Trigger: " << hlttrigs[index].HLT_name << std::endl;
        std::cout << alertprefix << "STATUS : " << hlttrigs[index].status << std::endl;
    }

    if (hlttrigs[index].status) {
        // Calculate
        hlttrigs[index].CalcPS();
        hlttrigs[index].PredictRate(myMu, myLumi);
        hlttrigs[index].OTFSetPred(myMu, myLumi);
        hlttrigs[index].OTFPredictRate(myMu, myLumi);
        hlttrigs[index].CalcOTFDeviation();
        hlttrigs[index].setParams("trig", hlttrigs[index].fit_uncertainty, hlttrigs[index].rate, hlttrigs[index].pred);
        hlttrigs[index].calculateError();
        hlttrigs[index].checkError(myLumi);
        hlttrigs[index].setParams("trigOTF", hlttrigs[index].OTF_uncertainty, hlttrigs[index].rate, hlttrigs[index].otfpred);
        hlttrigs[index].calculateError();
        hlttrigs[index].checkError(myLumi);
        hlttrigs[index].publishError(hlttrigs[index].offlineErrorFlag, hlttrigs[index].onlineErrorFlag, hlttrigs[index].offlineSigma, hlttrigs[index].onlineSigma);
        hlttrigs[index].FillHistogram(myLumi);

        // Display Debug
        if (m_debug) hlttrigs[index].DisplayDebug(alertprefix);
    }
}

/**
 * @brief Calculates all predictions for L1.
 * 
 * Calculates PS, rate (both offline and OTF), errors, and global offset data.
 * Uses the new data format for Run 3. This calculates predictions for single trigger item.
 */
void PredAdapter::calculatePred_new(unsigned int index) {
    std::string alertprefix = "Xmon()::calculatePred_new---> ";
    if (m_debug) {}
    if (l1trigs[index].status) {
        // Calculate
        //insert calculation for error tracking member variables here
        l1trigs[index].PredictRate (myMu, myLumi);
        l1trigs[index].OTFSetPred(myMu, myLumi);
        l1trigs[index].OTFPredictRate(myMu, myLumi);
        l1trigs[index].CalcOTFDeviation();
        l1trigs[index].setParams("trig", l1trigs[index].fit_uncertainty, l1trigs[index].rate, l1trigs[index].pred);
        l1trigs[index].calculateError();
        l1trigs[index].checkError(myLumi);
        l1trigs[index].setParams("trigOTF", l1trigs[index].OTF_uncertainty, l1trigs[index].rate, l1trigs[index].otfpred);
        l1trigs[index].calculateError();
        l1trigs[index].checkError(myLumi);
        l1trigs[index].publishError(l1trigs[index].offlineErrorFlag, l1trigs[index].onlineErrorFlag, l1trigs[index].offlineSigma, l1trigs[index].onlineSigma);
        if (l1trigs[index].in_List) {
            l1trigs[index].FillHistogram (myLumi);
        }

        // Display Debug
        if (m_debug)l1trigs[index].DisplayDebug (alertprefix);
    }
    if (m_debug) std::cout << "Calculation Successfull!! <<<<< " << std::endl;
}

/**
 * @brief Unsubscribes from everything, all servers and partitions. Called at end of run (or exit from program).
 * 
 */
void PredAdapter::Stop() {
    std::string alertprefix = "Xmon()::Stop---> ";
    if(m_receiver) {
        std::cout << alertprefix << "Now unsubscribing from TRP server '" << m_serverName << "'..." << std::endl;
        try {
            m_receiver->unsubscribe(".*", false);
        } 
        catch(const std::exception & exception) {
            std::cout << alertprefix << "Cannot unsubscribe from ISS_TRP: " << exception.what() << std::endl;
        }

        std::cout << alertprefix << "Now unsusbscribing from IS server holding 'RunParams'..." << std::endl;
        try {
            m_receiver->unsubscribe("RunParams", ".*");
        }
        catch(const std::exception &exception) {
            std::cout << alertprefix << "Cannot unsubscribe to PrescaleAP server, "  << exception.what() << std::endl;
        }
    }

    if(m_olcReceiver) {
        std::cout << alertprefix << "Now unsusbscribing from OLC server '" << m_olcIsServer << "'..." << std::endl;
        try {
            m_olcReceiver->unsubscribe(m_olcIsServer, ".*");
        }
        catch (const std::exception &ex) {
            std::cout << alertprefix << "Cannot unsubscribe to OLC server, " << ex.what() << std::endl;
        }
    }
}

//=====================================================================================
//Publishing Functions
//      Publishes predictions and global offset data to IS servers
//      New IS data format requires multiple functions due to new classes,
//      but they all have the same structure
//=====================================================================================
/**
 * @brief Publishes all L1 trigger info for a given trigger item to IS.
 * 
 * @param trig_name L1 trigger name that is published
 * @param timPoint_Pred Reference to the predictions corresponding to the relevant trigger item.
 */
void PredAdapter::L1_ISPublish(std::string trig_name, L1_Rate_Pred & timePoint_Pred) {
    std::string alertprefix = "XMon()::L1_ISPublish---> ";
    try {
        IPCPartition p(m_partitionName);
        ISInfoDictionary dict(p);
        std::string rate_server = m_serverName + "." + trig_name + "_pred";
        dict.checkin(rate_server, timePoint_Pred, true);
        if(m_smallDebugOn){
            std::cout<< alertprefix << "server + trig name: " << rate_server << std::endl;
            std::cout<< alertprefix << "timePoint_Pred: " << timePoint_Pred << std::endl;
            setSmallDebug();
        }
    }
    catch (std::exception & ex) {
        ers::warning(predadapter::Issue(ERS_HERE, ex));
        if(m_smallDebugOn){
            std::cout<< "No L1 predicitions" << std::endl;
            setSmallDebug();
        }
    }
}

/**
 * @brief Publishes all HLT trigger info for a given trigger item to IS.
 * 
 * @param trig_name HLT trigger name that is published
 * @param timPoint_Pred Reference to the predictions corresponding to the relevant trigger item.
 */
void PredAdapter::HLT_ISPublish(std::string trig_name, HLT_Rate_Pred &TimePoint_Pred) {
    std::string alertprefix = "XMon()::HLT_ISPublish---> ";
    try {
        IPCPartition p(m_partitionName);
        ISInfoDictionary dict(p);
        std::string rate_server = m_serverName + "." + trig_name + "_pred";
        dict.checkin(rate_server, TimePoint_Pred, true);
        if(m_smallDebugOn){
            std::cout<< alertprefix << "server + trig name: " << rate_server << std::endl;
            std::cout<< alertprefix << "timePoint_Pred: " << TimePoint_Pred << std::endl;
            setSmallDebug();    
        }        
    }
    catch (std::exception & ex) {
        ers::warning(predadapter::Issue(ERS_HERE, ex));
        if(m_smallDebugOn){
            std::cout<< "No HLT predicitions" << std::endl;
            setSmallDebug();
        }
    }
}

/**
 * @brief Publishes all global offset data for a given trigger group to IS
 * 
 * @param trig_name Trigger group name that is published
 * @param timPoint_Pred Reference to the global offset data corresponding to the relevant trigger group.
 */
void PredAdapter::GO_ISPublish(std::string grp_name, Global_Offset & data) {
    std::string alertprefix = "XMon()::GO_ISPublish---> ";
    try {
        IPCPartition p(m_partitionName);
        ISInfoDictionary dict(p);
        std::string rate_server = m_serverName + ".Global_Offset_" + grp_name;
        dict.checkin(rate_server, data, true);
        if(m_smallDebugOn){
            std::cout<< alertprefix << "server + trig name: " << rate_server << std::endl;
            std::cout<< alertprefix << "timePoint_Pred: " << data << std::endl;
            setSmallDebug();
        }               
    }
    catch (std::exception & ex) {
        ers::warning(predadapter::Issue(ERS_HERE, ex));
        if(m_smallDebugOn){
            std::cout<< "No GO predicitions" << std::endl;
            setSmallDebug();
        }
    }
}

/**
 * signal handler
 */
void PredAdapter::signal_handler() {
    if (m_semaphore) {
        m_semaphore->post();
    }
}

