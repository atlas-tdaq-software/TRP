/**
 * @file ErrorTracker.cpp
 * @author Connor Menzel (connor.harrison.menzel@cern.ch)
 * @brief Contains the main code for xMon error tracking functionalities.
 * @version 0.1
 * @date 2022-03-28
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "TRP/Utils.h"
#include <ipc/core.h>
#include <iostream>
#include "ers/ers.h"
#include <time.h>
#include <regex.h>
#include "dqmf/is/Tag.h"
#include "is/inforeceiver.h"
#include "ers/ers.h"
#include "is/serveriterator.h"
#include <errno.h>
#include <stdlib.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <sys/stat.h>
#include "PredAdapter.h"

ErrorTracker::ErrorTracker(){
    std::string alertprefix = "xMon()::ErrorTracker ---> ";

}
/**
 * @brief Sets parameters for the error tracking functionality.
 * 
 * @param typeName The name of the object being tracked, i.e. "trig", "group", "trigOTF".
 * @param warningLevel The threshold for cross-section at which a warning will be raised. Currently defined to be 5sigma from the prediciton.
 * @param errorLevel The threshold for cross-section at which an error will be raised. Currently defined to be 7sigma from the prediction.
 * @param live The live trigger rate pulled from IS, or the current ks-value for global offset functionality.
 * @param expected The predicted trigger rate or expected ks-value. To be compared to the live value.
 */
void ErrorTracker::setParams(std::string typeName, float sigma, float live, float expected){
    m_typeName = typeName;
    m_errorLevel = 7*sigma;
    m_warningLevel = 5*sigma;
    m_predSigma = sigma;
    m_live = live;
    m_expected = expected;
}

/**
 * @brief Calculates the residual for the current time point, defined as abs(live-expected). This is the value that is checked to be within error thresholds.
 * 
 */
void ErrorTracker::calculateError(){
    std::string alertprefix = "xMon()::ErrorTracker ---> ";

    m_error = m_live - m_expected;//this is error in rate, but allowed error is in cross-section
}

/**
 * @brief Compares calculated error to the warning and error thresholds. Sets error flag to 2 if there is error, 1 if there is a warning, 0 else.
 * 
 * @param lumi Live luminosity taken from IS.
 */
void ErrorTracker::checkError(float lumi){
    std::string alertprefix = "xMon()::ErrorTracker ---> ";

    //check type of item we are dealing with
    //trigger group
    if(m_typeName == "global"){
        m_offlinenSigma = m_error / m_predSigma;
        if(TMath::Abs(m_error)>m_errorLevel){
            m_offlineErrorFlag = 2;
        }else if(m_error>m_warningLevel){
            m_offlineErrorFlag = 1;
        }else{
            m_offlineErrorFlag = 0;
        }
    //otf predictions
    }else if (m_typeName == "trigOTF"){
        if (m_predSigma != 0.0){
            m_onlinenSigma = m_error / (m_predSigma*lumi);
            if (TMath::Abs(m_error)>(m_errorLevel*lumi)){
                m_onlineErrorFlag = 2;
            }else if (m_error>(m_warningLevel)){
                m_onlineErrorFlag = 1;
            }else{
                m_onlineErrorFlag = 0;
            }
        }
    //reference-based offline predictions
    }else{
        if (m_predSigma != 0.0){
            m_offlinenSigma = m_error / (m_predSigma*lumi);
            if (TMath::Abs(m_error)>(lumi*m_errorLevel)){
                m_offlineErrorFlag = 2;
            }else if(m_error>(lumi*m_warningLevel)){
                m_offlineErrorFlag = 1;
            }else{
                m_offlineErrorFlag = 0;
            }
        }
    }
}

/**
 * @brief This sets the error flags in the trigger object so they can be accessed in PredAdapter.cpp since they are protected member variables in ErrorTracker class.
 * 
 * @param offlineError Error flag variable of the object.
 * @param onlineError Online error flag variable of the object.
 */
void ErrorTracker::publishError(int& offlineError, int& onlineError, float& offlineSigma, float& onlineSigma){
    //this just serves to set the trigger objects member variable from the ErrorTracker, since its member variables are protected.
    offlineError = m_offlineErrorFlag;
    onlineError = m_onlineErrorFlag;
    offlineSigma = m_offlinenSigma;  
    onlineSigma = m_onlinenSigma;
}
