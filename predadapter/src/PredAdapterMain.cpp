/** \file PredAdapterMain.cpp
 * \authors Tae Min Hong
 * \authors Joerg Stelzer
 * \authors Connor Menzel
 * \authors Enzo Brandani
 * \brief This is the main file for xMon, a trigger rate monitoring tool installed at Point 1
 * \details xMon uses three distinct methods to monitor trigger rates at ATLAS:
 * 1. Offline Predictions
 *      - xMon compares the live rates to predicted rates calculated offline, using fits of cross section 
 *        versus pileup. It inputs live pileup into the prediction function to get the predicted cross section,
 *        which is then used to compute the predicted rate. Offline fits can currently be linear, quadratic, or exponential.
 * 2. On-the-Fly Predictions
 *      - On-the-Fly (OTF) predictions use the same procedure as offline predictions, but the fit is computed with the previous 
 *        five hours of data collected from ATLAS. After this attenuation period, the predictions snap to the correct value.
 * 3. Global Offset Detection
 *      - Global offset detection allows for the detection of small offsets among many triggers caused by some systemic issue. 
 *        We use a reference run to compare the distribution of the difference between the live rate and the 
 *        predicted rate. We then compare these reference histograms to live histograms of the same type filled as ATLAS 
 *        collects data.
*/

//Include section
#include "PredAdapter.h"

#include "TRP/Args.h"
#include "TRP/Utils.h"
#include "config/Configuration.h"
#include "ers/ers.h"
#include "ipc/partition.h"
#include <ipc/core.h>
#include <is/inforeceiver.h>
#include "is/serveriterator.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <fstream>
#include <signal.h>
#include <iostream>

namespace pt = boost::property_tree;

std::string partition_name;
std::string server_name;
std::string lumi_partition;
std::string object_name;
std::string lumi_server;
std::string predict_path;
std::string ref_path;
std::string l1_name_out;
std::string triggerdb_alias;
std::string hlt_name_out;
std::string trigger_groups;
bool enableDebug;
bool isHI;
bool newISFormat;
bool isTechRun;

std::string mainalertprefix;

namespace {
    PredAdapter* pred_adapter{nullptr};
    int ReceivedSignal = 0;

    void signal_handler( int sig ) {
        std::cout << "Calling signal handler with " << sig << std::endl;
        if (pred_adapter) {
            ReceivedSignal = sig;
            pred_adapter->signal_handler();
        }
    }
}

template<class T>
std::optional<T> getAttr(const pt::ptree & node, const std::string & path, const std::string & attribute, bool isRequired = false) {
    try {
        return node.get_child(path).get<T>("<xmlattr>." + attribute);
    }
    catch(std::exception &ex) {
        if(isRequired) {
            std::cerr << "Could not read required attribute " << attribute << " under " << path << std::endl;
            std::cerr << "As it is required, need to abort" << std::endl;
            throw;
        } else {
            return std::nullopt;
        }
    }
}
/** @brief Reads the xml configuration file
 * 
 * @details We no longer use xml configuration files. We typically use json files now, so this function 
 * is probably no longer needed.
 * 
 * @param config_file The xml file with all necessary configurations for predadapter
 * @return True if xml file is successfully read
 * @throw 
 */
bool readConfig(const std::string & config_file) {
    

    std::cout << mainalertprefix << "Read xmon adapter config file " << config_file << std::endl;
    pt::ptree cfg;
    try {
        pt::read_xml(config_file, cfg);
    }
    catch(std::exception &ex) {
        std::cerr << "Could not read configuration file: " << ex.what() << std::endl;
        throw;
    }

    bool isRequired{true};
    server_name = getAttr<std::string>(cfg, "adapter.common", "server", isRequired).value();
    predict_path = getAttr<std::string>(cfg, "adapter.predadapter", "PredPath", isRequired).value();

    ref_path = getAttr<std::string>(cfg, "adapter.predadapter", "HistPath").value_or("");

    // related to luminosity input
    lumi_partition = getAttr<std::string>(cfg, "adapter.predadapter", "lumiPartition").value_or("ATLAS");
    lumi_server    = getAttr<std::string>(cfg, "adapter.predadapter", "lumiServer").value_or("OLC");
    object_name    = getAttr<std::string>(cfg, "adapter.predadapter", "objectName").value_or("");

    l1_name_out  = getAttr<std::string>(cfg, "adapter.predadapter", "l1outputname").value_or("L1_Rate_Pred");
    hlt_name_out = getAttr<std::string>(cfg, "adapter.predadapter", "hltoutputname").value_or("HLT_Rate_Pred");

    triggerdb_alias = getAttr<std::string>(cfg, "adapter.predadapter", "triggerDBAlias").value_or("TRIGGERDB");
    trigger_groups  = getAttr<std::string>(cfg, "adapter.predadapter", "trigger_groups").value_or("MU,XE,EM");

    newISFormat = getAttr<bool>(cfg, "adapter.predadapter", "newISFormat").value_or(true);
    enableDebug = getAttr<bool>(cfg, "adapter.predadapter", "debug").value_or(false);
    isHI        = getAttr<bool>(cfg, "adapter.predadapter", "isHI").value_or(false);
    isTechRun   = getAttr<bool>(cfg, "adapter.predadapter", "isTechRun").value_or(false);

    return true;
}

/**
 * @brief Reads json configuration file
 * 
 * @param config_file The configuration file used for xMon
 * @param partitionName Partition xMon reads from
 * @return true If configuration file is successfully read 
 */

bool readConfigJson(const std::string & config_file, const std::string & partitionName) {

    std::cout << mainalertprefix << "Read xmon adapter config from json file " << config_file << " for partition " << partitionName << std::endl;
    pt::ptree cfg;
    try {
        pt::read_json(config_file, cfg);
    }
    catch(std::exception &ex) {
        std::cerr << "Could not read configuration file: " << ex.what() << std::endl;
        throw;
    }

    const pt::ptree & pCfg = cfg.get_child(partitionName);

    server_name = pCfg.get<std::string>("RateISServer"); 
    predict_path = pCfg.get<std::string>("PredPath");

    // related to luminosity input
    //setting global variables from config file
    lumi_partition = pCfg.get_optional<std::string>("lumiPartition").value_or(partitionName);
    lumi_server    = pCfg.get_optional<std::string>("lumiServer").value_or("OLC");
    object_name    = pCfg.get_optional<std::string>("objectName").value_or("");

    l1_name_out  = pCfg.get_optional<std::string>("l1outputname").value_or("L1_Rate_Pred");
    hlt_name_out = pCfg.get_optional<std::string>("hltoutputname").value_or("HLT_Rate_Pred");

    triggerdb_alias = pCfg.get_optional<std::string>("triggerDBAlias").value_or("TRIGGERDB");

    newISFormat = pCfg.get_optional<bool>("newISFormat").value_or(true);
    enableDebug = pCfg.get_optional<bool>("debug").value_or(false);
    isHI        = pCfg.get_optional<bool>("isHI").value_or(false);
    isTechRun   = pCfg.get_optional<bool>("isTechRun").value_or(false);

    // related to global offset feature
    ref_path = pCfg.get_optional<std::string>("HistPath").value_or("");
    trigger_groups  = pCfg.get_optional<std::string>("trigger_groups").value_or("MU,XE,EM");

    return true;
}

void showHelp(std::ostream& os, std::string pgmName){
    os
    << "Usage: " << pgmName << " [-p <str>] [-c <file.xml>] [-s <str>] [-k <str>] [-out <str>]" << std::endl
    << std::endl
    << "Options/Arguments:" << std::endl
    << "  -p <str>         partition name  (if not present $TDAQ_PARTITION is used)" << std::endl
    << "  -c <file1.xml>   xml file with configuration info" << std::endl
    << std::endl
    << "  or in alternative from the command line as follows" << std::endl
    << "  -s <str>   server name" << std::endl
    << "\nDescription:" << std::endl
    << "   The PredAdapterMain gets infos (IS_LB,   ) from IS" << std::endl
    << "   reads configurations at each LumiBlock, performs rate" << std::endl
    << "   calculatiom and stores in IS TimePoint variable with" << std::endl
    << "   dynamical time intervals.";
    exit(1);
}

//see possible arguments in showHelp()
int main( int ac, char* av[] ) {
    mainalertprefix = "Xmon()::Main---> ";

    // get arguments
    Args args( ac, av );

    // get program name
    std::string pgmName = av[0];
    std::string::size_type pos = pgmName.rfind( '/' );
    if ( pos != std::string::npos ) {
        pgmName = pgmName.substr( pos + 1 );
    }

    // check if requested help
    if ( args.hasArg("-h") || args.hasArg("-?") || args.hasArg("--help") ) {
        showHelp( std::cerr, pgmName );
    }

    // Retrive the partition name (from command line or from environment)
    if (args.hasArg("-p")) { // partition with HLT rates
        partition_name = args.getStr("-p");
        ERS_DEBUG(0, "Retrieved partition name from command line: '" << partition_name << "'");
        std::cout << mainalertprefix << "Retrieved partition name from command line: " << partition_name << std::endl;
    }
    else if (getenv("TDAQ_PARTITION")) {
        partition_name = getenv("TDAQ_PARTITION");
        ERS_DEBUG(0, "Retrieved partition name from environment: '" << partition_name << "'");
    }
    else {
        std::cerr << "Partition name not set (neither in command line nor in environment)" << std::endl;
        showHelp( std::cerr, pgmName );
    }

    //manual key input
    int smk = args.hasArg("--smk") ? args.getInt("--smk") : 0;
    int l1psk = args.hasArg("--l1k") ? args.getInt("--l1k") : 0;
    int hltpsk = args.hasArg("--hltk") ? args.getInt("--hltk") : 0;

    //reads everything from configuration file
    if (args.hasArg("-c")) { // configuration file
        std::string config_file(args.getStr("-c"));
        ERS_LOG("Config file from command line: '" << config_file  << "'");
        bool success{false};
        
        if(config_file.substr(config_file.size()-5) == ".json") {
            success = readConfigJson(config_file, partition_name);
        } else {
            success = readConfig(config_file);
        }
        if(!success) {
            std::cerr << "Can't read configuration file" << std::endl;
            return 1;
        }
    } 
    // basic settings have to come from commnand line (should also be abandoned)
    else {         // retrieve the Server Name
        if (args.hasArg("-s")) {
            server_name = args.getStr("-s");
            ERS_DEBUG(0, "Retrieved server name from command line: '" << server_name << "'");
        } else {
            std::cerr << "Server name not set in command line" << std::endl;
            showHelp( std::cerr, pgmName );
        }
        if (args.hasArg("-d")) {
            enableDebug = true;
            ERS_DEBUG(0, "The enableDebug setting is : '" << enableDebug << "'");
        }
        if (args.hasArg("-out")) {
            l1_name_out = args.getStr("-out");
            ERS_DEBUG(0, "Retrieved outputname from command line: '" << l1_name_out << "'");
        }
    }

    //printing configuration information
    std::cout << "Configuration" << std::endl;
    std::cout << "=============" << std::endl;
    std::cout << "  Main parameters" << std::endl;
    std::cout << "    partition          : " << partition_name << std::endl;
    std::cout << "    predition file     : " << predict_path << std::endl;
    std::cout << "    rate IS server     : " << server_name << std::endl;
    std::cout << "    L1 predictions IS  : " << l1_name_out << std::endl;
    std::cout << "    HLT predictions IS : " << hlt_name_out << std::endl;
    std::cout << "    Trigger DB alias   : " << triggerdb_alias << std::endl;
    std::cout << "    Run 3 Rate format  : " << (newISFormat ? "true" : "false") << std::endl;
    std::cout << "    HI predictions     : " << (isHI ? "true" : "false") << std::endl;
    std::cout << "    Technical run      : " << (isTechRun ? "true" : "false") << std::endl;
    std::cout << "    Debug              : " << (enableDebug ? "true" : "false") << std::endl;
    std::cout << "  Luminosity related" << std::endl;
    std::cout << "    lumi partition     : " << lumi_partition << std::endl;
    std::cout << "    lumi IS server     : " << lumi_server << std::endl;
    std::cout << "    lumi IS object     : " << object_name << std::endl;
    std::cout << "  Global offset related" << std::endl;
    std::cout << "    reference file     : " << ref_path << std::endl;
    std::cout << "    trigger groups     : " << trigger_groups << std::endl;
    if(smk!=0) {
        std::cout << "DB keys for testing" << std::endl;
        std::cout << "===================" << std::endl;
        std::cout << "   smk     : " << smk << std::endl;
        std::cout << "   l1 psk  : " << l1psk << std::endl;
        std::cout << "   hlt psk : " << hltpsk << std::endl;
    }

    try {
        IPCCore::init( ac, av );
        std::cout << mainalertprefix << "IPCCore::intialized(acav)" << std::endl;
    }
    catch ( daq::ipc::Exception & ex ) {
        ers::fatal( ex );
        return 1;
    }

    ERS_DEBUG(0, "Prediction Adapter: Starting program job" );
    std::cout << mainalertprefix << "Creating Prediction Adapter!!!" << std::endl;
    pred_adapter = new PredAdapter( partition_name, server_name, object_name,
                                    triggerdb_alias, l1_name_out, hlt_name_out, trigger_groups,
                                    enableDebug
                                ); //create instance of PredAdapter class
    pred_adapter->SetHI(isHI);        // set Run-type! 
    pred_adapter->SetIS(newISFormat); // set rate IS format
    pred_adapter->SetTest(isTechRun); // set technical run
    pred_adapter->setLumiPartition(lumi_partition, lumi_server); // set lumi IS information

    if(smk!=0) {
        pred_adapter->setDBKeysForTesting(smk, l1psk, hltpsk);
    }

	// read trigger db info
    if( ! pred_adapter->readTriggerDBInfo()) {
        std::cerr << "Failed to read trigger DB. Exiting." << std::endl;
        return 1;
    }
    std::cout << mainalertprefix << "DB Info Successful" << std::endl;

    // subscribe to IS and block the main function with semaphore.wait()
    if( ! pred_adapter->PredConfigure(predict_path) ) {
        std::cerr << "Failed to read prediction file. Exiting." << std::endl;
        return 1;
    }
    std::cout << mainalertprefix << "HLT Prediction Configure Successful" << std::endl;

    //check HLT dependencies on L1
    pred_adapter->CheckHLT();

    // global offset configuration
    if(pred_adapter->RefConfigure(ref_path)) {
        std::cout << mainalertprefix << "Global Offset Configure Successful" << std::endl;
        pred_adapter->ParseTriggers();
        std::cout << mainalertprefix << "Trigger Groups Created" << std::endl;
    } else {
        std::cout << mainalertprefix << "Global Offset not configured. That feature is currently in development and will be ignored." << std::endl;
    }

    if(! pred_adapter->SubscribeToIS()) {
        std::cerr << "Failed to subscribe to all needed IS information. Exiting." << std::endl;
        return 1;                
    }
    std::cout << mainalertprefix << "IS subscriptions done" << std::endl;

    signal(SIGABRT, signal_handler); //signal 6
    signal(SIGTERM, signal_handler); //signal 15
    signal(SIGINT,  signal_handler); //signal 2
    std::cout << mainalertprefix << "Starting run!!!" << std::endl;
    pred_adapter->Run(); //starts run

    // when signal received, signal_handler calls semaphore.post()
    if(ReceivedSignal) {
        ERS_LOG( "Signal " << ReceivedSignal << " received, stopping programm");
        pred_adapter->Stop();
    }

    // unsubscribe to IS, this code executed after semaphore.wait()
    //pred_adapter->Stop();
    std::cout << mainalertprefix << "Stop successful" << std::endl;

    ERS_LOG("Program terminated gracefully" );
    delete pred_adapter;
    return 0;
}
