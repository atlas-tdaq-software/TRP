/**
 * CHANGELOG
 * May 2017 - Daniel Zheng - Created PredAdapter.h to combine HLT and L1 Predadapters
 * August 2019 - Nick Felice - Created global offset functionality
 */


#ifndef predadapter_h
#define predadapter_h



#include <ipc/core.h>
#include <ipc/partition.h>

#include "owl/semaphore.h"

#include <is/inforeceiver.h>
#include <is/infodynany.h>

#include "TRP/TriggerMenuHelpers.h"
#include "TRP/TimePoint_IS.h"
#include "TRP/L1_Rate.h"
#include "TRP/HLT_Rate.h"
#include "TRP/L1_Rate_Pred.h"
#include "TRP/HLT_Rate_Pred.h"
#include "TRP/Global_Offset.h"
#include "ErrorTracker.h"

#include "TFormula.h"
#include "TH1F.h"
#include "TFile.h"
#include "TTree.h"
#include "TMath.h"

#include <vector>
#include <string>
#include <numeric>
#include <unordered_map>
#include <cmath>

/** @class PredAdapter PredAdapter.h
 * @brief The main class that publishes trigger rate predictions to compare to live rates.
 * 
 */
class PredAdapter {
    public:
        /**
         * Declare all the Class Level Variables and Members and Structures
         * 
         */
        PredAdapter(std::string partition_name,
                    std::string server_name,
                    std::string object_name,
                    std::string alias,
                    std::string L1_name_out,
                    std::string hlt_name_out,
                    std::string trigger_groups,
                    bool debug);
        ~PredAdapter() = default;
        PredAdapter(const PredAdapter &)=delete; // prevent copying

        // ------------------------------------------------
        // Public member methods necessary for running xMon
        // ------------------------------------------------

        void setLumiPartition(std::string lumiPartition, std::string lumiISServer);
        void SetHI(bool);
        void SetIS(bool);
        void SetTest(bool);
        void setDBKeysForTesting(unsigned int smk, unsigned int l1psk, unsigned int hltpsk);
        bool PredConfigure(const std::string & predictionFile);
        bool RefConfigure(std::string histPath);
        bool readTriggerDBInfo();
        void CheckHLT();
        void ParseTriggers();
        bool SubscribeToIS();
        void Run();
        void Stop();
        void signal_handler();
        
    private:
        static const bool success = true;
        static const bool failure = false;

        // ------------------------------------------------
        std::string m_partitionName;
        std::string m_objectName;
        std::string m_serverName;
        std::string m_lumiPartition;
        std::string m_olcIsServer;
        std::string m_l1OutputPartition;
        std::string m_hltOutputPartition;
        std::string m_triggerDB;
        std::string m_trigger_groups;
        std::string m_isServerFolder{"RunParams"};

        // ------------------------------------------------
        /// configuration flags
        bool m_isHIrun{false};
        bool m_newISFormat{false};
        bool m_isTechRun{false};

        //for debugging without generating massive log files
        bool m_smallDebugOn{false};
        int  m_smallDebugCounter = 0;

        // ------------------------------------------------
        /**
         * @defgroup ISInfoRecievers 
         * for accessing data from IS Servers
         * @{
         */
        std::unique_ptr<ISInfoReceiver>  m_receiver;    /** @brief IS receiver for accessing L1 and HLT trig data */ 
        std::unique_ptr<ISInfoReceiver>  m_olcReceiver; /** @brief IS receiver for acessing OLC data */
        OWLSemaphore* m_semaphore{nullptr};             /** @brief Semaphore pointer for synchronization between xMon processess and IS processess */
        /** @} */

        // ------------------------------------------------
        unsigned int m_smk{0};        // super master key
        unsigned int m_l1k{0};        // L1 trig prescale key
        unsigned int m_hltk{0};       // HLT trig prescale key

        // ------------------------------------------------
        bool L1_INITIALIZED{false};
        bool HLT_INITIALIZED{false};
        bool LUM_INITIALIZED{false};
        bool L1_FORMATTED{false};
        bool L1_ISCALLBACK{false};    // combining L1_ISCALLBACK/CALCULATING & HLT_ISCALLBACK/CALCULATING/
        bool L1_ISCALCULATING{false};
        bool HLT_isCallBack{false};
        bool HLT_isCalculating{false};
        bool TRIG_FORMATTED{false};

        bool m_debug;

        // ------------------------------------------------
        std::vector<std::string>  hlt_name_ps;
        std::vector<int>          hlt_ps;
        unsigned int              hlt_ps_size;

        // ------------------------------------------------
        unsigned int size_yHLT{0};
        int output_index{0};

        // ------------------------------------------------
        // Functions (see PredAdapter.cpp for detailed description)
        // ------------------------------------------------

        void callback_l1k(ISCallbackInfo *isc);
        void callback_hltk(ISCallbackInfo *isc);
        void callback_Mu(ISCallbackInfo *isc);
        void callback_l1rates(ISCallbackInfo *isc);
        void callback_hltrates(ISCallbackInfo *isc);
        void newcallback_hltrates(ISCallbackInfo *isc);
        void newcallback_l1rates(ISCallbackInfo *isc);
        void newcallback_l1rates_simple(ISCallbackInfo *isc);

        void calculate_HLT();
        void calculatePred();
        void calculate_HLT_new(unsigned int);
        void calculatePred_new(unsigned int);
        void setIndices();
        int  getL1Prescale(unsigned int);

        void HLT_ISPublish(std::string, HLT_Rate_Pred&);
        void L1_ISPublish(std::string, L1_Rate_Pred&);
        void GO_ISPublish(std::string, Global_Offset&);
        void publishIS(std::string, TimePoint_IS&);

        // ------------------------------------------------
        float myMu{0.};
        float myLumi{0.};
        float pDeadtime{0.};

        // ------------------------------------------------
        TimePoint_IS ISC_L1TimePoint;
        TimePoint_IS ISC_HLTTimePoint;
        TimePoint_IS Pred_TimePoint_HLT;
        TimePoint_IS Pred_TimePoint_L1;

        // ------------------------------------------------
        unsigned int    HLT_RunNumber{0};
        unsigned int    L1_RunNumber{0};
        unsigned short  HLT_LumiBlock{0};
        unsigned short  L1_LumiBlock{0};
        OWLTime         HLT_TimeStamp;
        OWLTime         L1_TimeStamp;

        // ------------------------------------------------
        // The number of L1 triggers on IS
        unsigned int l1rates_IS_trigN{0};
        unsigned int hltrates_IS_trigN{0};

        // ------------------------------------------------
        unsigned int YLabels_size{0}; // Number of Labels form rate object
        unsigned int TBP_index{0};    // Position of TBP in rate object
        unsigned int TAP_index{0};    // Position of TAP in rate object
        unsigned int PS_index{0};     // The position of PS from rate object

        //-------------------------------------------------
        // internal maps from trigger name to index
        std::unordered_map<std::string, unsigned int> m_hltmap;
        std::unordered_map<std::string, unsigned int> m_l1map;
        std::unordered_map<std::string, unsigned int> m_extral1map;


        /**
         * @brief print some debug statements only 5 times
         * to debug with small log files.
         */
        void setSmallDebug(){
                if(m_smallDebugCounter < 5){m_smallDebugCounter++;}
                else{m_smallDebugOn = false;}
            }

        /**
         * @brief Trigger Ratio structure,
         *  
         * used mainly for configuring xMon with the prediction file.
         * 
         */    
        struct trigger_ratio: public ErrorTracker {
            std::string nominator{""};
            std::string denominator{""};
            unsigned int xIndex_first{0};
            unsigned int xIndex_second{0};
            float rate_first{0.};
            float rate_second{0.};
            float ratio_calc{0.};
            float ratio_pred{0.};
            bool calc_success{false};
            bool found_first{false};
            bool found_second{false};
            bool status{false};
            float p1{0.};
            float p0{0.};
            int counter{0};
            float otfpred{0};

            std::vector<float> X;
            std::vector<float> Y;

            // Equations - Root stuff, Pred Paramaters usually only 2 slots used
            TString    equation;
            TFormula*  predFormula{nullptr};
            double     pred_parameters[10];
            float      ratio_uncertainty{0.0};
            
            //Brandani: Neither of these OTF functions are used, are they necessary?

            void OTFSetPred(float Mu) {
                float x = 0;
                float y = 0;
                if (Mu > 15) { // at low mu we don't care
                    x = Mu;
                    y = ratio_calc;
                    X.push_back(x);
                    Y.push_back(y);
                }

                if (X.size() > 800) X.erase(X.begin());
                if (Y.size() > 800) Y.erase(Y.begin());


                if (counter % 40 == 0) {
                    double xsum  = 0;
                    double ysum  = 0;
                    double xxsum = 0;
                    double xysum = 0;
                    int N = 0;

                    for (unsigned int i = 1; i < X.size(); i++) {
                        N++;
                        xsum += X[i];
                        ysum += Y[i];
                        xxsum += (X[i]*X[i]);
                        xysum += (X[i]*Y[i]);
                    }

                    //Calculate the new best fit slope/intercept
                    if (xsum*xsum - N*xxsum != 0) {
                        p1 = (ysum*xsum  - N*xysum)     / (xsum*xsum - N*xxsum);
                        p0 = (xsum*xysum - ysum* xxsum) / (xsum*xsum - N*xxsum);
                    }
                }
                counter++;
            }
            // ------------------------------------------------
            // OTF Predict Rate
            // ------------------------------------------------
            void OTFPredictRate(float Mu) {
                otfpred = (p1*Mu + p0);

                if (otfpred > 50000) otfpred = 50000;
            }
            /**
             * @brief Calculates ratio between rates of two triggers
             * 
             */
            void CalcRatio() {
                if (rate_second == 0) {
                    calc_success = false;
                }
                else {
                    ratio_calc = rate_first/rate_second;
                    if (ratio_calc > 10000) {
                        ratio_calc = 10000;
                    }
                    calc_success = true;
                }
            }

            /**
             * @brief Set the Formula object for predicting trig ratios
             * 
             * @param equation 
             * @param pred_parameters 
             * @return true 
             * @return false 
             */
            bool setFormula(std::string equation, double pred_parameters[10], double error_parameters[10]) {
                const char * use_name = (nominator + "/" + denominator).c_str();
                float variance = 0.0;
                for (int i=0; i<4; i++){
                    variance += TMath::Power(error_parameters[i],2);
                }
                ratio_uncertainty = TMath::Sqrt(variance);
                predFormula = new TFormula(Form("predFormula_%s",use_name), equation.c_str()) ;
                if (predFormula) {
                    predFormula->SetParameters(pred_parameters);
                    return true ;
                } else {
                    std::cout << "(trigger_ratio::setFormula()) Error, could not create TFormula for " << nominator + "/" + denominator << std::endl;
                    return false ;
                }
            }

            /**
             * @brief Performs Ratio Ratio Prediction
             * 
             */
            void PredictRatio (float Mu) {
                ratio_pred = predFormula->Eval(Mu);
            }

            std::string GetName() {
                return (nominator + "/" + denominator);
            }
        };

        /**
         * @struct Trigger Item
         * @brief Contains all trigger data and actual rate prediction methods. 
         * 
         */
        struct trigger_item: public ErrorTracker {

            // ============================================================
            // Variables
            // ============================================================
            bool                   is_HLT{false};     // tracks if hlt or l1
            float                  pred_unpresc{0.};  // added from L1
            float                  rate_unpresc{0.};  // added from L1

            std::string            L1_name;
            std::string            HLT_name;
            unsigned int           xIndex{0};
            unsigned int           L1_index{0};
            unsigned int           HLT_index{0};
            unsigned int           HLT_psIndex{0};
            int                    L1_PS{0};
            int                    HLT_PS{0};
            int                    PS{0};
            float                  pred{0.};
            float                  otfpred{0.};
            float                  rate{0.};
            float                  diff{0.};
            std::vector<float>     X{};
            std::vector<float>     Y{};
            std::vector<float>     OTF_residuals{};
            float                  OTF_uncertainty{0.};
            float                  p1{0.};
            float                  p0{0.};
            int                    counter{0};
            bool                   in_List{false}; // tracks if the trigger is in a global offset trigger_list
            unsigned int           list_Index{0};
            int                    vec_count{0};
            float                  error{0};
            float                  fit_uncertainty{};
            int                    offlineErrorFlag{0};
            int                    onlineErrorFlag{0};
            float                  offlineSigma{0};
            float                  onlineSigma{0};

	        //Histogram stuff 
            double trig_data[500];
            std::vector<Double_t> ref_data;
            int n_ref;
            bool ref_config{false};
            bool vecFull{false};
            Double_t ksTest{0.};
			std::vector<Double_t> ks_vec{};
			Double_t pValue{0.};
			std::vector<Double_t> p_vec{};
            Double_t relativeMean{0.};
			std::vector<Double_t> mean_vec{};

            // Status
            bool status{false};

            //Initialization
            bool initialized{false};

            // Check PS 0 = prescale is less than 1, 1 = prescale is not less than 1
            bool checkPS{false};

            // Equations - Root stuff, Pred Paramaters usually only 2 slots used
            std::string		equation;
            double		    pred_parameters[10];
            TFormula*       predFormula{nullptr};

            //IS Info
            L1_Rate_Pred    L1_IS_Pred;
            HLT_Rate_Pred   HLT_IS_Pred;
            L1_Rate         ISC_L1;
            HLT_Rate        ISC_HLT;

            // ============================================================
            // Member Functions
            // ============================================================

            /**
             * @brief Set the Formula object for L1 and HLT rate predictions using ROOT
             * 
             * @param equation 
             * @param pred_parameters 
             * @return true 
             * @return false 
             */
            bool setFormula(std::string equation, double pred_parameters[10], double error_parameters[10]) {
                const char * use_name;
                float variance = 0.0;
                for (int i=0; i<4; i++){
                    variance += TMath::Power(error_parameters[i],2);
                    std::cout << "err"+std::to_string(i)+" = " << error_parameters[i] << std::endl;
                }
                std::cout << variance << std::endl;
                fit_uncertainty = TMath::Sqrt(variance);
                std::cout << fit_uncertainty << std::endl;
                if (is_HLT) use_name = HLT_name.c_str();  // for HLT
                else
                    use_name = L1_name.c_str();        // for L1
                predFormula = new TFormula(Form("predFormula_%s",use_name), equation.c_str()) ;
                if (predFormula) {
                    predFormula->SetParameters(pred_parameters) ;
                    return true ;
                }
                std::cout << "(trigger_item::setFormula()) Error, could not create TFormula for " << HLT_name << std::endl ;
                return false ;
            }

            /**
             * @brief Sets parameters for triggers not formatted during run.
             * 
             * @param Mu Mu
             * @param L  Lumi
             */
            void OTFSetPred(float Mu, float L) {
                float x = 0;
                float y = 0;
                if ((L != 0) && (rate_unpresc > 1)){
                    x = Mu;
                    y = float(rate_unpresc)/float(L);
                    X.push_back(x);
                    Y.push_back(y);
                }
                if (X.size() > 500) X.erase(X.begin()); //why? to fix the size of the vector so we don't keep adding points
                if (Y.size() > 500) Y.erase(Y.begin());

                if (counter % 20 == 0) {
                    double xsum  = 0;
                    double ysum  = 0;
                    double xxsum = 0;
                    double xysum = 0;
                    int N = 0;

                    if (is_HLT){
                        for (unsigned int i = 0; i < X.size() || i < Y.size(); i++) {
                            N++;
                            xsum += X[i];
                            ysum += Y[i];
                            xxsum += (X[i]*X[i]);
                            xysum += (X[i]*Y[i]);
                        }
                    }else{
                        for (unsigned int i = 1; i < X.size(); i++) {
                            N++;
                            xsum += X[i];
                            ysum += Y[i];
                            xxsum += (X[i]*X[i]);
                            xysum += (X[i]*Y[i]);
                        }
                    }

                    N++; // Implied point 0,0

                    //Calculate the new best fit slope/intercept
                    if (xsum*xsum - N*xxsum != 0) {
                        p1 = (ysum*xsum  - N*xysum)     / (xsum*xsum - N*xxsum);
                        p0 = (xsum*xysum - ysum* xxsum) / (xsum*xsum - N*xxsum);
                    }
                }

                counter++;

            }


            // ------------------------------------------------
            // PS
            // ------------------------------------------------
            void CalcPS() {
                PS = abs(L1_PS * HLT_PS);
            }

            /**
             * @brief Calculates OTF prediction rate for L1 and HLT triggers.
             * 
             * Uses parameters set by OTFSetPred
             * 
             * Limits OTF predictions to 50 kHz. 
             * 
             * @param Mu Mu
             * @param L  Lumi
             */
            void OTFPredictRate(float Mu, float L) {
                otfpred = (p1*Mu + p0)*L;
                if (is_HLT){
                    if (PS != 0 && HLT_PS > 0) otfpred = otfpred/(double)PS;
                    else if (L1_PS != 0) otfpred = otfpred/(double)abs(L1_PS);  // Not recorded but still has rate
                    else if (HLT_PS == -1) otfpred = 0;  // No rate
                    else otfpred = 0;
                }else{
                    if (checkPS)  { otfpred = otfpred/PS; } //simple accounting for prescaling, divide rate by prescale factor
                    else          { otfpred = otfpred;    }
                }

                if (otfpred > 50000) otfpred = 50000;

                float difference = otfpred - pred;
                OTF_residuals.push_back(difference);
            }

            /**
             * @brief Calculates the uncertainty in the OTF predictions by calculating the standard deviation of the residuals.
             * 
             */
            void CalcOTFDeviation(){
                float variance = 0;
                for(long unsigned int i=0; i<OTF_residuals.size(); i++){
                    variance += TMath::Power(OTF_residuals[i],2);
                }
                OTF_uncertainty = TMath::Sqrt(variance / (OTF_residuals.size()-2));
            }


            /**
             * @brief Predict rate for L1 and HLT triggers using formulas from fit file evaluated at live values for Mu and Lumi.
             * 
             * @param Mu Mu
             * @param L Lumi
             */
            void PredictRate(float Mu, float L) {

				if (predFormula==nullptr){
					return;
				}

                if (is_HLT){
                    pred = predFormula->Eval(Mu)*L;
                    if (PS != 0 && HLT_PS > 0) pred = pred/(double)PS;
                    else if (L1_PS != 0) pred = pred/(double)L1_PS;  // Not recorded but still has rate
                    else if (HLT_PS == -1) pred = 0;  // No rate
                    else pred = 0;
                }else{
                    pred_unpresc = predFormula->Eval(Mu)*L;
                    if (checkPS)  { pred = pred_unpresc/PS; }
                    else          { pred = pred_unpresc;    }
                }
            }

    	    /**
    	     * @brief Fill reference histogram for global offset calculations.
    	     * 
    	     * @param lumi 
    	     */
            void FillHistogram(float lumi) {

                if (ref_config) {
                    int n = 500;
    	            float nSigma = offlineSigma;
                    Double_t *ref_arr = new Double_t[ref_data.size()];
					std::sort(ref_data.begin(), ref_data.end());
                    std::copy(ref_data.begin(), ref_data.end(), ref_arr);
                    trig_data[vec_count] = nSigma;
    	            vec_count++;
					if (vec_count <=3){
						return;
					}
                    if (!vecFull){
                        Double_t *temp_arr = new Double_t[vec_count];
                        for (int i=0; i<vec_count; i++){
                            temp_arr[i] = trig_data[i];
                        }
						std::sort(temp_arr, temp_arr + vec_count);
						ksTest = TMath::KolmogorovTest(vec_count, temp_arr, ref_data.size(), ref_arr, "M");
						pValue = TMath::KolmogorovTest(vec_count, temp_arr, ref_data.size(), ref_arr, "");
                        Double_t liveMean = TMath::Mean(vec_count, temp_arr);
                        Double_t predMean = TMath::Mean(ref_data.size(), ref_arr);
                        //might want to change this to prevent issue with very small predMean
                        //maybe we could also do a sigma deviation of the mean
                        relativeMean = (liveMean - predMean);
						ks_vec.push_back(ksTest);
						p_vec.push_back(pValue);
						mean_vec.push_back(relativeMean);
                    }else{
						std::sort(trig_data, trig_data + n);
                        ksTest = TMath::KolmogorovTest(n, trig_data, ref_data.size(), ref_arr, "M");
                        pValue = TMath::KolmogorovTest(n, trig_data, ref_data.size(), ref_arr, "");
						ks_vec.push_back(ksTest);
						Double_t liveMean = TMath::Mean(n, trig_data);
                        Double_t predMean = TMath::Mean(ref_data.size(), ref_arr);
                        relativeMean = (liveMean - predMean);
						mean_vec.push_back(relativeMean);
						p_vec.push_back(pValue);
                    }
    	   	        if (vec_count == n) {
		           	    vec_count -= n;
	       	            vecFull = true;
    	   	        } 
    	   	    }
    	    }
            
            /**
             * @brief Prints important trigger values for debugging.
             * 
             * @param stralt alert prefix
             */
            void DisplayDebug (std::string stralt) const {
                if (is_HLT){
                    std::cout << stralt << "HLT PS : " << HLT_PS << std::endl;
                    std::cout << stralt << "L1 PS : " << L1_PS << std::endl;
                    std::cout << stralt << "PS : "     << PS << std::endl;
                    std::cout << stralt << "HLT output : " << rate<< std::endl;
                    std::cout << stralt << "HLT pred : " << pred<< std::endl;
                }else{   // L1 debug info
                    std::cout << stralt << "L1 PBP : " << pred_unpresc<<std::endl;
                    std::cout << stralt << "L1 TBP : " << rate_unpresc<<std::endl;
                }
            }

            /**
             * @brief Determines if trigger is L1 or HLT
             * 
             * @param is_HLT 
             */
            void isHLT(bool is_HLT) {
                this->is_HLT =is_HLT;
            }

        };

        /**
         * @struct Trigger List
         * @brief structer for containing groups of correllated triggers.
         * Used for global offset calculations.
         * 
         */
        struct trigger_group: public ErrorTracker {
            
            Double_t                  avg_ks_val{0.};
            Double_t                  avg_mean_diff{0.};
			Double_t				  avg_p_val{0.};
            std::string               grp_name;
            std::vector<trigger_item *> incl_trigs;
            float                      blankerrorFlag{0.};
            float                      blankSigma{0.};

			//should these be vectors of float or vectors of Double_t? Does it matter?
            Global_Offset                group_stats;

            /**
             * @brief Create a vector of correlated triggers.
             * 
             * @param trig_list 
             */
            void GroupTriggers(std::vector<trigger_item> &trig_list) {
                for (unsigned int i = 0; i < trig_list.size(); i++) {
                    if (trig_list[i].L1_name.find(grp_name) != std::string::npos && trig_list[i].ref_config) {
                        incl_trigs.push_back(&trig_list[i]);
                    }
                }
            }

            /**
             * @brief Set bool for triggers in list
             * 
             * @param index 
             */
            void SetBool(unsigned int index) { 
                for (unsigned int i = 0; i < incl_trigs.size(); i++) {
                    incl_trigs[i]->in_List = true;
                    incl_trigs[i]->list_Index = index;
                }
            }

            /**
             * @brief Calculate average values for stats in triggers
             * 
             */
            void CalcAvg() {
				//I think this may be an issue, since we're referencing a pointer that may change when the vectors change size?
				std::vector<Double_t>        mean_diff_arr;
				std::vector<Double_t>        ks_test_arr;
				std::vector<Double_t>		 p_value_arr;

				int count = 0;
                for (unsigned int i = 0; i<incl_trigs.size(); i++) {
					if(incl_trigs[i]->mean_vec.size()>3 && incl_trigs[i]->ks_vec.size()>3 && incl_trigs[i]->p_vec.size()>3){
						mean_diff_arr.push_back(incl_trigs[i]->mean_vec.back());
						ks_test_arr.push_back(incl_trigs[i]->ks_vec.back());   
						p_value_arr.push_back(incl_trigs[i]->p_vec.back());
						count++;
					}
                }
				Double_t ks_sum = 0.0;
				Double_t mean_sum = 0.0;
				Double_t p_sum = 0.0;
				for(int i=0; i<count; i++){
					ks_sum += ks_test_arr[i];
					mean_sum += mean_diff_arr[i];
					p_sum += p_value_arr[i];
				}
				avg_ks_val = ks_sum / Double_t(incl_trigs.size());
				avg_mean_diff = mean_sum / Double_t(incl_trigs.size());
				avg_p_val = p_sum / Double_t(incl_trigs.size());
                group_stats.MEANDIF = avg_mean_diff;
                group_stats.KS = avg_ks_val;
				group_stats.PVAL = avg_p_val;
            }

        };

        std::vector<trigger_item>  hlttrigs;
        std::vector<trigger_item>  l1trigs;
        std::vector<trigger_item>  extral1trigs;
        std::vector<trigger_ratio> trig_ratios;
        std::vector<trigger_group>  trig_groups;   // for global offset
		std::vector<std::string>   l1names_in_menu;
		std::vector<std::string> hltnames_in_menu;
        
        void updateHLTPrescales(const confadapter_imp::TriggerMenuHelpers::Mapping & hlt_ps);
};
#endif
