#ifndef errortracker_h
#define errortracker_h

#include <ipc/core.h>
#include <ipc/partition.h>

#include "owl/semaphore.h"

#include <is/inforeceiver.h>

#include "TRP/TriggerMenuHelpers.h"
#include "TRP/TimePoint_IS.h"
#include "TRP/L1_Rate.h"
#include "TRP/HLT_Rate.h"
#include "TRP/L1_Rate_Pred.h"
#include "TRP/HLT_Rate_Pred.h"
#include "TRP/Global_Offset.h"

#include "TFormula.h"
#include "TH1F.h"
#include "TFile.h"
#include "TTree.h"
#include "TMath.h"

#include <vector>
#include <string>
#include <numeric>
#include <unordered_map>
#include <cmath>

/**
 * @brief The parent class to all trigger structures that keeps track of deviations of the live rate from predictions.
 * 
 */
class ErrorTracker{
    public:

        ErrorTracker();

        ~ErrorTracker() = default;

        void setParams(std::string typeName, float predSigma, float live, float expected);
        void calculateError();
        void checkError(float lumi);
        void checkOTFError(float lumi);
        void checkError();
        void publishError(int& offlineError, int& onlineError, float& offlineSigma, float& onlineSigma);

    protected:

        std::string m_typeName;
        float m_warningLevel;
        float m_errorLevel;
        float m_live;
        float m_offlineErrorFlag{0};
        float m_onlineErrorFlag{0};
        float m_error;
        float m_expected;
        float m_predSigma;
        int   m_offlinenSigma;
        int   m_onlinenSigma;

};
#endif
