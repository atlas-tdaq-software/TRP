#ifndef HLT_RATE_H
#define HLT_RATE_H

#include <is/info.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * The class responsible for HLT_Rates
 * 
 * @author  produced by the IS generator
 */

class HLT_Rate : public ISInfo {
public:

    /**
     * The rate at which data is passed into the HLT
     */
    float                         Input;

    /**
     * The rate after Prescaling is applied
     */
    float                         AfterPS;

    /**
     * The rate at which data is going out of the HLT
     */
    float                         Output;

    /**
     * The rate at which chains feed the express stream
     */
    float                         Express;


    static const ISType & type() {
	static const ISType type_ = HLT_Rate( ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	ISInfo::print( out );
	out << std::endl;
	out << "Input: " << Input << "\t// The rate at which data is passed into the HLT" << std::endl;
	out << "AfterPS: " << AfterPS << "\t// The rate after Prescaling is applied" << std::endl;
	out << "Output: " << Output << "\t// The rate at which data is going out of the HLT" << std::endl;
	out << "Express: " << Express << "\t// The rate at which chains feed the express stream";
	return out;
    }

    HLT_Rate( )
      : ISInfo( "HLT_Rate" )
    {
	initialize();
    }

    ~HLT_Rate(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    HLT_Rate( const std::string & type )
      : ISInfo( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << Input << AfterPS << Output << Express;
    }

    void refreshGuts( ISistream & in ){
	in >> Input >> AfterPS >> Output >> Express;
    }

private:
    void initialize()
    {
	Input = 0;
	AfterPS = 0;
	Output = 0;
	Express = 0;

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const HLT_Rate & info ) {
    info.print( out );
    return out;
}

#endif // HLT_RATE_H
