#ifndef TRP_ARGS_H
#define TRP_ARGS_H

#include <string>
#include <vector>
#include <algorithm>


/**
 * @brief  Manages command line options
 * @author <a href="mailto:meessen@cppm.in2p3.fr">Christophe Meessen</a> 
 *  
*/
class Args : public std::vector<std::string>
{
  public:
  Args() {;}

  /// The constructor parameters are the command line variables: argc, argv
  Args( const int32_t argc, char* argv[] );
	
  /// Finds the "arg" string in the argument list and 
  /// returns the associated string value 
  std::string getStr( const std::string arg, const std::string dflt="" ) const;
	
  /// Finds the "arg" string in the argument list and 
  /// returns the associated int32_t value 
  int32_t getInt( const std::string arg, const int32_t dflt = 0 ) const;

  /// Checks the presence of a given string in the argument list
  bool hasArg( const std::string arg ) const;

  /// Checks the presence of a given string in the argument list
  /// and exits if not present
  void mustHave( const std::string arg ) const;

  protected:
  typedef std::vector<std::string>::iterator iterator;
  typedef std::vector<std::string>::const_iterator const_iterator;

  /// Returns the iterator to the "arg" string in the argument list 
  const_iterator getArg( const std::string arg ) const;

  /// Returns the iterator of the next argument
  const_iterator getNextArg( const_iterator pos ) const;
};



#endif
