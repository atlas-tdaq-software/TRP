#ifndef L1RATES_H
#define L1RATES_H

#include <is/info.h>

#include <string>
#include <ostream>
#include <vector>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * LVL1 rates monitoring values
 * 
 * @author  generated by the IS tool
 * @version 28/05/09
 */

class L1Rates : public ISInfo {
public:

    /**
     * Label encoding the LVL1 sub-system, and the exact origin of the data. Separate fields should be separated by a dot, that is, period, like this: L1CT.CTPIN.InputCounters or L1Muon.RPC.Sector5.TowerXY
     */
    std::string                   label;

    /**
     * Further description or explanation of the kind of data. Not used for parsing, but will be displayed along with the data.
     */
    std::string                   description;

    /**
     * Labels of incoming rates cable 0.
     */
    std::vector<std::string>      rateLabels0;

    /**
     * Incoming rates in Hz cable 0.
     */
    std::vector<double>           rateValues0;

    /**
     * Labels of incoming rates cable 1.
     */
    std::vector<std::string>      rateLabels1;

    /**
     * Incoming rates in Hz cable 1.
     */
    std::vector<double>           rateValues1;

    /**
     * Labels of incoming rates cable 2.
     */
    std::vector<std::string>      rateLabels2;

    /**
     * Incoming rates in Hz cable 2.
     */
    std::vector<double>           rateValues2;

    /**
     * Labels of incoming rates cable 3.
     */
    std::vector<std::string>      rateLabels3;

    /**
     * Incoming rates in Hz cable 3.
     */
    std::vector<double>           rateValues3;


    static const ISType & type() {
	static const ISType type_ = L1Rates( ).ISInfo::type();
	return type_;
    }

    virtual std::ostream & print( std::ostream & out ) const {
	ISInfo::print( out );
	out << "label: " << label << "	//Label encoding the LVL1 sub-system, and the exact origin of the data. Separate fields should be separated by a dot, that is, period, like this: L1CT.CTPIN.InputCounters or L1Muon.RPC.Sector5.TowerXY" << std::endl;
	out << "description: " << description << "	//Further description or explanation of the kind of data. Not used for parsing, but will be displayed along with the data." << std::endl;
	out << "rateLabels0[" << rateLabels0.size() << "]:	//Labels of incoming rates cable 0." << std::endl;
	for ( size_t i = 0; i < rateLabels0.size(); ++i )
	    out << i << ": " << rateLabels0[i] << std::endl;
	out << "rateValues0[" << rateValues0.size() << "]:	//Incoming rates in Hz cable 0." << std::endl;
	for ( size_t i = 0; i < rateValues0.size(); ++i )
	    out << i << ": " << rateValues0[i] << std::endl;
	out << "rateLabels1[" << rateLabels1.size() << "]:	//Labels of incoming rates cable 1." << std::endl;
	for ( size_t i = 0; i < rateLabels1.size(); ++i )
	    out << i << ": " << rateLabels1[i] << std::endl;
	out << "rateValues1[" << rateValues1.size() << "]:	//Incoming rates in Hz cable 1." << std::endl;
	for ( size_t i = 0; i < rateValues1.size(); ++i )
	    out << i << ": " << rateValues1[i] << std::endl;
	out << "rateLabels2[" << rateLabels2.size() << "]:	//Labels of incoming rates cable 2." << std::endl;
	for ( size_t i = 0; i < rateLabels2.size(); ++i )
	    out << i << ": " << rateLabels2[i] << std::endl;
	out << "rateValues2[" << rateValues2.size() << "]:	//Incoming rates in Hz cable 2." << std::endl;
	for ( size_t i = 0; i < rateValues2.size(); ++i )
	    out << i << ": " << rateValues2[i] << std::endl;
	out << "rateLabels3[" << rateLabels3.size() << "]:	//Labels of incoming rates cable 3." << std::endl;
	for ( size_t i = 0; i < rateLabels3.size(); ++i )
	    out << i << ": " << rateLabels3[i] << std::endl;
	out << "rateValues3[" << rateValues3.size() << "]:	//Incoming rates in Hz cable 3." << std::endl;
	for ( size_t i = 0; i < rateValues3.size(); ++i )
	    out << i << ": " << rateValues3[i] << std::endl;
	return out;
    }

    L1Rates( )
      : ISInfo( "L1Rates" )
    {
	initialize();
    }

    ~L1Rates(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    L1Rates( const std::string & type )
      : ISInfo( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << label << description << rateLabels0 << rateValues0 << rateLabels1 << rateValues1;
	out << rateLabels2 << rateValues2 << rateLabels3 << rateValues3;
    }

    void refreshGuts( ISistream & in ){
	in >> label >> description >> rateLabels0 >> rateValues0 >> rateLabels1 >> rateValues1;
	in >> rateLabels2 >> rateValues2 >> rateLabels3 >> rateValues3;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const L1Rates & info ) {
    info.print( out );
    return out;
}

#endif // L1RATES_H
