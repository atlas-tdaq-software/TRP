#ifndef READXML_H
#define READXML_H

/**
 *  @file
 *  Class "GetConfig" provides the functions to read the XML data.
 *  @version 1.0
 */
#include <xercesc/dom/DOM.hpp>
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/dom/DOMDocumentType.hpp>
#include <xercesc/dom/DOMElement.hpp>
#include <xercesc/dom/DOMImplementation.hpp>
#include <xercesc/dom/DOMImplementationLS.hpp>
#include <xercesc/dom/DOMNodeIterator.hpp>
#include <xercesc/dom/DOMNodeList.hpp>
#include <xercesc/dom/DOMText.hpp>

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/XMLUni.hpp>

#include <vector>
#include <string>
#include <stdexcept>

// Error codes

enum {
  ERROR_ARGS = 1, 
  ERROR_XERCES_INIT,
  ERROR_PARSE,
  ERROR_EMPTY_DOCUMENT
};

class ReadXML{
 public:
  ReadXML();
  ~ReadXML();
  void readConfigFile(std::string&); //  throw(std::runtime_error);
  
  char *getOptionA() { return m_OptionA; };
  char *getOptionB() { return m_OptionB; };
  
  std::string GetValC(std::string my_element_str);
  std::string GetVal(std::string my_element_str);
  
  void SetElement(std::string my_str){m_element=my_str;};
  void SetAttribute(std::vector<std::string> my_element_str){
    m_vec_element=my_element_str;};

  

 private:
  xercesc::XercesDOMParser *m_ConfigFileParser;
  char* m_OptionA;
  char* m_OptionB;
  
  std::string m_element_0; // this IS common
  XMLCh* ch_el_0;
  std::vector<std::string> m_vec_element_0; // e.g partition
  std::vector< XMLCh*> m_vec_xmlch_0; // 
  std::vector<std::string> m_vec_text_0; // eg ATLAS
  
  
  
  std::string m_element; // eg l1adapter
  XMLCh* ch_el;
  std::vector<std::string> m_vec_element; // e.g l1k
  std::vector< XMLCh*> m_vec_xmlch;
  std::vector<std::string> m_vec_text; // eg "'(.*)'.*prescale.*\:(.*)\| TrigType(.*)"
  
  
  // Internal class use only. Hold Xerces data in UTF-16 SMLCh type.    
  XMLCh* TAG_root;
  
};
#endif


