#ifndef _TRP_EXCEPTIONS_H_
#define _TRP_EXCEPTIONS_H_


#include "ers/ers.h"
#include <iostream>

//namespace daq {
 
  /// Generic Issue
  ERS_DECLARE_ISSUE( trp,
		     GenericIssue,
		     "TRP problem",
		     (const char *)message
		     ) 
    
  ERS_DECLARE_ISSUE( trp,
		     Exception,
		     "TRP exception",
		     (const char *)message
		     ) 
    
  ERS_DECLARE_ISSUE( trp,
		     AnyError,
		     "TRP Any",
		     (const char *)message
		     )
		   
    // Qui ne puoi aggiungere altri
    
  //} // daq namespace

  //#ifndef ERS_DEBUG
  //#define ERS_DEBUG( L, MESSAGE ) ERS_DEBUG( L, MESSAGE )
  //#endif
			  
#ifndef ERS_WARNING
#define ERS_WARNING( ISSUE, MESSAGE )     \
{                                         \
  std::ostringstream out;                 \
  out << MESSAGE;                         \
  ISSUE i( ERS_HERE, out.str().c_str() ); \
  ers::warning(i);                        \
}
#endif

#endif // _TRP_EXCEPTIONS_H_
