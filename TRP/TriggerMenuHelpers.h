#ifndef TRP_TriggerMenuHelpers_h
#define TRP_TriggerMenuHelpers_h

#include <vector>
#include <map>
#include <string>
#include "TRP/Branch.h"
#include "TRP/Tree.h"

#include "boost/property_tree/ptree.hpp"

ERS_DECLARE_ISSUE( confadapter_imp,
                   TriggerMenuHelpersIssue,
                   ERS_EMPTY, ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE( confadapter_imp,
                        DBOpenIssue,
                        confadapter_imp::TriggerMenuHelpersIssue,
                        "Can't open trigger database " << connection << ". Reason: " << reason,
                        ERS_EMPTY,
                        ((const char *) connection) ((const char *) reason )
                        )

ERS_DECLARE_ISSUE_BASE( confadapter_imp,
                        KeyNotExistIssue,
                        confadapter_imp::TriggerMenuHelpersIssue,
                        "Trigger key " << key << " does not exist in table " << table,
                        ERS_EMPTY,
                        ((int) key) ((const char *) table )
                        )

ERS_DECLARE_ISSUE_BASE( confadapter_imp,
                        DBQueryIssue,
                        confadapter_imp::TriggerMenuHelpersIssue,
                        "Querying TriggerDB failed. Reason: " << reason,
                        ERS_EMPTY,
                        ((const char *) reason )
                        )

ERS_DECLARE_ISSUE_BASE( confadapter_imp,
                        JsonParsingIssue,
                        confadapter_imp::TriggerMenuHelpersIssue,
                        "Parsing the json content of key " << key << " in table " << table << " failed. Reason: " << reason,
                        ERS_EMPTY,
                        ((int) key) ((const char *) table ) ((const char *) reason )
                        )

ERS_DECLARE_ISSUE_BASE( confadapter_imp,
                        MenuLoadIssue,
                        confadapter_imp::TriggerMenuHelpersIssue,
                        "Can't parse the " << level << " trigger menu. Reason: " << reason,
                        ERS_EMPTY,
                        ((const char *) level) ((const char *) reason )
                        )

ERS_DECLARE_ISSUE_BASE( confadapter_imp,
                        MapAccessIssue,
                        confadapter_imp::TriggerMenuHelpersIssue,
                        "Map '" << mapName << "' does not exist.",
                        ERS_EMPTY,
                        ((const char *) mapName)
                        )

ERS_DECLARE_ISSUE_BASE( confadapter_imp,
                        MultiMapAccessIssue,
                        confadapter_imp::TriggerMenuHelpersIssue,
                        "MultiMap '" << mapName << "' does not exist.",
                        ERS_EMPTY,
                        ((const char *) mapName)
                        )

ERS_DECLARE_ISSUE_BASE( confadapter_imp,
                        StreamPSMapAccessIssue,
                        confadapter_imp::TriggerMenuHelpersIssue,
                        "Stream prescale map '" << mapName << "' does not exist.",
                        ERS_EMPTY,
                        ((const char *) mapName)
                        )

class TriggerConfigAccessImpl;

namespace confadapter_imp {

   class TriggerMenuHelpers {
   public:

      typedef std::map<std::string, std::vector<std::string> > Mapping;
      typedef std::multimap<std::string, std::string> MultiMapping;
      typedef std::map<std::string, float> MapStrInt;

      /**
       *  constructors
       */
      // this constructor take the partition name from the environment var TDAQ_PARTITION
      TriggerMenuHelpers(const std::string & dbAlias);
      TriggerMenuHelpers(const std::string & partitionName, const std::string & dbAlias);

      ~TriggerMenuHelpers();

      /**
       * retrievers and accessors of the configuration information
       */
      bool getConfiguration(unsigned int smk, unsigned int hltk, unsigned int l1k);

      bool getConfigurationRun2(unsigned int smk, unsigned int hltk, unsigned int l1k);

      /** LHC Run, returns 3 */
      unsigned int lhcRun() const;

      /**
       *  Access to maps
       */
      /** Get Mapping for given name */
      [[deprecated("Replaced by accessor function getMapping(name)")]]
      bool getMapping(Mapping& m, const std::string& name) const;

      /**
       * returns reference to map
       * throws std::out_of_range exception if map name doesn't exist
       */
      const Mapping & getMapping(const std::string& name) const; 

      [[deprecated("Replaced by accessor function getMultiMapping(name)")]]
      bool getMultiMapping(MultiMapping& m, const std::string& name) const;

      const MultiMapping & getMultiMapping(const std::string& name) const;

      [[deprecated("Replaced by accessor function getStreamPrescalesMap(stream)")]]
      bool getStreamPrescalesMap(MapStrInt &streamPrescales, const std::string& stream ) const;

      const TriggerMenuHelpers::MapStrInt & getStreamPrescalesMap(const std::string& stream ) const;

      /**
       * functions to convert maps into TRP Branches and TRP Trees
       */
      void Mapping2Branch( const MultiMapping & m, std::vector<Branch>& vecBranch) const;

      void Mapping2Tree( const MultiMapping & m, const MultiMapping & m2, std::vector<Tree> & vecTree) const;

      /**
       * functions to publish information 
       */
      void PublishBranch( const std::string & server_info, const std::vector<Branch> & my_branch);

      void PublishTree( const std::string & server_info, const std::vector<Tree> & my_tree);

      void EraseInfo( const std::string & server_info, const std::string & rm_string);

      /**
       * various print functions
       */
      void printPrescaleMaps(bool size_only = false) const;
    
      void printMapping( const Mapping& m, const std::string& prefix = " ", bool size_only = false) const;

      void printMultiMapping( const MultiMapping& m, const std::string& prefix = " ", bool size_only = false) const;

      /**
       * for testing
       */
      void printAllMaps(bool size_only = false) const;

   private:
      std::unique_ptr<TriggerConfigAccessImpl> m_config;
   };

} // end of namespace confadapter_imp


#endif 


