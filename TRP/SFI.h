#ifndef SFI_H
#define SFI_H

#include <is/info.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>

namespace DF_IS_Info
{
class SFI : public ISInfo {
public:

    unsigned int                  EventsAssigned;

    unsigned int                  EventsCreated;

    unsigned int                  EventsBuilt;

    unsigned int                  EventsIncompletelyBuilt;

    unsigned int                  EventsDeleted;

    double                        ActualEoERate;

    double                        RunAverageEoERate;

    double                        ActualDeletedEventRate;

    double                        RunAverageDeletedEventRate;

    unsigned int                  NumBusyCount;

    unsigned int                  NumNonBusyMessages;

    unsigned int                  NumRequests;

    double                        ActualRequestRate;

    unsigned int                  NumReasks;

    unsigned int                  NumTimeouts;

    unsigned int                  NumFragments;

    unsigned int                  NumFragmentsNotInserted;

    unsigned int                  EventDuplicWarnings;

    unsigned int                  NumMissingL2Results;

    unsigned int                  NumBCIDErrors;

    double                        Payload;

    double                        ActualPayloadRate;

    double                        RunAveragePayloadRate;

    double                        FragmentPayload;

    double                        EventPayload;

    unsigned int                  EventsCheckedForMonitoring;

    unsigned int                  MonitoredEventCount;

    double                        ActualMonitoredEventRate;

    unsigned int                  Number_of_disabled_ROSs;


    static const ISType & type() {
        static const ISType type_ = SFI( ).ISInfo::type();
        return type_;
    }

    virtual std::ostream & print( std::ostream & out ) const {
        ISInfo::print( out );
        out << "EventsAssigned: " << EventsAssigned << "        //Number of assigned events by the DFM." << std::endl;
        out << "EventsCreated: " << EventsCreated << "  //Number of created events." << std::endl;
        out << "EventsBuilt: " << EventsBuilt << "      //Number of events built." << std::endl;
        out << "EventsIncompletelyBuilt: " << EventsIncompletelyBuilt << "      //Number of events built with missing ROS fragments. DbgLvl=1" << std::endl;
        out << "EventsDeleted: " << EventsDeleted << "  //Number of events deleted (=done with output). DbgLvl=1" << std::endl;
        out << "ActualEoERate: " << ActualEoERate << "  //Actual End-of-Event rate (built)." << std::endl;
        out << "RunAverageEoERate: " << RunAverageEoERate << "  //Run-Average End-of-Event rate (built). DbgLvl=1" << std::endl;
        out << "ActualDeletedEventRate: " << ActualDeletedEventRate << "        //Actual Deleted-Event rate (done with output)." << std::endl;
        out << "RunAverageDeletedEventRate: " << RunAverageDeletedEventRate << "        //Run-Average Deleted-Event rate (done with output). DbgLvl=1" << std::endl;
        out << "NumBusyCount: " << NumBusyCount << "    //Number of Busy messages DbgLvl=1" << std::endl;
        out << "NumNonBusyMessages: " << NumNonBusyMessages << "        //Number of NonBusy messages DbgLvl=1" << std::endl;
        out << "NumRequests: " << NumRequests << "      //Number of requested fragments DbgLvl=1" << std::endl;
        out << "ActualRequestRate: " << ActualRequestRate << "  //Rate of requests sent to the ROSs DbgLvl=1" << std::endl;
        out << "NumReasks: " << NumReasks << "  //Number of reasked fragments DbgLvl=1" << std::endl;
        out << "NumTimeouts: " << NumTimeouts << "      //Number of Timeouts DbgLvl=1" << std::endl;
        out << "NumFragments: " << NumFragments << "    //Number of Event Fragments received DbgLvl=1" << std::endl;
        out << "NumFragmentsNotInserted: " << NumFragmentsNotInserted << "      //Number of Event Fragments that could not be inserted (duplication) DbgLvl=1" << std::endl;
        out << "EventDuplicWarnings: " << EventDuplicWarnings << "      //Number of possible duplicated events due to EFIO failures" << std::endl;
        out << "NumMissingL2Results: " << NumMissingL2Results << "      //Number of events for which the L2Result Handler node was not specified" << std::endl;
        out << "NumBCIDErrors: " << NumBCIDErrors << "  //Number of events for the BCID check failed" << std::endl;
        out << "Payload: " << Payload << "      //Total Payload received (MB) DbgLvl=1" << std::endl;
        out << "ActualPayloadRate: " << ActualPayloadRate << "  //Actual payload rate (MB/s) DbgLvl=1" << std::endl;
        out << "RunAveragePayloadRate: " << RunAveragePayloadRate << "  //Run-Average payload rate (MB/s) DbgLvl=1" << std::endl;
        out << "FragmentPayload: " << FragmentPayload << "      //Average Payload per fragment (Bytes) DbgLvl=1" << std::endl;
        out << "EventPayload: " << EventPayload << "    //Average Payload per event (kB) DbgLvl=1" << std::endl;
        out << "EventsCheckedForMonitoring: " << EventsCheckedForMonitoring << "        //Events retrieved from Sampler queue and checked for Monitoring criteria" << std::endl;
        out << "MonitoredEventCount: " << MonitoredEventCount << "      //Events Given to Monitoring Tasks" << std::endl;
        out << "ActualMonitoredEventRate: " << ActualMonitoredEventRate << "    //Actual Event Rate to Monitoring Tasks" << std::endl;
        out << "Number_of_disabled_ROSs: " << Number_of_disabled_ROSs << "      //Number of disabled ROSs" << std::endl;
        return out;
    }

    SFI( )
      : ISInfo( "SFI" )
    {
        initialize();
    }

    ~SFI(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    SFI( const std::string & type )
      : ISInfo( type )
    {
        initialize();
    }

    void publishGuts( ISostream & out ){
        out << EventsAssigned << EventsCreated << EventsBuilt << EventsIncompletelyBuilt;
        out << EventsDeleted << ActualEoERate << RunAverageEoERate << ActualDeletedEventRate;
        out << RunAverageDeletedEventRate << NumBusyCount << NumNonBusyMessages << NumRequests;
        out << ActualRequestRate << NumReasks << NumTimeouts << NumFragments << NumFragmentsNotInserted;
        out << EventDuplicWarnings << NumMissingL2Results << NumBCIDErrors << Payload << ActualPayloadRate;
        out << RunAveragePayloadRate << FragmentPayload << EventPayload << EventsCheckedForMonitoring;
        out << MonitoredEventCount << ActualMonitoredEventRate << Number_of_disabled_ROSs;
    }

    void refreshGuts( ISistream & in ){
        in >> EventsAssigned >> EventsCreated >> EventsBuilt >> EventsIncompletelyBuilt >> EventsDeleted;
        in >> ActualEoERate >> RunAverageEoERate >> ActualDeletedEventRate >> RunAverageDeletedEventRate;
        in >> NumBusyCount >> NumNonBusyMessages >> NumRequests >> ActualRequestRate >> NumReasks;
        in >> NumTimeouts >> NumFragments >> NumFragmentsNotInserted >> EventDuplicWarnings;
        in >> NumMissingL2Results >> NumBCIDErrors >> Payload >> ActualPayloadRate >> RunAveragePayloadRate;
        in >> FragmentPayload >> EventPayload >> EventsCheckedForMonitoring >> MonitoredEventCount;
        in >> ActualMonitoredEventRate >> Number_of_disabled_ROSs;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const SFI & info ) {
    info.print( out );
    return out;
}

}

#endif // SFI_H
