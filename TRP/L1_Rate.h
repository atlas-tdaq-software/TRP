#ifndef L1_RATE_H
#define L1_RATE_H

#include <is/info.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * The class responsible for HLT_Rates
 * 
 * @author  produced by the IS generator
 */

class L1_Rate : public ISInfo {
public:

    /**
     * Trigger rate before prescale [Hz]
     */
    float                         TBP;

    /**
     * Trigger rate after prescale [Hz]
     */
    float                         TAP;

    /**
     * Trigger rate after veto [Hz]
     */
    float                         TAV;

    /**
     * Effective prescale
     */
    float                         PS;

    /**
     * deadtime: 1-(TAV/TAP)
     */
    float                         DT;

    /**
     */
    bool                          enabled;


    static const ISType & type() {
	static const ISType type_ = L1_Rate( ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	ISInfo::print( out );
	out << std::endl;
	out << "TBP: " << TBP << "\t// Trigger rate before prescale [Hz]" << std::endl;
	out << "TAP: " << TAP << "\t// Trigger rate after prescale [Hz]" << std::endl;
	out << "TAV: " << TAV << "\t// Trigger rate after veto [Hz]" << std::endl;
	out << "PS: " << PS << "\t// Effective prescale" << std::endl;
	out << "DT: " << DT << "\t// deadtime: 1-(TAV/TAP)" << std::endl;
	out << "enabled: " << enabled << "\t// ";
	return out;
    }

    L1_Rate( )
      : ISInfo( "L1_Rate" )
    {
	initialize();
    }

    ~L1_Rate(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    L1_Rate( const std::string & type )
      : ISInfo( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << TBP << TAP << TAV << PS << DT << enabled;
    }

    void refreshGuts( ISistream & in ){
	in >> TBP >> TAP >> TAV >> PS >> DT >> enabled;
    }

private:
    void initialize()
    {
	TBP = 0;
	TAP = 0;
	TAV = 0;
	PS = 0;
	DT = 0;
	enabled = 1;

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const L1_Rate & info ) {
    info.print( out );
    return out;
}

#endif // L1_RATE_H
