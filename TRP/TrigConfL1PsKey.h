#ifndef TRIGCONFL1PSKEY_H
#define TRIGCONFL1PSKEY_H

#include <is/info.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * Current L1 prescale key
 * 
 * @author  generated by the IS tool
 * @version 26/04/09
 */

class TrigConfL1PsKey : public ISInfo {
public:

    /**
     * L1 prescale key of the trigger configuration
     */
    unsigned int                  L1PrescaleKey;

    /**
     * L1 prescale comment
     */
    std::string                   L1PrescaleComment;


    static const ISType & type() {
	static const ISType type_ = TrigConfL1PsKey( ).ISInfo::type();
	return type_;
    }

    virtual std::ostream & print( std::ostream & out ) const {
	ISInfo::print( out );
	out << "L1PrescaleKey: " << L1PrescaleKey << "	//L1 prescale key of the trigger configuration" << std::endl;
	out << "L1PrescaleComment: " << L1PrescaleComment << "	//L1 prescale comment" << std::endl;
	return out;
    }

    TrigConfL1PsKey( )
      : ISInfo( "TrigConfL1PsKey" )
    {
	initialize();
    }

    ~TrigConfL1PsKey(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    TrigConfL1PsKey( const std::string & type )
      : ISInfo( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << L1PrescaleKey << L1PrescaleComment;
    }

    void refreshGuts( ISistream & in ){
	in >> L1PrescaleKey >> L1PrescaleComment;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const TrigConfL1PsKey & info ) {
    info.print( out );
    return out;
}

#endif // TRIGCONFL1PSKEY_H
