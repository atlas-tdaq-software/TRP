#ifndef L2SV_H
#define L2SV_H

#include <is/info.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * 
 * @author  generated by the IS tool
 * @version 23/06/08
 */

class L2SV : public ISInfo {
public:

    /**
     * Identity of this node
     */
    std::string                   Identity;

    /**
     * Number of errors
     */
    unsigned int                  errors;

    /**
     * Number of disabled L2PUs
     */
    int                           disableCnt;

    /**
     * DFM XOFF asserted
     */
    unsigned int                  DFM_XOFF;

    /**
     * Total time DFM XOFF was asserted(s)
     */
    double                        accumulatedXoffTime;

    /**
     * Number of LVL1 events
     */
    uint64_t                      LVL1_events;

    /**
     * Number of LVL2 events
     */
    uint64_t                      LVL2_events;

    /**
     * Number of accepted events
     */
    uint64_t                      AcceptedEvents;

    /**
     * Number of rejected events
     */
    uint64_t                      RejectedEvents;

    /**
     * Number of forced accepts
     */
    unsigned int                  ForcedAccepts;

    /**
     * Interval length(s)
     */
    double                        IntervalTime;

    /**
     * Interval Throughput (Hz)
     */
    double                        IntervalEventRate;

    /**
     * Average Throughput (Hz)
     */
    double                        AvgEventRate;


    static const ISType & type() {
	static const ISType type_ = L2SV( ).ISInfo::type();
	return type_;
    }

    virtual std::ostream & print( std::ostream & out ) const {
	ISInfo::print( out );
	out << "Identity: " << Identity << "	//Identity of this node" << std::endl;
	out << "errors: " << errors << "	//Number of errors" << std::endl;
	out << "disableCnt: " << disableCnt << "	//Number of disabled L2PUs" << std::endl;
	out << "DFM_XOFF: " << DFM_XOFF << "	//DFM XOFF asserted" << std::endl;
	out << "accumulatedXoffTime: " << accumulatedXoffTime << "	//Total time DFM XOFF was asserted(s)" << std::endl;
	out << "LVL1_events: " << LVL1_events << "	//Number of LVL1 events" << std::endl;
	out << "LVL2_events: " << LVL2_events << "	//Number of LVL2 events" << std::endl;
	out << "AcceptedEvents: " << AcceptedEvents << "	//Number of accepted events" << std::endl;
	out << "RejectedEvents: " << RejectedEvents << "	//Number of rejected events" << std::endl;
	out << "ForcedAccepts: " << ForcedAccepts << "	//Number of forced accepts" << std::endl;
	out << "IntervalTime: " << IntervalTime << "	//Interval length(s)" << std::endl;
	out << "IntervalEventRate: " << IntervalEventRate << "	//Interval Throughput (Hz)" << std::endl;
	out << "AvgEventRate: " << AvgEventRate << "	//Average Throughput (Hz)" << std::endl;
	return out;
    }

    L2SV( )
      : ISInfo( "L2SV" )
    {
	initialize();
    }

    ~L2SV(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    L2SV( const std::string & type )
      : ISInfo( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << Identity << errors << disableCnt << DFM_XOFF << accumulatedXoffTime  << LVL1_events; 
    out << LVL2_events << AcceptedEvents << RejectedEvents << ForcedAccepts << IntervalTime; 
    out << IntervalEventRate << AvgEventRate;
    }

    void refreshGuts( ISistream & in ){
	in >> Identity >> errors >> disableCnt >> DFM_XOFF >> accumulatedXoffTime >> LVL1_events;
    in >> LVL2_events >> AcceptedEvents >> RejectedEvents >> ForcedAccepts >> IntervalTime;
    in >> IntervalEventRate >> AvgEventRate;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const L2SV & info ) {
    info.print( out );
    return out;
}

class L2SV_compact {
public:
	L2SV_compact() {
		errors = 0;
		IntervalEventRate = 0;
	}

	L2SV_compact(const L2SV& l) {
		errors = l.errors;
		IntervalEventRate = l.IntervalEventRate;
	}

	//    std::string                   Identity;
    unsigned int                  errors;
	//    int                           disableCnt;
	//    uint64_t                      LVL1_events;
	//    uint64_t                      LVL2_events;
	//    uint64_t                      AcceptedEvents;
	//    uint64_t                      RejectedEvents;
	//    unsigned int                  ForcedAccepts;
	//    double                        IntervalTime;
    double                        IntervalEventRate;
	//    double                        AvgEventRate;
};


#endif // L2SV_H
