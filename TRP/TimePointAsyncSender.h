#ifndef TRP_TimePointAsyncSender_h
#define TRP_TimePointAsyncSender_h

#include <deque>
#include <boost/noncopyable.hpp>
#include <boost/thread/condition.hpp>
#include <boost/thread/mutex.hpp>

class TimePoint_IS;
class ISInfoDictionary;

class TimePointAsyncSender : public boost::noncopyable {
public:
  TimePointAsyncSender(ISInfoDictionary* is, const std::string& publishname, bool fate = DeleteAfterSend);
  ~TimePointAsyncSender();
  
  /**
   * @brief this is to starting point of the thread
   **/
  void run();

  enum {
    KeepAfterSend = false,
    DeleteAfterSend = true
  };

  /**
   * @brief schedule for publishing the time point
   * @param p time point
   * @param dispose flag deciding if obj "p" is deleted after succesful commit   
   **/
  void send(const TimePoint_IS* p, const std::string& name = "" );
  
  /**
   * @brief informs about finished publication
   * @param p time point
   * @return true if object is already sent, false otherwise
   **/
  bool is_send(const TimePoint_IS* p);

  
private:
  ISInfoDictionary* m_is; 
  std::string m_default_name; //!< name under which object shoudl be sent
  bool m_fate;  //!< configuration flag, if set DeleteAfterSend objects are deleted after succesful send
  


  boost::thread *m_thread;
  bool m_keep_running;  //!< flag to stop thread 
  bool m_in_error;      //!< flag set to true if send failed, set back to false when objects can be safely send again 
  void wait_for_data(); //!< waits until there is new object to be sent
  boost::mutex     m_newdata_mutex;  
  boost::condition m_newdata_condition;
  
  class ObjToSend {
  public:
    ObjToSend(const TimePoint_IS* o, const std::string& n) 
      : object(o), name(n) {}
    bool operator==(const TimePoint_IS* p ) {
      return p == this->object;
    }
    bool operator==(const ObjToSend& p ) {
      return p.object == object && p.name == name;
    }

    const TimePoint_IS* object;
    std::string   name;
  };
  const static ObjToSend s_nothing;

  // queue and it's guards and utils
  std::deque<ObjToSend> m_queue;     //!< queue objects awaiting send
  boost::mutex m_insert_mutex;       //!< mutex to guard insertion/removal from the queue

  void insert(const ObjToSend& obj); //!< inserts object into the queue 
  const ObjToSend& retrieve();       //!< pulls next object to be sent
  void drop();                       //!< drops one timepoint from pool of objects to send
  void update(TimePoint_IS* p);      //!< updates various metadata in the TimePoint
};

#endif // TRP_TimePointAsyncSender
