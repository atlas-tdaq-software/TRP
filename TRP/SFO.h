#ifndef SFO_H
#define SFO_H

#include <is/info.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>

namespace DF_IS_Info
{
class SFO : public ISInfo {
public:

    unsigned int                  EventsReceived;

    unsigned int                  EventsSaved;

    double                        DataVolumeReceived;

    double                        DataVolumeSaved;

    double                        CurrentEventReceivedRate;

    double                        CurrentDataReceivedRate;

    double                        CurrentEventSavedRate;

    double                        CurrentDataSavedRate;

    double                        SamplingTime;


    static const ISType & type() {
        static const ISType type_ = SFO( ).ISInfo::type();
        return type_;
    }

    virtual std::ostream & print( std::ostream & out ) const {
        ISInfo::print( out );
        out << "EventsReceived: " << EventsReceived << "        //Number of events received from all EFDs." << std::endl;
        out << "EventsSaved: " << EventsSaved << "      //Number of events saved to disk." << std::endl;
        out << "DataVolumeReceived: " << DataVolumeReceived << "        //Data volume received from all EFDs (MB)." << std::endl;
        out << "DataVolumeSaved: " << DataVolumeSaved << "      //Data volume saved to disk (MB)." << std::endl;
        out << "CurrentEventReceivedRate: " << CurrentEventReceivedRate << "    //Current rate of received events (events/s). DbgLvl=1" << std::endl;
        out << "CurrentDataReceivedRate: " << CurrentDataReceivedRate << "      //Current rate of received data (MB/s). DbgLvl=1" << std::endl;
        out << "CurrentEventSavedRate: " << CurrentEventSavedRate << "  //Current rate of saved events (events/s). DbgLvl=1" << std::endl;
        out << "CurrentDataSavedRate: " << CurrentDataSavedRate << "    //Current rate of saved data (MB/s). DbgLvl=1" << std::endl;
        out << "SamplingTime: " << SamplingTime << "    //Time interval used to calculate the rates (s). DbgLvl=1" << std::endl;
        return out;
    }

    SFO( )
      : ISInfo( "SFO" )
    {
        initialize();
    }

    ~SFO(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    SFO( const std::string & type )
      : ISInfo( type )
    {
        initialize();
    }

    void publishGuts( ISostream & out ){
        out << EventsReceived << EventsSaved << DataVolumeReceived << DataVolumeSaved << CurrentEventReceivedRate;
        out << CurrentDataReceivedRate << CurrentEventSavedRate << CurrentDataSavedRate << SamplingTime;
    }

    void refreshGuts( ISistream & in ){
        in >> EventsReceived >> EventsSaved >> DataVolumeReceived >> DataVolumeSaved >> CurrentEventReceivedRate;
        in >> CurrentDataReceivedRate >> CurrentEventSavedRate >> CurrentDataSavedRate >> SamplingTime;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const SFO & info ) {
    info.print( out );
    return out;
}

}

#endif // SFO_H

