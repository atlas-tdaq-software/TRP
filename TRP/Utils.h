#ifndef UTILS_h
#define UTILS_h

#include <ctime>
#include <string>
#include <vector>
#include <owl/time.h>
#include <rc/RunParams.h>
//#include "rc/LuminosityInfo.h"
#include "TTCInfo/LumiBlock.h"
#include "TRP/TrigConfKeys.h"

#include <ipc/core.h>
#include <ipc/partition.h>
#include <is/infodictionary.h>
#include "TRP/TrigConfSmKey.h"
#include "TRP/TrigConfL1PsKey.h"
#include "TRP/TrigConfHltPsKey.h"
#include "TRP/TrigConfL1BgKey.h"
#include "TRP/TrigConfReleaseNamed.h"
//#include "TRP/TrigConfRelease.h"
#include "TRP/ISCTPCOREPSKey.h"
#include "TRP/OCLumi.h"

#include "TRP/TimePoint_IS.h"
#include <time.h>

namespace trp_utils {

  bool NeedToPublish(TimePoint_IS new_TP_IS, TimePoint_IS old_TP_IS,
		     float m_variation, float m_time, 
		     std::vector<int> items_to_check);
  double convertToDouble(const std::string& s);
  int convertToInt(const std::string& s);
  
  std::vector<std::string> split(const std::string& line, const std::string& del);

  void tokenize(const std::string& str,
		std::vector<std::string>& tokens,
		const std::string& delimiters = " ");

  void tokenize(const std::string& str,
		std::vector<int>& tokens,
		const std::string& delimiters = " ");

  void tokenize(const std::string& str,
		std::vector<int>& tokens,
		const std::string& delimiters1,
		const std::string& delimiters2 );

  
  template <class T>
    inline std::string  convertToString2(const T& t)
    {
      std::stringstream ss;
      ss << t;
      return ss.str();
    }
  

  std::string convertToString(float x);
  std::string convertToString(int x);
  void convertToString(int x, std::string &out_s);
  
  bool check_suffix(const std::string& my_string, const std::string& suff );

  std::string remove_char(const std::string& my_string, const std::string& suff );
  
  void GetTime(OWLTime &o_time);
  void GetRunParams(std::string pPartitionName, unsigned int & run, unsigned short & lb  );
  
  // Get trigger keys from IS (RunParams server)
  void GetTriggerKeys(std::string pPartitionName, unsigned int & smk, unsigned int & hltk, unsigned int & l1k);
  void GetTriggerKeys(std::string pPartitionName, unsigned int & smk, unsigned int & hltk, unsigned int & l1k, unsigned int & bgk );

  void GetTriggerRelease(std::string pPartitionName, std::string& triggerRelease);

  void GetLuminosityInfo(std::string pPartitionName,
                     double & var1, double & var2, double & var3, unsigned int & var4, unsigned int & var5 );

  void Add(TimePoint_IS & my_temp, TimePoint_IS tp);
  void Scale(TimePoint_IS & my_temp, float scale);
  TimePoint_IS Add(TimePoint_IS my_t1, float scale1, TimePoint_IS my_t2, float scale2);
  void CopyAxis(TimePoint_IS & my_temp, TimePoint_IS tp);
  time_t OWL2Time(OWLTime ow_time);
  void TotalSum(TimePoint_IS &m_mu_point);
  
  
} // end of namespace


#endif // UTILS_h
