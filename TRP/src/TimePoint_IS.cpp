#include <algorithm>
#include <boost/lexical_cast.hpp>

#include "../TimePoint_IS.h"

/* ----------------------------------------------- */
/* Utility functions                               */
/* ----------------------------------------------- */


void TimePoint_IS::Scale(float sf){
  int it_id = 0;
  for(std::vector<float>::iterator it=Data.begin();it != Data.end();++it,++it_id){
    Data[it_id] = Data[it_id]*sf;
  }
}

void TimePoint_IS::SetXAxis(const std::vector<std::string> &x){
  XLabels = x;
}

void TimePoint_IS::SetYAxis(const std::vector<std::string> &y){
  YLabels = y;
}

int TimePoint_IS::NumCol(){
  return XLabels.size();
}

int TimePoint_IS::NumRow(){
  return YLabels.size();
}

void TimePoint_IS::EraseCont(){
  XLabels.clear();
  YLabels.clear();
  Data.clear();
  MetaData.clear();
}

void TimePoint_IS::EraseData(){
  Data.clear();
}

std::ostream & TimePoint_IS::dump( std::ostream & out ) const {
  ISInfo::print( out );
  out << "RunNumber: " << RunNumber << "	//Run Number" << std::endl;
  out << "TimeStamp: " << TimeStamp << "	//TimeStamp" << std::endl;
  out << "LumiBlock: " << LumiBlock << "	//Luminosity Block number" << std::endl;
  out << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>" << std::endl;
  out << "XLabels[" << 0 << " ]: "  << XLabels[0] << std::endl;
  out << "XLabels[" <<  XLabels.size() << " ]: "  << XLabels[XLabels.size()-1] << std::endl;
  out << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>" << std::endl;
  out << "YLabels[" << 0 << " ]: "  << YLabels[0] << std::endl;
  out << "YLabels[" <<  YLabels.size() << " ]: "  << YLabels[YLabels.size()-1] << std::endl;
  out << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>" << std::endl;
  out << "Data[" << 0 << "]: " << Data[0] << std::endl;
  out << "Data[" << XLabels.size()-1 << "]: " << Data[XLabels.size()-1] << std::endl;
  out << "Data[" << XLabels.size()   << "]: " << Data[XLabels.size()] << std::endl;
  out << "Data[" << XLabels.size()+(XLabels.size()-1)   << "]: " 
      << Data[XLabels.size()+(XLabels.size()-1)] << std::endl;
  out << "Data[" << (YLabels.size()-1)*XLabels.size()   << "]: " 
      << Data[(YLabels.size()-1)*XLabels.size()] << std::endl;
  out << "Data[" <<  (YLabels.size()-1)*XLabels.size()+(XLabels.size()-1)   << "]: " 
      << Data[(YLabels.size()-1)*XLabels.size()+(XLabels.size()-1)] << std::endl;
  out << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>" << std::endl;
  if(MetaData.size() > 0) {
    out << "MetaData[" << MetaData.size() << "]:	//metadata info" << std::endl;
    out << "MetaData[0]:"  << ": " << MetaData[0] << std::endl;
    out << "MetaData[" <<   MetaData.size()-1 << "  ]:"  << ": " << MetaData[ MetaData.size()-1] << std::endl;
  } else
    out << "No MetaData" << std::endl;

  return out;
}
    

/* ----------------------------------------------- */
/* Functions to use with "set" and "get"           */
/* ----------------------------------------------- */

void TimePoint_IS::clear() {
  std::fill(Data.begin(), Data.end(), 0);
}


void TimePoint_IS::format(const std::vector<std::string> &x, 
                          const std::vector<std::string> &y) {
  XLabels = x;
  YLabels = y;
  Data.resize(x.size()* y.size());
  clear();
  m_IsFreshData = true;
}



bool TimePoint_IS::index(const std::string& x, const std::string& y, unsigned int& ret) const {
  LUT::const_iterator i = m_XLabelsMap.find(x);
  if ( i == m_XLabelsMap.end() ) 
    return false;
  unsigned xindex = i->second;

  i = m_YLabelsMap.find(y);
  if ( i == m_YLabelsMap.end() ) 
    return false;
  unsigned yindex = i->second;
  return index(xindex, yindex, ret);
}

bool TimePoint_IS::index(unsigned int x, unsigned int y, unsigned int& ret) const {
  ret = 0;
  if ( x < XLabels.size() && y < YLabels.size()) {
    ret = x*YLabels.size() + y;
    return true;
  }
  return false;
}

bool TimePoint_IS::get_xy(unsigned int id, std::string &x, std::string &y) const {
  if(id>= Data.size()) return false;
  
  int iy = id%YLabels.size();
  int ix = (id-iy)/YLabels.size();
  x = XLabels[ix];
  y = YLabels[iy];
  return true;
}

void setOneAxisMap(std::map<std::string, int >& lut, const std::vector<std::string>& labels) {
  lut.clear();
  unsigned size = labels.size();
  for ( unsigned i = 0; i < size ; ++i ) {
    std::string label = labels[i];   
    lut[label] = i;
  }
}

void TimePoint_IS::updateMap() const {
  if ( m_IsFreshData ) {
    m_IsFreshData = false;
    setOneAxisMap(m_XLabelsMap, XLabels);
    setOneAxisMap(m_YLabelsMap, YLabels);
  }
}

bool TimePoint_IS::get(const std::string &x, const std::string &y, float &value) const {
  updateMap();
  unsigned int idx;
  if ( index(x,y,idx) ) {
    value = Data[idx];
    return true;
  } 
  return false;
}

bool TimePoint_IS::set(const std::string &x, const std::string &y, float value) {
  updateMap();
  unsigned int idx;
  if ( index(x,y, idx) ) {
    Data[idx] = value;
    return true;
  }
  return false;
}

bool TimePoint_IS::get(unsigned int x, unsigned int y, float &value) const {
  unsigned int idx;
  if ( index(x,y,idx) ) {
    value = Data[idx];
    return true;
  } 
  return false;
}

bool TimePoint_IS::set(unsigned int x, unsigned int y, float value) {
  unsigned int idx;
  if ( index(x,y, idx) ) {
    Data[idx] = value;
    return true;
  }
  return false;
}


void TimePoint_IS::print_as_table(std::ostream & out) const {
  ISInfo::print(out);
  out << "RunNumber: " << RunNumber << " TimeStamp: " << TimeStamp << " LumiBlock: " << LumiBlock << std::endl;
  out << std::endl << " Axes: Y >, X ^ : " << std::endl;   
  out << std::setw(21) << " ";
  std::vector<std::string>::const_iterator yl;
  for( yl = YLabels.begin(); yl != YLabels.end(); yl++) {
    out << std::setw(15) << *yl << " ";
  }
  out << std::endl;

  std::vector<std::string>::const_iterator xl;
  for( xl = XLabels.begin(); xl != XLabels.end(); xl++) {
    out << std::setw(20) << *xl << " ";
    for( yl = YLabels.begin(); yl != YLabels.end(); yl++) {
      float val;
      if(get(*xl, *yl, val)) {
	out << std::setw(15) << val << " ";	  
      } else {
	out << std::setw(15) << "##";
      }      
    }
    out << std::endl;
  }
}

