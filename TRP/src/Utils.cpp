#ifndef utils_h
#define utils_h
#include "../Utils.h"
#include <sstream>
#include <boost/algorithm/string.hpp>


//#define DEBUG


// utility class to check if TimePoint_IS has varied 
namespace trp_utils {

  bool NeedToPublish(TimePoint_IS new_TP_IS, TimePoint_IS old_TP_IS,
              		   float m_variation, float m_time, 
		                 std::vector<int> items_to_check) {
#ifdef DEBUG
    std::cout << ">>>>>>>> Utils::NeedToPublish " << std::endl;
    std::cout << "Previous TimePoint_IS" << std::endl;
    old_TP_IS.dump(std::cout);
    std::cout << "Old TimePoint_IS" << std::endl;
    new_TP_IS.dump(std::cout);
#endif  

    if(old_TP_IS.RunNumber != new_TP_IS.RunNumber) return(true);
    if(old_TP_IS.LumiBlock != new_TP_IS.LumiBlock) return(true);
    float diff_time = (float)((old_TP_IS.TimeStamp).c_time()-
	                    		    (new_TP_IS.TimeStamp).c_time());
#ifdef DEBUG    
    std::cout << "diff_time:" << diff_time << "  m_time:" << m_time << std::endl;
#endif
    if(fabs(diff_time)>m_time) {
#ifdef DEBUG
      std::cout << "Need to publish since heartbeat time elapsed" << std::endl;
      std::cout << " Time - Limit " << diff_time << " - " << m_time << std::endl;
#endif
      return(true);
    }
    // now check if specific items are varying too much
    std::vector<int>::iterator it;
    // check TBP
    for( it =  items_to_check.begin(); it!= items_to_check.end();++it){
      int id = *it;
      float diff_id = ( (old_TP_IS.Data[id]+new_TP_IS.Data[id]) !=0 ) ? 
        (old_TP_IS.Data[id]-new_TP_IS.Data[id])/(old_TP_IS.Data[id]+new_TP_IS.Data[id]): 
        9999.;
      if(fabs(diff_id)>m_variation){
#ifdef DEBUG
        std::cout << "Need to publish since important rate variation:" << std::endl;
        std::cout << "Obs - Lim " << diff_id << "  - " << m_variation << std::endl;
#endif
	return(true);
      }
    }
#ifdef DEBUG
    std::cout << "No need to publish" << std::endl;
#endif
    return(false);
  }

  std::vector<std::string> split(const std::string& line, const std::string& del) {
    std::vector<std::string> res;
    std::string::size_type old_pos = 0, pos = line.find(del,0);
    while( pos != std::string::npos ) {
      res.push_back(line.substr(old_pos,pos-old_pos));
      old_pos = pos + del.size();
      pos = line.find(del, old_pos);
    }
    // last entry
    if (old_pos < line.size())
      res.push_back(line.substr(old_pos,line.size()-old_pos));
    return res;
  }
 
  void tokenize(const std::string& str,
		std::vector<std::string>& tokens,
		const std::string& delimiters){
    // Skip delimiters at beginning.
    std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // Find first "non-delimiter".
    std::string::size_type pos     = str.find_first_of(delimiters, lastPos);
    
    while (std::string::npos != pos || std::string::npos != lastPos){
      // Found a token, add it to the vector.
      tokens.push_back(str.substr(lastPos, pos - lastPos));
      // Skip delimiters.  Note the "not_of"
      lastPos = str.find_first_not_of(delimiters, pos);
      // Find next "non-delimiter"
      pos = str.find_first_of(delimiters, lastPos);
    }
  }


  void tokenize(const std::string& str,
		std::vector<int>& tokens,
		const std::string& delimiters){
    // Skip delimiters at beginning.
    std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // Find first "non-delimiter".
    std::string::size_type pos     = str.find_first_of(delimiters, lastPos);
    
    while (std::string::npos != pos || std::string::npos != lastPos){
      // Found a token, add it to the vector.
      tokens.push_back(trp_utils::convertToInt(str.substr(lastPos, pos - lastPos)));
      // Skip delimiters.  Note the "not_of"
      lastPos = str.find_first_not_of(delimiters, pos);
      // Find next "non-delimiter"
      pos = str.find_first_of(delimiters, lastPos);
    }
  }
  
 
  void tokenize(const std::string& str,
		std::vector<int>& tokens,
		const std::string& delimiters1,
		const std::string& delimiters2){
    std::vector<std::string> out1;
    boost::algorithm::split(out1, str, boost::is_any_of(delimiters1));
    std::vector<std::string>::iterator it_str;
    for( it_str = out1.begin();it_str!=out1.end();++it_str){
      std::vector<std::string> out2;
      boost::algorithm::split(out2, (*it_str), boost::is_any_of(delimiters2));
      std::cout << "out2 size = " << out2.size() << " (*it_str)=" << (*it_str) << std::endl; 
      if(out2.size()>1){
	int my_first  = trp_utils::convertToInt(out2[0]);
	int my_second = trp_utils::convertToInt(out2[out2.size()-1]);
	if(my_second>my_first && my_second>0 && my_first>0){
	  for(int it_it=my_first; it_it<=my_second;++it_it)
	    tokens.push_back(it_it);
	} else {
	  std::cerr << "Something wrong in trp_utils::tokenize(const std::string str, std::vector<int> tokens,const std::string, const std::string )" << std::endl;
	}
      } else {
	tokens.push_back(trp_utils::convertToInt(*it_str));
      }
    }
  }
  

  double convertToDouble(const std::string& s) {
    std::istringstream i(s);
    double x;
    if (!(i >> x))
      std::cerr << "BadConversion ( convertToDouble(\" "<<  s << "\")" << std::endl;
    return x;
  }

  int convertToInt(const std::string& s) {
    std::istringstream i(s);
    int x;
    if (!(i >> x))
      std::cerr << "BadConversion ( convertToInt(\" "<<  s << "\")" << std::endl;
    return x;
  }
  
  std::string convertToString(float x){
    std::stringstream sout;
    std::string out_s;
    sout << x;
    out_s = sout.str();
    return(out_s);
  }
  std::string convertToString(int x){
    std::stringstream sout;
    std::string out_s;
    sout << x;
    out_s = sout.str();
    return(out_s);
  }
  void convertToString(int x, std::string &out_s){
    std::stringstream sout;
    sout << x;
    out_s = sout.str();
  }

  bool check_suffix(const std::string& my_string, const std::string& suff ) {
    size_t found;
    found=my_string.find_last_of(suff);
    if(found==std::string::npos){
      return(false);
    } else {
      size_t len_suff = suff.size();
      size_t len_string = my_string.size();
      
      if(len_string==found+len_suff){
	return(true);
      }
    }
    return(false);
  }
  std::string remove_char(const std::string& my_string, const std::string& suff ) {
    std::string res = my_string;
    std::string::size_type old_pos = 0, pos = my_string.find(suff,0);
     while( pos != std::string::npos ) {
       res.erase(pos,suff.size());
       old_pos = pos + suff.size();
      pos = res.find(suff, old_pos);
     }
    return res;
  }



  void GetTime(OWLTime &o_time){
    OWLTime now(OWLTime::Seconds);
    o_time = now;
  }
  
  void GetRunParams(std::string pPartitionName,
		    unsigned int & run, unsigned short & lb  ){
    IPCPartition mPartition( pPartitionName );
    ISInfoDictionary dict( mPartition );
    //LuminosityInfo lbi;
    LumiBlock lbi;
    try{
      //dict.getValue("RunParams.LuminosityInfo", lbi);
      dict.getValue("RunParams.LumiBlock", lbi);
      run = lbi.RunNumber;
      lb = lbi.LumiBlockNumber;
      //  }catch(ers::Issue &e){
    }catch(...){
      //ERS_DEBUG(0, "Cannot access RunParams.LuminosityInfo info");
      ERS_DEBUG(0, "Cannot access RunParams.LumiBlock info");
      run = 99999;
      lb = 9;
    }
  }
  
  void GetTriggerKeys_OLD(std::string pPartitionName,
		      int & smk, int & hltk, int & l1k ){
    IPCPartition mPartition( pPartitionName );
    ISInfoDictionary dict( mPartition );
    // da cambiare
    TrigConfKeys trk;
    try{
      dict.getValue("L1CT.TrigConfKeys", trk);
      smk = trk.SuperMasterKey;
      l1k = trk.L1MasterKey;
      hltk = 999;
      //      lb = lb.LumiBlockNumber;
      //  }catch(ers::Issue &e){
    }catch(...){
      //ERS_DEBUG(0, "Cannot access RunParams.LuminosityInfo info");
      ERS_DEBUG(0, "Cannot access RunParams.LumiBlock info");
      smk = 999;
      l1k = 999;
      hltk = 999;
    }
  }

  void
  GetTriggerKeys( std::string pPartitionName,
                  unsigned int & smk, unsigned int & hltk, unsigned int & l1k ){
    IPCPartition mPartition( pPartitionName );
    ISInfoDictionary dict( mPartition );
    
    TrigConfSmKey sm_k;
    TrigConfL1PsKey l1_k;
    TrigConfHltPsKey hlt_k;
    // The keys are taken from RunParams IS Server
    // cf https://savannah.cern.ch/bugs/?40314
    try{
      dict.getValue( "RunParams.TrigConfSmKey", sm_k);
      smk = sm_k.SuperMasterKey;
    } catch(...){
      std::cerr << "No RunParams.TrigConfSmKey" << std::endl;
      smk = 0;
    }
      
    try {
      dict.getValue( "RunParams.TrigConfL1PsKey", l1_k);
      l1k = l1_k.L1PrescaleKey;
    } catch(...){
      std::cerr << "No RunParams.TrigConfL1PsKey" << std::endl;
      l1k = 0;
    }
    try{
      dict.getValue( "RunParams.TrigConfHltPsKey", hlt_k);
      hltk = hlt_k.HltPrescaleKey;
    } catch(...){
      std::cerr << "No RunParams.TrigConfHltPsKey" << std::endl;
      hltk = 0;
    }
  }

  void GetTriggerKeys(std::string pPartitionName,
                      unsigned int & smk, unsigned int & hltk, unsigned int & l1k, unsigned int & bgk ){

    IPCPartition mPartition( pPartitionName );
    ISInfoDictionary dict( mPartition );


    GetTriggerKeys(pPartitionName, smk, hltk, l1k);

    try {
      TrigConfL1BgKey bg_k;
      dict.getValue( "RunParams.TrigConfL1BgKey", bg_k);
      bgk = bg_k.L1BunchGroupKey;
    } catch(...){
      std::cerr << "No RunParams.TrigConfL1BgKey" << std::endl;
      bgk = 0;
    }

  }

  void GetTriggerRelease(std::string pPartitionName, std::string& triggerRelease)
  {
    IPCPartition mPartition( pPartitionName );
    ISInfoDictionary dict( mPartition );
   
    std::string name = "RunParams";
    name += ".";
    name += "TrigConfRelease";
    TrigConfReleaseNamed m_release_named(TrigConfReleaseNamed(pPartitionName, name.c_str()));
 
    try{
      dict.getValue( "RunParams.TrigConfRelease", m_release_named);
      triggerRelease += m_release_named.HLTPatchProject;
      triggerRelease += "_";
      triggerRelease += m_release_named.HLTReleaseVersion;
    } catch(...){
      std::cerr << "No RunParams.TrigConfRelease" << std::endl;
      triggerRelease = "NaN";
    }
  }


  void GetLuminosityInfo(std::string pPartitionName,
                          double & var1, double & var2, double & var3, unsigned int & var4, unsigned int & var5 ){

    IPCPartition mPartition( pPartitionName );  // OLC
    ISInfoDictionary dict( mPartition );

    OCLumi ol;  

    try {
      dict.getValue( "OLC.OLCApp/ATLAS_PREFERRED_Inst", ol);
      var1 = ol.RawLumiPerBX;
      var2 = ol.Mu;
      var3 = ol.CalibLumi;
      var4 = ol.NumberOfTurns;
      var5 = ol.NumberOfFilledBunches;
      
    } catch(...){
      std::cerr << "No OLC.OLCApp/ATLAS_PREFERRED_Inst" << std::endl;
      var1 = 0.;
      var2 = 0.;
      var3 = 0;
      var4 = 0;
      var5 = 0;
    }

  }
  


  void CopyAxis(TimePoint_IS & my_temp, TimePoint_IS tp){ // Copy axis to my_temp tp // Not used....
    std::vector<std::string>::iterator its;
    for(its=tp.XLabels.begin();its!=tp.XLabels.end();++its){
      my_temp.XLabels.push_back(*its);
    }
    for(its=tp.YLabels.begin();its!=tp.YLabels.end();++its){
      my_temp.YLabels.push_back(*its);
    }
  } // end of method
    
    // ---------------------------------------------------------------
  void Scale(TimePoint_IS & my_tp, float scale){
    // just Data is scaled 
    // takes no prisoners if different Data.size overwrites the contents
    TimePoint_IS my_temp_tp;
    my_temp_tp = my_tp;
    int it_0=0;
    for(std::vector<float>::iterator it=(my_tp.Data).begin();
	it!= (my_tp.Data).end();
	++it,++it_0){
      my_temp_tp.Data[it_0]= (*it)*scale;
    }
    my_tp.Data = my_temp_tp.Data;
    
#ifdef DEBUG
    std::cout << "Add result is:" << std::endl;
    my_tp.print(std::cout);
#endif
  }
  
  // ---------------------------------------------------------------
  
  
  void Add(TimePoint_IS & my_temp, TimePoint_IS tp){ // adds into my_temp tp
    // just Data is added 
    // takes no prisoners if different Data.size overwrites the contents
    // this is normal when the first histo is added
#ifdef DEBUG
    std::cout << "Adding tp:" << std::endl;
    tp.print(std::cout);
    std::cout << " to my_temp:" << std::endl;
    my_temp.print(std::cout);
#endif
    //    if(tp.Data.size()!=my_temp.Data.size()){
    if(my_temp.XLabels[0]=="xxx"){
      my_temp.EraseCont();
      my_temp.Data.reserve(tp.Data.size());
      my_temp.XLabels   = tp.XLabels;
      my_temp.YLabels   = tp.YLabels;
      my_temp.Data      = tp.Data;
      my_temp.MetaData  = tp.MetaData;
      OWLTime my_time   = tp.TimeStamp;
      my_temp.TimeStamp = my_time;
      my_temp.RunNumber = tp.RunNumber;
      my_temp.LumiBlock = tp.LumiBlock;
    } else {

      // check if already present in MetaData
      std::vector<std::string>::iterator its;
      bool found = false;
      for(its = (my_temp.MetaData).begin();its!=(my_temp.MetaData).end();++its){
	if((*its)==(tp.MetaData)[0]){
	  found = true;
#ifdef DEBUG
	  std::cout << "Already present Metadata:" << (tp.MetaData)[0] << std::endl;
#endif 
	  // return;
	  break;
	}
      }
      if(!found){
	// now add
	int it_0=0;
	for(std::vector<float>::iterator it=(tp.Data).begin();
	    it!= (tp.Data).end();
	    ++it,++it_0){
	  my_temp.Data[it_0]+= (*it);
	}
	// also append MetaData
	if(tp.MetaData.size()>0){
	  (my_temp.MetaData).push_back((tp.MetaData)[0]);
	}
      }
      OWLTime my_time =  tp.TimeStamp;
      my_temp.TimeStamp = my_time;
      my_temp.RunNumber = tp.RunNumber;
      my_temp.LumiBlock = tp.LumiBlock;
    }
#ifdef DEBUG
    std::cout << "Add result is:" << std::endl;
    my_temp.print(std::cout);
#endif
  }
  
  // ---------------------------------------------------------------
  
  TimePoint_IS  Add(TimePoint_IS my_t1, float scale1, 
		    TimePoint_IS my_t2, float scale2){
    TimePoint_IS my_tp;
    my_tp.EraseCont();
    // just do operation on data
    if(my_t1.Data.size()!=my_t2.Data.size()){
      std::cout << "Warning. Dimension of the two TP are different! " <<  
	my_t1.Data.size() << " vs. " << my_t2.Data.size() << std::endl;
#ifdef DEBUG 
     std::cout << "First member:" << std::cout;
      my_t1.print_as_table(std::cout);
      std::cout << "Second member:" << std::cout;
      my_t2.print_as_table(std::cout);
#endif
    }
    my_tp.Data.reserve(my_t1.Data.size());
    int it2=0;
    std::vector<float>::iterator it3 = my_t2.Data.begin();
    for(std::vector<float>::iterator it1 = my_t1.Data.begin();
	it1 != my_t1.Data.end(); ++it1, ++it2, ++it3){
      my_tp.Data.push_back((*it1)*scale1+(*it3)*scale2);
    }
    my_tp.TimeStamp = my_t1.TimeStamp;
    my_tp.RunNumber = my_t1.RunNumber;
    my_tp.LumiBlock = my_t1.LumiBlock;
    my_tp.XLabels   = my_t1.XLabels;
    my_tp.YLabels   = my_t1.YLabels;
    my_tp.MetaData  = my_t1.MetaData;
    return my_tp;
  }
  
  // ---------------------------------------------------------------
  
  time_t OWL2Time(OWLTime ow_time){
#ifdef DEBUG
    std::cout << "ow_time:" << ow_time << std::endl;
    std::cout << "  year:" << ow_time.year() << "  mon:" << ow_time.month() << "  day:" << ow_time.day() 
	      << "  hour:" << ow_time.hour() << "  min:" << ow_time.min() << "  sec:" << ow_time.sec() << std::endl;
#endif
    time_t rawtime;
    struct tm * time_ptr;
    time ( &rawtime );
    time_ptr = localtime ( &rawtime );

    time_ptr->tm_year = ow_time.year()-1900;
    time_ptr->tm_mon  = ow_time.month();
    time_ptr->tm_mday  = ow_time.day();
    time_ptr->tm_hour = ow_time.hour();
    time_ptr->tm_min  = ow_time.min();
    time_ptr->tm_sec  = ow_time.sec();
#ifdef DEBUG
    std::cout << " year:" << time_ptr->tm_year << "  mon:" <<  time_ptr->tm_mon << 
      "  hour:" << time_ptr->tm_hour << "  min:" << time_ptr->tm_min << "  sec:" << time_ptr->tm_sec << std::endl;
#endif
    //    std::cout << "  tm:" << *time_ptr << std::endl;
    time_t my_time = mktime(time_ptr);
#ifdef DEBUG
    std::cout << "my_time : " << my_time << std::endl;
#endif
    return(my_time);

  }

  void TotalSum(TimePoint_IS &m_mu_point){
    std::vector<float> my_sum;
    my_sum.reserve(m_mu_point.NumRow());
    my_sum.assign(m_mu_point.NumRow(),0);
    for(unsigned int ix=0;ix<(unsigned int)m_mu_point.NumRow();++ix){
      for(unsigned int iy=0;iy<(unsigned int)m_mu_point.NumCol()-1;++iy){
	float tmp_val = 0;
	if(m_mu_point.get(iy, ix, tmp_val)){
	  my_sum[ix]+=tmp_val;
	}
      }
    }
    // now set the last row
    for(unsigned int ix=0;ix<(unsigned int)m_mu_point.NumRow();++ix){
      m_mu_point.set(m_mu_point.NumCol()-1,ix, my_sum[ix]);
    }
  }
} // end of trp_utils namespace
#endif
