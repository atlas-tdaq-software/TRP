#include "TRP/Args.h"
#include <iostream>


Args::Args( const int32_t argc, char* argv[] ) 
{ 
  reserve(argc);
  for( int32_t i=0; i < argc; ++i )
    push_back( argv[i] );
}

Args::const_iterator Args::getArg( const std::string arg ) const
  { return find( begin(), end(), arg ); }
  
Args::const_iterator Args::getNextArg( const_iterator pos ) const
  { return ( pos == end() ) ? pos : ++pos; }

std::string Args::getStr( const std::string arg, const std::string dflt ) const
{
  const_iterator p = getNextArg( getArg(arg) );
  return ( p == end() ) ? dflt : *p;
}

int32_t Args::getInt( const std::string arg, const int32_t dflt ) const
{
  const_iterator p = getNextArg( getArg(arg) );
  return ( p == end() ) ? dflt : atoi(p->c_str());
}

bool Args::hasArg( const std::string arg ) const
  { return getArg( arg ) != end(); }

void Args::mustHave( const std::string arg ) const 
{
  if( !hasArg( arg ) )
  {
    std::cerr << "Args::mustHave(): Missing parameter '" << arg << "'" << std::endl;
    ::exit(1);
  }
}

