#include <iostream>
using namespace std;
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>

#include "is/infodictionary.h"
#include "is/exceptions.h"

//#include "rc/LuminosityInfo.h"
#include "TTCInfo/LumiBlock.h"

#include "../TimePoint_IS.h"
#include "../TimePointAsyncSender.h"

const TimePointAsyncSender::ObjToSend TimePointAsyncSender::s_nothing(0,"");

TimePointAsyncSender::TimePointAsyncSender(ISInfoDictionary* is, const std::string& name, bool fate)
  : m_is(is), 
    m_default_name(name), 
    m_fate(fate),
    m_thread(0), 
    m_keep_running(true),
    m_in_error(false) {
  m_thread = new boost::thread(boost::bind(&TimePointAsyncSender::run, this));
}


TimePointAsyncSender::~TimePointAsyncSender() {
  m_keep_running = false;
  m_thread->interrupt();
  m_thread->join();
  delete m_thread;
  m_thread = 0;
  if ( m_queue.size() )
    ERS_INFO("While terminating adapter execution there were still: " 
	     << m_queue.size() << "  object pending to be sent to IS" );
}



void TimePointAsyncSender::send(const TimePoint_IS* p, const std::string& name ) {
  insert(ObjToSend(p, name));
  m_newdata_condition.notify_all();
}



bool TimePointAsyncSender::is_send(const TimePoint_IS* p) {
  boost::lock_guard<boost::mutex> guard(m_insert_mutex);
  return find(m_queue.begin(), m_queue.end(), p) == m_queue.end();
}


void TimePointAsyncSender::run() {
  while (m_keep_running) {
    wait_for_data();
 }
}

void TimePointAsyncSender::wait_for_data() {

  boost::unique_lock<boost::mutex> newdata_lock(m_newdata_mutex);
  if ( (m_queue.empty() || m_in_error ) && m_keep_running) {
    // hang on condition varaible until new data arrives
    m_newdata_condition.wait(newdata_lock);
    ERS_DEBUG(3, "New data arrived, will attempt to sent it");
  }

  const ObjToSend& to_send = retrieve();
  
  update(const_cast<TimePoint_IS*>(to_send.object));
  std::string name_to_use = to_send.name.empty() ? m_default_name : to_send.name;
  try { 
    
    m_is->checkin(name_to_use, *(to_send.object), true);

    drop();
    m_in_error = false;
    ERS_DEBUG(3, "TimePoint " << name_to_use << " checked in correctly, dropped from send-queue");

  } catch (daq::is::Exception &ex ) {    
    ERS_INFO( "Failed checking in TimePoint " << name_to_use << " reason: " << ex.what());
    m_in_error = true;
  } catch (...) {
    throw;
  }
}


void TimePointAsyncSender::insert(const ObjToSend& p) {
  boost::lock_guard<boost::mutex> guard(m_insert_mutex);
  m_queue.push_back(p);
  ERS_DEBUG(1, "Send queue has size: " << m_queue.size()); 
}

const TimePointAsyncSender::ObjToSend& TimePointAsyncSender::retrieve() {
  boost::lock_guard<boost::mutex> guard(m_insert_mutex);
  if ( m_queue.empty() ) 
    return s_nothing;
  
  return m_queue.front();
}

void TimePointAsyncSender::drop() {
  boost::lock_guard<boost::mutex> guard(m_insert_mutex);
  if (m_fate == DeleteAfterSend)
    if ( m_queue.begin() != m_queue.end() )
      delete m_queue.front().object;
  m_queue.pop_front();
}

void TimePointAsyncSender::update(TimePoint_IS* p) {
  // pull current run & LBN
  //const std::string location("RunParams.LuminosityInfo");
  const std::string location("RunParams.LumiBlock");
  try {
    LumiBlock params;
    m_is->getValue(location, params);
    
    // here should also go reading of config keys and other metadata
    p->RunNumber = params.RunNumber;
    p->LumiBlock = params.LumiBlockNumber;

  } catch (daq::is::Exception &ex ) {    
    ERS_INFO( "Failed reading params from " << location << " reason: " << ex.what());
    m_in_error = true;
  } catch (...) {
    throw;
  }
}
