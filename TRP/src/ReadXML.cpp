#include "../ReadXML.h"
#include <string>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <list>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>


using namespace xercesc;
using namespace std;

/**
 *  Constructor initializes xerces-C libraries.
 *  The XML tags and attributes which we seek are defined.
 *  The xerces-C DOM parser infrastructure is initialized.
 */

ReadXML::ReadXML(){
  try{
    XMLPlatformUtils::Initialize();  // Initialize Xerces infrastructure
  } catch( XMLException& e ){
    char* message = XMLString::transcode( e.getMessage() );
    cerr << "XML toolkit initialization error: " << message << endl;
    XMLString::release( &message );
    // throw exception here to return ERROR_XERCES_INIT
  }
  
  // Tags and attributes used in XML file.
  // Can't call transcode till after Xerces Initialize()
  TAG_root        = XMLString::transcode("root");
    
  m_ConfigFileParser = new XercesDOMParser;
  
  // initialize the common part
  m_vec_element_0.reserve(20);
  m_vec_xmlch_0.reserve(20);
  m_vec_text_0.reserve(20);
  m_vec_element.reserve(20);
  m_vec_xmlch.reserve(20);
  m_vec_text.reserve(20);
  
  m_element_0="common";
  ch_el_0 =  XMLString::transcode(m_element_0.c_str());
  //  m_vec_element_0.push_back("partition");
  m_vec_element_0.push_back("server");
  int it_0 =0;
  for(std::vector<std::string>::iterator it = m_vec_element_0.begin();
      it!=m_vec_element_0.end();
      ++it,++it_0){
    XMLCh* ch_it;
    ch_it = XMLString::transcode((*it).c_str());
    m_vec_xmlch_0.push_back(ch_it);
    std::cout << "ReadXML:" << it_0 << "  " << (*it) << std::endl;
  }
  
}

/**
 *  Class destructor frees memory used to hold the XML tag and 
 *  attribute definitions. It als terminates use of the xerces-C
 *  framework.
 */

ReadXML::~ReadXML(){
//   try{
//     XMLPlatformUtils::Terminate();  // Terminate Xerces
//   }catch( xercesc::XMLException& e ){
//     char* message = xercesc::XMLString::transcode( e.getMessage() );
    
//     cerr << "XML ttolkit teardown error: " << message << endl;
//     XMLString::release( &message );
//   }
  
//   // Free memory
  

//   try{
//     XMLString::release( &TAG_root );
    
//     XMLString::release( &ch_el_0 );
//     XMLString::release( &ch_el );
//     for(std::vector< XMLCh*>::iterator it=m_vec_xmlch_0.begin();
// 	it!=m_vec_xmlch_0.end(); ++it){
//       XMLString::release( &(*it) );
//     }
//     for(std::vector< XMLCh*>::iterator it=m_vec_xmlch.begin();
// 	it!=m_vec_xmlch.end(); ++it){
//       XMLString::release( &(*it) );
//     }
    
    
//   }catch( ... ){
//     cerr << "Unknown exception encountered in TagNamesdtor" << endl;
//   }
  
}

/**
 *  This function:
 *  - Tests the access and availability of the XML configuration file.
 *  - Configures the xerces-c DOM parser.
 *  - Reads and extracts the pertinent information from the XML config file.
 *
 *  @param in configFile The text string name of the HLA configuration file.
 */

void ReadXML::readConfigFile(string& configFile)
  /* throw( std::runtime_error ) */
{
  // first set the varying vector
  ch_el =  XMLString::transcode(m_element.c_str());
  int it_0 =0;
  for(std::vector<std::string>::iterator it = m_vec_element.begin();
      it!=m_vec_element.end();
      ++it,++it_0){
    XMLCh* ch_it;
    ch_it = XMLString::transcode((*it).c_str());
    m_vec_xmlch.push_back(ch_it);
    std::cout << "ReadXML2:" << it_0 << "  " << (*it) << std::endl;
  }
  
  // Test to see if the file is ok.
  
  struct stat fileStatus;
  
  int iretStat = stat(configFile.c_str(), &fileStatus);
  if( iretStat == ENOENT )
    throw ( std::runtime_error("Path file_name does not exist, or path is an empty string.") );
  else if( iretStat == ENOTDIR )
    throw ( std::runtime_error("A component of the path is not a directory."));
  else if( iretStat == ELOOP )
    throw ( std::runtime_error("Too many symbolic links encountered while traversing the path."));
  else if( iretStat == EACCES )
    throw ( std::runtime_error("Permission denied."));
  else if( iretStat == ENAMETOOLONG )
    throw ( std::runtime_error("File can not be read\n"));
  
  // Configure DOM parser.
  
  m_ConfigFileParser->setValidationScheme( XercesDOMParser::Val_Never );
  m_ConfigFileParser->setDoNamespaces( false );
  m_ConfigFileParser->setDoSchema( false );
  m_ConfigFileParser->setLoadExternalDTD( false );
  
  try{
    m_ConfigFileParser->parse( configFile.c_str() );
    
    // no need to free this pointer - owned by the parent parser object
    DOMDocument* xmlDoc = m_ConfigFileParser->getDocument();
    
    // Get the top-level element: NAme is "root". No attributes for "root"
    
    DOMElement* elementRoot = xmlDoc->getDocumentElement();
    if( !elementRoot ) throw(std::runtime_error( "empty XML document" ));
    
    // Parse XML file for tags of interest: "ApplicationSettings"
    // Look one level nested within "root". (child of root)
    
    DOMNodeList*      children = elementRoot->getChildNodes();
    const  XMLSize_t nodeCount = children->getLength();
    
    // For all nodes, children of "root" in the XML tree.
    int it2=0;
    for( XMLSize_t xx = 0; xx < nodeCount; ++xx,++it2 ){
      DOMNode* currentNode = children->item(xx);
      if( currentNode->getNodeType() &&  // true is not NULL
	  currentNode->getNodeType() == DOMNode::ELEMENT_NODE ){ // is element 
	// Found node which is an Element. Re-cast node as element
	DOMElement* currentElement
	  = dynamic_cast< xercesc::DOMElement* >( currentNode );
	if( XMLString::equals(currentElement->getTagName(), ch_el_0)){ // this looks for common
	  // Already tested node as type element and of name "ch_el_0"
	  // Read attributes of element "ch_el_0".
	  int it_0 = 0;
	  for(std::vector< std::string >::iterator it = 
		m_vec_element_0.begin();
	      it!=m_vec_element_0.end(); ++it,++it_0){
	    std::cout << "ReadXML 3.1:" << (*it) << std::endl;
	    const XMLCh* xmlch_tmp
	      = currentElement->getAttribute(m_vec_xmlch_0[it_0]);
	    char *c_tmp = XMLString::transcode(xmlch_tmp);
	    if(c_tmp==NULL) continue;
	    std::string s_tmp(c_tmp);
	    //	    m_vec_text_0[it_0] = s_tmp;
	    m_vec_text_0.push_back(s_tmp);
	  }
	  break;
	} // end of if common
	// now look for specific adapter block
      }
    }
    it2=0;
    for( XMLSize_t xx = 0; xx < nodeCount; ++xx,++it2 ){
      DOMNode* currentNode = children->item(xx);
      if( currentNode->getNodeType() &&  // true is not NULL
	  currentNode->getNodeType() == DOMNode::ELEMENT_NODE ){ // is element 
	// Found node which is an Element. Re-cast node as element
	DOMElement* currentElement
	  = dynamic_cast< xercesc::DOMElement* >( currentNode );
	if( XMLString::equals(currentElement->getTagName(), ch_el)){ // this looks for common
	  // Already tested node as type element and of name "ch_el"
	  // Read attributes of element "ch_el".
	  int it_0 = 0;
	  for(std::vector< std::string >::iterator it = 
		m_vec_element.begin();
	      it!=m_vec_element.end(); ++it,++it_0){
	    std::cout << "ReadXML Config:" << (*it);
	    const XMLCh* xmlch_tmp
	      = currentElement->getAttribute(m_vec_xmlch[it_0]);
	    char *c_tmp = XMLString::transcode(xmlch_tmp);
	    if(c_tmp==NULL) continue;
	    std::cout << "  " << c_tmp << std::endl;
	    std::string s_tmp(c_tmp);
	    //	    m_vec_text_0[it_0] = s_tmp;
	    m_vec_text.push_back(s_tmp);
	  }
	  break;
	} // end of specific part of adapter
      }
    }
  }
  catch( xercesc::XMLException& e ){
    char* message = xercesc::XMLString::transcode( e.getMessage() );
    ostringstream errBuf;
    errBuf << "Error parsing file: " << message << flush;
    XMLString::release( &message );
  }
}

std::string ReadXML::GetValC(std::string my_element_str){
  std::string out_str = "";
  int it0=0;
  for(std::vector<std::string>::iterator its=m_vec_element_0.begin();
      its!=m_vec_element_0.end();
      ++its){
    if((*its)==my_element_str){
      out_str = m_vec_text_0[it0]; 
      break;
    }
    ++it0;
  }
  return(out_str);
}

std::string ReadXML::GetVal(std::string my_element_str){
  std::string out_str = "";
  int it0=0;
  for(std::vector<std::string>::iterator its=m_vec_element.begin();
      its!=m_vec_element.end();
      ++its){
    if((*its)==my_element_str){
      out_str = m_vec_text[it0];
      break;
    }
    ++it0;
  }
  return(out_str);
}


