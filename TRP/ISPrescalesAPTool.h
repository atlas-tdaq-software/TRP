#ifndef ISPRESCALESAPTOOL_H
#define ISPRESCALESAPTOOL_H

#include <is/info.h>

#include <string>
#include <ostream>
#include <vector>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * Prescales output of AutoPrescaleTool
 * 
 * @author  generated by the IS tool
 * @version 26/04/09
 */

class ISPrescalesAPTool : public ISInfo {
public:

    /**
     * Run number.
     */
    unsigned int                  Run;

    /**
     * Lumi block number.
     */
    unsigned int                  LB;

    /**
     * Supermaster key
     */
    unsigned int                  SuperMasterKey;

    /**
     * LVL1 prescale key.
     */
    unsigned int                  L1PrescaleKey;

    /**
     * Seconds of event dump.
     */
    unsigned int                  Seconds;

    /**
     * Nanoseconds of event dump.
     */
    unsigned int                  Nanoseconds;

    /**
     * Old L1 output rate.
     */
    double                        OldL1Rate;

    /**
     * New L1 output rate.
     */
    double                        NewL1Rate;

    /**
     * Old prescale values
     */
    std::vector<int>              OldPrescales;

    /**
     * New prescale values
     */
    std::vector<int>              NewPrescales;

    /**
     * Item names
     */
    std::vector<std::string>      ItemNames;

    /**
     * Item priorities
     */
    std::vector<std::string>      Priorities;


    static const ISType & type() {
	static const ISType type_ = ISPrescalesAPTool( ).ISInfo::type();
	return type_;
    }

    virtual std::ostream & print( std::ostream & out ) const {
	ISInfo::print( out );
	out << "Run: " << Run << "	//Run number." << std::endl;
	out << "LB: " << LB << "	//Lumi block number." << std::endl;
	out << "SuperMasterKey: " << SuperMasterKey << "	//Supermaster key" << std::endl;
	out << "L1PrescaleKey: " << L1PrescaleKey << "	//LVL1 prescale key." << std::endl;
	out << "Seconds: " << Seconds << "	//Seconds of event dump." << std::endl;
	out << "Nanoseconds: " << Nanoseconds << "	//Nanoseconds of event dump." << std::endl;
	out << "OldL1Rate: " << OldL1Rate << "	//Old L1 output rate." << std::endl;
	out << "NewL1Rate: " << NewL1Rate << "	//New L1 output rate." << std::endl;
	out << "OldPrescales[" << OldPrescales.size() << "]:	//Old prescale values" << std::endl;
	for ( size_t i = 0; i < OldPrescales.size(); ++i )
	    out << i << ": " << OldPrescales[i] << std::endl;
	out << "NewPrescales[" << NewPrescales.size() << "]:	//New prescale values" << std::endl;
	for ( size_t i = 0; i < NewPrescales.size(); ++i )
	    out << i << ": " << NewPrescales[i] << std::endl;
	out << "ItemNames[" << ItemNames.size() << "]:	//Item names" << std::endl;
	for ( size_t i = 0; i < ItemNames.size(); ++i )
	    out << i << ": " << ItemNames[i] << std::endl;
	out << "Priorities[" << Priorities.size() << "]:	//Item priorities" << std::endl;
	for ( size_t i = 0; i < Priorities.size(); ++i )
	    out << i << ": " << Priorities[i] << std::endl;
	return out;
    }

    ISPrescalesAPTool( )
      : ISInfo( "ISPrescalesAPTool" )
    {
	initialize();
    }

    ~ISPrescalesAPTool(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    ISPrescalesAPTool( const std::string & type )
      : ISInfo( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << Run << LB << SuperMasterKey << L1PrescaleKey << Seconds << Nanoseconds << OldL1Rate;
	out << NewL1Rate << OldPrescales << NewPrescales << ItemNames << Priorities;
    }

    void refreshGuts( ISistream & in ){
	in >> Run >> LB >> SuperMasterKey >> L1PrescaleKey >> Seconds >> Nanoseconds >> OldL1Rate;
	in >> NewL1Rate >> OldPrescales >> NewPrescales >> ItemNames >> Priorities;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const ISPrescalesAPTool & info ) {
    info.print( out );
    return out;
}

#endif // ISPRESCALESAPTOOL_H
