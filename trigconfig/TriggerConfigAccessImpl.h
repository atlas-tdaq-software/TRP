/* Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration */
#ifndef TRIGGERCONFIGACCESSIMPL_H
#define TRIGGERCONFIGACCESSIMPL_H

#include <vector>
#include <map>
#include <string>
#include "TRP/Branch.h"
#include "TRP/Tree.h"
#include <ipc/core.h>
#include <ipc/partition.h>

#include "TRP/TriggerMenuHelpers.h"
#include "TriggerDBHelper.h"

#include "boost/property_tree/ptree.hpp"

namespace coral {
   class ISessionProxy;
   class IConnection;
}

struct TriggerConfigAccessImpl {

   typedef confadapter_imp::TriggerMenuHelpers::Mapping Mapping;
   typedef confadapter_imp::TriggerMenuHelpers::MultiMapping MultiMapping;
   typedef confadapter_imp::TriggerMenuHelpers::MapStrInt MapStrInt;

   typedef std::pair<std::string, std::string> M_Pair;
   typedef std::map<std::string, MapStrInt> MapStreamPrescales;

   /**
    *  constructors
    */
   // this constructor take the partition name from the environment var TDAQ_PARTITION
   TriggerConfigAccessImpl(const std::string & dbAlias="TRIGGERDB");
   TriggerConfigAccessImpl(const std::string & partitionName, const std::string & dbAlias);

   /**
    * retrievers and accessors of the configuration information
    */
   bool getConfiguration(unsigned int smk, unsigned int hltk, unsigned int l1k);

   bool getConfigurationRun2(unsigned int smk, unsigned int hltk, unsigned int l1k);

   /** LHC Run, returns 3 */
   unsigned int lhcRun() const;

   /**
    *  Access to maps
    */
   /** Get Mapping for given name */
   bool getMapping(Mapping& m, const std::string& name) const;

   /**
    * returns reference to map
    * throws std::out_of_range exception if map name doesn't exist
    */
   const Mapping & getMapping(const std::string& name) const; 

   bool getMultiMapping(MultiMapping& m, const std::string& name) const;

   const MultiMapping & getMultiMapping(const std::string& name) const;

   bool getStreamPrescalesMap(MapStrInt &streamPrescales, const std::string& stream ) const;

   const MapStrInt & getStreamPrescalesMap(const std::string& stream ) const;

   /**
    * functions to convert maps into TRP Branches and TRP Trees
    */
   void Mapping2Branch( const MultiMapping & m, std::vector<Branch>& vecBranch) const;

   void Mapping2Tree( const MultiMapping & m, const MultiMapping & m2, std::vector<Tree> & vecTree) const;

   /**
    * functions to publish information 
    */
   void PublishBranch( const std::string & server_info, const std::vector<Branch> & my_branch);

   void PublishTree( const std::string & server_info, const std::vector<Tree> & my_tree);

   void EraseInfo( const std::string & server_info, const std::string & rm_string);

   /**
    * various print functions
    */
   void printPrescaleMaps(bool size_only = false) const;

   void printMapping( const Mapping& m, const std::string& prefix = " ", bool size_only = false) const;

   void printMultiMapping( const MultiMapping& m, const std::string& prefix = " ", bool size_only = false) const;

   /**
    * for testing
    */
   void printAllMaps(bool size_only = false) const;

   /**
    * internal functions
    */
   void insertStreamPrescale(std::string stream, std::string chain, float prescale);

   /** DB querying related functions */
   std::map<std::string, QueryDefinition> getQueryDefinitions() const;

   boost::property_tree::ptree loadKey (coral::ISessionProxy* session, QueryDefinition & qdef, unsigned int key) const;

   void open_db( coral::ISessionProxy*& );
   void createSession( coral::ISessionProxy*& );
   void close_db( coral::ISessionProxy*& );

   bool run_query ( coral::ISessionProxy* session, int smk, int hltk, int l1psk );

   bool run_query_Run2 ( coral::ISessionProxy* session, int smk, int hltk );
   bool run_query_ps_Run2 ( coral::ISessionProxy* session, int hltk );


   bool get_available_smks(coral::ISessionProxy* session);

   /**
    * data
    */
   std::string   m_connectionString {""};
   std::string   m_partitionName {""};
   IPCPartition  m_partition {}; 

   int m_smk {0};
   int m_hltk {0};
   int m_l1k {0};
   unsigned int m_lhcRun{0};

   /**
    *  collection of maps for certain associations within the menu
    */
   std::map<std::string, Mapping> m_mappings;
   std::map<std::string, MultiMapping> m_multimappings;
   MapStreamPrescales m_streamPrescales;

};

#endif 
