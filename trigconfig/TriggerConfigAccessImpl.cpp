/* Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration */

#include "./TriggerConfigAccessImpl.h"
#include "TRP/Utils.h"

#include <memory>
#include <iostream>
#include <set>
#include <stdexcept>

#include <boost/property_tree/json_parser/error.hpp>

#include "CoralBase/Attribute.h"
#include "CoralBase/AttributeList.h"
#include "CoralBase/Exception.h"

#include "CoralKernel/Context.h"

#include "RelationalAccess/SchemaException.h"

#include "RelationalAccess/IDatabaseServiceDescription.h"
#include "RelationalAccess/IDatabaseServiceSet.h"
#include "RelationalAccess/ILookupService.h"
#include "RelationalAccess/IAuthenticationCredentials.h"
#include "RelationalAccess/IAuthenticationService.h"
#include "RelationalAccess/IRelationalDomain.h"
#include "RelationalAccess/IRelationalService.h"
#include "RelationalAccess/IConnectionServiceConfiguration.h"
#include "RelationalAccess/IConnectionService.h"
#include "RelationalAccess/ConnectionService.h"
#include "RelationalAccess/IDatabaseServiceDescription.h"
#include "RelationalAccess/ITransaction.h"
#include "RelationalAccess/ISchema.h"
#include "RelationalAccess/IColumn.h"
#include "RelationalAccess/TableDescription.h"
#include "RelationalAccess/ISessionProxy.h"
#include "RelationalAccess/ITable.h"
#include "RelationalAccess/ITableDataEditor.h"
#include "RelationalAccess/ICursor.h"
#include "RelationalAccess/IQuery.h"

#include <is/infodictionary.h>
#include <is/infoiterator.h>
#include <sstream>
#include <stdio.h>

using namespace std;
using namespace confadapter_imp;

/**
 * tools
 */
std::string
notilda(const std::string& s)
{
   return s == "~" ? "" : s;
}

template <class T>
static bool from_string(T& t,const std::string& s,std::ios_base& (*f)(std::ios_base&))
{
  std::istringstream iss(s);
  return !(iss >> f >> t).fail();
}


static float convert2(string prescale_s)
{
   float prescale_f;
   if( from_string<float>(prescale_f, std::string(prescale_s), std::dec)){
      return prescale_f;
   }else{
      std::cerr << "String to prescale was not converted! " << std::endl;
      return -999.0;
   }
} 

/**
 *  constructors
 */
TriggerConfigAccessImpl::TriggerConfigAccessImpl(const std::string & dbAlias)
   : m_connectionString(dbAlias)
{
   if(std::getenv("TDAQ_PARTITION")) {
      m_partitionName = std::getenv("TDAQ_PARTITION");
      m_partition = IPCPartition( m_partitionName );
   }
}

TriggerConfigAccessImpl::TriggerConfigAccessImpl(const std::string & partitionName, const std::string & dbAlias)
   : m_connectionString(dbAlias),
     m_partitionName ( partitionName ),
     m_partition( partitionName )
{}

unsigned int
TriggerConfigAccessImpl::lhcRun() const
{ return m_lhcRun; }

/**
 * Access to maps
 */

bool
TriggerConfigAccessImpl::getMapping(Mapping& m, const std::string& name) const
{
   try {
      m =  m_mappings.at(name);
   }
   catch(std::out_of_range &ex) {
      std::cerr << "Map with name " << name << " does not exist. [" << ex.what() << "]" << std::endl;
      return false;
   }
   return true;
}

const TriggerConfigAccessImpl::Mapping &
TriggerConfigAccessImpl::getMapping(const std::string& name) const
{
   try {
      return m_mappings.at(name);
   }
   catch(std::out_of_range &ex) {
      confadapter_imp::MapAccessIssue issue(ERS_HERE, name.c_str());
      ers::error(issue);
      throw issue;
   }
}

bool
TriggerConfigAccessImpl::getMultiMapping(MultiMapping& m, const std::string& name) const
{
   try {
      m =  m_multimappings.at(name);
   }
   catch(std::out_of_range &ex) {
      std::cerr << "MultiMap with name " << name << " does not exist. [" << ex.what() << "]" << std::endl;
      return false;
   }
   return true;
}

const TriggerConfigAccessImpl::MultiMapping &
TriggerConfigAccessImpl::getMultiMapping(const std::string& name) const
{
   try {
      return m_multimappings.at(name);
   }
   catch(std::out_of_range &ex) {
      confadapter_imp::MultiMapAccessIssue issue(ERS_HERE, name.c_str());
      ers::error(issue);
      throw issue;
   }
}

bool
TriggerConfigAccessImpl::getStreamPrescalesMap(MapStrInt &streamPrescales, const std::string& stream ) const
{
   try {
      streamPrescales = m_streamPrescales.at(stream);
   }
   catch(std::out_of_range &ex) {
      std::cerr << "StreamPrescales map for stream " << stream << " does not exist. [" << ex.what() << "]" << std::endl;
      return false;
   }
   return true;
}

const TriggerConfigAccessImpl::MapStrInt &
TriggerConfigAccessImpl::getStreamPrescalesMap(const std::string& stream ) const
{
   try {
      return m_streamPrescales.at(stream);
   }
   catch(std::out_of_range &ex) {
      confadapter_imp::StreamPSMapAccessIssue issue(ERS_HERE, stream.c_str());
      ers::error(issue);
      throw issue;
   }
}

/**
 *  private functions
 */

void
TriggerConfigAccessImpl::insertStreamPrescale(std::string stream, std::string chain, float prescale) 
{
   m_streamPrescales[stream][chain] = prescale;
}

/**
 *  DB connection
 */
void
TriggerConfigAccessImpl::open_db(coral::ISessionProxy*& session)
{
   createSession(session);
}

void
TriggerConfigAccessImpl::createSession(coral::ISessionProxy*& session)
{
   try {
      coral::ConnectionService connSvc;
      coral::IConnectionServiceConfiguration& csc = connSvc.configuration();
      csc.setConnectionRetrialPeriod(2);  // time between 2 connection retrials
      // time out for the connection retrials before the connection service 
      // fails over to the next available replica or quits
      csc.setConnectionRetrialTimeOut(10);
      csc.setConnectionTimeOut(10);
      session = connSvc.connect(m_connectionString, coral::AccessMode::ReadOnly);
      ERS_LOG("Opened session " << m_connectionString);
   }
   catch(std::exception & ex ) {
      confadapter_imp::DBOpenIssue issue(ERS_HERE, m_connectionString.c_str(), ex.what());
      ers::error(issue);
      throw issue;
   }
}

void TriggerConfigAccessImpl::close_db( coral::ISessionProxy*& session)
{
   if(session) {
      delete session;
      ERS_LOG("Closing session " << m_connectionString);
   }
}

/***********************
 **
 **  Run 3 DB loading
 **
 ***********************/

std::map<std::string, QueryDefinition>
TriggerConfigAccessImpl::getQueryDefinitions() const {
   
   std::map<std::string, QueryDefinition> qm;
   
   { // query for the L1 menu
      auto q = QueryDefinition();
      // tables
      q.addToTableList ( "SUPER_MASTER_TABLE", "SMT" );
      q.addToTableList ( "L1_MENU", "L1TM" );
      // bind vars
      q.extendBinding<int>("key");
      // conditions
      q.extendCondition("SMT.SMT_ID = :key");
      q.extendCondition(" AND SMT.SMT_L1_MENU_ID = L1TM.L1TM_ID");
      // attributes
      q.extendOutput<std::string>( "SMT.SMT_NAME" );
      q.extendOutput<int>        ( "SMT.SMT_VERSION" );
      q.extendOutput<int>        ( "SMT.SMT_L1_MENU_ID" );
      q.extendOutput<coral::Blob>( "L1TM.L1TM_DATA" );
      // the field with the data
      q.setDataName("L1TM.L1TM_DATA");
      q.setKeyTable("SUPER_MASTER_TABLE");
      qm["l1menu"] = q;
   }

   { // query for the HLT menu
      auto q = QueryDefinition();
      // tables
      q.addToTableList ( "SUPER_MASTER_TABLE", "SMT" );
      q.addToTableList ( "HLT_MENU", "HTM" );
      // bind vars
      q.extendBinding<int>("key");
      // conditions
      q.extendCondition("SMT.SMT_ID = :key");
      q.extendCondition(" AND SMT.SMT_HLT_MENU_ID = HTM.HTM_ID");
      // attributes
      q.extendOutput<std::string>( "SMT.SMT_NAME" );
      q.extendOutput<int>        ( "SMT.SMT_VERSION" );
      q.extendOutput<int>        ( "SMT.SMT_HLT_MENU_ID" );
      q.extendOutput<coral::Blob>( "HTM.HTM_DATA" );
      // the field with the data
      q.setDataName("HTM.HTM_DATA");
      q.setKeyTable("SUPER_MASTER_TABLE");
      qm["hltmenu"] = q;
   }

   { // query for the L1 prescale set
      auto q = QueryDefinition();
      // tables
      q.addToTableList ( "L1_PRESCALE_SET" );
      // bind vars
      q.extendBinding<int>("key");
      // conditions
      q.extendCondition("L1PS_ID = :key");
      // attributes
      q.extendOutput<coral::Blob>( "L1PS_DATA" );
      // the field with the data
      q.setDataName("L1PS_DATA");
      q.setKeyTable("L1_PRESCALE_SET");
      qm["l1ps"] = q;
   }
   
   { // query for the HLT prescale set
      auto q = QueryDefinition();
      // tables
      q.addToTableList ( "HLT_PRESCALE_SET" );
      // bind vars
      q.extendBinding<int>("key");
      // conditions
      q.extendCondition("HPS_ID = :key");
      // attributes
      q.extendOutput<coral::Blob>( "HPS_DATA" );
      // the field with the data
      q.setDataName("HPS_DATA");
      q.setKeyTable("HLT_PRESCALE_SET");
      qm["hltps"] = q;
   }

   { // query for the bunchgroup set
      auto q = QueryDefinition();
      // tables
      q.addToTableList ( "L1_BUNCH_GROUP_SET" );
      // bind vars
      q.extendBinding<int>("key");
      // conditions
      q.extendCondition("L1BGS_ID = :key");
      // attributes
      q.extendOutput<coral::Blob>( "L1BGS_DATA" );
      // the field with the data
      q.setDataName("L1BGS_DATA");
      q.setKeyTable("L1_BUNCH_GROUP_SET");
      qm["bgs"] = q;
   }
   
   return qm;
}


boost::property_tree::ptree
TriggerConfigAccessImpl::loadKey (coral::ISessionProxy* session, QueryDefinition & qdef, unsigned int key) const
{
   boost::property_tree::ptree pt;
   if(key>0) {
      qdef.setBoundValue<int>("key", key);
      auto q = qdef.createQuery( session );
      try {
         auto & cursor = q->execute();
         if ( ! cursor.next() ) {
            confadapter_imp::KeyNotExistIssue issue(ERS_HERE, key, qdef.keyTable().c_str());
            ers::error(issue);
            throw issue;
         }
         const coral::AttributeList& row = cursor.currentRow();
         const coral::Blob& dataBlob = row[qdef.dataName()].data<coral::Blob>();
         blobToPtree( dataBlob, pt );
      }
      catch(coral::Exception & ex) {
         confadapter_imp::DBQueryIssue issue(ERS_HERE, ex.what());
         ers::error(issue);
         throw issue;
      }
      catch( boost::property_tree::json_parser::json_parser_error & ex) {
         confadapter_imp::JsonParsingIssue issue(ERS_HERE, key, qdef.keyTable().c_str(), ex.what());
         ers::error(issue);
         throw issue;
      }
   }
   return pt;
}

bool
TriggerConfigAccessImpl::run_query( coral::ISessionProxy* session, 
                                    int smk, int hltpsk, int /*l1psk*/ )
{
   // load the menu and prescales 
   auto qm = getQueryDefinitions();
   /* the L1 menu and prescales are currently not being asked for, but can be provided
      auto l1menu = loadKey( session, qm["l1menu"], smk);
      auto l1ps = loadKey( session, qm["l1ps"], l1psk);
   */

   if(smk!=0) {
      try {
         auto hltmenu = loadKey( session, qm["hltmenu"], smk);
         // now going to fill the maps as for Run 2
         Mapping chainCounter2name;
         Mapping chain2item;
         Mapping stream2chains; 
         Mapping group2chains; 
         MultiMapping chainCounter2nameMM;
         MultiMapping chain2itemMM;
         MultiMapping group2chainsMM;
         for( auto & chain : hltmenu.get_child("chains") ) {
            const std::string & chainName = chain.first;
            const std::string & itemName = chain.second.get<string>("l1item");
            std::string chainCounter = to_string(chain.second.get<unsigned int>("counter"));
            chainCounter2name[ chainCounter ].push_back(chainName);
            chain2item[ chainName ].push_back(itemName);
            chainCounter2nameMM.insert( { chainCounter, chainName } );
            chain2itemMM.insert( { itemName, chainName }); // this is wrong, but reflects what is in the code so far (probably not used)
            for( auto & stream : chain.second.get_child("streams") ) {
               const std::string & streamName = stream.second.data();
               stream2chains[ streamName ].push_back(chainName);
            }
            for( auto & group : chain.second.get_child("groups") ) {
               const std::string & groupName = group.second.data();
               group2chains[ groupName ].push_back( chainName );
               group2chainsMM.insert( { groupName, chainName } );
            }
         }
         m_mappings["cc_hlt"] = std::move( chainCounter2name );
         m_mappings["hlt_l1"] = std::move( chain2item );   
         m_mappings["str_hlt"] = std::move(stream2chains);
         m_mappings["gr_hlt"] = std::move( group2chains );
         m_multimappings["CC_HLT"] = std::move( chainCounter2nameMM );
         m_multimappings["HLT_L1"] = std::move( chain2itemMM );
         m_multimappings["gr_HLT"] = std::move( group2chainsMM );
         ERS_LOG("Filled map 'cc_hlt' with " << m_mappings["cc_hlt"].size() << " entries");
         ERS_LOG("Filled map 'hlt_l1' with " << m_mappings["hlt_l1"].size() << " entries");
         ERS_LOG("Filled map 'str_hlt' with " << m_mappings["str_hlt"].size() << " entries");
         ERS_LOG("Filled map 'gr_hlt' with " << m_mappings["gr_hlt"].size() << " entries");
         ERS_LOG("Filled multi-map 'CC_HLT' with " << m_multimappings["CC_HLT"].size() << " entries");
         ERS_LOG("Filled multi-map 'HLT_L1' with " << m_multimappings["HLT_L1"].size() << " entries");
         ERS_LOG("Filled multi-map 'gr_HLT' with " << m_multimappings["gr_HLT"].size() << " entries");

         Mapping stream2type;
         MultiMapping stream2typeMM;
         for( auto & stream : hltmenu.get_child("streams") ) {
            const std::string & name = stream.first;
            const std::string & type = stream.second.get<string>("type");
            stream2type[ name ].push_back(type);
            stream2typeMM.insert( { name, type } );
         }
         m_mappings["str_type"] = std::move( stream2type );
         m_multimappings["str_TYPE"] = std::move( stream2typeMM );
         ERS_LOG("Filled map 'str_type' with " << m_mappings["str_type"].size() << " entries");
         ERS_LOG("Filled multi-map 'str_TYPE' with " << m_multimappings["str_TYPE"].size() << " entries");

         // stream prescales map not yet defined in the HLT
         // this will be a placeholder
         for( auto & stream : hltmenu.get_child("streams") ) {
            m_streamPrescales[ stream.first ] = MapStrInt();
         }
      }
      catch(std::exception & ex) {
         confadapter_imp::MenuLoadIssue issue(ERS_HERE, "HLT Menu", ex.what());
         ers::error(issue);
         throw issue;
      }
   }

   if(hltpsk!=0) {
      // fills map hlt_ps and multi-map HLT_PS
      try {
         auto hltps = loadKey( session, qm["hltps"], hltpsk);
         Mapping chain2prescale;
         MultiMapping chain2prescaleMM;
         for( auto & ps : hltps.get_child("prescales") ) {
            const std::string & chainName = ps.first;
            float prescale = ps.second.get<float>("prescale");
            bool enabled = ps.second.get<bool>("enabled");
            if(enabled && prescale>0) {
               prescale = abs(prescale);
            } else {
               prescale = -abs(prescale);
            }
            string prescaleStr = to_string(prescale);
            chain2prescale[ chainName ].push_back( prescaleStr );
            chain2prescaleMM.insert( { chainName, prescaleStr } );
         }
         m_mappings["hlt_ps"] = std::move( chain2prescale );
         m_multimappings["HLT_PS"] = std::move( chain2prescaleMM );
         ERS_LOG("Filled map 'hlt_ps' with " << m_mappings["hlt_ps"].size() << " entries");
         ERS_LOG("Filled multi-map 'HLT_PS' with " << m_multimappings["HLT_PS"].size() << " entries");
      }
      catch(std::exception & ex)
      {
         confadapter_imp::MenuLoadIssue issue(ERS_HERE, "HLT PS", ex.what());
         ers::error(issue);
         throw issue;
      }
   }

   return true;
}


/***********************
 **
 **  Run 2 DB loading (will become obsolete soon)
 **
 ***********************/

// Get a list of available smks (for testing only)
bool
TriggerConfigAccessImpl::get_available_smks(coral::ISessionProxy* session)
{
   cout << "TriggerMenuHelpers: getting smk list " << std::endl;
   if(session->transaction().isActive()) { // the previous transaction was not closed. Perhaps an exception?
      session->transaction().rollback(); // we should not commmit the leftovers
   }
   session->transaction().start(true);
   coral::IQuery* smkQuery = session->nominalSchema().newQuery();
   smkQuery->addToTableList ( "SUPER_MASTER_TABLE",    "SM"    );
   coral::AttributeList bindList;
   bindList.extend<int>("min_smk"); 
   bindList[0].data<int>() = 0;
   string theCondition = "";
   theCondition += string( " SM.SMT_ID     > :min_smk");
   smkQuery->setCondition( theCondition, bindList );
   coral::AttributeList attList;
   attList.extend<int>( "SM.SMT_ID" );
   attList.extend<string>( "SM.SMT_NAME" );
   attList.extend<int> ( "SM.SMT_VERSION" );
   attList.extend<string> ( "SM.SMT_COMMENT" );
   attList.extend<string> ( "SM.SMT_ORIGIN" );
   attList.extend<int> ( "SM.SMT_L1_MASTER_TABLE_ID" );
   attList.extend<int> ( "SM.SMT_HLT_MASTER_TABLE_ID" );
   attList.extend<int> ( "SM.SMT_STATUS" );
   smkQuery->defineOutput(attList);
   smkQuery->addToOutputList( "SM.SMT_ID" );
   smkQuery->addToOutputList( "SM.SMT_NAME" );
   smkQuery->addToOutputList( "SM.SMT_VERSION" );
   smkQuery->addToOutputList( "SM.SMT_COMMENT" );
   smkQuery->addToOutputList( "SM.SMT_ORIGIN" );
   smkQuery->addToOutputList( "SM.SMT_L1_MASTER_TABLE_ID" );
   smkQuery->addToOutputList( "SM.SMT_HLT_MASTER_TABLE_ID" );
   smkQuery->addToOutputList( "SM.SMT_STATUS" );
   std::cout << "smk query is built, executing " << std::endl;
   coral::ICursor& smkCursor = smkQuery->execute();
   // loop over output
   while (smkCursor.next()) {
      const coral::AttributeList& row = smkCursor.currentRow();
      int smk            = row["SM.SMT_ID"].data<int>();
      string name        = row[ "SM.SMT_NAME" ].data<string>();
      int version        = row[ "SM.SMT_VERSION" ].data<int>();
      string comment     = row[ "SM.SMT_COMMENT" ].data<string>();
      string origin      = row[ "SM.SMT_ORIGIN" ].data<string>();
      int l1_id          = row[ "SM.SMT_L1_MASTER_TABLE_ID" ].data<int>();
      int hlt_id         = row[ "SM.SMT_HLT_MASTER_TABLE_ID" ].data<int>();
      int status         = row[ "SM.SMT_STATUS" ].data<int>();
      std::cout << "SMK: " << smk << std::endl;
      std::cout << "SM.SMT_NAME: " << name << std::endl;
      std::cout << "SM.SMT_VERSION: " << version << std::endl;
      std::cout << "SM.SMT_COMMENT: " << comment << std::endl;
      std::cout << "SM.SMT_ORIGIN: " << origin << std::endl;
      std::cout << "SM.SMT_L1_MASTER_TABLE_ID: " << l1_id << std::endl;
      std::cout << "SM.SMT_HLT_MASTER_TABLE_ID: " << hlt_id << std::endl;
      std::cout << "SM.SMT_STATUS: " << status << std::endl;
   }
   return true;
}

// This replaces run_query revised for run 2. (28.08.2014)
bool
TriggerConfigAccessImpl::run_query_Run2 ( coral::ISessionProxy* session, 
                                          int smk, int hltk )
{  
   cerr << "TriggerMenuHelpers: running query smk "<<smk<< endl;
   if(session->transaction().isActive()) { // the previous transaction was not closed. Perhaps an exception?
      session->transaction().rollback(); // we should not commmit the leftovers
   }
   session->transaction().start(true);  
   coral::AttributeList bindList;
   bindList.extend<int>("smid"); 
   bindList.extend<int>("psid"); 
   bindList[0].data<int>() = smk;
   bindList[1].data<int>() = hltk;
   coral::IQuery* chainQuery = session->nominalSchema().newQuery();
   chainQuery->addToTableList ( "SUPER_MASTER_TABLE",    "SM"    );
   chainQuery->addToTableList ( "HLT_MASTER_TABLE",      "HM"    );
   chainQuery->addToTableList ( "HLT_PRESCALE_SET",      "PS"    );
   chainQuery->addToTableList ( "HLT_TRIGGER_MENU",      "TM"    );
   chainQuery->addToTableList ( "HLT_TM_TO_TC",          "TM2TC" );
   chainQuery->addToTableList ( "HLT_TRIGGER_CHAIN",     "TC"    );
   chainQuery->addToTableList ( "HLT_TC_TO_TR",          "TC2TR" );
   chainQuery->addToTableList ( "HLT_PRESCALE",          "PR"    );
   chainQuery->addToTableList ( "HLT_TRIGGER_STREAM",    "TR"    );
   chainQuery->addToTableList ( "HLT_TRIGGER_GROUP",     "TG"    );
#ifdef POISON
   chainQuery->addToTableList ( "HLT_TRIGGER_TYPE",      "TT"    );
#endif
 
   string theCondition = "";
   theCondition += string( " SM.SMT_ID     = :smid");
   theCondition += string( " AND HM.HMT_ID = SM.SMT_HLT_MASTER_TABLE_ID"        );
   theCondition += string( " AND PS.HPS_ID = :psid");
   theCondition += string( " AND TM.HTM_ID = HM.HMT_TRIGGER_MENU_ID"            );
   theCondition += string( " AND TM.HTM_ID = TM2TC.HTM2TC_TRIGGER_MENU_ID "     );
   theCondition += string( " AND TC.HTC_ID = TM2TC.HTM2TC_TRIGGER_CHAIN_ID"     );
   // Chains with negative counter shouldn't be loaded
   theCondition += string( " AND TC.HTC_CHAIN_COUNTER >= 0"); 
   theCondition += string( " AND PR.HPR_CHAIN_COUNTER = TC.HTC_CHAIN_COUNTER"   );
   theCondition += string( " AND PS.HPS_ID = PR.HPR_PRESCALE_SET_ID"            );
   // the trigger stream
   theCondition += string( " AND TC.HTC_ID = TC2TR.HTC2TR_TRIGGER_CHAIN_ID"     );
   theCondition += string( " AND TR.HTR_ID = TC2TR.HTC2TR_TRIGGER_STREAM_ID"    );
   theCondition += string( " AND TG.HTG_TRIGGER_CHAIN_ID = TC.HTC_ID"); 
#ifdef POISON
   theCondition += string( " AND TC.HTC_ID = TT.HTT_TRIGGER_CHAIN_ID"           );
#endif
   chainQuery->setCondition( theCondition, bindList );
   //Output data and types
   coral::AttributeList attList;
   attList.extend<int>( "SM.SMT_ID" ); // for debug
   attList.extend<int>   ( "PS.HPS_ID"                            );
   attList.extend<string>( "PS.HPS_NAME"                          );
   attList.extend<int>   ( "PS.HPS_VERSION"                       );
   attList.extend<int>   ( "PR.HPR_VALUE"                         );
   attList.extend<string>( "PR.HPR_TYPE"                          );
   attList.extend<string>( "PR.HPR_CONDITION"                     );
   attList.extend<string>( "TC2TR.HTC2TR_TRIGGER_STREAM_PRESCALE" );
   attList.extend<string>( "TR.HTR_NAME"                          );
   attList.extend<string>( "TR.HTR_DESCRIPTION"                   );
   attList.extend<string>( "TR.HTR_TYPE"                          );
   attList.extend<int>   ( "TR.HTR_OBEYLB"                        );
   attList.extend<string>( "TG.HTG_NAME"                          );
   attList.extend<long>  ( "TC.HTC_ID"                            );
   attList.extend<string>( "TC.HTC_NAME"                          );
   attList.extend<int>   ( "TC.HTC_CHAIN_COUNTER"                 );
   attList.extend<string>( "TC.HTC_LOWER_CHAIN_NAME"              );
#ifdef POISON
   attList.extend<int>   ( "TT.HTT_TYPEBIT"                       );
   //attList.extend<string>( "TC.HTC_L2_OR_EF"                      );
   //attList.extend<string>( "TC.HTC_RERUN_PRESCALE"                );
   //attList.extend<string>( "PR.HPR_PASS_THROUGH_RATE"             );
   //attList.extend<string>( "PR.HPR_PRESCALE"                      );
#endif
   chainQuery->defineOutput(attList);
   chainQuery->addToOutputList( "SM.SMT_ID" ); // for debug
   chainQuery->addToOutputList( "PS.HPS_ID"                            );
   chainQuery->addToOutputList( "PS.HPS_NAME"                          );
   chainQuery->addToOutputList( "PS.HPS_VERSION"                       );
   chainQuery->addToOutputList( "PR.HPR_VALUE"                         );
   chainQuery->addToOutputList( "PR.HPR_TYPE"                          );
   chainQuery->addToOutputList( "PR.HPR_CONDITION"                     );
   chainQuery->addToOutputList( "TC2TR.HTC2TR_TRIGGER_STREAM_PRESCALE" );
   chainQuery->addToOutputList( "TR.HTR_NAME"                          );
   chainQuery->addToOutputList( "TR.HTR_DESCRIPTION"                   );
   chainQuery->addToOutputList( "TR.HTR_TYPE"                          );
   chainQuery->addToOutputList( "TR.HTR_OBEYLB"                        );
   chainQuery->addToOutputList( "TG.HTG_NAME"                          );
   chainQuery->addToOutputList( "TC.HTC_ID"                            );
   chainQuery->addToOutputList( "TC.HTC_NAME"                          );
   chainQuery->addToOutputList( "TC.HTC_CHAIN_COUNTER"                 );
   chainQuery->addToOutputList( "TC.HTC_LOWER_CHAIN_NAME"              );
#ifdef POISON
   chainQuery->addToOutputList( "TT.HTT_TYPEBIT"                       );
#endif
   cout << "query is built, executing " << endl;
   coral::ICursor& chainCursor = chainQuery->execute();
   // loop over output
   unsigned int lineCount(0);
   while (chainCursor.next()) {
      lineCount++;
      const coral::AttributeList& row = chainCursor.currentRow();
      std::string level = "HL";
      std::string chain_name       = notilda(row["TC.HTC_NAME"].data<string>());
      std::string lower_chain_name = notilda(row["TC.HTC_LOWER_CHAIN_NAME"].data<string>());
      std::string stream           = notilda(row["TR.HTR_NAME"].data<string>());
      std::string stream_type      = notilda(row["TR.HTR_TYPE"].data<string>());
      std::string group            = notilda(row["TG.HTG_NAME"].data<string>());
      std::string info_type        = notilda(row["PR.HPR_TYPE"].data<string>());
      std::string ps_condition     = notilda(row["PR.HPR_CONDITION"].data<string>());
      std::string prescale_str = "NA";
      if(info_type == "Prescale") {
         int prescale_int = row["PR.HPR_VALUE"].data<int>();
         stringstream pss;
         pss << prescale_int;
         pss >> prescale_str;
      }
      int chain_counter_int        = row["TC.HTC_CHAIN_COUNTER"].data<int>();
      stringstream ss;
      ss << chain_counter_int;
      std::string chain_counter;
      ss >> chain_counter;
      if(lower_chain_name=="") lower_chain_name="EMPTY";
      std::vector<std::string> &temp2 = m_mappings["str_hlt"][stream];
      if (std::find(temp2.begin(), temp2.end(), chain_name) == temp2.end()) {
         m_mappings["str_hlt"][stream].push_back(chain_name);
         m_multimappings["str_HLT"].insert(M_Pair(stream,chain_name));
         if(m_mappings["str_type"].find(stream) == m_mappings["str_type"].end() ) {
            m_mappings["str_type"][stream].push_back(stream_type);
            m_multimappings["str_TYPE"].insert(M_Pair(stream,stream_type));	
         }
      }
      if(info_type == "Stream") {
         float stream_prescale = float(row["PR.HPR_VALUE"].data<int>()); // a float for historical reasons
         insertStreamPrescale(stream, chain_name, stream_prescale ); 
      }
      std::vector<std::string> &temp3 = m_mappings["gr_hlt"][group];
      if (std::find(temp3.begin(), temp3.end(), chain_name) == temp3.end()) {
         m_mappings["gr_hlt"][group].push_back(chain_name);
         m_multimappings["gr_HLT"].insert(M_Pair(group,chain_name));
      }
      if(m_mappings["hlt_l1"].find(chain_name) == m_mappings["hlt_l1"].end()) {
         m_mappings["hlt_l1"][chain_name].push_back(lower_chain_name);
         m_multimappings["HLT_L1"].insert(M_Pair(lower_chain_name,chain_name));
      }
      Mapping::const_iterator i_ps = m_mappings["hlt_ps"].find(chain_name);
      if( (i_ps == m_mappings["hlt_ps"].end()) || m_mappings["hlt_ps"][chain_name][0] == "NA") {
         m_mappings["hlt_ps"][chain_name].push_back(prescale_str);
         m_multimappings["HLT_PS"].insert(M_Pair(chain_name,prescale_str));
      }
      if(m_mappings["cc_hlt"].find(chain_counter) == m_mappings["cc_hlt"].end()) {
         m_mappings["cc_hlt"][chain_counter].push_back(chain_name);
         m_multimappings["CC_HLT"].insert(M_Pair(chain_counter,chain_name));
      
      }
   }
   cerr << "Query resulted in " << lineCount << " lines" << endl;
   return lineCount>0;
}


// This is for getting the stream prescales. (assumed to be called after run_query)
// I changed it to only look for express stream prescales since no others are expected 27/08/2014 MM

bool
TriggerConfigAccessImpl::run_query_ps_Run2 ( coral::ISessionProxy* session, int hltk )
{
   const Mapping & str_hlt = getMapping("str_hlt");
   const Mapping & cc_hlt = getMapping("cc_hlt");
   try {
      cout << "running query_ps " << endl;
      if(session->transaction().isActive()) { // the previous transaction was not closed. Perhaps an exception?
         session->transaction().rollback(); // we should not commmit the leftovers
      }   
      session->transaction().start(true);
      coral::IQuery* query = session->nominalSchema().newQuery();
      //Set the tables that are used
      query->addToTableList ( "HLT_PRESCALE_SET",  "PS" );
      query->addToTableList ( "HLT_PRESCALE",      "PR" );
      query->addToTableList ( "HLT_TRIGGER_CHAIN", "TC" );    
      //Bind list
      coral::AttributeList bindList;
      bindList.extend<int>("psid"); 
      bindList[0].data<int>() = hltk;
      bindList.extend<string>("L2"); 
      bindList[1].data<string>() = "L2"; 
      bindList.extend<string>("exp"); 
      bindList[2].data<string>() = "express"; 
      string theCondition = "";
      theCondition += string( " PS.HPS_ID = :psid" );
      theCondition += string( " AND PS.HPS_ID = PR.HPR_PRESCALE_SET_ID" );
      theCondition += string( " AND PR.HPR_CHAIN_COUNTER = TC.HTC_CHAIN_COUNTER"   );
      theCondition += string( " AND PR.HPR_L2_OR_EF = :exp" );
      theCondition += string( " AND TC.HTC_L2_OR_EF != :L2" );     
      query->setCondition( theCondition, bindList );    
      //Output data and types
      coral::AttributeList attList;
      attList.extend<string>( "PS.HPS_NAME"              );
      attList.extend<int>   ( "PS.HPS_VERSION"           );
      attList.extend<string>( "PR.HPR_L2_OR_EF"          );
      attList.extend<string>( "TC.HTC_L2_OR_EF"          );
      attList.extend<int>   ( "PR.HPR_CHAIN_COUNTER"     );
      attList.extend<string>( "PR.HPR_PASS_THROUGH_RATE" );
      attList.extend<string>( "PR.HPR_PRESCALE"          );
      attList.extend<string>( "TC.HTC_NAME"              );
      query->defineOutput(attList);
      query->addToOutputList( "PS.HPS_NAME"              );
      query->addToOutputList( "PS.HPS_VERSION"           );
      query->addToOutputList( "PR.HPR_L2_OR_EF"          );
      query->addToOutputList( "TC.HTC_L2_OR_EF"          );
      query->addToOutputList( "PR.HPR_CHAIN_COUNTER"     );
      query->addToOutputList( "PR.HPR_PASS_THROUGH_RATE" );
      query->addToOutputList( "PR.HPR_PRESCALE"          );
      query->addToOutputList( "TC.HTC_NAME"              );
      // the ordering
      string theOrder = "";
      theOrder += "  PR.HPR_L2_OR_EF DESC";
      theOrder += ", PR.HPR_CHAIN_COUNTER ASC";
      query->addToOrderList( theOrder );
      query->setRowCacheSize(1000);
      query->setDistinct();
      coral::ICursor& cursor = query->execute();
      while (cursor.next()) {
         const coral::AttributeList& row = cursor.currentRow();
         string tclevel   = row["TC.HTC_L2_OR_EF"].data<string>(); // for testing
         string level   = row["PR.HPR_L2_OR_EF"].data<string>();
         string name    = row["PS.HPS_NAME"].data<string>();
         int    counter = row["PR.HPR_CHAIN_COUNTER"].data<int>();
         std::string prescaleStr = row["PR.HPR_PRESCALE"].data<string>();
         std::string chainName = row["TC.HTC_NAME"].data<string>();
         std::string passThruRate = row["PR.HPR_PASS_THROUGH_RATE"].data<string>();
         std::string chainCounter;
         stringstream ss;
         ss << counter;
         ss >> chainCounter;
         float pt = -999.;
         if(passThruRate != "na" && passThruRate != "NA")
            pt = convert2(passThruRate);
         if( lhcRun()>=2 ) {
            auto & streamsByLevel = str_hlt.at(level);
            if( find( streamsByLevel.begin(), streamsByLevel.end(), chainName) != streamsByLevel.end() &&
                cc_hlt.find(chainCounter) != cc_hlt.end() 
                && cc_hlt.at(chainCounter)[0] == chainName )
               {
                  insertStreamPrescale(level, chainName, pt);
               }
         }
      }
      delete query;
   }
   catch (coral::SchemaException& e) {
      std::cerr << "run_query_ps >> IRelationalException: " << e.what() << std::endl;
      session->transaction().rollback();
      return false;
   }
   catch (std::exception& e) {
      std::cerr << "run_query_ps >> Standard C++ exception: " << e.what() << std::endl;
      session->transaction().rollback();
      return false;
   }
   catch (...) {
      std::cerr << "run_query_ps >> unknown C++ exception" << std::endl;
      session->transaction().rollback();
      return false;
   }
   return true;
}

void
TriggerConfigAccessImpl::printPrescaleMaps(bool size_only) const
{
   if( size_only ) {
      cout << "Streams to stream prescales map has size " << m_streamPrescales.size() << endl;
   }
   for( auto & entry : m_streamPrescales ) {
      if( size_only ) {
         cout << "Stream prescale map for stream '" << entry.first << "' has size " << entry.second.size() << endl;
      } else {
         std::cout << "Stream prescales for stream: " << entry.first << std::endl;
         for ( auto & psentry : entry.second ) {
            std::cout << "        " << psentry.first << " --> " << psentry.second << std::endl;
         }
      }
   }
}

bool
TriggerConfigAccessImpl::getConfiguration(unsigned smk, unsigned hltpsk, unsigned l1psk)
{
   coral::ISessionProxy* session{nullptr};
   open_db(session);
   session->transaction().start(/*readonly=*/true);  
   bool success = run_query(session, smk, hltpsk, l1psk);
   session->transaction().commit();
   close_db(session);
   m_smk = smk;
   m_hltk = hltpsk;
   m_l1k = l1psk;
   m_lhcRun = 3;
   if(success) {
      ERS_INFO("For SMK " << smk << ", l1psk " << l1psk << ", hltpsk " << hltpsk << " loading of the information from DB and filling the internal maps succeeded");
   }
   return success;
}

bool
TriggerConfigAccessImpl::getConfigurationRun2(unsigned smk, unsigned hltk, unsigned l1k)
{
   coral::ISessionProxy* session{nullptr};
   open_db(session);
   bool success = run_query_Run2(session, smk, hltk);
   close_db(session);
   m_smk = smk;
   m_hltk = hltk;
   m_l1k = l1k;
   m_lhcRun = 2;
   return success;
}

void
TriggerConfigAccessImpl::printMapping(const Mapping& m, const string& prefix, bool size_only) const
{
   if( size_only ) {
      cout << "Map '" << prefix << "' has size " << m.size() << endl;
   } else {
      for ( auto & entry : m ) {
         cout << prefix << " " << entry.first << " -> ";
         for ( auto & v_elem : entry.second ) {
            cout << v_elem << " ";
         }
         cout << endl;
      }
   }
}

void
TriggerConfigAccessImpl::printMultiMapping(const MultiMapping& m, const string& prefix, bool size_only) const
{
   if( size_only ) {
      cout << "Map '" << prefix << "' has size " << m.size() << endl;
   } else {
      for ( auto & entry : m ) {
         cout << prefix << entry.first << " -> " << entry.second << endl;
      }
   }
}

void
TriggerConfigAccessImpl::printAllMaps(bool size_only) const
{
   static const  std::map<std::string, std::string> prefix { 
      { "str_hlt",  "stream hlt"  }, 
      { "str_type", "stream type" }, 
      { "gr_hlt",   "group hlt"   }, 
      { "hlt_l1",   "hlt l1"      }, 
      { "hlt_ps",   "hlt ps"      }, 
      { "cc_hlt",   "hlt chain"   }
   };
   std::cout << "Printing maps" << std::endl;
   cout << "The following maps are filled: ";
   for( auto & entry : m_mappings ) {
      cout << entry.first << ", ";
   }
   cout << endl;
   for( auto & entry : m_mappings ) {
      printMapping( entry.second, prefix.at(entry.first), size_only );
   }
   printPrescaleMaps(size_only);
}

void
TriggerConfigAccessImpl::Mapping2Branch( const MultiMapping & m,
                                         std::vector<Branch> & vecBranch) const
{
   // get time, run and lumiblock
   OWLTime o_time;
   trp_utils::GetTime(o_time);
   unsigned int runNr {0};
   unsigned short lbNr {0};
   trp_utils::GetRunParams(m_partitionName, runNr, lbNr);

   // build config
   Branch *mBranch = nullptr;
   int n_p = 0;
   for( auto & entry : m ) {
      std::string key_b = entry.first;
      std::string val_b = entry.second;
      int num_pu = m.count(key_b); // number of pu for that 
      if(n_p==0){  // first element of branch
         mBranch = new Branch();
         mBranch->EraseCont();
      }
      mBranch->Values.push_back(val_b);
      mBranch->Key=key_b;
      mBranch->RunNumber = runNr;
      mBranch->LumiBlock = lbNr;
      mBranch->TimeStamp = o_time;
      mBranch->ConfigItem = std::vector<std::string>{ to_string(m_smk), to_string(m_l1k), to_string(m_hltk) }; // vector of the 3 keys
    
      if(num_pu==(n_p+1)){ // last element of branch
         vecBranch.push_back(*mBranch);
         mBranch->EraseCont();
         n_p=-1;
      }
      ++n_p;
   }// end loop on Branch
}
    

// converts map to vector of trees
void
TriggerConfigAccessImpl::Mapping2Tree( const MultiMapping & mMapTree, 
                                       const MultiMapping & mMapBranch,
                                       std::vector<Tree>& mTreeVec) const
{

   mTreeVec.clear(); // this is the output

   // find time, run and lumiblock
   OWLTime o_time{};
   trp_utils::GetTime(o_time);
   unsigned int runNr{0};
   unsigned short lbNr{0};
   trp_utils::GetRunParams( m_partitionName, runNr, lbNr);
  
   MultiMapping::const_iterator mMap_it1,mMap_it2;

   Branch *mBranch = nullptr;
   Tree *mTree = nullptr;
   std::vector<Branch> mVecBranch;
   int n_p=0;
   int n_ss = 0;
   for( auto & treeMapEntry : mMapTree) { // loop on Tree
      std::string key_t = treeMapEntry.first;
      std::string val_t = treeMapEntry.second;
      if(n_ss==0) { // starting of a a new Tree
         mTree = new Tree();
         mTree->EraseCont();
      }
      int num_subseg = mMapTree.count(key_t); // number of subsegments for that SV
    
      for( auto & branchMapEntry : mMapBranch) { // loop on Branch
         std::string key_b = branchMapEntry.first;
         std::string val_b = branchMapEntry.second;
         int num_pu = mMapBranch.count(key_b); // number of pu for that 
         if(val_t!=key_b) {
            continue;
         }
         if(n_p==0){  // first element of branch
            mBranch = new Branch();
            mBranch->EraseCont();
         }
         (mBranch->Values).push_back(val_b);
         mBranch->Key=key_b;
         mBranch->RunNumber = runNr;
         mBranch->LumiBlock = lbNr;
         mBranch->TimeStamp = o_time;
         std::string s_smk  = trp_utils::convertToString(m_smk);
         std::string s_l1k  = trp_utils::convertToString(m_l1k);
         std::string s_hltk = trp_utils::convertToString(m_hltk);
      
         std::vector<std::string> trig_keys;
         trig_keys.push_back(s_smk);
         trig_keys.push_back(s_l1k);
         trig_keys.push_back(s_hltk);
    
         (mBranch->ConfigItem) = trig_keys;

      
         if(num_pu==(n_p+1)){ // last element of branch
            //std::cout << "last item in branch" << std::endl;
            //(mBranch.Values).push_back(val_b); 
            mVecBranch.push_back(*mBranch);
            //(mTree.branchlist).push_back(mBranch);
            mTree->branchlist=mVecBranch;
            //mBranch.EraseCont();
            n_p=-1;
         } 
         if(!((n_p==0)||(num_pu==(n_p+1)))){ // in the middle
            //std::cout << "In the middle of branch" << std::endl;
            //(mBranch.Values).push_back(val_b);
         }
         ++n_p;
         //      std::cout << "Branch iteration:" << n_p << std::endl;
      }// end loop on Branch
      // --------------------------------------------
      if(num_subseg==(n_ss+1)){ // last element of tree
         // (mTree.Values).push_back(val_t);
         mTree->Key=key_t;
         mTree->RunNumber = runNr;
         mTree->LumiBlock = lbNr;
         mTree->TimeStamp = o_time;

         std::string s_smk  = trp_utils::convertToString(m_smk);
         std::string s_l1k  = trp_utils::convertToString(m_l1k);
         std::string s_hltk = trp_utils::convertToString(m_hltk);
      
         std::vector<std::string> trig_keys;
         trig_keys.push_back(s_smk);
         trig_keys.push_back(s_l1k);
         trig_keys.push_back(s_hltk);
    
         (mTree->ConfigItem) = trig_keys;

         mTreeVec.push_back(*mTree);
         n_ss=-1;
      } 
      ++n_ss;
   } // end loop on Tree  
}

void
TriggerConfigAccessImpl::PublishBranch( const std::string & server_info,  
				        const std::vector<Branch> & my_branch){
  ISInfoDictionary dict( m_partition );
  std::cout << "PublishBranch in:" << server_info << std::endl; 
  std::cout << "Size:" << my_branch.size() << std::endl;
  int it=0;
  for( auto & branch : my_branch ) {
    std::string str_add2 = server_info + "_" + to_string(it);
    try {
      dict.insert(str_add2, branch);
    } catch( daq::is::InfoAlreadyExist & ) {
      dict.update(str_add2, branch, true);
    }
    ++it; 
  } 
}


void
TriggerConfigAccessImpl::PublishTree( const std::string & server_info,  
				      const std::vector<Tree> & my_tree)
{
   ISInfoDictionary dict( m_partition );
   std::cout << "PublishTree in:" << server_info << std::endl;
   std::cout << "Size:" << my_tree.size() << std::endl;
   int it=0;
   for ( auto & tree : my_tree ) {
      std::string str_add2 = server_info + "_" + to_string(it++);
      try {
         dict.insert(str_add2, tree);
      } catch( daq::is::InfoAlreadyExist & ) {
         dict.update(str_add2, tree, true);
      }
   }  
}

void
TriggerConfigAccessImpl::EraseInfo( const std::string & server_info, const std::string & rm_string)
{
   ISInfoDictionary dict( m_partition );
   std::string str_add2 = server_info;
   //  ISInfoIterator ii(m_partition, std::string(server_info) , Branch::type());
   std::cout << "Erase " <<  server_info.c_str() << "and" << rm_string << std::endl;
   //  ISInfoIterator ii(m_partition, server_info, Branch::type() && rm_string);
   //  while(ii())
   //    ii.remove();
   //  ISInfoIterator ii2(m_partition, server_info, Tree::type() && rm_string);
   //  while(ii2())
   //    ii2.remove();
}
