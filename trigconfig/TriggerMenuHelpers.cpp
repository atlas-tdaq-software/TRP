/* Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration */

#include "TriggerConfigAccessImpl.h"
#include "TRP/TriggerMenuHelpers.h"

using namespace confadapter_imp;

TriggerMenuHelpers::TriggerMenuHelpers(const std::string & dbAlias)
   : m_config( std::make_unique<TriggerConfigAccessImpl>(dbAlias) )
{}

TriggerMenuHelpers::TriggerMenuHelpers(const std::string & partitionName, const std::string & dbAlias)
   : m_config( std::make_unique<TriggerConfigAccessImpl>(partitionName, dbAlias) ) 
{}

TriggerMenuHelpers::~TriggerMenuHelpers() = default;

unsigned int
TriggerMenuHelpers::lhcRun() const
{ return m_config->lhcRun(); }

bool
TriggerMenuHelpers::getMapping(Mapping& m, const std::string& name) const
{ return m_config->getMapping(m, name); }

const TriggerMenuHelpers::Mapping &
TriggerMenuHelpers::getMapping(const std::string& name) const
{ return m_config->getMapping(name); }

bool
TriggerMenuHelpers::getMultiMapping(MultiMapping& m, const std::string& name) const
{ return m_config->getMultiMapping(m, name); }

const TriggerMenuHelpers::MultiMapping &
TriggerMenuHelpers::getMultiMapping(const std::string& name) const
{ return m_config->getMultiMapping(name); }

bool
TriggerMenuHelpers::getStreamPrescalesMap(MapStrInt &streamPrescales, const std::string& stream ) const
{ return m_config->getStreamPrescalesMap(streamPrescales, stream); }

const TriggerMenuHelpers::MapStrInt &
TriggerMenuHelpers::getStreamPrescalesMap(const std::string& stream ) const
{ return m_config->getStreamPrescalesMap( stream ); }

bool
TriggerMenuHelpers::getConfiguration(unsigned smk, unsigned hltpsk, unsigned l1psk)
{ return m_config->getConfiguration(smk, hltpsk, l1psk); }

bool
TriggerMenuHelpers::getConfigurationRun2(unsigned smk, unsigned hltk, unsigned l1k)
{ return m_config->getConfigurationRun2(smk, hltk, l1k); }

void
TriggerMenuHelpers::printMapping(const Mapping& m, const std::string& prefix, bool size_only) const
{ m_config->printMapping(m, prefix, size_only); }

void
TriggerMenuHelpers::printMultiMapping(const MultiMapping& m, const std::string& prefix, bool size_only) const
{ m_config->printMultiMapping(m, prefix, size_only); }

void
TriggerMenuHelpers::printAllMaps(bool size_only) const
{ m_config->printAllMaps(size_only); }

void
TriggerMenuHelpers::Mapping2Branch( const MultiMapping & m, std::vector<Branch> & vecBranch) const
{ m_config->Mapping2Branch(m, vecBranch); }

void
TriggerMenuHelpers::Mapping2Tree( const MultiMapping & mMapTree, const MultiMapping & mMapBranch, std::vector<Tree>& mTreeVec) const
{ m_config->Mapping2Tree( mMapTree, mMapBranch, mTreeVec); }

void
TriggerMenuHelpers::PublishBranch( const std::string & server_info, const std::vector<Branch> & my_branch)
{ m_config->PublishBranch( server_info, my_branch ); }

void
TriggerMenuHelpers::PublishTree( const std::string & server_info, const std::vector<Tree> & my_tree)
{ m_config->PublishTree(server_info,  my_tree); }

void
TriggerMenuHelpers::EraseInfo( const std::string & server_info, const std::string & rm_string)
{ m_config->EraseInfo(server_info, rm_string); }
