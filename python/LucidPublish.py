#!/usr/bin/env tdaq_python

#
# Sum all numerical attributes of a given type and republish
# the result.
# 


import sys
from ispy import *
from ispy.InfoReader import *
from optparse import OptionParser
from time import sleep
from time import localtime
import signal
import re
import os

####################################################
# This function get an ISInfoAny Object and return a dictionary
# with all the key value find in the object

def ISInfoReaderObjectUnpack(r):
   rr={}
   for k,v in r.iteritems():
      dd={}
      dd=ISInfoAnyUnpack(v)
      rr[k]=dd
   return rr

def ISInfoAnyUnpack(v):
   dd={}
   att=(v.__str__()).split('\n')
   for t in att:
      pp= t.split(':')
      if len(pp)>1:
         mykey=pp[0].strip()
         myval=pp[1].strip()
         #dd[pp[0].strip()]=pp[1].strip()
         # check if this is dictionary like string
         is_dict=re.compile("\[.*\]",re.M & re.IGNORECASE)
         result=is_dict.match(myval)
         if (result):
            dd[mykey]=SplitString(myval)
         else:
            dd[mykey]=myval

   return dd

#to dump the value in the array you can use
def ISInfoAnyDump(d):
   for k in d.keys():
      print "KEY:%s: VALUE:%s:" % (k,d[k])

def SplitString(s):
   s=re.sub('(\]|\[)','',s)
   s=re.sub(',','',s)
   s=re.sub('\'','',s)
   s=s.split()
   return s

#################################################################33




def handler(signum, f_globals):
   global run
   print 'Signal handler called with signal', signum
   run = False

def get_val(it, my_name):
   any = ISInfoAny()    
   it.value(any)
   any._update(it.name(), myp)
   rate = any.__getattribute__(my_name)
   return rate


def get_RunLB(part):
   run_lb = []
   reader=InfoReader(part, "RunParams","LuminosityInfo")
   reader.update()
#   print reader.objects
   x = reader.objects['RunParams.LuminosityInfo']
   run_num =  x.__getattribute__('RunNumber')
   lb_num =  x.__getattribute__('LumiBlockNumber')
#   print run_num, "   ", lb_num
   run_lb.append(run_num)
   run_lb.append(lb_num)
#   print "RunLB: ", run_lb
   return  run_lb


   

def convert2tp(my_tp,my_dict,Xlab,Ylab):
   id = 0
   my_data = []
   for xid in Xlab:
      for yid in Ylab:
         lab_str = xid + ' ' + yid
         my_data.append(my_dict [lab_str])
         
   my_tp.Data = my_data
   my_tp.XLabels = Xlab
   my_tp.YLabels = Ylab
   return


parser = OptionParser()
parser.add_option('-p', '--partition',
                  dest='partitionName', type='string',
                  help='Pass the partition name')
parser.add_option('-t', '--time',
                  dest='refresh', type='int',
                   help='Update Time')

parser.set_defaults(partitionName = 'ATLAS', refresh = 10)
(options, args) = parser.parse_args()

myp = IPCPartition(options.partitionName)
if not myp.isValid():
        print "Partition name is not valid"
        sys.exit(1)


global run
global my_time
run = True


if not myp.isValid():
   print "Partition:", myp.name(),"is not valid"
   sys.exit(1)

signal.signal(signal.SIGTERM, handler)

xlab = ['LucidA','LucidC','LucidAC' , 'Lumat']
ylab = ['Rate','Acc']





while (1):
   get_RunLB(myp)
#create the dictionary
   tp_dict = {}
   isc_rate = ISCriteria('L1CT.CTPIN8')
   it_rate = ISInfoIterator(myp, 'L1TriggerRates', isc_rate)
   while it_rate.next():
      rate_all = get_val(it_rate, 'rateValues2')
      my_time = it_rate.time()
      if len(rate_all) == 0 :
         run = False
      else:
         tp_dict ['LucidA Rate']= rate_all[4]/6.
         tp_dict ['LucidC Rate']= rate_all[5]/6.
         tp_dict ['LucidAC Rate']= rate_all[22]
         tp_dict ['Lumat Rate']= rate_all[32]

         
   isc_acc = ISCriteria('ISCTPIN.8')
   it_acc = ISInfoIterator(myp, 'L1CT', isc_acc)
   while it_acc.next():
      rate_acc = get_val(it_acc, 'counterValues2')
      if (run and len(rate_acc)==0) :
         run = False
      else:
         tp_dict ['LucidA Acc']= rate_acc[4]/6.
         tp_dict ['LucidC Acc']= rate_acc[5]/6.
         tp_dict ['LucidAC Acc']= rate_acc[22]
         tp_dict ['Lumat Acc']= rate_acc[32]

         
   if run:
#      print tp_dict
      my_tp = ISObject(myp, 'ISS_TRP.LU_Rate_Lucid', 'TimePoint_IS')
      convert2tp(my_tp, tp_dict,xlab,ylab)
      my_tp.TimeStamp = my_time
      my_tp.checkin()

   sleep(options.refresh)
   

print 'Done'




