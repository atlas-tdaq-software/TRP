function showChildTypes(n) // Only used for debugging
{
  var typeString = "";
  typeString += n + ", type: " + n.nodeType + ", name: " + n.nodeName + ", parent: " + n.parentNode + "\n";
  var children = n.childNodes;
  for(var i = 0; i < children.length; i++) {
    typeString += showChildTypes(children[i]);
  }
  return typeString;
}



/* 
   _importNode, adapted from a posting by Anthony Holdener
   http://www.alistapart.com/articles/crossbrowserscripting
   Needed since Safari's importNode doesn't support deep copying 
*/
    
    if (!document.ELEMENT_NODE) {
	document.ELEMENT_NODE = 1;
	document.ATTRIBUTE_NODE = 2;
	document.TEXT_NODE = 3;
	document.CDATA_SECTION_NODE = 4;
	document.ENTITY_REFERENCE_NODE = 5;
	document.ENTITY_NODE = 6;
	document.PROCESSING_INSTRUCTION_NODE = 7;
	document.COMMENT_NODE = 8;
	document.DOCUMENT_NODE = 9;
	document.DOCUMENT_TYPE_NODE = 10;
	document.DOCUMENT_FRAGMENT_NODE = 11;
	document.NOTATION_NODE = 12;
    }

document._importNode = function(node, allChildren) {
    
    switch (node.nodeType) {
	
    case document.ELEMENT_NODE:
	
	var newNode = document.createElement(node.nodeName);
	
	/* does the node have any attributes to add? */
	if (node.attributes && node.attributes.length > 0) {
	    for (var i = 0; i < node.attributes.length; i++) {
		newNode.setAttribute(node.attributes[i].nodeName, 
				     node.getAttribute(node.attributes[i].nodeName));
	    }
	}
	/* are we going after children too, and does the node have any? */
	if (allChildren && node.childNodes && node.childNodes.length > 0) {
	    for (var iChild = 0; iChild < node.childNodes.length; iChild++) {
		newNode.appendChild(document._importNode(node.childNodes[iChild], allChildren));
	    }
	}
	return newNode;
	
    case document.TEXT_NODE:
    case document.CDATA_SECTION_NODE:
    case document.COMMENT_NODE:
	return document.createTextNode(node.nodeValue);
	
    }
};

document._lastColumnSorted = -1;

document.headerRow;
document.dataRows = [];

function bootstrap(wtrp_id) // wtrp_id refers to a parameter imbedded in the first cell of the target table
{
    
    try {
	
	
	var typeString = "";
	/*
      Use the tagName property to identify element types. Assume the interesting stuff
      is in a "CENTER" element. Loop over all top level nodes in the body.
      Check the children for the button which was pressed (named <wtrp_id>)
      assume the next table in the node list is the one to extract. 
      This is rather fragile code. A change in page format could easily break it.
      (I had a better way before but Safari doesn't like it.)
    */
	
	var opTable = "";
	
	var theBody = window.opener.document.body;
	
	
	for(var inode = 0; inode < theBody.childNodes.length; inode++) {
	    var theBodyChild = theBody.childNodes[inode];
	    
	    if( theBodyChild.nodeType != document.ELEMENT_NODE) {
		continue;
	    }
	    if (theBodyChild.tagName === "CENTER" ) {
		
		for(var jnode = 0; jnode < theBodyChild.childNodes.length; jnode++) {
		    
		    var theSpanChild = theBodyChild.childNodes[jnode];
		    
		    if( theSpanChild.tagName === "BUTTON" && theSpanChild.name === wtrp_id) {
			// Found the right button, the next table on the node list is the one to extract
			
			for(var knode = jnode+1; knode < theBodyChild.childNodes.length; knode++) {
			    var theNextChild = theBodyChild.childNodes[knode];
			    
			    if(theBodyChild.childNodes[knode].tagName === "TABLE" ) {
				opTable = theBodyChild.childNodes[knode];
			    }
			}
		    }
		}
	    }
	}
	
	// find and embed the target table
	
	if(opTable === "") {
	    alert("error: can't find a table with id " + wtrp_id);
	    return;
	}
	
	if(navigator.appName === "Netscape") { // the native version is faster, use it for Netscape
	    document.importedTable = document.importNode(opTable, true);
	} else {
	    document.importedTable = document._importNode(opTable, true);
	}

	// document.importedTable = opTable.cloneNode(true); // doesn't work in Opera

	// put in the sort buttons
	var theCells = document.importedTable.rows.item(0).cells;
	
	
	var maxLength = []; // will be needed for inserting the filter boxes
	var isSortable = [];

	for(var ic = 0; ic < theCells.length; ic++) {
	    isSortable[ic] = false;
	    maxLength[ic] = 0;

	    var theCell = theCells.item(ic);
	    
	    // check if column is sortable
	    
	    var sortIt = true;
	    //alert( showChildTypes(theCell));

	    // Safari doesn't support param outside of objects so I switched to comments instead.
	    // These next lines will not be operative in the next version (got rid of param in wtrp
	    // -- but the following ones should work.
	    var theParams = theCell.getElementsByTagName("PARAM");
	    for(var jp = 0; jp < theParams.length; jp++) {
		if(theParams.item(jp).name === "wtrp_nosort") {
		    sortIt = false;
		    break;
		}
	    }

	    for(var cChild = 0; cChild < theCell.childNodes.length; cChild++) {
		if(theCell.childNodes[cChild].nodeType === document.COMMENT_NODE) {
		    if(theCell.childNodes[cChild].nodeValue === "wtrp_nosort") {
			sortIt = false;
		    }
		}
	    }

	    if( !sortIt ) {
		continue;
	    }
	    isSortable[ic] = true;

	    for(var lnode = 0; lnode < theCell.childNodes.length; lnode++) {
		
		if(theCell.childNodes[lnode].nodeType === document.TEXT_NODE) { // a text node
		    var aButton = document.createElement("button");
		    var itsLabel = document.createTextNode(theCell.childNodes[lnode].data);
		    aButton.appendChild(itsLabel);
		    aButton.onclick = sort;
		    theCell.replaceChild(aButton, theCell.childNodes[lnode]);
		    break;
		}
	    }
	}

	// delete all links in the table (they would cause confusion)
	var theRows = document.importedTable.rows;
	for(var ir = 0; ir < theRows.length; ir++){
	    for(var jc = 0; jc < theRows.item(ir).cells.length; jc++){
		var jCell = theRows.item(ir).cells.item(jc);
		var jAnchors = jCell.getElementsByTagName("a");
		if(jAnchors.length !== 0) {
		    for(var jn = 0; jn < jAnchors.item(0).childNodes.length; jn++) {
			if(jAnchors.item(0).childNodes[jn].nodeType === document.TEXT_NODE) {// text node
			    jText = document.createTextNode(jAnchors.item(0).childNodes[jn].data);
			    jCell.replaceChild(jText, jAnchors.item(0));
			    break;
			}
		    }
		}
		if(ir !== 0 && isSortable[jc]) {
		    var tLen = jCell.firstChild.data.length;
		    if(tLen > maxLength[jc]) {
			maxLength[jc] = tLen;
		    }
		}
	    }
	    if(ir === 0) {
		document.headerRow = theRows[ir];
	    } else { 	
		document.dataRows.push(theRows[ir]);
	    }
	}
	
	// Now insert filter boxes
	document.filterRow = document.importedTable.insertRow(1);

	for(var kc = 0; kc < maxLength.length; kc++) {
	    var filterCell = document.filterRow.insertCell(kc);
	    if(maxLength[kc] > 0) {
		var filterBox = document.createElement("input");
		filterBox.type = "text";
		filterBox.size = maxLength[kc];
		filterBox.size = (filterBox.size > 25)? 25 : filterBox.size;
		filterBox.onchange = filter
		filterBox.defaultValue = "";
		filterCell.appendChild(filterBox);
	    }
	}

	// Finally, add the table to the document, change color to be sure the user recognizes that this is something different
	var myBody = document.getElementsByTagName("BODY").item(0);
	myBody.bgColor="#6495ED";
	
	var centerElement = document.createElement("center");
	myBody.appendChild(centerElement);

	document.importedTable.bgColor="#B0DEFA";
	centerElement.appendChild(document.importedTable);

    }
    catch(e){
	alert("Page generation failed, possible reasons:" +
	      "Your browser is not supported (it's known to work on Firefox and Safari and known" +
	      "not to work in Explorer). It's also possible that a refresh interfered, try again." + 
	      "The exception is: " + e);
	return;
    }
    
    return true;
}


function replace(tableBody)
{
    // delete all rows except the header and filter rows
    while(tableBody.rows.length > 2) {
	tableBody.deleteRow(2);
    }
    
    // now copy back -- filtering out any unwanted rows
    
    for(var io = 0; io < document.dataRows.length; io++) {
	var suppress = false;
	for(var ic =0; ic < document.filterRow.cells.length; ic++){
	    var iCell = document.filterRow.cells.item(ic);
	    var iBox = iCell.getElementsByTagName("input");
	    if(iBox.length !== 0) {
		var trex = iBox.item(0).value;
		if(trex != "") {
		    // if the first character is !, suppress anything that matches 
		    var notMatch = false;
		    if(trex[0] === "!") {
			notMatch = true;
			trex = trex.substring(1);
		    }
		    var rex = new RegExp(trex);
		    var theEx = document.dataRows[io].cells.item(ic).firstChild.data;
		    suppress = (!notMatch && !rex.test(theEx)) || (notMatch && rex.test(theEx));
		    if(suppress)
			break;
		}
	    }
	}
	if(suppress)
	    continue;
	
	tableBody.appendChild(document.dataRows[io]);
    }
}

function filter()
{
    try {
	if( !(this instanceof HTMLInputElement) ) {
	    alert("expected <button>, got " + this);
	    return false;
	}
	
	if( !(this.parentNode instanceof HTMLTableCellElement) ) {
	    alert("expected <td>, got " + this.parentNode);
	    return false;
	}
	var myCell = this.parentNode;
	
	if( !(myCell.parentNode instanceof HTMLTableRowElement) ) {
	    alert("expected <tr>, got " + this);
	    return false;
	}
	
	var myFirstRow = myCell.parentNode;

	if( !(myFirstRow.parentNode instanceof HTMLTableSectionElement) ) {
	    alert("expected <tbody>, got " + myFirstRow.parentNode);
	    return false;
	}
	var myTableBody = myFirstRow.parentNode;

	replace(myTableBody);

    } catch (e) {
	alert(e);
    }

}

function sort()
{
    
    //climb the tree from the button (this) to tbody
    try {
	if( !(this instanceof HTMLButtonElement) ) {
	    alert("expected <button>, got " + this);
	    return false;
	}
	
	if( !(this.parentNode instanceof HTMLTableCellElement) ) {
	    alert("expected <td>, got " + this.parentNode);
	    return false;
	}
	var myCell = this.parentNode;
	
	if( !(myCell.parentNode instanceof HTMLTableRowElement) ) {
	    alert("expected <tr>, got " + this);
	    return false;
	}
	
	var myFirstRow = myCell.parentNode;
	// figure out which column was clicked
	for(var colNum = 0; colNum < myFirstRow.cells.length; colNum++){
	    if(myCell == myFirstRow.cells.item(colNum) ){
		break;
	    }
	}
	
	if( !(myFirstRow.parentNode instanceof HTMLTableSectionElement) ) {
	    alert("expected <tbody>, got " + myFirstRow.parentNode);
	    return false;
	}
	var myTableBody = myFirstRow.parentNode;
        
	var numRegEx = /^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/; // any floating point number
	
	document.dataRows.sort( 
	    function(r1,r2) { 
		// anything that looks like a number comes first, followed by non-numbers.
		// Unless the button is pressed twice in a row, the sort order is decreasing, otherwise increasing
		
		try {
		    if(r1.cells[colNum] === undefined) {
			if(r2.cells[colNum] === undefined) {
			    return 0;
			} else {
			    return -1;
			}
		    } else if(r2.cells[colNum] === undefined) {
			return 1;
		    }
		    
		    var cols = [null, null];
		    if(colNum !== document._lastColumnSorted) {
			cols[0] = r1.cells[colNum].firstChild.data;
			cols[1] = r2.cells[colNum].firstChild.data;
		    } else {
			cols[1] = r1.cells[colNum].firstChild.data;
			cols[0] = r2.cells[colNum].firstChild.data;
		    }
		    
		    if(cols[0] === null && cols[1] === null) {
			return 0;
		    } else if(cols[0] === null) {
			return -1;
		    } else if(cols[1] === null) {
			return 1;
		    }
		    
		    if( !numRegEx.test(cols[0]) ) {
			if( !numRegEx.test(cols[1]) ) { // both are alpha
			    if(cols[0] < cols[1]) {
				return 1;
			    } else if(cols[0] === cols[1]) {
				return 0;
			    } else {
				return -1;
			    }
			} else { // only first is alpha
			    return 1;
			}
		    }
		    
		    if( !numRegEx.test(cols[1]) ) { // only second is alpha
			return -1;
		    }
		    // Both are numbers
		    
		    if((cols[0]-0) < (cols[1]-0)) {
			return 1;
		    } else if(cols[0] === cols[1]) {
			return 0;
		    } else {
			return -1;
		    }
		}
		catch (e) {
		    alert("Exception caught while sorting: \n" + e + "\n Please file a bug report");
		}
	    }
	);
	
	replace(myTableBody)

	if(colNum !== document._lastColumnSorted) {
	    document._lastColumnSorted = colNum;
	} else {
	    document._lastColumnSorted = -1;
	}
    }
    catch(e){ 
	alert("Page generation failed, maybe a refresh interfered, try again.\n" + 
	      "If the error persists, please file a bug report\n  The exception: " + e);
	return false;
    }
    
}

