function showChildTypes(n)
{
  var typeString = "";
  typeString += n + ", type: " + n.nodeType + ", name: " + n.nodeName + ", parent: " + n.parentNode + "\n";
  var children = n.childNodes;
  for(var i = 0; i < children.length; i++) {
    typeString += showChildTypes(children[i]);
  }
  return typeString;
}

function makeNewWindow(wtrp_id)
{
  try {
    if(navigator.appName === "Microsoft Internet Explorer") {
      alert("Sorry, " + navigator.appName + " is not supported.");
      return;
    }

    var theScripts = document.getElementsByTagName("script");
    var sortFileName = "";
    for(var isc = 0; isc < theScripts.length; isc++) {
      var sortPat = /.*sort.js$/;
      if(sortPat.test(theScripts.item(isc).src)) {
	sortFileName = theScripts.item(isc).src;
	break;
      }
    }
    
    if(sortFileName === "") {
      alert("sorry, no sorting script found. Please file a bug report");
      return false;
    }


    var w = window.open("",  "", "width="+window.innerWidth+",height="+window.innerHeight+
			",status=yes,resizable=yes,scrollbars=yes");
    var d = w.document;
    
    d.open();
    d.write("<script src=\"" + sortFileName + "\" type=\"text/javascript\"></script>");
    // writing the header has the side effect of creating a body. I haven't figured out another way to do it.

    d.write("<center><h1> Sortable copy of " + document.getElementsByTagName("h1").item(0).innerHTML + "</h1></center>");

    d.write("<script>bootstrap(\"" + wtrp_id + "\");</script>");

    d.close();
  } 
  catch(e) {
    alert('error: ' + e);
  }

  return;
}
