var isDone = false;
function attachmap()
{
    if(isDone) {
	return;
    }
    isDone = true;

    var maps = document.getElementsByTagName("map");

    if(maps.length < 1) 
	return;

    var thePotMap = 0;

    for(var iMap = 0; iMap < maps.length; iMap++) {
	if( maps[iMap].name === "potmap")
	    thePotMap = maps[iMap];
    }

    if(thePotMap === 0)
	return;

    // give it some margin at the top so the user can grab the figure

    var areas = thePotMap.getElementsByTagName("area");
    for(var iArea = 0; iArea < areas.length; iArea++) {
	var theCoords = areas[iArea].coords.split(",");
	if(theCoords[1] === "0") {
	    theCoords[1] = "20";
	    areas[iArea].coords = theCoords[0] + "," + theCoords[1] + "," + theCoords[2] + "," + theCoords[3]
	}
    }

    var images = document.getElementsByTagName("img");

    for(var iImage = 0; iImage < images.length; iImage++) {
	var theSrc = images[iImage].src;
	if( images[iImage].src.match(/\/pot_/)) {
	    images[iImage].useMap = "#potmap";
	    images[iImage].border = 0;
	}
    }
    var nImages = images.length;
}

