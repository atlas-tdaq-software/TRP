#! /usr/local/bin/perl

# get rid of refreshes on all html pages and change the urls of the javascripts

use strict;

sub printhelp {
  print "modarch.pl -u <url of local javascript directory> <path to wtrp output directory>\n"
}

my($script_url) = "";

while($#ARGV >= 0 && $ARGV[0] =~ /^-/) {
  if($ARGV[0] eq "-h") { # "interactive" mode, sort of
    printhelp();
    exit;
  } elsif ($ARGV[0] eq "-u") { # the url of the directory holding the javascripts
    shift @ARGV;
    $script_url = $ARGV[0];
  } else {
    die "unrecognized flag: $ARGV[0]";
  }
  shift @ARGV;
}

$script_url eq "" && die "you need to specify a url with flag -u";

print "javascript directory url: $script_url\n";

$#ARGV != 0 && die "You must specify the target directory";
my($wtrp_path) = $ARGV[0];

my(@hFiles) = `ls -1 $wtrp_path/*.html`;

my($hFile);
my($iters) = 0;
while( $hFile = shift @hFiles ) {

  chomp $hFile;
  my($oFile) = $hFile . "_new";
  open OUTP, ">$oFile";

  open INP, $hFile;

  while(<INP>) {
    
    if( $_ =~ m/<\s*meta/i) {
       $_ =~ s/http-equiv\s*=\s*"[^"]+"\s//i;
       $_ =~ s/content\s*=\s*"\d+"//i;
    }

    if( $_ =~ m/<\s*script/ ) {
      $_ =~ s|src\s*=\s*"[^"]*/([^"]+)"|src="$script_url/$1"|;
    }
    print OUTP $_;

  }

  close INP;
  close OUTP;

  if( -s $oFile) {
     system("mv $oFile $hFile");
  } else {
     system("rm $oFile");
     die "Aborting because $oFile has zero length";
  }
}
