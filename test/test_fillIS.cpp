// ------------------------------
// Fills with dummy TimePoints a specified IS_Server
// Author: Antonio Sidoti - Humboldt Universitat zu Berlin
// 
// Date: 01/08/2009
// ------------------------------


#include <iostream>


#include "ers/ers.h"
#include "TRP/Args.h"

#include "config/Configuration.h"
#include "ipc/partition.h"
#include <ipc/core.h>
#include "boost/smart_ptr.hpp"
#include "TRP/ReadXML.h"
#include "TRP/Utils.h"
#include <fstream>
#include <cstdlib>

#include "is/infoany.h"
#include <is/infodictionary.h>
#include <is/infoT.h>
#include <is/info.h>
#include <is/infoiterator.h>
#include <is/inforeceiver.h>
#include <is/serveriterator.h>
#include "is/infodocument.h"


#include "TRP/TimePoint_IS.h" 


std::string partition_name;
std::string server_name;
std::string name_instance;
std::string X_input_name;
std::string Y_input_name;
std::vector<std::string> X_names;
std::vector<std::string> Y_names;
int run_number;


void ReadConfig(std::string  config_file){
  std::cout << "ReadConfig(" << config_file << " ) " << std::endl;

  
  ReadXML my_reader;
  my_reader.SetElement("test_fillIS");
  std::vector<std::string> my_str;
  

  my_str.push_back("name_instance");
  my_str.push_back("X_input_name");
  my_str.push_back("Y_input_name");
  my_str.push_back("run_number");
  my_reader.SetAttribute(my_str);
  my_reader.readConfigFile(config_file);
  server_name = my_reader.GetValC("server");
  name_instance = my_reader.GetVal("name_instance");
  X_input_name = my_reader.GetVal("X_input_name");
  Y_input_name = my_reader.GetVal("Y_input_name");
  run_number =  trp_utils::convertToInt(my_reader.GetVal("run_number"));
  
  std::cout << "name_instance:" <<  name_instance << std::endl;
  std::cout << "server:" << server_name << std::endl;
  std::cout << "X_input_name:" << X_input_name << std::endl;
  std::cout << "Y_input_name:" << Y_input_name << std::endl;
  std::cout << "run_number:" << run_number << std::endl;
  std::cout << "end reading configuration" << std::endl;
  
  
}
  

void showHelp( std::ostream& os, std::string pgmName ){
  os << "Usage: " << pgmName << " [-p <str>]  [-c <file.xml>] [-s <str>] [-k <str>] [-out <str>]" << std::endl;
  os << "                                                                      " << std::endl;
  os << "Options/Arguments:                                                    " << std::endl;
  os << "  -p <str>   partition name  (if not present $TDAQ_PARTITION is used) " << std::endl; 
  os << "  -c <file1.xml:file2.xml:...>   reads info from xml file instead. Picks info from existing file" << std::endl;
  os << "                                                                      " << std::endl;
  os << "  or in alternative from the command line as follows     " << std::endl;
  exit(1);
}

void Generate_TP(TimePoint_IS &my_tp, int i){
  my_tp.XLabels.clear();
  my_tp.YLabels.clear();
  my_tp.Data.clear();
  my_tp.SetXAxis(X_names);
  my_tp.SetYAxis(Y_names);
  int dim_data = my_tp.NumCol() * my_tp.NumRow();
   (my_tp.Data).reserve(dim_data);
  OWLTime now(OWLTime::Seconds);
  my_tp.TimeStamp = now;
  my_tp.RunNumber = run_number;
  my_tp.LumiBlock = i;
  for (unsigned int iy = 0; iy < Y_names.size(); iy++) {
    for (unsigned int ix = 0; ix < X_names.size(); ix++) {
      float x_val = ix * 100 + i * 10 + rand() % 10;
      my_tp.Data.push_back(x_val);
    }
  }
}


int main( int ac, char* av[] ){
  
  // get arguments
  Args args( ac, av );
  
  // get program name
  std::string pgmName = av[0];
  std::string::size_type pos = pgmName.rfind( '/' );
  if( pos != std::string::npos )
    pgmName = pgmName.substr( pos+1 );
  
  
  // check if requested help
  if( args.hasArg("-h")     || 
      args.hasArg("-?")     || 
      args.hasArg("--help") || 
      ac <= 1  )
    //           || 
    //	!args.hasArg("-n"))
    { showHelp( std::cerr, pgmName ); }
  
  IPCCore::init( ac, av );
  
  // reconstruct command line string for debugging feedback
  std::stringstream cmdLine;
  cmdLine << av[0];
  for( int i = 1; i < ac; ++i )
    cmdLine << " " << av[i];
  std::cout << "cmdline: '" << cmdLine.str() << "'" << std::endl;
  
  // Retrive the partition name (from command line or from environment) 
  if (args.hasArg("-p")) {
    partition_name = args.getStr("-p");
    //      TRP_DEBUG(0, "Retrieved partition name from command line: '" << partition_name << "'");
    std::cout << "Retrieved partition name from command line: " << partition_name << std::endl;
  } else if (getenv("TDAQ_PARTITION")) {
    partition_name = getenv("TDAQ_PARTITION");
    //TRP_DEBUG(0, "Retrieved partition name from environment: '" << partition_name << "'");
  } else {
    std::cerr << "Partition name not set (neither in command line nor in environment)" << std::endl;
    showHelp( std::cerr, pgmName );
  }
  
  std::string config_file;
  if (args.hasArg("-c")) {
    config_file  = args.getStr("-c");
    //ERS_DEBUG(0, "Config file from  command line: '" << config_file  << "'");
    std::cout<< "Config file from  command line: " << config_file  << std::endl;
    
    ReadConfig(config_file);
    
    trp_utils::tokenize(X_input_name,X_names,":");
    trp_utils::tokenize(Y_input_name,Y_names,":");
  }
    
  srand ( time(NULL) );
  
  IPCPartition * mPartition = new IPCPartition( partition_name );
  
  ISInfoDictionary dict( *mPartition );
  
  
  std::string rate_server = server_name + ".";
  rate_server += name_instance;
  int my_lb = 0;
  int iter = 0; 
  
  while(1) {
    TimePoint_IS mlvl1_point;
    my_lb = int (iter / 100);
    Generate_TP(mlvl1_point, my_lb);
    
    try {
      dict.insert(rate_server,mlvl1_point);
    } catch(daq::is::InfoAlreadyExist&){
      dict.update(rate_server, mlvl1_point, true);
    }
    sleep(10);
    ++iter;
  }
  
} // end of main


