#include <iostream>
#include <exception>

#include "TRP/TriggerMenuHelpers.h"

using namespace std;

void help(const char* appName, const std::string & msg="") {
   if(msg!="") {
      cout << msg << endl;
   }
   cout << "Usage\n" << appName << " --smk <smk> --hltpsk <hltpsk> [--db <dbalias> --run2 --print-size-only]" << endl << endl;
}

int main(int argc, const char* argv[]) {

   for (int i = 0; i < argc; ++i) {
      if( (string("--help") == argv[i]) or (string("-h") == argv[i]) ) {
         help(argv[0]);
         return 1;
      }
   }

   if(argc<5) { 
      help(argv[0], "Please specify smk and hltpsk (>0)");
      return 1;
   }

   // read cmdline parameters
   int smk(0);
   int hltpsk(0);
   string dbalias("TRIGGERDB");
   bool useRun2 = false;
   bool printSizeOnly = false;

   for (int i = 0; i < argc; ++i) {
      if( string("--smk") == argv[i] && i<argc-1) {
         smk = atoi(argv[++i]);
      }
      if( string("--hltpsk") == argv[i] && i<argc-1) {
         hltpsk = atoi(argv[++i]);
      }
      if( string("--db") == argv[i] && i<argc-1) {
         dbalias = argv[++i];
      }
      if( string("--run2") == argv[i]) {
         useRun2 = true;
      }
      if( string("--print-size-only") == argv[i]) {
         printSizeOnly = true;
      }
   }

   if(smk==0 or hltpsk==0) {
      help(argv[0]);
      return 1;
   }

   cout << "Going to read SMK " << smk << " and HLT PSK " << hltpsk << " from DB " << dbalias << " (assuming db for Run " << (useRun2 ? "2" : "3") << ")" << endl;

   confadapter_imp::TriggerMenuHelpers mh(dbalias);

   bool success = false;
   if(useRun2) {
      success = mh.getConfigurationRun2( smk, hltpsk, 0);

   } else {
      try {
         success = mh.getConfiguration( smk, hltpsk, 0);
      }
      catch( std::exception & ex ) {
         cerr << "Caught std::exception " << ex.what() << endl;
      }
   }
   if(success) {
      mh.printAllMaps(printSizeOnly);
   }
}
