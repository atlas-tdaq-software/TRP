#include <string>
#include <vector>
#include <iostream>
#include <ipc/core.h>
#include <ipc/partition.h>
#include <is/infodictionary.h>

#include "../TRP/TimePoint_IS.h"
#include "../TRP/Args.h"
#include "../TRP/TimePointAsyncSender.h"


void help(){
  std::cout << "Test usage: test_sender -p PARTITION -n ISSERVER" << std::endl;
  exit(0);
}

int main( int ac, char** av ) {
  Args args( ac, av );
  std::string partition_name;
  if (args.hasArg("-p")) {
    partition_name = args.getStr("-p");
  } else {
    help();
  }
  std::string server_name;
  if (args.hasArg("-n")) {
    server_name = args.getStr("-n");
  } else {
    help();
  }
  std::cout << "part: " << partition_name << " serv: " << server_name << std::endl;
  IPCCore::init( ac, av );

  // create partition object
  IPCPartition *partition = new IPCPartition( partition_name );
  ISInfoDictionary dict( *partition );
  TimePointAsyncSender sender(&dict, server_name+".some_info");
  //TimePointAsyncSender sender(&dict, "some_info");
  sleep(1);
  ERS_DEBUG(1, "Sender Prepared");

  TimePoint_IS* t0 = new TimePoint_IS; 
  sender.send(t0);
  ERS_DEBUG(1, "1st obj sent");
  t0 = new TimePoint_IS; 
  sender.send(t0);
  ERS_DEBUG(1, "2nd obj sent");
  sleep(1);
  t0 = new TimePoint_IS; 
  sender.send(t0);
  ERS_DEBUG(1, "3rd obj sent");
  sleep(1);
}
