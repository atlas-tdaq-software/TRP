#include <string>
#include <vector>
#include <iostream>
#include <ipc/core.h>
#include <ipc/partition.h>
#include <is/infodictionary.h>

#include "../TRP/TimePoint_IS.h"
#include "../TRP/Args.h"


void help(){
  std::cout << "Test usage: test_sender -p PARTITION -n ISSERVER -t timepoint name" << std::endl;
  exit(0);
}

int main( int ac, char** av ) {
  Args args( ac, av );
  std::string partition_name;
  if (args.hasArg("-p")) {
    partition_name = args.getStr("-p");
  } else {
    help();
  }
  std::string server_name;
  if (args.hasArg("-n")) {
    server_name = args.getStr("-n");
  } else {
    help();
  }

  std::string obj_name;
  if (args.hasArg("-t")) {
    obj_name = args.getStr("-t");
  } else {
    help();
  }
  std::cout << "part: " << partition_name << " serv: " << server_name << " obj: " << obj_name << std::endl;
  IPCCore::init( ac, av );

  // create partition object
  IPCPartition *partition = new IPCPartition( partition_name );

  ISInfoDictionary dict( *partition );
  TimePoint_IS tp;
  dict.getValue( server_name+"."+obj_name, tp);
  tp.print_as_table(std::cout);
  
}



