#include <iostream>
#include <string>

#include <owl/time.h>

#include <is/infoT.h>
#include <is/infodictionary.h>
#include <ipc/core.h>

#include "TimePoint_IS.h"

std::string partition_name = "TEST_PARTITION_TRPGUI";
std::string server_name = "TEST_SERVER_TRPGUI";
std::string schema_file = "../share/is_timepoint.schema.xml";

void start_ipc_partition(void);
void start_is_server(void);
void start_rdb_server(void);
int sys(std::string cmd, int pause = 0);

int main(int argc, char *argv[]) {
  std::string t_name;
  if (argc > 1) {
    t_name = argv[1];
  } else {
    t_name = "L1_Rate";
  }
    
  IPCPartition trp_partition = IPCPartition(partition_name);
  if (!trp_partition.isValid()) {
    start_ipc_partition();
  }
  start_is_server();
  start_rdb_server();

  IPCCore::init(argc,argv);   // Initialize communication library
  IPCPartition partition(partition_name);  // Create instance of partition
  ISInfoDictionary dict(partition);  // create IS dictionary in partition

  TimePoint_IS t;

  ISInfoFloat r1 = 0.0;
  ISInfoFloat r2 = 1.0;
  OWLTime time = (time_t)0;

  std::vector<std::string> x_names;
  x_names.push_back("rate1");
  x_names.push_back("rate2");

  std::vector<std::string> y_names;
  y_names.push_back("bps");
  y_names.push_back("aps");
  y_names.push_back("av");

  t.format(x_names, y_names);

  for (int i = 0; i < 100; i++) {
    t.set("rate1", "bps", r1);
    t.set("rate1", "aps", r1*0.9);
    t.set("rate1", "av",  r1*0.8);

    t.set("rate2", "bps", r2);
    t.set("rate2", "aps", r2*0.7);
    t.set("rate2", "av",  r2*0.4);

    time = OWLTime();
    t.TimeStamp = time;

    std::cout << "Publishing: time = " << time 
              << ", rate1: " << r1 << ", " << r1*0.9 << ", " << r1*0.8 
              << ", rate2: " << r2 << ", " << r2*0.7 << ", " << r2*0.4 
              << '\n';

    dict.checkin(server_name + "." + t_name, t, true);
    sleep(5);
    r1 = r1 + 0.2;
    r2 = r2 + 0.1;
  }

  std::cout << "All published!\n";
  return 0;
}

void start_ipc_partition(void) {
  std::cout << "Starting partition " << partition_name << '\n';
  sys("ipc_server -p " + partition_name + "&", 2);
}

void start_is_server(void) {
  std::cout << "Starting IS server " << server_name << '\n';
  sys("is_server -p " + partition_name 
             + " -n " + server_name + "&", 1);
}

void start_rdb_server(void) {
  std::cout << "Starting RDB server with " << schema_file << '\n';
  sys("rdb_server -p " + partition_name 
              + " -d " + server_name 
              + " -S " + schema_file + "&", 1);
}

int sys(std::string cmd, int pause) {
  std::cout << "{publish} " << cmd << '\n';
  int retval = system(cmd.c_str());  // std::string.c_str() returns a
                                     // const pointer
                                     // to the given string
  if (pause) {
    sleep(pause);
  }
  if (retval != 0) {
    std::cout << "{publish} Warning: system(" << cmd << ") "
              << "returns " << retval << '\n';
  }
  return retval;
}
