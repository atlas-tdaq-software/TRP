#include <string>
#include <vector>
#include <iostream>

#include "../TRP/TimePoint_IS.h"
using namespace std;
int main() {
  TimePoint_IS t0; 
  t0.print(cout);
  float v;
  if ( t0.get("bla", "bla", v) == true ) {
    cout << "ERROR Get method works! on unitialized and unknown data" << endl;
    return -1;
  }

  if ( t0.get("bla", "bla", v) == true ) {
    cout << "ERROR Set method works! on unitialized and unknown data" << endl;
    return -1;
  }
  

  TimePoint_IS t1;
  vector<string> x;
  vector<string> y;

  y.push_back("input");
  y.push_back("output");


  x.push_back("total");
  x.push_back("egamma");
  x.push_back("jets");
  x.push_back("muons");
  t1.format(x, y);


  t1.print(cout);

  if ( t1.get("total", "input", v) == false ) {
    cout << "ERROR Get method does not work!" << endl;
    return -1;
  }
  if ( v != 0 ) {
    cout << "ERROR structure not rest" << endl;
    return -1;
  }

  if ( t1.set("jets", "output", 17) == false ) {
    cout << "ERROR Set method does not work!" << endl;
    return -1;
  }
  float back;
  if ( t1.get("jets", "output", back) == false ) {
    cout << "ERROR Get method does not work!" << endl;
    return -1;
  }
  if ( back != 17 ) {
    cout << "ERROR Get method does not work, wrong value when accessing by label!" << endl;
    return -1;
  }

  // also try to address TimePoint by labels
  back = 0;
  t1.get(2,1, back);
  if ( back != 17 ) {
    cout << "ERROR Get method does not work, wrong value when accessing by int index!" << endl;
    return -1;
  }

  t1.print(cout);
  cout << "Test OK" << endl; 
}
