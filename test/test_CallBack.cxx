#include <string>
#include <vector>
#include <iostream>
#include <is/inforeceiver.h>
#include <ipc/core.h>
#include <ipc/partition.h>

#include "TRP/Args.h"
#include "TRP/TimePoint_IS.h"

using namespace std;


TimePoint_IS my_tp;
std::string partition_name="";
std::string server_name="";
std::string tp_name="";
std::string x_label="";
std::string y_label="";

void showHelp( std::ostream& os, std::string pgmName ){
  os << "Usage: " << pgmName << " [-p <str>]  [-c <file.xml>] [-s <str>] [-k <str>] [-out <str>]" << std::endl;
  os << "                                                                      " << std::endl;
  os << "Options/Arguments:                                                    " << std::endl;
  os << "  -p <str>   partition name  " << std::endl; 
  os << "  -s <str>   server name     " << std::endl; 
  os << "  -n <str>    TimePoint name " << std::endl;
  os << "  -x <str>   XLabel      " << std::endl;
  os << "  -y <str>   YLabel      " << std::endl;

  exit(1);
}

void my_callback(ISCallbackInfo *isc) {
  isc->value(my_tp);
  float my_rate = -99999.;
  if(my_tp.get(x_label, y_label, my_rate)){
    std::cout << "Rate for " << x_label << ":" << y_label << " = " << my_rate 
	      <<std::endl;
  } else {
    std::cout << "Cannot find  " << x_label << ":" << y_label << "  for timepoint " << tp_name  
	      <<std::endl;
  }
}

int main( int ac, char* av[] ){
  // get arguments
  Args args( ac, av );
  
  // get program name
  std::string pgmName = av[0];
  std::string::size_type pos = pgmName.rfind( '/' );
  if( pos != std::string::npos )
    pgmName = pgmName.substr( pos+1 );
  
  
  // check if requested help
  if( args.hasArg("-h")     || 
      args.hasArg("-?")     || 
      args.hasArg("--help") || 
      ac <= 1  )
    //           || 
    { showHelp( std::cerr, pgmName ); }
  
  // reconstruct command line string for debugging feedback
    std::stringstream cmdLine;
    cmdLine << av[0];
    for( int i = 1; i < ac; ++i )
      cmdLine << " " << av[i];
    std::cout << "cmdline: '" << cmdLine.str() << "'" << std::endl;
    
    // Retrive the partition name from command line 
    if (args.hasArg("-p")) { 
      partition_name = args.getStr("-p");
    } else {
      std::cerr << "Partition name not set" << std::endl;
      showHelp( std::cerr, pgmName );
    }
    if (args.hasArg("-s")) { 
      server_name = args.getStr("-s");
    } else {
      std::cerr << "Server name not set" << std::endl;
      showHelp( std::cerr, pgmName );
    }
    if (args.hasArg("-k")) { 
      tp_name = args.getStr("-k");
    } else {
      std::cerr << "TimePoint  name not set" << std::endl;
      showHelp( std::cerr, pgmName );
    }
    if (args.hasArg("-x")) { 
      x_label = args.getStr("-x");
    } else {
      std::cerr << "Xlabel name not set" << std::endl;
      showHelp( std::cerr, pgmName );
    }
    if (args.hasArg("-y")) { 
      y_label = args.getStr("-y");
    } else {
      std::cerr << "Server name not set" << std::endl;
      showHelp( std::cerr, pgmName );
    }
    IPCCore::init( ac, av );    
    IPCPartition  mPartition(partition_name);
    ISInfoReceiver rec(mPartition);
    ISCriteria m_crit (tp_name, TimePoint_IS::type(), // AS
		       ISCriteria::AND );
    rec.subscribe( server_name, m_crit, 
		   &my_callback);

    //rec.run();
}

