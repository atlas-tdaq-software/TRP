#include <is/infoT.h>
#include <is/infodictionary.h>
#include <ipc/core.h>
#include <unistd.h>

#include "TRP/L2SV.h"


int main( int ac, char ** av ){
  // Init	ialise communication library
  IPCCore::init( 	ac, av );
  
  // Create the instance of partition
  IPCPartition partition("MyTestPartitionMM");
  
  // Create the IS dictionary in the specific partitition
  ISInfoDictionary dict(partition);
  
  // Create the instances of the information
  // that will be published, and initialise them

  L2SV l2sv;

  l2sv.Identity = "Hi there";
  l2sv.errors = 0;
  l2sv.disableCnt = 1;
  l2sv.DFM_XOFF =1;
  l2sv.accumulatedXoffTime = 10.;
  l2sv.LVL1_events = 42; 
  l2sv.LVL2_events = 21;
  l2sv.AcceptedEvents = 17;
  l2sv.RejectedEvents = 4;
  l2sv.ForcedAccepts = 3;
  l2sv.IntervalTime = 10.;
  l2sv.IntervalEventRate = 27.;
  l2sv.AvgEventRate = 2.7;

  while(true) {
    l2sv.errors++;
    l2sv.disableCnt++;
    l2sv.DFM_XOFF = (l2sv.DFM_XOFF == 1)? 0 : 1;
    l2sv.accumulatedXoffTime += 1.;
    l2sv.LVL1_events++; 
    l2sv.LVL2_events++;
    l2sv.AcceptedEvents++;
    l2sv.RejectedEvents++;
    l2sv.ForcedAccepts++;
    l2sv.IntervalTime += 1.;
    l2sv.IntervalEventRate = 27.;
    l2sv.AvgEventRate += 0.1;

    // Publish information in the MyServer IS server
    dict.insert( "RunCtrlStatistics.L2SV-SUM", l2sv );

    sleep(10);
}
  
  return 0;
}
