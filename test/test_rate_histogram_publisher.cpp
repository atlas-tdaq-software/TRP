#include <TH2F.h>

// this is example rate histogram generated with ROOT "save as macro" feature
TH2F*  getHist(){
  //=========Macro generated from canvas: c1/c1
  //=========  (Wed Sep  2 21:18:44 2009) by ROOT version5.22/00d
   TH2F *Rate10s = new TH2F("Rate10s","Rates",34,0,34,4,0,4);
   Rate10s->SetBinContent(1,1, .7);
   Rate10s->SetBinContent(1,4, .6);

   Rate10s->GetXaxis()->SetBinLabel(1,"total");
   Rate10s->GetXaxis()->SetBinLabel(2,"str_Bphys_physics");
   Rate10s->GetXaxis()->SetBinLabel(3,"str_IDCalibration_calibration");
   Rate10s->GetXaxis()->SetBinLabel(4,"str_electrons_physics");
   Rate10s->GetXaxis()->SetBinLabel(5,"str_higgs_express");
   Rate10s->GetXaxis()->SetBinLabel(6,"str_higgs_physics");
   Rate10s->GetXaxis()->SetBinLabel(7,"str_jets_physics");
   Rate10s->GetXaxis()->SetBinLabel(8,"str_muons_physics");
   Rate10s->GetXaxis()->SetBinLabel(9,"str_test_physics");
   Rate10s->GetXaxis()->SetBinLabel(10,"str_zee_calibration");
   Rate10s->GetXaxis()->SetBinLabel(11,"grp_bphys");
   Rate10s->GetXaxis()->SetBinLabel(12,"grp_electrons");
   Rate10s->GetXaxis()->SetBinLabel(13,"grp_fakes");
   Rate10s->GetXaxis()->SetBinLabel(14,"grp_jets");
   Rate10s->GetXaxis()->SetBinLabel(15,"grp_muons");
   Rate10s->GetXaxis()->SetBinLabel(16,"e25i_EF");
   Rate10s->GetXaxis()->SetBinLabel(17,"mu20_EF");
   Rate10s->GetXaxis()->SetBinLabel(18,"2e15i_EF");
   Rate10s->GetXaxis()->SetBinLabel(19,"2mu6_EF");
   Rate10s->GetXaxis()->SetBinLabel(20,"g25_EF");
   Rate10s->GetXaxis()->SetBinLabel(21,"j200_EF");
   Rate10s->GetXaxis()->SetBinLabel(22,"3j90_EF");
   Rate10s->GetXaxis()->SetBinLabel(23,"tau25_xE30_EF");
   Rate10s->GetXaxis()->SetBinLabel(24,"j50_xE60_EF");
   Rate10s->GetXaxis()->SetBinLabel(25,"Z_EF");
   Rate10s->GetXaxis()->SetBinLabel(26,"H_EF");
   Rate10s->GetXaxis()->SetBinLabel(27,"NO_LOWER_CHAIN_EF");
   Rate10s->GetXaxis()->SetBinLabel(28,"interval_0");
   Rate10s->GetXaxis()->SetBinLabel(29,"interval_1");
   Rate10s->GetXaxis()->SetBinLabel(30,"interval_2");
   Rate10s->GetXaxis()->SetBinLabel(31,"interval_3");
   Rate10s->GetXaxis()->SetBinLabel(32,"interval_4");
   Rate10s->GetXaxis()->SetBinLabel(33,"interval_5");
   Rate10s->GetXaxis()->SetBinLabel(34,"time");

   Rate10s->GetYaxis()->SetBinLabel(1,"input");
   Rate10s->GetYaxis()->SetBinLabel(2,"prescale");
   Rate10s->GetYaxis()->SetBinLabel(3,"raw");
   Rate10s->GetYaxis()->SetBinLabel(4,"output");
   return Rate10s;
}


#include <string>
#include <vector>
#include <iostream>
#include <string>
#include <ipc/core.h>
#include <ipc/partition.h>
#include <oh/OHRootProvider.h>
#include <boost/lexical_cast.hpp>
#include "../TRP/Args.h"

void help(){
  std::cout << "Test usage: test_rate_histogram_publisher -p PARTITION -n ISSERVER -l EF/L2 -s sleeptime" << std::endl;
  exit(0);
}

std::vector<OHRootProvider*> providers;
void createProviders(IPCPartition*p, const std::string& server) {
  const unsigned num = 10;
  for ( unsigned i = 0 ; i < num; ++i ) {
    try {
      std::string name = "L2PU-"+boost::lexical_cast<std::string>(i);
      providers.push_back( new OHRootProvider(*p, server, name) );
    } catch (...) {
      throw;
    }
  }
}


int main( int ac, char** av ) {
  Args args( ac, av );
  std::string partition_name;
  if (args.hasArg("-p")) {
    partition_name = args.getStr("-p");
  } else {
    help();
  }
  std::string server_name;
  if (args.hasArg("-n")) {
    server_name = args.getStr("-n");
  } else {
    help();
  }
  std::string level;
  if (args.hasArg("-l")) {
    level = args.getStr("-l");
  } else {
    help();
  }
  unsigned sleeptime = 5;
  if (args.hasArg("-s")) {
    sleeptime = args.getInt("-s");
  } else {
    help();
  }

  std::cout << "part: " << partition_name << " serv: " << server_name << std::endl;
  IPCCore::init( ac, av );

  // create partition object
  IPCPartition *partition = new IPCPartition( partition_name );
  // create OH Providers
  createProviders(partition, server_name);
  
  // now in a loop with 5 seconds sleep interval publish example 
  // but each time tunning the time
  TH2F* h = getHist();

  OWLTime pubtime(time(0));
  while(1) {
    OWLTime nowtime(time(0));
    if ( nowtime.sec()/10 != pubtime.sec()/10 ) {
      pubtime = nowtime;
      std::cout << " publishing with new time" << pubtime << std::endl;
    } else {
      std::cout << " re publishing with old time" << pubtime << std::endl;
    }

    int binx = h->GetNbinsX();
    h->SetBinContent(binx, 1, pubtime.c_tm().tm_yday);
    h->SetBinContent(binx, 2, pubtime.hour());
    h->SetBinContent(binx, 3, pubtime.min());
    h->SetBinContent(binx, 4, pubtime.sec());
    
    // modify input/output rate a bit
    h->SetBinContent(1, 1, 100.+pubtime.sec()/8.);
    h->SetBinContent(1, 4, 100.+pubtime.sec()/10.);

    std::vector<OHRootProvider*>::iterator i;
    for (  i = providers.begin(); i != providers.end(); ++i) {
      (*i)->publish( *h, "/SHIFT/TrigSteer_"+level+"/Rate10s");
    }    
    sleep(sleeptime*(1+rand()%5)); // sleep 5,10,15 seconds
  }
} 
