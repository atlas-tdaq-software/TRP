/** 
 * @file GenericAdapter.h
 * @author Antonio Sidoti Humboldt Universitat zu Berlin antonio.sidoti@cern.ch
 * @date 12/12/2008
 * @brief Triger Level 1 classes for obtaining the data from IS.
 */



#include "GenericAdapter.h"
#include "ers/ers.h"
#include <time.h>
#include <regex.h>
#include "TRP/Utils.h"
#include "boost/regex.hpp"
#include "boost/numeric/conversion/cast.hpp"

ERS_DECLARE_ISSUE(genericadapter, Issue, "conversion issue", )
//#define PRINT_VECTOR_ATTRIBUTE(x,y) case ISType::y : is::print( ida, ida.getAttributeValue<std::vector<x> >( i ) ) << ", "; break;

#define TDAQ_NIGHTLY // for nightly (comment for tdaq-03-00-01)

using namespace boost;

GenericAdapter::GenericAdapter(std::string partition_name_in, std::string server_name_in,
			       std::string partition_name_out, std::string server_name_out,
			       std::string name_out, std::string mode, 
			       float sleep_time, 
			       std::string object_name, std::string replace_name, std::string value_name, std::string index_id,
			       const std::string my_separator){
  
  
  ERS_DEBUG( 3, "GenericAdapter::GenericAdapter()" );
  
  pPartitionNameIn = partition_name_in;
  pPartitionNameOut = partition_name_out;
  pServerNameIn = server_name_in;
  pServerNameOut = server_name_out;
  pNameOut = name_out;
  m_sleep_time = sleep_time;
  m_separator = my_separator;
  m_mode = mode;
  trp_utils::tokenize(object_name , m_obj_vec ,my_separator);
  trp_utils::tokenize(replace_name , m_rep_vec ,my_separator);
  trp_utils::tokenize(value_name , m_val_vec ,my_separator);
  trp_utils::tokenize(index_id , m_id_vec ,my_separator,"-");

  m_is_publishing  = false;
  
  myXaxis.clear( );
  myYaxis.clear( );
  
  
  std::cout << "In GenericAdapter::GenericAdapter partition_name_in" <<   pPartitionNameIn << "  ServerIn:" << pServerNameIn << "  NameOut:" << pNameOut << std::endl;

  std::cout << "Object names are:" << std::endl;
  std::vector<std::string>::iterator m_obj_vec_it = m_obj_vec.begin();
  for(;m_obj_vec_it!=m_obj_vec.end();++m_obj_vec_it)
    std::cout << (*m_obj_vec_it) << " ";
  std::cout << endl;


  std::cout << "Replace names are:" << std::endl;
  std::vector<std::string>::iterator m_rep_vec_it = m_rep_vec.begin();
  for(;m_rep_vec_it!=m_rep_vec.end();++m_rep_vec_it)
    std::cout << (*m_rep_vec_it) << " ";
  std::cout << endl;
  

  //sanity check
  if(m_obj_vec.size() != m_rep_vec.size()){
    std::cout << "Warning different size of object and replaced names" << std::endl;
    // here you should probably kill the application
    
  }
  

  std::cout << "Value names are:" << std::endl;
  std::vector<std::string>::iterator m_val_vec_it = m_val_vec.begin();
  for(;m_val_vec_it!=m_val_vec.end();++m_val_vec_it)
    std::cout << (*m_val_vec_it) << " ";
  std::cout << endl;

  std::cout << "Index  are:" << std::endl;
  std::vector<int>::iterator m_id_vec_it = m_id_vec.begin();
  for(;m_id_vec_it!=m_id_vec.end();++m_id_vec_it)
    std::cout << (*m_id_vec_it) << " ";
  std::cout << endl;
  
  m_count_event=0;
  
  
  
}

GenericAdapter::GenericAdapter(const GenericAdapter& )
{
  // implemented only to forbid copying
  
}

GenericAdapter::~GenericAdapter() 
{
  ERS_DEBUG( 3, "GenericAdapter::~GenericAdapter()" );
}

bool GenericAdapter::Configure(){
  bool my_ret = false;
  ERS_DEBUG(0, "In Configuration");
  std::cout <<  "In Configuration" << std::endl;
  mPartitionIn = new IPCPartition( pPartitionNameIn );
  mPartitionOut = new IPCPartition( pPartitionNameOut);
  ctpcore_oldtime = OWLTime();
  //if (!mPartitionIn) mPartitionIn = new IPCPartition( pPartitionNameIn );
  //if (!mPartitionOut) mPartitionOut = new IPCPartition( pPartitionNameOut);
  
  //m_InfoIt = NULL; 
  //m_InfoIt = new ISInfoInterator( *mPartitionIn, pServerNameIn );
  m_count_event = 0;
  for(int it_count=0;it_count<6;++it_count){
    cout << "Configure TP: " << it_count << "-th try.." << endl;
    bool my_ex = ConfigureTP();
    if(my_ex) {
      cout << "Configuration Succeeded" << endl;
      my_ret = true;
      break;
    }
    sleep(30);
  }
  

  //  // determine the dimension of vector Data and MetaData of TimePoint_IS
  //  std::cout << "TimePoint has Row:" <<  m_mu_point.NumRow() << "  Col:" << 
  //    m_mu_point.NumCol() << std::endl;
  //  int dim_data = m_mu_point.NumCol() * m_mu_point.NumRow();
  //  std::cout << "TimePoint Data is:" << dim_data << std::endl;
  //  (m_mu_point.Data).reserve(dim_data);
  
  return(my_ret);
}


bool GenericAdapter::ConfigureTP(){
  bool my_ret = false;
  std::cout <<  "In ConfigureTP" << std::endl;
  if(!mPartitionIn){
    std::cout << "Warning! Invalid pointer to mPartitionIn. Reinstantiating it";
    mPartitionIn = new IPCPartition( pPartitionNameIn );
  }
  //  mPartitionIn = new IPCPartition( pPartitionNameIn );
  //ISInfoIterator m_InfoIt0( *mPartitionIn, pServerNameIn );
  const ISCriteria& criteria = ISCriteria( ".*" );
  ISInfoIterator m_InfoIt0( *mPartitionIn, pServerNameIn, criteria);
  // reads the object from the server and establish what to plot  
  while( (m_InfoIt0)() ){ // loop on is servers
    // loop on all object names
    int idx = 0;
    std::vector<std::string>::iterator m_obj_vec_it = m_obj_vec.begin();
    for(;m_obj_vec_it!=m_obj_vec.end();++m_obj_vec_it,++idx){ // loop on object names
      // match the name in IS with object_name vector
    
      string m_InfoIt0_str = m_InfoIt0.name();
      std::cout << "IS Server Name = " << m_InfoIt0_str << std::endl;
      // here perform the regex_search
      boost::regex *expr_it = NULL;
      try {
	expr_it = new boost::regex((*m_obj_vec_it));
      }
      catch (boost::regex_error& e) {
	ers::warning(genericadapter::Issue(ERS_HERE, "Invalid regular expression "+boost::lexical_cast<std::string>( e.what()) 
					   + (*m_obj_vec_it) ));
      }
      if (regex_search( m_InfoIt0_str, *expr_it )){
       	std::cout << "MATCH-- with:" << (*m_obj_vec_it) << std::endl;
	boost::regex expr_it_rep_1(pServerNameIn+".");
	cout << "1-replace:" << m_InfoIt0_str << "with :" << expr_it_rep_1 << endl;
	string tmp_str_1 = boost::regex_replace(m_InfoIt0_str, expr_it_rep_1, "",boost::regex_constants::format_first_only); // strip off pServerNameIn
	// now do the replacement
	cout << "2-replace:" << tmp_str_1 << "with :" << *expr_it << "  " <<  (m_rep_vec)[idx] << endl;
	string tmp_str_2 = boost::regex_replace(tmp_str_1, *expr_it, (m_rep_vec)[idx]); 
	m_obj_name_vec.push_back(tmp_str_1);
	// insert in a map the original and the new name
	std::cout << " is replaced with: " << tmp_str_2 << std::endl;
	m_map_str[tmp_str_1]=tmp_str_2;
      }
    }
  }
  if(m_map_str.size()==0) {
    cout << "Cannot find Name in IS Server. Exiting configuration" << endl;
    return false;
  }
  std::vector<std::string>::iterator m_obj_name_vec_it = m_obj_name_vec.begin();
  for(;m_obj_name_vec_it!=m_obj_name_vec.end();++m_obj_name_vec_it){ // loop on object names
    ISCriteria m_criteria(*m_obj_name_vec_it);
    ISInfoIterator m_InfoIt( *mPartitionIn, pServerNameIn, m_criteria );
    std::cout << "criteria is:" << *m_obj_name_vec_it << std::endl;
    // now loop on the objects to find the value_name
    while  ((m_InfoIt)()){
      ISInfoAny isa;
      m_InfoIt.value( isa );      
      size_t count = isa.type().entries();
      auto isd = std::make_unique<ISInfoDocument>( *mPartitionIn, isa.type());
      for ( size_t i = 0; i < count; i++ ){ //loop on attributes
#ifndef TDAQ_NIGHTLY
	const ISInfoDocument::Attribute  * attr = isd.get() ? isd->attribute( i ) : 0; // tdaq-03-00-01
	string attr_name = attr->name().c_str(); // tdaq03-00-01
#endif
#ifdef TDAQ_NIGHTLY
	const ISInfoDocument::Attribute  & attr = isd->attribute( i ); // nightly
	string attr_name = attr.name().c_str(); // nightly
#endif	
	std::vector<std::string>::iterator m_val_vec_it = m_val_vec.begin();
	for(;m_val_vec_it!=m_val_vec.end();++m_val_vec_it){ // loop to find values 
	  size_t found_val;
	  found_val=attr_name.find((*m_val_vec_it));
	  if (found_val!=string::npos){
	    if(m_val_name_set.find(attr_name)==m_val_name_set.end()){
	      m_val_name_set.insert(attr_name);
	      break;
	    }
	  }
	}
      }
    }
  }
  
  if(m_val_name_set.size()==0) {
    cout << "Cannot find value in IS Names. Exiting configuration" << endl;
    my_ret = false;
    return(my_ret);
  }
  
  // now build the X axis

  std::cout << "X Axis is:" << std::endl;
  std::vector<std::string>::iterator m_obj_vec_it = m_obj_name_vec.begin();
  for(;m_obj_vec_it!=m_obj_name_vec.end();++m_obj_vec_it){ // loop on object names
    std::set<std::string>::iterator m_val_set_it = m_val_name_set.begin();
    for(;m_val_set_it!=m_val_name_set.end();++m_val_set_it){ // loop to find values 
      std::cout << "m_obj_vec_it:" << *m_obj_vec_it << "  val:" <<  *m_val_set_it << std::endl;
      string x_name = m_map_str[(*m_obj_vec_it)] + "_" + (*m_val_set_it);
      myXaxis.push_back(x_name);
      std::cout << "---:" << x_name  << std::endl;
    }
  }
  
  // now build the Y axis
  std::cout << "y axis is:" << std::endl;
  std::vector<int>::iterator m_id_vec_it = m_id_vec.begin();
  for(;m_id_vec_it!=m_id_vec.end();++m_id_vec_it){
    string y_name = boost::lexical_cast<std::string>(*m_id_vec_it);
    myYaxis.push_back(y_name);
    std::cout << "+++:" << y_name << std::endl;
  }
 
  m_tp_point.format(myXaxis,myYaxis);
  // determine the dimension of vector Data and MetaData of TimePoint_IS
  std::cout << "TimePoint has Row:" <<  m_tp_point.NumRow() << "  Col:" << 
    m_tp_point.NumCol() << std::endl;
  int dim_data = m_tp_point.NumCol() * m_tp_point.NumRow();
  std::cout << "TimePoint Data is:" << dim_data << std::endl;
  (m_tp_point.Data).reserve(dim_data);
  return(true);
  
}
  

void GenericAdapter::TestDany(std::string test_string,std::string test_string2 ){
  std::cout <<  "In TestDany" << std::endl;
  if(!mPartitionIn){
    std::cout << "Warning! Invalid pointer to mPartitionIn. Reinstantiating it";
    mPartitionIn = new IPCPartition( pPartitionNameIn );
  }
  //  mPartitionIn = new IPCPartition( pPartitionNameIn );

  ISInfoDictionary id( *mPartitionIn );
  
  ISInfoDynAny ida;
  id.getValue( test_string, ida );
  std::cout << ">>>>>>>>> getValue = ok" << std::endl;
  ida.print( std::cout);
  std::cout << std::endl;
  const std::vector<double> & pippo = ida.getAttributeValue<std::vector<double> >( test_string2 ); 
  size_t s = pippo.size();
  std::cout << "pippo.size is:" << s << std::endl;
  std::cout << "values:" << pippo[0] << "  " << pippo[2] << std::endl;
}





void GenericAdapter::Run(){

  if(m_count_event<=2){
    std::cout <<  "In Run" << std::endl;
    std::cout << "m_obj_name_vec.size is:" << m_obj_name_vec.size() << std::endl;
    std::cout << "m_val_name_set.size is:" << m_val_name_set.size() << std::endl;
  }

  if(m_mode == "legacy")
  {
    std::cout<<"Running legacy mode. Message appears every 10s\n";

    if(!mPartitionIn){
      std::cout << "Warning! Invalid pointer to mPartitionIn. Reinstantiating it";
      mPartitionIn = new IPCPartition( pPartitionNameIn );
    }
    //  mPartitionIn = new IPCPartition( pPartitionNameIn );

    // set the TP


    std::vector<std::string>::iterator m_obj_vec_it = m_obj_name_vec.begin();
    for(;m_obj_vec_it!=m_obj_name_vec.end();++m_obj_vec_it){ // loop on object names
      std::set<std::string>::iterator m_val_set_it = m_val_name_set.begin();
      for(;m_val_set_it!=m_val_name_set.end();++m_val_set_it){ // loop to find values 
        if(m_mode=="debug"|| m_count_event<=1)
          std::cout << "FillTP(" << *m_obj_vec_it << "," << *m_val_set_it << ")" << std::endl;
        legacyFillTP(*m_obj_vec_it, *m_val_set_it);
      }
    }

    if(m_mode=="debug"|| m_count_event<=1)
      std::cout << "Now publishing TP" << std::endl;

    unsigned int my_run = 0;
    unsigned short my_lb =  0;
    trp_utils::GetRunParams(pPartitionNameIn, my_run, my_lb);
    m_tp_point.RunNumber = my_run;
    m_tp_point.LumiBlock = my_lb;

    OWLTime o_time;
    trp_utils::GetTime(o_time);
    m_tp_point.TimeStamp =  o_time;
    if(m_mode=="debug" || m_count_event<=1){
      m_tp_point.print_as_table(std::cout);
    }
    // now publish
    if(m_mode!="debug"){
      if(m_count_event<=2)
        std::cout << "Now publishing!" << std::endl;
      if(!mPartitionOut){
        std::cout << "Warning! Invalid pointer to mPartitionOut. Reinstantiating it";
        mPartitionOut = new IPCPartition( pPartitionNameOut );
      }
      //    mPartitionOut = new IPCPartition( pPartitionNameOut );

      ISInfoDictionary *dict;

      dict = NULL;

      try{
        dict = new ISInfoDictionary(*mPartitionOut );
      } catch(...){
        ers::warning(genericadapter::Issue(ERS_HERE, "IS Cant Instantiate ISInfoDictionary "));
      }

      std::string pub_name = pServerNameOut + ".";
      pub_name += pNameOut;
      if(m_count_event<=2)
        std::cout << "Pub name is:" << pub_name << std::endl;

      try {
        dict->checkin(pub_name,m_tp_point, true);
      } catch(daq::is::InvalidName &ex){
        ers::warning(genericadapter::Issue(ERS_HERE, "ISInfoDictionary  Invalid Name" +boost::lexical_cast<std::string>( ex.what())));
      } catch(daq::is::RepositoryNotFound &ex) {
        ers::warning(genericadapter::Issue(ERS_HERE, "Repository not found" +boost::lexical_cast<std::string>( ex.what())));
      } catch(daq::is::InfoNotCompatible &ex){
        ers::warning(genericadapter::Issue(ERS_HERE, "Info not compatible" +boost::lexical_cast<std::string>( ex.what())));
      }
      delete dict;
    }
    ++m_count_event;

  }
  else if (m_mode == "run") 
  {
    std::cout<<"Running normal Run2  mode. Message appears once\n";

    if(!mPartitionIn){
      std::cout << "Warning! Invalid pointer to mPartitionIn. Reinstantiating it";
      mPartitionIn = new IPCPartition( pPartitionNameIn );
    }

    IPCPartition Partition(pPartitionNameIn);
    ISInfoReceiver receiver(Partition);
    Receiver = &receiver;
    OWLSemaphore s;
    semaphore = &s;


    //Subscribe to changes of DF.TopMIG-IS:HLT.info
    try {

      ERS_LOG("Setting up receiver.");
      Receiver->subscribe("DF.TopMIG-IS:HLT.info", &GenericAdapter::DFcallback_fillTP, this);
      //Receiver->subscribe("L1CT.CTPCORE.Instantaneous.TriggerRates", &GenericAdapter::L1CTcallback_CTPCORE_Run3, this);
    } catch( daq::is::RepositoryNotFound &ex) {
      ers::warning(genericadapter::Issue(ERS_HERE, "Repository not found: "
            +boost::lexical_cast<std::string>(ex.what())));
    } catch (daq::is::AlreadySubscribed &ex) {
      ers::warning(genericadapter::Issue(ERS_HERE, "Already subscribed: "
            +boost::lexical_cast<std::string>(ex.what())));
    } catch ( daq::is::InvalidCriteria &ex) {
      ers::warning(genericadapter::Issue(ERS_HERE, "Invalid Criteria: "
            +boost::lexical_cast<std::string>(ex.what())));
    }

    semaphore->wait();
    semaphore = nullptr;

  }

} // end Run Method


      

void GenericAdapter::SearchInfoAny(string obj_name, string val_name){
  std::cout << "In SearchInfoAny" << std::endl;
  if(!mPartitionIn){
    std::cout << "Warning! Invalid pointer to mPartitionIn. Reinstantiating it";
    mPartitionIn = new IPCPartition( pPartitionNameIn );
  }
  //  mPartitionIn = new IPCPartition( pPartitionNameIn );
  std::cout << "obj_name = " << obj_name << "  val_name = " << val_name << std::endl;
  ISCriteria m_criteria(obj_name);
  ISInfoIterator m_InfoIt( *mPartitionIn, pServerNameIn, m_criteria );
  // ISInfoIterator m_InfoIt( *mPartitionIn, pServerNameIn);
  // reads the object from the server and establish what to plot  
  while( (m_InfoIt)() ){ // loop on is servers
    ISInfoAny isa;
    m_InfoIt.value( isa );
    auto isd = std::make_unique<ISInfoDocument>( *mPartitionIn, isa.type());
    size_t count = isa.type().entries();
    std::cout << "count is:"<< count << std::endl;
    //   isa.reset();
    for ( size_t i = 0; i < count; i++ ){ //loop on attributes
#ifndef TDAQ_NIGHTLY
      const ISInfoDocument::Attribute  * attr = isd.get() ? isd->attribute( i ) : 0; // tdaq-03-00-01
      string attr_name = attr->name().c_str(); // tdaq03-00-01
#endif
#ifdef TDAQ_NIGHTLY
      const ISInfoDocument::Attribute  & attr = isd->attribute( i ); // nightly
      string attr_name = attr.name().c_str(); // nightly
#endif
      std::cout << "attr_name = " << attr_name  << "  val_name = " << val_name << std::endl;
      if(attr_name.compare(val_name)!=0) continue;
      std::cout << "count " << i << std::endl;
      if(isa.getAttributeType()==ISType::String)
	ISGetVal<std::string>::dummy_get(isa);
      if(isa.getAttributeType()==ISType::Date)
	ISGetVal<OWLDate>::dummy_get(isa);
      if(isa.getAttributeType()==ISType::Time)
	ISGetVal<OWLTime>::dummy_get(isa);
      
      std::vector<float> my_val;
      if(isa.getAttributeType()==ISType::Float)
	my_val = ISGetVal<float>::get(isa,m_id_vec);
      else if(isa.getAttributeType()==ISType::Double)
	my_val = ISGetVal<double>::get(isa,m_id_vec);
      else if(isa.getAttributeType()==ISType::Boolean)
	my_val = ISGetVal<bool>::get(isa,m_id_vec);
      else if(isa.getAttributeType()==ISType::S8 ||isa.getAttributeType()==ISType::U8 || 
	      isa.getAttributeType()==ISType::S16 )
	my_val = ISGetVal<int16_t>::get(isa,m_id_vec);
      else if(isa.getAttributeType()==ISType::U16){
	my_val = ISGetVal<uint16_t>::get(isa,m_id_vec);
      }
      else if(isa.getAttributeType()==ISType::S32){
	my_val = ISGetVal<int32_t>::get(isa,m_id_vec);
      }
      else if(isa.getAttributeType()==ISType::U32)
	my_val = ISGetVal<uint32_t>::get(isa,m_id_vec);
      
      
      if(my_val.size()>=1){
	if(my_val.size()<(unsigned int)(m_id_vec[m_id_vec.size()-1])){
	  ers::warning(genericadapter::
		       Issue(ERS_HERE, "Vector mismatch! "));
	  std::vector<float>::iterator my_val_vec_it = my_val.begin();
	  for(;my_val_vec_it!=my_val.end();++my_val_vec_it){
	    std::cout << "****" << *my_val_vec_it;
	  }
	  std::cout << endl;
	}
	std::vector<int>::iterator m_id_vec_it = m_id_vec.begin();
	std::vector<float>::iterator my_val_vec_it = my_val.begin();
	for(;m_id_vec_it!=m_id_vec.end();++m_id_vec_it,++my_val_vec_it){
	  if(my_val_vec_it==my_val.end()) break;
	  string m_id_vec_it_str = boost::lexical_cast<std::string>(*m_id_vec_it);
	  std::cout << "new_name =  m_map_str[" << obj_name << "] = " << m_map_str[obj_name] << std::endl;
	  string new_name = m_map_str[obj_name];
	  //	  string x_name = m_map_str[(*m_obj_vec_it)] + "_" + (*m_val_set_it);
	  string x_name = new_name + "_" + val_name;
	  string y_name = m_id_vec_it_str;
	  std::cout << "setting time point x_axis: " << x_name << "  y_axis:" << y_name << "  val = " <<   (float)(*my_val_vec_it) << std::endl;
	  m_tp_point.set(x_name,y_name, (float)(*my_val_vec_it));
	}
      } else {
	ers::warning(genericadapter::
		     Issue(ERS_HERE, "Can't find any item for " + 
			   boost::lexical_cast<std::string>( val_name )));
      }
    }
  }
}


void GenericAdapter::legacyFillTP(string obj_name, string val_name){
  
  if(m_mode=="debug"|| m_count_event<=1)
    std::cout << "In FillTP" << std::endl;
  if(!mPartitionIn){
    std::cout << "Warning! Invalid pointer to mPartitionIn. Reinstantiating it";
    mPartitionIn = new IPCPartition( pPartitionNameIn );
  }
  //  mPartitionIn = new IPCPartition( pPartitionNameIn );

  if(m_mode=="debug"|| m_count_event<=1)
    std::cout << "obj_name = " << obj_name << "  val_name = " << val_name << std::endl;

  ISCriteria m_criteria(obj_name);
  ISInfoIterator m_InfoIt( *mPartitionIn, pServerNameIn, m_criteria );
  // ISInfoIterator m_InfoIt( *mPartitionIn, pServerNameIn);
  // reads the object from the server and establish what to plot  
  while( (m_InfoIt)() ){ // loop on is servers
    
    ISInfoDynAny ida;
    std::string info_name = pPartitionNameIn + "." + obj_name;
    m_InfoIt.value( ida );
    ISInfoDocument isdoc(*mPartitionIn, ida.type() );
    size_t att_pos = isdoc.attributePosition(val_name );
    ISType::Basic my_type = ida.getAttributeType( att_pos);

    std::vector<float> my_val;
    switch (my_type){
    case ISType::Boolean:
      my_val = ISDynVal<bool>::get(ida,m_id_vec,val_name,att_pos);
      break;
    case ISType::S8:
      my_val = ISDynVal<int16_t>::get(ida,m_id_vec,val_name,att_pos);
      break;
    case ISType::U8:
      my_val = ISDynVal<uint16_t>::get(ida,m_id_vec,val_name,att_pos);
      break;
    case ISType::S16:
      my_val = ISDynVal<int16_t>::get(ida,m_id_vec,val_name,att_pos);
      break;
    case ISType::U16:
      my_val = ISDynVal<uint16_t>::get(ida,m_id_vec,val_name,att_pos);
      break;
    case ISType::S32:
      my_val = ISDynVal<int32_t>::get(ida,m_id_vec,val_name,att_pos);
      break;
    case ISType::U32:
      my_val = ISDynVal<uint32_t>::get(ida,m_id_vec,val_name,att_pos);
      break;
    case ISType::Float:
      my_val = ISDynVal<float>::get(ida,m_id_vec,val_name,att_pos);
      break;
    case ISType::Double:
      my_val = ISDynVal<double>::get(ida,m_id_vec,val_name,att_pos);
      break;
    case ISType::String:
      ers::warning(genericadapter::Issue(ERS_HERE, "Cannot convert string to float"));
      //std::cout << " { ERROR: Invalid Type } " << std::endl;
      //  my_val = ISDynVal<std::string>::get(ida,m_id_vec,val_name,att_pos);
      break;
    default:
      break;
      //ers::warning(genericadapter::Issue(ERS_HERE, "Invalid Type"));
      //std::cout << " { ERROR: Invalid Type } " << std::endl;
    } // end of switch
    
    // now fill the TimePoint
    std::vector<int>::iterator m_id_vec_it = m_id_vec.begin();
    std::vector<float>::iterator my_val_vec_it = my_val.begin();
    for(;m_id_vec_it!=m_id_vec.end();++m_id_vec_it,++my_val_vec_it){
      if(my_val_vec_it==my_val.end()) break;
      string m_id_vec_it_str = boost::lexical_cast<std::string>(*m_id_vec_it);
      string new_name = m_map_str[obj_name];
      //	  string x_name = m_map_str[(*m_obj_vec_it)] + "_" + (*m_val_set_it);
      string x_name = new_name + "_" + val_name;
      string y_name = m_id_vec_it_str;
      m_tp_point.set(x_name,y_name, (float)(*my_val_vec_it));
    } // end of fill TP
  }// end of loop on servers
} // end of method




void GenericAdapter::DFcallback_fillTP(ISCallbackInfo *isc) {

  //ensuring that we do not publish more often than SleepTime
  ctpcore_newtime = isc->time();
  if ( ctpcore_newtime.c_time() - ctpcore_oldtime.c_time() < m_sleep_time) return; 


  //ensuring its thread safe
  if (m_is_publishing) return;
  m_is_publishing = true;



  std::vector<std::string>::iterator m_obj_vec_it = m_obj_name_vec.begin();
  for(;m_obj_vec_it!=m_obj_name_vec.end();++m_obj_vec_it){ // loop on object names
    std::set<std::string>::iterator m_val_set_it = m_val_name_set.begin();
    for(;m_val_set_it!=m_val_name_set.end();++m_val_set_it){ // loop to find values 
      if(m_mode=="debug"|| m_count_event<=1) std::cout << "FillTP(" << *m_obj_vec_it << "," << *m_val_set_it << ")" << std::endl;
      FillTP(*m_obj_vec_it, *m_val_set_it);
    }
  }

  if(m_mode=="debug"|| m_count_event<=1) std::cout << "Now publishing TP" << std::endl;

  unsigned int my_run = 0;
  unsigned short my_lb =  0;
  trp_utils::GetRunParams(pPartitionNameIn, my_run, my_lb);
  m_tp_point.RunNumber = my_run;
  m_tp_point.LumiBlock = my_lb;
  
  OWLTime o_time;
  trp_utils::GetTime(o_time);
  m_tp_point.TimeStamp =  o_time;
  if(m_mode=="debug" || m_count_event<=1){
    m_tp_point.print_as_table(std::cout);
  }
  // now publish
  if(m_mode!="debug"){
    if(m_count_event<=2)
      std::cout << "Now publishing!" << std::endl;
    if(!mPartitionOut){
      std::cout << "Warning! Invalid pointer to mPartitionOut. Reinstantiating it";
      mPartitionOut = new IPCPartition( pPartitionNameOut );
    }
    //    mPartitionOut = new IPCPartition( pPartitionNameOut );
    
    ISInfoDictionary *dict;
    
    dict = NULL;
    
    try{
      dict = new ISInfoDictionary(*mPartitionOut );
    } catch(...){
      ers::warning(genericadapter::Issue(ERS_HERE, "IS Cant Instantiate ISInfoDictionary "));
    }
    
    std::string pub_name = pServerNameOut + ".";
    pub_name += pNameOut;
    if(m_count_event<=2)
      std::cout << "Pub name is:" << pub_name << std::endl;

    try {
      dict->checkin(pub_name,m_tp_point, true);
    } catch(daq::is::InvalidName &ex){
      ers::warning(genericadapter::Issue(ERS_HERE, "ISInfoDictionary  Invalid Name" +boost::lexical_cast<std::string>( ex.what())));
    } catch(daq::is::RepositoryNotFound &ex) {
      ers::warning(genericadapter::Issue(ERS_HERE, "Repository not found" +boost::lexical_cast<std::string>( ex.what())));
    } catch(daq::is::InfoNotCompatible &ex){
      ers::warning(genericadapter::Issue(ERS_HERE, "Info not compatible" +boost::lexical_cast<std::string>( ex.what())));
    }
    delete dict;
  }
  ++m_count_event;

  //now allowing other threads to enter the function as well
  m_is_publishing = false;
  ctpcore_oldtime = OWLTime();
}



void GenericAdapter::FillTP(string obj_name, string val_name){
  
  if(m_mode=="debug"|| m_count_event<=1)
    std::cout << "In FillTP" << std::endl;
  if(!mPartitionIn){
    std::cout << "Warning! Invalid pointer to mPartitionIn. Reinstantiating it";
    mPartitionIn = new IPCPartition( pPartitionNameIn );
  }
  //  mPartitionIn = new IPCPartition( pPartitionNameIn );

  if(m_mode=="debug"|| m_count_event<=1)
    std::cout << "obj_name = " << obj_name << "  val_name = " << val_name << std::endl;

  // Legacy code: ISInfoIterator is very inneficient. 
  //ISCriteria m_criteria(obj_name);
  //ISInfoIterator m_InfoIt( *mPartitionIn, pServerNameIn, m_criteria );

  ISInfoDictionary *dict { nullptr };
  ISInfoDynAny ida;
  std::string info_name = pPartitionNameIn + "." + obj_name;

  try{
    dict = new ISInfoDictionary(*mPartitionIn );
  } catch(...){
    ers::warning(genericadapter::Issue(ERS_HERE, "IS Cant Instantiate ISInfoDictionary "));
    return;
  }

  //ida = dict->getValue( obj_name, val_name );
  std::string full_obj_name = "DF." + obj_name;
  //std::cout<<"full object name: "<<full_obj_name<<std::endl;
  dict->getValue( full_obj_name, ida );


  ISInfoDocument isdoc(*mPartitionIn, ida.type() );
  size_t att_pos = isdoc.attributePosition(val_name );
  ISType::Basic my_type = ida.getAttributeType( att_pos);

  std::vector<float> my_val;
  switch (my_type){
    case ISType::Boolean:
      my_val = ISDynVal<bool>::get(ida,m_id_vec,val_name,att_pos);
      break;
    case ISType::S8:
      my_val = ISDynVal<int16_t>::get(ida,m_id_vec,val_name,att_pos);
      break;
    case ISType::U8:
      my_val = ISDynVal<uint16_t>::get(ida,m_id_vec,val_name,att_pos);
      break;
    case ISType::S16:
      my_val = ISDynVal<int16_t>::get(ida,m_id_vec,val_name,att_pos);
      break;
    case ISType::U16:
      my_val = ISDynVal<uint16_t>::get(ida,m_id_vec,val_name,att_pos);
      break;
    case ISType::S32:
      my_val = ISDynVal<int32_t>::get(ida,m_id_vec,val_name,att_pos);
      break;
    case ISType::U32:
      my_val = ISDynVal<uint32_t>::get(ida,m_id_vec,val_name,att_pos);
      break;
    case ISType::Float:
      my_val = ISDynVal<float>::get(ida,m_id_vec,val_name,att_pos);
      break;
    case ISType::Double:
      my_val = ISDynVal<double>::get(ida,m_id_vec,val_name,att_pos);
      break;
    case ISType::String:
      ers::warning(genericadapter::Issue(ERS_HERE, "Cannot convert string to float"));
      //std::cout << " { ERROR: Invalid Type } " << std::endl;
      //  my_val = ISDynVal<std::string>::get(ida,m_id_vec,val_name,att_pos);
      break;
    default:
      break;

      //ers::warning(genericadapter::Issue(ERS_HERE, "Invalid Type"));
      //std::cout << " { ERROR: Invalid Type } " << std::endl;
  } // end of switch
  // now fill the TimePoint
  std::vector<int>::iterator m_id_vec_it = m_id_vec.begin();
  std::vector<float>::iterator my_val_vec_it = my_val.begin();
  for(;m_id_vec_it!=m_id_vec.end();++m_id_vec_it,++my_val_vec_it){
    if(my_val_vec_it==my_val.end()) break;
    string new_name = m_map_str[obj_name];
    string x_name = new_name + "_" + val_name;
    string m_id_vec_it_str = boost::lexical_cast<std::string>(*m_id_vec_it);
    string y_name = m_id_vec_it_str;
    m_tp_point.set(x_name,y_name, (float)(*my_val_vec_it));
  }


    delete dict;

} // end of method




template <class T>
std::vector<float> ISDynVal<T>::get(ISInfoDynAny & ida,std::vector<int> m_id_vec,std::string val_name,
				    size_t att_pos){

  std::vector<float> out_vec;
  //  ida.print(std::cout);
  if ( !ida.isAttributeArray( att_pos )  ){
    T & value = ida.getAttributeValue< T >( att_pos );
    //    std::cout << "Value = " << value << std:endl;
    float single_val = boost::numeric_cast<float>(value);
    out_vec.push_back(single_val);
  }
  if(att_pos==9999 ||  ida.isAttributeArray( att_pos )){
    std::vector <T>  & value =  ida.getAttributeValue<std::vector<T> >( val_name ); 
    // now perform the cast
    std::vector<int>::iterator id_vec_it = m_id_vec.begin();
    for(;id_vec_it!=m_id_vec.end();++id_vec_it){
      int single_index = *id_vec_it;
      float single_val = boost::numeric_cast<float>(value[single_index]); 
      out_vec.push_back(single_val);
    }
  }
  return(out_vec);
}
  

