// ------------------------------
// runs the various trp adapters that store info into the trpIS 
// stolen from DAQ/DataFlow/efd/src/efd.cxx
// Author: Antonio Sidoti - Humboldt Universitat zu Berlin
// 
// Date: 01/12/2008
// ------------------------------


#include <iostream>
#include <fstream>
#include <signal.h>

#include "is/inforeceiver.h"


#include "ers/ers.h"
#include "TRP/Args.h"

#include "config/Configuration.h"
#include "ipc/partition.h"
#include "ipc/core.h"
#include "boost/smart_ptr.hpp"
#include "GenericAdapter.h" 
#include "TRP/ReadXML.h"
#include "TRP/Utils.h"



#undef TRP_DEBUG
#define TRP_DEBUG(lvl,msg) //cout << lvl << " ** " << msg << endl


const std::string my_separator = "|";

std::string partition_name_in="ATLAS"; // usually these two are the same
std::string server_name_in = "L1TriggerRates";
std::string partition_name_out="ATLAS";
std::string server_name_out = "ISS_TRP";
std::string name_out = "Test_Rate";
std::string mode = "real"; // or poll
float sleep_time = 10.; // for poll only
std::string object_name = "L1Muon.TGC.*|L1Calo.PPM.*"; // accept regular expressions and objects separated by my_separator
std::string replace_name =  "Muon|Calo"; // accept regular expressions and objects separated by my_separator
std::string value_name = "m_active_channels"; // accept regular expressions and objects separated by my_separator
std::string index_id = "1|2-5"; // single or mutliple index for vectors separated by : or -
std::string test_string = "pippo";
std::string test_string2 = "pippo";

void My_Stop(int sig){  
  std::cout << " I have received a SIG : " << sig << std::endl;
  //  if(sig==6) {
  std::cout << "Kill Kill Bang Bang" << endl;
  exit(0);
  //   }
//   IPCPartition *mPartition;
//   try {
//     mPartition = new IPCPartition(partition_name_in);
//   } catch(ers::Issue & ex) {
//     ers::error(ex);
//     exit(0);
//   }
//   ISInfoReceiver *rec;
//   try {
//     rec = new ISInfoReceiver(mPartition);
//   } catch(ers::Issue & ex) {
//     ers::error(ex);
//     exit(0);
//   }
//   try {
//     rec->unsubscribe(server_name_in);
//   }  catch(ers::Issue & ex) {
//     ers::error(ex);
//     exit(0);
//   }
//   exit(0);
}



void ReadConfig(std::string  config_file){
  std::cout << "ReadConfig(" << config_file << " ) " << std::endl;

  
  ReadXML my_reader;
  my_reader.SetElement("genericadapter");
  std::vector<std::string> my_str;
  my_str.push_back("ServerIn");
  my_str.push_back("OutputName");
  my_str.push_back("Mode");
  my_str.push_back("SleepTime");
  my_str.push_back("ObjectName");
  my_str.push_back("ReplaceName");
  my_str.push_back("ValueName");
  my_str.push_back("IndexId");

  my_reader.SetAttribute(my_str);

  my_reader.readConfigFile(config_file);
  
  server_name_out = my_reader.GetValC("server");
  
  server_name_in = my_reader.GetVal("ServerIn");
  name_out  = my_reader.GetVal("OutputName");
  replace_name  = my_reader.GetVal("ReplaceName");
  mode = my_reader.GetVal("Mode");
  sleep_time = atof(my_reader.GetVal("SleepTime").c_str());
  object_name  = my_reader.GetVal("ObjectName");
  value_name  = my_reader.GetVal("ValueName");  
  index_id  = my_reader.GetVal("IndexId");  
}


void showHelp( std::ostream& os, std::string pgmName ){
  os << "Usage: " << pgmName << " [-p <str>]  [-po <str>] [-c <file.xml>]" << std::endl;
  os << "                                                                      " << std::endl;
  os << "Options/Arguments:                                                    " << std::endl;
  os << "  -p <str>   partition name (Input)  (if not present $TDAQ_PARTITION is used) " << std::endl;
  os << "  -po <str>   partition name (Output)  (if not present the one specified by -p is used. If not present $TDAQ_PARTITION is used) " << std::endl;
  os << "  -c <file1.xml:file2.xml:...>   reads info from xml file instead. Picks info from existing file" << std::endl;
  os << "                                                                      " << std::endl;
  os << "Description:                                                          " << std::endl;
  os << "   The GenericAdapter gets infos from IS from configuration            " << std::endl;
  os << "   gets LumiBlock and Run Number and transforms them in TimePoints" << std::endl; 
   exit(1);
}


int main( int ac, char* av[] ){
  signal(SIGABRT, &My_Stop);
  signal(SIGTERM, &My_Stop);
  signal(SIGINT,  &My_Stop);

  
  try{
    
    // get arguments
    Args args( ac, av );
    
    // get program name
    std::string pgmName = av[0];
    std::string::size_type pos = pgmName.rfind( '/' );
    if( pos != std::string::npos )
      pgmName = pgmName.substr( pos+1 );
    

    // check if requested help
    if( args.hasArg("-h")     || 
        args.hasArg("-?")     || 
        args.hasArg("--help") || 
        ac <= 1  )
      showHelp( std::cerr, pgmName );
    
    
    // reconstruct command line string for debugging feedback
    std::stringstream cmdLine;
    cmdLine << av[0];
    for( int i = 1; i < ac; ++i )
      cmdLine << " " << av[i];
    std::cout << "cmdline: '" << cmdLine.str() << "'" << std::endl;
    
    // Retrive the partition name (from command line or from environment) 
    if (args.hasArg("-p")) { // from where you read
      partition_name_in = args.getStr("-p");
      TRP_DEBUG(0, "Retrieved partition name from command line: '" << partition_name_in << "'");
      std::cout << "Retrieved partition name from command line: " << partition_name_in << std::endl;
    } else if (getenv("TDAQ_PARTITION")) {
      partition_name_in = getenv("TDAQ_PARTITION");
      TRP_DEBUG(0, "Retrieved partition name from environment: '" << partition_name_in << "'");
    } else {
      std::cerr << "Partition name not set (neither in command line nor in environment)" << std::endl;
      showHelp( std::cerr, pgmName );
    }
     if (args.hasArg("-po")) { // where you publish
      partition_name_out = args.getStr("-po");
      TRP_DEBUG(0, "Retrieved partition name where you publish from command line: '" << partition_name_out << "'");
      std::cout << "Retrieved partition name where you publish from command line: " << partition_name_out << std::endl;
     } else {
       partition_name_out = partition_name_in;
     }
    
    
    std::string config_file;
    if (args.hasArg("-c")) {
      config_file  = args.getStr("-c");
      ERS_DEBUG(0, "Config file from  command line: '" << config_file  << "'");
      std::cout<< "Config file from  command line: " << config_file  << std::endl;
      std::vector<std::string> file_list; 
      trp_utils::tokenize(config_file,file_list,":");
      std::cout << "File_list size is:" << file_list.size() << std::endl;
      for(std::vector<std::string>::iterator it=file_list.begin();
	  it!=file_list.end();++it){
	const char* it_cstr = it->c_str();
	std::ifstream my_file(it_cstr);
	if(my_file.is_open()){
	  // here reads the file .xml
	  ReadConfig(*it);
	  break;
	}
      }
    } else {
      std::cerr << "No configuration file set in command line" << std::endl;
      showHelp( std::cerr, pgmName );
    } 
    
    
    // Dumping config
    std::cout << "Dumping Configurations for GenericAdapter:" << std::endl;
    std::cout << partition_name_in << " ,  " << partition_name_out << " , " << server_name_in << " , " << server_name_out << std::endl;
    std::cout << "config_file = " << config_file << std::endl;
    
    TRP_DEBUG(0, "Starting program job" );
    GenericAdapter my_adapter(partition_name_in,server_name_in,
			      partition_name_out,server_name_out,
			      name_out, mode, sleep_time, 
			      object_name, replace_name, value_name, index_id,
			      my_separator);
    IPCCore::init( ac, av );
    bool my_ret = my_adapter.Configure();
    //    my_adapter.TestDany(test_string,test_string2);
    if(!my_ret) {
      cout << "Cannot configure: Exiting application" << endl;
      return 0;
    }
    else {
      while(1){
	my_adapter.Run();
	sleep(sleep_time);
      }
      //    my_adapter.Run(mode);    
      TRP_DEBUG(0, "Program terminating gracefully" );
    }
  }
  
  catch( ers::Issue &ex)   { TRP_DEBUG( CoreIssue, "Caught ers::Issue: " << ex ); }
  catch( std::exception& e) { TRP_DEBUG( CoreIssue, "Caught std::exception: " << e.what() ); }
  catch( ... )             { TRP_DEBUG( CoreIssue, "Caught unidentified exception" );
  }
  return 0;
}

