/** 
 * @file LVL1RateAdapter.h
 * @author Antonio Sidoti
 * @date 28/11/2008
 * @brief Triger Level 1 classes for obtaining the data from IS.
 * @This reads from LVL1 IS and stores in TripIS
 * @Inspired from TDataManagerLvl1.h and .cpp
 */
 
#ifndef genericadapter_h
#define genericadapter_h


#include <iostream>

#include <ctime>

#include <vector>
#include <deque>
#include <utility>
#include <algorithm>
#include <string>
#include <set>

#include <unistd.h>
#include <stdio.h>
#include <signal.h>


#include "ipc/core.h"
#include "ipc/partition.h"


#include "is/infoany.h"
#include "is/infodictionary.h"
#include "is/infoT.h"
#include "is/info.h"
#include "is/infoiterator.h"
#include "is/inforeceiver.h"
#include "is/serveriterator.h"
#include "is/infodocument.h"
#include "is/criteria.h"
#include "TRP/TimePoint_IS.h"
#include "is/infodynany.h"
#include "owl/semaphore.h"

using std::string;
using std::vector;
using std::deque;
using std::pair;
using std::cout;
using std::cerr;
using std::endl;
using std::set;


template <class T>
class ISDynVal {
 public:
  static std::vector<float> get(ISInfoDynAny & ida,std::vector<int> m_id_vec, 
				std::string val_name, size_t att_pos=9999);
};

template <class T>
class ISGetVal {
 public:
  static inline void dummy_get(ISInfoAny & isa){
    if( !isa.isAttributeArray()){
      T value;
      isa >> value;
    } else {
      std::vector <T>  value;
      isa >> value;
    }
  }
  
  static inline std::vector<float> get(ISInfoAny & isa, 
				       std::vector<int> & id_vec){
    std::vector<float> out_vec;
    std::vector<int> out_id_vec;
    //    std::cout << "Dumping isa:" << isa << std::endl;
    if( !isa.isAttributeArray()){
      std::cout << "Is NOT AttributeArray" << std::endl;
      T value;
      isa >> value;
      float my_out = value;
      out_vec.push_back(my_out);
      std::cout << my_out << " ---- ";
    } else {
      std::cout << "Is  AttributeArray" << std::endl;
      std::vector <T>  value;
      isa >> value;
      
      for(size_t i=0; i<value.size();++i){
	std::vector<int>::iterator id_vec_it = id_vec.begin();
	for(;id_vec_it!=id_vec.end();++id_vec_it){
	  if(i==(unsigned int)(*id_vec_it)){
	    float my_out = value[i];
	    std::cout << my_out << " ==== ";
	    out_vec.push_back(my_out);
	    out_id_vec.push_back(i);
	    break;
	  }
	}
      }
    }
    std::cout << std::endl;
    //    id_vec = out_id_vec;
    return out_vec;
  }
};


/**
 * @brief GenericAdapter reads values and transforms them in rates.
 */
class GenericAdapter {
  
 public:
  
  bool Configure();
  
  void Run();
  
  void TestDany(string test_string, string test_string2);
  GenericAdapter(std::string partition_name_in, std::string server_name_in,
		 std::string partition_name_out, std::string server_name_out,
		 std::string name_out, std::string mode, float sleep_time, 
		 std::string object_name, std::string replace_name, std::string value_name, std::string index_id,
		 const std::string my_separator);
  ~GenericAdapter();	// Way of implementing the singleton pattern.
  GenericAdapter( const GenericAdapter & );	// Way of implementing the singleton pattern.

void DFcallback_fillTP(ISCallbackInfo *isc);
void L1CTcallback_CTPCORE_Run3(ISCallbackInfo *isc);

 private:
  
  bool ConfigureTP();
  void SearchInfoAny(string obj_name, string val_name);
  void FillTP(string obj_name, string val_name);
  void legacyFillTP(string obj_name, string val_name);
  
  std::string pPartitionNameIn;
  std::string pPartitionNameOut;
  std::string pServerNameIn;
  std::string pServerNameOut;
  std::string pNameOut;
  std::string pValeOut;
  std::string m_mode;
  float m_sleep_time;
  std::vector<std::string> m_obj_vec; // these might be with regexp
  std::vector<std::string> m_rep_vec; // these might be with regexp
  std::vector<std::string> m_val_vec; // these might be with regexp
  std::vector<int> m_id_vec;

  typedef std::map<string, float> MapVal;
  MapVal m_map_val;
  typedef std::map<string, string> MapString;
  MapString m_map_str;

  std::vector<std::string> m_obj_name_vec; // these are AFTER regexp
  std::set<std::string> m_val_name_set; // these are AFTER regexp
  std::string m_separator;
  
  IPCPartition * mPartitionIn;	// Partition object, provides connection to the IS.
  IPCPartition * mPartitionOut;	// Partition object, provides connection to the IS.
  ISInfoReceiver * mReceiver;	// Used for callback registration.
  //  ISInfoIterator *m_InfoIt; // Used to poll 
  TimePoint_IS m_tp_point;

  std::vector<std::string>  myXaxis; 
  std::vector<std::string>  myYaxis; 
  
  ISInfoAny m_isa;
  
  //  ISCriteria *m_criteria;
  ISCriteria *m_criteria2;
  
  int m_count_event;

  ISInfoReceiver   receiver;
  ISInfoReceiver * Receiver;
  OWLSemaphore*    semaphore; 

  bool m_is_publishing; 
  OWLTime ctpcore_newtime;
  OWLTime ctpcore_oldtime;


};



  


#endif


