
#include <iostream>

#include "ers/ers.h"
#include "TRP/Args.h"

#include "config/Configuration.h"
#include "ipc/partition.h"
#include <ipc/core.h>
#include "boost/smart_ptr.hpp"
#include "FarmAdapter.h"

#include "TRP/ReadXML.h"
#include "TRP/Utils.h"
#include <fstream>

std::string partition_name;
std::string server_name;
int farm_update = 300;
int rate_update = 60;
std::string output_name  = "";
std::string l2_seg_pat  = "DF-L2-([0-9]{1})-rack-(Y[0-9]{2}-[0-9]{2}D[0-9]).*";
std::string l2_sv_pat   = "DF-L2SV-Segment-([0-9]{1,2}).*L2SV-([0-9]{1}).*";
std::string ef_rack_pat =  "DF-EF-Segment-([0-9]{1,2})-rack-(Y[0-9]{2}-[0-9]{2}D[0-9]).*";
std::string pmg_l2_pat =  ".*(pc-tdq-xpu-[0-9]{4}).(L2PU-[0-9]{4})";
std::string pmg_ef_pat =  ".*(pc-tdq-xpu-[0-9]{4}).*\\|.PT.*:EF.*(pc-tdq-xpu-.*:.)";
std::string proc_l2 = "(L2PU-[0-9]{4}).*";
std::string proc_ef = "PT-1:EF-Segment.*:(pc-tdq-xpu-[0-9]{4}):.";

std::string threadPer = "0";
std::string maxServerThread = "9";
std::string threadPool = "0";
std::string maxGIOP =  "7";

std::list< std::pair< std::string, std::string > > options_ipc;

void ReadConfig(std::string  config_file){
  std::cout << "ReadConfig(" << config_file << " ) " << std::endl;
  ReadXML my_reader;
  my_reader.SetElement("farmadapter");
  std::vector<std::string> my_str;
  my_str.push_back("farm_update");
  my_str.push_back("rate_update");
  my_str.push_back("outputname");
  my_str.push_back("l2_seg_pat");
  my_str.push_back("l2_sv_pat");
  my_str.push_back("ef_rack_pat");
  my_str.push_back("pmg_l2_pat");
  my_str.push_back("pmg_ef_pat");
  my_str.push_back("proc_l2_pat");
  my_str.push_back("proc_ef_pat");
  
  
  my_reader.SetAttribute(my_str);
  
  my_reader.readConfigFile(config_file);
  
  //  partition_name = my_reader.GetValC("partition");
  server_name = my_reader.GetValC("server");
  farm_update = trp_utils::convertToInt(my_reader.GetVal("farm_update"));
  rate_update = trp_utils::convertToInt(my_reader.GetVal("rate_update"));
  output_name = my_reader.GetVal("outputname");
  l2_seg_pat  = my_reader.GetVal("l2_seg_pat");
  l2_sv_pat   = my_reader.GetVal("l2_sv_pat");
  ef_rack_pat = my_reader.GetVal("ef_rack_pat");
  pmg_l2_pat  = my_reader.GetVal("pmg_l2_pat");
  pmg_ef_pat  = my_reader.GetVal("pmg_ef_pat");
  proc_l2     = my_reader.GetVal("proc_l2_pat");
  proc_ef     = my_reader.GetVal("proc_ef_pat");
  

  // read ipc options
  ReadXML my_reader_ipc;
  my_reader_ipc.SetElement("ipcoptions");
  std::vector<std::string> my_str_ipc;
  my_str_ipc.push_back("threadPerConnectionPolicy");
  my_str_ipc.push_back("maxServerThreadPoolSize");
  my_str_ipc.push_back("threadPoolWatchConnection");
  my_str_ipc.push_back("maxGIOPConnectionPerServer");
    
  my_reader_ipc.SetAttribute(my_str_ipc);
  
  my_reader_ipc.readConfigFile(config_file);
  
  //  partition_name = my_reader.GetValC("partition");
  threadPer = my_reader_ipc.GetVal("threadPerConnectionPolicy");
  maxServerThread = my_reader_ipc.GetVal("maxServerThreadPoolSize");
  threadPool = my_reader_ipc.GetVal("threadPoolWatchConnection");
  maxGIOP = my_reader_ipc.GetVal("maxGIOPConnectionPerServer");
  
  options_ipc.push_front( std::make_pair( std::string("threadPerConnectionPolicy"),threadPer  ) );
  options_ipc.push_front( std::make_pair( std::string("maxServerThreadPoolSize"), maxServerThread ) );
  options_ipc.push_front( std::make_pair( std::string("threadPoolWatchConnection"), threadPool ) );
  options_ipc.push_front( std::make_pair( std::string("maxGIOPConnectionPerServer"), maxGIOP ) );
  
}


void showHelp( std::ostream& os, std::string pgmName ){
  os << "Usage: " << pgmName << " [-p <str>]  [-c <file1.xml:file2.xml>] [-s <str>]   " << std::endl;
  os << "                                                                      " << std::endl;
  os << "Options/Arguments:                                                    " << std::endl;
  os << "  -p <str>   partition name  (if not present $TDAQ_PARTITION is used) " << std::endl;
  os << "  -c <file1.xml:file2.xml:...>   reads info from xml file instead. Picks info from existing file" << std::endl;
  os << "                                                                      " << std::endl;
  os << "  or in alternative from the command line as follows:     " << std::endl;
  os << "  -s <str>   server name                                              " << std::endl;
  os << "  -out <str>  suffix to add to TimePoint name                            " << std::endl;
  os << "Description:                                                          " << std::endl;
  os << "   The FarmAdapterMain reads the HLT Farm structure, stores it in IS  " << std::endl;
  os << "   then reads the PROC variable and store it in IS                    " << std::endl; 
  os << "Debug level and verbosity:                                            " << std::endl;
  os << "   They are controlled via the TDAQ_ERS shell variables:              " << std::endl; 
  os << "     - $TDAQ_ERS_DEBUG_LEVEL        [0-4]                             " << std::endl; 
  os << "     - $TDAQ_ERS_VERBOSITY_LEVEL    [0-3]                             " << std::endl;
  os << "     - $TDAQ_ERS_FATAL              [eg: 'lsterr,throw']              " << std::endl;   
  os << "     - $TDAQ_ERS_ERROR                                                " << std::endl;
  os << "     - $TDAQ_ERS_WARNING                                              " << std::endl;
  os << "     - $TDAQ_ERS_INFO               [eg: 'filter(efd),lstdout']       " << std::endl;
  os << "     - $TDAQ_ERS_DEBUG                                                " << std::endl;
  exit(1);
}

int main(int ac, char* av[] ){
  try{
    
    // get arguments
    Args args( ac, av );
    
    // get program name
    std::string pgmName = av[0];
    std::string::size_type pos = pgmName.rfind( '/' );
    if( pos != std::string::npos )
      pgmName = pgmName.substr( pos+1 );
    

    // check if requested help
    if( args.hasArg("-h")     || 
        args.hasArg("-?")     || 
        args.hasArg("--help") || 
        ac <= 1  )
//           || 
//	!args.hasArg("-n"))
      { showHelp( std::cerr, pgmName ); }
    
    // reconstruct command line string for debugging feedback
    std::stringstream cmdLine;
    cmdLine << av[0];
    for( int i = 1; i < ac; ++i )
      cmdLine << " " << av[i];
    std::cout << "cmdline: '" << cmdLine.str() << "'" << std::endl;
// Retrive the partition name (from command line or from environment) 
    if (args.hasArg("-p")) {
      partition_name = args.getStr("-p");
      std::cout << "Retrieved partition name from command line: " << partition_name << std::endl;
    } else if (getenv("TDAQ_PARTITION")) {
      partition_name = getenv("TDAQ_PARTITION");
    } else {
      std::cerr << "Partition name not set (neither in command line nor in environment)" << std::endl;
      showHelp( std::cerr, pgmName );
    }
    std::string config_file;
    if (args.hasArg("-c")) {
      config_file  = args.getStr("-c");
      ERS_DEBUG(0, "Config file from  command line: '" << config_file  << "'");
      std::cout<< "Config file from  command line: " << config_file  << std::endl;
      std::vector<std::string> file_list; 
      //      for (unsigned int i = 0; i < config_file.size(); ++i) {
      //      file_list = trp_utils::split(config_file,"\:");
      trp_utils::tokenize(config_file,file_list,":");
      // }
      std::cout << "File_list size is:" << file_list.size() << std::endl;
      for(std::vector<std::string>::iterator it=file_list.begin();
	  it!=file_list.end();++it){
	const char* it_cstr = it->c_str();
	std::ifstream my_file(it_cstr);
	if(my_file.is_open()){
	  // here reads the file .xml
	  ReadConfig(*it);
	  break;
	}
      }
      
    } else {
      
      // Retreive the server name (from command line only) 
      if (args.hasArg("-s")) {
	server_name = args.getStr("-s");
	ERS_DEBUG(0, "Retrieved server name from command line: '" << server_name << "'");
      } else { 
	std::cerr << "Server name not set (in command line)" << std::endl;
	showHelp( std::cerr, pgmName );
      }
      
    } // end read conf form xml
    
    
    std::cout << "Dumping the Farmadapter Config:" << std::endl;
    std::cout << partition_name << " ,  " << server_name << std::endl;
    std::cout << farm_update << " ,  " << rate_update << std::endl;
    std::cout << "output_name = " << output_name << std::endl;
    std::cout << "Config file is:" << config_file << std::endl;
    
    ERS_DEBUG(0, "Starting program job" );
    IPCCore::init( options_ipc );
    FarmAdapter my_farm_adapter (partition_name,server_name, output_name, 
				 l2_seg_pat, l2_sv_pat, ef_rack_pat, 
				 pmg_l2_pat, pmg_ef_pat, proc_l2, proc_ef,
				 farm_update,rate_update);
    my_farm_adapter.Configure();  
    while(1){
      my_farm_adapter.Run();
      sleep(rate_update);
    }
    ERS_DEBUG(0, "Program terminating gracefully" );
  }
  catch( ers::Issue &ex)   { ERS_DEBUG( 0, "Caught ers::Issue: " << ex ); }
  catch( std::exception& e) { ERS_DEBUG( 0, "Caught std::exception: " << e.what() ); }
  catch( ... )             { ERS_DEBUG( 0, "Caught unidentified exception" );
  }
  return 0;
 
    
}
