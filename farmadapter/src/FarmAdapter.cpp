/** 
 * @file FarmAdapter.cpp
 * @author Antonio Sidoti
 * @date 21/01/2009
 * @brief Gets info from HLT farms Inspired from Trip/TFarmManager
 */

#include "FarmAdapter.h"
#include <oh/OHUtil.h>

#include <is/info.h>
#include <is/infodictionary.h>
#include "is/serveriterator.h"
#include <is/infoiterator.h>
#include <ipc/core.h>
#include <iostream>
#include <string>
#include "ers/ers.h"
#include "boost/regex.hpp"


#include <ipc/core.h>
#include "TRP/Utils.h"
#include "TRP/Branch.h"
#include "TRP/Tree.h"

#include <rc/RunParams.h>
#include <TH2F.h>

#include "TRP/PMGPublishedAgentData.h"
#include "TRP/PMGPublishedProcessData.h"

#include "ers/ers.h"

ERS_DECLARE_ISSUE(farmadapter, Issue, "conversion issue", ERS_EMPTY)

//#define DEBUG_FARM

using namespace std;
using namespace boost;

FarmAdapter::FarmAdapter(std::string partition_name,
			 std::string server_name, std::string out_name,
			 std::string l2_seg, std::string l2_sv,
			 std::string ef_rack, std::string pmg_l2, 
			 std::string pmg_ef, std::string l2pu_proc, 
			 std::string efpt_proc, int farm_update, int val_update){
  ERS_DEBUG( 0, "FarmAdapter::FarmAdapter()" );
  
  
  t_farm_update = farm_update;
  t_val_update = val_update;
  pPartitionName = partition_name;
  pServerName = server_name;
  pOutName = out_name;
  L2Segment_pattern = l2_seg;
  L2Supervisor_pattern = l2_sv;
  mPMG_L2_pattern = pmg_l2;
  mPMG_EF_pattern = pmg_ef;
  mL2PU_PROC = l2pu_proc;
  mEFPT_PROC = efpt_proc;
    //                                      n                    m   efd  pt
  //                                     rack                sfi   efd  pt
  //  EFRack_pattern = "DF-EF-Segment-([0-9]{1,2})-rack-(Y[0-9]{2}-[0-9]{2}D[0-9]).*pc-tdq-xpu-([0-9]{4}):([0-9]{1,2}).*";
  EFRack_pattern = ef_rack;
  
  L2Segment_pattern_c = L2Segment_pattern.c_str();
  L2Supervisor_pattern_c = L2Supervisor_pattern.c_str();
  EFRack_pattern_c = EFRack_pattern.c_str();
  
  prev_t =  prev_pu_t = time(NULL);
  num_l2xpu = num_efxpu = 0;
  m_farms_changed = true;
  m_is_first_event = true;
  if(!mPartition)
    mPartition = new IPCPartition(pPartitionName);
  std::cout << "Farmadapter::Farmadapter:" << std::endl;
  std::cout << "l2_seg: " << L2Segment_pattern << std::endl;
  std::cout << "l2_sv: " << L2Supervisor_pattern << std::endl;
  std::cout << "ef_rack: " << EFRack_pattern << std::endl;
  std::cout << "PMG Pattern: " <<  mPMG_L2_pattern << "  " << mPMG_EF_pattern << std::endl;
  std::cout << "PROC Pattern: " <<  mL2PU_PROC << "  " << mEFPT_PROC << std::endl;
  std::cout << "farm_update: " << t_farm_update << std::endl;
  std::cout << "val_update: " << t_val_update << std::endl;
}


FarmAdapter::FarmAdapter(const FarmAdapter& )
{
  // implemented only to forbid copying

}

FarmAdapter::~FarmAdapter()
{
  ERS_DEBUG( 3, "FarmAdapter::~FarmAdapter()" );
}

void FarmAdapter::Configure(){
  //  mPartition = 0;
  if(!mPartition){
    mPartition = new IPCPartition(pPartitionName);
  }
  //  FindServersMap();
}


void FarmAdapter::FindServersMap(){
  mPartition = new IPCPartition(pPartitionName);
  // map l2pu/efpt ->  pc-tdq-xpu-...
  mMap_ptpu_xpu.clear();
  // in the following key is xpu and value is rack, SV, etc...
  // maps for L2 // only SV --> xpu ; rack -> xpu are important
  mMap_xpu_sv.clear();
  mMap_xpu_l2rack.clear();
  // maps for EF same for EF
  mMap_xpu_efrack.clear();
  mMap_xpu_sfi.clear();
  // clear the average map
  mMap_avg.clear();

  
  infoRec_VectorL2.clear();
  infoRec_VectorEF.clear();

  
  const char* L2Segment_pattern_c = L2Segment_pattern.c_str();
  const char* L2Supervisor_pattern_c = L2Supervisor_pattern.c_str();
  const char* EFRack_pattern_c = EFRack_pattern.c_str();
  // const char* L2PU_pattern_c = "(L2PU-[0-9]{4}).*";
  //  const char* XPU_pattern_c = "PT-1:EF-Segment.*:(pc-tdq-xpu-[0-9]{4}):.";
  const char* L2PU_pattern_c =  mL2PU_PROC.c_str();
  const char* XPU_pattern_c = mEFPT_PROC.c_str();
  const char* XPU2_pattern_c = ".*(pc-tdq-xpu-[0-9]{4})";
  const char* XPU3_pattern_c = mPMG_L2_pattern.c_str(); // "PMG.*";
  const char* XPU4_pattern_c = mPMG_EF_pattern.c_str(); //".*(pc-tdq-xpu-[0-9]{4}).*\|.PT.*:EF.*(pc-tdq-xpu-.*:.)";
  regex r_L2Sg(L2Segment_pattern_c);
  regex r_L2Sv(L2Supervisor_pattern_c);
  regex r_EF(EFRack_pattern_c);
  regex r_L2PU(L2PU_pattern_c);
  regex r_XPU(XPU_pattern_c);
  regex r_XPU2(XPU2_pattern_c);
  regex r_XPU3(XPU3_pattern_c);
  regex r_XPU4(XPU4_pattern_c);

  // map l2pu/efpt ->  pc-tdq-xpu-...
  ISInfoIterator *ii2 = NULL;
  try {
    ii2 = new ISInfoIterator(*mPartition, "PMG", PMGPublishedProcessData::type() && "pc-tdq-xpu.*|.*");
  } catch (daq::is::RepositoryNotFound &ex){
    ers::warning(farmadapter::Issue(ERS_HERE, "IS Repository not found " 
				    +boost::lexical_cast<std::string>(ex.what())));
  } catch (daq::is::InvalidCriteria &ex) {
    ers::warning(farmadapter::Issue(ERS_HERE, "Invalid criteria " +boost::lexical_cast<std::string>( ex.what())));
  }
  //ISInfoIterator ii2(*mPartition, "PMG");
  while ( (*ii2)() ) {
    cmatch res_XPU3,res_XPU4;
    //if(regex_match(ii2->name(), res_XPU3, r_XPU3)){
    if(regex_match(ii2->name().c_str(), res_XPU3, r_XPU3)){
      string ptpu_str = res_XPU3[2].str(); // 
      string xpu_str = res_XPU3[1].str(); // 
      std::pair<string,string> mPair_ptpu_xpu(ptpu_str,xpu_str);
      mMap_ptpu_xpu.insert(mPair_ptpu_xpu);
    }  
    //if(regex_match(ii2->name(), res_XPU4, r_XPU4)){
    if(regex_match(ii2->name().c_str(), res_XPU4, r_XPU4)){
      string ptpu_str = res_XPU4[2].str(); // 
      string xpu_str = res_XPU4[1].str(); // 
      std::pair<string,string> mPair_ptpu_xpu(ptpu_str,xpu_str);
      mMap_ptpu_xpu.insert(mPair_ptpu_xpu);
    }
  }
#ifdef DEBUG_FARM
  std::cout << "Printing mMap_ptpu_xpu (pc-tdq-xpu-xxx  --> L2PUxyz /pc-tdq-xpu-xxx:y )" << std::endl;
  std::cout << "Size of mMap_ptpu_xpu is:" << mMap_ptpu_xpu.size() << std::endl;
  std::multimap<std::string, std::string>::iterator mMap_it;
  for(mMap_it=mMap_ptpu_xpu.begin(); mMap_it!=mMap_ptpu_xpu.end();++mMap_it)
    std::cout << "Map( " << mMap_it->first << " ) =  " <<  mMap_it->second << std::endl;
#endif
  
  
  //  ISServerIterator it(*mPartition);
  ISServerIterator *it = NULL;
  try {
    it = new ISServerIterator(*mPartition);
  } catch( daq::ipc::InvalidPartition &ex){
    ers::warning(farmadapter::Issue(ERS_HERE, "Invalid partition " +boost::lexical_cast<std::string>( ex.what())));
  } 
  while( (*it)() ) {
    // start find L2SV <---> L2SubSeg 
    cmatch res_L2Sg; 
    if(regex_match(it->name(), res_L2Sg, r_L2Sg)){ // match L2SegmentPattern
      // this is DF-L2-([0-9]{1})-rack-(Y[0-9]{2}-[0-9]{2}D[0-9]).*
      //               (  SV    )      ( rack                   )
      string sv_str =  "L2SV_"+res_L2Sg[1].str(); // this is the SuperVisor index
      string rack_str = "L2Rack_"+res_L2Sg[2].str(); // this it the rack index
      // now loop on ISInfoIterator to get L2PU names
      infoRec_VectorL2.push_back(it->name());
      
      ISInfoIterator *ii = NULL;
      try {
	ii = new ISInfoIterator(*mPartition, it->name(), PROC::type() && "L2PU.*" );
      } catch (daq::is::RepositoryNotFound &ex){
	ers::warning(farmadapter::Issue(ERS_HERE, "IS Repository not found " +boost::lexical_cast<std::string>( ex.what())));
      } catch (daq::is::InvalidCriteria &ex) {
	ers::warning(farmadapter::Issue(ERS_HERE, "Invalid criteria PORC and L2PU" +boost::lexical_cast<std::string>( ex.what())));
      }
      while( (*ii)()) {  // start loop to determine L2PU
	cmatch res_L2PU;
	//if(regex_match(ii->name(), res_L2PU, r_L2PU)){
	if(regex_match(ii->name().c_str(), res_L2PU, r_L2PU)){
	  string l2pu_str =  res_L2PU[1].str();
	  // now find the xpu associated to l2pu
	  string xpu_str = mMap_ptpu_xpu.find(l2pu_str)->second;
	  std::pair<string,string> mPair_xpu_sv(xpu_str,sv_str);
	  mMap_xpu_sv.insert(mPair_xpu_sv);
	  std::pair<string,string> mPair_xpu_l2rack(xpu_str,rack_str);
	  mMap_xpu_l2rack.insert(mPair_xpu_l2rack);
	}
      } // end loop to determine L2PU
    } // end of match L2SegmentPattern
        
    // match EF
    cmatch res_EF;
    if(regex_match(it->name(), res_EF, r_EF)){ // match EFRackPattern
      //  DF-EF-Segment-([0-9]{1,2})-rack-(Y[0-9]{2}-[0-9]{2}D[0-9]).*"
      //                (   sfi    )      (      rack              )          
      // to check.... AS
      string rack_str =  "EFRack_"+res_EF[2].str(); // this is the "n" index --> rack
      string sfi_str = "EFSfi_"+res_EF[1].str(); // this it the "m" index --> sfi
      infoRec_VectorEF.push_back(it->name());
      ISInfoIterator *ief = NULL;
      try {
	ief = new ISInfoIterator(*mPartition,it->name(), PROC::type() && "PT-.*" );
      } catch (daq::is::RepositoryNotFound &ex){
	ers::warning(farmadapter::Issue(ERS_HERE, "IS Repository not found " +boost::lexical_cast<std::string>( ex.what())));
      } catch (daq::is::InvalidCriteria &ex) {
	ers::warning(farmadapter::Issue(ERS_HERE, "Invalid criteria " +boost::lexical_cast<std::string>( ex.what())));
      }
      while( (*ief)() ){// loop to find efpt
	cmatch res_EFPT;
	if(regex_match(ief->name().c_str(), res_EFPT, r_XPU)){
	  string xpu_str = res_EFPT[1].str();
	  // rack <--> efpt
	  std::pair<string,string> mPair_xpu_efrack(xpu_str,rack_str);
	  mMap_xpu_efrack.insert(mPair_xpu_efrack);
	  // sfi <--> efpt
	  std::pair<string,string> mPair_xpu_sfi(xpu_str,sfi_str);
	  mMap_xpu_sfi.insert(mPair_xpu_sfi);
	}
      }
    } // end on match EF server
  } // end loop on ISServer iterators
#ifdef DEBUG_FARM
  std::cout << "Dumping mMap_xpu_l2rack" << std::endl;
  std::multimap<std::string, std::string>::iterator mMap_it2;
  for(mMap_it2=mMap_xpu_l2rack.begin(); mMap_it2!=mMap_xpu_l2rack.end();++mMap_it2)
    std::cout << "mMap_xpu_l2rack( " << mMap_it2->first << " ) =  " <<  mMap_it2->second << std::endl;
  
  std::cout << "Dumping mMap_xpu_sv" << std::endl;
  for(mMap_it2=mMap_xpu_sv.begin(); mMap_it2!=mMap_xpu_sv.end();++mMap_it2)
    std::cout << "mMap_xpu_sv( " << mMap_it2->first << " ) =  " <<  mMap_it2->second << std::endl;
  
  std::cout << "Dumping mMap_xpu_efrack" << std::endl;
  for(mMap_it2=mMap_xpu_efrack.begin(); mMap_it2!=mMap_xpu_efrack.end();++mMap_it2)
    std::cout << "mMap_xpu_efrack( " << mMap_it2->first << " ) =  " <<  mMap_it2->second << std::endl;
  
  std::cout << "Dumping mMap_xpu_sfi" << std::endl;
  for(mMap_it2=mMap_xpu_sfi.begin(); mMap_it2!=mMap_xpu_sfi.end();++mMap_it2)
    std::cout << "mMap_xpu_sfi( " << mMap_it2->first << " ) =  " <<  mMap_it2->second << std::endl;
#endif
  
  
  // check if structure of farm changed wrt the previous
  int t_num_l2xpu     = mMap_xpu_l2rack.size();
  int t_num_efxpu     = mMap_xpu_efrack.size();
  if(t_num_l2xpu     != num_l2xpu     ||
     t_num_efxpu     != num_efxpu) {
    m_farms_changed = true;
  }
  num_l2xpu = t_num_l2xpu;
  num_efxpu = t_num_efxpu;
  if(m_farms_changed) std::cout << "FarmAdapter:: Farm Structure Changed!" << std::endl; 
  if(m_is_first_event){
    std::cout << "Num L2 Pu     " << num_l2xpu << std::endl;
    std::cout << "Num EF Pt     " << num_efxpu << std::endl;
  }
  // now convert all that in trees and branches // but this will be just a vector of Branches
  // for now just don;t do it
  
}// end method find server

// is not used anymore but still useful may be
void FarmAdapter::PublishTree(std::string server, const char* info,  std::vector<Tree> my_tree){
  ISInfoDictionary dict( *mPartition );
  std::string str_server = server;
  std::string str_info = info;
  std::string str_add = str_server + "."; 
  str_add = str_add + info;
  std::vector<Tree>::iterator it_t;
  int it=0;
  for(it_t = my_tree.begin();it_t != my_tree.end();++it_t){
    std::string s_it = "";
    char buf[11];
    std::string str_add2; 
    str_add2 = str_add + "_";
    // str_add2 = str_add;
    sprintf(buf, "%d",it);
    s_it += buf;
    str_add2 += s_it;
#ifdef DEBUG_FARM
    if(m_is_first_event){
      std::cout << "Publishing   :" << std::endl;
      (*it_t).print(std::cout);
    }
#endif
    try {
      dict.insert(str_add2,*it_t);
    } catch(daq::is::InfoAlreadyExist&){
      dict.update(str_add2,*it_t,true);
    }
    ++it;
  }
  
  }

void FarmAdapter::ConfigureFarmTP(){
  // Only one single TP that contains:
  // L2 rack, L2 SV, EF rack, EF sfi
  my_farmtp.EraseCont();
  
  std::vector<std::string> ylabels, xlabels_farm;
  // to be defined... AS
  ylabels.push_back("ram");
  ylabels.push_back("swap");
  ylabels.push_back("load1");
  ylabels.push_back("load5");
  ylabels.push_back("load15");
  ylabels.push_back("numcpu");
  ylabels.push_back("ram_90"); // fractions of cpu with ram > 90%
  ylabels.push_back("load5_90"); // ibid...

  // here you initialize mMap_avg
  std::vector<float> my_val;
  my_val.reserve(8);
  for(int i=0;i<8;++i){
    my_val.push_back(0.);
  }
  
  std::multimap<std::string, std::string>::iterator i_map;
  // loop to get L2 rack
  for(i_map=mMap_xpu_l2rack.begin();i_map!=mMap_xpu_l2rack.end();++i_map){
    // sono qui.... qui devi inserirne solo una
    // cosi`  sembra non funzionare...
    if(mMap_avg.find((*i_map).second)!=mMap_avg.end()) continue;
    xlabels_farm.push_back((*i_map).second);
    mMap_avg.insert(MF_Pair((*i_map).second,my_val) );
  }

  // loop to get L2 SV
  for(i_map=mMap_xpu_sv.begin();i_map!=mMap_xpu_sv.end();++i_map){
    if(mMap_avg.find((*i_map).second)!=mMap_avg.end()) continue;
    xlabels_farm.push_back((*i_map).second);
    mMap_avg.insert(MF_Pair((*i_map).second,my_val) );
  }

  // loop to get EF rack
  for(i_map=mMap_xpu_efrack.begin();i_map!=mMap_xpu_efrack.end();++i_map){
    if(mMap_avg.find((*i_map).second)!=mMap_avg.end()) continue;
    xlabels_farm.push_back((*i_map).second);
    mMap_avg.insert(MF_Pair((*i_map).second,my_val) );
  }

  // loop to get EF sfi
  for(i_map=mMap_xpu_sfi.begin();i_map!=mMap_xpu_sfi.end();++i_map){
    if(mMap_avg.find((*i_map).second)!=mMap_avg.end()) continue;
    xlabels_farm.push_back((*i_map).second);
    mMap_avg.insert(MF_Pair((*i_map).second,my_val) );
  }

  my_farmtp.format(xlabels_farm,ylabels);

  
  OWLTime o_time;
  trp_utils::GetTime(o_time);
  my_farmtp.TimeStamp =  o_time;
}

void FarmAdapter::Run(){
  //  mPartition = 0;
  
  if (!mPartition) mPartition = new IPCPartition( pPartitionName );
  // build the Farm tree structure
  FindServersMap();
  // initialize TimePoints and mMap_avg
  ConfigureFarmTP();


  if(m_farms_changed) 
    std::cout << "m_farms_changed:" << std::endl;

  // here you have to loop on PMG IS and get L2PU and EFPT and summ them in mMap_Avg
  ISInfoIterator *ii = NULL;
  try {
    ii = new ISInfoIterator(*mPartition, "PMG", 
			    PMGPublishedAgentData::type()  && "AGENT.*xpu.*");
  } catch (daq::is::RepositoryNotFound &ex){
    ers::warning(farmadapter::Issue(ERS_HERE, "IS Repository not found " +boost::lexical_cast<std::string>( ex.what())));
  } catch (daq::is::InvalidCriteria &ex) {
    ers::warning(farmadapter::Issue(ERS_HERE, "Invalid criteria " +boost::lexical_cast<std::string>( ex.what())));
  }
  // il problema e`  che questo tira fuori info del tipo
  // pc-tdq-xpu-.... e devi are il match con L2PU e EFPT (piu`  semplice quest' ultimo)
  while( (*ii)() ){
    PMGPublishedAgentData my_pmg;
    ii->value(my_pmg);
    // get the name of the pc-tdq-xpu
    size_t pos1,pos2;
    std::string s_name =  ii->name();
    pos1 = s_name.find("pc");
    pos2 = s_name.find(".cern.ch");
    std::string pc_name = s_name.substr(pos1,pos2-pos1);
    // get the values
    std::vector<float> new_load;
    new_load.reserve(6);
    new_load.push_back(my_pmg.ram);
    new_load.push_back(my_pmg.swap);
    new_load.push_back(my_pmg.load1);
    new_load.push_back(my_pmg.load5);
    new_load.push_back(my_pmg.load15);
    new_load.push_back(1);
    // Here you do the average calculation
    if(mMap_xpu_l2rack.find(pc_name)!=mMap_xpu_l2rack.end()){
      SumUp(pc_name, "l2sv", new_load);
      SumUp(pc_name, "l2rack", new_load);
    } else if (mMap_xpu_efrack.find(pc_name)!=mMap_xpu_efrack.end()) {
      SumUp(pc_name, "efsfi", new_load);
      SumUp(pc_name, "efrack", new_load);
    }
  } // end of looping on all PMG
  
  
  // convert mMap_avg in timepoints
  FillTP();
#ifdef DEBUG_FARM
  std::cout << "Dumping my_farmtp 2" << std::endl;
  my_farmtp.print(std::cout);

  std::cout << "Dumping mMap_avg End" << std::endl;
  MF_it mMap_avg_it;
  for(mMap_avg_it=mMap_avg.begin();mMap_avg_it!=mMap_avg.end();++mMap_avg_it){
    std::cout << mMap_avg_it->first << std::endl;
    for(std::vector<float>::iterator it=mMap_avg_it->second.begin();
	it!=mMap_avg_it->second.end(); ++it){
      std::cout << (*it) << "  ";
    }
    std::cout << std::endl;
  }
#endif
  // publish TimePOints
  // do we really need TimePointAsyncSender ? 
  
  ISInfoDictionary *dict = NULL;
  try{
    dict = new ISInfoDictionary(*mPartition );
  } catch(...){
    ers::warning(farmadapter::Issue(ERS_HERE, "IS Cant Instantiate ISInfoDictionary "));
  }
  try {
    dict->checkin("ISS_TRP.Farm",my_farmtp, true);
  } catch(daq::is::InvalidName &ex){
    ers::warning(farmadapter::Issue(ERS_HERE, "ISInfoDictionary  Invalid Name" +boost::lexical_cast<std::string>( ex.what())));
  } catch(daq::is::RepositoryNotFound &ex) {
    ers::warning(farmadapter::Issue(ERS_HERE, "Repository not found" +boost::lexical_cast<std::string>( ex.what())));
  } catch(daq::is::InfoNotCompatible &ex){
    ers::warning(farmadapter::Issue(ERS_HERE, "Info not compatible" +boost::lexical_cast<std::string>( ex.what())));
  }
  if(m_is_first_event) m_is_first_event = false;
}


// converts map to vector of trees
void FarmAdapter::Map2Tree(std::multimap<std::string, std::string> mMapTree,
			   std::multimap<std::string, std::string> mMapBranch,
			   std::vector<Tree>& mTreeVec){
  // find time, run and lumiblock
  OWLTime o_time;
  trp_utils::GetTime(o_time);
  unsigned int m_run;
  unsigned short m_lb;
  trp_utils::GetRunParams(pPartitionName,m_run,m_lb);
  
  
  std::multimap<std::string, std::string>::iterator mMap_it1,mMap_it2;
  mTreeVec.clear(); // this is the output
  // loop on subseg<-->l2pu multimap (the branch)
  //  Branch *mBranch;
  //  mBranch->EraseCont();
  std::vector<Branch> mVecBranch;
  int n_p=0;
  int n_ss = 0;
  //  Tree mTree;
  // mTree.EraseCont();
  // ************************************************
  for(mMap_it2=mMapTree.begin();mMap_it2!=mMapTree.end();
      ++mMap_it2){ // loop on Tree
    std::string key_t = (*mMap_it2).first;
    std::string val_t = (*mMap_it2).second;
    if(n_ss==0){ // starting of a a new Tree
      //std::cout << "first element of Tree" << std::endl;
      mTree = new Tree();
      mTree->EraseCont();
    }
    int num_subseg = mMapTree.count(key_t); // number of subsegments for that SV
    // --------------------------------------------
    for(mMap_it1=mMapBranch.begin();mMap_it1!=mMapBranch.end();
	++mMap_it1){ // loop on Branch
      std::string key_b = (*mMap_it1).first;
      std::string val_b = (*mMap_it1).second;
      if(val_t!=key_b) {
	continue;
      }
      int num_pu = mMapBranch.count(key_b); // number of pu for that 
      if(n_p==0){  // first element of branch
	mBranch = new Branch();
        mBranch->EraseCont();
      }
      (mBranch->Values).push_back(val_b);
      mBranch->Key=key_b; 
      mBranch->RunNumber = m_run;
      mBranch->LumiBlock = m_lb;
      mBranch->TimeStamp = o_time;
      if(num_pu==(n_p+1)){ // last element of branch
	mVecBranch.push_back(*mBranch);
	mTree->branchlist=mVecBranch;
	n_p=-1;
      } 
      if(!((n_p==0)||(num_pu==(n_p+1)))){ // in the middle
      }
      ++n_p;
    }// end loop on Branch
    // --------------------------------------------
    if(num_subseg==(n_ss+1)){ // last element of tree
      // (mTree.Values).push_back(val_t);
      mTree->Key=key_t;
      mTree->RunNumber = m_run;
      mTree->LumiBlock = m_lb;
      mTree->TimeStamp = o_time;
      mTreeVec.push_back(*mTree);
      n_ss=-1;
    } 
    if(!((n_ss==0)||(num_subseg==(n_ss+1)))){ // in the middle
    }
    //      (mTree.Values).push_back(val_t);
    ++n_ss;
  } // end loop on Tree
  // ************************************************
  // avoid memory leaks
  // delete mTree;
  // delete mBranch;
  
} // end of method

// --------------------------------------------------------------
// sums up and perform average for all pu_name ancestors
void FarmAdapter::SumUp(std::string pu_name, std::string type, std::vector<float> &loads){
  // four different types: l2sv, l2rack, efsfi, efrack
  // find all ancestors of xpu
  // here subseg
  std::string anc_name = GetAncestor(type,pu_name);
#ifdef DEBUG_FARM
  if(m_is_first_event) std::cout << "Ancestor type: " << type << " for:" 
				 << pu_name << "  is:" << anc_name << std::endl;
#endif

  //  for(int it=0;it<7;++it) { my_avg[it] = (mMap_avg.find(subseg_name)->second)[it];}
  MF_it my_it= mMap_avg.find(anc_name);
  if(my_it==mMap_avg.end()){
    std::cout << "Cannot find ancestor for pu:" << pu_name << "  and type:" <<  type << std::endl;
    return;
  }
  std::vector<float> my_avg;
  my_avg.reserve(8);
  my_avg = my_it->second;
  mMap_avg.erase(my_it);
  if(my_avg[5]==0){
    my_avg[0] = loads[0];
    my_avg[1] = loads[1];
    my_avg[2] = loads[2];
    my_avg[3] = loads[3];
    my_avg[4] = loads[4];
  } else {
    my_avg[0] += ( loads[0]  - my_avg[0]) /  my_avg[5];
    my_avg[1] += ( loads[1]-   my_avg[1]) /  my_avg[5];
    my_avg[2] += ( loads[2]-   my_avg[2]) /  my_avg[5];
    my_avg[3] += ( loads[3]-   my_avg[3]) /  my_avg[5];
    my_avg[4] += ( loads[4]-   my_avg[4]) /  my_avg[5];
  }
  ++my_avg[5]; // this is n
  if(loads[0]>=90) ++my_avg[6];
  if(loads[3]>=90) ++my_avg[7];
  // here you set this in the map
  mMap_avg.insert ( MF_Pair(anc_name,my_avg) );
}
//-------------------------------------------------------------
std::string FarmAdapter::GetAncestor(std::string type, std::string pu_name2){
  std::multimap<std::string, std::string>::iterator mMap_it;
  std::string anc = "default";
  if(type=="l2sv"){
    mMap_it = mMap_xpu_sv.find(pu_name2); // questo e` giusto !!
    if(mMap_it==mMap_xpu_sv.end()){
      std::cout << "No Anc for type:" << type << " and xpu:" <<   pu_name2 << std::endl;
    } else {
      anc = mMap_it->second;
    }
  } else if(type=="l2rack"){
    mMap_it = mMap_xpu_l2rack.find(pu_name2); // questo e` giusto !!
    if(mMap_it==mMap_xpu_l2rack.end()){
      std::cout << "No Anc for type:" << type << " and xpu:" <<   pu_name2 << std::endl;
    } else {
      anc = mMap_it->second;
    }
  }else if(type=="efrack"){
    mMap_it = mMap_xpu_efrack.find(pu_name2); // questo e` giusto !!
    if(mMap_it==mMap_xpu_efrack.end()){
      std::cout << "No Anc for type:" << type << " and xpu:" <<   pu_name2 << std::endl;
    } else {
      anc = mMap_it->second;
    }
  }else if(type=="efsfi"){
    mMap_it = mMap_xpu_sfi.find(pu_name2); // questo e` giusto !!
    if(mMap_it==mMap_xpu_sfi.end()){
      std::cout << "No Anc for type:" << type << " and xpu:" <<   pu_name2 << std::endl;
    } else {
      anc = mMap_it->second;
    }
  }
  return(anc);
}

void FarmAdapter::FillTP(){
  // fill l2svTP
  MF_it mMap_avg_it;
  for(mMap_avg_it=mMap_avg.begin();mMap_avg_it!=mMap_avg.end();++mMap_avg_it){
    std::string x_bin = mMap_avg_it->first;
    std::vector<float> new_load = mMap_avg_it->second;
    
    my_farmtp.set(x_bin, "ram", new_load[0]);
    my_farmtp.set(x_bin, "swap", new_load[1]);
    my_farmtp.set(x_bin, "load1", new_load[2]);
    my_farmtp.set(x_bin, "load5", new_load[3]);
    my_farmtp.set(x_bin, "load15", new_load[4]);
    my_farmtp.set(x_bin, "numcpu", new_load[5]);
    my_farmtp.set(x_bin, "ram_90", new_load[6]);
    my_farmtp.set(x_bin, "load5_90", new_load[7]);
    
  }
}


