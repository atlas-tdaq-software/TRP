#ifndef FARMADAPTER_H
#define FARMADAPTER_H
/** 
 * @file FarmAdapter
 * @author Antonio Sidoti
 * @date 21/01/2009
 * @brief Gets farms structure and CPU
 */

#include <owl/regexp.h>
#include <ipc/core.h>
#include <ipc/partition.h>
#include <oh/OHRootReceiver.h>
#include <oh/OHSubscriber.h>
#include <owl/regexp.h>

#include "TRP/IS_LB.h"
#include "TRP/PROC.h"
#include "TRP/L2SV.h"
#include "TRP/L2PU.h"
#include "TRP/PT.h"
#include "TRP/Tree.h"


#include "TRP/TimePoint_IS.h"


#include <time.h>

#include <vector>
#include <map>

class FarmAdapter {
 public:
  void Run();
  void Configure();
  FarmAdapter(std::string partition_name,
	      std::string server_name, std::string out_name,
	      std::string l2_seg, std::string l2_sv,
	      std::string ef_rack, std::string pmg_l2, 
	      std::string pmg_ef, std::string l2pu_proc, 
	      std::string efpt_proc,
	      int farm_update, int val_update);          // Way of implementing the singleton pattern.
  ~FarmAdapter();    // Way of implementing the singleton pattern.
  FarmAdapter( const FarmAdapter & );     // Way of implementing the singleton pattern.
  void ConfigureFarmTP();
  //  void PublishTree(const char* server, const char* info,  std::vector<Tree> my_tree);
  void PublishTree(std::string  server, const char* info,  std::vector<Tree> my_tree);
  void FindServersMap();
 private:

  std::string pPartitionName;
  std::string pServerName;
  std::string pOutName;
  IPCPartition * mPartition;    // Partition object, provides connection to the IS.
  std::string  L2Segment_pattern, L2Supervisor_pattern, EFRack_pattern;
  std::string  mPMG_L2_pattern, mPMG_EF_pattern;
  std::string  mL2PU_PROC, mEFPT_PROC;
  const char* L2Segment_pattern_c;
  const char* L2Supervisor_pattern_c;
  const char* EFRack_pattern_c;
  std::vector<std::string> infoRec_VectorL2;
  std::vector<std::string> infoRec_VectorEF;
  PROC my_proc;
  TimePoint_IS my_farmtp;
  time_t prev_t, prev_pu_t;
  // the map of the structure
  // iterator
  std::multimap<std::string, std::string>::iterator mMap_it2;
  std::multimap<std::string, std::string>::iterator mMap_it3;
  // map for EF/PT -->pc-tdq-xpu-*
  std::multimap<std::string, std::string> mMap_ptpu_xpu;
  // maps for L2
  std::multimap<std::string, std::string> mMap_xpu_l2rack;
  std::multimap<std::string, std::string> mMap_xpu_sv;
  // mapfs for EF
  std::multimap<std::string, std::string> mMap_xpu_efrack;
  std::multimap<std::string, std::string> mMap_xpu_sfi;
  
  // here is the maps that stores the containers per subseg, efd, rack, SV
  typedef  std::map<std::string, std::vector<float> >  MapFloat;
  typedef  std::pair<std::string,std::vector<float> > MF_Pair;
  typedef  std::map<std::string, std::vector<float> >::iterator MF_it;
  MapFloat mMap_avg;
  int num_l2xpu;
  int num_efxpu;
  bool m_farms_changed;
  void Map2Tree(std::multimap<std::string, std::string> mMapTree,
		std::multimap<std::string, std::string> mMapBranch,
		std::vector<Tree>& mTreeVec);
  std::string GetAncestor(std::string type, 
			  std::string pu_name2);
  void SumUp(std::string pu_name, std::string type, std::vector<float> &loads);
  void FillTP();
  Branch *mBranch;
  Tree *mTree;
  int t_farm_update;
  int t_val_update;
  bool m_is_first_event;
};

#endif
